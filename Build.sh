#***********************************************************
#* Copyright (C) 2018, Shanghai NIO VEHICLE CO., LTD
#* All rights reserved.
#* Product        : ZOO
#* Component id   : 
#* File Name      : 
#* Description    : Build 
#* History        : 
#* Version        date          author         context 
#* V1.0.0         2019-01-11    Generator      created
#*************************************************************
#!/bin/bash
ZOO_PATH="$( cd "$( dirname "$0" )" && pwd )"
cd $ZOO_PATH/SRC

#Build arm
make clean && make 

#Build x86
make clean && make ARCH=x86

cd ..
