#!/usr/bin/expect -f

#############################################################################
#  Define Global Environment 
#############################################################################
set timeout 60
set user zoo
set ip 10.110.19.90
set PROCESS [lindex $argv 1]
set UT utmf_$PROCESS
set EXEC_PATH [lindex $argv 2]

#############################################################################
#  Pkill process 
#############################################################################
spawn ssh -o "StrictHostKeyChecking no" $user@$ip "cd $EXEC_PATH && pkill $PROCESS"
expect {
send "exit 0"
expect "*$*"
}

#############################################################################
#  Run process && Execute unit test
#############################################################################
spawn ssh -o "StrictHostKeyChecking no" $user@$ip "cd $EXEC_PATH && ./$PROCESS& && ./$UT"
expect {
send "exit 0"
expect "*$*"
}

#############################################################################
#  Kill unit test
#############################################################################
spawn ssh -o "StrictHostKeyChecking no" $user@$ip "cd $EXEC_PATH && pkill $PROCESS && pkill $UT"
expect {
send "exit 0"
expect "*$*"
}

