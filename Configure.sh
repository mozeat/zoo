#***********************************************************
#* Copyright (C) 2018, Shanghai NIO VEHICLE CO., LTD
#* All rights reserved.
#* Product        : ZOO
#* Component id   : 
#* File Name      : 
#* Description    : Push target sw package to pre-publisher gitlab.
#* History        : 
#* Version        date          author         context 
#* V1.0.0         2019-01-11    Generator      created
#*************************************************************
#!/bin/bash
ZOO_PATH="$( cd "$( dirname "$0" )" && pwd )"
ZOO_BIN_X86=$ZOO_PATH/IT/bin/linux/x86
ZOO_LIB_X86=$ZOO_PATH/IT/lib/linux/x86
QT_BIN=/opt/Qt5.11.2/5.11.2/gcc_64/bin
QT_CREATOR_BIN=/opt/Qt5.11.2/Tools/QtCreator/bin
ARM_LINIX_GCC=/opt/arm/arm-linux-gcc/bin
export PATH=$ARM_LINIX_GCC:$QT_BIN:$QT_CREATOR_BIN:$ZOO_BIN_X86:${PATH}
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$ZOO_LIB_X86:/usr/local/lib
