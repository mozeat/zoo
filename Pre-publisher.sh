#***********************************************************
#* Copyright (C) 2018, Shanghai NIO VEHICLE CO., LTD
#* All rights reserved.
#* Product        : ZOO
#* Component id   : 
#* File Name      : 
#* Description    : Push target sw package to pre-publisher gitlab.
#* History        : 
#* Version        date          author         context 
#* V1.0.0         2019-01-11    Generator      created
#*************************************************************
#!/bin/bash

#############################################################################
#  Define Global Environment 
#############################################################################
PACKAGE_PATH=$1
PACKAGE_NAME=$2
VERSION=$3
GIT_PARENT_DIR=$HOME
GIT_PREPUBLISH_DIR=$GIT_PARENT_DIR/Pre-publisher
PROJRCT=ZOO

#############################################################################
#  Step 1: Remove Old Repository if exists
#############################################################################
if [ -d "$GIT_PREPUBLISH_DIR" ]; then
	rm -rf $GIT_PREPUBLISH_DIR	
	echo "$GIT_PREPUBLISH_DIR"
fi 

#############################################################################
#  Step 2: Git pull Pre-publisher repository
#############################################################################
cd $GIT_PARENT_DIR
git clone git@git.nevint.com:PESW/Pre-publisher.git
cd $GIT_PREPUBLISH_DIR

#############################################################################
#  Step 3: Copy package to repository dir,prepare add
#############################################################################
SRC_FILE=$PACKAGE_PATH/$PACKAGE_NAME
DST_DIR=$GIT_PREPUBLISH_DIR/$PROJRCT
if [ ! -d "$DST_DIR" ]; then
	echo "create path: $DST_DIR"
	mkdir -p $DST_DIR
fi
DST_FILE=$GIT_PREPUBLISH_DIR/$PROJRCT/$PACKAGE_NAME
if [ ! -f "$DST_FILE" ]; then	
	echo "Copy $SRC_FILE to path: $GIT_PREPUBLISH_DIR/$PROJRCT"
else
	echo "[Warning]Replace old $DST_FILE to $SRC_FILE"
fi
cp -f $SRC_FILE $DST_DIR/

#############################################################################
#  Step 4: Push new package to master branch
#############################################################################
git add .
git commit -m "release for $VERSION"
git push origin master
exit 0