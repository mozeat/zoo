#!/bin/bash

#############################################################################
#  Define Global Environment 
#############################################################################
PROJECT_PATH=$(pwd)
echo "[unit test]:: project path: $PROJECT_PATH"
RELATIVE_PATH=IT/bin/linux/x86
echo "[unit test]:: execute relative path: $RELATIVE_PATH"
EXECUTE_PATH=$PROJECT_PATH/$RELATIVE_PATH
echo "[unit test]:: execute absolute path: $EXECUTE_PATH"

TEST_OBJECT=$1'MA'
echo "[unit test]:: test object: $TEST_OBJECT"

UNIT_TEST='utmf_'$TEST_OBJECT
echo "[unit test]:: it's unit test: $UNIT_TEST"

#############################################################################
#  Startup process 
#############################################################################
./SMMA &
./UMMA &

#############################################################################
#  Run process && Execute unit test
#############################################################################
sleep 1
echo "[unit test]:: start up $TEST_OBJECT"
./$TEST_OBJECT &

sleep 1
echo "[unit test]:: start up $UNIT_TEST"
./$UNIT_TEST

#############################################################################
#  Kill unit test
#############################################################################
echo "[unit test]:: shutdown $TEST_OBJECT ..."
ps -ef | grep $TEST_OBJECT | grep -v grep | awk '{print $2}' | xargs kill -9
echo "[unit test]:: shutdown $TEST_OBJECT ...SUCCESS"

echo "[unit test]:: shutdown SMMA ..."
ps -ef | grep SMMA | grep -v grep | awk '{print $2}' | xargs kill -9
echo "[unit test]:: shutdown SMMA ...SUCCESS"

echo "[unit test]:: shutdown UMMA ..."
ps -ef | grep UMMA | grep -v grep | awk '{print $2}' | xargs kill -9
echo "[unit test]:: shutdown UMMA ...SUCCESS"

