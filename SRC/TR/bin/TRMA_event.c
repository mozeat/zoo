/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : TRMA_event.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-03-29    Generator      created
*************************************************************/
#include <TRMA_event.h>

/**
 *@brief TRMA_raise_4A_set_mode
 *@param resource_id
 *@param component_id
 *@param trace_mode
 *@param sim_mode
 *@param watch_mode
**/
ZOO_INT32 TRMA_raise_4A_set_mode(IN ZOO_INT32 error_code,
                                     IN TR4I_REPLY_HANDLE reply)
{
    TR4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply == NULL)
    {
        rtn = TR4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply->reply_wanted == ZOO_TRUE)
        {
            rtn = TR4I_get_reply_message_length(TR4A_SET_MODE_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (TR4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = TR4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = TR4A_SET_MODE_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = TR4I_send_reply_message(reply->reply_addr,reply->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = TR4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply != NULL)
    {
        MM4A_free(reply);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return rtn;
}

/**
 *@brief TRMA_raise_4A_get_mode
 *@param resource_id
 *@param component_id
 *@param sim_mode
 *@param trace_mode
 *@param watch_mode
**/
ZOO_INT32 TRMA_raise_4A_get_mode(IN ZOO_INT32 error_code,
                                     IN ZOO_SIM_MODE_ENUM sim_mode,
                                     IN ZOO_TRACE_MODE_ENUM trace_mode,
                                     IN ZOO_WATCH_MODE_ENUM watch_mode,
                                     IN TR4I_REPLY_HANDLE reply)
{
    TR4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply == NULL)
    {
        rtn = TR4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply->reply_wanted == ZOO_TRUE)
        {
            rtn = TR4I_get_reply_message_length(TR4A_GET_MODE_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (TR4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = TR4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = TR4A_GET_MODE_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.get_mode_rep_msg.sim_mode,&sim_mode,sizeof(ZOO_SIM_MODE_ENUM));
                memcpy(&reply_message->reply_body.get_mode_rep_msg.trace_mode,&trace_mode,sizeof(ZOO_TRACE_MODE_ENUM));
                memcpy(&reply_message->reply_body.get_mode_rep_msg.watch_mode,&watch_mode,sizeof(ZOO_WATCH_MODE_ENUM));
                rtn = TR4I_send_reply_message(reply->reply_addr,reply->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = TR4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply != NULL)
    {
        MM4A_free(reply);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return rtn;
}

/**
 *@brief TRMA_raise_4A_trace
 *@param resource_id
 *@param component_id
 *@param function_name
 *@param format_spec
**/
ZOO_INT32 TRMA_raise_4A_trace(IN ZOO_INT32 error_code,
                                  IN TR4I_REPLY_HANDLE reply)
{
    TR4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply == NULL)
    {
        rtn = TR4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply->reply_wanted == ZOO_TRUE)
        {
            rtn = TR4I_get_reply_message_length(TR4A_TRACE_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (TR4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = TR4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = TR4A_TRACE_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = TR4I_send_reply_message(reply->reply_addr,reply->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = TR4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply != NULL)
    {
        MM4A_free(reply);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return rtn;
}

/**
 *@brief TRMA_raise_4A_print_log
 *@param resource_id
 *@param level
 *@param format_spec
**/
ZOO_INT32 TRMA_raise_4A_print_log(IN ZOO_INT32 error_code,
                                      IN TR4I_REPLY_HANDLE reply)
{
    TR4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply == NULL)
    {
        rtn = TR4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply->reply_wanted == ZOO_TRUE)
        {
            rtn = TR4I_get_reply_message_length(TR4A_PRINT_LOG_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (TR4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = TR4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = TR4A_PRINT_LOG_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = TR4I_send_reply_message(reply->reply_addr,reply->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = TR4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply != NULL)
    {
        MM4A_free(reply);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return rtn;
}

/**
 *@brief TRMA_raise_4A_check_sim_mode
 *@param resource_id
 *@param component_id
 *@param sim_mode
**/
ZOO_INT32 TRMA_raise_4A_check_sim_mode(IN ZOO_INT32 error_code,
                                           IN TR4I_REPLY_HANDLE reply)
{
    TR4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply == NULL)
    {
        rtn = TR4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply->reply_wanted == ZOO_TRUE)
        {
            rtn = TR4I_get_reply_message_length(TR4A_CHECK_SIM_MODE_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (TR4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = TR4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = TR4A_CHECK_SIM_MODE_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = TR4I_send_reply_message(reply->reply_addr,reply->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = TR4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply != NULL)
    {
        MM4A_free(reply);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return rtn;
}


