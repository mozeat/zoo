/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TRACE_MANAGER_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "TRACE_MANAGER_CLASS.h"
namespace ZOO_TR
{
	boost::shared_ptr<TRACE_MANAGER_CLASS> TRACE_MANAGER_CLASS::m_instance = NULL;
	
    TRACE_MANAGER_CLASS::TRACE_MANAGER_CLASS()
    {
        //ctor
    }

    TRACE_MANAGER_CLASS::~TRACE_MANAGER_CLASS()
    {
        //dtor
    }
    
	/**
     * @brief Get instance 
    **/
	boost::shared_ptr<TRACE_MANAGER_CLASS> TRACE_MANAGER_CLASS::get_instance()
    {
        if(TRACE_MANAGER_CLASS::m_instance == NULL)
        {
        	TRACE_MANAGER_CLASS::m_instance.reset(new TRACE_MANAGER_CLASS());
        }
        return TRACE_MANAGER_CLASS::m_instance;
    }
    
    /**
     * @brief Initialize log 
    **/
    void TRACE_MANAGER_CLASS::initialize()
    {
    	
    }

	/**
     * @brief Print log to file
     * @param resource_id
     * @param componet_id
     * @param function_name
     * @param format
    **/
    void TRACE_MANAGER_CLASS::trace(IN const char* resource_id,
					    					IN const char* componet_id,
									        IN const ZOO_CHAR* thread_id,
					                    	IN const char* function_name,
					                    	IN const char* format, ...)
    {
        std::string var_str;
        va_list	ap;
        va_start(ap, format);
        int len = vsnprintf(NULL,0,format, ap);
        if (len > 0)
        {
            std::vector<char> buf(len + 1);
            vsprintf(&buf.front(), format, ap);
            var_str.assign(buf.begin(), buf.end() - 1);
        }
        va_end(ap);
        std::ostringstream os;
        os << function_name << " " << var_str;	
		ZOO_log_t(ZOO_SEVERITY_LEVEL_NORMAL,resource_id,componet_id,thread_id,os.str().c_str());
    }                      
}

