/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : TRMA_implement.c
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-11-05    Generator      created
*************************************************************/
#include <TRMA_implement.h>
#include "TR_PROCESSING_FLOW_FACADE_WRAPPER.h"
/**
 *@brief TRMA_implement_4A_set_mode
 *@param resource_id
 *@param component_id
 *@param trace_mode
 *@param sim_mode
 *@param watch_mode
**/
void TRMA_implement_4A_set_mode(IN const ZOO_CHAR* resource_id,
                                    IN const ZOO_CHAR* component_id,
                                    IN ZOO_TRACE_MODE_ENUM trace_mode,
                                    IN ZOO_SIM_MODE_ENUM sim_mode,
                                    IN ZOO_WATCH_MODE_ENUM watch_mode,
                                    IN TR4I_REPLY_HANDLE reply)
{

    ZOO_INT32 rtn = OK;
    
    /* User add ... BEGIN */
    rtn = TR_set_mode(resource_id,component_id,trace_mode,sim_mode,watch_mode);

    /* User add ... END*/
    TRMA_raise_4A_set_mode(rtn,reply);
}

/**
 *@brief TRMA_implement_4A_get_mode
 *@param resource_id
 *@param component_id
 *@param sim_mode
 *@param trace_mode
 *@param watch_mode
**/
void TRMA_implement_4A_get_mode(IN const ZOO_CHAR* resource_id,
		                                    IN const ZOO_CHAR* component_id,
		                                    IN TR4I_REPLY_HANDLE reply)
{
    ZOO_INT32 rtn = OK;
    ZOO_SIM_MODE_ENUM sim_mode = ZOO_SIM_MODE_MAX;
    ZOO_TRACE_MODE_ENUM trace_mode = ZOO_TRACE_MODE_MAX;
    ZOO_WATCH_MODE_ENUM watch_mode = ZOO_WATCH_MODE_MAX;
    /* User add ... BEGIN */

    rtn = TR_get_mode(resource_id,component_id,&sim_mode,&trace_mode,&watch_mode);
    
    /* User add ... END*/
    TRMA_raise_4A_get_mode(rtn,sim_mode,trace_mode,watch_mode,reply);
}

/**
 *@brief TRMA_implement_4A_trace
 *@param resource_id
 *@param component_id
 *@param function_name
 *@param format_spec
**/
void TRMA_implement_4A_trace(IN const ZOO_CHAR* resource_id,
		                                 IN const ZOO_CHAR* component_id,
                                         IN const ZOO_CHAR* thread_id,
		                                 IN const ZOO_CHAR* function_name,
		                                 IN const ZOO_CHAR* format_spec,
		                                 IN TR4I_REPLY_HANDLE reply)
{

    ZOO_INT32 rtn = OK;
	
    /* User add ... BEGIN */
    rtn = TR_trace(resource_id,component_id,thread_id,function_name,format_spec);

    /* User add ... END*/
    TRMA_raise_4A_trace(rtn,reply);
}

/**
 *@brief TRMA_implement_4A_print_log
 *@param resource_id
 *@param level
 *@param format_spec
**/
void TRMA_implement_4A_print_log(IN const ZOO_CHAR* resource_id,
                                     IN ZOO_SEVERITY_LEVEL_ENUM level,
                                     IN const ZOO_CHAR* thread_id,
                                     IN const ZOO_CHAR* format_spec,
                                     IN TR4I_REPLY_HANDLE reply)
{
    ZOO_INT32 rtn = OK;
    /* User add ... BEGIN */

	rtn = TR_log(resource_id,level,thread_id,format_spec);

    /* User add ... END*/
    TRMA_raise_4A_print_log(rtn,reply);
}

/**
 *@brief TRMA_implement_4A_check_sim_mode
 *@param resource_id
 *@param component_id
 *@param sim_mode
**/
void TRMA_implement_4A_check_sim_mode(IN const ZOO_CHAR* resource_id,
                                          IN const ZOO_CHAR* component_id,
                                          IN ZOO_SIM_MODE_ENUM sim_mode,
                                          IN TR4I_REPLY_HANDLE reply)
{
    ZOO_INT32 rtn = OK;
    /* User add ... BEGIN */

    rtn = TR_check_sim_mode(resource_id,component_id,sim_mode);

    /* User add ... END*/
    TRMA_raise_4A_check_sim_mode(rtn,reply);
}

