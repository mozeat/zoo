/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TRMA_executor_wrapper.c
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-11-05    Generator      created
*************************************************************/
#include "TRMA_executor_wrapper.h"
#include <signal.h>
#include <boost/smart_ptr.hpp>
#include <boost/stacktrace.hpp>
#include <sstream>
#include "LOG_MANAGER_CLASS.h"
#include "TRACE_MANAGER_CLASS.h"
#include "PROCESSING_FLOW_FACADE_CLASS.h"
#include "TR_CONFIGURE.h"
boost::shared_ptr<ZOO_TR::PROCESSING_FLOW_FACADE_INTERFACE> g_processing_flow_facade;

void default_signal_handler(IN int signal_id)
{
    ZOO_TR::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," > function entry ...");
    ZOO_TR::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," receive signal[%d] ...",signal_id);
    signal(signal_id, SIG_DFL);
    ZOO_TR::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," recover signal[%d] to default handler ...",signal_id);
    std::stringstream os;
    os << boost::stacktrace::stacktrace();
    ZOO_TR::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," stack trace: %s",os.str().c_str());

    std::string dump_core_name = "./TRMA.dump";
    if(boost::filesystem::exists(dump_core_name.data()))
    {
        boost::filesystem::remove(dump_core_name.data());
    }
    boost::stacktrace::safe_dump_to(dump_core_name.data());
    ZOO_TR::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," < function exit ...");
    return;
}

/**
 * @brief Register system signal handler,throw PARAMETER_EXCEPTION_CLASS if register fail,
 * the default signal handling is save stack trace to the log file and generate a dump file at execute path.
 * register a self-defined callback to SYSTEM_SIGNAL_HANDLER::resgister_siganl will change the default behavior.
**/
static void TRMA_register_system_signals()
{
    signal(SIGSEGV,default_signal_handler);

    /* Add more signals if needs,or register self-defined callback function
       to change the default behavior... */
}

/**
 *@brief Execute the start up flow.
 * This function is executed in 3 steps:
 * Step 1: Load configurations
 * Step 2: Create controllers
 * Step 3: Create facades and set controllers to created facades
**/
void TRMA_startup(void)
{
    ZOO_TR::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," > function entry ...");
    
    TRMA_register_system_signals();
	
	/**
	 *@brief Step 1: Load configurations
	*/
	ZOO_TR::TR_CONFIGURE::get_instance()->initialize();
	ZOO_TR::LOG_MANAGER_CLASS::get_instance()->initialize();

    /**
     *@brief Step 2: Create controllers
    */
    g_processing_flow_facade.reset(new ZOO_TR::PROCESSING_FLOW_FACADE_CLASS());

    /**
     *@brief Step 3: Create facades and set controllers to created facades
    */
    ZOO_TR::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," < function exit ...");
}

/**
 *@brief This function response to release instance or memory
**/
void TRMA_shutdown(void)
{
    /** User add */
    g_processing_flow_facade.reset();
}

/**
 *@brief Subscribe events published from hardware drivers
**/
void TRMA_subscribe_driver_event(void)
{
    /** Subscribe events */
}

