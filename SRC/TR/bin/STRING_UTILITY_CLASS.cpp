/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        :
 * Component id   :
 * File Name      : STRING_UTILITY_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-16       weiwang.sun      created
 *************************************************************/
 #include "STRING_UTILITY_CLASS.h"
namespace ZOO_TR
{
    /**
     * @brief Define default singleton
     */
    boost::shared_ptr<STRING_UTILITY_CLASS> STRING_UTILITY_CLASS::m_instance;

    STRING_UTILITY_CLASS::~STRING_UTILITY_CLASS()
    {
        //dtor
    }

    /**
     * @brief Get instance
     * @return
     */
    boost::shared_ptr<STRING_UTILITY_CLASS> STRING_UTILITY_CLASS::get_instance()
    {
        if (NULL == STRING_UTILITY_CLASS::m_instance)
        {
            STRING_UTILITY_CLASS::m_instance.reset(new STRING_UTILITY_CLASS());
        }
        return STRING_UTILITY_CLASS::m_instance;
    }

    /**
     * @brief Convert ZOO_SIM_MODE_ENUM to string
     */
    const ZOO_CHAR* STRING_UTILITY_CLASS::to_string(ZOO_SIM_MODE_ENUM sim_mode)
    {
        return get_instance()->m_sim_mode_converter.to_string(sim_mode);
    }

    /**
     * @brief Convert ZOO_TRACE_MODE_ENUM to string
     */
    const ZOO_CHAR* STRING_UTILITY_CLASS::to_string(ZOO_TRACE_MODE_ENUM trace_mode)
    {
        return get_instance()->m_trace_mode_converter.to_string(trace_mode);
    }

    /**
     * @brief Convert TM4A_WATCH_MODE_ENUM to string
     */
    const ZOO_CHAR* STRING_UTILITY_CLASS::to_string(ZOO_WATCH_MODE_ENUM watch_mode)
    {
        return get_instance()->m_watch_mode_converter.to_string(watch_mode);
    }
}

