/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TR_CONFIGURE.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "TR_CONFIGURE.h"
namespace ZOO_TR
{
    /**
     * @brief Call one for instance
     */
    static boost::once_flag g_tr_instance_once_flag = BOOST_ONCE_INIT;

    /**
     * @brief Initialize NULL for static member instance
     */
    boost::shared_ptr<TR_CONFIGURE> TR_CONFIGURE::m_instance = NULL;

    TR_CONFIGURE::TR_CONFIGURE()
    {
        //ctor
    }

    TR_CONFIGURE::~TR_CONFIGURE()
    {
        //dtor
    }

    boost::shared_ptr<TR_CONFIGURE> TR_CONFIGURE::get_instance()
    {
        /**
        * @brief call only once
        */
        boost::call_once([&]{TR_CONFIGURE::m_instance.reset(new TR_CONFIGURE());},g_tr_instance_once_flag);
        return TR_CONFIGURE::m_instance;
    }

    /**
     *@brief Initialize
    **/
    void TR_CONFIGURE::initialize()
    {
        boost::call_once([&]{TR_CONFIGURE::m_instance.reset(new TR_CONFIGURE());},g_tr_instance_once_flag);
        TR_CONFIGURE::m_instance->reload();
    }

    /**
     *@brief Reload
    **/
    void TR_CONFIGURE::reload()
    {
        this->m_execute_path = ZOO_COMMON::ENVIRONMENT_UTILITY_CLASS::get_execute_path();

		if(OK != ZOO_get_user_files_path(&this->m_user_path))    
		{
		
		}

		if(OK != ZOO_get_entity_tasks(&this->m_cfg_tasks))
		{	
    		
		}
    }

    /**
     *@brief Current execute path
    **/
    std::string TR_CONFIGURE::get_execute_path()
    {
        return this->m_execute_path;
    }

	/**
     *@brief log configure path
    **/
    std::string TR_CONFIGURE::get_log_path()
	{
		return this->m_user_path.output.log;
	}

	/**
     *@brief trace configure path
    **/
    std::string TR_CONFIGURE::get_trace_path()
	{
		return this->m_user_path.output.log;
	}

	std::vector<ZOO_TASK_STRUCT *> TR_CONFIGURE::get_cfg_tasks()
	{
	    std::vector<ZOO_TASK_STRUCT *> tasks;
        for(int i = 0; i < this->m_cfg_tasks.number ; i++)
        {
            tasks.push_back(&this->m_cfg_tasks.task[i]);
        }    
		return tasks;
	}
}

