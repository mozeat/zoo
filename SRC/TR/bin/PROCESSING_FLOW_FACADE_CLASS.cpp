/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : PROCESSING_FLOW_FACADE_INTERFACE.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-11-05    Generator      created
*************************************************************/
#include "PROCESSING_FLOW_FACADE_CLASS.h"
#include <LOG_MANAGER_CLASS.h>
namespace ZOO_TR
{

    PROCESSING_FLOW_FACADE_CLASS::PROCESSING_FLOW_FACADE_CLASS():m_task_loaded_state(ZOO_FALSE)
    {
    //ctor
    }

    PROCESSING_FLOW_FACADE_CLASS::~PROCESSING_FLOW_FACADE_CLASS()
    {
    //dtor
    }

    /**
     *@brief Set configure mode
     *@param component_id
     *@param trace_mode
     *@param sim_mode
     *@param watch_mode
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::set_mode(IN const char* resource_id,
			    											IN const char* component_id,
			                                                IN ZOO_TRACE_MODE_ENUM trace_mode,
			                                                IN ZOO_SIM_MODE_ENUM sim_mode,
			                                                IN ZOO_WATCH_MODE_ENUM watch_mode)
    {
        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"> function entry ...");

        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,
                                    ":: comp_id:%s,sim_mode=%d,trace_mode=%d,watch_mode=%d",
            component_id,sim_mode,trace_mode,watch_mode);
            
        if(resource_id == NULL)
        {
            //return TR4A_PARAMETER_ERR;
        }

        if(component_id == NULL)
        {
            return TR4A_PARAMETER_ERR;
        }

        if(trace_mode <= ZOO_TRACE_MODE_MIN || trace_mode >= ZOO_TRACE_MODE_MAX)
        {
            return TR4A_PARAMETER_ERR;
        }

        if(sim_mode <= ZOO_SIM_MODE_MIN || sim_mode >= ZOO_SIM_MODE_MAX)
        {
            return TR4A_PARAMETER_ERR;
        }

        if(watch_mode <= ZOO_WATCH_MODE_MIN || watch_mode >= ZOO_WATCH_MODE_MAX)
        {
            return TR4A_PARAMETER_ERR;
        }

        
        ZOO_INT32 result = OK;
        auto tasks = TR_CONFIGURE::get_instance()->get_cfg_tasks();
		auto ite = tasks.begin();
    	while(ite != tasks.end())
        {
        	ZOO_TASK_STRUCT * task = *ite;
	        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,
                                    "::p:%d comp:%s,name:%s,sim_mode=%d,trace_mode=%d,watch_mode=%d",task,
                                                        task->component,
                                                        task->task_name,
                                                        task->sim_mode,
                                                        task->trace_mode,
                                                        task->monitor_mode);                                         
        	if(strcmp((const char *)task->component,component_id) == 0)
        	{
        		task->monitor_mode = watch_mode;
        		task->trace_mode = trace_mode;
        		task->sim_mode = sim_mode;
	        	result = ZOO_update_task_configuration(task);
        		break;
        	}
        	++ite;
        }
        
        if(ite == tasks.end())
        {
        	result = TR4A_SYSTEM_ERR;
        }
      
        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"< function exit ...");
        return result;
    }


    /**
     *@brief Get configure mode
     *@param component_id
     *@param sim_mode
     *@param trace_mode
     *@param watch_mode
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::get_mode(IN const char* resource_id,
		    											IN  const char* component_id,
		                                                OUT ZOO_SIM_MODE_ENUM* sim_mode,
		                                                OUT ZOO_TRACE_MODE_ENUM* trace_mode,
		                                                OUT ZOO_WATCH_MODE_ENUM* watch_mode)
    {
        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"> function entry ...");

        if(component_id == NULL)
        {
            return TR4A_PARAMETER_ERR;
        }
        
        ZOO_INT32 result = OK;
        auto tasks = TR_CONFIGURE::get_instance()->get_cfg_tasks();
		auto ite = tasks.begin();
    	while(ite != tasks.end())
        {
        	ZOO_TASK_STRUCT * task = *ite;
        	if(strcmp((const char *)task->component,component_id) == 0)
        	{
        		*watch_mode = task->monitor_mode;
	        	*trace_mode = task->trace_mode;
	        	*sim_mode   = task->sim_mode;
        		break;
        	}
        	++ite;
        }
        
	    if(ite == tasks.end())
        {
        	result = TR4A_SYSTEM_ERR;
        }
        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,
            ":: comp_id:%s,sim_mode=%d,trace_mode=%d,watch_mode=%d",component_id,
            *sim_mode,* trace_mode,*watch_mode);

        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"< function exit ...");
        return result;
    }

    /**
     *@brief Trace
     *@param component_id
     *@param function_name
     *@param format_spec
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::trace(IN const char* resource_id,
    												IN const char* component_id,
    												IN const ZOO_CHAR* thread_id,
                                                    IN const char* function_name,
                                                    IN const char* format_spec)
    {
        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"> function entry ...");

        if(component_id == NULL)
        {
            return TR4A_PARAMETER_ERR;
        }
        
        ZOO_INT32 result = OK;
        auto tasks = TR_CONFIGURE::get_instance()->get_cfg_tasks();
		auto ite = tasks.begin();
    	while(ite != tasks.end())
        {
        	ZOO_TASK_STRUCT * task = *ite;
        	LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"::task:%s,trace_mode:%d",
        									component_id,task->trace_mode);
        	if(strcmp((const char *)task->component,component_id) == 0)
        	{
        		if(task->trace_mode == ZOO_TRACE_ENABLE)
        		{
        			TRACE_MANAGER_CLASS::trace(resource_id,
        						component_id,thread_id,function_name,format_spec);
        		}
        		break;
        	}
        	++ite;
        }
        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"< function exit ...");
        return result;
    }

	/**
     *@brief log
     *@param resource_id
     *@param level
     *@param format_spec
    **/
	ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::log(IN const char* resource_id,
												IN ZOO_SEVERITY_LEVEL_ENUM level,
									            IN const ZOO_CHAR* thread_id,
												IN const char* format_spec)
	{
        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"> function entry ...");
     	if(resource_id == NULL)
        {
            return TR4A_PARAMETER_ERR;
        }
        
        if(level < ZOO_SEVERITY_LEVEL_NORMAL || level > ZOO_SEVERITY_LEVEL_CRITICAL)
        {
            return TR4A_PARAMETER_ERR;
        }
        
        LOG_MANAGER_CLASS::get_instance()->log(resource_id,level,thread_id,"%s",format_spec);
        
        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"< function exit ...");
        return OK;
    }
    
    /**
     *@brief Check simulate mode
     *@param component_id
     *@param sim_mode
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::check_sim_mode(IN const char* resource_id,
    															  IN const char* component_id,
                                                            	  IN ZOO_SIM_MODE_ENUM sim_mode)
    {
        LOG_MANAGER_CLASS::get_instance()->debug(__ZOO_FUNC__,"> function entry ...");
        if(component_id == NULL)
        {
            return TR4A_PARAMETER_ERR;
        }
        
        if(sim_mode <= ZOO_SIM_MODE_MIN || sim_mode >= ZOO_SIM_MODE_MAX)
        {
            return TR4A_PARAMETER_ERR;
        }
        
        ZOO_INT32 result = ZOO_FALSE;
        std::vector<ZOO_TASK_STRUCT *> tasks = 
        				TR_CONFIGURE::get_instance()->get_cfg_tasks();
		auto ite = tasks.begin();
    	while(ite != tasks.end())
        {
        	ZOO_TASK_STRUCT * task = *ite;
        	if(strcmp((const char *)task->component,component_id) == 0)
        	{
        		if(task->sim_mode == sim_mode)
        		{
        			result = ZOO_TRUE;
        		}
        		break;
        	}
        	++ite;
        }
        LOG_MANAGER_CLASS::get_instance()->debug(__ZOO_FUNC__,"< function exit ...");
        return result;
    }
}

