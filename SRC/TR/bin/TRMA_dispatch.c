/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : TRMA_dispatch.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-03-29    Generator      created
*************************************************************/
#include <TR4A_if.h>
#include <TRMA_dispatch.h>
#include <TRMA_implement.h>

/**
 *@brief TRMA_local_4A_set_mode
 *@param resource_id
 *@param component_id
 *@param trace_mode
 *@param sim_mode
 *@param watch_mode
**/
static ZOO_INT32 TRMA_local_4A_set_mode(const MQ4A_SERV_ADDR server,TR4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    TR4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = TR4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (TR4I_REPLY_HANDLE)MM4A_malloc(sizeof(TR4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = TR4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        TRMA_implement_4A_set_mode(request->request_body.set_mode_req_msg.resource_id,
                                           request->request_body.set_mode_req_msg.component_id,
                                           request->request_body.set_mode_req_msg.trace_mode,
                                           request->request_body.set_mode_req_msg.sim_mode,
                                           request->request_body.set_mode_req_msg.watch_mode,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief TRMA_local_4A_get_mode
 *@param resource_id
 *@param component_id
 *@param sim_mode
 *@param trace_mode
 *@param watch_mode
**/
static ZOO_INT32 TRMA_local_4A_get_mode(const MQ4A_SERV_ADDR server,TR4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    TR4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = TR4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (TR4I_REPLY_HANDLE)MM4A_malloc(sizeof(TR4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = TR4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        TRMA_implement_4A_get_mode(request->request_body.get_mode_req_msg.resource_id,
                                           request->request_body.get_mode_req_msg.component_id,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief TRMA_local_4A_trace
 *@param resource_id
 *@param component_id
 *@param function_name
 *@param format_spec
**/
static ZOO_INT32 TRMA_local_4A_trace(const MQ4A_SERV_ADDR server,TR4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    TR4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = TR4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (TR4I_REPLY_HANDLE)MM4A_malloc(sizeof(TR4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = TR4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        TRMA_implement_4A_trace(request->request_body.trace_req_msg.resource_id,
                                           request->request_body.trace_req_msg.component_id,
                                           request->request_body.trace_req_msg.thread_id,
                                           request->request_body.trace_req_msg.function_name,
                                           request->request_body.trace_req_msg.format_spec,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief TRMA_local_4A_print_log
 *@param resource_id
 *@param level
 *@param format_spec
**/
static ZOO_INT32 TRMA_local_4A_print_log(const MQ4A_SERV_ADDR server,TR4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    TR4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = TR4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (TR4I_REPLY_HANDLE)MM4A_malloc(sizeof(TR4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = TR4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        TRMA_implement_4A_print_log(request->request_body.print_log_req_msg.resource_id,
                                           request->request_body.print_log_req_msg.level,
                                           request->request_body.print_log_req_msg.thread_id,
                                           request->request_body.print_log_req_msg.format_spec,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief TRMA_local_4A_check_sim_mode
 *@param resource_id
 *@param component_id
 *@param sim_mode
**/
static ZOO_INT32 TRMA_local_4A_check_sim_mode(const MQ4A_SERV_ADDR server,TR4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    TR4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = TR4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (TR4I_REPLY_HANDLE)MM4A_malloc(sizeof(TR4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = TR4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        TRMA_implement_4A_check_sim_mode(request->request_body.check_sim_mode_req_msg.resource_id,
                                           request->request_body.check_sim_mode_req_msg.component_id,
                                           request->request_body.check_sim_mode_req_msg.sim_mode,
                                           reply_handle);
    }
    return rtn;
}

/**
*@brief Dispatch message from client to server internal interface
*@param context        
*@param server        address
*@param msg           request message to server
*@param len           request message length
*@param reply_msg     reply message length to caller
*@param reply_msg_len reply message length
**/
void TRMA_callback_handler(void * context,const MQ4A_SERV_ADDR server,void * msg,ZOO_UINT32 mig_id)
{
    ZOO_INT32 rtn = OK;
    ZOO_INT32 rep_length = 0;
    TR4I_REQUEST_STRUCT *request = (TR4I_REQUEST_STRUCT*)msg;
    TR4I_REPLY_STRUCT *reply = NULL;
    if(request == NULL)
    {
        rtn = TR4A_SYSTEM_ERR;
        return;
    }

    if(OK == rtn)
    {
        switch(request->request_header.function_code)
        {
            case TR4A_SET_MODE_CODE:
                TRMA_local_4A_set_mode(server,request,mig_id);
                break;
            case TR4A_GET_MODE_CODE:
                TRMA_local_4A_get_mode(server,request,mig_id);
                break;
            case TR4A_TRACE_CODE:
                TRMA_local_4A_trace(server,request,mig_id);
                break;
            case TR4A_PRINT_LOG_CODE:
                TRMA_local_4A_print_log(server,request,mig_id);
                break;
            case TR4A_CHECK_SIM_MODE_CODE:
                TRMA_local_4A_check_sim_mode(server,request,mig_id);
                break;
            default:
                rtn = TR4A_SYSTEM_ERR;
                break;
        }
    }

    if(OK != rtn && request->request_header.need_reply)
    {
        rtn = TR4I_get_reply_message_length(request->request_header.function_code, &rep_length);
        if(OK != rtn)
        {
            rtn = TR4A_SYSTEM_ERR;
        }

        if(OK == rtn)
        {
            reply = (TR4I_REPLY_STRUCT *)MM4A_malloc(rep_length);
            if(reply == NULL)
            {
                rtn = TR4A_SYSTEM_ERR;
            }
        }

        if(OK == rtn)
        {
            memset(reply, 0x0, rep_length);
            reply->reply_header.execute_result = rtn;
            reply->reply_header.function_code = request->request_header.function_code;
            TR4I_send_reply_message(server,mig_id,reply);
        }
    }
    return;
}

