/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TR_PROCESSING_FLOW_FACADE_WRAPPER.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "TR_PROCESSING_FLOW_FACADE_WRAPPER.h"
#include "PROCESSING_FLOW_FACADE_CLASS.h"

extern ZOO_TR::PROCESSING_FLOW_FACADE_CLASS * g_processing_flow_facade;

/**
 *@brief Set configure mode
 *@param component_id
 *@param trace_mode
 *@param sim_mode
 *@param watch_mode
**/
ZOO_INT32 TR_set_mode(IN const char* resource_id,IN const char* component_id,
                                IN ZOO_TRACE_MODE_ENUM trace_mode,
                                IN ZOO_SIM_MODE_ENUM sim_mode,
                                IN ZOO_WATCH_MODE_ENUM watch_mode)
{
    if(g_processing_flow_facade != nullptr)
         return g_processing_flow_facade->set_mode(resource_id,component_id,trace_mode,sim_mode,watch_mode);
    return TR4A_SYSTEM_ERR;
}

/**
 *@brief Get configure mode
 *@param component_id
 *@param sim_mode
 *@param trace_mode
 *@param watch_mode
**/
ZOO_INT32 TR_get_mode(IN const char* resource_id,IN  const char* component_id,
                                OUT ZOO_SIM_MODE_ENUM* sim_mode,
                                OUT ZOO_TRACE_MODE_ENUM* trace_mode,
                                OUT ZOO_WATCH_MODE_ENUM* watch_mode)
{
    if(g_processing_flow_facade != nullptr)
         return g_processing_flow_facade->get_mode(resource_id,component_id,sim_mode,trace_mode,watch_mode);
    return TR4A_SYSTEM_ERR;
}


/**
 *@brief Trace
 *@param component_id
 *@param function_name
 *@param format_spec
**/
ZOO_INT32 TR_trace(IN const char* resource_id,
							IN const char* component_id,
							IN const ZOO_CHAR* thread_id,
                            IN const char* function_name,
                            IN const char* format_spec)
{
    if(g_processing_flow_facade != nullptr)
         return g_processing_flow_facade->trace(resource_id,component_id,thread_id,function_name,format_spec);
    return TR4A_SYSTEM_ERR;
}
/**
 *@brief log
 *@param resource_id
 *@param level
 *@param format_spec
**/
ZOO_INT32 TR_log(IN const char* resource_id,
							IN ZOO_SEVERITY_LEVEL_ENUM level,
							IN const ZOO_CHAR* thread_id,
							IN const char* format_spec)
{
    if(g_processing_flow_facade != nullptr)
         return g_processing_flow_facade->log(resource_id,level,thread_id,format_spec);
    return TR4A_SYSTEM_ERR;
}


/**
 *@brief Check simulate mode
 *@param component_id
 *@param sim_mode
**/
ZOO_INT32 TR_check_sim_mode(IN const char* resource_id,IN const char* component_id,
                                        IN ZOO_SIM_MODE_ENUM sim_mode)
{
    if(g_processing_flow_facade != nullptr)
        return g_processing_flow_facade->check_sim_mode(resource_id,component_id,sim_mode);
    return TR4A_SYSTEM_ERR;
}
