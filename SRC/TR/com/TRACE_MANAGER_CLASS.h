/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : TRACE_MANAGER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef TRACE_MANAGER_CLASS_H
#define TRACE_MANAGER_CLASS_H

extern "C"
{
    #include <TR4I_type.h>
    #include <ZOO_tc.h>
    #include "ZOO_log.h"
}
#include "TR_CONFIGURE.h"
#include "TR_ENUM_REGISTER_CLASS.h"
#include <stdarg.h>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <fstream>
#include <functional>

namespace ZOO_TR
{
    class TRACE_MANAGER_CLASS
    {
    public:
    
    	/**
         * @brief Constructor
        **/
        TRACE_MANAGER_CLASS();

        /**
         * @brief Destructor 
        **/
        virtual ~TRACE_MANAGER_CLASS();
    public:
    
    	/**
         * @brief Get instance 
        **/	
    	static boost::shared_ptr<TRACE_MANAGER_CLASS> get_instance();
    	
        /**
         * @brief Initialize log 
        **/
        void initialize();

        /**
		 * @brief Print log to file
		 * @param resource_id
		 * @param componet_id
		 * @param function_name
		 * @param format
		**/
        static void trace(const char* resource_id,
        					const char* componet_id,
						    IN const ZOO_CHAR* thread_id,
                        	const char* function_name,
                        	const char* format, ...);                         	
    private:
    
    	/**
         * @brief The instance 
        **/
    	static boost::shared_ptr<TRACE_MANAGER_CLASS> m_instance;
	};
}
#endif // TRACE_MANAGER_CLASS_H

