/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TR_PROCESSING_FLOW_FACADE_WRAPPER.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         20181001      mozeat.sun      created
 *************************************************************/
#ifndef TR_PROCESSING_FLOW_FACADE_WRAPPER_H
#define TR_PROCESSING_FLOW_FACADE_WRAPPER_H
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
#include <ZOO.h>
#include <ZOO_tc.h>
#include <TR4A_type.h>
    /**
     *@brief Set configure mode
     *@param component_id
     *@param trace_mode
     *@param sim_mode
     *@param watch_mode
    **/
    ZOO_INT32 TR_set_mode(IN const char* resource_id,IN const char* component_id,
                                    IN ZOO_TRACE_MODE_ENUM trace_mode,
                                    IN ZOO_SIM_MODE_ENUM sim_mode,
                                    IN ZOO_WATCH_MODE_ENUM watch_mode);


    /**
     *@brief Get configure mode
     *@param component_id
     *@param sim_mode
     *@param trace_mode
     *@param watch_mode
    **/
    ZOO_INT32 TR_get_mode(IN const char* resource_id,IN  const char* component_id,
                                    OUT ZOO_SIM_MODE_ENUM* sim_mode,
                                    OUT ZOO_TRACE_MODE_ENUM* trace_mode,
                                    OUT ZOO_WATCH_MODE_ENUM* watch_mode);

    /**
     *@brief Trace
     *@param component_id
     *@param function_name
     *@param format_spec
    **/
    ZOO_INT32 TR_trace(IN const char* resource_id,IN const char* component_id,IN const ZOO_CHAR* thread_id,
                                IN const char* function_name,
                                IN const char* format_spec);
                                
	/**
     *@brief log
     *@param resource_id
     *@param level
     *@param format_spec
    **/
	ZOO_INT32 TR_log(IN const char* resource_id,
								IN ZOO_SEVERITY_LEVEL_ENUM level,
								IN const ZOO_CHAR* thread_id,
								IN const char* format_spec);
    /**
     *@brief Check simulate mode
     *@param component_id
     *@param sim_mode
    **/
    ZOO_INT32 TR_check_sim_mode(IN const char* resource_id,IN const char* component_id,
                                            IN ZOO_SIM_MODE_ENUM sim_mode);
#ifdef __cplusplus
}
#endif // __cplusplus

#endif // TR_PROCESSING_FLOW_FACADE_WRAPPER_H
