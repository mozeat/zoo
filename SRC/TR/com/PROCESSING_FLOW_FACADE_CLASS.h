/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : PROCESSING_FLOW_FACADE_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-11-05    Generator      created
*************************************************************/
#ifndef PROCESSING_FLOW_FACADE_CLASS_H
#define PROCESSING_FLOW_FACADE_CLASS_H
#include "PROCESSING_FLOW_FACADE_INTERFACE.h"
#include "LOG_MANAGER_CLASS.h"
#include "TRACE_MANAGER_CLASS.h"
namespace ZOO_TR
{
    class PROCESSING_FLOW_FACADE_CLASS : public virtual PROCESSING_FLOW_FACADE_INTERFACE
    {
    public:
        PROCESSING_FLOW_FACADE_CLASS();
        virtual ~PROCESSING_FLOW_FACADE_CLASS();

        /**
         *@brief Set configure mode
         *@param component_id
         *@param trace_mode
         *@param sim_mode
         *@param watch_mode
        **/
        ZOO_INT32 set_mode(IN const char* resource_id,
        							IN const char* component_id,
										IN ZOO_TRACE_MODE_ENUM trace_mode,
										IN ZOO_SIM_MODE_ENUM sim_mode,
										IN ZOO_WATCH_MODE_ENUM watch_mode);


        /**
         *@brief Get configure mode
         *@param component_id
         *@param sim_mode
         *@param trace_mode
         *@param watch_mode
        **/
		ZOO_INT32 get_mode(IN const char* resource_id,
										IN const char* component_id,
										OUT ZOO_SIM_MODE_ENUM* sim_mode,
										OUT ZOO_TRACE_MODE_ENUM* trace_mode,
										OUT ZOO_WATCH_MODE_ENUM* watch_mode);

        /**
         *@brief Trace
         *@param component_id
         *@param function_name
         *@param format_spec
        **/
		ZOO_INT32 trace(IN const char* resource_id,
									IN const char* component_id,
									IN const ZOO_CHAR* thread_id,
									IN const char* function_name,
									IN const char* format_spec);

		/**
         *@brief log
         *@param resource_id
         *@param level
         *@param format_spec
        **/
		ZOO_INT32 log(IN const char* resource_id,
									IN ZOO_SEVERITY_LEVEL_ENUM level,
									IN const ZOO_CHAR* thread_id,
									IN const char* format_spec);
									
        /**
         *@brief Check simulate mode
         *@param component_id
         *@param sim_mode
        **/
        ZOO_INT32 check_sim_mode(IN const char* resource_id,
        										IN const char* component_id,
										        IN ZOO_SIM_MODE_ENUM sim_mode);
	private:
		ZOO_BOOL m_task_loaded_state;
    };
}

#endif // PROCESSING_FLOW_FACADE_CLASS_H
