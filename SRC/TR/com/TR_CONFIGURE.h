/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TR_CONFIGURE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef TR_CONFIGURE_H
#define TR_CONFIGURE_H
extern "C"
{
	#include "ZOO_if.h"
}
#include <utils/ENVIRONMENT_UTILITY_CLASS.h>
#include <boost/smart_ptr.hpp>
#include <boost/thread/once.hpp>
namespace ZOO_TR
{
    class TR_CONFIGURE
    {
    public:
        TR_CONFIGURE();
        virtual ~TR_CONFIGURE();
        static boost::shared_ptr<TR_CONFIGURE> get_instance();
    public:
        /**
         *@brief Initialize
        **/
        static void initialize();

        /**
         *@brief reload
        **/
        void reload();

        /**
         *@brief alarm db path
        **/
        std::string get_execute_path();

		/**
         *@brief log configure path
        **/
        std::string get_log_path();

        /**
         *@brief trace configure path
        **/
        std::string get_trace_path();

        std::vector<ZOO_TASK_STRUCT *> get_cfg_tasks();
    private:
        std::string m_execute_path;
        /**
         * @brief The output files path
         */
        ZOO_USER_PATH_STRUCT m_user_path;
		ZOO_TASKS_STRUCT m_cfg_tasks;
        static boost::shared_ptr<TR_CONFIGURE> m_instance;
    };
}

#endif // TR_CONFIGURE_H
