/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : TRMA_event.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-03-29    Generator      created
*************************************************************/
#ifndef TRMA_EVENT_H
#define TRMA_EVENT_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include <TR4A_if.h>
#include <TR4I_type.h>
#include <TR4A_type.h>
#include "TR4I_if.h"
/**
 *@brief TRMA_raise_4A_set_mode
 *@param resource_id
 *@param component_id
 *@param trace_mode
 *@param sim_mode
 *@param watch_mode
**/
ZOO_EXPORT ZOO_INT32 TRMA_raise_4A_set_mode(IN ZOO_INT32 error_code,
                                                IN TR4I_REPLY_HANDLE reply);


/**
 *@brief TRMA_raise_4A_get_mode
 *@param resource_id
 *@param component_id
 *@param sim_mode
 *@param trace_mode
 *@param watch_mode
**/
ZOO_EXPORT ZOO_INT32 TRMA_raise_4A_get_mode(IN ZOO_INT32 error_code,
                                                IN ZOO_SIM_MODE_ENUM sim_mode,
                                                IN ZOO_TRACE_MODE_ENUM trace_mode,
                                                IN ZOO_WATCH_MODE_ENUM watch_mode,
                                                IN TR4I_REPLY_HANDLE reply);


/**
 *@brief TRMA_raise_4A_trace
 *@param resource_id
 *@param component_id
 *@param function_name
 *@param format_spec
**/
ZOO_EXPORT ZOO_INT32 TRMA_raise_4A_trace(IN ZOO_INT32 error_code,
                                             IN TR4I_REPLY_HANDLE reply);


/**
 *@brief TRMA_raise_4A_print_log
 *@param resource_id
 *@param level
 *@param format_spec
**/
ZOO_EXPORT ZOO_INT32 TRMA_raise_4A_print_log(IN ZOO_INT32 error_code,
                                                 IN TR4I_REPLY_HANDLE reply);


/**
 *@brief TRMA_raise_4A_check_sim_mode
 *@param resource_id
 *@param component_id
 *@param sim_mode
**/
ZOO_EXPORT ZOO_INT32 TRMA_raise_4A_check_sim_mode(IN ZOO_INT32 error_code,
                                                      IN TR4I_REPLY_HANDLE reply);



#endif // TRMA_event.h
