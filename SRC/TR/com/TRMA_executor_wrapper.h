/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TRMA_executor_wrapper.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-03-29    Generator      created
*************************************************************/
#ifndef TRMA_EXECUTOR_WRAPPER_H
#define TRMA_EXECUTOR_WRAPPER_H

#ifdef __cplusplus 
extern "C" 
{
#endif

#include <ZOO.h>
#include <TR4I_type.h>
#include "ZOO_if.h"

    /**
    *@brief This function response to create factory instance or load environment configurations 
    **/
    ZOO_EXPORT void TRMA_startup(void);

    /**
    *@brief This function response to release instance or memory 
    **/
    ZOO_EXPORT void TRMA_shutdown(void);

    /**
    *@brief Subscribe events published from hardware drivers 
    **/
    ZOO_EXPORT void TRMA_subscribe_driver_event(void);


#ifdef __cplusplus 
 }
#endif

#endif // TRMA_executor_wrapper.h
