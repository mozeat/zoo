/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : LOG_MANAGER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef LOG_MANAGER_CLASS_H
#define LOG_MANAGER_CLASS_H

extern "C"
{
    #include <TR4I_type.h>
    #include <ZOO_if.h>
    #include "ZOO_log.h"
}
#include "TR_CONFIGURE.h"
#include "TR_ENUM_REGISTER_CLASS.h"
#include <sstream>
#include <iostream>
#include <stdarg.h>


namespace ZOO_TR
{
    class LOG_MANAGER_CLASS
    {
    public:
    	/**
         * @brief Constructor
        **/
        LOG_MANAGER_CLASS();

        /**
         * @brief Destructor 
        **/
        virtual ~LOG_MANAGER_CLASS();
    public:

    	/**
         * @brief Get instance 
        **/
    	static boost::shared_ptr<LOG_MANAGER_CLASS> get_instance();
    	
        /**
         * @brief Initialize log 
        **/
        void initialize();

		/**
         * @brief print log to file
         * @param resource_id
         * @param level
         * @param format_spec
        **/
        void log(IN const char* resource_id,
						IN ZOO_SEVERITY_LEVEL_ENUM level,
									IN const ZOO_CHAR* thread_id,
						IN const char* format_spec,...);
        /**
         * @brief debug info to console
         * @param function_name
         * @param format_spec
        **/
        static void debug(const char* function_name,
                         	const char* format, ...);
                         	
    private:
    	/**
         * @brief The instance 
        **/
        static boost::shared_ptr<LOG_MANAGER_CLASS> m_instance;
    };
}
#endif // LOG_MANAGER_CLASS_H
