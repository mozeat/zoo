/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : TRMA_implement.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-03-29    Generator      created
*************************************************************/
#ifndef TRMA_IMPLEMENT_H
#define TRMA_IMPLEMENT_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include <TR4A_if.h>
#include <TRMA_event.h>
#include <TR4I_type.h>
#include <TR4A_type.h>


/**
 *@brief TRMA_implement_4A_set_mode
 *@param resource_id
 *@param component_id
 *@param trace_mode
 *@param sim_mode
 *@param watch_mode
**/
ZOO_EXPORT void TRMA_implement_4A_set_mode(IN const ZOO_CHAR* resource_id,
                                               IN const ZOO_CHAR* component_id,
                                               IN ZOO_TRACE_MODE_ENUM trace_mode,
                                               IN ZOO_SIM_MODE_ENUM sim_mode,
                                               IN ZOO_WATCH_MODE_ENUM watch_mode,
                                               IN TR4I_REPLY_HANDLE reply);
/**
 *@brief TRMA_implement_4A_get_mode
 *@param resource_id
 *@param component_id
 *@param sim_mode
 *@param trace_mode
 *@param watch_mode
**/
ZOO_EXPORT void TRMA_implement_4A_get_mode(IN const ZOO_CHAR* resource_id,
                                               IN const ZOO_CHAR* component_id,
                                               IN TR4I_REPLY_HANDLE reply);
/**
 *@brief TRMA_implement_4A_trace
 *@param resource_id
 *@param component_id
 *@param function_name
 *@param format_spec
**/
ZOO_EXPORT void TRMA_implement_4A_trace(IN const ZOO_CHAR* resource_id,
                                            IN const ZOO_CHAR* component_id,
                                            IN const ZOO_CHAR* thread_id,
                                            IN const ZOO_CHAR* function_name,
                                            IN const ZOO_CHAR* format_spec,
                                            IN TR4I_REPLY_HANDLE reply);
/**
 *@brief TRMA_implement_4A_print_log
 *@param resource_id
 *@param level
 *@param format_spec
**/
ZOO_EXPORT void TRMA_implement_4A_print_log(IN const ZOO_CHAR* resource_id,
                                                IN ZOO_SEVERITY_LEVEL_ENUM level,
                                                IN const ZOO_CHAR* thread_id,
                                                IN const ZOO_CHAR* format_spec,
                                                IN TR4I_REPLY_HANDLE reply);
/**
 *@brief TRMA_implement_4A_check_sim_mode
 *@param resource_id
 *@param component_id
 *@param sim_mode
**/
ZOO_EXPORT void TRMA_implement_4A_check_sim_mode(IN const ZOO_CHAR* resource_id,
                                                     IN const ZOO_CHAR* component_id,
                                                     IN ZOO_SIM_MODE_ENUM sim_mode,
                                                     IN TR4I_REPLY_HANDLE reply);

#endif // TRMA_implement.h
