#ifndef TR_COMMON_MACRO_DEFINE_H
#define TR_COMMON_MACRO_DEFINE_H


#include <ZOO.h>
#include <exceptions/PARAMETER_EXCEPTION_CLASS.h>
#include <soci/soci.h>

/**
 * @brief Wrapper try
 */
#define __TR_TRY try

/**
 * @brief Define a macro to throw TR exception
 * @param error_code        The error code
 * @param error_message     The error message
 * @param inner_exception   The inner exception,& std::exception
 */
#define __THROW_TR_EXCEPTION(error_code,error_message,inner_exception) \
        throw ZOO_COMMON::PARAMETER_EXCEPTION_CLASS(error_code, error_message, inner_exception);

/**
 * @brief Define a macro catch soci::soci_error
 * @param continue        continue throw exception
 */
#define __TR_CATCH_DAO(continue) catch(soci::sqlite3_soci_error & e) \
        {\
            if(continue) throw ZOO_COMMON::PARAMETER_EXCEPTION_CLASS(TR4A_SYSTEM_ERR, e.what(),NULL);\
        }

/**
 * @brief Define a macro to catch all exception
 * @param result
 */
#define __TR_CATCH_ALL(result) catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & e) \
        { \
            result = e.get_error_code();\
        }\
        catch(...){}

#endif // TR_COMMON_MACRO_DEFINE_H
