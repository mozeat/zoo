/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TR_unit_test.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-11-09    Generator      created
*************************************************************/
extern "C"
{
    #include <TR4A_if.h>
    #include <TR4A_type.h>
    #include <MM4A_if.h>
    #include <ZOO_if.h>
}
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/output_test_stream.hpp>
#include <boost/test/results_reporter.hpp>
#include <boost/test/unit_test_log.hpp>
#include <string>
/**
* @brief Define a test suite entry/exit,so that the setup function is called only once
* upon entering the test suite.
*/
struct TC_TR_GOLBAL_FIXTURE
{
    TC_TR_GOLBAL_FIXTURE()
    {
        BOOST_TEST_MESSAGE("Global test environment initialize ...");
        MM4A_initialize();
    }

    ~TC_TR_GOLBAL_FIXTURE()
    {
        BOOST_TEST_MESSAGE("Global test environment terminate ...");
        MM4A_terminate();
    }
};

BOOST_TEST_GLOBAL_FIXTURE(TC_TR_GOLBAL_FIXTURE);


/**
 * @brief Define test report output formate, default --log_level=message.
*/
struct TC_TR_REPORTER
{
    TC_TR_REPORTER()
    {
        ZOO_PL_PATH_STRUCT file_path;
        BOOST_ASSERT(OK == ZOO_get_pl_files_path(&file_path));
        reporter.open(file_path.output.utils);
        boost::unit_test::unit_test_log.set_stream(reporter);
        boost::unit_test::unit_test_log.set_threshold_level(boost::unit_test::log_test_units);
    }

    ~TC_TR_REPORTER()
    {
        boost::unit_test::unit_test_log.set_stream( std::cout );
    }
    std::ofstream reporter;
};

BOOST_TEST_GLOBAL_CONFIGURATION(TC_TR_REPORTER);

