/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TR_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-09    Generator      created
*************************************************************/
extern "C"
{
    #include <TR4A_if.h>
    #include <TR4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_TR_PRINT_LOG_004)

	/**
	 *@brief 
	 *@param resource_id
	 *@param level
	 *@param format_spec
	**/
    BOOST_AUTO_TEST_CASE( TC_TR_PRINT_LOG_004_001 )
    {
        const ZOO_CHAR* resource_id = "PS-ZOO-3d98cd15-d3450184";
        ZOO_SEVERITY_LEVEL_ENUM level = ZOO_SEVERITY_LEVEL_WARNING;
        BOOST_TEST(TR4A_log(level,"test[%s]",resource_id) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()