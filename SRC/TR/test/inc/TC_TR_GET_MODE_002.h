/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TR_unit_test.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-11-09    Generator      created
*************************************************************/
extern "C"
{
    #include <TR4A_if.h>
    #include <TR4A_type.h>
    #include <MM4A_if.h>
}

BOOST_AUTO_TEST_SUITE(TC_TR_GET_MODE_002)

    /**
     *@brief
     *@param component_id
     *@param sim_mode
     *@param trace_mode
     *@param watch_mode
    **/
    BOOST_AUTO_TEST_CASE( TC_TR_GET_MODE_002_001 )
    {
        const char* component_id = "TR";
        ZOO_SIM_MODE_ENUM sim_mode = ZOO_SIM_DISABLE;
        ZOO_TRACE_MODE_ENUM trace_mode = ZOO_TRACE_DISABLE;
        ZOO_WATCH_MODE_ENUM watch_mode = ZOO_WATCH_MODE_DISABLE;
        BOOST_TEST(TR4A_get_mode(component_id,&sim_mode,&trace_mode,&watch_mode) == OK);
        BOOST_TEST(TR4A_get_mode(component_id,&sim_mode,&trace_mode,&watch_mode) == OK);
        BOOST_TEST(TR4A_get_mode(component_id,&sim_mode,&trace_mode,&watch_mode) == OK);
        BOOST_TEST(TR4A_get_mode(component_id,&sim_mode,&trace_mode,&watch_mode) == OK);
        BOOST_TEST(TR4A_get_mode(component_id,&sim_mode,&trace_mode,&watch_mode) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()
