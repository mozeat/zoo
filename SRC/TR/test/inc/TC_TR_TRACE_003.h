/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TR_unit_test.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-11-09    Generator      created
*************************************************************/
extern "C"
{
    #include <TR4A_if.h>
    #include <TR4A_type.h>
    #include <MM4A_if.h>
}
#include <thread>
BOOST_AUTO_TEST_SUITE(TC_TR_TRACE_003)

    /**
     *@brief
     *@param component_id
     *@param function_name
     *@param format_spec
    **/
    BOOST_AUTO_TEST_CASE( TC_TR_TRACE_003_001 )
    {
        const char* component_id = "TR";
        const char* function_name = "TC_TR_TRACE_003_001";
        char format_spec[255] = " > function entry";
        BOOST_TEST(TR4A_trace(component_id,function_name,"%s",format_spec) == OK);
    }

	BOOST_AUTO_TEST_CASE( TC_TR_TRACE_003_002 )
    {
        const char* component_id = "TR";
        const char* function_name = "TC_TR_TRACE_003_002";
        char format_spec[255] = " < function exit";
        BOOST_TEST(TR4A_trace(component_id,function_name,"%s",format_spec) == OK);
    }

    
	BOOST_AUTO_TEST_CASE( TC_TR_TRACE_003_003 )
    {
        const char* component_id = "TR";
        const char* function_name = "TC_TR_TRACE_003_003";
        char format_spec[255] = "test";
        for(int i = 0;i < 100; ++i)
        {
        	BOOST_TEST(TR4A_trace(component_id,function_name,"%s [%d]",format_spec,i) == OK);
        }
    }

BOOST_AUTO_TEST_SUITE_END()
