/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TR_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-09    Generator      created
*************************************************************/
extern "C"
{
    #include <TR4A_if.h>
    #include <TR4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_TR_CHECK_SIM_MODE_005)

/**
 *@brief 
 *@param resource_id
 *@param component_id
 *@param sim_mode
**/
    BOOST_AUTO_TEST_CASE( TC_TR_CHECK_SIM_MODE_005_001 )
    {
        const char* component_id = "TR";;
        ZOO_SIM_MODE_ENUM sim_mod = ZOO_SIM_MODE_1;
        BOOST_TEST(TR4A_check_sim_mode(component_id,sim_mod) == ZOO_FALSE);
    }

BOOST_AUTO_TEST_SUITE_END()