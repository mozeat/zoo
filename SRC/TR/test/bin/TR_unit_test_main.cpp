/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TR_unit_test.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-11-09    Generator      created
*************************************************************/
#define BOOST_TEST_MODULE TR_test_module 
#define BOOST_TEST_DYN_LINK 
#include <boost/test/unit_test.hpp>
#include <boost/test/unit_test_log.hpp>
#include <boost/test/unit_test_suite.hpp>
#include <boost/test/framework.hpp>
#include <boost/test/unit_test_parameters.hpp>
#include <boost/test/utils/nullstream.hpp>
/**
 * @brief Execute <TR> UTMF ...
 * @brief <Global Fixture> header file can define the global variable.
 * @brief modify the include header files sequence to change the unit test execution sequence but not case SHUTDOWN.
 */
#include <TC_TR_GLOBAL_FIXTRUE.h>
//#include <TC_TR_SET_MODE_001.h>
#include <TC_TR_GET_MODE_002.h>
#include <TC_TR_TRACE_003.h>
#include <TC_TR_PRINT_LOG_004.h>
#include <TC_TR_CHECK_SIM_MODE_005.h>

/**
 * @brief This case should be executed at the end of all test cases to exit current process.
 * @brief Close all this process events subscribe.
 * @brief Close message queue context thread.
 */
//#include <TC_TR_SHUTDOWN.h>
