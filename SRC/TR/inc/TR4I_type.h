/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : TR4I_type.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-03-29    Generator      created
*************************************************************/
#ifndef TR4I_TYPE_H
#define TR4I_TYPE_H
#include <MQ4A_type.h>
#include "TR4A_type.h"
#include "TR4A_if.h"


/**
 *@brief Macro Definitions
**/
#define TR4I_COMPONET_ID "TR"
#define TR4A_SERVER     "TR4A_SERVER"
#define TR4I_BUFFER_LENGTH    1024
#define TR4I_COMPONET_ID_LENGTH    32
#define TR4I_RESOURCE_ID_LENGTH    32
#define TR4I_THREAD_ID_LENGTH      32
#define TR4I_FUNCTION_NAME_LENGTH  64
#define TR4I_RETRY_INTERVAL   3

/**
 *@brief Function Code Definitions
**/
#define TR4A_SET_MODE_CODE 0x5452ff00
#define TR4A_GET_MODE_CODE 0x5452ff01
#define TR4A_TRACE_CODE 0x5452ff02
#define TR4A_PRINT_LOG_CODE 0x5452ff03
#define TR4A_CHECK_SIM_MODE_CODE 0x5452ff04

/*Request and reply header struct*/
typedef struct
{
    MQ4A_SERV_ADDR reply_addr;
    ZOO_UINT32 msg_id;
    ZOO_BOOL reply_wanted;
    ZOO_UINT32 func_id;
}TR4I_REPLY_HANDLER_STRUCT;

typedef  TR4I_REPLY_HANDLER_STRUCT * TR4I_REPLY_HANDLE;

/*Request message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL need_reply;
}TR4I_REQUEST_HEADER_STRUCT;
                                                                                            
/*Reply message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL execute_result;
}TR4I_REPLY_HEADER_STRUCT;

/**
*@brief TR4I_SET_MODE_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_TRACE_MODE_ENUM trace_mode;
    ZOO_SIM_MODE_ENUM sim_mode;
    ZOO_WATCH_MODE_ENUM watch_mode;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR resource_id[TR4I_RESOURCE_ID_LENGTH];
    ZOO_CHAR component_id[TR4I_COMPONET_ID_LENGTH];
    ZOO_CHAR g_filler[8];
}TR4I_SET_MODE_CODE_REQ_STRUCT;

/**
*@brief TR4I_GET_MODE_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_CHAR resource_id[TR4I_RESOURCE_ID_LENGTH];
    ZOO_CHAR component_id[TR4I_COMPONET_ID_LENGTH];
    ZOO_CHAR g_filler[8];
}TR4I_GET_MODE_CODE_REQ_STRUCT;

/**
*@brief TR4I_TRACE_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_CHAR resource_id[TR4I_RESOURCE_ID_LENGTH];
    ZOO_CHAR component_id[TR4I_COMPONET_ID_LENGTH];
    ZOO_CHAR thread_id[TR4I_THREAD_ID_LENGTH];
    ZOO_CHAR function_name[TR4I_BUFFER_LENGTH];
    ZOO_CHAR format_spec[TR4I_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}TR4I_TRACE_CODE_REQ_STRUCT;

/**
*@brief TR4I_PRINT_LOG_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_SEVERITY_LEVEL_ENUM level;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR resource_id[TR4I_RESOURCE_ID_LENGTH];
    ZOO_CHAR thread_id[TR4I_THREAD_ID_LENGTH];
    ZOO_CHAR format_spec[TR4I_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}TR4I_PRINT_LOG_CODE_REQ_STRUCT;

/**
*@brief TR4I_CHECK_SIM_MODE_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_SIM_MODE_ENUM sim_mode;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR resource_id[TR4I_RESOURCE_ID_LENGTH];
    ZOO_CHAR component_id[TR4I_COMPONET_ID_LENGTH];
    ZOO_CHAR g_filler[8];
}TR4I_CHECK_SIM_MODE_CODE_REQ_STRUCT;

/**
*@brief TR4I_SET_MODE_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}TR4I_SET_MODE_CODE_REP_STRUCT;

/**
*@brief TR4I_GET_MODE_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_SIM_MODE_ENUM sim_mode;
    ZOO_TRACE_MODE_ENUM trace_mode;
    ZOO_WATCH_MODE_ENUM watch_mode;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}TR4I_GET_MODE_CODE_REP_STRUCT;

/**
*@brief TR4I_TRACE_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}TR4I_TRACE_CODE_REP_STRUCT;

/**
*@brief TR4I_PRINT_LOG_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}TR4I_PRINT_LOG_CODE_REP_STRUCT;

/**
*@brief TR4I_CHECK_SIM_MODE_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}TR4I_CHECK_SIM_MODE_CODE_REP_STRUCT;

typedef struct
{
    TR4I_REQUEST_HEADER_STRUCT request_header;
    union
    {
        TR4I_SET_MODE_CODE_REQ_STRUCT set_mode_req_msg;
        TR4I_GET_MODE_CODE_REQ_STRUCT get_mode_req_msg;
        TR4I_TRACE_CODE_REQ_STRUCT trace_req_msg;
        TR4I_PRINT_LOG_CODE_REQ_STRUCT print_log_req_msg;
        TR4I_CHECK_SIM_MODE_CODE_REQ_STRUCT check_sim_mode_req_msg;
     }request_body;
}TR4I_REQUEST_STRUCT;


typedef struct
{
    TR4I_REPLY_HEADER_STRUCT reply_header;
    union
    {
        TR4I_SET_MODE_CODE_REP_STRUCT set_mode_rep_msg;
        TR4I_GET_MODE_CODE_REP_STRUCT get_mode_rep_msg;
        TR4I_TRACE_CODE_REP_STRUCT trace_rep_msg;
        TR4I_PRINT_LOG_CODE_REP_STRUCT print_log_rep_msg;
        TR4I_CHECK_SIM_MODE_CODE_REP_STRUCT check_sim_mode_rep_msg;
    }reply_body;
}TR4I_REPLY_STRUCT;


#endif //TR4I_type.h
