/**************************************************************
 * Copyright (C) 2018, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : TR
 * File Name    : TR4A_type.h
 * Description  : 
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef TR4A_TYPE_H
#define TR4A_TYPE_H
#include <stdio.h>
#include <stdlib.h>
#include <ZOO.h>

/*************************************************************
 @brief 定义TR模块错误码
*************************************************************/
#define TR4A_BASE_ERR               0x54520000
#define TR4A_SYSTEM_ERR   	        (TR4A_BASE_ERR + 0x01)
#define TR4A_PARAMETER_ERR 	        (TR4A_BASE_ERR + 0x02)
#define TR4A_TIMEOUT_ERR  	        (TR4A_BASE_ERR + 0x03)
#define TR4A_ILLEGAL_CALL_ERR       (TR4A_BASE_ERR + 0x04)
#define TR4A_NOT_SIM_MODE_ERR       (TR4A_BASE_ERR + 0x05)

/*************************************************************
 @brief 定义服务端处理的工作线程，最大不超过20个
        该关键字用于刷代码工具
*************************************************************/
#define TR4A_SERVER_WORKERS         (1)
#endif
