/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : TR4A.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-03-28    Generator      created
*************************************************************/

#include <ZOO.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include <TR4A_if.h>
#include <TR4I_type.h>
#include <TR4A_type.h>
#include <TR4I_if.h>
#include <stdarg.h>
#include <ZOO_if.h>

#include <unistd.h>
#include <sys/syscall.h>
#define gettid() syscall(SYS_gettid)

/**
 *@brief TR4A_set_mode
 *@param resource_id
 *@param component_id
 *@param trace_mode
 *@param sim_mode
 *@param watch_mode
**/
ZOO_INT32 TR4A_set_mode(IN const ZOO_CHAR* component_id, 
										IN ZOO_TRACE_MODE_ENUM trace_mode, 
										IN ZOO_SIM_MODE_ENUM sim_mode,
										IN ZOO_WATCH_MODE_ENUM watch_mode)
{
    ZOO_INT32 result = OK;
    TR4I_REQUEST_STRUCT *request_message = NULL; 
    TR4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 func_code = TR4A_SET_MODE_CODE;
    ZOO_CHAR resource_id[ZOO_RESOURCE_ID_LENGHT];
    result = TR4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (TR4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = TR4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = TR4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (TR4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = TR4A_PARAMETER_ERR;
        }
    }

	if(OK == result)
    {
        result = ZOO_get_resource_id(resource_id);
    }
    
    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(request_message->request_body.set_mode_req_msg.resource_id),resource_id,strlen(resource_id) + 1);
        memcpy((void *)(request_message->request_body.set_mode_req_msg.component_id),component_id,strlen(component_id) + 1);
        memcpy((void *)(&request_message->request_body.set_mode_req_msg.trace_mode),&trace_mode,sizeof(ZOO_TRACE_MODE_ENUM));
        memcpy((void *)(&request_message->request_body.set_mode_req_msg.sim_mode),&sim_mode,sizeof(ZOO_SIM_MODE_ENUM));
        memcpy((void *)(&request_message->request_body.set_mode_req_msg.watch_mode),&watch_mode,sizeof(ZOO_WATCH_MODE_ENUM));
    }

    if(OK == result)
    {
        result = TR4I_send_request_and_reply(TR4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }
    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief TR4A_get_mode
 *@param resource_id
 *@param component_id
 *@param sim_mode
 *@param trace_mode
 *@param watch_mode
**/
ZOO_INT32 TR4A_get_mode(IN  const ZOO_CHAR* component_id, 
										OUT ZOO_SIM_MODE_ENUM* sim_mode, 
										OUT ZOO_TRACE_MODE_ENUM* trace_mode,
										OUT ZOO_WATCH_MODE_ENUM* watch_mode)
{
    ZOO_INT32 result = OK;
    TR4I_REQUEST_STRUCT *request_message = NULL; 
    TR4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 func_code = TR4A_GET_MODE_CODE;
    ZOO_CHAR resource_id[ZOO_RESOURCE_ID_LENGHT];
    result = TR4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (TR4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = TR4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = TR4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (TR4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = TR4A_PARAMETER_ERR;
        }
    }
    
	if(OK == result)
	{
		result = ZOO_get_resource_id(resource_id);
	}

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(request_message->request_body.get_mode_req_msg.resource_id),resource_id,strlen(resource_id)+1);
        memcpy((void *)(request_message->request_body.get_mode_req_msg.component_id),component_id,strlen(component_id)+1);
    }

    if(OK == result)
    {
        result = TR4I_send_request_and_reply(TR4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(sim_mode,&reply_message->reply_body.get_mode_rep_msg.sim_mode,sizeof(ZOO_SIM_MODE_ENUM));
        memcpy(trace_mode,&reply_message->reply_body.get_mode_rep_msg.trace_mode,sizeof(ZOO_TRACE_MODE_ENUM));
        memcpy(watch_mode,&reply_message->reply_body.get_mode_rep_msg.watch_mode,sizeof(ZOO_WATCH_MODE_ENUM));
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }
    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief TR4A_trace
 *@param resource_id
 *@param component_id
 *@param function_name
 *@param format_spec
**/
ZOO_INT32 TR4A_trace(IN const ZOO_CHAR* component_id,
									IN const ZOO_CHAR* function_name,
									IN const ZOO_CHAR* format_spec,...)
{
    ZOO_INT32 result = OK;
    TR4I_REQUEST_STRUCT *request_message = NULL; 
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = TR4A_TRACE_CODE;
	char *buffer = NULL;
    ZOO_CHAR resource_id[ZOO_RESOURCE_ID_LENGHT];
    ZOO_CHAR thread_id[TR4I_THREAD_ID_LENGTH];
    sprintf(thread_id, "%ld", gettid());
	if(component_id == NULL|| function_name == NULL || format_spec == NULL)
	{
	    result = TR4A_PARAMETER_ERR;
	}
	
	if(OK == result)
	{
		buffer = MM4A_malloc(TR4I_BUFFER_LENGTH);
		if(buffer == NULL)
		{
			result = TR4A_SYSTEM_ERR;
		}
	}
	
	if(OK == result)
    {
        request_message = (TR4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
        if(request_message == NULL)
        {
            result = TR4A_PARAMETER_ERR;
        }
    }

	if(OK == result)
    {
        va_list args;
    	va_start(args, format_spec);
    	vsprintf(buffer, format_spec, args);
    	va_end(args);
		buffer[TR4I_BUFFER_LENGTH - 1] = '\0';
	}
    
	if(OK == result)
    {
        result = ZOO_get_resource_id(resource_id);
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_FALSE;
        memcpy((void *)(request_message->request_body.trace_req_msg.resource_id),
        			resource_id,strlen(resource_id) + 1);
        memcpy((void *)(request_message->request_body.trace_req_msg.component_id),
        			component_id,strlen(component_id) + 1);
        memcpy((void *)(request_message->request_body.trace_req_msg.function_name),
        			function_name,strlen(function_name) + 1);
        memcpy((void *)(request_message->request_body.trace_req_msg.format_spec),
        			buffer,sizeof(ZOO_CHAR) * TR4I_BUFFER_LENGTH);
        memcpy((void *)(request_message->request_body.trace_req_msg.thread_id),
        			thread_id,sizeof(ZOO_CHAR) * TR4I_THREAD_ID_LENGTH);
    }

    if(OK == result)
    {
        result = TR4I_send_signal_message(TR4A_SERVER,request_message);
	}
	
	if(request_message != NULL)
	{
        MM4A_free(request_message);
    }
    
    if(buffer != NULL)
    {
    	MM4A_free(buffer);
   	}
    return result;
}
/**
 *@brief TR4A_print_log
 *@param resource_id
 *@param level
 *@param format_spec
**/
ZOO_INT32 TR4A_log(IN ZOO_SEVERITY_LEVEL_ENUM level,
									IN const ZOO_CHAR* format_spec,...)
{
    //TR4A_trace(TR4I_COMPONET_ID, __ZOO_FUNC__, "> function entry ... ");
    ZOO_INT32 result = OK;
    TR4I_REQUEST_STRUCT *request_message = NULL; 
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = TR4A_PRINT_LOG_CODE;
    ZOO_CHAR resource_id[ZOO_RESOURCE_ID_LENGHT];
    ZOO_CHAR thread_id[TR4I_THREAD_ID_LENGTH];
    char *buffer = NULL;
    sprintf(thread_id, "%ld", gettid());
	if(format_spec == NULL)
	{
	    result = TR4A_PARAMETER_ERR;
	}
	
	if(OK == result)
	{
		buffer = MM4A_malloc(TR4I_BUFFER_LENGTH);
		if(buffer == NULL)
		{
			result = TR4A_SYSTEM_ERR;
		}
	}
	
	if(OK == result)
    {
        result = ZOO_get_resource_id(resource_id);
    }
	
	if(OK == result)
    {
        va_list args;
    	va_start(args, format_spec);
    	vsprintf(buffer, format_spec, args);
    	va_end(args);
		buffer[TR4I_BUFFER_LENGTH - 1] = '\0';
	}
	
    if(OK == result)
    {
        result = TR4I_get_request_message_length(func_code, &request_length);
    }
    
    if(OK == result)
    {
        request_message = (TR4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
        if(OK == result)
	    {
	        if(request_message == NULL)
	        {
	            result = TR4A_PARAMETER_ERR;
	        }
	    }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_FALSE;
        memcpy((void *)(request_message->request_body.print_log_req_msg.resource_id),resource_id,strlen(resource_id) + 1);
        memcpy((void *)(&request_message->request_body.print_log_req_msg.level),&level,sizeof(ZOO_SEVERITY_LEVEL_ENUM));
        memcpy((void *)(request_message->request_body.print_log_req_msg.format_spec),buffer,sizeof(ZOO_CHAR) * TR4I_BUFFER_LENGTH);
        memcpy((void *)(request_message->request_body.print_log_req_msg.thread_id),
        			thread_id,sizeof(ZOO_CHAR) * TR4I_THREAD_ID_LENGTH);
    }

    if(OK == result)
    {
        result = TR4I_send_signal_message(TR4A_SERVER,request_message);
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }
    
	if(NULL != buffer) 
	{
        MM4A_free(buffer);
    }
    return result;
}

/**
 *@brief TR4A_check_sim_mode
 *@param resource_id
 *@param component_id
 *@param sim_mode
**/
ZOO_BOOL TR4A_check_sim_mode(IN const ZOO_CHAR* component_id,
										        IN ZOO_SIM_MODE_ENUM sim_mode)
{
    //TR4A_trace(TR4I_COMPONET_ID, __ZOO_FUNC__, "> function entry ... ");
    ZOO_INT32 result = OK;
    TR4I_REQUEST_STRUCT *request_message = NULL; 
    TR4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 func_code = TR4A_CHECK_SIM_MODE_CODE;
    ZOO_CHAR resource_id[ZOO_RESOURCE_ID_LENGHT];
    result = TR4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (TR4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(component_id == NULL)
	{
	    result = TR4A_PARAMETER_ERR;
	}

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = TR4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = TR4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (TR4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = TR4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = ZOO_get_resource_id(resource_id);
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(request_message->request_body.check_sim_mode_req_msg.resource_id),resource_id,strlen(resource_id) + 1);
        memcpy((void *)(request_message->request_body.check_sim_mode_req_msg.component_id),component_id,strlen(component_id) + 1);
        memcpy((void *)(&request_message->request_body.check_sim_mode_req_msg.sim_mode),&sim_mode,sizeof(ZOO_SIM_MODE_ENUM));
    }

    if(OK == result)
    {
        result = TR4I_send_request_and_reply(TR4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }
    
    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    //TR4A_trace(TR4I_COMPONET_ID, __ZOO_FUNC__, "< function exit ..." );
    return result;
}

