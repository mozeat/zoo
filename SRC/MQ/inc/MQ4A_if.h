 /*******************************************************************************
* Copyright (C) 2018, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: MQ
* Component id: MQ
* File name: MQ4A_if.h
* Description: interface for message queue
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
* 1.1       2020-03-21     weiwang.sun       新增signal接口
******************************************************************************/
#ifndef MQ4A_IF_H
#define MQ4A_IF_H
#include <ZOO.h>
#include <MQ4A_type.h>

/**************************************************************************
INTERFACE <ZOO_INT32 MQ4A_server_initialize>
{
<InterfaceType>:FUNCTION<NonBlocking>
<Parameters>
    IN:         MQ4A_SERV_ADDR server --服务端地址
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正确
                MQ4A_SYSTEM_ERR -- 系统错误
                MQ4A_TIMEOUT_ERR -- 超时错误
                MQ4A_PARAMETER_ERR -- 参数错误
<Description>:  初始化服务端，绑定端口及地址。通过zoo.json文件，配置服务端工作线程数
    PRECONDITION:
    POSTCONDITION:
**************************************************************************/
ZOO_EXPORT ZOO_INT32 MQ4A_server_initialize(IN const MQ4A_SERV_ADDR server);

/**************************************************************************
INTERFACE <ZOO_INT32 MQ4A_server_terminate>
{
<InterfaceType>:FUNCTION<NonBlocking>
<Parameters>
    IN:         MQ4A_SERV_ADDR server --服务端地址
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正确
                MQ4A_SYSTEM_ERR    -- 系统错误
                MQ4A_TIMEOUT_ERR   -- 超时错误
                MQ4A_PARAMETER_ERR -- 参数错误
<Description>:  终止服务端收发消息
                PRECONDITION:   服务端已经初始化
                POSTCONDITION:
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 MQ4A_server_terminate(IN const MQ4A_SERV_ADDR server);

/**************************************************************************
INTERFACE <ZOO_INT32 MQ4A_server_publish>
{
<InterfaceType>:FUNCTION<NonBlocking>
<Parameters>
    IN:         const MQ4A_SERV_ADDR server -- 客户端接收地址
                ZOO_INT32 event_id -- 事件号
                void *req_msg --  消息
                int req_len -- 消息长度
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正常
                MQ4A_SYSTEM_ERR -- 系统错误
                MQ4A_TIMEOUT_ERR -- 超时错误
                MQ4A_PARAMETER_ERR -- 参数错误
<Description>:  服务端发布广播消息，订阅的客户端能够收到消息
                PRECONDITION:  服务端初始化完成
                POSTCONDITION: 客户端订阅消息
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 MQ4A_publish(IN const MQ4A_SERV_ADDR server,
		                                         IN ZOO_INT32 event_id,
		                                         IN void *req_msg,
		                                         IN int req_len);

/**************************************************************************
INTERFACE <ZOO_INT32 MQ4A_send_request>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking>
<Parameters>
    IN:         const MQ4A_SERV_ADDR server -- 服务端地址
                void *req_meg               -- 消息
                ZOO_INT32 req_len           -- 消息长度
                ZOO_INT32 retry_interval    -- 消息重发次数
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正常
                MQ4A_SYSTEM_ERR -- 系统错误
                MQ4A_TIMEOUT_ERR -- 超时错误
                MQ4A_PARAMETER_ERR -- 参数错误
<Description>:  客户端向服务端发送异步消息请求，一般需配合MQ4A_receive_reply一起使用
        PRECONDITION:服务端初始化完成，客户端初始化完成
        POSTCONDITION:
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 MQ4A_send_request(IN const MQ4A_SERV_ADDR server,
                                                        IN void *req_msg,
                                                        IN ZOO_INT32 req_len,
                                                        IN ZOO_INT32 retry_interval);

/**************************************************************************
INTERFACE <ZOO_INT32 MQ4A_send_signal>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking>
<Parameters>
    IN:         const MQ4A_SERV_ADDR server -- 服务端地址
                void *req_meg               -- 消息
                ZOO_INT32 req_len           -- 消息长度
                ZOO_INT32 retry_interval    -- 消息重发次数
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正常
                MQ4A_SYSTEM_ERR -- 系统错误
                MQ4A_TIMEOUT_ERR -- 超时错误
                MQ4A_PARAMETER_ERR -- 参数错误
<Description>:  客户端向服务端发送异步消息请求，单向发送，无需服务端应答
        PRECONDITION:服务端初始化完成，客户端初始化完成
        POSTCONDITION:
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 MQ4A_send_signal(IN const MQ4A_SERV_ADDR server,
                                                        IN void *req_msg,
                                                        IN ZOO_INT32 req_len,
                                                        IN ZOO_INT32 retry_interval);

/**************************************************************************
INTERFACE <ZOO_INT32 MQ4A_send_reply>
{
<InterfaceType>:FUNCTION<NonBlocking>
<Parameters>
    IN:         const MQ4A_SERV_ADDR server -- 客户端地址
    			ZOO_INT32 msg_id            -- 消息号
                void *req_meg               -- 请求消息           
                ZOO_INT32 req_len           -- 消息长度
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正确
                MQ4A_SYSTEM_ERR -- 系统错误
                MQ4A_TIMEOUT_ERR -- 超时错误
                MQ4A_PARAMETER_ERR -- 参数错误
<Description>:  服务端发送应答消息给客户端
        PRECONDITION:
        1.服务端初始化完成
        2.客户端初始化完成
        3.客户端调用了MQ4A_send_request
        POSTCONDITION:
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 MQ4A_send_reply(IN const MQ4A_SERV_ADDR server,
												IN ZOO_UINT32 msg_id,
                                                IN void *rep_msg,
                                                IN ZOO_INT32 rep_len);
                                                
/**************************************************************************
INTERFACE <ZOO_INT32 MQ4A_send_request_and_get_reply>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking>
<Parameters>
    IN:         const MQ4A_SERV_ADDR server --服务端地址
                void *req_meg               --请求消息 
                ZOO_INT32 req_len           --请求消息长度
                void *rep_msg               --应答消息    
                ZOO_INT32 rep_len           --应答消息长度
                ZOO_INT32 *act_rep_len      --实际应答消息长度
                ZOO_INT32 retry_interval    --重试次数
                ZOO_INT32 timeout           --超时时间（毫秒）
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正确
                MQ4A_SYSTEM_ERR -- 系统错误
                MQ4A_TIMEOUT_ERR -- 超时错误
                MQ4A_PARAMETER_ERR -- 参数错误
<Description>:  客户端向服务端发送消息请求并接收应答消息
        PRECONDITION:服务端初始化完成，客户端初始化完成
        POSTCONDITION:
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 MQ4A_send_request_and_receive_reply(IN const MQ4A_SERV_ADDR server,
			                                                				IN void *req_msg,
			                                                				IN ZOO_INT32 req_len,
					                                                		IN void *rep_msg,
					                                                		IN ZOO_INT32 rep_len,
					                                                		IN ZOO_INT32 *act_rep_len,
			                                                				IN ZOO_INT32 retry_interval,
					                                                		IN ZOO_INT32 timeout);
                                                

/**************************************************************************
INTERFACE <ZOO_INT32 MQ4A_receive_reply>
{
<InterfaceType>:FUNCTION<NonBlocking>
<Parameters>
    IN:         const MQ4A_SERV_ADDR server --
                void *req_meg -- 
                ZOO_INT32 req_len -- 
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正确
                MQ4A_SYSTEM_ERR -- 系统错误
                MQ4A_TIMEOUT_ERR -- 超时错误
                MQ4A_PARAMETER_ERR -- 参数错误
<Description>:  客户端获取回答
        PRECONDITION:
        POSTCONDITION:
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 MQ4A_receive_reply(IN const MQ4A_SERV_ADDR server,
                                                IN void *rep_msg,
                                                IN ZOO_INT32 rep_len,
                                                IN ZOO_INT32 *act_rep_len,
                                                IN ZOO_INT32 timeout);

/**************************************************************************
INTERFACE <ZOO_INT32 MQ4A_subscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         const MQ4A_SERV_ADDR server
                callback_function -- the callback function
                callback_struct   -- the callback function arguments
                context           -- the identity pointer      
    OUT:        ZOO_HANDLE *handle --
    INOUT:      void* context
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正确
                MQ4A_SYSTEM_ERR -- 系统错误
                MQ4A_TIMEOUT_ERR -- 超时错误
                MQ4A_PARAMETER_ERR -- 参数错误
<Description>:  客户端订阅接口
                PRECONDITION:
                POSTCONDITION:
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 MQ4A_subscribe(IN const MQ4A_SERV_ADDR server,
                                             IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
                                             IN MQ4A_CALLBACK_STRUCT * callback_struct,
                                             IN ZOO_INT32 event_id,
                                             INOUT ZOO_HANDLE *handle,
                                             INOUT void* context);

/**************************************************************************
INTERFACE <ZOO_INT32 MQ4A_unsubscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         const MQ4A_SERV_ADDR server
                handle --
    OUT:        none
    INOUT:      void* context
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正确
                MQ4A_SYSTEM_ERR -- 系统错误
                MQ4A_TIMEOUT_ERR -- 超时错误
                MQ4A_PARAMETER_ERR -- 参数错误
<Description>:  客户端取消订阅事件
                PRECONDITION:
                POSTCONDITION:
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 MQ4A_unsubscribe(IN const MQ4A_SERV_ADDR server,
												IN ZOO_INT32 event_id,
												IN ZOO_HANDLE handle);

/**************************************************************************
INTERFACE <ZOO_INT32 MQ4A_register_event_handler>
{
<InterfaceType>:FUNCTION<NonBlocking>
<Parameters>
    IN:         const MQ4A_SERV_ADDR server 
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正确
                MQ4A_SYSTEM_ERR -- 系统错误
                MQ4A_TIMEOUT_ERR -- 超时错误
                MQ4A_PARAMETER_ERR -- 参数错误
<Description>:  服务端注册处理事件回调函数
                PRECONDITION: 服务端初始化完成
                POSTCONDITION:
**************************************************************************/
ZOO_EXPORT ZOO_INT32 MQ4A_register_event_handler(IN const MQ4A_SERV_ADDR server,
															IN MQ4A_BIND_CALLBACK_FUNCTION callback_function);


/**************************************************************************
INTERFACE <ZOO_INT32 MQ4A_enter_event_loop>
{
<InterfaceType>:FUNCTION<Blocking>
<Parameters>
    IN:         const MQ4A_SERV_ADDR server 
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正确
                MQ4A_SYSTEM_ERR -- 系统错误
                MQ4A_TIMEOUT_ERR -- 超时错误
                MQ4A_PARAMETER_ERR -- 参数错误
<Description>:  服务端执行事件监听
                PRECONDITION: 服务端初始化完成
                POSTCONDITION: 进入事件监听
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 MQ4A_enter_event_loop( IN const MQ4A_SERV_ADDR server);

/**************************************************************************
INTERFACE <ZOO_INT32 MQ4A_shutdown_all_clients>
{
<InterfaceType>:FUNCTION<NonBlocking>
<Parameters>
    IN:         none
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正确
                MQ4A_SYSTEM_ERR -- 系统错误
                MQ4A_TIMEOUT_ERR -- 超时错误
                MQ4A_PARAMETER_ERR -- 参数错误
<Description>:  关闭客户端所有背景线程，例如订阅背景线程
                PRECONDITION: 
                POSTCONDITION:
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 MQ4A_shutdown_all_clients();															
#endif
