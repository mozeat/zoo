/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: MQ4A_type.h
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef MQ4A_TYPE_H
#define MQ4A_TYPE_H
#include <ZOO.h>

/*
* Component Id Definition
*/
#ifndef COMPONENT_ID_MQ
#define COMPONENT_ID_MQ "MQ"
#endif

#define MQ4A_SERVER_LENGHT 32
#define MQ4A_SERVER_THREAD_NUMBER 20
#define MQ4A_MESSAGE_LENGHT 32


/*
* Error Code Definition
*/
#define MQ4A_BASE_ERR               (0x4D510000)
#define MQ4A_SYSTEM_ERR             ((MQ4A_BASE_ERR) + 0x01)
#define MQ4A_TIMEOUT_ERR            ((MQ4A_BASE_ERR) + 0x02)
#define MQ4A_PARAMETER_ERR          ((MQ4A_BASE_ERR) + 0x03)
#define MQ4A_CONNECT_SERVER_ERR     ((MQ4A_BASE_ERR) + 0x04)
#define MQ4A_SERVER_NO_RESPONSE_ERR ((MQ4A_BASE_ERR) + 0x05)
#define MQ4A_SERVER_LOST_ERR        ((MQ4A_BASE_ERR) + 0x06)
#define MQ4A_SEND_ERR        		((MQ4A_BASE_ERR) + 0x07)

/*****************************************************************************
*@brief server type definition
*****************************************************************************/
typedef char MQ4A_SERV_ADDR[MQ4A_SERVER_LENGHT];

/*
*@brief MQ4A_CALLBACK_STRUCT
*@member:void *callback_function 
*@member:void *parameter --
*@description:
*/
typedef struct 
{
    void *callback_function;
	void *parameter;
}MQ4A_CALLBACK_STRUCT;

/******************************************************************
*@brief MQ4A_EVENT_CALLBACK_FUNCTION
*@param : void* context --上下文参数
*@param : MQ4A_CALLBACK_STRUCT *callback_struct --回调函数结构体
*@param : void *msg --消息
*@description:
*****************************************************************/
typedef void(*MQ4A_EVENT_CALLBACK_FUNCTION)(void* context, MQ4A_CALLBACK_STRUCT *callback_struct,void *msg);  

/******************************************************************
*@brief MQ4A_BIND_CALLBACK_FUNCTION_ASYNC
*@param : void* context --上下文参数
*@param : const MQ4A_SERV_ADDR server 
*@param : void *req_msg 
*@param : IN ZOO_INT32 req_msg_len 
*@param : void * rep_msg 
*@param : IN ZOO_INT32  *rep_msg_len 
*@description:
*****************************************************************/
typedef void(*MQ4A_BIND_CALLBACK_FUNCTION)(void * context,const MQ4A_SERV_ADDR server,void * req_msg,IN ZOO_UINT32 msg_id);

/*****************************************************************************
* Enum Type Definitions
*****************************************************************************/
/*MESSAGE TYPE define*/
typedef enum
{
    MQ4A_TYPE_MIN = 0,
    MQ4A_TYPE_IPC,//ipc mode
    MQ4A_TYPE_TCP,//tcp mode
    MQ4A_TYPE_INTER_IPC,// message transmits in thread
    MQ4A_TYPE_MAX
}MQ4A_TYPE_ENUM;

/*SERVER MODE define*/
typedef enum
{
    MQ4A_SERVER_MODE_MIN = 0,
    MQ4A_SERVER_MODE_SYNC,//synchronization mode
    MQ4A_SERVER_MODE_ASYNC,//asynchronization mode,multithread
    MQ4A_SERVER_MODE_MAX
}MQ4A_SERVER_MODE_ENUM;
    
/*socket type*/
typedef enum
{
    MQ4A_SOCKET_TYPE_MIN = 0,
    MQ4A_SOCKET_TYPE_PAIR = 0,
    MQ4A_SOCKET_TYPE_PUB  = 1,
    MQ4A_SOCKET_TYPE_SUB,
    MQ4A_SOCKET_TYPE_REQ,    
    MQ4A_SOCKET_TYPE_REP,
    MQ4A_SOCKET_TYPE_DEALER,
    MQ4A_SOCKET_TYPE_ROUTER,
    MQ4A_SOCKET_TYPE_PULL,
    MQ4A_SOCKET_TYPE_PUSH,
    MQ4A_SOCKET_TYPE_XPUB,
    MQ4A_SOCKET_TYPE_XSUB,
    MQ4A_SOCKET_TYPE_STREAM,
    MQ4A_SOCKET_TYPE_MAX
}MQ4A_SOCKET_TYPE_ENUM;

/* 超时枚举定义*/
typedef enum
{
    MQ4A_TIMEOUT_MIN = 0,
    MQ4A_TIMEOUT_INFINITE = -1,//ipc mode
    MQ4A_TIMEOUT_60_SECONDS = 60,//ipc mode
    MQ4A_TIMEOUT_MAX
}MQ4A_TIMEOUT_ENUM;

typedef enum 
{ 
    MQ4A_MSG_EVENT_REQUESTED = 0,
    MQ4A_MSG_EVENT_REPLIED   = 1,
    MQ4A_MSG_EVENT_SUBSCRIPTION = 2,
    MQ4A_MSG_EVENT_CONSUMING = 3,
    MQ4A_MSG_EVENT_CONSUMED  = 4,
    MQ4A_MSG_EVENT_ABANDONED  = 5,
}MQ4A_MSG_EVENT_ENUM;
   

/* Workers 结构体定义*/
typedef struct
{
    ZOO_INT32 id;
    MQ4A_BIND_CALLBACK_FUNCTION handle;
}MQ4A_WORKER_STRUCT;

#endif
