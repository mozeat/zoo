/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: CLIENT_FACADE_FLOW_CLASS.h
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#include "CLIENT_FACADE_FLOW_CLASS.h"
namespace ZOO_MQ
{
	CLIENT_FACADE_FLOW_CLASS::CLIENT_FACADE_FLOW_CLASS()
	{
		boost::shared_ptr<CLIENT_CONTROLLER_CLASS> controller;
		controller.reset(new CLIENT_CONTROLLER_CLASS);
		this->set_client_controller(controller);
	}

    /**
     * @brief disconstructor
    */
    CLIENT_FACADE_FLOW_CLASS::~CLIENT_FACADE_FLOW_CLASS()
	{
		
	}

	
	/**
     * @brief 客户端发送异步请求
     * @param server 服务器地址
     * @param req_msg 请求消息
     * @param rep_len 请求消息长度
     * @param retry_interval 重试次数
     * @return 错误码
    */									
    ZOO_INT32 CLIENT_FACADE_FLOW_CLASS::send_request(IN const std::string & server,
                                        IN void *req_msg,
                                        IN ZOO_INT32 req_len,
                                        IN ZOO_INT32 retry_interval,
                                        IN ZOO_BOOL signal)
	{
		__MQ_TRY
		{
			this->m_client_controller->send_request(server,req_msg,req_len,retry_interval,signal);
		}
		__MQ_CATCH
		{
			return ex.get_error_code();
		}
		return OK;
	}

	/**
     * @brief 客户端获取回答
     * @param server 服务器地址
     * @param req_msg 请求消息
     * @param rep_len 请求消息长度
     * @param act_rep_len 实际收到的字节长度
     * @param timeout  超时时间
     * @return 错误码
    */	
    ZOO_INT32 CLIENT_FACADE_FLOW_CLASS::get_reply(IN const std::string & server,
                                        IN void *rep_msg,
                                        IN ZOO_INT32 rep_len,
                                        IN ZOO_INT32 *act_rep_len,
                                        IN ZOO_INT32 timeout)
	{
		__MQ_TRY
		{
			this->m_client_controller->get_reply(server,
													rep_msg,
													rep_len,
													act_rep_len,
													timeout);
		}
		__MQ_CATCH
		{
			return ex.get_error_code();
		}
		return OK;
	}

	/**
	 * @brief 客户端发送请求,客户端获取回答
	 * @param server 服务器地址
	 * @param req_msg 请求消息
	 * @param rep_len 请求消息长度
	 * @param act_rep_len 实际收到的字节长度
	 * @param timeout  超时时间
	 * @return 错误码
	*/	
	ZOO_INT32 CLIENT_FACADE_FLOW_CLASS::send_and_reply(IN const MQ4A_SERV_ADDR server,
	                                    		IN void *req_msg,
	                                    		IN ZOO_INT32 req_len,
	                                            IN void *rep_msg,
	                                            IN ZOO_INT32 rep_len,
	                                            IN ZOO_INT32 *act_rep_len,
	                                    		IN ZOO_INT32 retry_interval,
	                                            IN ZOO_INT32 timeout)
	{
		__MQ_TRY
		{
			this->m_client_controller->send_and_reply(server,req_msg,
													req_len,rep_msg,rep_len,
													act_rep_len,retry_interval,
													timeout);
		}
		__MQ_CATCH
		{
			return ex.get_error_code();
		}
		return OK;
	}                                            
    /**
     * @brief 客户端订阅接口
     * @param server 服务器地址
     * @param callback_function 注册消息回调
     * @param callback_struct   请求消息长度
     * @param event_id          事件ID
     * @param handle            注册事件返回唯一ID 
     * @param context           上下文传递参数（一般NULL）
     * @return 错误码
    */
    ZOO_INT32 CLIENT_FACADE_FLOW_CLASS::subscribe(IN const std::string & server,
                                            IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
                                            IN MQ4A_CALLBACK_STRUCT * callback_struct,
                                            IN ZOO_INT32 event_id,
                                            INOUT ZOO_HANDLE *handle,
                                            INOUT void* context)
	{
		__MQ_TRY
		{
			this->m_client_controller->subscribe(server,
														callback_function,
														callback_struct,
														event_id,
														handle,
														context);
		}
		__MQ_CATCH
		{
			return ex.get_error_code();
		}
		return OK;
	}
											
    /**
     * @brief 客户端取消订阅
     * @param server 服务器地址
     * @param event_id          事件ID
     * @param handle            注册事件返回唯一ID 
     * @return 错误码
    */
    ZOO_INT32 CLIENT_FACADE_FLOW_CLASS::unsubscribe(IN const std::string & server,
    							IN ZOO_INT32 event_id,
    							IN ZOO_HANDLE handle)
	{
		__MQ_TRY
		{
			this->m_client_controller->unsubscribe(server,event_id,handle);
		}
		__MQ_CATCH
		{
			return ex.get_error_code();
		}
		return OK;
	}

    /**
     *@brief 关闭客户端所有背景线程，例如订阅背景线程
    */
    ZOO_INT32 CLIENT_FACADE_FLOW_CLASS::shutdown_all_clients()
	{
		__MQ_TRY
		{
			this->m_client_controller->shutdown_all();
		}
		__MQ_CATCH
		{
			return ex.get_error_code();
		}
		return OK;
	}
}
