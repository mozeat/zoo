/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: SERVER_CONTROLLER_CLASS.cpp
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2017-12-21     weiwang.sun       created
******************************************************************************/
#include "SERVER_CONTROLLER_CLASS.h"
namespace ZOO_MQ
{
    SERVER_CONTROLLER_CLASS::SERVER_CONTROLLER_CLASS()
    {
    
    }
    
    SERVER_CONTROLLER_CLASS::~SERVER_CONTROLLER_CLASS()
    {
    
    }

    /*
     * @brief add_model
     * @param:std::string address
     * @description: add client model by server address
    */
    void SERVER_CONTROLLER_CLASS::add_model(IN boost::shared_ptr<SERVER_MODEL_CLASS> server_model)
    {
        if(server_model != NULL)
            this->m_server_map[server_model->get_marking_code()] = server_model;            
    }

    /*
     * @brief remove_model
     * @param:std::string address
     * @description: delete client model by server address
    */
    void SERVER_CONTROLLER_CLASS::remove_model(IN boost::shared_ptr<SERVER_MODEL_CLASS> server_model)
    {
        std::map<std::string,boost::shared_ptr<SERVER_MODEL_CLASS> >::iterator  found = 
                                this->m_server_map.find(server_model->get_marking_code());
        std::map<std::string,boost::shared_ptr<SERVER_MODEL_CLASS> >::iterator end =
                                                this->m_server_map.end();
        if(found != end)
        {
            this->m_server_map.erase(found);
        }
    }

    /*
     * @brief initialize
     * @param server_name -process name
     * @param type - message queue type : tcp/ipc/intipc 
     * @description : enable server service
    */
    void SERVER_CONTROLLER_CLASS::initialize(std::string server)
    {
        std::map<std::string,boost::shared_ptr<SERVER_MODEL_CLASS> >::iterator  found = 
                                this->m_server_map.find(server);
        boost::shared_ptr<SERVER_MODEL_CLASS> model = NULL;
        if(found == this->m_server_map.end())
        {
            model =  boost::make_shared<SERVER_MODEL_CLASS>(server);
            this->add_model(model);
        }
        else
        {
            model = this->m_server_map[server];
        }
        model->initialize();
    } 

    /*
     * @brief terminate
     * @param server_name -process name
     * @description : destory bind
    */
    void SERVER_CONTROLLER_CLASS::terminate(std::string server)
    {
        std::map<std::string,boost::shared_ptr<SERVER_MODEL_CLASS> >::iterator  found = 
                                this->m_server_map.find(server);
        boost::shared_ptr<SERVER_MODEL_CLASS> model = NULL;
        if(found == this->m_server_map.end())
        {
            model =  boost::make_shared<SERVER_MODEL_CLASS>(server);
            this->add_model(model);
        }
        else
        {
            model = this->m_server_map[server];
        }
        model->terminate();
    }    

    /*
     * @brief publish
     * @param server_name -
     * @param event_id -
     * @param request_message - 
     * @param request_message_lenght
     * @description : destory bind
    */                    
    void SERVER_CONTROLLER_CLASS::publish(std::string server,IN ZOO_INT32 event_id,
                                        IN void *request_message,
                                        IN ZOO_INT32 request_message_lenght)
    {
        std::map<std::string,boost::shared_ptr<SERVER_MODEL_CLASS> >::iterator  found = 
                                this->m_server_map.find(server);
        boost::shared_ptr<SERVER_MODEL_CLASS> model = NULL;
        if(found == this->m_server_map.end())
        {
            model =  boost::make_shared<SERVER_MODEL_CLASS>(server);
            this->add_model(model);
        }
        else
        {
            model = this->m_server_map[server];
        }
        model->publish(event_id,request_message,request_message_lenght);
    }

    /*
     * @brief send_reply
     * @param server_addr -process name
     * @description : reply message to client
    */
    void SERVER_CONTROLLER_CLASS::send_reply(std::string server,IN ZOO_UINT32 msg_id,IN void *rep_msg,
                                        IN ZOO_INT32 rep_len)
    {
        std::map<std::string,boost::shared_ptr<SERVER_MODEL_CLASS> >::iterator  found = 
                                this->m_server_map.find(server);
        boost::shared_ptr<SERVER_MODEL_CLASS> model = NULL;
        if(found == this->m_server_map.end())
        {
            model =  boost::make_shared<SERVER_MODEL_CLASS>(server);
            this->add_model(model);
        } 
        else
        {
            model = this->m_server_map[server];
        }
        model->send_reply(msg_id,rep_msg,rep_len);
    }     
    
    /*
     * @brief register_events
     * @param callback -
     * @param timeout -
     * @param request_message_lenght - 
     * @description : destory bind
    */                    
    void SERVER_CONTROLLER_CLASS::register_events_handler(std::string server,IN MQ4A_BIND_CALLBACK_FUNCTION callback_function)
    {
        std::map<std::string,boost::shared_ptr<SERVER_MODEL_CLASS> >::iterator  found = 
                                this->m_server_map.find(server);
        boost::shared_ptr<SERVER_MODEL_CLASS> model = NULL;
        if(found == this->m_server_map.end())
        {
            model =  boost::make_shared<SERVER_MODEL_CLASS>(server);
            this->add_model(model);
        } 
        else
        {
            model = this->m_server_map[server];
        }
        model->register_events_handler(callback_function);
        return ;
    }
    
    /*
     * @brief process_events 
     * @description : enter eventloop and process 
    */  
    void SERVER_CONTROLLER_CLASS::loop_event(std::string server)
    {
        std::map<std::string,boost::shared_ptr<SERVER_MODEL_CLASS> >::iterator  found = 
                                this->m_server_map.find(server);
        boost::shared_ptr<SERVER_MODEL_CLASS> model = NULL;
        if(found == this->m_server_map.end())
        {
            model =  boost::make_shared<SERVER_MODEL_CLASS>(server);
            this->add_model(model);
        } 
        else
        {
            model = this->m_server_map[server];
        }
        model->loop_event();
    }
}
