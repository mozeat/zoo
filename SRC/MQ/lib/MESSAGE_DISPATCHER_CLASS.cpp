/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: MESSAGE_DISPATCHER_CLASS.cpp
* Description: 
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     weiwang.sun       		created
******************************************************************************/
#include "MESSAGE_DISPATCHER_CLASS.h"
#include <iostream>
namespace ZOO_MQ
{
    /*
     * @brief constructor
    */
    MESSAGE_DISPATCHER_CLASS::MESSAGE_DISPATCHER_CLASS(ZOO_INT32 capacity, 
    ZOO_BOOL background,boost::shared_ptr<MQ_THREAD_POOL> thread_pool):m_thread_pool(thread_pool)
    {
        this->m_wait_handle.reset(new MQ_AUTO_RESET_EVENT_CLASS(ZOO_FALSE));
        this->m_thread_pool = thread_pool;
        this->initialize(capacity,background);
    }

    /*
     * @brief disconstructor
    */
    MESSAGE_DISPATCHER_CLASS::~MESSAGE_DISPATCHER_CLASS()
    {
        this->m_started = ZOO_FALSE;
        this->m_thread_pool->shutdown();
        this->m_sync_message_queue.clear();
        this->m_subscribe_message_queue.clear();
    }
    
    /*
     * @brief add message in queue
    */
    void MESSAGE_DISPATCHER_CLASS::en_queue_message(boost::shared_ptr<MESSAGE_CLASS> message)
    {
        boost::unique_lock<boost::shared_mutex> l(this->m_shared_lock);
        if(message)
        {
            if(message->event.load() == MQ4A_MSG_EVENT_SUBSCRIPTION)
            {   
                if(this->m_subscribe_message_queue.size() < this->m_capacity)
                {
                    this->m_subscribe_message_queue.emplace_back(message);
                    this->m_wait_handle->set();
                }
                else
                {
                    ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__,"subscribe_message_queue is full");
                }
            }
            else
            {
                if(this->m_sync_message_queue.size() < this->m_capacity)
                {
                    this->m_sync_message_queue.emplace_back(message);
                    this->m_wait_handle->set();
                }
                else
                {
                    ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__,"sync_message_queue is full");
                }
            }
        }
    }

    /*
     * @brief update message in queue
    */
    void MESSAGE_DISPATCHER_CLASS::update_queue_message(boost::shared_ptr<MESSAGE_CLASS> message)
    {
        boost::unique_lock< boost::shared_mutex > l(this->m_shared_lock);
        for(std::list<boost::shared_ptr<MESSAGE_CLASS> >::iterator ite = this->m_sync_message_queue.begin();
                    ite != this->m_sync_message_queue.end();
                    ++ite)
        {
            boost::shared_ptr<MESSAGE_CLASS> m =*ite;
            if(message->id == m->id)
            {
                m->data = message->data;
                m->thread_id = message->thread_id;
                m->event.store(message->event);
                this->m_wait_handle->set();
                break;
            }
        }
    }
    
   /*
     * @brief get message 
     * @brief message_id  the message id
    */
    boost::shared_ptr<MESSAGE_CLASS> MESSAGE_DISPATCHER_CLASS::get_message(ZOO_UINT32            message_id)
    {
        boost::shared_lock< boost::shared_mutex > l(this->m_shared_lock);
        boost::shared_ptr<MESSAGE_CLASS> message;
        for(std::list<boost::shared_ptr<MESSAGE_CLASS> >::iterator ite = this->m_sync_message_queue.begin();
                    ite != this->m_sync_message_queue.end(); ++ite)
        {
            if((*ite)->id == message_id)
            {
                message = *ite;
                break;
            }
        }
  
        return message;
    }
    
    /*
     * @brief get message 
     * @brief t_id  the thread id
     */
    boost::shared_ptr<MESSAGE_CLASS> MESSAGE_DISPATCHER_CLASS::get_message_by_thread(ZOO_LONG t_id)
    {
        boost::shared_lock< boost::shared_mutex > l(this->m_shared_lock);
        boost::shared_ptr<MESSAGE_CLASS> message;
        for(std::list<boost::shared_ptr<MESSAGE_CLASS> >::iterator ite = this->m_sync_message_queue.begin();
                    ite != this->m_sync_message_queue.end();
                    ++ite)
        {
            if((*ite)->thread_id == t_id)
            {
                message = *ite;
                break;
            }
        }
        return message;
    }
    
     /**
     * @brief shutdown the message dispatcher 
    */
    void MESSAGE_DISPATCHER_CLASS::shutdown()
    {
        this->m_started = ZOO_FALSE;
    }
    
    /**
     * @brief initialize the dispatcher
     */
    void MESSAGE_DISPATCHER_CLASS::initialize(ZOO_INT32 capacity, ZOO_BOOL background)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"capacity:%d,background:%d",capacity,background);
        this->m_capacity = capacity;
        this->m_started = ZOO_TRUE;
        this->m_wait_handle->reset();
        if(background)
        {
            this->m_thread_pool->queue_working(boost::bind(&MESSAGE_DISPATCHER_CLASS::dispatch_message,this));
        }
        else
        {
            this->dispatch_message();
        }
    }
            
    /**
     * @brief run the dispatch
    */
    void MESSAGE_DISPATCHER_CLASS::dispatch_message()
    {
        
        while(this->m_started)
        {
           __MQ_TRY
           { 
                this->m_wait_handle->reset();
                
                /**
                 * @brief handle sync message
                 */
                boost::shared_ptr<MESSAGE_CLASS> message;
                ZOO_BOOL dequeue = this->dequeue_synchronize_message(message);
                if(dequeue)
                {
                    this->on_handle_synchronize_message(message);
                }

                /**
                 * @brief handle subscribe message
                 */
                message.reset();
                dequeue = this->dequeue_subscribe_message(message);
                if(dequeue)
                {
                    this->on_handle_subscribe_message(message);
                }

                /**
                 * @brief stop
                 */    
                if(!this->m_started)
                {
                    break;
                }

                /**
                 * @brief wait one message
                 */
                this->m_wait_handle->wait_one();
            }
            __MQ_CATCH_ALL
        }

        this->m_subscribe_message_queue.pop_front();
        this->m_sync_message_queue.pop_front();
    }
    
    /**
     * @brief subscribe message handler
     * @brief message 
     */
    void MESSAGE_DISPATCHER_CLASS::on_handle_subscribe_message(boost::shared_ptr<MESSAGE_CLASS> message)
    {      
        if(message)
        {      
            for(auto const & handler : message->handler)
            {
                if(handler)
            	{
            	    
                	static_cast<MQ4A_EVENT_CALLBACK_FUNCTION>(handler->get_callback_function())
                                                             (handler->get_callback_context(),
                                                              handler->get_callback_parameter(),
                                                              (void *)message->data.data());
                }
            }
        }
    }

    /**
     * @brief synchronize message handler
     * @brief message 
     */ 
    void MESSAGE_DISPATCHER_CLASS::on_handle_synchronize_message(boost::shared_ptr<MESSAGE_CLASS> message)
    {
        if(message)
        {      
            /*ZOO_TIME_T now = ZOO_get_current_timestamp_usec();
            if(now > message->deadline)
            {
                message->store(MQ4A_MSG_EVENT_CONSUMED);
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"remove id:%d out of date",message->id);
            }*/
        }
    }

    /**
     * @brief dequeue synchronize message 
     * @brief message 
     */ 
    ZOO_BOOL MESSAGE_DISPATCHER_CLASS::dequeue_synchronize_message(boost::shared_ptr<MESSAGE_CLASS> & message)
    {
        boost::unique_lock< boost::shared_mutex > l(this->m_shared_lock);
        ZOO_BOOL result = ZOO_FALSE;
        if(!this->m_sync_message_queue.empty())
        {
            message = this->m_sync_message_queue.front();
            switch(message->event.load())
            {
                case MQ4A_MSG_EVENT_CONSUMED:
                case MQ4A_MSG_EVENT_ABANDONED:
                {
                    result = ZOO_TRUE;
                    this->m_sync_message_queue.pop_front();
                    break;
                }
                case MQ4A_MSG_EVENT_REQUESTED:
                {
                    ZOO_UINT64 now = ZOO_get_current_timestamp_msec();
                    if(now > message->deadline)
                    {
                        message = this->m_sync_message_queue.front();
                        result = ZOO_TRUE;
                        this->m_sync_message_queue.pop_front();
                        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"dequeue id :%ld,now:%llu,deadline:%llu as timeout",
                                            message->id,now,message->deadline);
                    }
                    break;
                }
                default:break;
            }
        }
        return result;
    }
    
     /**
      * @brief dequeue subscribe message 
      * @brief message 
      */
    ZOO_BOOL MESSAGE_DISPATCHER_CLASS::dequeue_subscribe_message(boost::shared_ptr<MESSAGE_CLASS> & message)
    {
        boost::unique_lock< boost::shared_mutex > l(this->m_shared_lock);
        ZOO_BOOL result = ZOO_FALSE;
        if(!this->m_subscribe_message_queue.empty())
        {
            message = this->m_subscribe_message_queue.front();
            result = ZOO_TRUE;
            this->m_subscribe_message_queue.pop_front();
        }
        return result;
    }
}
