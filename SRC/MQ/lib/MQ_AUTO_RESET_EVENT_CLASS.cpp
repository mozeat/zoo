/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : MQ
 * File Name    : MQ_AUTO_RESET_EVENT_CLASS.cpp
 * Description  : This class is used to support synchronization between
 *                  thread via events.
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-05-18			mozeat				initialize
 ***************************************************************/
#include "MQ_AUTO_RESET_EVENT_CLASS.h"

namespace ZOO_MQ
{
    /**
     * @brief Default constructor.
     * @param initial_state
     *          true to set the initial state to signaled;
     *          false to set the initial state to non-signaled.
     */
    MQ_AUTO_RESET_EVENT_CLASS::MQ_AUTO_RESET_EVENT_CLASS()
            : m_flag(ZOO_FALSE)
    {
    }

    /**
     * @brief Default constructor.
     * @param initial_state
     *          true to set the initial state to signaled;
     *          false to set the initial state to non-signaled.
     */
    MQ_AUTO_RESET_EVENT_CLASS::MQ_AUTO_RESET_EVENT_CLASS(ZOO_BOOL initial_state)
            : m_flag(initial_state)
    {
    
    }

    /**
     * @brief Deconstruct
     */
    MQ_AUTO_RESET_EVENT_CLASS::~MQ_AUTO_RESET_EVENT_CLASS()
    {

    }

    /**
     * @brief Sets the state of the event to signaled, allowing one or more waiting threads to proceed.
     */
    void MQ_AUTO_RESET_EVENT_CLASS::set()
    {
        {
            boost::lock_guard<boost::mutex> synchronize(m_protect);
            this->m_flag = ZOO_TRUE;
        }

        this->m_signal.notify_one();
    }

    /**
     * @brief Sets the state of the event to nonsignaled, causing threads to block.
     */
    void MQ_AUTO_RESET_EVENT_CLASS::reset()
    {
        boost::lock_guard<boost::mutex> synchronize(m_protect);
        this->m_flag = ZOO_FALSE;
    }

    /**
     * @brief Blocks the current thread until the current WaitHandle receives a signal.
     */
    void MQ_AUTO_RESET_EVENT_CLASS::wait_one()
    {
        boost::unique_lock<boost::mutex> synchronize(m_protect);
        while (!this->m_flag) // prevent spurious wakeups from doing harm
        {
            this->m_signal.wait(synchronize);
        }

        this->m_flag = ZOO_FALSE; // waiting resets the flag
    }

    /**
     * @brief Blocks the current thread until the current WaitHandle receives a signal.
     * @param delay_time    The delay time (ms)
     */
    ZOO_BOOL MQ_AUTO_RESET_EVENT_CLASS::wait_one(ZOO_UINT32 delay_time)
    {
        if (delay_time > 0)
        {
            boost::unique_lock<boost::mutex> synchronize(m_protect);
            boost::system_time const timeout = boost::get_system_time()
                    + boost::posix_time::milliseconds(delay_time);
            return this->m_signal.timed_wait(synchronize, timeout);
        }
		return ZOO_FALSE;
    }

    /**
     * @brief The execution of the current thread (which shall have locked lock's mutex) is blocked during delay_time
     * @param The time span during which the thread will block waiting to be notified (delay_time in second)
     */
    void MQ_AUTO_RESET_EVENT_CLASS::sleep_for(ZOO_UINT32 delay_time)
    {
        if (delay_time > 0)
        {
            boost::unique_lock<boost::mutex> synchronize(m_protect);
            boost::system_time const timeout = boost::get_system_time()
                    + boost::posix_time::seconds(delay_time);
            this->m_signal.timed_wait(synchronize, timeout);
        }
    }

} /* namespace MQ */
