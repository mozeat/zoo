/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: SOCKET_INTERFACE.h
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2017-12-21     weiwang.sun       created
******************************************************************************/
#include "SOCKET_INTERFACE.h"
#include <iostream>
namespace ZOO_MQ
{
    /**
     * @brief default constructor
    */
    SOCKET_INTERFACE::SOCKET_INTERFACE(){}

    /**
     * @brief destructor
    */
    SOCKET_INTERFACE::~SOCKET_INTERFACE(){}

    /**
     * @brief initialize
     * @param server_addr -address
     * @param type - message queue type : tcp/ipc/intipc
     * @description : enable server service
    */
    void SOCKET_INTERFACE::initialize()
    {
        return ;
    }

    /**
     * @brief terminate
     * @param server_addr -address
     * @description : destory bind
    */
    void SOCKET_INTERFACE::terminate()
    {
        return ;
    }

    /*
     * @brief send
     * @param server_addr -address
     * @param req_msg -request message
     * @param req_len -size of message
     * @param retry_interval
     * @param timeout
     * @description : destory bind
    */
    void SOCKET_INTERFACE::send_reply(IN ZOO_UINT32 msg_id,IN void *rep_msg,
                                IN ZOO_INT32 rep_len)
    {
        return ;
    }

    /**
     * @brief send_and_reply
     * @param server_addr -address
     * @param req_msg -request message
     * @param req_len -size of message
     * @param retry_interval
     * @param timeout :sencods
     * @description : destory bind
    */
    void SOCKET_INTERFACE::send_and_reply(IN void *req_msg,
                                                        IN ZOO_INT32 req_len,
                                                        OUT void *rep_msg,
                                                        IN ZOO_INT32 rep_len,
                                                        IN ZOO_INT32 *act_rep_len,
                        							    IN ZOO_INT32 retry_interval,
                        							    IN ZOO_INT32 timeout)
    {
        return ;
    }

   /**
    * @brief send_request
    * @param server_addr -address
    * @param req_msg -request message
    * @param req_len -size of message
    * @param retry_interval
    * @param timeout
    * @description : destory bind
   */
   void SOCKET_INTERFACE::send_request(IN void *req_msg,
                                           IN ZOO_INT32 req_len,
                                           IN ZOO_INT32 retry_interval,
                                                    IN ZOO_BOOL signal)
   {
        return ;
   }

   /**
    * @brief recv
    * @param server_addr -address
    * @description : destory bind
   */
   void SOCKET_INTERFACE::get_reply(OUT void *rep_msg,
                                           IN ZOO_INT32 rep_len,
                                           IN ZOO_INT32 *act_rep_len,
                                           IN ZOO_INT32 timeout)
   {
       return ;
   }

   /**
     * @brief register_events
     * @param callback -
     * @param timeout -
     * @param request_message_lenght -
     * @description : destory bind
    */
    void SOCKET_INTERFACE::register_events_handler(IN MQ4A_BIND_CALLBACK_FUNCTION callback_function)
    {
        return ;
    }

   /**
     * @brief subscribe
     * @param server_name -
     * @param event_id -
     * @param request_message_lenght -
     * @param callback_function -
     * @description : destory bind
    */
    void SOCKET_INTERFACE::subscribe(IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
                                                IN MQ4A_CALLBACK_STRUCT * callback_struct,
                                                IN ZOO_INT32 event_id,IN ZOO_HANDLE *handle,
                                                INOUT void* context)
    {
        return ;
    }

    /**
     * @brief unsubscribe
     * @param server_name -
     * @param event_id -
     * @description : destory bind
    */
    void SOCKET_INTERFACE::unsubscribe(IN ZOO_INT32 event_id,IN ZOO_HANDLE handle)
    {
        return ;
    }

   /**
     * @brief publish
     * @param server_name -
     * @param event_id -
     * @param request_message -
     * @param request_message_lenght
     * @description : destory bind
    */
    void SOCKET_INTERFACE::publish(IN ZOO_INT32 event_id,
                                            IN void *request_message,
                                            IN ZOO_INT32 request_message_lenght)
    {
        return ;
    }

    /**
     * @brief process_events
     * @description : enter eventloop and process
    */
    void SOCKET_INTERFACE::loop_event()
    {
        return ;
    }

    void SOCKET_INTERFACE::shutdown()
    {
        return;
    }
}
