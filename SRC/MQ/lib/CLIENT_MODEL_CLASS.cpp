/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: CLIENT_MODEL_CLASS.cpp
* Description: frontend
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     weiwang.sun       		created
******************************************************************************/
#include "CLIENT_MODEL_CLASS.h"
#include <mutex>
#include <thread>
#include <chrono>
#include <MM4A_if.h>
#include <sstream>
#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/random.hpp>
#include "common_mirco_define.h"
namespace ZOO_MQ
{
    /**
     * @brief constructor
    */
    CLIENT_MODEL_CLASS::CLIENT_MODEL_CLASS(std::string marking_code)
    :m_context(1),
	m_local_sub(m_context,ZMQ_SUB),
	m_local_pub(m_context,ZMQ_PUB),
	m_controller(m_context,ZMQ_PAIR)
    {
        this->m_marking_code  = marking_code;   
        this->initialize();
    }

    /**
     * @brief distructor
    */
    CLIENT_MODEL_CLASS::~CLIENT_MODEL_CLASS()
    {
        this->m_message_dispatcher->shutdown();
        this->m_thread_pool->shutdown();
    }

    /**
     * @brief subscribe
     * @param callback_function - user resgister callback
     * @param callback_struct - call parameters
     * @param event_id -
     * @param context -
     * @description : subscribe event by event id
    */
    void CLIENT_MODEL_CLASS::subscribe(IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
                                                IN MQ4A_CALLBACK_STRUCT * callback_struct,
                                                IN ZOO_INT32 event_id,
                                                INOUT ZOO_HANDLE *handle,
                                                INOUT void* context)
    {
    	boost::unique_lock<boost::shared_mutex> subscriber_lock(this->m_event_readwrite_lock);
        static boost::mt19937 engine(boost::random_device{}());
        static boost::random::uniform_int_distribution<uint32_t> distribution;
        *handle = static_cast<ZOO_HANDLE> (distribution(engine));
        this->add_event_handle(callback_function,callback_struct,event_id,*handle,context);
        subscriber_lock.unlock();
    }

    /**
     * @brief Unsubscribe event
     * @param event_id the event id for subscribtion
     * @description :
    */
    void CLIENT_MODEL_CLASS::unsubscribe(IN ZOO_INT32 event_id,IN ZOO_HANDLE handle)
    {
        boost::shared_lock<boost::shared_mutex> remove_lock(this->m_event_readwrite_lock);
        this->remove_event_handle(event_id,handle);
        remove_lock.unlock();
    }

	/**
     * @brief send_and_reply
     * @param server_addr -address
     * @param req_msg -request message
     * @param req_len -size of message
     * @param retry_interval
     * @param timeout
     * @description : destory bind
    */
    void CLIENT_MODEL_CLASS::send_and_reply(IN void *req_msg,
		                                            IN ZOO_INT32 req_len,
		                                            OUT void *rep_msg,
		                                            IN ZOO_INT32 rep_len,
		                                            IN ZOO_INT32 *act_rep_len,
		    									    IN ZOO_INT32 retry_interval,
		    									    IN ZOO_INT32 timeout)
    {
        boost::lock_guard<boost::mutex> sync_locker(this->m_sync_lock);
        /*
         * @brief for timeout
         */
        ZOO_UINT64 now = ZOO_get_current_timestamp_msec();
        ZOO_UINT64 deadline = now + timeout;
        
        /*
         * @brief mid type is int and subscribe type also is int,
         * may cause they were the same id while recv.hence,it has to exclude the event id when generate mid
         */
        std::vector<ZOO_UINT32> excluded_ids = this->get_subscribe_events();        
        ZOO_UINT32 mid = this->generate_mid(excluded_ids);
        
        /*ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
                                "send %s,id:%d,now:%llu,deadline:%llu",this->m_marking_code.data(),mid,now,deadline);*/

        /*
         * @brief convert mid from int to string to send convenience
         */
        std::string mid_str = boost::lexical_cast<std::string>(mid);

        /*
         * @brief create message for send
         */
        boost::shared_ptr<MESSAGE_CLASS> request_message =
                boost::make_shared<MESSAGE_CLASS>(mid,MQ4A_MSG_EVENT_REQUESTED);
        request_message->deadline = deadline;	 
        request_message->server = this->m_marking_code;
        /*
    	 * @brief store message for recv.should be insert queue before send.
    	 */
    	this->m_message_dispatcher->en_queue_message(request_message);
    	
        for(int i = 0;i < retry_interval; i++)
	    {
	    	__MQ_TRY
	        {
	        	this->send_message(this->m_local_pub,mid_str.data(),mid_str.size(),req_msg,req_len);
	        	break;
	        }
	        __MQ_CATCH
			{
				if((retry_interval-1) == i)
				{
				    request_message->event = MQ4A_MSG_EVENT_ABANDONED;
					__THROW_MQ_EXCEPTION(ex.get_error_code(), ex.get_error_message().data(),NULL);
				}
			}
        }  
       
        while(1)
        {
            /*
        	 * @brief grab this thread message from message queue.
        	 */
            boost::shared_ptr<MESSAGE_CLASS> reply_message = this->get_request_message(mid);
            if(reply_message)
            {
               MQ4A_MSG_EVENT_ENUM event = reply_message->event.load();
               if(event == MQ4A_MSG_EVENT_REPLIED)
               {
                   memcpy(rep_msg,reply_message->data.data(),rep_len);
                   *act_rep_len = reply_message->data.size();
                   reply_message->event.store(MQ4A_MSG_EVENT_CONSUMED);
                   /*ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
                                "recv from %s 
                                id:%ld,event:%d,",this->m_marking_code.data(),reply_message->id,reply_message->event.load());*/
                   break;
               }

               /*
            	* @brief check timeout.
            	*/
               now = ZOO_get_current_timestamp_msec();
               if(reply_message->deadline <= now)
               {
                    reply_message->event.store(MQ4A_MSG_EVENT_ABANDONED);
                    __THROW_MQ_EXCEPTION(MQ4A_TIMEOUT_ERR,"reply timeout",NULL);
               }
           }
       }
    }
	
    /** @brief Send request to server
     * @param req_msg -request message to server
     * @param req_len -the length of message
     * @description : send message to server
    */
    void CLIENT_MODEL_CLASS::send_request(IN void *req_msg,
                                                        IN ZOO_INT32 req_len,
    									                IN ZOO_INT32 retry_interval,
                                                        IN ZOO_BOOL signal )
    {        
        boost::lock_guard<boost::mutex> sync_locker(this->m_sync_lock);
        std::vector<ZOO_UINT32> excluded_ids = this->get_subscribe_events();        
        ZOO_UINT32 mid = this->generate_mid(excluded_ids);      
        std::string mid_str = boost::lexical_cast<std::string>(mid);
        boost::shared_ptr<MESSAGE_CLASS> message =
                boost::make_shared<MESSAGE_CLASS>(mid,MQ4A_MSG_EVENT_REQUESTED);
        if(!signal)
        {
            this->m_message_dispatcher->en_queue_message(message);
        }

        for(int i = 0;i < retry_interval; i++)
	    {
	    	__MQ_TRY
	        {
	        	this->send_message(this->m_local_pub,mid_str.data(),mid_str.size(),req_msg,req_len);
	        	break;
	        }
	        __MQ_CATCH
			{
				if((retry_interval-1) == i)
				{
				    if(!signal)
                    {
                        message->event = MQ4A_MSG_EVENT_ABANDONED;
                    }
					__THROW_MQ_EXCEPTION(ex.get_error_code(), ex.get_error_message().data(),NULL);
				}
			}
        }
    }

    /** @brief Get reply message from server
     * @param rep_msg -request message
     * @param rep_len -except message length
     * @param act_rep_len -the actually tranfer message from server
     * @param timeout     -milliseconds
    */
    void CLIENT_MODEL_CLASS::get_reply(OUT void * rep_msg,
                                                    IN ZOO_INT32 rep_len,
                                                    IN ZOO_INT32 *act_rep_len,
            									    IN ZOO_INT32 timeout)
    {       
        /*
    	 * @brief grab this thread message from message queue.
    	 */
        boost::shared_ptr<MESSAGE_CLASS> message = this->grab_message();
        if(message == nullptr)
        {
            __THROW_MQ_EXCEPTION(MQ4A_PARAMETER_ERR,"it's an invalid request",NULL);
        }

        message->deadline = timeout + message->request_time;
        if(message->event.load() == MQ4A_MSG_EVENT_REPLIED)
        {
            memcpy(rep_msg,message->data.data(),rep_len);
            *act_rep_len = message->data.size();
            message->event = MQ4A_MSG_EVENT_CONSUMED;
        }
        else
        {
            ZOO_UINT64 now = ZOO_get_current_timestamp_msec();
            /*ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"now:%lu,deadline:%lu",now,message->deadline);*/
            if(message->deadline < now)
            {
                message->event = MQ4A_MSG_EVENT_ABANDONED;
                __THROW_MQ_EXCEPTION(MQ4A_TIMEOUT_ERR,"reply time out",NULL);
            }
            
            /*
        	 * @brief reset reply message,indicate that message had not arrived yet.
        	 */
            if(rep_msg)
            {
                memset(rep_msg,0x0,rep_len*sizeof(ZOO_CHAR));
            }
        }
    }

    /**
     * @brief add_callback_fucntion
     * @description : add callback to map
    */
    void CLIENT_MODEL_CLASS::add_event_handle(IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
                                                                    IN MQ4A_CALLBACK_STRUCT * callback_struct,
                                                                    IN ZOO_UINT32 event_id,
                                                                    IN ZOO_HANDLE handle,
                                                                    INOUT void* context)
    {
        auto event_handle = boost::make_shared<CALLBACK_FUNCTION>(callback_function,
                                                        callback_struct,context,handle);
        if(this->m_event_map.find(event_id) == this->m_event_map.end())
        {
           CALLBACK_FUNCTION_VECTOR event_handle_vector;
           event_handle_vector.push_back(event_handle);
           this->m_event_map[event_id] = event_handle_vector;
        }
        else
        {
           this->m_event_map[event_id].push_back(event_handle);
        }
    }

    /**
     * @brief remove_callback_function
     * @description : remove callback
    */
    void CLIENT_MODEL_CLASS::remove_event_handle(IN ZOO_UINT32 event_id,IN ZOO_HANDLE handle)
    {
        if(this->m_event_map.find(event_id) != this->m_event_map.end())
        {
            auto found = this->m_event_map[event_id].begin();
            auto end   = this->m_event_map[event_id].end();
            while(found != end)
            {
                CALLBACK_FUNCTION_PTR callback = *found;
                if(callback->get_handle() == handle)
                {
                    this->m_event_map[event_id].erase(found);
                    break;
                }
                ++found;
            }
        }
    }

    /**
     * @brief get_callback_fucntion
     * @description : get callback from map
    */
    const CALLBACK_FUNCTION_VECTOR & CLIENT_MODEL_CLASS::get_callback_context(IN ZOO_UINT32 event_id)
    {
        if(this->m_event_map.find(event_id) != this->m_event_map.end())
        {
            return this->m_event_map[event_id];
        }
        else
        {
            __THROW_MQ_EXCEPTION(MQ4A_PARAMETER_ERR,"no such event id subscribed ...ERROR",NULL);						
        }
    }

    /**
     * @brief shutdown the subscribe socket when has subsriber
    */
    void CLIENT_MODEL_CLASS::shutdown()
    {
        std::string shutdown("shutdown");
        zmq::message_t message(shutdown.size());
        memcpy(message.data(),shutdown.data(),shutdown.size());
        zmq::socket_t shutdown_signal(this->m_context,ZMQ_PAIR);
        shutdown_signal.connect(this->m_controller_server);
        BOOST_ASSERT_MSG(shutdown_signal.send(message) > 0, "Shut down backgroud thread fail");
		std::this_thread::sleep_for(std::chrono::seconds(1));
		shutdown_signal.close();	
    }
    
    /**
     * @brief initialize
     * @description : connect server
    */
    void CLIENT_MODEL_CLASS::initialize()
    {
        ZOO_BROKER_STRUCT broker;
        BOOST_ASSERT_MSG(OK == ZOO_get_brokers(&broker),"Get broker configuration failed");
        
		std::string ipc_file_path     = ENVIRONMENT_ULTILITY_CLASS::get_instance()->get_ipc_path();
		std::string inproc_file_path  = ENVIRONMENT_ULTILITY_CLASS::get_instance()->get_inproc_path();
		this->m_local_pub_server  = ipc_file_path + "/" + broker.local_fe_send;
		this->m_local_sub_server  = ipc_file_path + "/" + broker.local_fe_recv;
		this->m_controller_server = inproc_file_path + "/" + this->m_marking_code + ".c.controller";
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION__,"server name:%s,pub server:%s,sub server:%s",
					this->m_marking_code.c_str(),this->m_local_pub_server.c_str(),this->m_local_sub_server.c_str());
	
        this->m_thread_pool.reset(new MQ_THREAD_POOL(this->get_frontend_thread_number()));		
		this->m_message_dispatcher.reset(new MESSAGE_DISPATCHER_CLASS(broker.hwm,ZOO_TRUE,this->m_thread_pool));
		
        this->m_local_pub.connect(this->m_local_pub_server);
		this->m_local_pub.setsockopt(ZMQ_SNDHWM,&broker.hwm,sizeof(broker.hwm));
		this->m_local_pub.setsockopt(ZMQ_LINGER,&broker.linger,sizeof(broker.linger));
		
		this->m_local_sub.connect(this->m_local_sub_server);
		this->m_local_sub.setsockopt(ZMQ_RCVHWM,&broker.hwm,sizeof(broker.hwm));
		this->m_local_sub.setsockopt(ZMQ_LINGER,&broker.linger,sizeof(broker.linger));
		this->m_local_sub.setsockopt(ZMQ_SUBSCRIBE,this->m_marking_code.data(),this->m_marking_code.size());
		
		this->m_controller.bind(this->m_controller_server);
		this->m_controller.setsockopt(ZMQ_SNDHWM,&broker.hwm,sizeof(broker.hwm));
		this->m_controller.setsockopt(ZMQ_LINGER,&broker.linger,sizeof(broker.linger));
		
		//time for establishing connection 
		std::this_thread::sleep_for(std::chrono::seconds(1));
		
		this->m_thread_pool->queue_working(boost::bind(&CLIENT_MODEL_CLASS::loop_event,this));

    }

    /**
     * @brief listen
     * @description : event loop
    */
    void CLIENT_MODEL_CLASS::loop_event(void)
    {
        zmq::pollitem_t items [] = {{ static_cast<void *>(this->m_local_sub), 0, ZMQ_POLLIN, 0 },
                                    { static_cast<void *>(this->m_controller), 0, ZMQ_POLLIN, 0 }};                            
        __MQ_EVENT_LOOP_BEGIN
        {
            zmq::poll(&items[0],2, MQ4A_TIMEOUT_INFINITE);

            /**
             * @brief Process message from publish server
             */
            if(items[0].revents & ZMQ_POLLIN)
            {
                std::string mid;
                std::string message;   
                this->receive_message(this->m_local_sub,mid,message);
                this->consume_message(mid,message);
            }

            /**
             * @brief Shutdown
             */
            if(items[1].revents & ZMQ_POLLIN)
            {
                this->m_message_dispatcher->shutdown();
                this->m_thread_pool->shutdown();
				break;
            }
        }
        __MQ_EVENT_LOOP_END
    }

	/**
     * @brief listen
     * @description : event_loop
    */
    void CLIENT_MODEL_CLASS::reconnect()
    {
    	this->m_local_pub.disconnect(this->m_local_pub_server);
    	this->m_local_pub.connect(this->m_local_pub_server);
    }
    
    /**
     * @brief consume one message from queue
     * @param mid -
     * @param data -
    */
    void CLIENT_MODEL_CLASS::consume_message(std::string & mid,const std::string & data)
    {
        ZOO_UINT32 id = boost::lexical_cast<ZOO_UINT32>(mid); 
        boost::shared_ptr<MESSAGE_CLASS> message = this->m_message_dispatcher->get_message(id);
        if(message)
        {
            message->data = data;
            message->event.store(MQ4A_MSG_EVENT_REPLIED);
            this->m_message_dispatcher->update_queue_message(message);
        }

        //check is event id subscribe
        else
        {
            std::vector<ZOO_UINT32> ids = this->get_subscribe_events();
            auto result = std::find(std::begin(ids), std::end(ids),id);
            if(result != ids.end())
            {
                message.reset(new MESSAGE_CLASS(id,data,this->get_callback_context(id)));
                this->m_message_dispatcher->en_queue_message(message);
            }
            else
            {
                ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,__ZOO_FUNC__,"mid:%d either request nor subscribe,abandon",id);
            }
        }
    }

    /**
     * @brief get message from queue by thread id
     */
    boost::shared_ptr<MESSAGE_CLASS> CLIENT_MODEL_CLASS::grab_message()
    {
        return this->m_message_dispatcher->get_message_by_thread(ZOO_get_thread_id());
    }

    /**
     * @brief get message from queue by message id
     * @brief mid message id
     */
    boost::shared_ptr<MESSAGE_CLASS> CLIENT_MODEL_CLASS::get_request_message(ZOO_UINT32 mid)
    {
        return this->m_message_dispatcher->get_message(mid);
    }

    /**
     * @brief the subscribe fucntion code vector
     */
    std::vector<ZOO_UINT32> CLIENT_MODEL_CLASS::get_subscribe_events()
    {
        std::vector<ZOO_UINT32> events;
        for(std::map<ZOO_UINT32,CALLBACK_FUNCTION_VECTOR>::iterator ite = this->m_event_map.begin();
                ite != this->m_event_map.end();++ite)
        {
            events.emplace_back(ite->first);
        }
        return events;
    }
}
