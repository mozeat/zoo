/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: FACADE_FLOW_ABSTRACT_CLASS.cpp
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#include "FACADE_FLOW_ABSTRACT_CLASS.h"
namespace ZOO_MQ
{
	/**
	 * @brief default constructor
	*/
	FACADE_FLOW_ABSTRACT_CLASS::FACADE_FLOW_ABSTRACT_CLASS()
	{
	
	}

	/**
	 * @brief Destructor
	*/
	FACADE_FLOW_ABSTRACT_CLASS::~FACADE_FLOW_ABSTRACT_CLASS()
	{
	
	}
	
	/**
	 * @brief 设置服务端控制器实例
	 * @param server_controller 服务器控制器
	 * @return 
	*/
	void FACADE_FLOW_ABSTRACT_CLASS::set_server_controller(IN boost::shared_ptr<SERVER_CONTROLLER_CLASS> server_controller)
	{
		this->m_server_controller = server_controller;
	}

	/**
	 * @brief 获取服务端控制器实例
	 * @param server_controller 服务器控制器
	 * @return 
	*/
	boost::shared_ptr<SERVER_CONTROLLER_CLASS> FACADE_FLOW_ABSTRACT_CLASS::get_server_controller()
	{
		return this->m_server_controller;
	}

	/**
	 * @brief 设置客户端控制器实例
	 * @param client_controller 服务器控制器
	 * @return 
	*/
	void FACADE_FLOW_ABSTRACT_CLASS::set_client_controller(IN boost::shared_ptr<CLIENT_CONTROLLER_CLASS> client_controller)
	{
		this->m_client_controller = client_controller;
	}

	/**
	 * @brief 获取服务端控制器实例
	 * @param server_controller 服务器控制器
	 * @return 
	*/
	boost::shared_ptr<CLIENT_CONTROLLER_CLASS> FACADE_FLOW_ABSTRACT_CLASS::get_client_controller()
	{
		return this->m_client_controller;
	}
}
