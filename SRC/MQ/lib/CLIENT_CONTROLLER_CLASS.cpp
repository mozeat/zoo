/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: CLIENT_CONTROLLER_CLASS.cpp
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#include <CLIENT_CONTROLLER_CLASS.h>
#include <chrono>
namespace ZOO_MQ
{
    /**
     * @brief Default constructor
    */
    CLIENT_CONTROLLER_CLASS::CLIENT_CONTROLLER_CLASS()
    {

    }

    /**
     * @brief Destructor
    */
    CLIENT_CONTROLLER_CLASS::~CLIENT_CONTROLLER_CLASS()
    {

    }

    /**
     * @brief add_model
     * @param:std::string server
     * @description: add client model by server server
    */
    void CLIENT_CONTROLLER_CLASS::add_model(IN boost::shared_ptr<CLIENT_MODEL_CLASS> client_model)
    {
        if(client_model != NULL)
        {
            this->m_client_map[client_model->get_marking_code()] = client_model;
        }
    }

    /**
     * @brief remove_model
     * @param:std::string server
     * @description: delete client model by server server
    */
    void CLIENT_CONTROLLER_CLASS::remove_model(IN boost::shared_ptr<CLIENT_MODEL_CLASS> client_model)
    {
        if(client_model != NULL)
        {
            std::map<std::string,boost::shared_ptr<CLIENT_MODEL_CLASS> >::iterator ite =
                                      this->m_client_map.find(client_model->get_marking_code());
            if(ite != this->m_client_map.end())
            {
                this->m_client_map.erase(ite);
            }
        }
    }

    /**
     * @brief subscribe
     * @param:std::string server
     * @description: subscribe callback function by event id
    */
    void CLIENT_CONTROLLER_CLASS::subscribe(IN std::string server,
                                                    IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
                                                    IN MQ4A_CALLBACK_STRUCT * callback_struct,
                                                    IN ZOO_INT32 event_id,
                                                    IN ZOO_HANDLE *handle,
                                                    INOUT void* context)
    {
        boost::shared_ptr<CLIENT_MODEL_CLASS> client_model = this->get_model(server);
        if(client_model != NULL)
        {
            client_model->subscribe(callback_function,callback_struct,event_id,handle,context);
        }
    }

    /**
     * @brief unsubscribe
     * @param:std::string server
     * @description: unsubscribe event  id
    */
    void CLIENT_CONTROLLER_CLASS::unsubscribe(IN std::string server,IN ZOO_INT32 event_id,IN ZOO_HANDLE handle)
    {
        boost::shared_ptr<CLIENT_MODEL_CLASS> client_model = this->get_model(server);
        if(client_model != NULL)
        {
            client_model->unsubscribe(event_id,handle);
        }
    }

    /**
     * @brief push
     * @param:std::string server
     * @description:
    */
    void CLIENT_CONTROLLER_CLASS::send_request(IN const std::string server,
                                                   IN void *req_msg,
                                                   IN ZOO_INT32 req_len,
                                                   IN ZOO_INT32 retry_interval,
                                                   IN ZOO_BOOL signal)
    {
        boost::shared_ptr<CLIENT_MODEL_CLASS> client_model = this->get_model(server);
        if(client_model != NULL)
        {
            client_model->send_request(req_msg,req_len,retry_interval,signal);
        }
    }

    /**
     * @brief push
     * @param:std::string server
     * @description:
    */
    void CLIENT_CONTROLLER_CLASS::get_reply(IN const std::string server,
				                                IN void *rep_msg,
				                                IN ZOO_INT32 rep_len,
				                                IN ZOO_INT32 *act_rep_len,
				                                IN ZOO_INT32 timeout)
    {
        boost::shared_ptr<CLIENT_MODEL_CLASS> client_model = this->get_model(server);
        if(client_model != NULL)
        {
            client_model->get_reply(rep_msg,rep_len,act_rep_len,timeout);
        }
    }

	/**
     * @brief 客户端发送请求,客户端获取回答
     * @param server 服务器地址
     * @param req_msg 请求消息
     * @param rep_len 请求消息长度
     * @param act_rep_len 实际收到的字节长度
     * @param timeout  超时时间
     * @return 错误码
    */	
    void CLIENT_CONTROLLER_CLASS::send_and_reply(IN const MQ4A_SERV_ADDR server,
                                        		IN void *req_msg,
                                        		IN ZOO_INT32 req_len,
                                                IN void *rep_msg,
                                                IN ZOO_INT32 rep_len,
                                                IN ZOO_INT32 *act_rep_len,
                                        		IN ZOO_INT32 retry_interval,
                                                IN ZOO_INT32 timeout)
	{
		boost::shared_ptr<CLIENT_MODEL_CLASS> client_model = this->get_model(server);
		if(client_model != NULL)
		{
			client_model->send_and_reply(req_msg,req_len,rep_msg,rep_len,
												act_rep_len,retry_interval,timeout);
		}
    }
                                                
    /**
     * @brief subscribe
     * @param:std::string server
     * @description: delete client model by server server
    */
    boost::shared_ptr<CLIENT_MODEL_CLASS> CLIENT_CONTROLLER_CLASS::get_model(std::string server)
    {
    	boost::unique_lock<boost::mutex> locker(this->m_model_create_mutex);
        boost::shared_ptr<CLIENT_MODEL_CLASS> client_model = NULL;
        if(this->m_client_map.find(server) != this->m_client_map.end())
        {
            client_model = this->m_client_map[server];
        }
        else
        {
            client_model = boost::make_shared<CLIENT_MODEL_CLASS>(server);
            this->m_client_map[server] = client_model;
        }
        return client_model;
    }

    /**
     * @brief shutdown client loop
     */
    void CLIENT_CONTROLLER_CLASS::shutdown_all()
    {
        std::for_each(this->m_client_map.begin(),this->m_client_map.end(),
        [&](auto & c)
        {
            try
            {
                auto client = c.second;
                client->shutdown();
            }
            catch(...){}
        });
    }
}
