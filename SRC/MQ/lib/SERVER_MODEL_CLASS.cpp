/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: SERVER_MODEL_CLASS.cpp
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#include "SERVER_MODEL_CLASS.h"
#include <thread>
namespace ZOO_MQ
{
    /*
     * @brief default constructor
    */
    SERVER_MODEL_CLASS::SERVER_MODEL_CLASS(IN std::string marking_code):
    m_context(1)
    ,m_local_sub(m_context,ZMQ_SUB)
    ,m_local_pub(m_context,ZMQ_PUB)
    ,m_event_handler(NULL)
    {
        this->m_marking_code = marking_code; 
    }
            
    /*
     * @brief disconstructor
    */
    SERVER_MODEL_CLASS::~SERVER_MODEL_CLASS()
    {
        
    }
    
    /*
     * @brief bind
     * @param server_name -address
     * @param type - message queue type : tcp/ipc/intipc 
     * @description : enable server service
    */
    void SERVER_MODEL_CLASS::initialize()
    {      

        ZOO_TASKS_STRUCT tasks;		
        ZOO_BROKER_STRUCT broker;
        BOOST_ASSERT_MSG(OK == ZOO_get_entity_tasks(&tasks),"Get config tasks failed");
        BOOST_ASSERT_MSG(OK == ZOO_get_brokers(&broker),"Get broker configuration failed");
        
		/**
		 * @brief change the stack size for this server,from configuration file.
		 */
		for(int t = 0; t < tasks.number; ++t)
		{
			if(this->m_marking_code.compare(tasks.task[t].task_name) == 0)
			{
				struct rlimit stack_limits;
	    		stack_limits.rlim_cur = stack_limits.rlim_max = tasks.task[t].stack_size*1024*1024;
	    		setrlimit(RLIMIT_STACK, &stack_limits);
	    		break;
			}
		}
        
		std::string ipc_file_path = ENVIRONMENT_ULTILITY_CLASS::get_instance()->get_ipc_path();
		std::string inproc_file_path  = ENVIRONMENT_ULTILITY_CLASS::get_instance()->get_inproc_path();
		this->m_local_pub_server  = ipc_file_path    + "/" + broker.local_be_send;
    	this->m_local_sub_server  = ipc_file_path    + "/" + broker.local_be_recv;
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION__,"pub server:%s,sub server:%s,hwm:%d",
    				this->m_local_pub_server.c_str(),this->m_local_sub_server.c_str(),broker.hwm);
		this->m_thread_numbers = this->get_backend_thread_number();
		if(this->m_thread_numbers > 1)
		{
            this->m_thread_pool.reset(new MQ_THREAD_POOL(this->m_thread_numbers));
            this->m_message_dispatcher.reset(new MESSAGE_DISPATCHER_CLASS(broker.hwm,ZOO_TRUE,this->m_thread_pool));
        }
        
    	this->m_local_pub.connect(this->m_local_pub_server); 
		this->m_local_pub.setsockopt(ZMQ_SNDHWM,&broker.hwm,sizeof(broker.hwm));
		this->m_local_pub.setsockopt(ZMQ_LINGER,&broker.linger,sizeof(broker.linger));		
		this->m_local_sub.connect(this->m_local_sub_server);
		this->m_local_sub.setsockopt(ZMQ_RCVHWM,&broker.hwm,sizeof(broker.hwm));
		this->m_local_sub.setsockopt(ZMQ_LINGER,&broker.linger,sizeof(broker.linger));
		this->m_local_sub.setsockopt(ZMQ_SUBSCRIBE,this->m_marking_code.data(),this->m_marking_code.size());
		
		std::this_thread::sleep_for(std::chrono::seconds(1));        
    }

    /*
     * @brief terminate
     * @param server_addr -address
     * @description : destory bind
    */
    void SERVER_MODEL_CLASS::terminate()
    {
         this->m_thread_pool->shutdown();    
    }

    /*
     * @brief register_events
     * @param callback -
     * @param timeout -
     * @param request_message_lenght - 
     * @description : destory bind
    */                    
    void SERVER_MODEL_CLASS::register_events_handler(IN MQ4A_BIND_CALLBACK_FUNCTION callback_function)
    {
        this->m_event_handler = callback_function;  
    }
    
     /*
     * @brief publish
     * @param server_name -
     * @param event_id -
     * @param request_message - 
     * @param request_message_lenght
     * @description : destory bind
    */                    
    void SERVER_MODEL_CLASS::publish(IN ZOO_INT32 event_id,
                                                IN void *message,
                                                IN ZOO_INT32 message_lenght)
    {      
        boost::unique_lock<boost::mutex> guard(this->m_publish_sync_lock);
        std::string meg_id = boost::lexical_cast<std::string>(event_id);
        this->send_message(this->m_local_pub,meg_id.data(),meg_id.size(),message,message_lenght);
    }          

	/*
     * @brief send_reply
     * @param server_addr -process name
     * @description : reply message to client
    */
    void SERVER_MODEL_CLASS::send_reply(IN ZOO_UINT32 msg_id,
    										IN void *rep_msg,
                                        	IN ZOO_INT32 rep_len)
    {      
        boost::unique_lock<boost::mutex> guard(this->m_publish_sync_lock);
        std::string id = boost::lexical_cast<std::string>(msg_id);
        this->send_message(this->m_local_pub,id.data(),id.size(),rep_msg, rep_len);
    }     
                                            
    /*
     * @brief process_events 
    */  
    void SERVER_MODEL_CLASS::loop_event()
    {
        this->enter_event_loop();
    }
 
    /*
     * @brief Sync recv message  
    */
    void SERVER_MODEL_CLASS::enter_event_loop()
    {
        zmq::pollitem_t items [] = 
        {
            { static_cast<void *>(this->m_local_sub), 0, ZMQ_POLLIN, 0 }
        }; 
        
        __MQ_EVENT_LOOP_BEGIN
        {   
            zmq::poll(&items[0], 1, MQ4A_TIMEOUT_INFINITE); 
            if(items[0].revents & ZMQ_POLLIN)
            {
				std::string mid,body;  
                this->receive_message(this->m_local_sub,mid,body);
                ZOO_UINT32 id = boost::lexical_cast<ZOO_UINT32>(mid); 
                //mutli-workers
                if(this->m_thread_numbers > 1)
                {
                    boost::shared_ptr<MESSAGE_CLASS> message = boost::make_shared<MESSAGE_CLASS>(id,body);
                    this->m_thread_pool->queue_working(
                                   boost::bind(&SERVER_MODEL_CLASS::handle_request,this,message));
                }
                //single worker 
                else
                {
                    this->handle_request(body.data(),id);
                }
            }
        }
        __MQ_EVENT_LOOP_END       
    }
    
    /*
     * @brief execute_callback 
     * @param message 
     * @param msg_id 
    */    
    void SERVER_MODEL_CLASS::handle_request(IN const void * message,ZOO_UINT32 msg_id)
    {             
        auto handler = this->m_event_handler;
		if(handler != NULL)
		{	
			handler(NULL,this->m_marking_code.data(),static_cast<void *>(message),msg_id);
		}
		else
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,__FUNCTION__,":: handler is null");
		}
    }   

    /*
     * @brief execute_callback 
     * @param message
     */ 
    void SERVER_MODEL_CLASS::handle_request(boost::shared_ptr<MESSAGE_CLASS> message)
    {
    	auto handler = this->m_event_handler;
		if(handler != NULL)
		{	
			handler(NULL,this->m_marking_code.data(),static_cast<void *>(message->data.data()),message->id);
		}
		else
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,__FUNCTION__,":: handler is null");
		}
    }
}
