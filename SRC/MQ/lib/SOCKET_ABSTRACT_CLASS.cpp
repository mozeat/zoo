/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: SOCKET_ABSTRACT_CLASS.cpp
* Description: frontend
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     weiwang.sun       		created
******************************************************************************/
#include "SOCKET_ABSTRACT_CLASS.h"
#include <mutex>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <boost/lexical_cast.hpp>
namespace ZOO_MQ
{
    /*
     * @brief default constructor
    */
    SOCKET_ABSTRACT_CLASS::SOCKET_ABSTRACT_CLASS():m_mid(1)
    {
        this->m_begin = ZOO_get_current_timestamp_usec();
    }

    /*
     * @brief disconstructor
    */
    SOCKET_ABSTRACT_CLASS::~SOCKET_ABSTRACT_CLASS()
    {

    }

	/*
     * @brief Set markiong code
     * @param marking code
    */
    void SOCKET_ABSTRACT_CLASS::set_marking_code(std::string marking_code)
    {
        this->m_marking_code = marking_code;
    }

	/*
     * @brief Get markiong code
     * @param marking code
    */
    const std::string & SOCKET_ABSTRACT_CLASS::get_marking_code()
    {
        return this->m_marking_code;
    }
    

	/*
     * @brief Parse message to int value
     * @param message
     * @param function_id
    */
    void SOCKET_ABSTRACT_CLASS::parse_event_id(zmq::message_t & message,ZOO_INT32 & function_id)
    {
        ZOO_INT32 id = 0xffffffff;
        std::istringstream is(std::string(static_cast<char*>(message.data()),message.size()));
        is >> id;
        function_id = id;
    }

    /*
     * @brief Set socket send time
     * @param socket
     * @param milliseconds
    */
	void SOCKET_ABSTRACT_CLASS::set_socket_send_timeout(IN zmq::socket_t & socket,
																	IN ZOO_INT32 milliseconds)
	{
        socket.setsockopt(ZMQ_SNDTIMEO,&milliseconds, sizeof(ZOO_INT32));
    }

	/*
     * @brief Set socket recv time
     * @param socket
     * @param milliseconds
    */
	void SOCKET_ABSTRACT_CLASS::set_socket_recv_timeout(IN zmq::socket_t & socket,
																	IN ZOO_INT32 milliseconds)
	{
        socket.setsockopt(ZMQ_RCVTIMEO,&milliseconds, sizeof(ZOO_INT32));
    }
    
    /*
     * @brief Serialize message 
     * @param message
     * @param function_id
    */
    void SOCKET_ABSTRACT_CLASS::make_pack(IN void * src, 
					    				   		IN ZOO_INT32 len,
					    				   		INOUT zmq::message_t & dest)
    {
	    //std::stringstream ss;
	    //msgpack::pack(ss,std::move(std::string(static_cast<char *>(src),len)));
	    // send the buffer ...
	    //ss.seekg(0);
	    //dest.rebuild(ss.str().size());
	    //memcpy(dest.data(),ss.str().data(),ss.str().size());
	    dest.rebuild(len);
	    memcpy(dest.data(),src,len);
    }

    /*
     * @brief Unserialize message 
     * @param message
     * @param function_id
    */
    void SOCKET_ABSTRACT_CLASS::make_unpack(IN zmq::message_t & src,
					    						IN void * dest,
					    						INOUT ZOO_INT32 * act_len)
    {
        /*std::stringstream ss(std::move(std::string(static_cast<char *>(src.data()),src.size())));
        msgpack::object_handle oh = msgpack::unpack(ss.str().data(), ss.str().size());
   		msgpack::object obj = oh.get();
   		std::string buffer;
   		obj.convert(buffer);
   		*act_len = buffer.size();
   		memcpy(dest,buffer.data(),buffer.size());*/
   		memcpy(dest,src.data(),src.size());
   		*act_len = src.size();
    	//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__,"message len:%d",act_len);
    }
    
    /*
     * @brief Unserialize message 
     * @param message
     * @param function_id
    */
    void SOCKET_ABSTRACT_CLASS::make_unpack(IN zmq::message_t & src,
			    									IN std::string & message)
    {
    	//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__,"input src message size: %d",src.size());
    	//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__,"input out message size: %d",message.size());
        /*std::stringstream ss(std::move(std::string(static_cast<char *>(src.data()),src.size())));
        msgpack::object_handle oh = msgpack::unpack(ss.str().data(),ss.str().size());
   		msgpack::object obj = oh.get();
   		message.resize(src.size());
   		obj.convert(message);*/
   		message = std::string((char *)src.data(),src.size());
   		//memcpy(dest,buffer.data(),buffer.size());
    	//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__,"converted message len:%d",message.size());
    	//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__,"< ...");
    }
    
    /*
     * @brief Send with nopack message 
     * @param message
     * @param function_id
    */
	void SOCKET_ABSTRACT_CLASS::send_raw_message(IN zmq::socket_t & socket,
														IN zmq::message_t & message,
														IN ZOO_BOOL more)
    {
    	if(more)
    	{
        	if(!socket.send(message,ZMQ_SNDMORE))
        	{
        		__THROW_MQ_EXCEPTION(MQ4A_SEND_ERR, "Send message error.", NULL);
        	}
        }
        else
        {
        	if(!socket.send(message))
        	{
        		__THROW_MQ_EXCEPTION(MQ4A_SEND_ERR, "Send message error.", NULL);
        	}
        }
    }

    /*
     * @brief Send with nopack message 
     * @param message
     * @param function_id
    */
	void SOCKET_ABSTRACT_CLASS::send_raw_message(IN zmq::socket_t & socket,
														IN std::string & message,
														IN ZOO_BOOL more)
    {
    	if(more)
    	{
        	if(0 >= socket.send(message.data(),message.size(),ZMQ_SNDMORE))
        	{
        		__THROW_MQ_EXCEPTION(MQ4A_SEND_ERR, "Send message error.", NULL);
        	}
        }
        else
        {
        	if(0 >= socket.send(message.data(),message.size()))
        	{
        		__THROW_MQ_EXCEPTION(MQ4A_SEND_ERR, "Send message error.", NULL);
        	}
        }
    }
    
	/*
     * @brief Send with nopack message 
     * @param message
     * @param function_id
    */
	void SOCKET_ABSTRACT_CLASS::send_raw_message(IN zmq::socket_t & socket,
														IN void * message,
														IN ZOO_INT32 len,
														IN ZOO_BOOL more )
    {
    	if(more)
    	{
        	if(0 >= socket.send(message,len,ZMQ_SNDMORE))
        	{
        		__THROW_MQ_EXCEPTION(MQ4A_SEND_ERR, "Send message error.", NULL);
        	}
        }
        else
        {
        	if(0 >= socket.send(message,len))
        	{
        		__THROW_MQ_EXCEPTION(MQ4A_SEND_ERR, "Send message error.", NULL);
        	}
        }
    }
    
    /*
     * @brief pack message send message 
     * @param message
     * @param more 
    */
	void SOCKET_ABSTRACT_CLASS::pack_and_send_message(IN zmq::socket_t & socket,
																IN zmq::message_t & message,
																IN ZOO_BOOL more)
    {
    	zmq::message_t req(message.size());
    	this->make_pack(message.data(),message.size(),req);
    	if(!socket.send(req,more ? ZMQ_SNDMORE:0))
    	{
    		__THROW_MQ_EXCEPTION(MQ4A_SEND_ERR, "Send message error.", NULL);
    	}
    }	

	/*
     * @brief pack message send message 
     * @param message
     * @param more 
    */
	void SOCKET_ABSTRACT_CLASS::pack_and_send_message(IN zmq::socket_t & socket,
																IN void * src, 
											    				IN ZOO_INT32 len,
																IN ZOO_BOOL more)
	{
		zmq::message_t req;
		this->make_pack(src,len,req);
    	if(!socket.send(req,more ? ZMQ_SNDMORE:0))
    	{
    		__THROW_MQ_EXCEPTION(MQ4A_SEND_ERR, "Send message error.", NULL);
    	}
	}
	
	/*
     * @brief receive message 
     * @param message
     * @param function_id
    */
	void SOCKET_ABSTRACT_CLASS::receive_and_unpack(IN zmq::socket_t & socket,
															INOUT zmq::message_t & message)
	{
		zmq::message_t reply;
		socket.recv(&reply);
		ZOO_INT32 act_len = 0;
		this->make_unpack(reply,message.data(),&act_len);
		if(0 == act_len)
		{
			__THROW_MQ_EXCEPTION(MQ4A_TIMEOUT_ERR, "Recv message timeout.", NULL);
		}
	}
	
	/*
     * @brief receive message 
     * @param message
     * @param function_id
    */
	void SOCKET_ABSTRACT_CLASS::receive_and_unpack(IN zmq::socket_t & socket,
															INOUT void * rep_msg, 
															IN ZOO_INT32 rep_len,
            												INOUT ZOO_INT32 * act_rep_len)
    {
        zmq::message_t reply;
		socket.recv(&reply);
		this->make_unpack(reply,rep_msg,act_rep_len);
		if(0 == *act_rep_len)
		{
			__THROW_MQ_EXCEPTION(MQ4A_TIMEOUT_ERR, "Recv message timeout.", NULL);
		}
    }	
    
    /*
     * @brief receive message 
     * @param message
     * @param function_id
    */
	void SOCKET_ABSTRACT_CLASS::receive_and_unpack(IN zmq::socket_t & socket,
															INOUT std::string & message)
    { 	
        zmq::message_t reply;
		socket.recv(&reply);
		this->make_unpack(reply,message);
		ZOO_INT32 act_len = reply.size();
		if(0 == act_len)
		{
			__THROW_MQ_EXCEPTION(MQ4A_SYSTEM_ERR, "Recv message err.", NULL);
		}
    }
    
    /*
     * @brief receive message 
     * @param message
     * @param function_id
    */
	void SOCKET_ABSTRACT_CLASS::receive_raw_message(IN zmq::socket_t & socket,
																INOUT void * rep_msg, 
																IN ZOO_INT32 rep_len,
							    								INOUT ZOO_INT32 * act_rep_len)
    {
        zmq::message_t reply;
		socket.recv(&reply);
    	memcpy(rep_msg,reply.data(),reply.size());
    	*act_rep_len = reply.size();
    	if(*act_rep_len == 0)
    	{
    		__THROW_MQ_EXCEPTION(MQ4A_SYSTEM_ERR, "Recv message err.", NULL);
    	}
    }	
    
    /*
     * @brief receive message 
     * @param message
     * @param function_id
    */
	void SOCKET_ABSTRACT_CLASS::receive_raw_message(IN zmq::socket_t & socket,
															INOUT zmq::message_t & message)
    {
    	if(!socket.recv(&message))
    	{
    		__THROW_MQ_EXCEPTION(MQ4A_SYSTEM_ERR, "Recv message err.", NULL);
    	}
    }

    /*
     * @brief receive message 
     * @param message
     * @param function_id
    */
	void SOCKET_ABSTRACT_CLASS::receive_raw_message(IN zmq::socket_t & socket,
															INOUT std::string & message)
    {
    	//ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION,__FUNCTION__," > ...");
        zmq::message_t reply;
		socket.recv(&reply);
    	//ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION,__FUNCTION__,"recv msg len:%d",reply.size());
		if(reply.size() == 0)
    	{
    		__THROW_MQ_EXCEPTION(MQ4A_SYSTEM_ERR, "Recv message err.", NULL);
    	}
    	else
    	{
    		message.resize(reply.size());
    		memcpy((void *)message.data(),reply.data(),reply.size());
    	}
    	//ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION,__FUNCTION__," < ...");
    }
    
    /*
     * @brief receive message 
     * @param socket
     * @param mid
     * @param data
     * @param function_id
    */
    void SOCKET_ABSTRACT_CLASS::send_message(IN zmq::socket_t & socket,
											    IN const void * mid,std::size_t mid_len,
											    IN const void * data,std::size_t data_len)
    {
        socket.send(this->m_marking_code.data(),this->m_marking_code.size(),ZMQ_SNDMORE);
        socket.send(mid,mid_len,ZMQ_SNDMORE);
        socket.send(data,data_len);
    }

    /*
     * @brief receive message 
     * @param socket
     * @param mid
     * @param data
     * @param function_id
    */
	void SOCKET_ABSTRACT_CLASS::receive_message(IN zmq::socket_t & socket,
									                            INOUT std::string & mid,
									                            INOUT std::string & data)
    {
        zmq::message_t server;
        zmq::message_t id;
        zmq::message_t body;   
        socket.recv(&server);
        socket.recv(&id);
        socket.recv(&body);
        if(body.more())
        {
            zmq::message_t dirty;
            socket.recv(&dirty);
            while(dirty.more())
            {
                socket.recv(&dirty);
            }
        }
        mid.resize(id.size());
        memcpy((void *)mid.data(),id.data(),id.size());
        data.resize(body.size());
        memcpy((void *)data.data(),body.data(),body.size());
    }

    ZOO_INT32 SOCKET_ABSTRACT_CLASS::get_frontend_thread_number()
    {
        ZOO_INT32 thread_num = 2;
        ZOO_TASKS_STRUCT entity_task;
		if(OK == ZOO_get_entity_tasks(&entity_task))
		{
    		for(int i = 0;i < entity_task.number; i ++)
    		{
        		if(strcmp(entity_task.task[i].task_name,program_invocation_short_name) == 0)
        		{		
        		    if(entity_task.task[i].fe_thread_number < 2)
        		    {   
        		        entity_task.task[i].fe_thread_number = 2;
        		    }

        		    ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION,
        		                                    __FUNCTION__,
        		                                    "fe_thread_number:%d",
        		                                    entity_task.task[i].fe_thread_number);
        		    break;                               
        		}
    		}
		}
		return thread_num;
    }
    
    ZOO_INT32 SOCKET_ABSTRACT_CLASS::get_backend_thread_number()
    {
        ZOO_INT32 thread_num = 0;
        ZOO_TASKS_STRUCT entity_task;
		if(OK == ZOO_get_entity_tasks(&entity_task))
		{
    		for(int i = 0;i < entity_task.number; i ++)
    		{
        		if(strcmp(entity_task.task[i].task_name,program_invocation_short_name) == 0)
        		{		
        		    ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION,
        		                                    __FUNCTION__,
        		                                    "be_thread_number:%d",
        		                                    entity_task.task[i].be_thread_number);
        		    break;                               
        		}
    		}
		}
		return thread_num;
    }   
    
    ZOO_UINT32 SOCKET_ABSTRACT_CLASS::generate_mid()
    {
        if(this->m_mid >= std::numeric_limits<ZOO_UINT32>::max())
        {
            this->m_mid = 0;
        }
        else
        {
            this->m_mid ++;
        }
        return this->m_mid;
    }

    /*
     * @brief generate message id
    */
    ZOO_UINT64 SOCKET_ABSTRACT_CLASS::generate_mid(const std::vector<ZOO_UINT32> & exclude_conditions)
    {
        ZOO_UINT32 mid = this->generate_mid();
        for (ZOO_UINT32 i = 0;i < exclude_conditions.size(); ++i)
        {
            auto pos = std::find(std::begin(exclude_conditions), std::end(exclude_conditions),mid);
            if(pos != exclude_conditions.end())
            {
                mid = this->generate_mid();
            }
            else
            {
                break;
            }
        }
        return mid;
    }
}
