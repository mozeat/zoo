/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: MQ_THREAD_POOL.cpp
* Description: broker model class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     weiwang.sun     		created
******************************************************************************/
#include "MQ_THREAD_POOL.h"

namespace ZOO_MQ
{   
    /*
     * @brief constructor
     */
    MQ_THREAD_POOL::MQ_THREAD_POOL(ZOO_INT32 thread_numbers)
    :m_ioc(thread_numbers),m_idle_worker(m_ioc)
    {
        this->create_threads(thread_numbers);
    }

    /*
     * @brief disconstructor
     */
    MQ_THREAD_POOL::~MQ_THREAD_POOL()
    {
        this->shutdown();
    }

    /*
     * @brief shutdown 
     */
    void MQ_THREAD_POOL::shutdown()
    {
        if(!this->m_ioc.stopped())
        {
            this->m_ioc.stop();
            try
            {
                this->m_thread_pool.join_all();
            }
            catch(...)
            {
                
            }
        }
    }

    /*
     * @brief create threads
     * @brief thread_numbers 
     */
    void MQ_THREAD_POOL::create_threads(ZOO_INT32 thread_numbers)
    {   
        for(ZOO_INT32 i = 0;i < thread_numbers; i++)
            this->m_thread_pool.create_thread(boost::bind(&MQ_THREAD_POOL::initialize_thread,this));
    }
    
    /*
     * @brief create threads 
     */
    void MQ_THREAD_POOL::initialize_thread()
    {
        this->m_ioc.run();
    }
}
