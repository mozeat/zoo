/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: SERVER_FACADE_FLOW_CLASS.cpp
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#include "SERVER_FACADE_FLOW_CLASS.h"
namespace ZOO_MQ
{
    /**
     * @brief default constructor
    */
    SERVER_FACADE_FLOW_CLASS::SERVER_FACADE_FLOW_CLASS()
	{
		boost::shared_ptr<SERVER_CONTROLLER_CLASS> server_controller;
		server_controller.reset(new SERVER_CONTROLLER_CLASS);
		this->set_server_controller(server_controller);
	}

    /**
     * @brief Destructor
    */
     SERVER_FACADE_FLOW_CLASS::~SERVER_FACADE_FLOW_CLASS()
	{
		
	}
	 
    /**
     * @brief 初始化服务端服务
     * @param server 服务器地址
     * @param mode  服务端工作模式
    */
    ZOO_INT32 SERVER_FACADE_FLOW_CLASS::initialize(IN const std::string & server)
	{
		__MQ_TRY
		{
			this->m_server_controller->initialize(server);
		}
		__MQ_CATCH
		{
			return ex.get_error_code();
		}
		return OK;
	}
	
    /**
     * @brief 关闭服务端服务
     * @param server 服务器地址
    */
    ZOO_INT32 SERVER_FACADE_FLOW_CLASS::terminate(IN const std::string & server)
	{
		__MQ_TRY
		{
			this->m_server_controller->terminate(server);
		}
		__MQ_CATCH
		{
			return ex.get_error_code();
		}
		return OK;
	}

    /**
     * @brief 服务端回答客户端请求
     * @param server 服务器地址
     * @param rep_msg 请求消息
     * @param rep_len 请求消息长度
     * @return 错误码
    */
    ZOO_INT32 SERVER_FACADE_FLOW_CLASS::send_reply(IN const std::string & server,
                                        IN ZOO_UINT32 msg_id,
                                        IN void * rep_msg,
                                        IN ZOO_INT32 rep_len)
	{
		__MQ_TRY
		{
			this->m_server_controller->send_reply(server,msg_id,rep_msg,rep_len);
		}
		__MQ_CATCH
		{
			return ex.get_error_code();
		}
		return OK;
	}

    /**
     *@brief 服务端发布消息客户端
     * @param server 服务器地址
     * @param event_id 事件ID
     * @param request_message 请求消息
     * @param request_message_lenght 请求消息长度
     * @return 错误码
    */
    ZOO_INT32 SERVER_FACADE_FLOW_CLASS::publish(IN const std::string &  server,
				                                        IN ZOO_INT32 event_id,
				                                        IN void *request_message,
				                                        IN ZOO_INT32 request_message_lenght)
	{
		__MQ_TRY
		{
			this->m_server_controller->publish(server,
												event_id,
												request_message,
												request_message_lenght);
		}
		__MQ_CATCH
		{
			return ex.get_error_code();
		}
		return OK;
	}
				
   /**
     *@brief 服务端注册处理事件回调函数
     *@param server 服务器地址
     *@param callback_function 处理事件回调函数
     *@return 错误码
    */
    ZOO_INT32 SERVER_FACADE_FLOW_CLASS::register_event_handler(IN const std::string &  server_name,
    											IN MQ4A_BIND_CALLBACK_FUNCTION callback_function)
	{
		__MQ_TRY
		{
			this->m_server_controller->register_events_handler(server_name,callback_function);
		}
		__MQ_CATCH
		{
			return ex.get_error_code();
		}
		return OK;
	}

    /**
     * @brief 服务端执行事件监听，执行将会阻塞调用方
     * @param server 服务器地址
    */
    ZOO_INT32 SERVER_FACADE_FLOW_CLASS::enter_event_loop(IN const std::string  & server)
	{
		__MQ_TRY
		{
			this->m_server_controller->loop_event(server);
		}
		__MQ_CATCH
		{
			return ex.get_error_code();
		}
		return OK;
	}
}
