/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: ENVIRONMENT_ULTILITY_CLASS.cpp
* Description: 
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#include "ENVIRONMENT_ULTILITY_CLASS.h"
#include <iostream>
#include <boost/filesystem.hpp>
namespace ZOO_MQ
{
    boost::shared_ptr<ENVIRONMENT_ULTILITY_CLASS>  ENVIRONMENT_ULTILITY_CLASS::m_instance;

    ENVIRONMENT_ULTILITY_CLASS::ENVIRONMENT_ULTILITY_CLASS():m_call_one_flag(BOOST_ONCE_INIT)
    {
        //ctor
    }

    ENVIRONMENT_ULTILITY_CLASS::~ENVIRONMENT_ULTILITY_CLASS()
    {
        //dtor
    }

    boost::shared_ptr<ENVIRONMENT_ULTILITY_CLASS>  ENVIRONMENT_ULTILITY_CLASS::get_instance()
    {
        if(ENVIRONMENT_ULTILITY_CLASS::m_instance == nullptr)
        {
            ENVIRONMENT_ULTILITY_CLASS::m_instance.reset(new ENVIRONMENT_ULTILITY_CLASS());
            ENVIRONMENT_ULTILITY_CLASS::m_instance->initialize();
        }
        return ENVIRONMENT_ULTILITY_CLASS::m_instance;
    }

    void ENVIRONMENT_ULTILITY_CLASS::initialize()
    {
        if (getenv("HOME") == NULL)
        {
            this->m_home_path = getpwuid(getuid())->pw_dir;
        }
        else
        {
            this->m_home_path = getenv("HOME");
        }

        std::string pipe_path     = this->m_home_path + "/pipe";
        std::string ipc_header    = "ipc://";
        std::string inproc_header = "inproc://";
        this->m_ipc_path = ipc_header + pipe_path;
        if(!boost::filesystem::exists(pipe_path))
        {
            boost::filesystem::create_directory(boost::filesystem::path(pipe_path));
        }
        this->m_inproc_path = inproc_header;
    }

    std::string ENVIRONMENT_ULTILITY_CLASS::get_home_path()
    {
        return this->m_home_path;
    }

    /**
     * @breif get ipc file store path
    */
    std::string ENVIRONMENT_ULTILITY_CLASS::get_ipc_path()
    {
        return this->m_ipc_path;
    }

    /**
     * @breif get inproc file store path
    */
    std::string ENVIRONMENT_ULTILITY_CLASS::get_inproc_path()
    {
        return this->m_inproc_path;
    }
}
