/*******************************************************************************
* Copyright (C) 2017, 上海蔚来汽车有限公司
* All rights reserved.
* 产品�?:
* 所属组�?:
* 模块名称 :
* 文件名称 : ZOO_MQ_if.h
* 概要描述 : message queue
* 历史记录 :
* 版本      日期        作�?   内容
* 1.0    2017-12-21     weiwang.sun   新建
******************************************************************************/
#include <MQ4A_if.h>
#include <memory>
#include "SERVER_FACADE_FLOW_CLASS.h"
#include "CLIENT_FACADE_FLOW_CLASS.h"
#include "SERVER_CONTROLLER_CLASS.h"

/*
*@brief facade flow
*/
ZOO_MQ::SERVER_FACADE_FLOW_CLASS * g_server_facade_flow = new ZOO_MQ::SERVER_FACADE_FLOW_CLASS();

/*
*@brief facade flow
*/
ZOO_MQ::CLIENT_FACADE_FLOW_CLASS * g_client_facade_flow = new ZOO_MQ::CLIENT_FACADE_FLOW_CLASS();

/**
 * @brief 初始化服务端服务
 * @param server 服务器地址
 * @param mode  服务端工作模式
*/
ZOO_EXPORT ZOO_INT32 MQ4A_server_initialize(IN const MQ4A_SERV_ADDR server)
{
    return g_server_facade_flow->initialize(server);
}

/**
 * @brief 关闭服务端服务
 * @param server 服务器地址
*/
ZOO_EXPORT ZOO_INT32 MQ4A_server_terminate(IN const MQ4A_SERV_ADDR server)
{
    return g_server_facade_flow->terminate(server);
}

/**
 *@brief 服务端发布消息客户端
 * @param server 服务器地址
 * @param event_id 事件ID
 * @param request_message 请求消息
 * @param request_message_lenght 请求消息长度
 * @return 错误码
*/
ZOO_EXPORT ZOO_INT32 MQ4A_publish(IN const MQ4A_SERV_ADDR server,
		                                         IN ZOO_INT32 event_id,
		                                         IN void *req_msg,
		                                         IN int req_len)
{
    return g_server_facade_flow->publish(server,event_id,req_msg,req_len);
}		                                         

/**
 * @brief 客户端发送异步请求
 * @param server 服务器地址
 * @param req_msg 请求消息
 * @param rep_len 请求消息长度
 * @param retry_interval 重试次数
 * @return 错误码
*/
ZOO_EXPORT ZOO_INT32 MQ4A_send_request(IN const MQ4A_SERV_ADDR server,
                                                IN void *req_msg,
                                                IN ZOO_INT32 req_len,IN ZOO_INT32 retry_interval)
{
    return g_client_facade_flow->send_request(server,req_msg,req_len,retry_interval,ZOO_FALSE);
}

/**
 * @brief 客户端发送异步请求
 * @param server 服务器地址
 * @param req_msg 请求消息
 * @param rep_len 请求消息长度
 * @param retry_interval 重试次数
 * @return 错误码
*/

ZOO_EXPORT ZOO_INT32 MQ4A_send_signal(IN const MQ4A_SERV_ADDR server,
                                                        IN void *req_msg,
                                                        IN ZOO_INT32 req_len,
                                                        IN ZOO_INT32 retry_interval)
{
    return g_client_facade_flow->send_request(server,req_msg,req_len,retry_interval,ZOO_TRUE);
}

/**
 * @brief 客户端发送异步请求及客户端获取回答
 * @param server 服务器地址
 * @param req_msg 请求消息
 * @param rep_len 请求消息长度
 * @param act_rep_len 实际收到的字节长度
 * @param timeout  超时时间
 * @return 错误码
*/
ZOO_EXPORT ZOO_INT32 MQ4A_send_reply(IN const MQ4A_SERV_ADDR server,
												IN ZOO_UINT32 msg_id,
                                                IN void *rep_msg,
                                                IN ZOO_INT32 rep_len)
{
    return g_server_facade_flow->send_reply(server,msg_id,rep_msg,rep_len);
}

/**
 * @brief 客户端发送异步请求及客户端获取回答
 * @param server 服务器地址
 * @param req_msg 请求消息
 * @param rep_len 请求消息长度
 * @param act_rep_len 实际收到的字节长度
 * @param timeout  超时时间
 * @return 错误码
*/
ZOO_EXPORT ZOO_INT32 MQ4A_send_request_and_receive_reply(IN const MQ4A_SERV_ADDR server,
			                                                		IN void *req_msg,
			                                                		IN ZOO_INT32 req_len,
					                                                IN void *rep_msg,
					                                                IN ZOO_INT32 rep_len,
					                                                IN ZOO_INT32 *act_rep_len,
			                                                		IN ZOO_INT32 retry_interval,
					                                                IN ZOO_INT32 timeout)
{
    return g_client_facade_flow->send_and_reply(server,
    								req_msg,
    								req_len,rep_msg,rep_len,act_rep_len,
    								retry_interval,timeout);
}

/**
 * @brief 客户端获取回答
 * @param server 服务器地址
 * @param req_msg 请求消息
 * @param rep_len 请求消息长度
 * @param act_rep_len 实际收到的字节长度
 * @param timeout  超时时间
 * @return 错误码
*/
ZOO_EXPORT ZOO_INT32 MQ4A_receive_reply(IN const MQ4A_SERV_ADDR server,
                                                IN void *rep_msg,
                                                IN ZOO_INT32 rep_len,
                                                IN ZOO_INT32 *act_rep_len,
                                                IN ZOO_INT32 timeout)
{
    return g_client_facade_flow->get_reply(server,rep_msg,rep_len,act_rep_len,timeout);
}

/**
 * @brief 客户端订阅接口
 * @param server 服务器地址
 * @param callback_function 注册消息回调
 * @param callback_struct   请求消息长度
 * @param event_id          事件ID
 * @param handle            注册事件返回唯一ID 
 * @param context           上下文传递参数（一般NULL）
 * @return 错误码
*/
ZOO_EXPORT ZOO_INT32 MQ4A_subscribe(IN const MQ4A_SERV_ADDR server,
                                IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
                                IN MQ4A_CALLBACK_STRUCT * callback_struct,
                                IN ZOO_INT32 event_id,
                                INOUT ZOO_HANDLE *handle,
                                INOUT void* context)
{
    return g_client_facade_flow->subscribe(server,callback_function,callback_struct,event_id,handle,context);
}

/**
 * @brief 客户端取消订阅
 * @param server 服务器地址
 * @param event_id          事件ID
 * @param handle            注册事件返回唯一ID 
 * @return 错误码
*/
ZOO_EXPORT ZOO_INT32 MQ4A_unsubscribe(IN const MQ4A_SERV_ADDR server,IN ZOO_INT32 event_id,IN ZOO_HANDLE handle)
{
    return g_client_facade_flow->unsubscribe( server,event_id,handle);
}

/**
 *@brief 服务端注册处理事件回调函数
 *@param server 服务器地址
 *@param callback_function 处理事件回调函数
 *@return 错误码
*/
ZOO_EXPORT ZOO_INT32 MQ4A_register_event_handler(IN const MQ4A_SERV_ADDR server,IN MQ4A_BIND_CALLBACK_FUNCTION callback_function)
{
    return g_server_facade_flow->register_event_handler(server,callback_function);
}

/**
* @brief 服务端执行事件监听
* @param server 服务器地址
*/
ZOO_EXPORT ZOO_INT32 MQ4A_enter_event_loop( IN const MQ4A_SERV_ADDR server)
{
    return g_server_facade_flow->enter_event_loop(server);
}

/**
 *@brief 关闭客户端所有背景线程，例如订阅背景线程
*/
ZOO_EXPORT ZOO_INT32 MQ4A_shutdown_all_clients()
{
    return g_client_facade_flow->shutdown_all_clients();
}
