/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: BROKER_CONFIGURATION_CLASS.cpp
* Description: broker config class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#include "ENVIRONMENT_ULTILITY_CLASS.h"
#include "BROKER_CONFIGURATION_CLASS.h"

namespace ZOO_MQ {

    BROKER_CONFIGURATION_CLASS::BROKER_CONFIGURATION_CLASS()
    {
		this->m_broker_conf.hwm = 10000;
		this->m_broker_conf.linger = 0;
    }

    BROKER_CONFIGURATION_CLASS::~BROKER_CONFIGURATION_CLASS()
    {

    }

    void BROKER_CONFIGURATION_CLASS::load_configure()
    {
    	if(OK == ZOO_get_brokers(&this->m_broker_conf))
    	{
	        this->m_ipc_path = ENVIRONMENT_ULTILITY_CLASS::get_instance()->get_ipc_path();
	        this->m_ipc_path += "/";
        }
        else
        {
        	ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__FUNCTION__,"get broker cfg data failed.");
        }
    }

    ZOO_UINT32 BROKER_CONFIGURATION_CLASS::get_hwm() const
    {
        return this->m_broker_conf.hwm * 1.5;
    }

    ZOO_UINT32 BROKER_CONFIGURATION_CLASS::get_linger() const
    {
        return this->m_broker_conf.linger;
    }

    std::string BROKER_CONFIGURATION_CLASS::get_local_fe_send() const
    {
        return this->m_broker_conf.local_fe_send;
    }

    std::string BROKER_CONFIGURATION_CLASS::get_local_fe_recv() const
    {
        return this->m_broker_conf.local_fe_recv;
    }

    std::string BROKER_CONFIGURATION_CLASS::get_local_be_send() const
    {
        return this->m_broker_conf.local_be_send;
    }

    std::string BROKER_CONFIGURATION_CLASS::get_local_be_recv() const
    {
        return this->m_broker_conf.local_be_recv;
    }

    std::string BROKER_CONFIGURATION_CLASS::get_remote_frontend() const
    {
        return this->m_broker_conf.remote_frontend;
    }

    std::string BROKER_CONFIGURATION_CLASS::get_remote_backend() const
    {
        return this->m_broker_conf.remote_backend;
    }

    std::string BROKER_CONFIGURATION_CLASS::get_ipc_path() const
    {
        return this->m_ipc_path;
    }

}//ZOO_MQ
