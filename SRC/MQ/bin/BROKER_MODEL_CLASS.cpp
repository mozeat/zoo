/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: BROKER_MODEL_CLASS.cpp
* Description: broker model class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#include "BROKER_MODEL_CLASS.h"

namespace ZOO_MQ {

    BROKER_MODEL_CLASS::BROKER_MODEL_CLASS(FRONTEND_TYPE type)
        : m_context(1),
          m_type(type)
    {
        this->m_run_flag = true;
        this->m_backends.clear();
        this->m_hwm = 1000;
        this->m_linger = 0;
    }

    BROKER_MODEL_CLASS::~BROKER_MODEL_CLASS()
    {
        this->stop();
    }

    void BROKER_MODEL_CLASS::set_frontend_address(std::string &adress)
    {
        this->m_frontend_adress = adress;
    }

    void BROKER_MODEL_CLASS::set_backend_address(std::vector<std::string> &addresses)
    {
        this->m_backend_addresses = addresses;
    }

    void BROKER_MODEL_CLASS::register_backend(std::vector<std::shared_ptr<BROKER_BASE_CLASS> > backends)
    {
        this->m_backends = backends;
    }

    void BROKER_MODEL_CLASS::set_hwm(ZOO_UINT32 hwm)
    {
        this->m_hwm = hwm;
    }

    void BROKER_MODEL_CLASS::set_linger(ZOO_UINT32 linger)
    {
        this->m_linger = linger;
    }

    void BROKER_MODEL_CLASS::set_ipc_path(std::string path)
    {
        this->m_ipc_path = path;
    }

    void BROKER_MODEL_CLASS::create_frontend_backend()
    {        
        create_frontend();
        create_backend();
    }

    void BROKER_MODEL_CLASS::run()
    {
        while (this->m_run_flag)
        {
            zmq::poll(this->m_poll_items);

            if (this->m_poll_items[0].revents & ZMQ_POLLIN)
            {
                while (this->m_run_flag)
                {
                    this->m_frontend->recv();
                    int more = this->m_frontend->get_more();
                    for (auto backend : this->m_backends)
                    {
                    	
                    	std::string msg((char *)this->m_frontend->get_message().data(),this->m_frontend->get_message().size());				
                        //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,
                    	//				__FUNCTION__,
                    	//				"send size:%d,%s",
                    	//				this->m_frontend->get_message().size(),msg.data());
                    	backend->send(this->m_frontend->get_message(),more ? ZMQ_SNDMORE : 0);
                    }

                    if (!more)
                    {
                        break;
                    }
                }
            }
        }
    }

    void BROKER_MODEL_CLASS::stop()
    {
        this->m_run_flag = false;
        if (this->m_frontend)
        {
            this->m_frontend->close();
        }

        for (auto backend : this->m_backends)
        {
            backend->close();
            backend.reset();
        }

        this->m_frontend.reset();
        this->m_backends.clear();
    }

    void BROKER_MODEL_CLASS::create_frontend()
    {
        if (m_type == FRONTEND_TYPE::RECV_REMOTE_TYPE)
        {
            this->create_frontend(this->m_frontend_adress, false);
        }
        else
        {
            this->create_frontend(this->m_ipc_path + this->m_frontend_adress);
        }
    }

    void BROKER_MODEL_CLASS::create_backend()
    {
        int index = -1;
        for (auto address : this->m_backend_addresses)
        {
            ++index;
            if (address.empty())
            {
                continue;
            }

            if (index == 0)
            {
                this->create_backend(this->m_ipc_path + address);
            }
            else
            {
                this->create_backend(address, false);
            }
        }
    }

    void BROKER_MODEL_CLASS::create_frontend(std::string addr, ZOO_BOOL bind)
    {
        this->m_frontend = std::make_shared<FRONTEND_CLASS>(this->m_context, addr);
        this->m_frontend->set_hwm(this->m_hwm);
        this->m_frontend->set_linger(this->m_linger);
        zmq::socket_t &socket = this->m_frontend->get_socket();
        this->add_poll_item(socket);
        if (bind)
        {
            this->m_frontend->bind();
        }
        else
        {
            this->m_frontend->connect();
        }        
    }

    void BROKER_MODEL_CLASS::create_backend(std::string addr, ZOO_BOOL bind)
    {
        std::shared_ptr<BROKER_BASE_CLASS> backend(new BACKEND_CLASS(this->m_context, addr));
        backend->set_hwm(this->m_hwm);
        backend->set_linger(this->m_linger);
        if (bind)
        {
            backend->bind();
        }
        else
        {
            backend->connect();
        }

        this->m_backends.push_back(backend);
    }

    void BROKER_MODEL_CLASS::add_poll_item(zmq::socket_t &socket)
    {
        zmq::pollitem_t items[] = {{static_cast<void *>(socket), 0, ZMQ_POLLIN, 0}};
        this->m_poll_items.push_back(items[0]);
    }

    std::shared_ptr<BROKER_BASE_CLASS> &BROKER_MODEL_CLASS::get_frontend()
    {
        return this->m_frontend;
    }

    std::vector<std::shared_ptr<BROKER_BASE_CLASS>> &BROKER_MODEL_CLASS::get_backend()
    {
        return this->m_backends;
    }

}//ZOO_MQ
