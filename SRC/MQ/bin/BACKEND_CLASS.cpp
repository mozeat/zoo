/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: BACKEND_CLASS.cpp
* Description: broker publish class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#include "BACKEND_CLASS.h"

namespace ZOO_MQ {

    BACKEND_CLASS::BACKEND_CLASS(zmq::context_t &context, std::string addr)
        : BROKER_BASE_CLASS(context, addr, ZMQ_PUB)
    {

    }

    BACKEND_CLASS::~BACKEND_CLASS()
    {

    }


}//ZOO_MQ
