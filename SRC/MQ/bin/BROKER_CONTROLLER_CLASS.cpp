/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: BROKER_CONTROLLER_CLASS.cpp
* Description: broker control class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#include "BROKER_CONTROLLER_CLASS.h"

namespace ZOO_MQ
{

    BROKER_CONTROLLER_CLASS::BROKER_CONTROLLER_CLASS()
        : m_broker_conf(new BROKER_CONFIGURATION_CLASS())
    {
    	
    }

    BROKER_CONTROLLER_CLASS::~BROKER_CONTROLLER_CLASS()
    {
        this->stop();
    }

    void BROKER_CONTROLLER_CLASS::create_all_broker_models()
    {
        this->create_broker_models(FRONTEND_TYPE::RECV_LOCAL_TYPE);
        this->create_broker_models(FRONTEND_TYPE::RECV_LOCAL_SERVER_TYPE);
        this->create_broker_models(FRONTEND_TYPE::RECV_REMOTE_TYPE);
    }

    void BROKER_CONTROLLER_CLASS::load_configuration()
    {        
        this->m_broker_conf->load_configure();
    }

    void BROKER_CONTROLLER_CLASS::run()
    {
        std::map<FRONTEND_TYPE, std::shared_ptr<BROKER_MODEL_CLASS>>::iterator iter = this->m_broker_models.begin();
        while (iter != this->m_broker_models.end())
        {
            std::shared_ptr<BROKER_MODEL_CLASS> model = iter->second;
            if (nullptr == model)
            {
                continue;
            }

            this->m_group_thread.create_thread([=](){
                model->run();
            });

            ++iter;
        }

        this->m_group_thread.join_all();
    }

    void BROKER_CONTROLLER_CLASS::stop()
    {
        std::map<FRONTEND_TYPE, std::shared_ptr<BROKER_MODEL_CLASS>>::iterator iter = this->m_broker_models.begin();
        while (iter != this->m_broker_models.end())
        {
            std::shared_ptr<BROKER_MODEL_CLASS> model = iter->second;
            if (nullptr == model)
            {
                continue;
            }

            this->m_group_thread.create_thread([=](){
                model->stop();
            });

            ++iter;
        }
        this->m_group_thread.join_all();
    }

    void BROKER_CONTROLLER_CLASS::create_broker_models(FRONTEND_TYPE type)
    {
        std::shared_ptr<BROKER_MODEL_CLASS> model = find_model(type);
        if (nullptr == model)
        {
            model = std::make_shared<BROKER_MODEL_CLASS>(type);
            this->m_broker_models[type] = model;
        }

        std::string frontend_adress;
        std::vector<std::string> backend_adresses;
        switch (type)
        {
        case FRONTEND_TYPE::RECV_LOCAL_TYPE:

            frontend_adress = this->m_broker_conf->get_local_fe_send();
            backend_adresses.push_back(this->m_broker_conf->get_local_be_recv());
            backend_adresses.push_back(this->m_broker_conf->get_remote_backend());

            break;
        case FRONTEND_TYPE::RECV_LOCAL_SERVER_TYPE:

            frontend_adress = this->m_broker_conf->get_local_be_send();
            backend_adresses.push_back(this->m_broker_conf->get_local_fe_recv());

            break;
        case FRONTEND_TYPE::RECV_REMOTE_TYPE:
        {
            frontend_adress = this->m_broker_conf->get_remote_frontend();
            std::shared_ptr<BROKER_MODEL_CLASS> local_model = find_model(FRONTEND_TYPE::RECV_LOCAL_SERVER_TYPE);
            if (nullptr != local_model)
            {
                model->register_backend(local_model->get_backend());
            }
        }
            break;
        }

        model->set_ipc_path(this->m_broker_conf->get_ipc_path());
        model->set_hwm(this->m_broker_conf->get_hwm());
        model->set_linger(this->m_broker_conf->get_linger());
        model->set_frontend_address(frontend_adress);
        model->set_backend_address(backend_adresses);
        model->create_frontend_backend();
    }

    std::shared_ptr<BROKER_MODEL_CLASS> BROKER_CONTROLLER_CLASS::find_model(FRONTEND_TYPE type)
    {
        std::map<FRONTEND_TYPE, std::shared_ptr<BROKER_MODEL_CLASS>>::iterator iter;
        iter = this->m_broker_models.find(type);
        if (iter != this->m_broker_models.end())
        {
            return iter->second;
        }

        std::shared_ptr<BROKER_MODEL_CLASS> model;
        return model;
    }

}//ZOO_MQ
