/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: MQMA_main.cpp
* Description: broker application main
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#include "ZOO_if.hpp"
#include <string>
#include "MQ4A_type.h"
#include <iostream>
#include "MM4A_if.h"

#include "BROKER_FLOW_FACADE_CLASS.h"
#include "BROKER_CONTROLLER_CLASS.h"

std::shared_ptr<ZOO_MQ::BROKER_FLOW_FACADE_CLASS> g_flow;
std::shared_ptr<ZOO_MQ::BROKER_CONTROLLER_CLASS> g_broker_controller;
ZOO_INT32 main(int argv,char *argc[])
{
    try 
    {
        g_flow.reset(new ZOO_MQ::BROKER_FLOW_FACADE_CLASS());
        g_broker_controller.reset(new ZOO_MQ::BROKER_CONTROLLER_CLASS);
        g_flow->set_broker_controller(g_broker_controller);
        g_flow->load_configuration();
        g_flow->create_all_broker_models();
        g_flow->run();
    }
    catch(...)
    {
        __ZOO_LOG_ERROR(":: unhandle exception ...")
    }
    return OK;
}
