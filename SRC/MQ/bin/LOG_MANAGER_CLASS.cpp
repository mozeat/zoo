/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : LOG_MANAGER_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "LOG_MANAGER_CLASS.h"
#include "ENVIRONMENT_ULTILITY_CLASS.h"

namespace ZOO_MQ
{
	boost::shared_ptr<LOG_MANAGER_CLASS> LOG_MANAGER_CLASS::m_instance = NULL;
	ZOO_BOOL ENABLE_DEBUG_MODE = ZOO_FALSE;
    LOG_MANAGER_CLASS::LOG_MANAGER_CLASS()
    {
        //ctor
    }

    LOG_MANAGER_CLASS::~LOG_MANAGER_CLASS()
    {
        //dtor
    }
    
    /**
     * @brief Get instance 
    **/
	boost::shared_ptr<LOG_MANAGER_CLASS> LOG_MANAGER_CLASS::get_instance()
    {
        if(LOG_MANAGER_CLASS::m_instance == NULL)
        {
        	LOG_MANAGER_CLASS::m_instance.reset(new LOG_MANAGER_CLASS());
        }
        return LOG_MANAGER_CLASS::m_instance;
    }

    /**
     * @brief Initialize log 
    **/
    void LOG_MANAGER_CLASS::initialize()
    {

    }

	/**
     * @brief print log to file
     * @param resource_id
     * @param level
     * @param format_spec
    **/
    void LOG_MANAGER_CLASS::log(IN const char* resource_id,
									IN int level,
									IN const char* format_spec,...)
    {
        char buffer[256];
        va_list args;
    	va_start(args, format_spec);
   		// print out remainder of message
    	vsprintf(buffer, format_spec, args);
    	va_end(args);
    	buffer[255] = '\0';
        std::ostringstream os;
		os << "[" << resource_id << "]" <<buffer;
		ZOO_slog((ZOO_SEVERITY_LEVEL_NORMAL),"",buffer);
    }

    /**
     * @brief debug info to console
     * @param function_name
     * @param format_spec
    **/
    void LOG_MANAGER_CLASS::debug(const char* function_name,const char* format, ...)
    {
        if(ENABLE_DEBUG_MODE)
        {
            std::string var_str;
            va_list	ap;
            va_start(ap, format);
            int len = vsnprintf(NULL,0,format, ap);
            if (len > 0)
            {
                std::vector<char> buf(len + 1);
                vsprintf(&buf.front(), format, ap);
                var_str.assign(buf.begin(), buf.end() - 1);
            }
            va_end(ap);
			ZOO_slog((ZOO_SEVERITY_LEVEL_NORMAL),function_name,var_str.c_str());
        }
    }
   
}
