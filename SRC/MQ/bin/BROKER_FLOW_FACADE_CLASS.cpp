/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: BROKER_FLOW_FACADE_CLASS.cpp
* Description: broker flosw facade class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#include "BROKER_FLOW_FACADE_CLASS.h"
#include "LOG_MANAGER_CLASS.h"

namespace ZOO_MQ {
    BROKER_FLOW_FACADE_CLASS::BROKER_FLOW_FACADE_CLASS()
    {
		LOG_MANAGER_CLASS::get_instance()->initialize();
    }

    BROKER_FLOW_FACADE_CLASS::~BROKER_FLOW_FACADE_CLASS()
    {
    }

    void BROKER_FLOW_FACADE_CLASS::set_broker_controller(const std::shared_ptr<BROKER_CONTROLLER_CLASS> &controller)
    {
        this->m_controller = controller;
    }

    void BROKER_FLOW_FACADE_CLASS::create_all_broker_models()
    {
        if (this->m_controller)
        {
            this->m_controller->create_all_broker_models();
        }
    }

    void BROKER_FLOW_FACADE_CLASS::load_configuration()
    {
        if (this->m_controller)
        {
            this->m_controller->load_configuration();
        }
    }

    void BROKER_FLOW_FACADE_CLASS::run()
    {
        if (this->m_controller)
        {
            this->m_controller->run();
        }
    }

    void BROKER_FLOW_FACADE_CLASS::stop()
    {
        if (this->m_controller)
        {
            this->m_controller->stop();
        }
    }

}//ZOO_MQ
