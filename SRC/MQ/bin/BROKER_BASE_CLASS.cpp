/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: BROKER_BASE_CLASS.cpp
* Description: broker publish, subscribe base class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#include "BROKER_BASE_CLASS.h"
#include "common_mirco_define.h"
#include "LOG_MANAGER_CLASS.h"

const ZOO_INT32 Retry_Times = 3;

namespace ZOO_MQ {

    BROKER_BASE_CLASS::BROKER_BASE_CLASS(zmq::context_t &context, std::string addr, int zmq_type)
        : m_socket(context, zmq_type),
          m_address(addr)
    {
        if (zmq_type == ZMQ_SUB)
        {
			__MQ_TRY
	    	{		    
           		this->m_socket.setsockopt(ZMQ_SUBSCRIBE, "", 0);
        	}
			__MQ_CATCH
			{
				ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __FUNCTION__,"%s", ex.get_error_message().c_str());
			}
		}			
    }

    BROKER_BASE_CLASS::~BROKER_BASE_CLASS()
    {
        this->destroy();
    }

    void BROKER_BASE_CLASS::set_hwm(ZOO_UINT32 hwm)
    {
		__MQ_TRY
		{
        	this->m_socket.setsockopt(ZMQ_SNDHWM, &hwm, sizeof(hwm));
		}
		__MQ_CATCH
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __FUNCTION__,"%s",ex.get_error_message().c_str());
		}

		__MQ_TRY
		{
        	this->m_socket.setsockopt(ZMQ_RCVHWM, &hwm, sizeof(hwm));
		}
		__MQ_CATCH
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __FUNCTION__,"%s", ex.get_error_message().c_str());
		}
    }

    void BROKER_BASE_CLASS::set_linger(ZOO_UINT32 linger)
    {
		__MQ_TRY
		{
        	this->m_socket.setsockopt(ZMQ_LINGER, &linger, sizeof(linger));
		}
		__MQ_CATCH
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __FUNCTION__,"%s",ex.get_error_message().c_str());
		}
    }

    void BROKER_BASE_CLASS::bind()
    {
		__MQ_TRY
		{
        	this->m_socket.bind(this->m_address);
		}
		__MQ_CATCH
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __FUNCTION__,"%s",ex.get_error_message().c_str());
		}
    }

    void BROKER_BASE_CLASS::connect()
    {
		__MQ_TRY
		{
       		this->m_socket.connect(this->m_address);
		}
		__MQ_CATCH
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __FUNCTION__,"%s",ex.get_error_message().c_str());
		}
    }

    zmq::message_t &BROKER_BASE_CLASS::get_message()
    {
        return this->m_message;
    }

    int BROKER_BASE_CLASS::get_more()
    {
        return this->m_more;
    }

    zmq::socket_t &BROKER_BASE_CLASS::get_socket()
    {
        return this->m_socket;
    }

    void BROKER_BASE_CLASS::close()
    {
        this->m_socket.close();
    }

    void BROKER_BASE_CLASS::destroy()
    {
        this->m_socket.close();
    }

    void BROKER_BASE_CLASS::recv()
    {
		__MQ_TRY
		{
        	this->m_socket.recv(&this->m_message);
		}
		__MQ_CATCH
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __FUNCTION__,"%s", ex.get_error_message().c_str());
		}
        size_t more_size = sizeof (m_more);
        this->m_more = this->m_more? ZMQ_SNDMORE : 0;
        this->m_socket.getsockopt(ZMQ_RCVMORE, &m_more, &more_size);
    }

    void BROKER_BASE_CLASS::send(zmq::message_t &msg, int more)
    {        
        ZOO_INT32 times = 0;
        while(true)
        {
			__MQ_TRY
			{
            	if (this->m_socket.send(msg, more))
            	{
                	break;
            	}

            	if (++times >= 3)
            	{
                	break;
            	}
			}
			__MQ_CATCH
			{
				ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __FUNCTION__,"%s", ex.get_error_message().c_str());
			}
        }
    }

} //ZOO_MQ
