/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: FRONTEND_CLASS.cpp
* Description: broker subscribe class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#include "FRONTEND_CLASS.h"

namespace ZOO_MQ {

    FRONTEND_CLASS::FRONTEND_CLASS(zmq::context_t &context, std::string addr)
        : BROKER_BASE_CLASS(context, addr, ZMQ_SUB)
    {        
    }

    FRONTEND_CLASS::~FRONTEND_CLASS()
    {

    }

}//ZOO_MQ
