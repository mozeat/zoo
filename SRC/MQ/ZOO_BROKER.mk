include ../Makefile_tpl_cov
include ../Project_config

TARGET   := ZOO_BROKER
SRCEXTS  := .cpp
INCDIRS  := ./inc ./com 
SOURCES  := ./lib/ENVIRONMENT_ULTILITY_CLASS.cpp \
			./bin/MQMA_main.cpp \
			./bin/BROKER_BASE_CLASS.cpp \
			./bin/BACKEND_CLASS.cpp \
			./bin/BROKER_CONTROLLER_CLASS.cpp \
			./bin/FRONTEND_CLASS.cpp \
			./bin/BROKER_MODEL_CLASS.cpp \
			./bin/BROKER_FLOW_FACADE_CLASS.cpp\
			./bin/LOG_MANAGER_CLASS.cpp
			
SRCDIRS  := ./bin
CFLAGS   :=
CXXFLAGS := -std=c++14 -fPIC
CPPFLAGS := -DBOOST_ALL_DYN_LINK
LDFPATH  := 
LDFLAGS  := $(GCOV_LINK) $(LDFPATH) -lZOO 

include ../Makefile_tpl_linux

