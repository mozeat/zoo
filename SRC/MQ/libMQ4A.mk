include ../Makefile_tpl_cov
TARGET   := libMQ4A.so
SRCEXTS  := .cpp .c
INCDIRS  := ./inc ./com
SOURCES  := 
SRCDIRS  := ./lib
CFLAGS   :=
CXXFLAGS := -std=c++14
CPPFLAGS := $(GCOVER_COMP)  -fPIC 
LDFPATH  :
LDFLAGS  := $(GCOVER_LINK)  $(LDFPATH) -lnsl -shared 

include ../Project_config
include ../Makefile_tpl_linux
