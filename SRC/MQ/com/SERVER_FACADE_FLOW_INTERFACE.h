/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: SERVER_FACADE_FLOW_INTERFACE.h
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef SERVER_FACADE_FLOW_INTERFACE_H_
#define SERVER_FACADE_FLOW_INTERFACE_H_
extern "C"
{
	#include <ZOO.h>
	#include <MQ4A_type.h>
}
#include <string>
#include <mutex>
#include "common_mirco_define.h"
#include "FLOW_FACADE_INTERFACE.h"
namespace ZOO_MQ
{
    class SERVER_FACADE_FLOW_INTERFACE : virtual public FLOW_FACADE_INTERFACE
    {
    public:

        /**
         * @brief Default constructor
        */
        SERVER_FACADE_FLOW_INTERFACE(){}

        /**
         * @brief Destructor
        */
        virtual ~SERVER_FACADE_FLOW_INTERFACE(){}
    public:
		
        /**
         * @brief 初始化服务端服务
         * @param server 服务器地址
         * @param mode  服务端工作模式
        */
        virtual ZOO_INT32 initialize(IN const std::string & server) = 0;
		
        /**
         * @brief 关闭服务端服务
         * @param server 服务器地址
        */
        virtual ZOO_INT32 terminate(IN const std::string & server) = 0;

        /**
         * @brief 服务端回答客户端请求
         * @param server 服务器地址
         * @param req_msg 请求消息
         * @param rep_len 请求消息长度
         * @return 错误码
        */
        virtual ZOO_INT32 send_reply(IN const std::string & server,
                                            IN ZOO_UINT32 msg_id,
                                            IN void *rep_msg,
                                            IN ZOO_INT32 rep_len) = 0;

	   /**
         *@brief 服务端发布消息客户端
         * @param server 服务器地址
         * @param event_id 事件ID
         * @param request_message 请求消息
         * @param request_message_lenght 请求消息长度
         * @return 错误码
        */
        virtual ZOO_INT32 publish(IN const std::string   &  server,
                                            IN ZOO_INT32 event_id,
                                            IN void *request_message,
                                            IN ZOO_INT32 request_message_lenght) = 0;
					
	   /**
         *@brief 服务端注册处理事件回调函数
         *@param server 服务器地址
         *@param callback_function 处理事件回调函数
         *@return 错误码
        */
        virtual ZOO_INT32 register_event_handler(IN const std::string &  server_name,
        											IN MQ4A_BIND_CALLBACK_FUNCTION callback_function) = 0;



		
        /**
         * @brief 服务端执行事件监听
         * @param server 服务器地址
        */
        virtual ZOO_INT32 enter_event_loop(IN const std::string  & server) = 0;
    };
}
#endif
