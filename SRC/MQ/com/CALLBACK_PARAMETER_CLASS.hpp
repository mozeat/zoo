/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: CALLBACK_PARAMETER_CLASS.H
* Description: 
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef _CALLBACK_PARAMETER_CLASS_h
#define _CALLBACK_PARAMETER_CLASS_h
#include <ZOO.h>
#include <boost/move/move.hpp>
namespace ZOO_MQ
{
    template<typename Function,
                typename Parameter,
                    typename Context,
                        typename HANDLE = ZOO_UINT32>
    class CALLBACK_PARAMETER_CLASS
    {
        public:
            CALLBACK_PARAMETER_CLASS(Function & f,
                                                Parameter & param,
                                                Context & context,
                                                HANDLE handle):
                                                m_funtion_wrap(f),
                                                m_parameter_wrap((param)),
                                                m_context_wrap((context)),
                                                m_handle(boost::move(handle))
            
            {}
           virtual ~CALLBACK_PARAMETER_CLASS(){}
        public:
        
            void set_callback_function(Function         & f)
            {
                this->m_funtion_wrap = f;
            }
            
            const Function & get_callback_function()
            {
                return this->m_funtion_wrap;
            }
            
            void set_callback_parameter(Parameter & param)
            {
                this->m_parameter_wrap = (param);
            }
            
            const Parameter & get_callback_parameter()
            {
                return this->m_parameter_wrap;
            }
            
            void set_callback_context(Context & context)
            {
                this->m_context_wrap = (context);
            }
            
            const Context & get_callback_context()
            {
                return this->m_context_wrap;
            }
            
            void set_handle(HANDLE     handle)
            {
                this->m_handle = boost::move(handle);
            }
            
            const HANDLE & get_handle()
            {
                return this->m_handle;
            }
       private:
            Function m_funtion_wrap;
            Parameter m_parameter_wrap;
            Context m_context_wrap;
            HANDLE m_handle;
    };
}
#endif
