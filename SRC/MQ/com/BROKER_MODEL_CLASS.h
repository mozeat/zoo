/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: BROKER_MODEL_CLASS.h
* Description: broker model class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#ifndef BROKER_MODEL_CLASS_H
#define BROKER_MODEL_CLASS_H

extern "C"
{
	#include "ZOO_type.h"
	#include "ZOO_if.h"
}

#include "FRONTEND_CLASS.h"
#include "BACKEND_CLASS.h"
#include "BROKER_TYPE.h"
#include <string>
#include <map>
#include <vector>
#include <memory>

namespace ZOO_MQ {

    class BROKER_MODEL_CLASS
    {
    public:

        /*!
         * \brief default constructor
         */
        BROKER_MODEL_CLASS(FRONTEND_TYPE type);

        /*!
         * \brief disconstructor
         */
        ~BROKER_MODEL_CLASS();

    public:
        /*!
         * \brief set_frontend_address
         * \param adress
         */
        void set_frontend_address(std::string &adress);

        /*!
         * \brief set_backend_address
         * \param adresses
         */
        void set_backend_address(std::vector<std::string> &addresses);

        /*!
         * \brief set_hwm
         * \param hwm
         */
        void set_hwm(ZOO_UINT32 hwm);

        /*!
         * \brief set_linger
         * \param linger
         */
        void set_linger(ZOO_UINT32 linger);

        /*!
         * \brief set_ipc_path
         * \param path
         */
        void set_ipc_path(std::string path);

        /*!
         * \brief load_configure
         */
        void load_configure();

        /*!
         * \brief create_frontend_backend
         */
        void create_frontend_backend();

        /*!
         * \brief run
         */
        void run();

        /*!
         * \brief stop
         */
        void stop();

        /*!
         * \brief get_frontend
         * \param type
         * \return
         */
        std::shared_ptr<BROKER_BASE_CLASS> &get_frontend();

        /*!
         * \brief get_backend
         * \param type
         * \return
         */
        std::vector<std::shared_ptr<BROKER_BASE_CLASS> > &get_backend();

        /*!
         * \brief register_backend
         * \param backends
         */
        void register_backend(std::vector<std::shared_ptr<BROKER_BASE_CLASS>> backends);


    private:
        /*!
         * \brief create_frontend
         */
        void create_frontend();

        /*!
         * \brief create_frontend
         * \param type
         * \param addr
         * \param bind
         */
        void create_frontend(std::string addr, ZOO_BOOL bind = true);

        /*!
         * \brief create_backend
         */
        void create_backend();

        /*!
         * \brief create_backend
         * \param type
         * \param addr
         * \param bind
         */
        void create_backend(std::string addr, ZOO_BOOL bind = true);

        /*!
         * \brief add_poll_item
         * \param socket
         */
        void add_poll_item(zmq::socket_t &socket);


    private:
        zmq::context_t m_context;
        FRONTEND_TYPE m_type;

        std::string m_frontend_adress;
        std::vector<std::string> m_backend_addresses;

        ZOO_UINT32 m_hwm;
        ZOO_UINT32 m_linger;

        std::string m_ipc_path;

        std::vector<zmq::pollitem_t> m_poll_items;

        std::shared_ptr<BROKER_BASE_CLASS> m_frontend;

        std::vector<std::shared_ptr<BROKER_BASE_CLASS>> m_backends;

        ZOO_BOOL m_run_flag;
    };

}//ZOO_MQ

#endif // BROKER_MODEL_CLASS_H
