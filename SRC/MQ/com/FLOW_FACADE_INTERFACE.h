/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: FLOW_FACADE_INTERFACE.h
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef FLOW_FACADE_INTERFACE_H_
#define FLOW_FACADE_INTERFACE_H_
extern "C"
{
	#include <ZOO.h>
	#include <MQ4A_type.h>
}
#include <string>
#include <mutex>
#include "common_mirco_define.h"
namespace ZOO_MQ
{
    class FLOW_FACADE_INTERFACE
    {
    public:

        /**
         * @brief Default constructor
        */
        FLOW_FACADE_INTERFACE(){}

        /**
         * @brief Destructor
        */
        virtual ~FLOW_FACADE_INTERFACE(){}
    public:									
    };
}
#endif

