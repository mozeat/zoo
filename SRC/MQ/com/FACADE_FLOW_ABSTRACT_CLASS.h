/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: FACADE_FLOW_ABSTRACT_CLASS.h
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef FACADE_FLOW_ABSTRACT_CLASS_H_
#define FACADE_FLOW_ABSTRACT_CLASS_H_
extern "C"
{
	#include <ZOO.h>
	#include <MQ4A_type.h>
}
#include <string>
#include <mutex>
#include "SERVER_CONTROLLER_CLASS.h"
#include "CLIENT_CONTROLLER_CLASS.h"
#include <boost/smart_ptr.hpp>

namespace ZOO_MQ
{
    class FACADE_FLOW_ABSTRACT_CLASS
    {
    public:
        /**
         * @brief default constructor
        */
        FACADE_FLOW_ABSTRACT_CLASS();

        /**
         * @brief Destructor
        */
        virtual ~FACADE_FLOW_ABSTRACT_CLASS();
    public:
		
        /**
         * @brief 设置服务端控制器实例
         * @param server_controller 服务器控制器
         * @return 
        */
        void set_server_controller(IN boost::shared_ptr<SERVER_CONTROLLER_CLASS> server_controller);

		/**
         * @brief 获取服务端控制器实例
         * @param server_controller 服务器控制器
         * @return 
        */
        boost::shared_ptr<SERVER_CONTROLLER_CLASS> get_server_controller();
		
        /**
         * @brief 设置客户端控制器实例
         * @param client_controller 服务器控制器
         * @return 
        */
        void set_client_controller(IN boost::shared_ptr<CLIENT_CONTROLLER_CLASS> client_controller);

		/**
         * @brief 获取服务端控制器实例
         * @param server_controller 服务器控制器
         * @return 
        */
        boost::shared_ptr<CLIENT_CONTROLLER_CLASS> get_client_controller();
      
	protected:
		/**
         * @brief 服务端控制器实例
        */
		boost::shared_ptr<SERVER_CONTROLLER_CLASS> m_server_controller;

		/**
         * @brief 客户端控制器实例
        */
		boost::shared_ptr<CLIENT_CONTROLLER_CLASS> m_client_controller;
    };
}
#endif
