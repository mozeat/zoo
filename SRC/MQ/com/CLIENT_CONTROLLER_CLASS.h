/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: CLIENT_CONTROLLER_CLASS.h
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef CLIENT_CONTROLLER_CLASS_H
#define CLIENT_CONTROLLER_CLASS_H
#include "CLIENT_MODEL_CLASS.h"
#include <boost/smart_ptr.hpp>
#include <boost/thread/thread.hpp>
namespace ZOO_MQ
{
    class CLIENT_CONTROLLER_CLASS
    {
    public:
        /**
         * @brief Default constructor
        */
        CLIENT_CONTROLLER_CLASS();

        /**
         * @brief Destructor
        */
        virtual ~CLIENT_CONTROLLER_CLASS();
    public:
        /**
         * @brief Add a new client model to
         * @param client_model 
        */
        void add_model(IN boost::shared_ptr<CLIENT_MODEL_CLASS> client_model);

        /**
         * @brief Remove a client model from controller
         * @param client_model 
        */
        void remove_model(IN boost::shared_ptr<CLIENT_MODEL_CLASS> client_model);

        /**
         * @brief Subscribe callback function by event id
         * @param server              -server address 
         * @param callback_function   -callback for subscriber
         * @param callback_struct     -callback function parameter struct
         * @param event_id            -function id
         * @param handle              -a unique id for subscriber
         * @param context             -context
        */
        void subscribe(IN std::string server,
                                    IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
                                    IN MQ4A_CALLBACK_STRUCT * callback_struct,
                                    IN ZOO_INT32 event_id,
                                    IN ZOO_HANDLE *handle,
                                    INOUT void* context);
        /**
         * @brief Unsubscribe event  id
         * @param server             -server address
         * @param event_id           -subscriber id
         * @param handle             -the unique id for subscriber which from get from api subscribe
        */
        void unsubscribe(IN     const std::string server,
        						IN ZOO_INT32 event_id,
        						IN ZOO_HANDLE handle);

        /**
         * @brief push
         * @param server             -server address
         * @param req_meg
         * @param req_len
         * @param retry_interval
        */
        void send_request(IN const std::string server,
                               IN void *req_msg,
                               IN ZOO_INT32 req_len,
                               IN ZOO_INT32 retry_interval,
                               IN ZOO_BOOL signal = ZOO_FALSE);

        /* @brief push
         * @param server             -server address
         * @param rep_len
         * @param rep_len
         * @param retry_interval
         * @param timeout
        */
        void get_reply(IN const std::string server,
                                    IN void *rep_msg,
                                    IN ZOO_INT32 rep_len,
                                    IN ZOO_INT32 *act_rep_len,
                                    IN ZOO_INT32 timeout);
		/**
         * @brief 客户端发送请求,客户端获取回答
         * @param server 服务器地址
         * @param rep_msg 请求消息
         * @param rep_len 请求消息长度
         * @param act_rep_len 实际收到的字节长度
         * @param timeout  超时时间
         * @return 错误码
        */	
        void send_and_reply(IN const MQ4A_SERV_ADDR server,
                                            		IN void *req_msg,
                                            		IN ZOO_INT32 req_len,
	                                                IN void *rep_msg,
	                                                IN ZOO_INT32 rep_len,
	                                                IN ZOO_INT32 *act_rep_len,
                                            		IN ZOO_INT32 retry_interval,
	                                                IN ZOO_INT32 timeout);
        /**
         * @brief shutdown client loop
        */
        void shutdown_all();

		/**
         * @brief Get model by server
         * @param server
        */
        boost::shared_ptr<CLIENT_MODEL_CLASS> get_model(std::string server);
   private:

   		/**
         * @brief model add or remove sync lock
        */
        boost::mutex m_model_create_mutex;

		/**
         * @brief sync lock
        */
        boost::mutex m_sync_lock;
		
        /*
         * @brief m_client_map
         * @description: client model map
        */
        std::map<std::string,boost::shared_ptr<CLIENT_MODEL_CLASS> > m_client_map;
    };
}
#endif
