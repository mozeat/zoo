/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: SERVER_CONTROLLER_CLASS.h
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef SERVER_CONTROLLER_CLASS_H
#define SERVER_CONTROLLER_CLASS_H
#include "SERVER_MODEL_CLASS.h"
#include <boost/smart_ptr.hpp>
#include <map>
namespace ZOO_MQ
{
    class SERVER_CONTROLLER_CLASS
    {
    public:
        SERVER_CONTROLLER_CLASS();
        ~SERVER_CONTROLLER_CLASS();
    public:
        
        /*
         * @brief add_model
         * @param:std::string address
         * @description: add client model by server address
        */
        void add_model(IN boost::shared_ptr<SERVER_MODEL_CLASS> server_model);

        /*
         * @brief remove_model
         * @param:std::string address
         * @description: delete client model by server address
        */
        void remove_model(IN boost::shared_ptr<SERVER_MODEL_CLASS> server_model);

        /*
         * @brief initialize
         * @param server_name -process name
         * @param type - message queue type : tcp/ipc/intipc 
         * @description : enable server service
        */
        void initialize(std::string server); 

        /*
         * @brief terminate
         * @param server_name -process name
         * @description : destory bind
        */
        void terminate(std::string server);     

        /*
         * @brief publish
         * @param server_name -
         * @param event_id -
         * @param request_message - 
         * @param request_message_lenght
         * @description : destory bind
        */                    
        void publish(std::string server,IN ZOO_INT32 event_id,
                                            IN void *request_message,
                                            IN ZOO_INT32 request_message_lenght);

        /*
         * @brief send_reply
         * @param server_addr -process name
         * @description : reply message to client
        */
        void send_reply(std::string server,IN ZOO_UINT32 msg_id,IN void *rep_msg,
                                            IN ZOO_INT32 rep_len);                                    
        /*
         * @brief register_events
         * @param callback -
         * @param timeout -
         * @param request_message_lenght - 
         * @description : destory bind
        */                    
        void register_events_handler(std::string server,IN MQ4A_BIND_CALLBACK_FUNCTION callback_function);
        /*
         * @brief process_events 
         * @description : enter eventloop and process 
        */  
        void loop_event(std::string server);
    private:
        std::map<std::string,boost::shared_ptr<SERVER_MODEL_CLASS> > m_server_map;
    };
}
#endif
