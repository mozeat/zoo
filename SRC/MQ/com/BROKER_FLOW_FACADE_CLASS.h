/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: BROKER_FLOW_FACADE_CLASS.h
* Description: broker flow facade class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#ifndef BROKER_FLOW_FACADE_CLASS_H
#define BROKER_FLOW_FACADE_CLASS_H

#include "BROKER_CONTROLLER_CLASS.h"

#include <iostream>

namespace ZOO_MQ {

    class BROKER_FLOW_FACADE_CLASS
    {
    public:
        explicit BROKER_FLOW_FACADE_CLASS();
        ~BROKER_FLOW_FACADE_CLASS();

        /*!
         * \brief set_broker_controller
         * \param controller
         */
        void set_broker_controller(const std::shared_ptr<BROKER_CONTROLLER_CLASS> &controller);

        /*!
         * \brief create_all_models
         */
        void create_all_broker_models();

        /*!
         * \brief load_configuration
         */
        void load_configuration();

        /*!
         * \brief run
         */
        void run();

        /*!
         * \brief stop
         */
        void stop();

    private:
        std::shared_ptr<BROKER_CONTROLLER_CLASS> m_controller;
    };

}//ZOO_MQ

#endif // BROKER_FLOW_FACADE_CLASS_H
