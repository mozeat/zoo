/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: CLIENT_FACADE_FLOW_INTERFACE.h
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef CLIENT_FACADE_FLOW_INTERFACE_H_
#define CLIENT_FACADE_FLOW_INTERFACE_H_
extern "C"
{
	#include <ZOO.h>
	#include <MQ4A_type.h>
}
#include <string>
#include <mutex>
#include "common_mirco_define.h"
namespace ZOO_MQ
{
    class CLIENT_FACADE_FLOW_INTERFACE
    {
    public:

        /**
         * @brief default constructor
        */
        CLIENT_FACADE_FLOW_INTERFACE(){}

        /**
         * @brief disconstructor
        */
        virtual ~CLIENT_FACADE_FLOW_INTERFACE(){}
    public:
		
		/**
         * @brief 客户端发送异步请求
         * @param server 服务器地址
         * @param req_msg 请求消息
         * @param rep_len 请求消息长度
         * @param retry_interval 重试次数
         * @return 错误码
        */									
        virtual ZOO_INT32 send_request(IN const std::string & server,
                                            IN void *req_msg,
                                            IN ZOO_INT32 req_len,
                                            IN ZOO_INT32 retry_interval,
                                            IN ZOO_BOOL signal = ZOO_FALSE) = 0;

		/**
         * @brief 客户端获取回答
         * @param server 服务器地址
         * @param req_msg 请求消息
         * @param rep_len 请求消息长度
         * @param act_rep_len 实际收到的字节长度
         * @param timeout  超时时间
         * @return 错误码
        */	
        virtual ZOO_INT32 get_reply(IN const std::string & server,
                                            IN void *rep_msg,
                                            IN ZOO_INT32 rep_len,
                                            IN ZOO_INT32 *act_rep_len,
                                            IN ZOO_INT32 timeout) = 0;
        /**
         * @brief 客户端发送请求,客户端获取回答
         * @param server 服务器地址
         * @param req_msg 请求消息
         * @param rep_len 请求消息长度
         * @param act_rep_len 实际收到的字节长度
         * @param timeout  超时时间
         * @return 错误码
        */	
        virtual ZOO_INT32 send_and_reply(IN const MQ4A_SERV_ADDR server,
                                            		IN void *req_msg,
                                            		IN ZOO_INT32 req_len,
	                                                IN void *rep_msg,
	                                                IN ZOO_INT32 rep_len,
	                                                IN ZOO_INT32 *act_rep_len,
                                            		IN ZOO_INT32 retry_interval,
	                                                IN ZOO_INT32 timeout) = 0;
                                            
        /**
         * @brief 客户端订阅接口
         * @param server 服务器地址
         * @param callback_function 注册消息回调
         * @param callback_struct   请求消息长度
         * @param event_id          事件ID
         * @param handle            注册事件返回唯一ID 
         * @param context           上下文传递参数（一般NULL）
         * @return 错误码
        */
        virtual ZOO_INT32 subscribe(IN const std::string & server,
                                                IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
                                                IN MQ4A_CALLBACK_STRUCT * callback_struct,
                                                IN ZOO_INT32 event_id,
                                                INOUT ZOO_HANDLE *handle,
                                                INOUT void* context) = 0;
        /**
         * @brief 客户端取消订阅
         * @param server 服务器地址
         * @param event_id          事件ID
         * @param handle            注册事件返回唯一ID 
         * @return 错误码
        */
        virtual ZOO_INT32 unsubscribe(IN const std::string & server,
        							IN ZOO_INT32 event_id,
        							IN ZOO_HANDLE handle) = 0;





        /**
         *@brief 关闭客户端所有背景线程，例如订阅背景线程
        */
        virtual ZOO_INT32 shutdown_all_clients() = 0;
    };
}
#endif

