/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : MQ_EXCEPTION_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef MQ_EXCEPTION_CLASS_H
#define MQ_EXCEPTION_CLASS_H
extern "C"
{
	#include "ZOO_if.h"
}
#include <zmq.hpp>

namespace ZOO_MQ
{
    class MQ_EXCEPTION_CLASS :zmq::error_t
    {
        public:

            MQ_EXCEPTION_CLASS(int        error_code,std::string error_message,const char* std_exception = NULL)
            :m_error_code(error_code)
            ,m_error_message(error_message)
            ,m_std_excetpion(std_exception)
            {
                
            }
            ~MQ_EXCEPTION_CLASS(){}
            int get_error_code(){return m_error_code;}
            std::string get_error_message(){return m_error_message;}
            const char * get_std_excetpion(){return m_std_excetpion;}
        private:
            int m_error_code;
            std::string m_error_message;
            const char * m_std_excetpion;
    };
}
#endif
