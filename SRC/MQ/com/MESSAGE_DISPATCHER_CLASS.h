/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: MESSAGE_DISPATCHER_CLASS.h
* Description: broker model class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     weiwang.sun          created
******************************************************************************/
#ifndef MESSAGE_DISPATCHER_CLASS_H_
#define MESSAGE_DISPATCHER_CLASS_H_
extern "C"
{
    #include <ZOO.h>
    #include <MQ4A_type.h>
}
#include "MESSAGE_CLASS.h"
#include "MQ_THREAD_POOL.h"
#include "MQ_AUTO_RESET_EVENT_CLASS.h"
#include "common_mirco_define.h"
#include <string>
#include <mutex>
#include <condition_variable>

namespace ZOO_MQ
{
    class MESSAGE_DISPATCHER_CLASS 
    {
    public:
        /*
         * @brief constructor
        */
        MESSAGE_DISPATCHER_CLASS(ZOO_INT32 capacity = 2, ZOO_BOOL background = ZOO_TRUE,boost::shared_ptr<MQ_THREAD_POOL> thread_pool = nullptr);

        /*
         * @brief disconstructor
        */
        virtual ~MESSAGE_DISPATCHER_CLASS();
    public:

        /*
         * @brief add message in queue
        */
        void en_queue_message(boost::shared_ptr<MESSAGE_CLASS> message);

        /*
         * @brief update message in queue
        */
        void update_queue_message(boost::shared_ptr<MESSAGE_CLASS> message);
        
        /*
         * @brief get message 
        */
        boost::shared_ptr<MESSAGE_CLASS> get_message(ZOO_UINT32 message_id);

        /*
         * @brief get message 
        */
        boost::shared_ptr<MESSAGE_CLASS> get_message_by_thread(ZOO_LONG t_id);
        
        /**
         * @brief shutdown the message dispatcher 
        */
         void shutdown(); 
         
    private:
        /**
         * @brief synchronize message handler
         * @param capacity    the max message queue size
         * @param background  the flag for running mode
         */ 
        void initialize(ZOO_INT32 capacity, ZOO_BOOL background);
        
		/**
         * @brief run the dispatcher
         */
        void dispatch_message();
        
        /**
         * @brief handle subscribe message 
         * @param message 
         */
        void on_handle_subscribe_message(boost::shared_ptr<MESSAGE_CLASS> message);

        /**
         * @brief handle synchronize message
         * @param message 
         */       
        void on_handle_synchronize_message(boost::shared_ptr<MESSAGE_CLASS> message);

        /**
         * @brief dequeue synchronize message 
         * @param message 
         */ 
        ZOO_BOOL dequeue_synchronize_message(boost::shared_ptr<MESSAGE_CLASS> & message);

        /**
         * @brief dequeue subscribe message 
         * @param message 
         */ 
        ZOO_BOOL dequeue_subscribe_message(boost::shared_ptr<MESSAGE_CLASS> & message);
    private:
        /**
         * @brief the capacity of message size
         */ 
        std::size_t m_capacity;

        /**
         * @brief start flag
         */
        ZOO_BOOL m_started;

        /**
         * @brief the sync lock for en or dequeue
         */
        boost::recursive_mutex m_syn_lock;

        boost::shared_mutex m_shared_lock;

        /**
         * @brief the thread pool
         */
        boost::shared_ptr<MQ_THREAD_POOL> m_thread_pool;

        /**
         * @brief the event handle 
         */
        boost::shared_ptr<MQ_AUTO_RESET_EVENT_CLASS> m_wait_handle;

        /**
         * @brief the sync message queue
         */
        std::list<boost::shared_ptr<MESSAGE_CLASS> > m_sync_message_queue;

        /**
         * @brief the subscribe message dequeue
         */
        std::list<boost::shared_ptr<MESSAGE_CLASS> > m_subscribe_message_queue;
    };
}
#endif
