/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: SOCKET_INTERFACE.h
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2017-12-21     weiwang.sun       created
******************************************************************************/
#ifndef _SOCKET_INTERFACE_H
#define _SOCKET_INTERFACE_H
extern "C"
{
	#include <ZOO_if.h>
    #include <MQ4A_type.h>
}

#include <mutex>
#include <string>
#include <map>
#include <zmq.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/nondet_random.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/assert.hpp>


namespace ZOO_MQ
{
    class SOCKET_INTERFACE
    {
        public:
            /*
             * @brief default constructor
            */
            SOCKET_INTERFACE();

            /*
             * @brief disconstructor
            */
            virtual ~SOCKET_INTERFACE();
        public:
            /*
             * @brief type CALLBACK context
            */
            typedef struct
            {
            	zmq::message_t server;
				zmq::message_t msg_id;
                zmq::message_t msg_body;
            }CALLBACK_CONTEXT_STRUCT;

            /**
             * @brief initialize
             * @param server_addr -address
             * @param type - message queue type : tcp/ipc/intipc
             * @description : enable server service
            */
            virtual void initialize();

            /**
             * @brief terminate
             * @param server_addr -address
             * @description : destory bind
            */
            virtual void terminate();

            /**
             * @brief send
             * @param server_addr -address
             * @param req_msg -request message
             * @param req_len -size of message
             * @param retry_interval
             * @param timeout
             * @description : destory bind
            */
            virtual void send_reply(IN ZOO_UINT32 msg_id,IN void *rep_msg,
                                                IN ZOO_INT32 rep_len);

            /**
             * @brief send_and_reply
             * @param server_addr -address
             * @param req_msg -request message
             * @param req_len -size of message
             * @param retry_interval
             * @param timeout
             * @description : destory bind
            */
            virtual void send_and_reply(IN void *req_meg,
                                                    IN ZOO_INT32 req_len,
                                                    OUT void *rep_msg,
                                                    IN ZOO_INT32 rep_len,
                                                    IN ZOO_INT32 *act_rep_len,
            									    IN ZOO_INT32 retry_interval,
            									    IN ZOO_INT32 timeout);
            /**
             * @brief send_request
             * @param server_addr -address
             * @param req_msg -request message
             * @param req_len -size of message
             * @param retry_interval
             * @param timeout
             * @description : destory bind
            */
            virtual void send_request(IN void *req_meg,
                                                    IN ZOO_INT32 req_len,
            									    IN ZOO_INT32 retry_interval,
                                                    IN ZOO_BOOL signal = ZOO_FALSE);
            /*
             * @brief recv
             * @param server_addr -address
             * @description : destory bind
            */
            virtual void get_reply(OUT void *rep_msg,
                                                    IN ZOO_INT32 rep_len,
                                                    IN ZOO_INT32 *act_rep_len,
    									            IN ZOO_INT32 timeout);

           /**
             * @brief register_events
             * @param callback -
             * @param timeout -
             * @param request_message_lenght -
             * @description : destory bind
            */
            virtual void register_events_handler(IN MQ4A_BIND_CALLBACK_FUNCTION callback_function);

           /**
             * @brief subscribe
             * @param server_name -
             * @param handle -
             * @param request_message_lenght -
             * @param callback_function -
             * @description : destory bind
            */
            virtual void subscribe(IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
                                                IN MQ4A_CALLBACK_STRUCT * callback_struct,
                                                IN ZOO_INT32 event_id,IN ZOO_HANDLE *handle,
                                                INOUT void* context);

           /**
             * @brief unsubscribe
             * @param server_name -
             * @param event_id -
             * @description : destory bind
            */
           virtual void unsubscribe(IN ZOO_INT32 event_id,IN ZOO_HANDLE handle);
		   
           /**
             * @brief publish
             * @param server_name -
             * @param event_id -
             * @param request_message -
             * @param request_message_lenght
             * @description : destory bind
            */
            virtual void publish(IN ZOO_INT32 event_id,
                                                IN void *message,
                                                IN ZOO_INT32 message_lenght);
            /**
             * @brief process_events
             * @description : enter eventloop and process
            */
            virtual void loop_event();

			/**
             * @brief close backgroud thread 
             * @description : process event message
            */
            virtual void shutdown();
    };
}
#endif
