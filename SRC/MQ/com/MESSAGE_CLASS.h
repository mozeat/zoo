/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: SOCKET_ABSTRACT_CLASS.h
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2017-12-21     weiwang.sun       created
******************************************************************************/
#ifndef _MESSAGE_CLASS_H
#define _MESSAGE_CLASS_H
extern "C"
{
	#include "ZOO_if.h"
    #include <MQ4A_type.h>
}
#include "CALLBACK_PARAMETER_CLASS.hpp"
#include <string>
#include <map>
#include <vector>
#include <atomic>
#include <boost/flyweight.hpp>
namespace ZOO_MQ
{
    /*
     * @brief type CALLBACK_FUNCTION
    */
    typedef CALLBACK_PARAMETER_CLASS<MQ4A_EVENT_CALLBACK_FUNCTION,MQ4A_CALLBACK_STRUCT*,void*> CALLBACK_FUNCTION;
    typedef boost::shared_ptr<CALLBACK_FUNCTION> CALLBACK_FUNCTION_PTR;
    typedef std::vector<CALLBACK_FUNCTION_PTR> CALLBACK_FUNCTION_VECTOR;
    /*
     * @brief type EVENT_MAP
    */
    typedef std::map<ZOO_UINT32,CALLBACK_FUNCTION_VECTOR> EVENT_MAP;
    typedef std::map<ZOO_UINT32,CALLBACK_FUNCTION_VECTOR>::iterator EVENT_MAP_ITE;

             
    struct MESSAGE_CLASS
    {     
        /*
         * @brief constructor
         * @param mid
         * @param event_
         */
        MESSAGE_CLASS(ZOO_UINT32 mid,MQ4A_MSG_EVENT_ENUM event_)
        :id(mid)
        {
            event.store(event_);
            thread_id =  ZOO_get_thread_id();
            request_time = ZOO_get_current_timestamp_msec();
            deadline = request_time + 60000;
        }

        /*
         * @brief constructor
         * @param mid
         * @param data_
         */
        MESSAGE_CLASS(ZOO_UINT32 mid,std::string & data_)
        :id(mid),data(data_)
        {
            event.store(MQ4A_MSG_EVENT_REQUESTED);
            thread_id =  ZOO_get_thread_id();
            request_time = ZOO_get_current_timestamp_msec();
            deadline = request_time + 60000;
        }

        /*
         * @brief constructor
         * @param mid
         * @param data_
         * @param handler_
         */
        MESSAGE_CLASS(ZOO_UINT32 mid,const std::string & data_,
            const CALLBACK_FUNCTION_VECTOR & handler_)
            :id(mid),data(data_),handler(handler_)
        {
            event.store(MQ4A_MSG_EVENT_SUBSCRIPTION);
            thread_id = ZOO_get_thread_id();
        }
            
        /*
         *@brief requset thread id
         */
        ZOO_LONG thread_id;
            
        /*
         *@brief message id
         */
        ZOO_UINT32 id;

        /*
         *@brief message request time
         */
        ZOO_UINT64 request_time;

        /*
         *@brief message content
         */
        std::string data;
        
        /*
         *@brief message invalid time
         */
        ZOO_UINT64 deadline;

        /*
         *@brief the message event for recording data process flow
         */
        std::atomic<MQ4A_MSG_EVENT_ENUM> event;

        /*
         *@brief message handler for subsriber
         */
        CALLBACK_FUNCTION_VECTOR handler;

        /*
         *@brief the factory id of message processes 
         */
        std::string server;
    };
}
#endif
