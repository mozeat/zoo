/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: MQ_THREAD_POOL.h
* Description: queue working for multi-thread
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     weiwang.sun    		created
******************************************************************************/
#ifndef MQ_THREAD_POOL_H_
#define MQ_THREAD_POOL_H_
extern "C"
{
    #include <ZOO.h>
    #include <MQ4A_type.h>
}
#include <string>
#include <mutex>
#include <condition_variable>
#include <boost/asio.hpp>
#include <boost/function.hpp>
#include <boost/thread/thread.hpp>

namespace ZOO_MQ
{
    class MQ_THREAD_POOL 
    {
    public:
        /*
         * @brief constructor
        */
        MQ_THREAD_POOL(ZOO_INT32 thread_numbers);

        /*
         * @brief disconstructor
        */
        virtual ~MQ_THREAD_POOL();
    public:
        
        /**
         * @brief Adds a task to the thread pool.
         * @param task
         */
        template<typename T>
        void queue_working(T task)
        {
            boost::asio::post(this->m_ioc,task);
        }
        
        /*
         * @brief shutdown 
         */
        void shutdown();

    private:

        /*
         * @brief create threads
         * @brief thread_numbers 
         */
        void create_threads(ZOO_INT32 thread_numbers);

        /*
         * @brief create threads 
         */
        void initialize_thread();
       
    private:
        boost::asio::io_context m_ioc;
        boost::asio::io_context::work m_idle_worker;
        boost::thread_group m_thread_pool;
    };
}
#endif
