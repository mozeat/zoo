#ifndef COMMON_MIRCO_DEFINE_H
#define COMMON_MIRCO_DEFINE_H
#include <zmq.hpp>
#include "MQ_EXCEPTION_CLASS.h"

/********************************************************
* @ brief try catch 
* @subscritption:event loop scope begin definition
*********************************************************/
#define __MQ_TRY  try 
    
#define __MQ_CATCH  catch(ZOO_MQ::MQ_EXCEPTION_CLASS & ex) 
  
#define __MQ_CATCH_ALL  catch(ZOO_MQ::MQ_EXCEPTION_CLASS & ex){}\
                        catch(...){}

/********************************************************
* @ brief __THROW_MQ_EXCEPTION
* @subscritption:
*********************************************************/            
#define __THROW_MQ_EXCEPTION(error_code,error_message,std_excetpion) \
    ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__FUNCTION__,"%s",error_message); \
    throw(ZOO_MQ::MQ_EXCEPTION_CLASS(error_code,error_message,std_excetpion));
   
/********************************************************
* @ brief __MQ_EVENT_LOOP_BEGIN
* @subscritption:event loop scope begin definition
*********************************************************/
#define __MQ_EVENT_LOOP_BEGIN  for(;;)\
                               {\
                                  try 
                                
/********************************************************
* @ brief __MQ_EVENT_LOOP_END
* @subscritption:event loop scope end definition
*********************************************************/    
#define __MQ_EVENT_LOOP_END      catch(ZOO_MQ::MQ_EXCEPTION_CLASS & ex) \
								 {ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__FUNCTION__,"%s",ex.get_error_message().c_str());} \
                                 catch(std::exception & ex){ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__FUNCTION__,"%s",ex.what());}\
                               }

    
#endif

