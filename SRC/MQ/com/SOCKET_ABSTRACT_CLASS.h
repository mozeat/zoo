/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: SOCKET_ABSTRACT_CLASS.h
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2017-12-21     weiwang.sun       created
******************************************************************************/
#ifndef _SOCKET_ABSTRACT_CLASS_H
#define _SOCKET_ABSTRACT_CLASS_H
extern "C"
{
	#include <MM4A_if.h>
	#include <ZOO_if.h>
}
#include "SOCKET_INTERFACE.h"
#include <ENVIRONMENT_ULTILITY_CLASS.h>
#include "common_mirco_define.h"
namespace ZOO_MQ
{
    class SOCKET_ABSTRACT_CLASS
    {
        public:
            /*
             * @brief default constructor
            */
            SOCKET_ABSTRACT_CLASS();

            /*
             * @brief destructor
            */
            virtual ~SOCKET_ABSTRACT_CLASS();
        public:

			/*
             * @brief delegate
            */
            template<typename F ,typename... Args>
            void transfer_function(const F & f, Args &&...args)
            {
                std::initializer_list<int>{(f(std::forward< Args>(args)),0)...};
            }

			/*
             * @brief Set markiong code
             * @param marking code
            */
            void set_marking_code(std::string marking_code);

			/*
             * @brief Get markiong code
             * @param marking code
            */
            const std::string & get_marking_code();

			/*
             * @brief Parse message to int value
             * @param message
             * @param function_id
            */
            void parse_event_id(zmq::message_t & message,ZOO_INT32 & function_id);

			/*
             * @brief Set socket send time
             * @param socket
             * @param milliseconds
            */
			void set_socket_send_timeout(IN zmq::socket_t & socket,
													IN ZOO_INT32 milliseconds);

			/*
             * @brief Set socket recv time
             * @param socket
             * @param milliseconds
            */
			void set_socket_recv_timeout(IN zmq::socket_t & socket,
													IN ZOO_INT32 milliseconds);
													
			/*
             * @brief Serialize message 
             * @param message
             * @param function_id
            */
            void make_pack(IN void * src, 
            				   		IN ZOO_INT32 len,
            				   		INOUT zmq::message_t & dst);

            /*
             * @brief Unserialize message 
             * @param message
             * @param function_id
            */
            void make_unpack(IN zmq::message_t & src,
					    				IN void * dest,
					    				INOUT ZOO_INT32 * act_len);

			/*
             * @brief Unserialize message 
             * @param message
             * @param function_id
            */
            void make_unpack(IN zmq::message_t & src,
					    				IN std::string & message);
					    				
			/*
             * @brief Send with nopack message 
             * @param message
             * @param function_id
            */
			void send_raw_message(IN zmq::socket_t & socket,
											IN zmq::message_t & message,
											IN ZOO_BOOL more = ZOO_FALSE);

			/*
             * @brief Send with nopack message 
             * @param message
             * @param function_id
            */
			void send_raw_message(IN zmq::socket_t & socket,
											IN std::string & message,
											IN ZOO_BOOL more = ZOO_FALSE);
			/*
             * @brief Send with nopack message 
             * @param message
             * @param function_id
            */
			void send_raw_message(IN zmq::socket_t & socket,
											IN void * message,
											IN ZOO_INT32 len,
											IN ZOO_BOOL more = ZOO_FALSE);
											
			/*
             * @brief send message 
             * @param message
             * @param more 
            */
			void pack_and_send_message(IN zmq::socket_t & socket,
													IN zmq::message_t & message,
													IN ZOO_BOOL more = ZOO_FALSE);	
						
			/*
             * @brief send message 
             * @param message
             * @param len      the size of message
             * @param more 
            */
			void pack_and_send_message(IN zmq::socket_t & socket,
												IN void * src, 
					            				IN ZOO_INT32 len,
												IN ZOO_BOOL more = ZOO_FALSE);	
			/*
             * @brief receive message 
             * @param message
             * @param function_id
            */
			void receive_and_unpack(IN zmq::socket_t & socket,
											INOUT zmq::message_t & message);	
			/*
             * @brief receive message 
             * @param message
             * @param function_id
            */
			void receive_and_unpack(IN zmq::socket_t & socket,
											INOUT void * rep_msg, 
											IN ZOO_INT32 rep_len,
            								INOUT ZOO_INT32 * act_rep_len);
            /*
             * @brief receive message 
             * @param message
             * @param function_id
            */
			void receive_and_unpack(IN zmq::socket_t & socket,
											INOUT std::string & message);								
            /*
             * @brief receive message 
             * @param message
             * @param function_id
            */
			void receive_raw_message(IN zmq::socket_t & socket,
											INOUT void * rep_msg, 
											IN ZOO_INT32 rep_len,
            								INOUT ZOO_INT32 * act_rep_len);	
            /*
             * @brief receive message 
             * @param message
             * @param function_id
            */
			void receive_raw_message(IN zmq::socket_t & socket,
											INOUT zmq::message_t & message);	
			/*
             * @brief receive message 
             * @param message
             * @param function_id
            */
			void receive_raw_message(IN zmq::socket_t & socket,
											INOUT std::string & message);

           /*
             * @brief receive message 
             * @param socket
             * @param mid
             * @param data
             * @param function_id
            */
			void send_message(IN zmq::socket_t & socket,
											    IN const void * mid,std::size_t mid_len,
											    IN const void * data,std::size_t data_len);
            /*
             * @brief receive message 
             * @param socket
             * @param mid
             * @param data
             * @param function_id
            */
			void receive_message(IN zmq::socket_t & socket,
											    INOUT std::string & mid,
											    INOUT std::string & data);

            /*
             * @brief get frontend thread numbers 
            */
            ZOO_INT32 get_frontend_thread_number(); 

            /*
             * @brief get backend thread numbers 
            */
            ZOO_INT32 get_backend_thread_number();  

            /*
             * @brief generate message id
            */
            ZOO_UINT32 generate_mid();

            /*
             * @brief generate message id
            */
            ZOO_UINT64 generate_mid(const std::vector<ZOO_UINT32> & exclude_conditions);
       protected:
            /*
             * @brief model identity 
            */
            std::string m_marking_code;

            /*
             * @brief for mid 
            */
            ZOO_UINT64 m_begin;

            ZOO_UINT32 m_mid;
    };
}
#endif
