/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: BROKER_TYPE.h
* Description: broker type define file
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#ifndef BROKER_TYPE_H
#define BROKER_TYPE_H

namespace ZOO_MQ {

    enum FRONTEND_TYPE
    {
        RECV_LOCAL_TYPE         = 0,
        RECV_LOCAL_SERVER_TYPE  = 1,
        RECV_REMOTE_TYPE        = 2
    };

}//ZOO_MQ

#endif // BROKER_TYPE_H
