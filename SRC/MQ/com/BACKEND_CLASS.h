/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: BACKEND_CLASS.h
* Description: broker publish class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#ifndef BACKEND_CLASS_H
#define BACKEND_CLASS_H

#include "BROKER_BASE_CLASS.h"

namespace ZOO_MQ {

    class BACKEND_CLASS : public BROKER_BASE_CLASS
    {
    public:
        explicit BACKEND_CLASS(zmq::context_t &context, std::string addr);
        ~BACKEND_CLASS();
    };

} //ZOO_MQ

#endif // BACKEND_CLASS_H
