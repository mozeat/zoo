/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: ENVIRONMENT_ULTILITY_CLASS.H
* Description: 
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef ENVIRONMENT_ULTILITY_CLASS_H
#define ENVIRONMENT_ULTILITY_CLASS_H

extern "C"
{
    #include <unistd.h>
    #include <sys/types.h>
    #include <pwd.h>
}
#include <boost/thread/thread.hpp>
#include <boost/thread/once.hpp>
#include <boost/shared_ptr.hpp>
#include <string>
namespace ZOO_MQ
{
    class ENVIRONMENT_ULTILITY_CLASS
    {
    public:
        ENVIRONMENT_ULTILITY_CLASS();
        virtual ~ENVIRONMENT_ULTILITY_CLASS();
        static boost::shared_ptr<ENVIRONMENT_ULTILITY_CLASS> get_instance();
    public:

        /**
         * @breif get current user home path ,equal to echo $HOME path
        */
        std::string get_home_path();

        /**
         * @breif get ipc file store path
        */
        std::string get_ipc_path();

        /**
         * @breif get ipoc file store path
        */
        std::string get_inproc_path();
    private:
        void initialize();
    private:
        std::string m_home_path;
        std::string m_ipc_path;
        std::string m_inproc_path;
        boost::once_flag  m_call_one_flag;
        static boost::shared_ptr<ENVIRONMENT_ULTILITY_CLASS> m_instance;
    };
}


#endif // ENVIRONMENT_ULTILITY_CLASS_H
