/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: BROKER_CONFIGURATION_CLASS.h
* Description: broker config class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#ifndef BROKER_CONFIGURATION_H
#define BROKER_CONFIGURATION_H

extern "C"
{
	#include "ZOO_if.h"
}

#include <string>

namespace ZOO_MQ {
class BROKER_CONFIGURATION_CLASS
{
    public:
        /*!
         * \brief BROKER_CONFIGURATION
         */
        explicit BROKER_CONFIGURATION_CLASS();

        ~BROKER_CONFIGURATION_CLASS();

        /*!
         * \brief load_configure
         */
        void load_configure();

        /*!
         * \brief get_hwm
         * \return
         */
        ZOO_UINT32 get_hwm() const;

        /*!
         * \brief get_linger
         * \return
         */
        ZOO_UINT32 get_linger() const;

        /*!
         * \brief get_local_fe_send
         * \return
         */
        std::string get_local_fe_send() const;

        /*!
         * \brief get_local_fe_recv
         * \return
         */
        std::string get_local_fe_recv() const;

        /*!
         * \brief get_local_be_send
         * \return
         */
        std::string get_local_be_send() const;

        /*!
         * \brief get_local_be_recv
         * \return
         */
        std::string get_local_be_recv() const;

        /*!
         * \brief get_remote_frontend
         * \return
         */
        std::string get_remote_frontend() const;

        /*!
         * \brief get_remote_backend
         * \return
         */
        std::string get_remote_backend() const;

        std::string get_ipc_path() const;

    private:
        ZOO_BROKER_STRUCT m_broker_conf;
        std::string m_ipc_path;
    };
}//ZOO_MQ
#endif // BROKER_CONFIGURATION_H
