/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: BROKER_BASE_CLASS.h
* Description: broker publish, subscribe base class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#ifndef BROKER_BASE_CLASS_H
#define BROKER_BASE_CLASS_H
extern "C"
{
	#include "ZOO_type.h"
	#include "ZOO_if.h"
}

#include <zmq.hpp>
#include <string>

namespace ZOO_MQ {
    class BROKER_BASE_CLASS
    {
    public:
        /*!
         * \brief BROKER_BASE_CLASS
         * \param context
         * \param addr
         * \param zmq_type
         */
        explicit BROKER_BASE_CLASS(zmq::context_t &context, std::string addr, int zmq_type);
        /*!
         * \brief disconstructor
         */
        virtual ~BROKER_BASE_CLASS();

    public:
        /*!
         * \brief set_hwm
         * \param hwm
         */
        virtual void set_hwm(ZOO_UINT32 hwm);
        /*!
         * \brief set_linger
         * \param linger
         */
        virtual void set_linger(ZOO_UINT32 linger);
        /*!
         * \brief close socket
         */
        virtual void destroy();
        /*!
         * \brief recv msg
         */
        virtual void recv();

        /*!
         * \brief send
         * \param msg
         * \param more
         */
        virtual void send(zmq::message_t &msg, int more);

        /*!
         * \brief bind address
         */
        virtual void bind();
        /*!
         * \brief connect address
         */
        virtual void connect();

        /*!
         * \brief get_message
         * \return
         */
        zmq::message_t &get_message();

        /*!
         * \brief get_more
         * \return
         */
        int get_more();

        /*!
         * \brief get_socket
         * \return
         */
        zmq::socket_t &get_socket();

        /*!
         * \brief close
         */
        void close();

    private:
        zmq::socket_t m_socket;
        std::string m_address;

        zmq::message_t m_message;

        ZOO_INT32 m_more;
    };
}//ZOO_MQ

#endif // BROKER_BASE_CLASS_H
