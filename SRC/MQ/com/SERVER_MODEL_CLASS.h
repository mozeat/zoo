/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: SERVER_MODEL_CLASS.h
* Description: interface for socket
* History recorder:
* Version   date           author            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef SERVER_MODEL_CLASS_H_
#define SERVER_MODEL_CLASS_H_
extern "C"
{
	#include <ZOO.h>
	#include <MQ4A_type.h>	
	#include <MM4A_if.h>
	#include <ZOO_if.h>
}

#include "SOCKET_INTERFACE.h"
#include "SOCKET_ABSTRACT_CLASS.h"
#include "common_mirco_define.h"
#include <string>
#include <mutex>
#include <boost/thread.hpp>
#include <map>
#include <cstring>
#include <iostream>
#include <sstream>
#include <boost/algorithm/string.hpp>  
#include <boost/lexical_cast.hpp>
#include <utility>
#include <sys/resource.h>
#include "MESSAGE_DISPATCHER_CLASS.h"
#include "MQ_THREAD_POOL.h"
namespace ZOO_MQ
{
    class SERVER_MODEL_CLASS:public virtual SOCKET_INTERFACE
                                    ,public virtual SOCKET_ABSTRACT_CLASS
    {
    public:
        /*
         * @brief default constructor
        */
        SERVER_MODEL_CLASS(IN std::string marking_code);
        /*
         * @brief disconstructor
        */
        virtual ~SERVER_MODEL_CLASS();

    public:            

        /*
         * @brief initialize
         * @param server_name -process name
         * @param type - message queue type : tcp/ipc/intipc 
         * @description : enable server service
        */
        void initialize(); 

        /*
         * @brief terminate
         * @description : destory bind
        */
        void terminate();     
       
        /*
         * @brief publish
         * @param event_id - the subscrition event id 
         * @param request_message - the message 
         * @param request_message_lenght
         * @description : publish events to client
        */                    
        void publish(IN ZOO_INT32 event_id,
                            IN void *message,
                            IN ZOO_INT32 message_lenght);

		/*
         * @brief send_reply
         * @param server_addr -process name
         * @description : reply message to client
        */
        void send_reply(IN ZOO_UINT32 msg_id,IN void *rep_msg,
                                            IN ZOO_INT32 rep_len);    
                                            
        /*
         * @brief register_events
         * @param callback_function - 
         * @description : register server events handler callback
        */  
        void register_events_handler(IN MQ4A_BIND_CALLBACK_FUNCTION callback_function);

        /*
         * @brief process_events 
         * @description : enter eventloop and listen  
        */  
        void loop_event();
        
    private:   
		
		/**
		 * @brief delete constructor
		 */
        SERVER_MODEL_CLASS() = delete;
        
       /*
         * @brief enter_event_loop 
         * @description : enter eventloop  
        */
        void enter_event_loop();

        /*
         * @brief execute_callback 
         * @param message
         * @param len
        */ 
        void handle_request(IN const void * message,ZOO_UINT32 msg_id); 
        
        /*
         * @brief execute_callback 
         * @param message
        */ 
        void handle_request(boost::shared_ptr<MESSAGE_CLASS> message); 

    private:
        
        ZOO_INT32 m_thread_numbers;
        
		/**
         * @brief socket context
        */
        zmq::context_t m_context;

		/**
         * @brief Local subscribe zmq socket
        */
        zmq::socket_t m_local_sub;

		/**
         * @brief Local publish zmq socket
        */
		zmq::socket_t m_local_pub;
        
		/**
         * @brief Local publish server address
        */
		std::string m_local_pub_server;

		/**
         * @brief Local sub server address
        */
        std::string m_local_sub_server;

		/**
         * @brief Local sub server address
        */
        MQ4A_BIND_CALLBACK_FUNCTION m_event_handler;  
		
		/**
         * @brief Lock for sync publish
        */
        boost::mutex m_publish_sync_lock;

		/**
         * @brief message dispatcher
         */
        boost::shared_ptr<MESSAGE_DISPATCHER_CLASS> m_message_dispatcher;

        /**
         * @brief thread pool
         */
        boost::shared_ptr<MQ_THREAD_POOL> m_thread_pool;
    };
}
#endif
