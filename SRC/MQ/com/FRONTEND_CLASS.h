/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: FRONTEND_CLASS.h
* Description: broker subscribe class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#ifndef FRONTEND_CLASS_H
#define FRONTEND_CLASS_H

#include "BROKER_BASE_CLASS.h"

namespace ZOO_MQ {

    class FRONTEND_CLASS : public BROKER_BASE_CLASS
    {
    public:
        explicit FRONTEND_CLASS(zmq::context_t &context, std::string addr);
        ~FRONTEND_CLASS();
    };

} //ZOO_MQ

#endif // FRONTEND_CLASS_H
