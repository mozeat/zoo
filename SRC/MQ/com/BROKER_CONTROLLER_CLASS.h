/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: BROKER_CONTROLLER_CLASS.h
* Description: broker control class
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     chao.luo       		created
******************************************************************************/
#ifndef BROKER_CONTROLLER_CLASS_H
#define BROKER_CONTROLLER_CLASS_H
extern "C"
{
	#include "ZOO_if.h"
}
#include "BROKER_MODEL_CLASS.h"
#include "BROKER_TYPE.h"
#include "BROKER_CONFIGURATION_CLASS.h"
#include <boost/thread.hpp>
#include <vector>
#include <map>

namespace ZOO_MQ {

    class BROKER_CONTROLLER_CLASS
    {
    public:

        /*!
         * \brief default constructor
         */
        explicit BROKER_CONTROLLER_CLASS();

        /*!
         * \brief disconstructor
         */
        ~BROKER_CONTROLLER_CLASS();

    public:
        /*!
         * \brief create_all_models
         */
        void create_all_broker_models();

        /*!
         * \brief load_configuration
         */
        void load_configuration();

        /*!
         * \brief run
         */
        void run();

        /*!
         * \brief stop
         */
        void stop();

    private:
        void create_broker_models(FRONTEND_TYPE type);
        std::shared_ptr<BROKER_MODEL_CLASS> find_model(FRONTEND_TYPE type);

    private:
        std::shared_ptr<BROKER_CONFIGURATION_CLASS> m_broker_conf;

        std::map<FRONTEND_TYPE, std::shared_ptr<BROKER_MODEL_CLASS>> m_broker_models;

        boost::thread_group m_group_thread;
    };

}//ZOO_MQ

#endif // BROKER_CONTROLLER_CLASS_H
