/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MQ
* File name: CLIENT_MODEL_CLASS.h
* Description: frontend
* History recorder:
* Version   date           author            	context
* 1.0       2019-04-12     weiwang.sun       		created
******************************************************************************/
#ifndef CLIENT_MODEL_CLASS_H_
#define CLIENT_MODEL_CLASS_H_
#include "SOCKET_INTERFACE.h"
#include "SOCKET_ABSTRACT_CLASS.h"
#include "CALLBACK_PARAMETER_CLASS.hpp"
#include "MESSAGE_CLASS.h"
#include "MESSAGE_DISPATCHER_CLASS.h"
#include "MQ_THREAD_POOL.h"
namespace ZOO_MQ
{
    class CLIENT_MODEL_CLASS : virtual public SOCKET_INTERFACE,
										    public SOCKET_ABSTRACT_CLASS
    {
    public:
        /*
         * @brief constructor
        */
        CLIENT_MODEL_CLASS(std::string marking_code);

        /*
         * @brief disconstructor
        */
        virtual ~CLIENT_MODEL_CLASS();
    public:
		/**
         * @brief send_and_reply
         * @param server_addr -address
         * @param req_msg -request message
         * @param req_len -size of message
         * @param retry_interval
         * @param timeout
         * @description : sync interface
        */
        void send_and_reply(IN void *req_meg,
                                                IN ZOO_INT32 req_len,
                                                OUT void *rep_msg,
                                                IN ZOO_INT32 rep_len,
                                                IN ZOO_INT32 *act_rep_len,
        									    IN ZOO_INT32 retry_interval,
        									    IN ZOO_INT32 timeout);
		
        /** @brief send_request
         * @param server_addr -address
         * @param req_msg -request message
         * @param req_len -size of message
         * @param retry_interval
         * @param timeout
         * @description : async interface
        */
        void send_request(IN void *req_msg,
                                IN ZOO_INT32 req_len,
        						IN ZOO_INT32 retry_interval,
                                            IN ZOO_BOOL signal = ZOO_FALSE);

        /**
         * @brief get_reply
         * @param server_addr -address
         * @param rep_msg -request message
         * @param rep_len -size of message
         * @param act_rep_len
         * @description : 
        */
        void get_reply(OUT void *rep_msg,
                            IN ZOO_INT32 rep_len,
                            IN ZOO_INT32 *act_rep_len,
				            IN ZOO_INT32 timeout);
         /**
         * @brief subscribe
         * @param server_name -
         * @param event_id -
         * @param request_message_lenght -
         * @param callback_function -
         * @description : 
        */
        void subscribe(IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
                                            IN MQ4A_CALLBACK_STRUCT * callback_struct,
                                            IN ZOO_INT32 event_id,INOUT ZOO_HANDLE *handle,
                                            INOUT void* context);

         /**
         * @brief unsubscribe
         * @description :
        */
         void unsubscribe(IN ZOO_INT32 event_id,IN ZOO_HANDLE handle);

         /**
         * @brief unsubscribe
         * @description :
        */
         OVERRIDE
         void shutdown();
    private:

        /**
         * @brief add_callback_fucntion
         * @description : add callback to map
        */
        void add_event_handle(IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
                                            IN MQ4A_CALLBACK_STRUCT * callback_struct,
                                            IN ZOO_UINT32 event_id,IN ZOO_HANDLE handle,
                                            INOUT void* context);
        /**
         * @brief remove_callback_function
         * @description :
        */
        void remove_event_handle(IN ZOO_UINT32 event_id,IN ZOO_HANDLE handle);

        /**
         * @brief get_callback_fucntion
         * @description : get callback from map
        */
        const CALLBACK_FUNCTION_VECTOR & get_callback_context(IN ZOO_UINT32 event_id);

        /**
         * @brief initialize
        */
        void initialize();

        /**
         * @brief listen
         * @description : event_loop
        */
        void loop_event(void);

		/**
         * @brief listen
         * @description : event_loop
        */
        void reconnect();

        /**
         * @brief consume one message from queue
         * @param mid -
         * @param data -
        */
        void consume_message(std::string & mid,const std::string & data);

        /**
         * @brief get message from queue by thread id
        */
        boost::shared_ptr<MESSAGE_CLASS> grab_message();

        /**
         * @brief get message from queue by message id
        */
        boost::shared_ptr<MESSAGE_CLASS> get_request_message(ZOO_UINT32 mid);

        /**
         * @brief get event id,for generate mid
        */
        std::vector<ZOO_UINT32> get_subscribe_events();
    private:
		/**
         * @brief socket context
        */
        zmq::context_t m_context;

		/**
         * @brief Local subscribe zmq socket
         */
        zmq::socket_t m_local_sub;

		/**
         * @brief Local publish zmq socket
         */
		zmq::socket_t m_local_pub;

		/**
         * @brief Local controller for shutdown background thread(subscribe)
         */
		zmq::socket_t m_controller;

		/**
         * @brief Local publish server address
         */
		std::string m_local_pub_server;

		/**
         * @brief Local sub server address
         */
        std::string m_local_sub_server;

		/**
         * @brief Internal controller server address
         */
		std::string m_controller_server;

		/**
         * @brief Lock for add / remove event
         */
        boost::shared_mutex m_event_readwrite_lock;

		/**
         * @brief Lock for sync request
         */
        boost::mutex m_sync_lock;
            
		/**
         * @brief The model init flag
         */
        std::once_flag m_initialize_flag;

		/**
         * @brief Local publish zmq socket
         */
        std::map<ZOO_UINT32,CALLBACK_FUNCTION_VECTOR> m_event_map;

        /**
         * @brief message dispatcher
         */
        boost::shared_ptr<MESSAGE_DISPATCHER_CLASS> m_message_dispatcher;

        /**
         * @brief thread pool
         */
        boost::shared_ptr<MQ_THREAD_POOL> m_thread_pool;
    };
}
#endif
