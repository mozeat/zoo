/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : MARKING_MODEL_INTERFACE.h
 * Description  : Present a model included with an marking code
 * History :
 * Version      Date				User			Comments
 * V1.0.0.0     2018-05-19			MOZEAT			Initialize
 ***************************************************************/
#ifndef MARKING_MODEL_INTERFACE_H_
#define MARKING_MODEL_INTERFACE_H_

extern "C"
{
#include "ZOO.h"
}
//#include "NOTIFY_PROPERTY_CHANGED_INTERFACE.h"
//#include "PROPERTY_CHANGED_OBSERVER_INTERFACE.h"
#include <vector>
#include <string.h>
#include "ZOO_COMMON_MACRO_DEFINE.h"
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>
#include "PARAMETER_EXCEPTION_CLASS.h"
#include <stdio.h>

using namespace std;

namespace ZOO_COMMON
{
    /**
     * @brief Present a model included with an marking code
     */
    class MARKING_MODEL_INTERFACE
    {
    public:
        /**
         * @brief Constructor
         */
        MARKING_MODEL_INTERFACE();

        /**
         * @brief Destructor
         */
        virtual ~MARKING_MODEL_INTERFACE();

        /**
         * @brief Get the marking_code attribute value
         */
        virtual const ZOO_CHAR* get_marking_code();

        /**
         * @brief Set the marking_code attribute value
         * @param marking_code    The new marking_code attribute value
         */
        virtual void set_marking_code(const ZOO_CHAR* marking_code);

        /**
         * @brief Clean mem
         */
        virtual void clean_mem();

        /**
         * @brief Get pointer
         * @return
         */
        template<typename T>
        boost::shared_ptr<T> get_pointer()
        {
            if (NULL == this->m_pointer)
            {
                this->m_pointer.reset(this);
            }

            return boost::dynamic_pointer_cast<T>(this->m_pointer);
        }

        /**
         * @brief Create shared pointer
         * @return
         */
        template<typename T>
        static boost::shared_ptr<T> get_pointer(MARKING_MODEL_INTERFACE* marking_model)
        {
            return boost::dynamic_pointer_cast<T>(marking_model->get_pointer<T>());
        }

    protected:
        /**
         * @brief The marking_code attribute
         */
        boost::shared_array<ZOO_CHAR> m_marking_code;

        /**
         * @brief Current pointer instance
         */
        boost::shared_ptr<MARKING_MODEL_INTERFACE> m_pointer;
    };

} /* namespace ZOO_COMMON */

#endif /* MARKING_MODEL_INTERFACE_H_ */
