/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : EVENT_BASE_CLASS.h
 * Description  : Defines an event bases, it will be used for Event Aggregator
 * History :
 * Version          Date				User				Comments
 * V1.0.0.          2018-06-18			weiwang.sun		        Initialize
 ***************************************************************/
#ifndef EVENT_BASE_CLASS_H_
#define EVENT_BASE_CLASS_H_

#include <vector>
#include "DELEGATE_CLASS.h"
#include <boost/shared_ptr.hpp>
#include "EVENT_BASE_INTERFACE.h"
#include "ZOO_COMMON_MACRO_DEFINE.h"
#include "THREAD_POOL.h"

namespace ZOO_COMMON
{
    /**
     * @brief Thread option define
     */
    typedef enum
    {
        PUBLISHER_THREAD = 0, BACKGROUND_THREAD
    } THREAD_OPTION;

    /**
     * @brief Defines an event bases, it will be used for Event Aggregator
     */
    template<class EventType>
    class EVENT_BASE_CLASS : public EVENT_BASE_INTERFACE
    {

    public:

        /**
         * @brief Default constructor
         */
        EVENT_BASE_CLASS()
        {

        }

        /**
         * @brief Default destructor
         */
        virtual ~EVENT_BASE_CLASS()
        {
            this->m_subscriptions.clear();
        }

        /**
         * @brief Subscribe event with a callback function.
         * It will be executed by the time the registered event is raised
         * @param func  The callback function
         */
        void subscribe(void (*func)(boost::shared_ptr<EventType>))
        {
            boost::shared_ptr<DELEGATE_CLASS<EventType> > subscribe(
                    new DELEGATE_CLASS<EventType>(func));
            this->m_subscriptions.push_back(subscribe);
        }

        /**
         * @brief Subscribe event with a callback function with specified parameter.
         * It will be executed by the time the registered event is raised
         * @param object    The owner of callback method
         * @param method    The callback method
         */
        template<class T, class Method>
        void subscribe(T *object, Method method)
        {
            boost::shared_ptr<DELEGATE_CLASS<EventType> > subscribe(
                    new DELEGATE_CLASS<EventType>(object, method));
            this->m_subscriptions.push_back(subscribe);
        }

        /**
         * @brief Unsubscribe event
         * @param object    The owner of callback method
         * @param method    The callback method
         */
        template<class T, class Method>
        void unsubscribe(T *object, Method method)
        {
            if (!this->m_subscriptions.empty())
            {
                int size = this->m_subscriptions.size();
                for (int i = 0; i < size; i++)
                {
                    boost::shared_ptr<DELEGATE_CLASS<EventType> > subscription =
                            this->m_subscriptions[i];
                    if (NULL != subscription)
                    {
                        if (subscription->is_register(object, method))
                        {
                            this->m_subscriptions.erase(this->m_subscriptions.begin() + i);
                            break;
                        }
                    }
                }
            }
        }

        /**
         * @brief Publish event for subscribers
         */
        void publish(boost::shared_ptr<EventType> message)
        {
            this->publish(message, PUBLISHER_THREAD);
        }

        /**
         * @brief Publish event for subscribers
         */
        void publish(boost::shared_ptr<EventType> message, THREAD_OPTION thread_option)
        {
            switch (thread_option)
            {
                case PUBLISHER_THREAD:
                    this->do_publish(message);
                    break;

                case BACKGROUND_THREAD:
                    THREAD_POOL::get_instance()->queue_working(
                            boost::bind(&EVENT_BASE_CLASS::do_publish, this, message), NULL);
                    break;

                default:
                    break;
            }

        }

    private:
        /**
         * @brief Publish message
         */
        void do_publish(boost::shared_ptr<EventType> message)
        {
            int size = this->m_subscriptions.size();
            for (int i = 0; i < size; i++)
            {
                boost::shared_ptr<DELEGATE_CLASS<EventType> > subscription =
                        this->m_subscriptions[i];
                if (NULL != subscription)
                {
                    subscription->execute(message);
                }
            }
        }

        /**
         * @brief The subscription class
         */
        std::vector<boost::shared_ptr<DELEGATE_CLASS<EventType> > > m_subscriptions;
    };

} /* namespace ZOO_COMMON */

#endif /* EVENT_BASE_CLASS_H_ */
