/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : CALL_BACK_CLASS.h
 * Description  : This is a wrapper of a callback function, it will be invoked to execute a callback
 *                function which has been registered before when event raised
 * History :
 * Version          Date				User				Comments
 * V1.0.0.0         2018-6-3			weiwang		        Initialize
 ***************************************************************/
#ifndef CALL_BACK_CLASS_H_
#define CALL_BACK_CLASS_H_
extern "C"
{
#include "ZOO.h"
}
#include <boost/shared_ptr.hpp>
#include "ZOO_COMMON_MACRO_DEFINE.h"

namespace ZOO_COMMON
{
    /**
     * @brief Presents a callback function, it will be executed when event raised
     */
    template<typename EventType>
    class CALL_BACK_CLASS
    {
    public:
        /**
         * @brief Destructor
         */
        virtual ~CALL_BACK_CLASS()
        {
        }

        /**
         * @brief Execute a callback
         */
        virtual void execute(boost::shared_ptr<EventType> event) = 0;
    };

} /* namespace  ZOO_COMMON */

#endif /* CALL_BACK_CLASS_H_ */
