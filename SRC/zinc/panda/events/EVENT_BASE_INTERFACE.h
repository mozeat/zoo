/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : EVENT_BASE_INTERFACE.h
 * Description  : Defines an event bases, it will be used for Event Aggregator
 * History :
 * Version          Date				User				Comments
 * V1.0.0.0         2018-06-28			MOZEAT		        Initialize
 ***************************************************************/
#ifndef EVENT_BASE_INTERFACE_H_
#define EVENT_BASE_INTERFACE_H_
#include "ZOO_COMMON_MACRO_DEFINE.h"
namespace ZOO_COMMON
{

    /**
     * @brief Defines an event bases, it will be used for Event Aggregator
     */
    class EVENT_BASE_INTERFACE
    {

    public:

        /**
         * @brief Default constructor
         */
        EVENT_BASE_INTERFACE()
        {

        }

        /**
         * @brief Default destructor
         */
        virtual ~EVENT_BASE_INTERFACE()
        {
        }
    };
}/* ZOO_COMMON */

#endif /* EVENT_BASE_INTERFACE_H_ */
