/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : EVENT_AGGREGATOR_CLASS.h
 * Description  : This class implements mechanism to subscribe/publish events
 * History :
 * Version          Date				User				Comments
 * V1.0.0         2018-6-28			mozeat		        Initialize
 ***************************************************************/
#ifndef EVENT_AGGREGATOR_CLASS_H_
#define EVENT_AGGREGATOR_CLASS_H_
#include <boost/shared_ptr.hpp>
#include <string>
#include <map>
#include "EVENT_BASE_CLASS.h"
#include "ZOO_COMMON_MACRO_DEFINE.h"
namespace ZOO_COMMON
{

    /**
     * @brief Class description
     */
    class EVENT_AGGREGATOR_CLASS
    {
    public:

        /**
         * @brief Deconstructor
         */
        virtual ~EVENT_AGGREGATOR_CLASS();

        /**
         * @brief Clear all event
         */
        static void clear()
        {
            get_instance()->clear_base();
        }

        /**
         * @brief Get event which has been registered before
         * @return
         */
        template<class MessageType>
        static boost::shared_ptr<EVENT_BASE_CLASS<MessageType> > get_event()
        {
            boost::shared_ptr<EVENT_BASE_CLASS<MessageType> > event =
                    get_instance()->get_event_base<MessageType>();
            return event;
        }

    private:

        /**
         * @brief Clear all event
         */
        void clear_base()
        {
            while (!this->m_events.empty())
            {
                this->m_events.clear();
            }
        }

        /**
         * @brief Get event which has been registered before
         * @return
         */
        template<class MessageType>
        boost::shared_ptr<EVENT_BASE_CLASS<MessageType> > get_event_base()
        {
            std::string event_type = typeid(MessageType).name();
            boost::shared_ptr<EVENT_BASE_CLASS<MessageType> > event;
            std::map<std::string, boost::shared_ptr<EVENT_BASE_INTERFACE> >::iterator find_ptr =
                    this->m_events.find(event_type);
            if (find_ptr == this->m_events.end())
            {
                event.reset(new EVENT_BASE_CLASS<MessageType>());
                this->m_events[event_type] = boost::dynamic_pointer_cast<EVENT_BASE_INTERFACE>(
                        event);
            }
            else
            {
                boost::shared_ptr<EVENT_BASE_INTERFACE> tmp = find_ptr->second;
                event = boost::static_pointer_cast<EVENT_BASE_CLASS<MessageType> >(tmp);
            }

            return event;
        }

        /**
         * @brief Get instance
         */
        static boost::shared_ptr<EVENT_AGGREGATOR_CLASS> get_instance();

        /**
         * @brief Default constructor
         */
        EVENT_AGGREGATOR_CLASS();

        /**
         * @brief The singleton instance
         */
        static boost::shared_ptr<EVENT_AGGREGATOR_CLASS> m_instance;

        /**
         * @brief The event collections
         */
        std::map<std::string, boost::shared_ptr<EVENT_BASE_INTERFACE> > m_events;
    }
    ;

} /* namespace ZOO_COMMON */

#endif /* EVENT_AGGREGATOR_CLASS_H_ */
