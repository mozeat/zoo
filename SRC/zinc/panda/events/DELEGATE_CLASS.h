/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : STATIC_CALL_BACK_CLASS.h
 * Description  : This is a wrapper of a callback function, it will be invoked to execute a callback
 *                function which has been registered before when event raised
 * History :
 * Version      Date				User				Comments
 * V1.0.0      2018-06-20			weiwang.sun		Initialize
 ***************************************************************/
#ifndef DELEGATE_CLASS_H_
#define DELEGATE_CLASS_H_
#include "STATIC_CALL_BACK_CLASS.h"
#include "METHOD_CALL_BACK_CLASS.h"
#include <boost/shared_ptr.hpp>
#include "ZOO_COMMON_MACRO_DEFINE.h"
namespace ZOO_COMMON
{
    /**
     * @brief This is a wrapper of a callback function, it will be invoked to execute a callback
     *        function which has been registered before when event raised
     */
    template<typename EventType>
    class DELEGATE_CLASS : public CALL_BACK_CLASS<EventType>
    {
    public:

        /**
         * @brief Static delegate constructor
         */
        DELEGATE_CLASS(void (*func)(boost::shared_ptr<EventType>))
                : m_callback(new STATIC_CALL_BACK_CLASS<EventType>(func))
        {
        }

        /**
         * @brief Function delegate constructor
         */
        template<typename T, typename Method>
        DELEGATE_CLASS(T *object, Method method)
                : m_callback(new METHOD_CALL_BACK_CLASS<EventType, T, Method>(object, method))
        {
        }

        /**
         * @brief Deconstructor
         */
        ~DELEGATE_CLASS(void)
        {
        }

        /**
         * @brief Execute method
         */
        void execute(boost::shared_ptr<EventType> message_type)
        {
            this->m_callback->execute(message_type);
        }

        /**
         * @brief Register a callback function, it will be executed when event raised
         * @param object    The owner class of callback method
         * @param method    The callback method
         * @return True: success, False: otherwise
         */
        template<typename T, typename Method>
        bool is_register(T *object, Method method)
        {
            METHOD_CALL_BACK_CLASS<EventType, T, Method>* call_back =
                    static_cast<METHOD_CALL_BACK_CLASS<EventType, T, Method>*>(this->m_callback.get());
            if (NULL != call_back)
            {
                return call_back->is_register(object, method);
            }

            return false;
        }

    private:
        /**
         * @brief The call back register
         */
        boost::shared_ptr<CALL_BACK_CLASS<EventType> > m_callback;

    };

} /* namespace ZOO_COMMON */

#endif /* DELEGATE_CLASS_H_ */
