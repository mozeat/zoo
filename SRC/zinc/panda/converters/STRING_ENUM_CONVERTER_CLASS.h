/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : STRING_ENUM_CONVERTER_BASE_CLASS.h
 * Description  : The class takes responsibility to mapping between enum value and enum string
 * History :
 * Version      Date                User            Comments
 * V1.0.0.0     2015-05-22          weiwang          Initialize
 ***************************************************************/
#ifndef STRING_ENUM_CONVERTER_BASE_CLASS_H_
#define STRING_ENUM_CONVERTER_BASE_CLASS_H_

extern "C"
{
#include "ZOO.h"
}
#include <string>
#include <map>
#include <cassert>

// Helper macros
#define BEGIN_STRING_ENUM(EnumerationName)                                      \
        template <> class STRING_ENUM_CONVERTER_CLASS<EnumerationName> :                            \
            public STRING_ENUM_CONVERTER_BASE_CLASS<STRING_ENUM_CONVERTER_CLASS<EnumerationName>, EnumerationName>   \
        {                                                                           \
        public:                                                                 \
            static void register_enumerators()
//      {

#define STRING_ENUM(EnumeratorName)                                             \
                register_enumerator(EnumeratorName, #EnumeratorName);
//      }

#define END_STRING_ENUM                                                         \
        }

/**
 * @brief The STRING_ENUM_CONVERTER_CLASS base class
 */
template<class DerivedType, class EnumType>
class STRING_ENUM_CONVERTER_BASE_CLASS
{
public:
    /**
     * @brief Converts from an enumerator to a string.
     * Returns an empty string if the enumerator was not registered.
     */
    static const ZOO_CHAR* to_string(const EnumType e);

    /**
     * @brief Converts from a string to an enumerator.
     * Returns true if the conversion is successful; false otherwise.
     */
    static const EnumType to_enum(const std::string str);

protected:
    /**
     * @brief Constructor
     */
    explicit STRING_ENUM_CONVERTER_BASE_CLASS();

    /**
     * @brief Destructor
     */
    ~STRING_ENUM_CONVERTER_BASE_CLASS();

private:
    /**
     * @brief Copy Constructor
     * @param
     */
    STRING_ENUM_CONVERTER_BASE_CLASS(const STRING_ENUM_CONVERTER_BASE_CLASS&);

    /**
     * @brief Assignment Operator
     */
    const STRING_ENUM_CONVERTER_BASE_CLASS &operator =(const STRING_ENUM_CONVERTER_BASE_CLASS&);

protected:
    /**
     * @brief Association mapping
     */
    typedef std::map<const std::string, EnumType> ASSOCIATION_MAP;

    /**
     * @brief Use this helper function to register each enumerator
     * and its string representation.
     */
    static void register_enumerator(const EnumType e, const std::string eStr);

private:
    /**
     * @brief Get association map
     */
    static ASSOCIATION_MAP& get_map();

};

/**
 * @brief The STRING_ENUM_CONVERTER_CLASS structure
 * Note: Specialize this class for each enumeration, and implement
 * the register_enumerators() function.
 */
template<class EnumType>
class STRING_ENUM_CONVERTER_CLASS : public STRING_ENUM_CONVERTER_BASE_CLASS<STRING_ENUM_CONVERTER_CLASS<EnumType>, EnumType>
{
public:
    static void register_enumerators();
};

/**
 * @brief Get association map
 */
template<class D, class E>
typename STRING_ENUM_CONVERTER_BASE_CLASS<D, E>::ASSOCIATION_MAP &STRING_ENUM_CONVERTER_BASE_CLASS<D, E>::get_map()
{
    // A static map of associations from strings to enumerators
    static ASSOCIATION_MAP m_association_map;
    static ZOO_BOOL m_is_first_access = true;

    // If this is the first time we're accessing the map, then populate it.
    if (m_is_first_access)
    {
        m_is_first_access = false;
        D::register_enumerators();
        //assert( !m_association_map.empty() );
    }

    return m_association_map;
}

/**
 * @brief Use this helper function to register each enumerator
 * and its string representation.
 */
template<class D, class E>
void STRING_ENUM_CONVERTER_BASE_CLASS<D, E>::register_enumerator(const E e, const std::string eStr)
{
    const ZOO_BOOL bRegistered = get_map().insert(typename ASSOCIATION_MAP::value_type(eStr, e)).second;
    //assert( bRegistered );
    (void) sizeof(bRegistered); // This is to avoid the pesky 'unused variable' warning in Release Builds.
}

/**
 * @brief Converts from an enumerator to a string.
 * Returns an empty string if the enumerator was not registered.
 */
template<class D, class E>
const ZOO_CHAR* STRING_ENUM_CONVERTER_BASE_CLASS<D, E>::to_string(const E e)
{
    for (;;) // Code block
    {
        // Search for the enumerator in our map
        typename ASSOCIATION_MAP::const_iterator i;
        for (i = get_map().begin(); i != get_map().end(); ++i)
            if ((*i).second == e)
                break;

        // If we didn't find it, we can't do this conversion
        if (i == get_map().end())
            break;

        // Keep searching and see if we find another one with the same value
        typename ASSOCIATION_MAP::const_iterator j(i);
        for (++j; j != get_map().end(); ++j)
            if ((*j).second == e)
                break;

        // If we found another one with the same value, we can't do this conversion
        if (j != get_map().end())
            break;

        // We found exactly one string which matches the required enumerator
        return ((*i).first).c_str();
    }

    // We couldn't do this conversion; return an empty string.
    static const std::string dummy;
    return dummy.c_str();
}

/**
 * @brief Converts from a string to an enumerator.
 * Returns true if the conversion is successful; false otherwise.
 */
template<class D, class E>
const E STRING_ENUM_CONVERTER_BASE_CLASS<D, E>::to_enum(const std::string str)
{
    // Search for the string in our map.
    const typename ASSOCIATION_MAP::const_iterator itr(get_map().find(str));
    return (*itr).second;
}

#endif /* STRING_ENUM_CONVERTER_BASE_CLASS_H_ */
