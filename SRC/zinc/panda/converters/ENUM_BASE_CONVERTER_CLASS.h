/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : ENUM_BASE_CONVERTER_CLASS.h
 * Description  : The class to support to convert string to enum and otherwise.
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0       2018-06-21	    weiwang.sun		Initialize
 ***************************************************************/
#ifndef ENUM_BASE_CONVERTER_CLASS_H_
#define ENUM_BASE_CONVERTER_CLASS_H_

/*##########################################################################*
 Header File
 *###########################################################################*/
#include <iostream>
#include <cstring>
#include <string>
#include <map>
#include <iostream>
/*##########################################################################*
 Function Declaration
 *###########################################################################*/
#define REGISTER_ENUM_BEGIN(name)   class ENUM_##name :public ENUM_BASE_CONVERTER_CLASS<name>\
                                    {\
                                    public: \
                                        ~ENUM_##name(){}\
                                         ENUM_##name()
//{
#define REGISTER_ENUM(item)                 this->operator[](#item) = item
//}
#define REGISTER_ENUM_END           }

#define ENUM_CONVERTER(name)        ENUM_##name

template<typename E>
class ENUM_BASE_CONVERTER_CLASS : public std::map<const char*, E>
{
public:
    typedef E TYPE;
    /**
     * Convert from string to enum
     * @param value is output enum value
     * @param text is enum string
     * @return TRUE if found enum, otherwise return FALSE
     */
    bool to_enum(E& value, const char* str)
    {
        for (typename std::map<const char*, E>::iterator i = this->begin(); i != this->end(); i++)
        {
            if (strcmp(i->first, str) == 0)
            {
                value = i->second;
                return true;
            }
        }
        return false;
    }

    /**
     * Convert from enum to string
     * @param value is enum value
     * @return string of enum
     */
    const char* to_string(E value)
    {
        for (typename std::map<const char*, E>::iterator i = this->begin(); i != this->end(); i++)
        {
            if (i->second == value)
            {
                return i->first;
            }
        }
        return "";
    }
};

#endif /* ENUM_BASE_CONVERTER_CLASS_H_ */
