/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : PARAMETER_EXCEPTION_CLASS.h
 * Description  : Defines a custom exception which will be raised up to upper layer
 * History :
 * Version      Date            User            Comments
 * V1.0.0     2018-06-04      MOZEAT       Create file
 ***************************************************************/
#ifndef PARAMETER_EXCEPTION_CLASS_H_
#define PARAMETER_EXCEPTION_CLASS_H_

extern "C"
{
#include "ZOO.h"
}
#include <ZOO_COMMON_MACRO_DEFINE.h>
#include <exception>
#include <string.h>

using namespace std;

namespace ZOO_COMMON
{
    /**
     * @brief Invalid access memory exception error code
     */
    #define MAX_MESSAGE_LENGTH 250
    #define UNHANDLE_EXCEPTION_ERROR                               (0x1005E783)
    #define INVALID_ACCESS_MEMORY_EXCEPTION_ERROR                  (0x1005E784)

    /**
     */
    static const ZOO_INT32 PARAMETER_EXCEPTION_LENGTH(1000);

    /**
     * @brief Defines a custom exception which will be raised up to upper layer
     */
    class PARAMETER_EXCEPTION_CLASS : public std::exception
    {
    public:
        /**
         * @brief Constructor
         * @param error_code        Error code
         * @param error_message     Error message
         * @param inner_exception   Inner exception
         */
        PARAMETER_EXCEPTION_CLASS(ZOO_INT32 error_code, const ZOO_CHAR* error_message,
                                  std::exception* inner_exception);

        /**
         * @brief Destructor
         */
        virtual ~PARAMETER_EXCEPTION_CLASS() throw ();

        /**
         * @brief Get the error_code attribute value
         */
        ZOO_INT32 get_error_code();

        /**
         * @brief Set the error_code attribute value
         * @param error_code    The new error_code attribute value
         */
        void set_error_code(ZOO_INT32 error_code);

        /**
         * @brief Get the error_message attribute value
         */
        const ZOO_CHAR* get_error_message();

        /**
         * @brief Set the error_message attribute value
         * @param error_message    The new error_message attribute value
         */
        void set_error_message(const ZOO_CHAR* error_message);

        /**
         * @brief Get the inner_exception attribute value
         */
        std::exception* get_inner_exception();

        /**
         * @brief Set the inner_exception attribute value
         * @param inner_exception    The new inner_exception attribute value
         */
        void set_inner_exception(std::exception* inner_exception);

        /**
         * @brief Free
         */
        void free();

    private:
        /**
         * @brief The error_code attribute
         */
        ZOO_INT32 m_error_code;

        /**
         * @brief The error_message attribute
         */
        ZOO_CHAR m_error_message[PARAMETER_EXCEPTION_LENGTH];

        /**
         * @brief The inner_exception attribute
         */
        std::exception* m_inner_exception;
    };
}

#endif /* PARAMETER_EXCEPTION_CLASS_H_ */
