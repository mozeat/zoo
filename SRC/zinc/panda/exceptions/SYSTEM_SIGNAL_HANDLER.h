/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : COMMON
 * File Name      : SYSTEM_SIGNAL_HANDLER.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#ifndef SYSTEM_SIGNAL_HANDLER_H
#define SYSTEM_SIGNAL_HANDLER_H
#include <functional>
#include <string>
#include <boost/smart_ptr.hpp>
#include <signal.h>
namespace ZOO_COMMON
{
    class SYSTEM_SIGNAL_HANDLER
    {
    public:
        /**
         * @brief Destructor
         */
        virtual ~SYSTEM_SIGNAL_HANDLER();

        /**
         * @brief instance
         */
        static boost::shared_ptr<SYSTEM_SIGNAL_HANDLER> get_instance();
    public:

        /**
         * @brief Raise a system defined signal
         * @param sig_id           this is the system define signal id,such as SIGINT
         */
        void raise_signal(int sig_id) noexcept;

        /**
         * @brief Reset system signal to default behavior, call signal(sig_id,SIG_DFL)
         * @param sig_id           this is the system define signal id,such as SIGINT
         */
        void reset_signal_behavior(int sig_id) noexcept;

        /**
         * @brief Ignore system signal ,call signal(sig_id, SIG_IGN);
         * @param sig_id           this is the system define signal id,such as SIGINT
         */
        void ignore_signal_behavior(int sig_id) noexcept;

        /**
         * @brief Register system signal
         * @param sig_id           this is the system define signal id,such as SIGINT
         * @param handler          callback for signal process
         */

        void register_signal_handler(std::string componet_id,int sig_id,std::function<void(int)> & handler) throw();

        /**
         * @brief Register system signal use default handler,the default handler
         * will record the stack informations to the log file when capture an associated signal
         * @param sig_id           this is the system define signal id,such as SIGINT
         */
        void register_signal_by_default_handler(std::string componet_id,int sig_id) throw();
    private:
        /**
         * @brief Constructor
         */
        SYSTEM_SIGNAL_HANDLER();

        /**
         * @brief default handler,the default handler
         * will record the stack informations to the log file when capture an associated signal
         * @param sig_id           this is the system define signal id,such as SIGINT
         */
        static void default_signal_handler(IN int signal_id);

        /**
         * @brief Convert from signal int value to string
         */
        static const ZOO_CHAR * to_string(int signal_id);
    private:
        /**
         * @brief The instance
         */
        static boost::shared_ptr<SYSTEM_SIGNAL_HANDLER> m_instance;
    };
}

#endif // SYSTEM_SIGNAL_HANDLER_H
