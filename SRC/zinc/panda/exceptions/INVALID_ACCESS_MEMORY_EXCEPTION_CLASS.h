/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : INVALID_ACCESS_MEMORY_EXCEPTION_CLASS.h
 * Description  : Defines an exception which will be raised when there is any invalid memory accessing
 * History :
 * Version      Date				User			Comments
 * V1.0.0.0     2018-06-10			MOZEAT			Initialize
 ***************************************************************/
#ifndef INVALID_ACCESS_MEMORY_EXCEPTION_CLASS_H_
#define INVALID_ACCESS_MEMORY_EXCEPTION_CLASS_H_

#include "PARAMETER_EXCEPTION_CLASS.h"

namespace ZOO_COMMON
{

    /**
     * @brief Defines an exception which will be raised when there is any invalid memory accessing
     */
    class INVALID_ACCESS_MEMORY_EXCEPTION_CLASS : public PARAMETER_EXCEPTION_CLASS
    {
    public:
        /**
         * @brief Constructor
         * @param error_code        Error code
         * @param error_message     Error message
         * @param inner_exception   Inner exception
         */
        INVALID_ACCESS_MEMORY_EXCEPTION_CLASS(ZOO_INT32 error_code, ZOO_CHAR* error_message,
                                              exception* inner_exception)
                : PARAMETER_EXCEPTION_CLASS(error_code, error_message, inner_exception)
        {

        }

        /**
         * @brief Constructor
         * @param error_code        Error code
         * @param error_message     Error message
         * @param inner_exception   Inner exception
         */
        INVALID_ACCESS_MEMORY_EXCEPTION_CLASS(ZOO_INT32 error_code, const ZOO_CHAR* error_message,
                                              exception* inner_exception)
                : PARAMETER_EXCEPTION_CLASS(error_code, (ZOO_CHAR*) error_message, inner_exception)
        {

        }

        /**
         * @brief Destructor
         */
        virtual ~INVALID_ACCESS_MEMORY_EXCEPTION_CLASS() throw ()
        {

        }
    };

} /* namespace ZOO_COMMON */

#endif /* INVALID_ACCESS_MEMORY_EXCEPTION_CLASS_H_ */
