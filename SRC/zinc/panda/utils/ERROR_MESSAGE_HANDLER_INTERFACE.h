/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : HANDLE_ERROR_MESSAGE_INTERFACE.h
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-06-20			MOZEAT		Initialize
 ***************************************************************/
#ifndef ERROR_MESSAGE_HANDLER_INTERFACE_H_
#define ERROR_MESSAGE_HANDLER_INTERFACE_H_

extern "C"
{
#include <ZOO.h>
}
#include "PARAMETER_EXCEPTION_CLASS.h"
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>
#include <boost/thread.hpp>
#include <boost/thread/condition_variable.hpp>

namespace ZOO_COMMON
{

    /**
     * @brief This class is used to locate robot instance.
     */
    class ERROR_MESSAGE_HANDLER_INTERFACE
    {
    public:

        /**
         * @brief Default destructor.
         */
        ERROR_MESSAGE_HANDLER_INTERFACE();

        /**
         * @brief Default destructor.
         */
        virtual ~ERROR_MESSAGE_HANDLER_INTERFACE();

        /**
         * @brief handler error code
         * @param exception The throw exception instance
         */
        virtual void handle_error(PARAMETER_EXCEPTION_CLASS& exception);

        /**
         * @brief Has error
         * @return
         */
        virtual ZOO_BOOL has_error();

        /**
         * @brief Reset handle
         */
        virtual void reset_handler();

        /**
         * @brief Get message
         * @return
         */
        virtual const ZOO_CHAR* get_message();

        /**
         * @brief Get error code
         * @return
         */
        virtual ZOO_INT32 get_error_code();

    private:

        /**
         * @brief Has error occurs
         */
        ZOO_CHAR m_message[MAX_MESSAGE_LENGTH];

        /**
         * @brief Handler exception
         */
        ZOO_INT32 m_error_code;

        /**
         * @brief Has error
         */
        ZOO_BOOL m_has_error;

        /**
         * @brief The concurrency lock
         */
        boost::recursive_mutex m_syn_lock;
    };
}

#endif /* ERROR_MESSAGE_HANDLER_INTERFACE_H_ */
