/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : ENVIRONMENT_UTILITY_CLASS.h
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0       2018-06-28			MOZEAT Initialize
 ***************************************************************/
#ifndef ENVIRONMENT_UTILITY_CLASS_H_
#define ENVIRONMENT_UTILITY_CLASS_H_
extern "C"
{
#include "ZOO.h"
}

#include <string>
#include <map>
#include <boost/filesystem.hpp>

using namespace std;

namespace ZOO_COMMON
{

#define SLASH_CHAR "/"

    class ENVIRONMENT_UTILITY_CLASS
    {
    public:

        /**
         * @brief Default constructor
         */
        ENVIRONMENT_UTILITY_CLASS();

        /**
         * @brief Deconstructor
         */
        virtual ~ENVIRONMENT_UTILITY_CLASS();

        /**
         * @brief Get current user name
         */
        static string get_current_user();

        /**
         * @brief Combine path
         * @param dir
         * @param file_name
         * @return
         */
        static string combine_path(string dir, string file_name);

        /**
         * @brief Get full path
         * @param dir
         * @param file_name
         * @return
         */
        static string get_full_path(string file_name);

        /**
         * @brief Get dir
         * @param file_path
         * @return
         */
        static string get_dir(string file_path);

        /**
         * @brief Get configure base
         * @return
         */
        static string get_configure_base();

        /**
         * @brief Get parent dir
         * @param level
         * @return
         */
        static string get_parent_dir(string dir, int level);

        /**
         * @brief Get the execute base
         * @return
         */
        static string get_execute_path();

        /**
         * @brief Get thread id
         * @return
         */
        static string get_thread_id();

        /**
         * @brief Get thread number
         * @return
         */
        static ZOO_INT32 get_thread_number();

        /**
         * @brief Get all files located in specified directory recursively
         * @param dir       The directory contains files
         * @param files     The files located in specified directory
         * @return Error code for this function
         */
        static ZOO_INT32 get_all_files(string dir, map<string, string>& files, string file_type =
                                                ".xml");

        /**
         * @brief Convert path to string
         */
        static string convert_path_to_string(boost::filesystem::path path);

        /**
         * @brief Get trace info
         * @param module
         * @param function
         * @param context
         * @return
         */
        static string get_trace_info(const ZOO_CHAR* module, const ZOO_CHAR* function,
                                     const ZOO_CHAR* context = NULL);
    };

} /* namespace ZOO_COMMON */

#endif /* ENVIRONMENT_UTILITY_CLASS_H_ */
