/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO 
 * Component ID : CM
 * File Name    : CM4A_if.h
 * Description  : 数据追踪
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef CM4A_IF_H
#define CM4A_IF_H
#include <ZOO.h>
#include <CM4A_type.h>
#include <stddef.h>
/**************************************************************************
INTERFACE <ZOO_INT32 CM4A_write_data>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         const char* component_id
                void * data
                size_t length
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                CM4A_SYSTEM_ERR -- 系统错误
                CM4A_PARAMETER_ERR -- 参数错误
<Description>:  写配置文件，将数据结构体转换成文件内容
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CM4A_write_data(IN const char * component_id,IN void * data,IN size_t length);

/**************************************************************************
INTERFACE <ZOO_INT32 CM4A_read_data>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         const char* identity
                void * data
                size_t length
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                CM4A_SYSTEM_ERR -- 系统错误
                CM4A_PARAMETER_ERR -- 参数错误
<Description>:  读取配置文件，并将文件内容转换成数据结构体
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CM4A_read_data(IN const char * component_id,INOUT void * data,IN size_t length);

/**************************************************************************
INTERFACE <ZOO_INT32 CM4A_read_config>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         CM4A_GROUP_STRUCT *conf
                const char *component_id
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                CM4A_SYSTEM_ERR -- 系统错误
                CM4A_PARAMETER_ERR -- 参数错误
<Description>:  读取配置文件内容，每一行内容转成CM4A_ROW_DATA_STRUCT结构
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CM4A_read_config(IN const char *component_id, OUT CM4A_GROUP_STRUCT *conf);

/**************************************************************************
INTERFACE <ZOO_INT32 CM4A_write_config>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         CM4A_GROUP_STRUCT *conf
                const char *component_id
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                CM4A_SYSTEM_ERR -- 系统错误
                CM4A_PARAMETER_ERR -- 参数错误
<Description>:  写配置文件内容，每一行内容可以通过CM4A_ROW_DATA_STRUCT结构进行修改
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CM4A_write_config(IN const char *component_id, IN CM4A_GROUP_STRUCT *conf);

/*****************************************************************************************
 *@brief 配置文件变更通知订阅接口，CM4A_write_config 和 CM4A_write_data接口调用会触发订阅消息
 *@return -OK
          -CM4A_SYSTEM_ERR
 *@precondition
 *@postcondition
 ****************************************************************************************/
typedef void(*CM4A_COMPONENT_CHANGE_CALLBACK_FUNCKTION)(IN CM4A_STATUS_STRUCT status,
                                                                  IN ZOO_INT32 error_code,
                                                                  IN void *context);
ZOO_EXPORT ZOO_INT32 CM4A_component_change_subscribe(IN CM4A_COMPONENT_CHANGE_CALLBACK_FUNCKTION callback_function,
                                                     OUT ZOO_UINT32 *handle,
                                                     INOUT void *context);

/*****************************************************************************************
 *@brief 配置文件变更通知去订阅接口，CM4A_write_config 和 CM4A_write_data接口调用将不会收到
 			配置文件变更的订阅通知
 *@return -OK
          -CM4A_SYSTEM_ERR
 *@precondition
 *@postcondition
 ****************************************************************************************/

ZOO_EXPORT ZOO_INT32 CM4A_component_change_unsubscribe(IN ZOO_HANDLE handle);

#endif
