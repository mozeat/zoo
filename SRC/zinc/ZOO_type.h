/*******************************************************************************
* Copyright (C) 2017, 上海蔚来汽车有限公司
* All rights reserved.
* 产品�?: ZOO
* 所属组�?:
* 模块名称 :
* 文件名称 : ZOO_type.h
* 概要描述 : 该文件定义了基于C/C++语言的基本类�?
* 历史记录 :
* 版本      日期        作�?   内容
* 1.0    2017-12-7      孙伟�?  新建
******************************************************************************/

#ifndef ZOO_TYPE_H_
#define ZOO_TYPE_H_

#include "ZOO_station.h"

#define ZOO_CHAR_BIT   8                   /* max # of bits in a “char�?*/
#define ZOO_CHAR_MIN   (-128)              /* min value of a “signed char�?*/
#define ZOO_CHAR_MAX   127                 /* max value of a “signed char�?*/
#define ZOO_SHRT_MIN   (-32768)            /* min value of a “short int�?*/
#define ZOO_SHRT_MAX   32767               /* max value of a “short int�?*/
#define ZOO_USHRT_MAX  65535               /* max value of “unsigned short int�?*/
#define ZOO_INT_MIN    (-2147483647-1)     /* min value of an “int�?*/
#define ZOO_INT_MAX    2147483647          /* max value of an “int�?*/
#define ZOO_UINT_MAX   4294967295U         /* max value of an “unsigned int�?*/
#define ZOO_LONG_MIN   (-2147483647L-1L)
#define ZOO_LONG_MAX   2147483647L         /* max value of a “long int�?*/
#define ZOO_ULONG_MAX  4294967295UL        /* max value of “unsigned long int�?*/
#define ZOO_LLONG_MIN  (-9223372036854775807LL-1LL)
#define ZOO_LLONG_MAX  9223372036854775807LL
#define ZOO_ULLONG_MAX 18446744073709551615ULL

#define ZOO_INT8_MAX (127)
#define ZOO_INT16_MAX (32767)
#define ZOO_INT32_MAX (2147483647)
#define ZOO_INT64_MAX (9223372036854775807LL)
#define ZOO_INT8_MIN (-128)
#define ZOO_INT16_MIN (-32767-1)
#define ZOO_INT32_MIN (-2147483647-1)
#define ZOO_INT64_MIN (-9223372036854775807LL-1)

#define ZOO_UINT8_MAX (255U)
#define ZOO_UINT16_MAX (65535U)
#define ZOO_UINT32_MAX (4294967295U)
#define ZOO_UINT64_MAX (18446744073709551615ULL)

#define  ZOO_SIZEOF_CHAR               1
#define  ZOO_SIZEOF_SHORT              2
#define  ZOO_SIZEOF_INT                4
#define  ZOO_SIZEOF_LLONG              8
#define  ZOO_SIZEOF_DOUBLE             8

#if TARGET_OS == LINUX32_OS
#define  ZOO_SIZEOF_LONG               4
#define  ZOO_SIZEOF_VOID_P             4
#elif TARGET_OS == LINUX64_OS
#define  ZOO_SIZEOF_LONG               8
#define  ZOO_SIZEOF_VOID_P             8
#endif
#if TARGET_MACHINE == ZOO_X86_32 || TARGET_MACHINE == ZOO_X86_64
#define  ZOO_SIZEOF_LONG_DOUBLE        12
#endif

typedef char                        ZOO_CHAR;
typedef char                        ZOO_INT8;
typedef unsigned char               ZOO_UINT8;
typedef short                       ZOO_INT16;
typedef unsigned short              ZOO_UINT16;
typedef int                         ZOO_INT32;
typedef unsigned int                ZOO_UINT32;
typedef long long                   ZOO_INT64;
typedef unsigned long long          ZOO_UINT64;
typedef float                       ZOO_FLOAT;
typedef double                      ZOO_DOUBLE;
typedef long double                 ZOO_LDOUBLE;
typedef unsigned long               ZOO_ULONG32;
typedef char*                       ZOO_STRING;
typedef long                        ZOO_LONG;
typedef void                        *ZOO_LPVOID;
typedef ZOO_LONG                   ZOO_TIME_T;//time_t
typedef ZOO_INT32 ZOO_BOOL;
#define ZOO_TRUE 1
#define ZOO_FALSE 0

/*
为了使各平台代码趋于统一，除了上面的环境宏开关外�?
下面将定义一组全局性宏，替代各平台功能相同而名称不同的系统函数和变量类型，
各编程人员应注意在所有代码中严格使用下列宏定义，而不是非统一的系统函数名�?
*/

/* Other different in different platform */
#if TARGET_OS==WIN_OS
    typedef ZOO_ULONG32    ZOO_TIMER_ID;
    typedef ZOO_LPVOID     ZOO_HANDLE;
    typedef ZOO_HANDLE     ZOO_FD;
    typedef ZOO_UINT32     ZOO_SOCKET;
    typedef ZOO_HANDLE     ZOO_PID;
    typedef ZOO_HANDLE     ZOO_SEM_ID;
    typedef ZOO_INT32      ZOO_DWORD;
#elif TARGET_OS == LINUX_OS
    typedef ZOO_INT32      ZOO_TIMER_ID;
    typedef ZOO_UINT32     ZOO_HANDLE;
    typedef ZOO_INT32      ZOO_FD;
    typedef ZOO_INT32      ZOO_SOCKET;
    typedef ZOO_INT32      ZOO_PID;
    typedef ZOO_INT32      ZOO_SEM_ID;
    typedef ZOO_INT32      ZOO_DWORD;
#endif


#endif //ZOO_TYPE_H_
