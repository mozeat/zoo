/*******************************************************************************
* Copyright (C) 2017, 上海蔚来汽车有限公司
* All rights reserved.
* 产品: ZOO
* 所属组件 ：
* 模块名称 :
* 文件名称 : ZOO_if.h
* 概要描述 : 
* 历史记录 :
* 版本      日期        作者    内容
* 1.0    2017-12-7  孙伟旺   新建
******************************************************************************/
#ifndef ZOO_IF_H_
#define ZOO_IF_H_

#include "ZOO_tc.h"

/**
 @brief get ZOO configure path
 @return path string
**/
ZOO_EXPORT const ZOO_CHAR * ZOO_get_pl_cfg_base_dir();

/**
 @brief get ZOO version info
 @param ZOO_VERSION_STRUCT* version_info
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_pl_version_info(INOUT ZOO_VERSION_STRUCT* version_info);

/**
 @brief get user version info
 @param ZOO_USER_VERSION_STRUCT user_version_info
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_user_version_info(INOUT ZOO_USER_VERSION_STRUCT* user_version_info);

/**
 @brief get user cfg file
 @param ZOO_CHAR* cfg_file
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_user_cfg_file(INOUT ZOO_CHAR cfg_file[ZOO_PATH_LENGHT]);

/**
 @brief get user cfg path
 @param ZOO_CHAR* path
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_user_cfg_path(INOUT ZOO_CHAR cfg_path[ZOO_PATH_LENGHT] );

/**
 @brief get user files path
 @param ZOO_USER_FILE_PATH_STRUCT* path
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_user_files_path(INOUT ZOO_USER_PATH_STRUCT* files_path);

/**
 @brief get platform files path
 @param ZOO_PL_PATH_STRUCT* path
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_pl_files_path(INOUT ZOO_PL_PATH_STRUCT* files_path);

/**
 @brief get all files path,include user and platform
 @param ZOO_PATH_STRUCT* path
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_files_path(INOUT ZOO_PATH_STRUCT* files_path);


/**
 @brief get resource id from brown dragon
 @param resource_id
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_resource_id(INOUT ZOO_CHAR resource_id[ZOO_RESOURCE_ID_LENGHT]);

/**
 @brief get timestamp 
 @param timestamp
 @return error code
**/
ZOO_EXPORT void ZOO_get_current_timestamp(INOUT ZOO_CHAR timestamp[ZOO_TIME_BUFFER_LENGHT]);

/**
 @brief get timestamp by mircoseconds
 @return mircoseconds
**/
ZOO_EXPORT ZOO_UINT64 ZOO_get_current_timestamp_usec();

/**
 @brief get timestamp by milliseconds
 @return milliseconds
**/
ZOO_EXPORT ZOO_UINT64 ZOO_get_current_timestamp_msec();

/**
 @brief get current execute path
 @param path
 @return error code
**/
ZOO_EXPORT const ZOO_CHAR* ZOO_get_current_exec_path();

/**
 @brief get current parent path
 @param dir
 @level the relative path
 @return error code
**/
ZOO_EXPORT const ZOO_CHAR*  ZOO_get_parent_dir(IN const ZOO_CHAR dir[ZOO_PATH_LENGHT],
										  				IN ZOO_INT32 level);
/**
 @brief get self process name
 @return process name
**/
ZOO_EXPORT const ZOO_CHAR * ZOO_get_process_name();

/**
 @brief get current system   name
 @return user name
**/
ZOO_EXPORT const ZOO_CHAR * ZOO_get_uname();

/**
 @brief get current thread id
 @return id
**/
ZOO_EXPORT ZOO_LONG ZOO_get_thread_id();

/**
 *@brief Create a thread and detach immediately.
 *@param f -the callback function
 *@param arg - function parameter
 *return error code
*/
ZOO_EXPORT ZOO_INT32 ZOO_create_thread(void *(*f) (void *),void * arg);

/**
 *@brief Create a timer.
 *@param f -the callback function
 *@param arg - function parameter
 *@param milliseconds - timeout
 *@param id - the timer id
 *return error code  
*/
ZOO_EXPORT ZOO_INT32 ZOO_create_timer(void *(*f) (void *),
											IN void * arg,
											IN ZOO_INT32 milliseconds,
											INOUT ZOO_HANDLE * id);
/**
 *@brief Query tasks configurations from database.
 *@param id -the timer id
 *return error code
*/
ZOO_EXPORT ZOO_INT32 ZOO_stop_timer(IN ZOO_HANDLE id);

/**
 *@brief Simple log function.A "setting.txt" file should be placed : ./config/log/.
 *@param function_name 
 *@param format_spec 
*/
ZOO_EXPORT void ZOO_slog(IN ZOO_SEVERITY_LEVEL_ENUM level,
									IN const char * function_name,
									IN const char * format_spec, ...);

/**
 *@brief Get zoo platform tasks,throw std::exception
 *@param zoo_tasks     
 *@return error_code
 */
ZOO_EXPORT ZOO_INT32 ZOO_get_pl_tasks(INOUT ZOO_TASKS_STRUCT* pl_tasks);

/**
 *@brief Get user configure tasks,throw std::exception
 *@param zoo_tasks     
 *@return error_code
 */
ZOO_EXPORT ZOO_INT32 ZOO_get_user_tasks(INOUT ZOO_TASKS_STRUCT* user_tasks);

/**
 *@brief Update user configure tasks,throw std::exception
 *@param zoo_tasks     
 *@return error_code
 */
ZOO_EXPORT ZOO_INT32 ZOO_update_task_configuration(IN ZOO_TASK_STRUCT* task);


/**
 *@brief Get zoo entity tasks
 *@param zoo_tasks     
 *@return error_code
 */
ZOO_EXPORT ZOO_INT32 ZOO_get_entity_tasks(INOUT ZOO_TASKS_STRUCT* tasks);


/**
 *@brief Get broker infor
 *@param     broker
 *@return error_code.
*/
ZOO_EXPORT ZOO_INT32 ZOO_get_brokers(INOUT ZOO_BROKER_STRUCT * broker);

/**
 *@brief Get host_info.
 *@param     host_info
 *@return error_code.
*/
ZOO_EXPORT ZOO_INT32 ZOO_get_host_info(INOUT ZOO_HOST_INFO_STRUCT * host_info);

/**
 *@brief Get http server parameters.
 *@param    server
 *@return error_code.
*/
ZOO_EXPORT ZOO_INT32 ZOO_get_server_info(INOUT ZOO_SERVER_CFG_STRUCT * server);

/**
 *@brief Get OTA  parameters.
 *@param     
 *@return error_code.
*/
ZOO_EXPORT  ZOO_INT32 ZOO_get_OTA_info(INOUT ZOO_OTA_CFG_STRUCT *ota);

#endif
