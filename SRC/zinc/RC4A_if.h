/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO 
 * Component ID : RC
 * File Name    :
 * Description  : 本模块用于和OSS收发消息
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef RC4A_IF_H
#define RC4A_IF_H
#include <stdio.h>
#include <stdlib.h>
#include <ZOO.h>
#include <RC4A_type.h>
		 
/**************************************************************************
INTERFACE <ZOO_INT32 RC4A_connect>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         RC4A_OPTION_STRUCT option
    OUT:        none
    INOUT:      ZOO_FD * fd    -connection identity
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                RC4A_SYSTEM_ERR -- 系统错误
                RC4A_TIMEOUT_ERR -- 超时错误
                RC4A_PARAMETER_ERR -- 参数错误
<Description>:  连接成功后，自动开启事件收发loop

}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 RC4A_connect(IN RC4A_OPTION_STRUCT * option, INOUT ZOO_FD * fd);

/**************************************************************************
INTERFACE <ZOO_INT32 RC4A_set_ssl_option>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_FD fd      -connection identity
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                RC4A_SYSTEM_ERR -- 系统错误
                RC4A_TIMEOUT_ERR -- 超时错误
                RC4A_PARAMETER_ERR -- 参数错误
<Description>:  1.断开和远程broker连接，并停止事件loop
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 RC4A_disconnect(IN ZOO_FD fd);


/**************************************************************************
INTERFACE <ZOO_INT32 RC4A_get_fd>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_CHAR * client_id
    OUT:        none
    INOUT:      ZOO_FD * fd
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                RC4A_SYSTEM_ERR -- 系统错误
                RC4A_TIMEOUT_ERR -- 超时错误
                RC4A_PARAMETER_ERR -- 参数错误
<Description>:  获取对应client_id fd
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 RC4A_get_fd(IN ZOO_CHAR * client_id,INOUT ZOO_FD * fd);


/**************************************************************************
INTERFACE <ZOO_INT32 RC4A_get_connected>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_FD fd
    OUT:        none
    INOUT:      ZOO_BOOL * is_connected
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                RC4A_SYSTEM_ERR -- 系统错误
                RC4A_TIMEOUT_ERR -- 超时错误
                RC4A_PARAMETER_ERR -- 参数错误
<Description>:  1.获取连接状态
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 RC4A_get_connected(IN ZOO_FD fd,INOUT ZOO_BOOL * is_connected);

/**************************************************************************
INTERFACE <ZOO_INT32 RC4A_add_sub_topic>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_FD fd
                ZOO_CHAR  topic[RC4A_BUFFER_LENGTH]
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                RC4A_SYSTEM_ERR -- 系统错误
                RC4A_TIMEOUT_ERR -- 超时错误
                RC4A_PARAMETER_ERR -- 参数错误
<Description>:  设置订阅OSS主题
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 RC4A_set_subscribe_topic(IN ZOO_FD fd,
												                      IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH]);

/**************************************************************************
INTERFACE <ZOO_INT32 RC4A_remove_sub_topic>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_FD fd
    OUT:        ZOO_CHAR topic[RC4A_BUFFER_LENGTH]
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                RC4A_SYSTEM_ERR -- 系统错误
                RC4A_TIMEOUT_ERR -- 超时错误
                RC4A_PARAMETER_ERR -- 参数错误
<Description>:  1.增加订阅OSS主题
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 RC4A_remove_subscribe_topic(IN ZOO_FD fd,
														                    IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH]);

/**************************************************************************
INTERFACE <ZOO_INT32 RC4A_clear_queue>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_FD fd
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                RC4A_SYSTEM_ERR -- 系统错误
                RC4A_TIMEOUT_ERR -- 超时错误
                RC4A_PARAMETER_ERR -- 参数错误
<Description>:  清除对应id的数据库，需要先停止loop
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 RC4A_clear_db(IN ZOO_FD fd);

/**************************************************************************
INTERFACE <ZOO_INT32 RC4A_publish>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_FD fd
    IN:         ZOO_CHAR topic[RC4A_BUFFER_LENGTH]   
    IN:         ZOO_INT32 topic_length            -topic消息长度
    IN:         ZOO_CHAR message[RC4A_BUFFER_LENGTH]   
    IN:         ZOO_INT32 message_length            -消息长度
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                RC4A_SYSTEM_ERR -- 系统错误
                RC4A_TIMEOUT_ERR -- 超时错误
                RC4A_PARAMETER_ERR -- 参数错误
<Description>:  发送消息。当一条消息长度超过RC4A_MESSAGE_LENGTH时，需要将消息拆分成多条。total_messages
                表示消息条数，message_index表示发送第几条消息
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 RC4A_publish(IN ZOO_FD fd,
											    IN const ZOO_CHAR *topic,
											    IN ZOO_CHAR topic_length,
											    IN const ZOO_CHAR *message,
											    IN ZOO_INT32 message_length);

/**************************************************************************
INTERFACE <ZOO_INT32 RC4A_connect_status_subscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         RC4A_CONNECTION_CALLBACK_FUNCTION callback_function
    OUT:        ZOO_UINT32 *handle
    INOUT:      void *context
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                RC4A_SYSTEM_ERR -- 系统错误
                RC4A_TIMEOUT_ERR -- 超时错误
                RC4A_PARAMETER_ERR -- 参数错误
<Description>:  客户端订阅连接状态
}
**************************************************************************/
typedef void(*RC4A_CONNECT_STATUS_CALLBACK_FUNCTION)(IN RC4A_CONNECTION_STATUS_STRUCT status,
														 IN ZOO_INT32 error_code,
														 IN void *context);

ZOO_EXPORT ZOO_INT32 RC4A_connect_status_subscribe(IN RC4A_CONNECT_STATUS_CALLBACK_FUNCTION callback_function,
																OUT ZOO_UINT32 *handle,
																INOUT void *context);	  

ZOO_EXPORT ZOO_INT32 RC4A_connect_status_unsubscribe(IN ZOO_UINT32 handle);	

/**************************************************************************
INTERFACE <ZOO_INT32 RC4A_message_subscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         RC4A_CONNECTION_CALLBACK_FUNCTION callback_function
    OUT:        ZOO_UINT32 *handle
    INOUT:      void *context
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                RC4A_SYSTEM_ERR -- 系统错误
                RC4A_TIMEOUT_ERR -- 超时错误
                RC4A_PARAMETER_ERR -- 参数错误
<Description>:  客户端订阅OSS消息
}
**************************************************************************/
typedef void(*RC4A_MESSAGE_CALLBACK_FUNCTION)(IN RC4A_MESSAGE_STRUCT * message,
													IN ZOO_INT32 error_code,
													IN void *context);

ZOO_EXPORT ZOO_INT32 RC4A_message_subscribe(IN RC4A_MESSAGE_CALLBACK_FUNCTION callback_function,
																OUT ZOO_UINT32 *handle,
																INOUT void *context);	  

ZOO_EXPORT ZOO_INT32 RC4A_message_unsubscribe(IN ZOO_UINT32 handle);																	
#endif
