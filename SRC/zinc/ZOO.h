/*******************************************************************************
* Copyright (C) 2017, 上海蔚来汽车有限公司 
* All rights reserved.
* 产品叿: ZOO
* 所属组仿:
* 模块名称 :
* 文件名称 : ZOO.h
* 概要描述 : 该文件总的包括了ZOO各方面通用类型定义?
* 历史记录 :
* 版本      日期        作耿   内容
* 1.0    2017-12-21     孙伟  新建
******************************************************************************/
#ifndef ZOO_H_
#define ZOO_H_

#include "ZOO_station.h"
#include "ZOO_type.h"


/* 函数声明中用于表示输入参敿*/
#ifndef IN
    #define IN
#endif
/* 函数声明中用于表示输出参敿*/
#ifndef OUT
    #define OUT
#endif
/* 函数声明中用于表示既是输入又是输出参敿*/
#ifndef INOUT
    #define INOUT
#endif
/* 函数声明中用于表示既是重载函敿*/
#ifndef OVERRIDE
    #define OVERRIDE
#endif

/*! 定义函数返回值OK */
#ifndef OK
    #define OK          0
#endif

/* 当未定义NULL时，定义NULL */
#ifndef NULL
    #define NULL        0
#endif

#ifndef SAFTY_DELETE_POINTER
    #define SAFTY_DELETE_POINTER(pointer)        if(pointer != NULL){delete (pointer);(pointer) = NULL;}
#endif
#ifndef SAFTY_DELETE_ARRAY
    #define SAFTY_DELETE_ARRAY(array)        if((array) != NULL){delete [] array;(array) = NULL;}
#endif
#ifndef SAFTY_FREE_POINTER
    #define SAFTY_FREE_POINTER(pointer)        if((pointer) != NULL){free (pointer);(pointer) = NULL;}
#endif

/* 获取当前的函数名*/
#if TARGET_OS == LINUX32_OS || TARGET_OS == LINUX64_OS
/* 定义统一的函数名宿*/
#ifdef __cplusplus
#define __ZOO_FUNC__  (char*)__PRETTY_FUNCTION__
#else
    #define __ZOO_FUNC__  (char*)__func__
#endif
#elif TARGET_OS == WIN32_OS || TARGET_OS == WIN64_OS
/* 定义统一的函数名宿*/
    #define __ZOO_FUNC__  __FUNCTION__
#endif


/* 接口函数限定符前缀 */
#if TARGET_OS == LINUX32_OS || TARGET_OS == LINUX64_OS
    #ifdef  __cplusplus
        #define ZOO_EXPORT extern "C"
    #else
    /*! \brief
    * \~chinese 当操作系统是SUN或VxWorks且编程语言为C时，定义外部接口的限定符?
    * \~english When OS is Solaris or VxWorks, and program language is C,
    define export directive as extern. */
        #define ZOO_EXPORT extern
    #endif

    #define ZOO_VAR_EXPORT
#elif TARGET_OS == WIN32_OS || TARGET_OS == WIN64_OS
    #ifdef __cplusplus
        /*! \brief
        * \~chinese 当操作系统是WIN32且编程语言为C++时，定义外部接口的限定符?
        * \~english When OS is WIN32, and program language is C++, define export
        directive as extern "C" __declspec (dllexport). */
        #define ZOO_EXPORT extern "C" __declspec ( dllexport )
        #define ZOO_IMPORT extern "C" __declspec ( dllimport )
    #else
    /*! \brief
    * \~chinese 当操作系统是WIN32且编程语言为C+时，定义外部接口的限定符?
    * \~english When OS is WIN32, and program language is C, define export
    directive as __declspec (dllexport). */
        #define ZOO_EXPORT extern __declspec ( dllexport )
        #define ZOO_IMPORT extern __declspec ( dllimport )
    #endif

    #define ZOO_VAR_EXPORT __declspec ( dllexport )
#endif

#endif

