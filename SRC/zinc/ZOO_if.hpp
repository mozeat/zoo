/*******************************************************************************
* Copyright (C) 2017, 上海蔚来汽车有限公司
* All rights reserved.
* 产品: ZOO
* 所属组件: zoo
* 模块名称 :
* 文件名称 : ZOO_if.hpp
* 概要描述 : C++封装接口库
* 历史记录 :
* 版本      日期        作者     内容
* 1.0    2017-12-7  孙伟旺    ZOO C++
******************************************************************************/
#ifndef ZOO_IF_HPP_
#define ZOO_IF_HPP_
extern "C"
{
	#include "ZOO_if.h"
	#include "TR4A_if.h"
	#include "EH4A_if.h"
	#include "MM4A_if.h"
}
#include <boost/log/utility/setup/settings.hpp>
#include <boost/log/utility/setup/settings_parser.hpp>
#include <sstream>
#include <iostream>
#include <fstream>
#include "utils/THREAD_POOL.h"

/**
 @brief Get test data from ini file
 @param file     the .ini file absolute path
 @param child    the data joint
 @return result
 @exeption std::exception
**/
template<typename R>
R ZOO_parse_tdata(IN std::string file,IN std::string section) 
{
	boost::log::settings settings;
	std::ifstream ifs(file);
	if(ifs.is_open())
	{
		settings = boost::log::parse_settings(ifs);
		ifs.close();
	}
	return boost::optional<R>(settings[section]).get();
}

/**
 @brief Log ,the max string size should less than 1024 bytes. 
 @param M os << "" 
**/
#ifndef __ZOO_LOG_LEN_MAX
#define __ZOO_LOG_LEN_MAX 1024
#endif
	
/**
 @brief Trace log,the max string size should less than 512 bytes.
 @param comp_id 	char string 
 @param M	 "" << ""
**/
#define __ZOO_TRACE(comp_id,M) \
		{ \
			std::stringstream ss; ss << M; \
			if(ss.str().size() < __ZOO_LOG_LEN_MAX) TR4A_trace(comp_id,__ZOO_FUNC__,"%s",std::move(ss).str().c_str()); \
		}

/**
 @brief Normal log,the max string size should less than 1024 bytes.
 @param comp_id 	char string 
 @param M	 "" << ""
**/
#define __ZOO_LOG(M) \
		{\
			std::stringstream ss; ss << M; \
			if(ss.str().size() < __ZOO_LOG_LEN_MAX) ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"%s",std::move(ss).str().c_str()); \
		}
		
/**
 @brief Error log,the max string size should less than 1024 bytes.
 		The log is internal.
 @param comp_id 	char string 
 @param M	 "" << ""
**/
#define __ZOO_LOG_ERROR(M) \
		{ \
			std::stringstream ss; ss << M; \
			if(ss.str().size() < __ZOO_LOG_LEN_MAX) ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,"%s",std::move(ss).str().c_str()); \
		}

/**
 @brief Warning log,the max string size should less than 1024 bytes.
		The log is internal.
 @param comp_id 	char string 
 @param M	 "" << ""
**/
#define __ZOO_LOG_WARNING(M) \
		{ \
			std::stringstream ss; ss << M; \
			if(ss.str().size() < __ZOO_LOG_LEN_MAX) ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,__ZOO_FUNC__,"%s",std::move(ss).str().c_str()); \
		}

/**
 @brief Level log,the max string size should less than 1024 bytes.
		The log is internal.
 @param comp_id 	char string 
 @param M	 "" << ""
**/
#define __ZOO_LOG_(level,M) \
		{ \
			std::stringstream ss; ss << (M); \
			if(ss.str().size() < __ZOO_LOG_LEN_MAX) ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"%s",std::move(ss).str().c_str()); \
		}

/**
 @brief Startup thread pool.
**/
#define __ZOO_THREAD_POOL_STARTUP \
		ZOO_COMMON::THREAD_POOL::get_instance()->startup(); 

/**
 @brief Shutdown thread pool.
**/	
#define __ZOO_THREAD_POOL_SHUTDOWN \
		ZOO_COMMON::THREAD_POOL::get_instance()->shutdown(); 

/**
 @brief Post a task to work async.
**/		
#define __ZOO_THREAD_POOL_RUN(function) \
		ZOO_COMMON::THREAD_POOL::get_instance()->queue_working(function,NULL); 

#endif
