/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO 
 * Component ID : UM
 * File Name    : UM4A_type.h
 * Description  : Upgrade Management
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef UM4A_TYPE_H
#define UM4A_TYPE_H
#include "ZOO_tc.h"

/*************************************************************
 @brief Define error Code  
*************************************************************/
#define UM4A_BASE_ERR          0x554d4100
#define UM4A_SYSTEM_ERR   	   (UM4A_BASE_ERR + 0x01)
#define UM4A_PARAMETER_ERR     (UM4A_BASE_ERR + 0x02)
#define UM4A_TIMEOUT_ERR  	   (UM4A_BASE_ERR + 0x03)
#define UM4A_ILLEGAL_CALL_ERR  (UM4A_BASE_ERR + 0x04)

/***********************************************************
 @brief Define buffer length 
************************************************************/
#define UM4A_BUFFER_LENGTH      1024
#define UM4A_VERISON_LENGTH     128

/**
 * @brief Define OTA status struct
*/
typedef struct
{
	ZOO_INT32 package_id;//当前升级包的id
	ZOO_INT32 inprogress;//升级进度条 0-100
	ZOO_CHAR description[UM4A_BUFFER_LENGTH];//升级包说明
	ZOO_CHAR version[UM4A_BUFFER_LENGTH];//本次升级版本
}UM4A_INPROGRESS_STRUCT;

/**
 * @brief Define OTA status struct
*/
typedef struct
{
	ZOO_OTA_STATE_ENUM state;//组件工作状态
	ZOO_INT32 packages;//安装包总数
	ZOO_INT32 estimate_total_time;//minutes
	ZOO_CHAR major_version[UM4A_VERISON_LENGTH];//大版本号
	ZOO_CHAR release_note[UM4A_BUFFER_LENGTH];//版本详细说明
}UM4A_STATUS_STRUCT;

#endif
