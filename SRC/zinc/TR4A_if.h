/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO 
 * Component ID : TR
 * File Name    : TR4A_if.h
 * Description  : 记录软件过程log
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef TR4A_IF_H
#define TR4A_IF_H
#include <stdio.h>
#include <stdlib.h>
#include "ZOO_tc.h"
#include "TR4A_type.h"

/**************************************************************************
INTERFACE <ZOO_INT32 TR4A_set_mode>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         const ZOO_CHAR* resource_id         设备id
				const ZOO_CHAR* component_id        组件id       
                ZOO_TRACE_MODE_ENUM sim_mode   使能/非使能
                ZOO_SIM_MODE_ENUM trace_mode   组件仿真模式
				ZOO_WATCH_MODE_ENUM watch_mode 组件守护模式
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                TR4A_SYSTEM_ERR -- 系统错误
                TR4A_TIMEOUT_ERR -- 超时错误
                TR4A_PARAMETER_ERR -- 参数错误
<Description>:  设置对应设备内部组件工作模式
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 TR4A_set_mode(IN const ZOO_CHAR* component_id, 
										IN ZOO_TRACE_MODE_ENUM trace_mode, 
										IN ZOO_SIM_MODE_ENUM sim_mode,
										IN ZOO_WATCH_MODE_ENUM watch_mode);
							  
							  
/**************************************************************************
INTERFACE <ZOO_INT32 TR4A_get_mode>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         const ZOO_CHAR* resource_id         设备id
				const ZOO_CHAR* component_id        组件id       
                ZOO_TRACE_MODE_ENUM sim_mode   使能/非使能
                ZOO_SIM_MODE_ENUM trace_mode   组件仿真模式
				ZOO_WATCH_MODE_ENUM watch_mode 组件守护模式
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                TR4A_SYSTEM_ERR -- 系统错误
                TR4A_TIMEOUT_ERR -- 超时错误
                TR4A_PARAMETER_ERR -- 参数错误
<Description>:  获取对应设备的内部组件工作模式
}
**************************************************************************/						  
ZOO_EXPORT ZOO_INT32 TR4A_get_mode(IN const ZOO_CHAR* component_id, 
										OUT ZOO_SIM_MODE_ENUM* sim_mode, 
										OUT ZOO_TRACE_MODE_ENUM* trace_mode,
										OUT ZOO_WATCH_MODE_ENUM* watch_mode);

/**************************************************************************
INTERFACE <ZOO_INT32 TR4A_trace>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         const ZOO_CHAR* component_id        组件id
                const ZOO_CHAR function_name        函数名
                const ZOO_CHAR* format_spec         可变参数
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                TR4A_SYSTEM_ERR -- 系统错误
                TR4A_TIMEOUT_ERR -- 超时错误
                TR4A_PARAMETER_ERR -- 参数错误
<Description>:  记录软件运行的轨迹
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 TR4A_trace(IN const ZOO_CHAR* component_id,
									IN const ZOO_CHAR* function_name,
									IN const ZOO_CHAR* format_spec,...);
									
/**************************************************************************
INTERFACE <ZOO_INT32 TR4A_log>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         const ZOO_CHAR* resource_id       设备id
                ZOO_SEVERITY_LEVEL_ENUM level         等级
                const ZOO_CHAR* format_spec       可变参数
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                TR4A_SYSTEM_ERR -- 系统错误
                TR4A_TIMEOUT_ERR -- 超时错误
                TR4A_PARAMETER_ERR -- 参数错误
<Description>:  记录过程数据
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 TR4A_log(IN ZOO_SEVERITY_LEVEL_ENUM level,
											IN const ZOO_CHAR* format_spec,...);
									
/**************************************************************************
INTERFACE <ZOO_INT32 TR4A_check_sim_mode>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         const ZOO_CHAR* component_id
                ZOO_SIM_MODE_ENUM sim_mode
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      TRUE or FALSE
<Description>:  检查组件对应的是否为仿真模式
}
**************************************************************************/
ZOO_EXPORT ZOO_BOOL TR4A_check_sim_mode(IN const ZOO_CHAR* component_id,
										        IN ZOO_SIM_MODE_ENUM sim_mode);

												
#endif