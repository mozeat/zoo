/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO 
 * Component ID : MM
 * File Name    : MM4A_if.h
 * Description  :
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef MM4A_IF_H
#define MM4A_IF_H
#include <ZOO.h>
#include <stdlib.h>
#include <stddef.h>
/*****************************************************
**@brief  MM4A_initialize
**@description     : 申请一块20M内存区
**@preconditions   :
**@input  parameter: 
**@output parameter:
**@return          : ZOO_INT32
****************************************************/
ZOO_EXPORT ZOO_INT32 MM4A_initialize();

/*****************************************************
**@brief  MM4A_terminate
**@description     : 释放申请的内存区
**@preconditions   : 已调用MM4A_initialize
**@input  parameter: 
**@output parameter:
**@return          : ZOO_INT32
****************************************************/
ZOO_EXPORT void MM4A_terminate();

/*****************************************************
**@brief  MM4A_malloc
**@description     : 在20M空间内存，申请指定字节大小的内存小块
**@preconditions   : 已调用MM4A_initialize
**@input  parameter: size_t bytes
**@output parameter:
**@return          : void * - 
    
****************************************************/
ZOO_EXPORT void* MM4A_malloc(size_t bytes);

/*****************************************************
**@brief  MM4A_free
**@description     : 释放指定字节大小的内存小块
**@preconditions   : 已调用MM4A_malloc
**@input  parameter: 
**@output parameter:
**@return          : ZOO_INT32
****************************************************/
ZOO_EXPORT void MM4A_free(void* pointer);

/*****************************************************
**@brief  MM4A_malloc_lock
**@description     : 申请指定字节大小的内存块，用于多线程
**@preconditions   :已调用MM4A_initialize
**@input  parameter: 
**@output parameter:
**@return          : ZOO_INT32
****************************************************/
ZOO_EXPORT void * MM4A_malloc_lock(size_t bytes);

/*****************************************************
**@brief  MM4A_malloc_unlock
**@description     : 释放指定字节大小的内存块，用于多线程
**@preconditions   :
**@input  parameter: 
**@output parameter:
**@return          : ZOO_INT32
****************************************************/
ZOO_EXPORT void MM4A_malloc_unlock(void* pointer);

#endif