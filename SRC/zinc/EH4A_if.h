/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : EH
 * File Name    : EH4A_if.h
 * Description  :
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef EH4A_IF_H
#define EH4A_IF_H

#include <stdlib.h>
#include <ZOO.h>
#include "EH4A_type.h"
/**************************************************************************
INTERFACE <ZOO_INT32 EH4A_show_exception>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         const ZOO_CHAR* component_id
                const ZOO_CHAR * file
                const ZOO_CHAR * function_name
				ZOO_INT32 line
				ZOO_INT32 error_code
				ZOO_INT32 link_error_code
				const ZOO_CHAR * info
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                EH4A_SYSTEM_ERR -- 系统错误
                EH4A_TIMEOUT_ERR -- 超时错误
                EH4A_PARAMETER_ERR -- 参数错误
<Description>:  错误信息写入日志，可用于调试
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 EH4A_show_exception(IN const ZOO_CHAR * component_id,
	        								        IN const ZOO_CHAR * file,
	        								        IN const ZOO_CHAR * function_name,
	        								        IN ZOO_INT32 line,
	        								        IN ZOO_INT32 error_code,
													IN ZOO_INT32 link_error_code,
	        								        IN const ZOO_CHAR * info);

/**************************************************************************
INTERFACE <ZOO_INT32 EH4A_set_alarm_with_material_id>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         const ZOO_CHAR* material_id
                ZOO_INT32 alarm_id
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                EH4A_SYSTEM_ERR -- 系统错误
                EH4A_TIMEOUT_ERR -- 超时错误
                EH4A_PARAMETER_ERR -- 参数错误
<Description>:  订阅表中的ALARM状态被设置，可用于向HMI发送报警消息，
                并可以拉响警报，根据配置的错误等级。
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 EH4A_set_alarm_with_material_id(IN const ZOO_CHAR* material_id,
																		IN ZOO_INT32 alarm_id);

/**************************************************************************
INTERFACE <ZOO_INT32 EH4A_set_alarm>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_INT32 alarm_id
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                EH4A_SYSTEM_ERR -- 系统错误
                EH4A_TIMEOUT_ERR -- 超时错误
                EH4A_PARAMETER_ERR -- 参数错误
<Description>:  订阅表中的ALARM状态被设置，可用于向HMI发送报警消息，
                并可以拉响警报，根据配置的错误等级。
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 EH4A_set_alarm(IN ZOO_INT32 alarm_id);

/**************************************************************************
INTERFACE <ZOO_INT32 EH4A_clear_alarm>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_INT32 alarm_id
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                EH4A_SYSTEM_ERR -- 系统错误
                EH4A_TIMEOUT_ERR -- 超时错误
                EH4A_PARAMETER_ERR -- 参数错误
<Description>:  订阅表中的ALARM状态被清除。
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 EH4A_clear_alarm(IN ZOO_INT32 alarm_id);

/**************************************************************************
INTERFACE <ZOO_INT32 EH4A_clear_all_alarms>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_INT32 alarm_id
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                EH4A_SYSTEM_ERR -- 系统错误
                EH4A_TIMEOUT_ERR -- 超时错误
                EH4A_PARAMETER_ERR -- 参数错误
<Description>:  清除所有组件的错误，订阅表的错误信息被删除
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 EH4A_clear_all_alarms(void);

/**************************************************************************
INTERFACE <ZOO_INT32 EH4A_get_alarm_information>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_INT32 alarm_id
    OUT:        none
    INOUT:      EH4A_ALARM_DETAILS_STRUCT * details
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                EH4A_SYSTEM_ERR -- 系统错误
                EH4A_TIMEOUT_ERR -- 超时错误
                EH4A_PARAMETER_ERR -- 参数错误
<Description>:  获取报警信息
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 EH4A_get_alarm_info(IN ZOO_INT32 alarm_id,
													INOUT EH4A_ALARM_DETAILS_STRUCT * details);

/**************************************************************************
INTERFACE <ZOO_INT32 EH4A_find_alarm_counts>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         EH4A_FOUND_TYPE_ENUM found_type 
    OUT:        none
    INOUT:      ZOO_INT32 *counts
                ZOO_INT32 *session_id    the uuid for identify requerster
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                EH4A_SYSTEM_ERR -- 系统错误
                EH4A_TIMEOUT_ERR -- 超时错误
                EH4A_PARAMETER_ERR -- 参数错误
<Description>:  获取当前配置的所有告警点个数，配合EH4A_get_alarm_data一起使用
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 EH4A_find_alarm_counts(IN EH4A_FOUND_TYPE_ENUM found_type,
                                                                  INOUT ZOO_INT32 * counts,
                                                                  INOUT ZOO_INT32 * session_id);

/**************************************************************************
INTERFACE <ZOO_INT32 EH4A_get_alarm_data>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_INT32 index 范围[0,counts]
                ZOO_INT32 session_id    the uuid for identify requerster
    OUT:        none
    INOUT:      EH4A_ALARM_STRUCT * data
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                EH4A_SYSTEM_ERR -- 系统错误
                EH4A_TIMEOUT_ERR -- 超时错误
                EH4A_PARAMETER_ERR -- 参数错误
<Description>:  根据EH4A_find_alarm_counts接口获取的count数，获取告警信息，
                在获取完最后一条告警之后，消息自动清空。
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 EH4A_get_alarm_data(IN ZOO_INT32 session_id,
                                                IN ZOO_INT32 index,
												INOUT EH4A_ALARM_STRUCT * data);

/**************************************************************************
INTERFACE <ZOO_INT32 EH4A_alarm_info_subscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         EH4A_ALARM_CALLBACK_FUNCTION callback_function
    OUT:        ZOO_UINT32 * handle
    INOUT:      void * context
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                EH4A_SYSTEM_ERR -- 系统错误
                EH4A_TIMEOUT_ERR -- 超时错误
                EH4A_PARAMETER_ERR -- 参数错误
<Description>:  错误信息订阅
}
**************************************************************************/
typedef void(*EH4A_ALARM_CALLBACK_FUNCTION)(IN EH4A_ALARM_STRUCT * status,IN ZOO_INT32 error_code,
											IN void *context);

ZOO_EXPORT ZOO_INT32 EH4A_alarm_info_subscribe(IN EH4A_ALARM_CALLBACK_FUNCTION callback_function,
												OUT ZOO_UINT32 *handle,
												INOUT void *context);
/**************************************************************************
INTERFACE <ZOO_INT32 EH4A_alarm_info_unsubscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_UINT32 handle
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                EH4A_SYSTEM_ERR -- 系统错误
                EH4A_TIMEOUT_ERR -- 超时错误
                EH4A_PARAMETER_ERR -- 参数错误
<Description>:   解除告警信息订阅接口
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 EH4A_alarm_info_unsubscribe(IN ZOO_UINT32 handle);

#endif
