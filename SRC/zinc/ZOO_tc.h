/**************************************************************
 * Copyright (C) 2015, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : EH
 * File Name    : ZOO_tc.h
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0       2018-03-13	    weiwang.sun		Initialize
 ***************************************************************/
#ifndef ZOO_TC_H_
#define ZOO_TC_H_
#include "ZOO.h"

/************************************************************
*  Define Marco
*************************************************************/

 /**
 *@Define Errorcode 
 */
#define ZOO_BASE_ERR                0X1000
#define ZOO_FILE_NOT_EXIST_ERR      (ZOO_BASE_ERR + 0x01)
#define ZOO_READ_FILE_FAILED_ERR    (ZOO_BASE_ERR + 0x02)
#define ZOO_WRITE_FILE_FAILED_ERR   (ZOO_BASE_ERR + 0x03)
#define ZOO_UPDATE_DB_FAILED_ERR    (ZOO_BASE_ERR + 0x04)
#define ZOO_INSERT_DB_FAILED_ERR    (ZOO_BASE_ERR + 0x05)
#define ZOO_DELETE_ITEM_FAILED_ERR  (ZOO_BASE_ERR + 0x06)
#define ZOO_PARAMETER_ERR  	        (ZOO_BASE_ERR + 0x20)

/**
*@Define Buffer length
*/
#define ZOO_TIME_BUFFER_LENGHT   30
#define ZOO_RESOURCE_ID_LENGHT   64
#define ZOO_PATH_LENGHT          128
#define ZOO_BROKER_BUFFER_LENGHT 64
#define ZOO_TASK_BUFFER_LENGHT   64
#define ZOO_HOST_BUFFER_LENGHT   64
#define ZOO_THREAD_NUM_MAX       30
#define ZOO_CERT_PATH_LENGHT     64
#define ZOO_IP_LENGHT            16
#define ZOO_PORT_LENGHT          32
#define ZOO_DOMAIN_LENGHT        64
#define ZOO_TOKEN_LENGHT         64
#define ZOO_USER_LENGHT          64
#define ZOO_VERSION_LENGHT          32
#define ZOO_PASSWORD_LENGHT  64

/************************************************************
*@Define Enum type
*************************************************************/
/**
 *@Define driver state enum  
 */
typedef enum
{
	ZOO_DRIVER_STATE_MIN = -1,
	ZOO_DRIVER_STATE_UNKNOWN    = 0,  
	ZOO_DRIVER_STATE_IDLE       = 1,  
	ZOO_DRIVER_STATE_BUSY       = 2,  
	ZOO_DRIVER_STATE_TERMINATED  = 3,  
	ZOO_DRIVER_STATE_MAX
}ZOO_DRIVER_STATE_ENUM;

/**
 *@Define driver state enum  
 */
typedef enum
{
	ZOO_RUNNING_MODE_MIN = 0,
	ZOO_RUNNING_MODE_DEBUG,      
	ZOO_RUNNING_MODE_WORK,        
	ZOO_RUNNING_MODE_MAX
}ZOO_RUNNING_MODE_ENUM;

/**
 *@Define Server type enum  
 */
typedef enum
{
	ZOO_SERVER_TYPE_MIN   = 0,
	ZOO_SERVER_TYPE_HTTP  = 1,      
	ZOO_SERVER_TYPE_WEB   = 2,   
	ZOO_SERVER_TYPE_HTTPS = 3,      
	ZOO_SERVER_TYPE_WEBS  = 4,      
	ZOO_SERVER_TYPE_MAX
}ZOO_SERVER_TYPE_ENUM;

/**
 *@Define Log severity type enum  
 */
typedef enum 
{
	ZOO_SEVERITY_LEVEL_NORMAL = 0,
	ZOO_SEVERITY_LEVEL_NOTIFICATION,
	ZOO_SEVERITY_LEVEL_WARNING,
	ZOO_SEVERITY_LEVEL_ERROR,
	ZOO_SEVERITY_LEVEL_CRITICAL
}ZOO_SEVERITY_LEVEL_ENUM;

/**
 *@Define simulate mode enum  
 */
typedef enum
{
	ZOO_SIM_MODE_MIN = -1,
	ZOO_SIM_DISABLE  = 0,  // simulation mode disabled
	ZOO_SIM_MODE_1   = 1,  // external simulation mode
	ZOO_SIM_MODE_2   = 2,  // internal simulation mode
	ZOO_SIM_MODE_3   = 3,  //
	ZOO_SIM_MODE_4   = 4,  //test mode for testing
	ZOO_SIM_MODE_MAX
}ZOO_SIM_MODE_ENUM;

/**
 *@Define Trace mode enum  
 */
typedef enum
{
	ZOO_TRACE_MODE_MIN = -1,
	ZOO_TRACE_DISABLE  = 0,
	ZOO_TRACE_ENABLE   = 1 ,
	ZOO_TRACE_MODE_MAX
}ZOO_TRACE_MODE_ENUM;

/**
 *@Define Watch Mode enum
 */
typedef enum
{
	ZOO_WATCH_MODE_MIN     = -1,
	ZOO_WATCH_MODE_DISABLE = 0,
	ZOO_WATCH_MODE_TIMES_3 = 3,
	ZOO_WATCH_MODE_TIMES_5 = 5,
	ZOO_WATCH_MODE_MAX
}ZOO_WATCH_MODE_ENUM;

/**
 *@brief Task state
 */
typedef enum
{
	ZOO_TASK_STATE_MIN          = -1,
	ZOO_TASK_STATE_SETUP        = 0,
	ZOO_TASK_STATE_STARTED      = 1,
	ZOO_TASK_STATE_NOT_STARTED  = 2,
	ZOO_TASK_STATE_MAX
}ZOO_TASK_STATE_ENUM;

/**
 *@brief Task state
 */
typedef enum
{
	ZOO_OTA_STATE_MIN         = -1,
	ZOO_OTA_STATE_IDLE        = 0,
	ZOO_OTA_STATE_DOWNLOADING = 1,
	ZOO_OTA_STATE_READY       = 2, //升级准备完成
	ZOO_OTA_STATE_INSTALLING  = 3, //正在安装
	ZOO_OTA_STATE_FINISHED    = 4, //升级完成
	ZOO_OTA_STATE_MAX
}ZOO_OTA_STATE_ENUM;


/**
 *@Define cert file 
 */
typedef enum
{
    ZOO_PEM_FILE_MIN = 0,
    ZOO_PEM_FILE_SERVER_CERT,
    ZOO_PEM_FILE_SERVER_KEY,
    ZOO_PEM_FILE_CLIENT_CERT,
    ZOO_PEM_FILE_CLEINT_KEY,
    ZOO_PEM_FILE_VEHICLE_TRUST_CHAIN,
    ZOO_PEM_FILE_SERVER_TRUST_CHAIN,
    ZOO_PEM_FILE_PHONE_TRUST_CHAIN,
    ZOO_PEM_FILE_MAX
}ZOO_PEM_FILE_ENUM;

/**
 *@Define broker data struct
 */
typedef struct
{
	ZOO_UINT32 hwm;  // message queue buffer length,if full some socket type will block
	ZOO_UINT32 linger; // messages hold time in queue after socket close [milliseconds]
	ZOO_CHAR local_fe_send[ZOO_BROKER_BUFFER_LENGHT];
	ZOO_CHAR local_fe_recv[ZOO_BROKER_BUFFER_LENGHT];
	ZOO_CHAR local_be_send[ZOO_BROKER_BUFFER_LENGHT];
	ZOO_CHAR local_be_recv[ZOO_BROKER_BUFFER_LENGHT];
	ZOO_CHAR remote_frontend[ZOO_BROKER_BUFFER_LENGHT];
	ZOO_CHAR remote_backend[ZOO_BROKER_BUFFER_LENGHT];
    ZOO_CHAR mode[ZOO_BROKER_BUFFER_LENGHT];
}ZOO_BROKER_STRUCT;


/**
 *@Define task data struct
 */
typedef struct
{
	ZOO_CHAR component[ZOO_TASK_BUFFER_LENGHT];
	ZOO_CHAR task_name[ZOO_TASK_BUFFER_LENGHT];
	ZOO_SIM_MODE_ENUM sim_mode;
	ZOO_TRACE_MODE_ENUM trace_mode;
	ZOO_WATCH_MODE_ENUM monitor_mode;
	ZOO_INT32 priority;
	ZOO_INT32 stack_size;// set stack size 
    ZOO_CHAR bin_path[ZOO_PATH_LENGHT];//the task location
    ZOO_CHAR env_var[ZOO_PATH_LENGHT];//environment variable
    ZOO_CHAR ec_definition_file[ZOO_PATH_LENGHT];//error code definition file,should be in sqlite3
    ZOO_CHAR mock_file[ZOO_PATH_LENGHT];//simulate file
    ZOO_CHAR cm_file[ZOO_PATH_LENGHT];//confiuration manager file
    ZOO_INT32 fe_thread_number;//client thread number
    ZOO_INT32 be_thread_number;//server thread number
}ZOO_TASK_STRUCT;

/**
 *@Define task data struct
 */
typedef struct
{
    ZOO_INT32 number;//total task numbers
	ZOO_TASK_STRUCT task[128];
}ZOO_TASKS_STRUCT;

/**
 *@brief Define host info struct
*/
typedef struct 
{
    ZOO_CHAR id[ZOO_HOST_BUFFER_LENGHT];
    ZOO_CHAR name[ZOO_HOST_BUFFER_LENGHT];
    ZOO_CHAR alias[ZOO_HOST_BUFFER_LENGHT];
    ZOO_CHAR location[ZOO_HOST_BUFFER_LENGHT];
    ZOO_CHAR domain[ZOO_HOST_BUFFER_LENGHT];
    ZOO_CHAR gps[ZOO_HOST_BUFFER_LENGHT];
    ZOO_BOOL present;
}ZOO_HOST_INFO_STRUCT;

/**
 *@brief Define http server : Neophron
*/
typedef struct 
{
    ZOO_CHAR ip[ZOO_IP_LENGHT];
    ZOO_CHAR port[ZOO_PORT_LENGHT];
    ZOO_SERVER_TYPE_ENUM type;//server type
    ZOO_INT32 thread_number;//the max threads
    ZOO_INT32 deadline;//seconds
    ZOO_CHAR cert_path[ZOO_CERT_PATH_LENGHT];
    ZOO_CHAR cert[ZOO_CERT_PATH_LENGHT];
    ZOO_CHAR private_key[ZOO_CERT_PATH_LENGHT];
    ZOO_CHAR trust_chain[ZOO_CERT_PATH_LENGHT];
    ZOO_INT32 verify_mode;
    ZOO_INT32 tls_version;
}ZOO_SERVER_CFG_STRUCT;

/**
 *@brief Define OTA 
 */
typedef struct 
{
    ZOO_CHAR domain[ZOO_DOMAIN_LENGHT];
    ZOO_CHAR token[ZOO_TOKEN_LENGHT];
    ZOO_CHAR user[ZOO_USER_LENGHT];
    ZOO_CHAR password[ZOO_PASSWORD_LENGHT];
    ZOO_CHAR type[ZOO_USER_LENGHT];
    ZOO_BOOL ui_enbaled;
}ZOO_OTA_CFG_STRUCT;

/**
 *@brief Define software module version 
 */
typedef struct 
{
    ZOO_CHAR name[ZOO_VERSION_LENGHT];
    ZOO_CHAR version[ZOO_VERSION_LENGHT];
}ZOO_MODULE_VERSION_STRUCT;

/**
 *@brief Define version 
 */
typedef struct 
{
    ZOO_CHAR zoo[ZOO_VERSION_LENGHT];
    ZOO_CHAR kernel[ZOO_VERSION_LENGHT];
    ZOO_CHAR uboot[ZOO_VERSION_LENGHT];
    ZOO_CHAR release_date[ZOO_VERSION_LENGHT]; 
}ZOO_VERSION_STRUCT;

/**
 *@brief Define remote syslog structure 
 */
typedef struct 
{
    ZOO_CHAR addr[ZOO_IP_LENGHT];
    ZOO_UINT16 port;
    ZOO_BOOL enabled;
}ZOO_LOG_SERVER_STRUCT;
    

/**
 *@brief Define user version 
 */
typedef struct 
{
    ZOO_CHAR project[ZOO_VERSION_LENGHT];//project type
    ZOO_CHAR major[ZOO_VERSION_LENGHT];//major project or software version
    ZOO_INT32 module_numbers;//sub device number
    ZOO_MODULE_VERSION_STRUCT modules[32];
}ZOO_USER_VERSION_STRUCT;

/**
 *@brief Define user version 
 */
typedef struct 
{
    ZOO_CHAR db[ZOO_USER_LENGHT];
    ZOO_CHAR log[ZOO_USER_LENGHT];
    ZOO_CHAR download[ZOO_USER_LENGHT];
    ZOO_CHAR utils[ZOO_USER_LENGHT];
}ZOO_OUTPUT_PATH_STRUCT;

/**
 *@brief Define user version 
 */
typedef struct 
{
    ZOO_CHAR project_cfg_file[ZOO_USER_LENGHT];
    ZOO_CHAR bin[ZOO_USER_LENGHT];
    ZOO_CHAR lib[ZOO_USER_LENGHT];
    ZOO_CHAR log[ZOO_USER_LENGHT];
    ZOO_CHAR error_code[ZOO_USER_LENGHT];
    ZOO_CHAR configuration_manager[ZOO_USER_LENGHT];
    ZOO_CHAR cert[ZOO_USER_LENGHT];
}ZOO_CONFIGURE_PATH_STRUCT;

/**
 *@brief Define user path  
 */
typedef struct 
{
    ZOO_OUTPUT_PATH_STRUCT output;
    ZOO_CONFIGURE_PATH_STRUCT configure;
}ZOO_USER_PATH_STRUCT;

/**
 *@brief Define zoo platform path 
 */
typedef struct 
{
    ZOO_OUTPUT_PATH_STRUCT output;
    ZOO_CONFIGURE_PATH_STRUCT configure;
}ZOO_PL_PATH_STRUCT;

/**
 *@brief Define all path 
 */
typedef struct 
{
    ZOO_USER_PATH_STRUCT user;
    ZOO_PL_PATH_STRUCT platform;
}ZOO_PATH_STRUCT;

#endif
