/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : DB
 * File Name    : DB4A_if.h
 * Description  : database access
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-05-07      MOZEAT       Create
 ***************************************************************/
#ifndef DB4A_TYPE_H
#define DB4A_TYPE_H
#include <ZOO.h>

//database error
/**
*  @brief Error Code Definition
*/
#define DB4A_BASE_ERR                   (0x44420000)
#define DB4A_SYSTEM_ERR                 ((DB4A_BASE_ERR) + 0X0001)
#define DB4A_PARAMETER_ERR              ((DB4A_BASE_ERR) + 0X0002)
#define DB4A_TIMEOUT_ERR                ((DB4A_BASE_ERR) + 0X0003)
#define DB4A_ILLEGAL_ERR                ((DB4A_BASE_ERR) + 0X0004)


/**
*  @brief Macro Definition
*/
#define DB4A_BUFFER_LENGTH      64
#define DB4A_TABLE_NAME_LEN	    32                  //表名字长度
#define DB4A_FIELD_NAME_LEN     32                  //字段名字长度
#define DB4A_DEFAULT_STRING_LEN 256             //默认字符长度
#define DB4A_MAX_VALUE_LEN      100 * 1024 * 1024   //数据库最大数据长度100k
#define DB4A_BIND_BUFF_LEN      128					//预处理绑定字符串长度
#define DB4A_MAX_COL			20

#define DB4A_USER_NAME_LEN (32)
#define DB4A_PASSWORD_LEN (64)
#define DB4A_DB_NAME_LEN (32)
#define DB4A_HOST_NAME_LEN (32)


typedef ZOO_UINT32 ZOO_HANDLE;


/**
 * @brief type of data base 
 *
 */

typedef enum
{	
	DB4A_SQLITE3,
	DB4A_MYSQL,
    DB4A_DATABASE_OTHERS
}DB4A_DATA_BASE_TYPE;

/**
 * @brief connect parameters
 */
typedef struct
{
	ZOO_CHAR name[DB4A_USER_NAME_LEN];
	ZOO_CHAR password[ DB4A_PASSWORD_LEN];
	ZOO_CHAR db_name[DB4A_DB_NAME_LEN];
	ZOO_CHAR host_name[DB4A_HOST_NAME_LEN];
	DB4A_DATA_BASE_TYPE db_type;
}DB4A_CONNECT_OPTION_STRUCT;

/**
 * @brief type of data could be stored
*/
typedef enum
{
	DB4A_FIELD_TYPE_MIN = 0,
	DB4A_FIELD_TYPE_NULL,//一个 NULL 值
	DB4A_FIELD_TYPE_CHAR,//一个 CHAR
	DB4A_FIELD_TYPE_INTEGER,//一个带符号的整数，根据值的大小存储在 1、2、3、4、6 或 8 字节中
	DB4A_FIELD_TYPE_REAL,//一个浮点值，存储为 8 字节的 IEEE 浮点数字
	DB4A_FIELD_TYPE_TEXT,//一个文本字符串，使用数据库编码（UTF-8、UTF-16BE 或 UTF-16LE）存储
	DB4A_FIELD_TYPE_BLOB,//一个 blob 数据，完全根据它的输入存储
	DB4A_FIELD_TYPE_MAX
}DB4A_FIELD_TYPE_ENUM;

/**
 * @brief field type
*/
typedef struct
{
	DB4A_FIELD_TYPE_ENUM type;
	ZOO_UINT32 length;	
	ZOO_CHAR  field_name[DB4A_FIELD_NAME_LEN];	//字段名称					//字段值，最大长度ADAE_MAX_VALUE_LEN
}DB4A_FIELD_TYPE_STRUCT;

/**
 * @brief field data struct
*/
typedef struct
{
	ZOO_CHAR  field_name[DB4A_FIELD_NAME_LEN];	//字段名称
    ZOO_CHAR field_data[DB4A_FIELD_NAME_LEN];						    //字段值，最大长度ADAE_MAX_VALUE_LEN
	ZOO_UINT32 len;
	ZOO_CHAR filler[4];
}DB4A_FIELD_DATA_STRUCT;

/**
 * @brief record data struct for get data form db
*/
typedef struct
{
    ZOO_INT32 fields_number;
    DB4A_FIELD_DATA_STRUCT field_list[DB4A_MAX_COL];
}DB4A_RECORD_STRUCT;

/**
 * @brief the logic type
*/
typedef enum
{
	DB4A_LOGIC_MIN = 0,
	DB4A_LOGIC_EQ ,                          //字段数据结构关系 等于
	DB4A_LOGIC_NOTEQ ,                       //字段数据结构关系  不等于
	DB4A_LOGIC_BG ,                          //字段数据结构关系  大于
	DB4A_LOGIC_LI,                           //字段数据结构关系  小于
	DB4A_LOGIC_LIKE,                         //字段数据结构关系  字符串匹配
	DB4A_LOGIC_NOTLIKE,                      //字段数据结构关系  字符串不匹配
	DB4A_LOGIC_AND,                          //条件逻辑与关系
	DB4A_LOGIC_OR,                           //条件逻辑或关系
	DB4A_LOGIC_MAX
}DB4A_LOGIC_ENUM;

/**
 * @brief 条件逻辑关系
*/
typedef enum
{
	DB4A_RELATION_AND ,           //与关系
	DB4A_RELATION_OR              //或关系
}DB4A_RELATION_ENUM;



#endif

