/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: TM
* File name: SM4A_type.h
* Description: interface for socket
* HistTMy recTMder:
* Version   date           auth            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef SM4A_TYPE_H
#define SM4A_TYPE_H
#include <ZOO.h>

/*
* Error Code Definition
*/
#define SM4A_BASE_ERR           (0x534D0000)
#define SM4A_SYSTEM_ERR         ((SM4A_BASE_ERR) + 0x01)
#define SM4A_TIMEOUT_ERR        ((SM4A_BASE_ERR) + 0x02)
#define SM4A_PARAMETER_ERR      ((SM4A_BASE_ERR) + 0x03)
#define SM4A_IILEGAL_CALL_ERR   ((SM4A_BASE_ERR) + 0x04)

#define SM_HOST_NAME_LENGTH 32				/*定义HOST名字最大长度*/
#define SM_HOST_ID_LENGTH   32				/*定义HOST号最大长度*/
#define SM_HOST_IP_LENGTH   32				/*定义HOST地址最大长度*/
#define SM_TASK_NAME_LENGHT 32              /*定义进程名字最大长度*/

#define SM_INVALID_PID      (-1)
#define SM_ILLEGAL_PID      (0)  /*其他用户进程号*/

/*数据结构：工作站信息*/
typedef struct
{
	ZOO_BOOL	host_present;						/*定义HOST是否已配置*/
	ZOO_CHAR	host_id[SM_HOST_ID_LENGTH];			/*定义HOST的ID号，棕龙的resource_id*/
	ZOO_CHAR	host_name[SM_HOST_NAME_LENGTH];		/*定义HOST的名字*/
	ZOO_CHAR	host_ip[SM_HOST_IP_LENGTH];			/*定义HOST的IP*/
} SM4A_HOST_INFO_STRUCT;

#endif

