/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: RC
* File name: RC_type.h
* Description: interface fRC socket
* HistRCy recRCder:
* Version   date           authRC            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef RC4A_TYPE_H
#define RC4A_TYPE_H
#include <ZOO.h>

/**
 * @brief Define buffer length
*/
#define RC4A_BUFFER_LENGTH  128
#define RC4A_MESSAGE_LENGTH 2048 /* 2K */

/**
 * @brief Define Error Code
*/
#define RC4A_BASE_ERR          (0x52430000)
#define RC4A_SYSTEM_ERR        ((RC4A_BASE_ERR) + 0x01)
#define RC4A_TIMEOUT_ERR       ((RC4A_BASE_ERR) + 0x02)
#define RC4A_PARAMETER_ERR     ((RC4A_BASE_ERR) + 0x03)
#define RC4A_ILLEGAL_CALL_ERR  ((RC4A_BASE_ERR) + 0x04)

/**
 * @brief Define connection option struct
*/
typedef struct
{
	ZOO_CHAR addr[RC4A_BUFFER_LENGTH];//地址
	ZOO_INT32 port;//端口号
	ZOO_INT32 keepalive;//心跳间隔
	ZOO_INT32 qos;
	ZOO_CHAR user[RC4A_BUFFER_LENGTH];//用户名
	ZOO_CHAR password[RC4A_BUFFER_LENGTH];//登入密码
	ZOO_CHAR client_id[RC4A_BUFFER_LENGTH];
	ZOO_UINT32 reconnect_delay;//the number of seconds to wait between reconnects
	ZOO_UINT32 reconnect_delay_max;//the maximum number of seconds to wait between reconnects.
}RC4A_CONNECT_OPTION_STRUCT;


typedef enum
{
    RC4A_TLS_V_MIN = -1,
    RC4A_TLS_V_DEFAULT = 0,
    RC4A_TLS_V_1_0 = 1,
    RC4A_TLS_V_1_1 = 2,
    RC4A_TLS_V_1_2 = 3,
    RC4A_TLS_V_1_3 = 4,
    RC4A_TLS_V_1_4 = 5,
    RC4A_TLS_V_1_5 = 6,
    RC4A_TLS_V_1_6 = 7,
    RC4A_TLS_V_2_1,
    RC4A_TLS_V_MAX
}RC4A_TLS_V_ENUM;


/**
 * @brief Define SSL connection option struct
*/
typedef struct
{
    ZOO_BOOL enable_ssl;
    RC4A_TLS_V_ENUM tls_v;
	ZOO_CHAR trust_chain_file[RC4A_BUFFER_LENGTH];//证书链
	ZOO_CHAR private_key_file[RC4A_BUFFER_LENGTH];//私钥
	ZOO_CHAR public_key_file[RC4A_BUFFER_LENGTH];//公钥		
}RC4A_SSL_OPTION_STRUCT;

/**
 * @brief Define offline persistence option struct
*/
typedef struct
{
	ZOO_BOOL enable_offline_persistence;
    ZOO_BOOL enable_rotate;
    ZOO_INT32 record_max;
    ZOO_CHAR db_file_name[RC4A_BUFFER_LENGTH];
    ZOO_CHAR table_name[RC4A_BUFFER_LENGTH];
}RC4A_OFFLINE_PERSISTENCE_OPTION_STRUCT;

/**
 * @brief Define option struct
*/
typedef struct
{
	RC4A_CONNECT_OPTION_STRUCT connection;
    RC4A_OFFLINE_PERSISTENCE_OPTION_STRUCT persistence;
    RC4A_SSL_OPTION_STRUCT ssl;
}RC4A_OPTION_STRUCT;

/**
 * @brief Define Connection status struct
*/
typedef struct
{
	ZOO_FD fd;
	ZOO_BOOL online;//是否在线
}RC4A_CONNECTION_STATUS_STRUCT;

/**
 * @brief Define Message struct
*/
typedef struct
{
	ZOO_FD fd;//the receiver id
	ZOO_INT32 number;//消息个数
	ZOO_INT32 index;//消息偏移 index = [0,number - 1]
	ZOO_INT32 length;//当前消息的长度
	ZOO_CHAR topic[RC4A_BUFFER_LENGTH];	//订阅的主题
	ZOO_CHAR data[RC4A_MESSAGE_LENGTH];//每个消息的内容
}RC4A_MESSAGE_STRUCT;

#endif
