/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : OI
 * File Name      : PerformFunctionThread.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef _ZOOPERFORM_FUNCTION_THREAD_H
#define _ZOOPERFORM_FUNCTION_THREAD_H

#include <QtGui>


namespace ZOO 
{

	/**
	 * @brief  线程执行函数类型定义
	 */
	typedef void (*THREAD_RUN_FUNC_HANDLE)(void * context_p);

	/**
	 * @brief  界面执行接口调用函数输入参数数据包定义
	 *
	 * 注：工作线程使用者可根据具体需求自己定义数据包结构体
	 */
	typedef struct
	{
		QObject * performObj;
		int widgetIndex;
		int funcCode;
		int filler;
	} THREAD_RUN_FUNC_CONTEXT_STRUCT;


	/**
	 * @brief  
	 */
	class PerformFunctionThread: public QThread
	{
		Q_OBJECT

	public:

		/**
		 * @brief 
		 */
		PerformFunctionThread(QObject * parent = 0);

		/**
		 * @brief 
		 */
		virtual ~PerformFunctionThread();

		/**
		 * @brief 
		 * @param runFunc 
		 * @param context_p 
		 */
		bool performFunctionStart(THREAD_RUN_FUNC_HANDLE runFunc, void * context_p);

	signals:

		/**
		 * @brief 
		 */
		void doFuncStartedSignal();

		/**
		 * @brief 
		 *
		 */
		void doFuncFinishedSignal();

	protected:

		/**
		 * @brief 
		 */
		void run();

	private:

		/**
		 * @brief
		 */
		QWaitCondition m_NoFuncRunWaitCondition;

		/**
		 * @brief
		 */
		QMutex m_ThreadMutex;

		/**
		 * @brief 
		 */
		bool b_ThreadTerminated;

		/**
		 * @brief 
		 */
		THREAD_RUN_FUNC_HANDLE m_ThreadRunFunc_p;

		/**
		 * @brief 
		 */
		void * m_ThreadRunFuncContext_p;
	};
}

#endif // _PERFORM_FUNCTION_THREAD_H
