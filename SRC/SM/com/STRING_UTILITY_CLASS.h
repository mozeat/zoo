/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        :
 * Component id   :
 * File Name      : TM_ENUM_REGISTER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-16       weiwang.sun      created
 *************************************************************/
#ifndef STRING_UTILITY_CLASS_H
#define STRING_UTILITY_CLASS_H
#include "SM_ENUM_REGISTER_CLASS.h"
#include <boost/smart_ptr.hpp>
extern "C"
{
    #include "SM4A_type.h"
}
namespace ZOO_SM
{
    class STRING_UTILITY_CLASS
    {
    public:
        virtual ~STRING_UTILITY_CLASS();
    public:
        /**
         * @brief Convert ZOO_TASK_STATE_ENUM to string
         */
        static const ZOO_CHAR* to_string(ZOO_TASK_STATE_ENUM task_state);

        /**
         * @brief Convert ZOO_TRACE_MODE_ENUM to string
         */
        static const ZOO_CHAR* to_string(ZOO_TRACE_MODE_ENUM trace_mode);

        /**
         * @brief Convert ZOO_WATCH_MODE_ENUM to string
         */
        static const ZOO_CHAR* to_string(ZOO_WATCH_MODE_ENUM watch_mode);

        /**
         * @brief Convert bool/ZOO_BOOL to string
         */
        template<typename BOOLEN = ZOO_BOOL>
        static const ZOO_CHAR* to_string(BOOLEN predicate)
        {
            const ZOO_CHAR * TRUE_STRING = "true";
            const ZOO_CHAR * FALSE_STRING = "false";
            if(predicate) return TRUE_STRING;
            return FALSE_STRING;
        }

        static ZOO_TRACE_MODE_ENUM to_trace_enum(const ZOO_CHAR* enum_string)
        {
            ZOO_TRACE_MODE_ENUM enumerator;
            if(get_instance()->m_trace_mode_converter.to_enum(enumerator,enum_string))
            {
               return enumerator;
            }
            return (ZOO_TRACE_MODE_ENUM)0;
        }

        static ZOO_WATCH_MODE_ENUM to_watch_enum(const ZOO_CHAR* enum_string)
        {
            ZOO_WATCH_MODE_ENUM enumerator;
            if(get_instance()->m_watch_mode_converter.to_enum(enumerator,enum_string))
            {
               return enumerator;
            }
            return (ZOO_WATCH_MODE_ENUM)0;
        }

        static ZOO_SIM_MODE_ENUM to_sim_mode_enum(const ZOO_CHAR* enum_string)
        {
            ZOO_SIM_MODE_ENUM enumerator;
            if(get_instance()->m_sim_mode_converter.to_enum(enumerator,enum_string))
            {
               return enumerator;
            }
            return (ZOO_SIM_MODE_ENUM)0;
        }

    private:
        STRING_UTILITY_CLASS()= default;

        /**
         * @brief Get instance
         * @return
         */
        static boost::shared_ptr<STRING_UTILITY_CLASS> get_instance();

        /**
         * @brief The singleton instance
         */
        static boost::shared_ptr<STRING_UTILITY_CLASS> m_instance;

        /**
         * @brief The ZOO_TASK_STATE_ENUM converter
         */
        ENUM_CONVERTER(ZOO_TASK_STATE_ENUM) m_task_state_converter;

        /**
         * @brief The ZOO_TRACE_MODE_ENUM converter
         */
        ENUM_CONVERTER(ZOO_TRACE_MODE_ENUM) m_trace_mode_converter;

        /**
         * @brief The ZOO_WATCH_MODE_ENUM converter
         */
        ENUM_CONVERTER(ZOO_WATCH_MODE_ENUM) m_watch_mode_converter;

        /**
         * @brief The ZOO_WATCH_MODE_ENUM converter
         */
        ENUM_CONVERTER(ZOO_SIM_MODE_ENUM) m_sim_mode_converter;
    };
}


#endif // STRING_UTILITY_CLASS_H
