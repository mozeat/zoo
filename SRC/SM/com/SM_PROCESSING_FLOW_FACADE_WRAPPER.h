/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : TM_PROCESSING_FLOW_FACADE_WRAPPER.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         20181001      mozeat.sun      created
 *************************************************************/
#ifndef TM_PROCESSING_FLOW_FACADE_WRAPPER_H
#define TM_PROCESSING_FLOW_FACADE_WRAPPER_H


#ifdef __cplusplus
extern "C"
{
#endif

#include "SM4A_type.h"
#include <ZOO_tc.h>

	/**
     * @brief Execute the business logic to start all tasks
     * @return Error code: OK
     *                     TM4A_SYSTEM_ERROR
     *                     TM4A_ILLEGAL_CALL_ERROR
     *                     TM4A_TIMEOUT_ERROR
     */
    ZOO_EXPORT ZOO_INT32 SM_initialize();
    
    /**
     * @brief Execute the business logic to start all tasks
     * @return Error code: OK
     *                     TM4A_SYSTEM_ERROR
     *                     TM4A_ILLEGAL_CALL_ERROR
     *                     TM4A_TIMEOUT_ERROR
     */
    ZOO_EXPORT ZOO_INT32 SM_start_all_tasks();

    /**
     * @brief Execute the business logic to stop all tasks
     * @return Error code: OK
     *                     TM4A_SYSTEM_ERROR
     *                     TM4A_ILLEGAL_CALL_ERROR
     *                     TM4A_TIMEOUT_ERROR
     */
    ZOO_EXPORT ZOO_INT32 SM_stop_all_tasks();

    /**
     * @brief Execute the business logic to start a single tasks
     * @return Error code: OK
     *                     TM4A_SYSTEM_ERROR
     *                     TM4A_ILLEGAL_CALL_ERROR
     *                     TM4A_TIMEOUT_ERROR
     */
    ZOO_EXPORT ZOO_INT32 SM_start_single_task(IN char name[SM_TASK_NAME_LENGHT],IN int stack_size,OUT ZOO_UINT32 * pid);

    /**
     * @brief Execute the business logic to stop a single tasks
     * @return Error code: OK
     *                     TM4A_SYSTEM_ERROR
     *                     TM4A_ILLEGAL_CALL_ERROR
     *                     TM4A_TIMEOUT_ERROR
     */
    ZOO_EXPORT ZOO_INT32 SM_stop_single_task(IN char name[SM_TASK_NAME_LENGHT]);


    /**
     * @brief get all task running state
     * @return Error code: OK
     *                     TM4A_SYSTEM_ERROR
     *                     TM4A_ILLEGAL_CALL_ERROR
     *                     TM4A_TIMEOUT_ERROR
     */
    ZOO_EXPORT ZOO_INT32 SM_get_task_state(IN char name[SM_TASK_NAME_LENGHT],OUT ZOO_TASK_STATE_ENUM * state);

    /**
     * @brief get all task running state
     * @return Error code: OK
     *                     TM4A_SYSTEM_ERROR
     *                     TM4A_ILLEGAL_CALL_ERROR
     *                     TM4A_TIMEOUT_ERROR
     */
    ZOO_EXPORT ZOO_INT32 SM_get_host_info(IN SM4A_HOST_INFO_STRUCT * host_info);

#ifdef __cplusplus
}
#endif

#endif // TM_PROCESSING_FLOW_FACADE_WRAPPER_H
