/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : TM_EXCEPTION_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#ifndef TM_EXCEPTION_CLASS_H_
#define TM_EXCEPTION_CLASS_H_
#include <exceptions/PARAMETER_EXCEPTION_CLASS.h>

namespace ZOO_SM
{
    class SM_EXCEPTION_CLASS : public ZOO_COMMON::PARAMETER_EXCEPTION_CLASS
    {
    public:
        /**
         * @brief Constructor
         * @param error_code        Error code
         * @param error_message     Error message
         * @param inner_exception   Inner exception
         */
        SM_EXCEPTION_CLASS(ZOO_INT32 error_code, ZOO_CHAR* error_message,
                           std::exception* inner_exception)
                : PARAMETER_EXCEPTION_CLASS(error_code, error_message, inner_exception)
        {

        }

        /**
         * @brief Constructor
         * @param error_code        Error code
         * @param error_message     Error message
         * @param inner_exception   Inner exception
         */
        SM_EXCEPTION_CLASS(ZOO_INT32 error_code, const ZOO_CHAR* error_message,
                           std::exception* inner_exception)
                : PARAMETER_EXCEPTION_CLASS(error_code, (ZOO_CHAR*) error_message, inner_exception)
        {

        }

        /**
         * @brief Destructor
         */
        virtual ~SM_EXCEPTION_CLASS() throw ()
        {

        }
    };
}


#endif // TM_EXCEPTION_CLASS_H_INCLUDED
