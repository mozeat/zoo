/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : TASK_CONTROLLER_INTERFACE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#ifndef TASK_CONTROLLER_INTERFACE_H
#define TASK_CONTROLLER_INTERFACE_H

extern "C"
{
	#include <SM4A_type.h>
	#include "ZOO_if.h"
}
#include <string>
#include "TASK_INTERFACE.h"
namespace ZOO_SM
{
    class TASK_CONTROLLER_INTERFACE
    {
    public:
        /**
		 * @brief Constructor
		 */
        TASK_CONTROLLER_INTERFACE() {}

        /**
		 * @brief Destructor
		 */
        virtual ~TASK_CONTROLLER_INTERFACE() {}
    public:
        /**
		 * @brief initialize
		 * @param
		 * @return void
		 */
	    virtual void initialize() = 0;

		/**
		 * @brief create all configuration running tasks
		 * @param
		 * @return void
		 */
		virtual void create_all_models() = 0;

		/**
		 * @brief startup all configuration running tasks
		 * @param
		 * @return void
		 */
		virtual void startup_all_models() = 0;

		/**
		 * @brief stop all configuration running tasks
		 * @param
		 * @return void
		 */
		virtual void stop_all_models() = 0;

		/**
		 * @brief startup a task by name
		 * @param name       task file name
		 * @return void
		 */
		virtual void start(IN const std::string & name) = 0;

		/**
		 * @brief stop a task by name
		 * @param name       task file name
		 * @return void
		 */
		virtual void stop(IN const std::string & name) = 0;

		/**
		 * @brief get task running status
		 * @param name       task file name
		 * @return ZOO_TASK_STATE_ENUM
		 */
		virtual ZOO_TASK_STATE_ENUM  get_running_state(IN std::string & name) = 0;

		/**
		 * @brief set task running status
		 * @param name		 task name
		 * @param state		 task state
		 * @return void
		 */
		virtual void set_running_state(IN std::string & name,
												IN ZOO_TASK_STATE_ENUM state) = 0;

		/**
		 * @brief get task instance by name
		 * @param name       task name
		 * @return object instance
		 */
		virtual boost::shared_ptr<TASK_INTERFACE> get_task(IN std::string & name) = 0;

		/**
		 * @brief get process pid
		 * @param name       process name
		 * @return ZOO_INT32
		 */
		virtual ZOO_INT32  get_pid(IN std::string & name) = 0;

		/**
		 * @brief set process pid
		 * @param pid       pid
		 * @return 
		 */
		virtual void  set_pid(IN std::string & name,
		                         IN ZOO_INT32 pid) = 0;
    };

}

#endif // TASK_CONTROLLER_INTERFACE_H
