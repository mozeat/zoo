/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        :
 * Component id   :
 * File Name      : TM_ENUM_REGISTER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-16       weiwang.sun      created
 *************************************************************/
#ifndef TM_ENUM_REGISTER_CLASS_H
#define TM_ENUM_REGISTER_CLASS_H


/*##########################################################################*
                    Header File
*###########################################################################*/
#include <converters/ENUM_BASE_CONVERTER_CLASS.h>
#include <ZOO_tc.h>
#include <SM4A_type.h>
namespace ZOO_SM
{
    REGISTER_ENUM_BEGIN(ZOO_TASK_STATE_ENUM){
        REGISTER_ENUM(ZOO_TASK_STATE_MIN);
        REGISTER_ENUM(ZOO_TASK_STATE_STARTED);
        REGISTER_ENUM(ZOO_TASK_STATE_NOT_STARTED);
        REGISTER_ENUM(ZOO_TASK_STATE_MAX);
    }REGISTER_ENUM_END;


    REGISTER_ENUM_BEGIN(ZOO_WATCH_MODE_ENUM){
        REGISTER_ENUM(ZOO_WATCH_MODE_MIN);
        REGISTER_ENUM(ZOO_WATCH_MODE_DISABLE);
        REGISTER_ENUM(ZOO_WATCH_MODE_TIMES_3);
        REGISTER_ENUM(ZOO_WATCH_MODE_TIMES_5);
        REGISTER_ENUM(ZOO_WATCH_MODE_MAX);
    }REGISTER_ENUM_END;


    REGISTER_ENUM_BEGIN(ZOO_TRACE_MODE_ENUM){
        REGISTER_ENUM(ZOO_TRACE_MODE_MIN);
        REGISTER_ENUM(ZOO_TRACE_DISABLE);
        REGISTER_ENUM(ZOO_TRACE_ENABLE);
        REGISTER_ENUM(ZOO_TRACE_MODE_MAX);
    }REGISTER_ENUM_END;

    REGISTER_ENUM_BEGIN(ZOO_SIM_MODE_ENUM){
        REGISTER_ENUM(ZOO_SIM_MODE_MIN);
        REGISTER_ENUM(ZOO_SIM_DISABLE);
        REGISTER_ENUM(ZOO_SIM_MODE_1);
        REGISTER_ENUM(ZOO_SIM_MODE_2);
        REGISTER_ENUM(ZOO_SIM_MODE_3);
        REGISTER_ENUM(ZOO_SIM_MODE_4);
        REGISTER_ENUM(ZOO_SIM_MODE_MAX);
    }REGISTER_ENUM_END;
}
#endif // TM_ENUM_REGISTER_CLASS_H
