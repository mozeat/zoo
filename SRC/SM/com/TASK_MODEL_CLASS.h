/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : TASK_MODEL_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018��9��14��       weiwang.sun      created
 *************************************************************/
#ifndef TASK_MODEL_CLASS_H_
#define TASK_MODEL_CLASS_H_
extern "C"
{
	#include "SM4A_type.h"
}

#include <string>
#include <TASK_ABSTACT_MODEL_CLASS.h>
namespace ZOO_SM
{
	class TASK_MODEL_CLASS : public virtual TASK_ABSTACT_MODEL_CLASS,
								    public virtual TASK_INTERFACE
	{
	public:
		/**
		 * @brief Constructor
		 */
		TASK_MODEL_CLASS();

		/**
		 * @brief Destructor
		 */
		virtual ~TASK_MODEL_CLASS();

	public:
		/**
         * @brief Start process
         * @return
         */
        void start();

        /**
         * @brief Kill process
         * @return
         */
        void stop();

		/**
		 * @brief Restart process
		 * @return
		 */
		void restart();

		/**
		 * @brief Get process pid
		 * @return
		 */
		ZOO_INT32 get_pid(std::string process_name);
	private:
		ZOO_BOOL m_stop;

		
		/**
         * @brief execute
         */
        boost::shared_ptr<EXECUTE_HALEPER_CLASS> m_execute_helper;
	};
}
#endif /* SRC_TM_COM_TASK_CLASS_H_ */
