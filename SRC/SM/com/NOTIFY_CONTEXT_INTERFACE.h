/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : NOTIFY_CONTEXT_INTERFACE.h
 * Description  : The observer class to listen property change
 * History :
 * Version      Date				User			Comments
 * V1.0.0.0     2018-05-20			MOZEAT			Initialize
 ***************************************************************/
#ifndef NOTIFY_CONTEXT_INTERFACE_H_
#define NOTIFY_CONTEXT_INTERFACE_H_
extern "C"
{
#include "ZOO.h"
}

namespace ZOO_SM
{

    class NOTIFY_CONTEXT_INTERFACE
    {
    public:
        NOTIFY_CONTEXT_INTERFACE();
        virtual ~NOTIFY_CONTEXT_INTERFACE();

    };

} /* namespace SCHEDULER */

#endif /* NOTIFY_CONTEXT_INTERFACE_H_ */

