/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : ERROR_CODE_CONVERTER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#ifndef ERROR_CODE_CONVERTER_CLASS_H
#define ERROR_CODE_CONVERTER_CLASS_H
#include <boost/smart_ptr.hpp>
extern "C"
{
    #include "SM4A_type.h"
}
namespace ZOO_SM
{
    class ERROR_CODE_CONVERTER_CLASS
    {
    public:
        /**
         * @brief Destructor
         */
        virtual ~ERROR_CODE_CONVERTER_CLASS();
    public:

        /**
         * @brief Get instance of EXCEPTION_CONVERTER_CLASS
         */
        static boost::shared_ptr<ERROR_CODE_CONVERTER_CLASS> get_instance();

        /**
         * @brief Convert exception
         * @param ex    exception class
         */
        ZOO_INT32 convert_error_code_exception(ZOO_INT32 error_code);

    private:
        /**
         * @brief Constructor
         */
        ERROR_CODE_CONVERTER_CLASS();

        /**
         * @brief Singleton instance of EXCEPTION_CONVERTER_CLASS
         */
        static boost::shared_ptr<ERROR_CODE_CONVERTER_CLASS> m_instance;
    };
}


#endif // ERROR_CODE_CONVERTER_CLASS_H
