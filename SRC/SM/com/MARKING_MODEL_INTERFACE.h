#ifndef MARKING_MODEL_INTERFACE_H
#define MARKING_MODEL_INTERFACE_H

extern "C"
{
    #include <ZOO.h>
    #include <stdio.h>
    #include <string.h>
}
#include <boost/shared_array.hpp>
namespace ZOO_SM
{
    class MARKING_MODEL_INTERFACE
    {
    public:
        /**
         * @brief Constructor
         */
        MARKING_MODEL_INTERFACE();

        /**
         * @brief Destructor
         */
        virtual ~MARKING_MODEL_INTERFACE();
    public:
        /**
         * @brief Get the marking_code attribute value
         */
        virtual const ZOO_CHAR* get_marking_code();

        /**
         * @brief Set the marking_code attribute value
         * @param marking_code    The new marking_code attribute value
         */
        virtual void set_marking_code(const ZOO_CHAR* marking_code);

    protected:
        /**
         * @brief The marking_code attribute
         */
        boost::shared_array<ZOO_CHAR> m_marking_code;
    };
}
#endif // MARKING_MODEL_INTERFACE_H
