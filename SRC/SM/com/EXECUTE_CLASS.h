/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        :
 * Component id   :
 * File Name      : EXECUTE_HALEPER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-16       weiwang.sun      created
 *************************************************************/
#ifndef EXECUTE_CLASS_H
#define EXECUTE_CLASS_H
#include <string>
#include <vector>
namespace ZOO_SM
{
    class EXECUTE_CLASS
    {
    public:
        /**
         * @brief Constructor
         */
        EXECUTE_CLASS();

        /**
         * @brief Destructor
         */
        virtual ~EXECUTE_CLASS();
    public:

        /**
         * @brief Set command
         * @param command    the .exe name
        */
        void set_command(std::string command);

        /**
         * @brief Set the command parameters
         * @param parameters
        */
        void set_parameters(const std::vector<std::string> & parameters);

        /**
         * @brief Get command parameters
         * @return parameters
        */
        const std::vector<std::string> & get_parameters();

        /**
         * @brief Get command
         * @return command name
        */
        const std::string & get_command();
    private:

        /**
         * @brief  command
        */
        std::string m_command;

        /**
         * @brief Parameters
        */
        std::vector<std::string> m_parameters;
    };
}


#endif // EXECUTE_CLASS_H
