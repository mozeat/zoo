/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : PROCESSING_FLOW_FACADE_CALSS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#ifndef PROCESSING_FLOW_FACADE_CALSS_H
#define PROCESSING_FLOW_FACADE_CALSS_H
extern "C"
{
    #include "SM4A_type.h"
    #include "SM4I_type.h"
}
#include "FLOW_FACADE_ABSTRACT_CLASS.h"
#include "SM_EXCEPTION_CLASS.h"
#include "PROCESSING_FLOW_FACADE_INTERFACE.h"
namespace ZOO_SM
{
    class PROCESSING_FLOW_FACADE_CALSS : public virtual FLOW_FACADE_ABSTRACT_CLASS,
                                         public virtual PROCESSING_FLOW_FACADE_INTERFACE
    {
    public:
        /**
         * @brief Constructor
         */
        PROCESSING_FLOW_FACADE_CALSS();

        /**
         * @brief Destructor
         */
        virtual ~PROCESSING_FLOW_FACADE_CALSS();
    public:

    	/**
         * @brief Load platform configure file ,generate database file
         * @param
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        ZOO_INT32 initialize();
        
        /**
         * @brief Start all configuration task
         * @param
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        ZOO_INT32 start_all_tasks();

        /**
         * @brief Stop all configuration task
         * @param
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        ZOO_INT32 stop_all_tasks();

        /**
         * @brief Start single task
         * @param name
         * @param stack_size
         * @param pid         the process id
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        ZOO_INT32 start_single_task(IN std::string name,
        										  IN ZOO_INT32 stack_size,
        										  OUT ZOO_UINT32 * pid);

        /**
         * @brief Stop single task
         * @param
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        ZOO_INT32 stop_single_task(IN std::string name);

        /**
         * @brief Stop single task
         * @param name
         * @param state
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        ZOO_INT32 get_task_state(IN std::string name,
        										OUT ZOO_TASK_STATE_ENUM * state);
        										
		/**
         * @brief Get host info
         * @param name
         * @param state
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        ZOO_INT32 get_host_info(IN SM4A_HOST_INFO_STRUCT * host_info);	

    	/**
         * @brief Startup local brokers before process communication
         * @return void
         */
    	void startup_local_broker();

    	/**
         * @brief Shutdown local brokers 
         * @return void
         */
    	void shutdown_local_broker();
    private:

    	/**
         * @brief brokers task 
         */
    	boost::shared_ptr<TASK_MODEL_CLASS> m_broker_task;
    };
}


#endif // PROCESSING_FLOW_FACADE_CALSS_H
