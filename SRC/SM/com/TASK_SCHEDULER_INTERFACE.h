/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : TASK_SHULDULER_INTERFACE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#ifndef TASK_SHULDULER_INTERFACE_H
#define TASK_SHULDULER_INTERFACE_H
#include <boost/asio.hpp>
#include <NOTIFY_CONTEXT_INTERFACE.h>
#include <TASK_MODEL_CLASS.h>
#include <LOG_MANAGER_CLASS.h>
#include <list>
namespace ZOO_SM
{
    class TASK_SCHEDULER_INTERFACE
    {
        public:
            TASK_SCHEDULER_INTERFACE() {}
            virtual ~TASK_SCHEDULER_INTERFACE() {}

        public:
        	/**
	         * @brief Start eventloop to monitor the process running state
	         * @return
	         */
            virtual void start() = 0;
			
			/**
	         * @brief Stop eventloop
	         * @return
	         */
			virtual void stop() = 0;
			
			/**
			 * @brief Add an process to watch 
			 * @return
			 */
			virtual void add_observer(boost::shared_ptr<TASK_INTERFACE> observer) = 0;

			/**
			 * @brief Remove an process to watch 
			 * @return
			 */
			virtual void remove_observer(boost::shared_ptr<TASK_INTERFACE> observer) = 0;

			/**
			 * @brief Clean
			 */
			virtual void clean() = 0;

			/**
			 * @brief Notify property has been changed
			 * @param property_name     The property has been changed
			 */
			void notify_of_property_changed(const ZOO_INT32 property_name);
    };
}

#endif // TASK_SHULDULER_INTERFACE_H
