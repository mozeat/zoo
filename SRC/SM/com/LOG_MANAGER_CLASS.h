/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : LOG_MANAGER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         20181001      mozeat.sun      created
 *************************************************************/
#ifndef LOG_MANAGER_CLASS_H
#define LOG_MANAGER_CLASS_H
extern "C"
{
    #include <ZOO_if.h>
}
#include <iostream>
#include <stdexcept>
#include <string>
#include <fstream>
#include <functional>
#include <boost/smart_ptr.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
namespace ZOO_SM
{
    class LOG_MANAGER_CLASS
    {
    public:
        LOG_MANAGER_CLASS();
        virtual ~LOG_MANAGER_CLASS();
        enum severity_level
        {
            normal,
            notification,
            warning,
            error,
            critical
        };
    public:
        static void initialize();
        static void log(const char* function_name,const char* format, ...);
		static void debug(const char* function_name,const char* format, ...);
    private:
        static void add_sink(int level,std::string log_file);
    };
}


#endif // LOG_MANAGER_CLASS_H
