/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : SMMA_event.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/
#ifndef SMMA_EVENT_H
#define SMMA_EVENT_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include "SM4I_type.h"
#include "SM4I_if.h"
#include "SM4A_type.h"

/**
 *@brief SMMA_raise_4A_initialize
**/
ZOO_EXPORT ZOO_INT32 SMMA_raise_4A_initialize(IN ZOO_INT32 error_code,
                                                  IN SM4I_REPLY_HANDLE reply);


/**
 *@brief SMMA_raise_4A_start_all_tasks
**/
ZOO_EXPORT ZOO_INT32 SMMA_raise_4A_start_all_tasks(IN ZOO_INT32 error_code,
                                                       IN SM4I_REPLY_HANDLE reply);


/**
 *@brief SMMA_raise_4A_stop_all_tasks
**/
ZOO_EXPORT ZOO_INT32 SMMA_raise_4A_stop_all_tasks(IN ZOO_INT32 error_code,
                                                      IN SM4I_REPLY_HANDLE reply);


/**
 *@brief SMMA_raise_4A_start_single_task
 *@param name[SM_TASK_NAME_LENGHT]
 *@param stack_size
 *@param pid
**/
ZOO_EXPORT ZOO_INT32 SMMA_raise_4A_start_single_task(IN ZOO_INT32 error_code,
                                                         IN ZOO_UINT32 pid,
                                                         IN SM4I_REPLY_HANDLE reply);


/**
 *@brief SMMA_raise_4A_stop_single_task
 *@param name[SM_TASK_NAME_LENGHT]
**/
ZOO_EXPORT ZOO_INT32 SMMA_raise_4A_stop_single_task(IN ZOO_INT32 error_code,
                                                        IN SM4I_REPLY_HANDLE reply);


/**
 *@brief SMMA_raise_4A_get_task_state
 *@param name[SM_TASK_NAME_LENGHT]
 *@param state
**/
ZOO_EXPORT ZOO_INT32 SMMA_raise_4A_get_task_state(IN ZOO_INT32 error_code,
                                                      IN ZOO_TASK_STATE_ENUM state,
                                                      IN SM4I_REPLY_HANDLE reply);


/**
 *@brief SMMA_raise_4A_get_host_info
 *@param host_info
**/
ZOO_EXPORT ZOO_INT32 SMMA_raise_4A_get_host_info(IN ZOO_INT32 error_code,
                                                     IN SM4A_HOST_INFO_STRUCT host_info,
                                                     IN SM4I_REPLY_HANDLE reply);



#endif // SMMA_event.h
