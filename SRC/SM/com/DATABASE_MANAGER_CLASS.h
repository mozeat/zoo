/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : DATABASE_MANAGER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#ifndef DATABASE_MANAGER_CLASS_H
#define DATABASE_MANAGER_CLASS_H
#include "SM_CONFIGURE.h"
#include <boost/assert.hpp>

namespace ZOO_SM
{
    class DATABASE_MANAGER_CLASS
    {
    public:
        /**
         * @brief destructor
         */
        virtual ~DATABASE_MANAGER_CLASS();

        /**
         * @brief get unique instance
         * @return instance object
         */
        static  boost::shared_ptr<DATABASE_MANAGER_CLASS>  get_instance();

        /**
         * @brief Initialize
         */
        void initialize();
        
        /**
         * @brief Get resource_id
         * @return
         */
        std::string get_resource_id();
        
		/**
		 * @brief Get host info
		 */
		const SM4A_HOST_INFO_STRUCT & get_host_info();

    private:
        /**
         * @brief Constructor
         */
        DATABASE_MANAGER_CLASS();
        
    	/**
         * @brief Create database by xlsx
         */
		void create_database_by_xlsx(IN std::string xlsx_file, 
												IN std::string output);
		/**
         * @brief Check file exist
         */										
		ZOO_BOOL exist(IN std::string db_file);

		/**
		 * @brief Check file exist
		 */ 
		ZOO_BOOL rename(IN std::string xlsx_file, 
												IN std::string & output);
		/**
		 * @brief create output path exist
		 */ 										
		void create_database_output_path(IN std::string path);					

		/**
		 * @brief Create error code database
		 */ 										
		void create_error_code_database(IN std::string cfg_path,
													IN std::string output_path,
													IN std::vector<ZOO_TASK_STRUCT*> & tasks);				
    private:

        /**
         * @brief get unique instance
         * @return instance object
         */
        static  boost::shared_ptr<DATABASE_MANAGER_CLASS>  m_instance;

        /**
		 * @brief The resource id
		 */
		std::string m_resource_id;
		
		/**
		 * @brief host info
		 */
		SM4A_HOST_INFO_STRUCT m_host_info;
    };
}
#endif // DATABASE_MANAGER_CLASS_H

