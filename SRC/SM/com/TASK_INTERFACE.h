/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : TASK_INTERFACE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#ifndef TASK_INTERFACE_H
#define TASK_INTERFACE_H

extern "C"
{
    #include "SM4A_type.h"
}

#include "EXECUTE_HALEPER_CLASS.h"
#include "MARKING_MODEL_INTERFACE.h"
#include "ENVIRONMENT_UTILITY_CLASS.h"
#include "SM_CONFIGURE.h"
#include "SM_macro_define.h"
#include <property_changes/NOTIFY_PROPERTY_CHANGED_INTERFACE.h>
#include <atomic>
#include <boost/process.hpp>
#include <boost/filesystem.hpp>
#include <boost/memory_order.hpp>
#include <chrono>
#include <thread>
#include <string>
namespace ZOO_SM
{
    class TASK_INTERFACE : public virtual MARKING_MODEL_INTERFACE
    {
    public:
        /**
         * @brief Constructor
         */
        TASK_INTERFACE();

        /**
         * @brief Destructor
         */
        virtual ~TASK_INTERFACE();
    public:
        /**
         * @brief Start process
         * @return
         */
        virtual void start() = 0;

        /**
         * @brief Kill process
         * @return
         */
        virtual void stop() = 0;

		/**
		 * @brief restart process
		 * @return
		 */
		virtual void restart() = 0;

		/**
		 * @brief Set process pid
		 * @return
		 */
		//virtual void set_pid(ZOO_INT32 pid) = 0;

		/**
		 * @brief Get process pid
		 * @return
		 */
		virtual ZOO_INT32 get_pid(std::string process_name) = 0;
    };
}
#endif // TASK_INTERFACE_H
