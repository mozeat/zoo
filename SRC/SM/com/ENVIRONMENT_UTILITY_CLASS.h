#ifndef ENVIRONMENT_UTILITY_CLASS_H
#define ENVIRONMENT_UTILITY_CLASS_H
#include <unistd.h>
#include <string>
#include <stdlib.h>
extern "C"
{
	#include "SM4A_type.h"
	#include "ZOO_if.h"
}
namespace ZOO_SM
{
    class ENVIRONMENT_UTILITY_CLASS
    {
    public:
        ENVIRONMENT_UTILITY_CLASS();
        virtual ~ENVIRONMENT_UTILITY_CLASS();

    public:
    
        /**
         * @brief get username
         */
        static std::string get_username();
        
        /**
         * @brief get process id by name
         */
        static pid_t get_pid(std::string process_name);
        
        /**
         * @brief get process id by name
         */
        static pid_t get_current_user_pid(std::string process_name);
    private:
    };
}


#endif // ENVIRONMENT_UTILITY_CLASS_H
