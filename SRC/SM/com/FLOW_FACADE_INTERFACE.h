/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : FLOW_FACADE_INTERFACE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/

#ifndef FLOW_FACADE_INTERFACE_H
#define FLOW_FACADE_INTERFACE_H
#include "TASK_CONTROLLER_INTERFACE.h"
#include "TASK_SCHEDULER_INTERFACE.h"
#include <boost/smart_ptr.hpp>
namespace ZOO_SM
{
    class FLOW_FACADE_INTERFACE
    {
    public:
        /**
         * @brief Constructor
         */
        FLOW_FACADE_INTERFACE();

        /**
         * @brief Destructor
         */
        virtual ~FLOW_FACADE_INTERFACE();
    public:
        /**
         * @brief Set reference to task controller
         * @param device_controller    The pointer which is pointed to task controller
         */
        virtual void set_task_controller(
                boost::shared_ptr<TASK_CONTROLLER_INTERFACE> task_controller) = 0;

		/**
         * @brief Set reference to task scheduler controller
         * @param device_controller    The pointer which is pointed to task scheduler controller
         */
        virtual void set_task_scheduler(
                boost::shared_ptr<TASK_SCHEDULER_INTERFACE> task_scheduler) = 0;
    };
}


#endif // FLOW_FACADE_INTERFACE_H
