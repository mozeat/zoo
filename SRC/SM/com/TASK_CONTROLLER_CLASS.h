/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        :
 * Component id   :
 * File Name      : TASK_CONTROLLER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         20181001       weiwang.sun      created
 *************************************************************/

#ifndef TASK_CONTROLLER_CLASS_H_
#define TASK_CONTROLLER_CLASS_H_

extern "C"
{
    #include "SM4A_type.h"
    #include "ZOO_if.h"
}

#include <boost/smart_ptr.hpp>
#include <boost/foreach.hpp>
#include <boost/fusion/include/for_each.hpp>
#include <map>
#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include "TASK_MODEL_CLASS.h"
#include "TASK_CONTROLLER_INTERFACE.h"
#include "LOG_MANAGER_CLASS.h"
#include <utils/ENVIRONMENT_UTILITY_CLASS.h>
#include <utils/THREAD_POOL.h>
#include "SM_CONFIGURE.h"

namespace ZOO_SM
{
	class TASK_CONTROLLER_CLASS: public virtual TASK_CONTROLLER_INTERFACE
	{
	public:
	    /**
		 * @brief Constructor
		 */
		TASK_CONTROLLER_CLASS();

		/**
		 * @brief Destructor
		 */
		virtual ~TASK_CONTROLLER_CLASS();
	public:

	    /**
		 * @brief initialize
		 * @param
		 * @return void
		 */
	    void initialize();

		/**
		 * @brief create all configuration running tasks
		 * @param
		 * @return void
		 */
		void create_all_models();

		/**
		 * @brief startup all configuration running tasks
		 * @param
		 * @return void
		 */
		void startup_all_models();

		/**
		 * @brief stop all configuration running tasks
		 * @param
		 * @return void
		 */
		void stop_all_models();

		/**
		 * @brief startup a task by name
		 * @param name       task file name
		 * @return void
		 */
		void start(const std::string & name);

		/**
		 * @brief stop a task by name
		 * @param name       task file name
		 * @return void
		 */
		void stop(const std::string & name);

		/**
		 * @brief set task running status
		 * @param name		 task name
		 * @param state 	 task state
		 * @return void
		 */
		void set_running_state(std::string & name,ZOO_TASK_STATE_ENUM state);

		/**
		 * @brief get task running status
		 * @param name       task file name
		 * @return ZOO_TASK_STATE_ENUM
		 */
		ZOO_TASK_STATE_ENUM  get_running_state(std::string & name);

		/**
		 * @brief get task instance by name
		 * @param name       task name
		 * @return object instance
		 */
		boost::shared_ptr<TASK_INTERFACE> get_task(std::string & name);

		/**
		 * @brief get all enabled tasks
		 * @return list
		 */
		std::list<boost::shared_ptr<TASK_MODEL_CLASS> > get_all_tasks();
			
		/**
		 * @brief get process pid
		 * @param name       process name
		 * @return ZOO_INT32
		 */
		ZOO_INT32  get_pid(std::string & name);

		/**
		 * @brief set process pid
		 * @param pid       pid
		 * @return 
		 */
		void  set_pid(std::string & name,ZOO_INT32 pid);
	private:
	    /**
		 * @brief check watch tasks and stop them
		 * @param
		 * @return void
		 */
	    void stop_all_watching();

	    /**
		 * @brief stop the task by name
		 * @param name       task  name
		 * @return void
		 */
        void stop_watching(std::string & name);

		/**
		 * @brief get task running status
		 * @param name       task file name
		 * @return boost::shared_ptr<TASK_MODEL_CLASS>
		 */
		void create_model(ZOO_TASK_STRUCT * task);
	private:

        /**
		 * @brief Componet vector
		 */
        std::map<std::string,bool> m_run_componet;

		/**
		 * @brief tasks container
		 */
		std::map<std::string,boost::shared_ptr<TASK_MODEL_CLASS> > m_tasks_in_db;
	};
} /* namespace ZOO_SM */

#endif /* TASK_CONTROLLER_CLASS_H_ */
