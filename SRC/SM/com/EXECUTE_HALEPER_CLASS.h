/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        :
 * Component id   :
 * File Name      : EXECUTE_HALEPER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-16       weiwang.sun      created
 *************************************************************/
#ifndef EXECUTE_HALEPER_CLASS_H
#define EXECUTE_HALEPER_CLASS_H
extern "C"
{
    #include "SM4A_type.h"
    #include "SM4I_type.h"
}
#include <boost/process.hpp>
#include <boost/process/posix.hpp>
#include <boost/process/extend.hpp>
#include <boost/thread/future.hpp>
#include <thread>
#include <unistd.h>
#include <errno.h>
#include <sstream>
#include "EXECUTE_CLASS.h"
#include "ZOO_COMMON_MACRO_DEFINE.h"
#include "LOG_MANAGER_CLASS.h"
namespace ZOO_SM
{
    typedef enum
    {
        PUBLISHER_THREAD = 0,
        BACKGROUND_THREAD
    }THREAD_OPTION;

    class TASK_INTERFACE;
    class EXECUTE_HALEPER_CLASS : public boost::process::extend::handler
    {
    public:
        /**
         * @brief Constructor
         */
        EXECUTE_HALEPER_CLASS(IN boost::shared_ptr<TASK_INTERFACE> task = NULL);

        /**
         * @brief Destructor
         */
        virtual ~EXECUTE_HALEPER_CLASS();


        template<typename Executor>
        void on_setup(boost::process::extend::posix_executor<Executor> & exec)
        {
            LOG_MANAGER_CLASS(__FUNCTION_NAME__," [%s] father process setup ...",exec.exe);
        }

        /** This function is invoked if an error occured while trying to launch the process.
         * \note It is not required to be const.
         */
        template <class Executor>
        void on_error(boost::process::extend::posix_executor<Executor> & exec, const std::error_code & ec)
        {
            std::stringstream ss;
            ss <<"can not launch " << exec.exe << " ...ERROR[" << ec.value() << "]" ;
            LOG_MANAGER_CLASS(__FUNCTION_NAME__," %",ss.str().c_str());
        }

        /** This function is invoked if the process was successfully launched.
         * \note It is not required to be const.
         */
        template <class Executor>
        void on_success(boost::process::extend::posix_executor<Executor> & exec)
        {

        }

        /**This function is invoked if an error occured during the call of `fork`.
         * \note This function will only be called on posix.
         */
        template<typename Executor>
        void on_fork_error(boost::process::extend::posix_executor<Executor> & exec, const std::error_code & ec)
        {
            std::stringstream ss;
            ss <<"can not fork " << exec.exe << " ...ERROR[" << ec.value() << "]" ;
            LOG_MANAGER_CLASS(__FUNCTION_NAME__," %s",ss.str().c_str());
        }

        /**This function is invoked if the call of `fork` was successful, before
         * calling `execve`.
         * \note This function will only be called on posix.
         * \attention It will be invoked from the new process.
         */
        template<typename Executor>
        void on_exec_setup(boost::process::extend::posix_executor<Executor> & exec )
        {
            LOG_MANAGER_CLASS(__FUNCTION_NAME__," [%s] on exec setup ...SUCCESS",exec.exe);
            this->notify(ZOO_TASK_STATE_STARTED);
        }

        /**This function is invoked if the call of `execve` failed.
         * \note This function will only be called on posix.
         * \attention It will be invoked from the new process.
         */
        template<typename Executor>
        void on_exec_error(boost::process::extend::posix_executor<Executor> & exec, const std::error_code & ec)
        {
            this->notify(ZOO_TASK_STATE_NOT_STARTED);
            std::stringstream ss;
            ss <<"can not execute " << exec.exe << " ...ERROR[" << ec.value() << "]" ;
            LOG_MANAGER_CLASS(__FUNCTION_NAME__," %s",ss.str().c_str());
        }
    public:
        /**
         * @brief Invoke a process
         */
        void invoke(boost::shared_ptr<EXECUTE_CLASS> execute,THREAD_OPTION option);

        /**
         * @brief Terminate a process if running
         */
        void terminate();

        /**
         * @brief Check is running
         */
        bool running();
    private:
        /**
         * @brief Execute a process
         */
        void do_execute(std::string exe,boost::process::extend::handler & handle);
    private:

        /**
         * @brief Notify task state
         */
        void notify(ZOO_TASK_STATE_ENUM state);

        /**
         * @brief Task
         */
        boost::shared_ptr<TASK_INTERFACE> m_task;

        /**
         * @brief System action
         */
        boost::shared_ptr<boost::process::child> m_system;
    };
}
#endif // EXECUTE_HALEPER_CLASS_H
