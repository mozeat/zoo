/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        :
 * Component id   :
 * File Name      : TASK_ABSTACT_MODEL_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         20181001      mozeat.sun      created
 *************************************************************/
#ifndef TASK_ABSTACT_MODEL_CLASS_H
#define TASK_ABSTACT_MODEL_CLASS_H

extern "C"
{
    #include "SM4A_type.h"
}
#include "TASK_INTERFACE.h"
#include "LOG_MANAGER_CLASS.h"
#include <STRING_UTILITY_CLASS.h>
namespace ZOO_SM
{
    /**
     * @brief class TASK_ABSTACT_MODEL_CLASS  for task
     */
    class TASK_ABSTACT_MODEL_CLASS
    {
    public:
        /**
		 * @brief Constructor
		 */
        TASK_ABSTACT_MODEL_CLASS();

        /**
		 * @brief Destructor
		 */
        virtual ~TASK_ABSTACT_MODEL_CLASS();
    public:

		/**
		 * @brief Get the task start order
		 * @param
		 * @return alias
		 */
		virtual const ZOO_CHAR * get_componet_id();

		/**
		 * @brief Set the task start order
		 * @param order
		 * @return void
		 */
		virtual void set_componet_id(const ZOO_CHAR * componet_id);

		/**
		 * @brief Get the task pid
		 * @param
		 * @return pid
		 */
		virtual const ZOO_INT32 & get_pid();

		/**
		 * @brief Set the task pid
		 * @param pid
		 * @return void
		 */
		virtual void set_pid(ZOO_INT32    pid);

		/**
		 * @brief Get task priority
		 * @param
		 * @return priority
		 */
		virtual ZOO_INT32 get_priority();

		/**
		 * @brief Set task priority
		 * @param priority
		 * @return void
		 */
		virtual void set_priority(ZOO_INT32 priority);

		/**
		 * @brief Get task running status : started or not started
		 * @param
		 * @return ZOO_TASK_STATE_ENUM
		 */
		virtual const ZOO_TASK_STATE_ENUM & get_running_state();

		/**
		 * @brief Set task running status
		 * @param running_status started or not started
		 * @return ZOO_TASK_STATE_ENUM
		 */
		virtual void set_running_state(ZOO_TASK_STATE_ENUM running_state);

		/**
		 * @brief Get task stack configuration size
		 * @param
		 * @return ZOO_INT32
		 */
		virtual ZOO_INT32 get_stack_size() ;

		/**
		 * @brief Set task stack configuration size
		 * @param stack_size    the size of a process stack
		 * @return void
		 */
		virtual void set_stack_size(ZOO_INT32 stack_size);

		/**
		 * @brief Get task trace mode
		 * @param
		 * @return ZOO_TRACE_MODE_ENUM
		 */
		virtual const ZOO_TRACE_MODE_ENUM & get_trace_mode();

		/**
		 * @brief Set the task trace mode
		 * @param trace_mode          TR4A_TRACE_DISABLE:TR4A_TRACE_ENABLE
		 * @return void
		 */
		virtual void set_trace_mode( ZOO_TRACE_MODE_ENUM trace_mode);

		/**
		 * @brief set task watch configuration
		 * @param
		 * @return TM4A_WATCH_CONFIGURATION_STRUCT
		 */
		virtual const ZOO_WATCH_MODE_ENUM & get_watch_mode();

		/**
		 * @brief Set task watch configuration
		 * @param watch_config   watch configuration
		 * @return void
		 */
		virtual void set_watch_mode(const ZOO_WATCH_MODE_ENUM & watch_mode);

		/**
		 * @brief Set task configuration
		 * @param config
		 * @return void
		 */
		virtual void set_configuration(ZOO_TASK_STRUCT & config);

		/**
		 * @brief Get task  configuration
		 * @param
		 * @return void
		 */
		virtual ZOO_TASK_STRUCT get_configuration();

		/**
		 * @brief Update process pid
		 * @param pid
		 * @return void
		 */
		void update_pid(ZOO_INT32 pid);

		
		/**
		 * @brief Update restart times
		 * @return void
		 */
		void increase_restart_times();
		
		/**
		 * @brief Update restart times
		 * @return void
		 */
		void reset_restart_times();
		
		/**
		 * @brief Check current restart times out of range
		 * @return void
		 */
		ZOO_BOOL check_reach_max_restart_times();
    protected:
		
        /**
		 * @brief Configuration
		 */
        ZOO_TASK_STRUCT m_configure;
	
		/**
		 * @brief Task running status
		 */
		ZOO_TASK_STATE_ENUM m_running_state;

		/**
		 * @brief Process pid 
		 */
		ZOO_INT32 m_pid;  

		/**
		 * @brief Restart times 
		 */
		ZOO_INT32 m_restart_times; 
    };
}

#endif // TASK_ABSTACT_MODEL_CLASS_H
