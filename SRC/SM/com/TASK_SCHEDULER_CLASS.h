/**************************************************************
 * Copyright (C) 2015, SHANGHAI MICRO ELECTRONICS EQUIPMENT CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : TM
 * File Name    : TASK_SHULDULER_CLASS.h
 * Description  : 
 * History :
 * Version      Date				User			Comments
 * V1.0.0.0     2018-06-19			WEIWANG			Initialize
 ***************************************************************/
#ifndef TASK_SHULDULER_CLASS_H
#define TASK_SHULDULER_CLASS_H
#include <TASK_SCHEDULER_INTERFACE.h>
#include <list>
#include <thread>
#include <chrono>
#include <mutex>
#include <condition_variable>
namespace ZOO_SM
{
	class TASK_SCHEDULER_CLASS : public virtual TASK_SCHEDULER_INTERFACE
	{
		public:
			TASK_SCHEDULER_CLASS();
			virtual ~TASK_SCHEDULER_CLASS();
		public:
			/**
			 * @brief Add observer will be notified when property changed
			 * @param observer  Property changed observer
			 */
			void start();

			/**
			 * @brief Add observer will be notified when property changed
			 * @param observer  Property changed observer
			 */
			void stop();	
			
			/**
			 * @brief Add an process to watch 
			 * @return
			 */
			void add_observer(boost::shared_ptr<TASK_INTERFACE> observer);

			/**
			 * @brief Remove an process to watch 
			 * @return
			 */
			void remove_observer(boost::shared_ptr<TASK_INTERFACE> observer);
			
			/**
			 * @brief Remove observer from subscribe list
			 */
			void clean();

			/**
			 * @brief Notify property has been changed
			 * @param property_name     The property has been changed
			 */
			void notify_of_property_changed(const ZOO_INT32 property_name);
		private:
			/**
	         * @brief Is stopped.
	         * @return stop dispatcher
	         */
			ZOO_BOOL is_stop();
		private:

			/**
			 * @brief All observer 
			 */
			std::list<boost::shared_ptr<TASK_INTERFACE> > m_notify_observer;

			/**
         	* @brief This flag is used to check wether dispatched thread is stopped.
         	*/
			ZOO_BOOL m_stop;

			/**
         	* @brief This flag is used to inform observers changes in list.
         	*/
			ZOO_BOOL m_observer_changed;

			/**
         	* @brief A condition for evenloop release access observer list.
         	*/
			std::condition_variable m_read_observer_list_cv;

			/**
         	* @brief A condition for observers list changes.
         	*/
			std::condition_variable m_write_observer_list_cv;

			/**
         	* @brief a sync lock for thread.
         	*/
			std::mutex m_sync_lock;
	};
}
#endif
