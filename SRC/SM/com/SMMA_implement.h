/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : SMMA_implement.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/
#ifndef SMMA_IMPLEMENT_H
#define SMMA_IMPLEMENT_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include "SMMA_event.h"
#include "SM4I_type.h"
#include "SM4A_type.h"


/**
 *@brief SMMA_implement_4A_initialize
**/
ZOO_EXPORT void SMMA_implement_4A_initialize(IN SM4I_REPLY_HANDLE reply);
/**
 *@brief SMMA_implement_4A_start_all_tasks
**/
ZOO_EXPORT void SMMA_implement_4A_start_all_tasks(IN SM4I_REPLY_HANDLE reply);
/**
 *@brief SMMA_implement_4A_stop_all_tasks
**/
ZOO_EXPORT void SMMA_implement_4A_stop_all_tasks(IN SM4I_REPLY_HANDLE reply);
/**
 *@brief SMMA_implement_4A_start_single_task
 *@param name[SM_TASK_NAME_LENGHT]
 *@param stack_size
 *@param pid
**/
ZOO_EXPORT void SMMA_implement_4A_start_single_task(IN ZOO_CHAR name[SM_TASK_NAME_LENGHT],
                                                        IN ZOO_INT32 stack_size,
                                                        IN SM4I_REPLY_HANDLE reply);
/**
 *@brief SMMA_implement_4A_stop_single_task
 *@param name[SM_TASK_NAME_LENGHT]
**/
ZOO_EXPORT void SMMA_implement_4A_stop_single_task(IN char name[SM_TASK_NAME_LENGHT],
                                                       IN SM4I_REPLY_HANDLE reply);
/**
 *@brief SMMA_implement_4A_get_task_state
 *@param name[SM_TASK_NAME_LENGHT]
 *@param state
**/
ZOO_EXPORT void SMMA_implement_4A_get_task_state(IN char name[SM_TASK_NAME_LENGHT],
                                                     IN SM4I_REPLY_HANDLE reply);
/**
 *@brief SMMA_implement_4A_get_host_info
 *@param host_info
**/
ZOO_EXPORT void SMMA_implement_4A_get_host_info(IN SM4I_REPLY_HANDLE reply);

#endif // SMMA_implement.h
