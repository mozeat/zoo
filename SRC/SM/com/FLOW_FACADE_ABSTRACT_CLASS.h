/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : FLOW_FACADE_ABSTRACT_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#ifndef FLOW_FACADE_ABSTRACT_CLASS_H
#define FLOW_FACADE_ABSTRACT_CLASS_H

#include "FLOW_FACADE_INTERFACE.h"
#include "TASK_CONTROLLER_INTERFACE.h"

namespace ZOO_SM
{
    class FLOW_FACADE_ABSTRACT_CLASS : public FLOW_FACADE_INTERFACE
    {
    public:
        /**
         * @brief Constructor
         */
        FLOW_FACADE_ABSTRACT_CLASS();

        /**
         * @brief Destructor
         */
        virtual ~FLOW_FACADE_ABSTRACT_CLASS();

    public:

        /**
         * @brief Get reference to task controller
         * @return task_controller
         */
        boost::shared_ptr<TASK_CONTROLLER_INTERFACE> get_task_controller();

       /**
         * @brief Set reference to task controller
         * @param device_controller    The pointer which is pointed to task controller
         */
        void set_task_controller(
                boost::shared_ptr<TASK_CONTROLLER_INTERFACE> task_controller);

		/**
         * @brief Set reference to task scheduler 
         * @param device_controller    The pointer which is pointed to task scheduler 
         */
        void set_task_scheduler(
                boost::shared_ptr<TASK_SCHEDULER_INTERFACE> task_scheduler);

		/**
         * @brief Get reference to task scheduler
         * @return task_controller
         */
        boost::shared_ptr<TASK_SCHEDULER_INTERFACE> get_task_scheduler();		
    private:
        /**
         * @brief The task controller
         */
        boost::shared_ptr<TASK_CONTROLLER_INTERFACE> m_task_controller;

		/**
         * @brief The task controller
         */
        boost::shared_ptr<TASK_SCHEDULER_INTERFACE> m_task_scheduler;
    };
}

#endif // FLOW_FACADE_ABSTRACT_CLASS_H
