/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : SQLITERBITER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#ifndef SQLITERBITER_CLASS_HPP
#define SQLITERBITER_CLASS_HPP
#include <string>
#include <system_error>
#include <boost/process.hpp>
#include "SM_macro_define.h"
#include "LOG_MANAGER_CLASS.h"
namespace ZOO_SM
{
    class SQLITERBITER_CLASS
    {
   	public:
   		/**
         * @brief Constructor
         */
   		SQLITERBITER_CLASS(IN std::string xlxs,
   									IN std::string sql_file) 
		{
		    LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"::xlxs is %s  ...",xlxs.data());
			namespace bp = boost::process;
			const std::string exe = "sqlitebiter";
			std::error_code ec;
	        bp::child 
	        	sqlitebiter(bp::search_path(exe),bp::args={"-o",sql_file,"file",xlxs},ec);
		    LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"::execute sqlitebiter -o %s file %s",sql_file.data(),xlxs.data());
	        sqlitebiter.wait();
	        if(ec)
	        {
	            LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"::fail is %s  ...",ec.message().data());
	        	__THROW_SM_EXCEPTION(SM4A_PARAMETER_ERR, ec.message().data(), NULL);
	        }
	        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"::sql_file is %s  ...",sql_file.data());
		}
   		
   		/**
         * @brief Destructor
         */
   		~SQLITERBITER_CLASS(){}
    };
}
#endif
