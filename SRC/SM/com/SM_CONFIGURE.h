/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : SM_CONFIGURE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#ifndef SM_CONFIGURE_H
#define SM_CONFIGURE_H

extern "C"
{
    #include "SM4A_type.h"
    #include "SM4I_type.h"
    #include "ZOO_if.h"
    #include <stdlib.h>
}
#include <utils/ENVIRONMENT_UTILITY_CLASS.h>
#include <boost/thread/once.hpp>
#include <boost/smart_ptr.hpp>
#include <string>
#include <ZOO_COMMON_MACRO_DEFINE.h>
#include "SQLITERBITER_CLASS.hpp"
#include "SM_macro_define.h"
namespace ZOO_SM
{
    class SM_CONFIGURE
    {
    public:
        /**
         * @brief destructor
         */
        virtual ~SM_CONFIGURE();

        /**
         * @brief get unique instance
         * @return instance object
         */
        static  boost::shared_ptr<SM_CONFIGURE>  get_instance();

        /**
         * @brief Initialize
         */
        static void initialize();

        /**
         * @brief Get execute path
         * @return
         */
        std::string get_execute_path();

		/**
		 * @brief Get execute path
		 * @return
		 */
		std::string get_log_path();

        /**
         * @brief Reload configure
         * @return
         */
        void reload();

        /**
         * @brief Get task configure database path
         * @return
         */
        std::string get_task_configure_path();

		/**
         * @brief Get tasks 
         * @return
         */
        std::vector<ZOO_TASK_STRUCT*> get_tasks();

		/**
         * @brief Get tasks 
         * @return
         */
        std::string get_config_base_path();

        /**
         * @brief Get db base 
         * @return
         */
        std::string get_db_base_path();
    private:
        /**
         * @brief constructor
         */
        SM_CONFIGURE();
        
        void load_usr_library_path(std::string lib_path);
    private:

        /**
         * @brief get unique instance
         * @return instance object
         */
        static  boost::shared_ptr<SM_CONFIGURE>  m_instance;

        /**
         * @brief The execute path
         */
        std::string m_execute_path;

         /**
         * @brief The execute path
         */
        std::string m_lib_path_env;

        /**
         * @brief The config tasks structure
         */
		ZOO_TASKS_STRUCT  m_cfg_tasks;

        /**
         * @brief The output files path
         */
        ZOO_USER_PATH_STRUCT m_user_path;
    };
}
#endif // SM_CONFIGURE_H
