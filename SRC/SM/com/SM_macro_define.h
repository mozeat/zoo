/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : TM_macro_define
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#ifndef TM_MACRO_DEFINE_H_INCLUDED
#define TM_MACRO_DEFINE_H_INCLUDED

#include <ZOO_COMMON_MACRO_DEFINE.h>
#include "ERROR_CODE_CONVERTER_CLASS.h"
#include "SM_EXCEPTION_CLASS.h"
#include "SM4A_type.h"
#include "SM4I_type.h"

/**
 * @brief Define a macro to handle try-catch exception caught in controller, facade layer
 * @param error_code                    The error code
 * @param continue_throw_exception      True: continue throw exception, False: otherwise
 */
#define __SM_TRY try
#define __SM_CATCH_ALL(error_code, continue_throw_exception)                                       \
  catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS& ex) {                                               \
      if(continue_throw_exception)                                                                 \
      {                                                                                            \
         throw ZOO_SM::SM_EXCEPTION_CLASS(error_code,ex.get_error_message(),NULL);                        \
      }                                                                                            \
      else                                                                                         \
      {                                                                                            \
          error_code = ZOO_SM::ERROR_CODE_CONVERTER_CLASS::get_instance()->convert_error_code_exception( \
                        ex.get_error_code());                                                         \
      }                                                                                       \
  }                                                                                                \
  catch(std::exception& ex)                                                                             \
  {                                                                                                \
      ZOO_CHAR error_msg[ZOO_COMMON::PARAMETER_EXCEPTION_LENGTH];                                             \
      sprintf(error_msg, "Unhandle exception, please collect the log file and report this issue to administrator: %s", ex.what()); \
      error_code = SM4A_SYSTEM_ERR;                                                              \
  }

/**
 * @brief Define a macro to throw TM exception
 * @param error_code        The error code
 * @param error_message     The error message
 * @param inner_exception   The inner exception
 */
#define __THROW_SM_EXCEPTION(error_code, error_message, inner_exception) \
    throw ZOO_SM::SM_EXCEPTION_CLASS(error_code, error_message, inner_exception); \
    ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR ,__FUNCTION__,error_message);

#endif // SM_MACRO_DEFINE_H_INCLUDED
