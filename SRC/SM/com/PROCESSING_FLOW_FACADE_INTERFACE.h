/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : PROCESSING_FLOW_FACADE_INTERFACE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#ifndef PROCESSING_FLOW_FACADE_INTERFACE_H
#define PROCESSING_FLOW_FACADE_INTERFACE_H
#include "FLOW_FACADE_ABSTRACT_CLASS.h"
#include <string>
#include <thread>
#include <chrono>
namespace ZOO_SM
{
    class PROCESSING_FLOW_FACADE_INTERFACE : public virtual FLOW_FACADE_ABSTRACT_CLASS
    {
    public:
        /**
         * @brief Destructor
         */
        virtual ~PROCESSING_FLOW_FACADE_INTERFACE() {}

    public:

		/**
         * @brief Load platform configure file ,generate database file
         * @param
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        virtual ZOO_INT32 initialize() = 0;
        
        /**
         * @brief Start all configuration task
         * @param
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        virtual ZOO_INT32 start_all_tasks() = 0;

        /**
         * @brief Stop all configuration task
         * @param
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        virtual ZOO_INT32 stop_all_tasks() = 0;

        /**
         * @brief Start single task
         * @param name
         * @param stack_size
         * @param pid         the process id
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        virtual ZOO_INT32 start_single_task(IN std::string name,
        										  IN ZOO_INT32 stack_size,
        										  OUT ZOO_UINT32 * pid) = 0;

        /**
         * @brief Stop single task
         * @param
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        virtual ZOO_INT32 stop_single_task(IN std::string name) = 0;

        /**
         * @brief Get task running state
         * @param name
         * @param state
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        virtual ZOO_INT32 get_task_state(IN std::string name,
        										OUT ZOO_TASK_STATE_ENUM * state) = 0;

        /**
         * @brief Get host info
         * @param name
         * @param state
         * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
         */
        virtual ZOO_INT32 get_host_info(IN SM4A_HOST_INFO_STRUCT * host_info) = 0;	

        /**
         * @brief Startup local brokers before process communication
         * @return void
         */
    	virtual void startup_local_broker() = 0;


    	/**
         * @brief Shutdown local brokers 
         * @return void
         */
    	virtual void shutdown_local_broker() = 0;
    };
}

#endif // ROCESSING_FLOW_FACADE_INTERFACE_H
