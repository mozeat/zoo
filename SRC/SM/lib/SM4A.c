/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : SM4A.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/

#include <ZOO.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include "SM4I_type.h"
#include "SM4A_type.h"
#include "SM4I_if.h"
/**
 *@brief SM4A_initialize
**/
ZOO_INT32 SM4A_initialize()
{
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "> function entry ... ");
    ZOO_INT32 result = OK;
    SM4I_REQUEST_STRUCT *request_message = NULL; 
    SM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 func_code = SM4A_INITIALIZE_CODE;
    result = SM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (SM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = SM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (SM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
    }

    if(OK == result)
    {
        result = SM4I_send_request_and_reply(SM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
        MM4A_free(request_message);
    if(reply_message != NULL)
        MM4A_free(reply_message);
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "< function exit ..." );
    return result;
}

/**
 *@brief SM4A_start_all_tasks
**/
ZOO_INT32 SM4A_start_all_tasks()
{
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "> function entry ... ");
    ZOO_INT32 result = OK;
    SM4I_REQUEST_STRUCT *request_message = NULL; 
    SM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 func_code = SM4A_START_ALL_TASKS_CODE;
    result = SM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (SM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = SM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (SM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
    }

    if(OK == result)
    {
        result = SM4I_send_request_and_reply(SM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
        MM4A_free(request_message);
    if(reply_message != NULL)
        MM4A_free(reply_message);
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "< function exit ..." );
    return result;
}

/**
 *@brief SM4A_stop_all_tasks
**/
ZOO_INT32 SM4A_stop_all_tasks()
{
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "> function entry ... ");
    ZOO_INT32 result = OK;
    SM4I_REQUEST_STRUCT *request_message = NULL; 
    SM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 func_code = SM4A_STOP_ALL_TASKS_CODE;
    result = SM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (SM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = SM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (SM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
    }

    if(OK == result)
    {
        result = SM4I_send_request_and_reply(SM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
        MM4A_free(request_message);
    if(reply_message != NULL)
        MM4A_free(reply_message);
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "< function exit ..." );
    return result;
}

/**
 *@brief SM4A_start_single_task
 *@param name[SM_TASK_NAME_LENGHT]
 *@param stack_size
 *@param pid
**/
ZOO_INT32 SM4A_start_single_task(IN ZOO_CHAR name[SM_TASK_NAME_LENGHT],IN ZOO_INT32 stack_size,OUT ZOO_UINT32 * pid)
{
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "> function entry ... ");
    ZOO_INT32 result = OK;
    SM4I_REQUEST_STRUCT *request_message = NULL; 
    SM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 func_code = SM4A_START_SINGLE_TASK_CODE;
    result = SM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (SM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = SM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (SM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.start_single_task_req_msg.name[0]),&name[0],sizeof(ZOO_CHAR) * SM_TASK_NAME_LENGHT);
        memcpy((void *)(&request_message->request_body.start_single_task_req_msg.stack_size),&stack_size,sizeof(ZOO_INT32));
    }

    if(OK == result)
    {
        result = SM4I_send_request_and_reply(SM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(pid,&reply_message->reply_body.start_single_task_rep_msg.pid,sizeof(ZOO_UINT32));
    }

    if(request_message != NULL)
        MM4A_free(request_message);
    if(reply_message != NULL)
        MM4A_free(reply_message);
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "< function exit ..." );
    return result;
}

/**
 *@brief SM4A_stop_single_task
 *@param name[SM_TASK_NAME_LENGHT]
**/
ZOO_INT32 SM4A_stop_single_task(IN char name[SM_TASK_NAME_LENGHT])
{
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "> function entry ... ");
    ZOO_INT32 result = OK;
    SM4I_REQUEST_STRUCT *request_message = NULL; 
    SM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 func_code = SM4A_STOP_SINGLE_TASK_CODE;
    result = SM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (SM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = SM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (SM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.stop_single_task_req_msg.name[0]),&name[0],sizeof(char) * SM_TASK_NAME_LENGHT);
    }

    if(OK == result)
    {
        result = SM4I_send_request_and_reply(SM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
        MM4A_free(request_message);
    if(reply_message != NULL)
        MM4A_free(reply_message);
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "< function exit ..." );
    return result;
}

/**
 *@brief SM4A_get_task_state
 *@param name[SM_TASK_NAME_LENGHT]
 *@param state
**/
ZOO_INT32 SM4A_get_task_state(IN char name[SM_TASK_NAME_LENGHT],OUT ZOO_TASK_STATE_ENUM * state)
{
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "> function entry ... ");
    ZOO_INT32 result = OK;
    SM4I_REQUEST_STRUCT *request_message = NULL; 
    SM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 func_code = SM4A_GET_TASK_STATE_CODE;
    result = SM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (SM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = SM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (SM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.get_task_state_req_msg.name[0]),&name[0],sizeof(char) * SM_TASK_NAME_LENGHT);
    }

    if(OK == result)
    {
        result = SM4I_send_request_and_reply(SM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(state,&reply_message->reply_body.get_task_state_rep_msg.state,sizeof(ZOO_TASK_STATE_ENUM));
    }

    if(request_message != NULL)
        MM4A_free(request_message);
    if(reply_message != NULL)
        MM4A_free(reply_message);
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "< function exit ..." );
    return result;
}

/**
 *@brief SM4A_get_host_info
 *@param host_info
**/
ZOO_INT32 SM4A_get_host_info(INOUT SM4A_HOST_INFO_STRUCT * host_info)
{
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "> function entry ... ");
    ZOO_INT32 result = OK;
    SM4I_REQUEST_STRUCT *request_message = NULL; 
    SM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 func_code = SM4A_GET_HOST_INFO_CODE;
    result = SM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (SM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = SM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (SM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = SM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
    }

    if(OK == result)
    {
        result = SM4I_send_request_and_reply(SM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(host_info,&reply_message->reply_body.get_host_info_rep_msg.host_info,sizeof(SM4A_HOST_INFO_STRUCT));
    }

    if(request_message != NULL)
        MM4A_free(request_message);
    if(reply_message != NULL)
        MM4A_free(reply_message);
    //TR4A_trace(SM4I_COMPONET_ID, __ZOO_FUNC__, "< function exit ..." );
    return result;
}

