 /***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : TASK_INTERFACE.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#include "TASK_INTERFACE.h"
namespace ZOO_SM
{
    /**
     * @brief Constructor
     */
    TASK_INTERFACE::TASK_INTERFACE()
    {
        //ctor
    }

    /**
     * @brief Destructor
     */
    TASK_INTERFACE::~TASK_INTERFACE()
    {
        //dtor
    }
}
