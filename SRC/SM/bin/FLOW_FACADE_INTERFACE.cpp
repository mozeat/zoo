/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : FLOW_FACADE_INTERFACE.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#include "FLOW_FACADE_INTERFACE.h"

namespace ZOO_SM
{
    FLOW_FACADE_INTERFACE::FLOW_FACADE_INTERFACE()
    {
        //ctor
    }

    FLOW_FACADE_INTERFACE::~FLOW_FACADE_INTERFACE()
    {
        //dtor
    }
}

