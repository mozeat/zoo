/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : SMMA_executor_wrapper.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/
#include <SMMA_executor_wrapper.h>
#include "SM_CONFIGURE.h"
#include "TASK_CONTROLLER_CLASS.h"
#include "PROCESSING_FLOW_FACADE_CALSS.h"
#include "TASK_SCHEDULER_CLASS.h"
#include "SM_macro_define.h"
#include "DATABASE_MANAGER_CLASS.h"
#include "LOG_MANAGER_CLASS.h"
#include <boost/stacktrace.hpp>
#include <signal.h>
#include <sstream>
#include <utils/THREAD_POOL.h>

/**
 *@brief The task controller instance
**/
boost::shared_ptr<ZOO_SM::TASK_CONTROLLER_INTERFACE> g_task_controller;

/**
 *@brief The task scheduler instance
**/
boost::shared_ptr<ZOO_SM::TASK_SCHEDULER_INTERFACE> g_task_scheduler;


/**
 *@brief The processing flow facade instance
**/
boost::shared_ptr<ZOO_SM::PROCESSING_FLOW_FACADE_INTERFACE> g_processing_flow_facade;

static void SM_default_signal_handler(IN int signal_id)
{
    ZOO_SM::LOG_MANAGER_CLASS::log(__ZOO_FUNC__," > function entry ...");
    ZOO_SM::LOG_MANAGER_CLASS::log(__ZOO_FUNC__," receive signal[%d] ...",signal_id);
    signal(signal_id, SIG_DFL);
    ZOO_SM::LOG_MANAGER_CLASS::log(__ZOO_FUNC__," recover signal[%d] to default handler ...",signal_id);
    std::stringstream os;
    os << boost::stacktrace::stacktrace();
    ZOO_SM::LOG_MANAGER_CLASS::log(__ZOO_FUNC__," stack trace: %s",os.str().c_str());

    std::string dump_core_name = "./TMMA.dump";
    if (boost::filesystem::exists(dump_core_name.data()))
    {
        boost::filesystem::remove(dump_core_name.data());
    }
    boost::stacktrace::safe_dump_to(dump_core_name.data());
    ZOO_SM::LOG_MANAGER_CLASS::log(__ZOO_FUNC__," < function exit ...");
    return;
}

/**  
 * @brief Register system signal handler,throw PARAMETER_EXCEPTION_CLASS if register fail,
 * the default signal handling is save stack trace to the log file and generate a dump file at execute path.
 * register a self-defined callback to SYSTEM_SIGNAL_HANDLER::resgister_siganl will change the default behavior.
**/
static void SMMA_register_system_signals()
{
    signal(SIGSEGV,SM_default_signal_handler);
    /* Add more signals if needs,or register self-defined callback function
       to change the default behavior... */
 }

/**
 *@brief Execute the start up flow.
 * This function is executed in 3 steps: 
 * Step 1: Load configurations 
 * Step 2: Create controllers
 * Step 3: Create facades and set controllers to created facades
**/ 
void SMMA_startup(void)
{
    /** User add */
    ZOO_INT32 error_code = OK;
    __SM_TRY
    {
    	/**
		 *@brief register signal 
		**/ 
        SMMA_register_system_signals();
        ZOO_SM::SM_CONFIGURE::initialize();
        ZOO_SM::LOG_MANAGER_CLASS::initialize();
		ZOO_SM::DATABASE_MANAGER_CLASS::get_instance()->initialize();
		
        /**
		 *@brief Create controllers
		**/ 
		g_task_scheduler.reset(new ZOO_SM::TASK_SCHEDULER_CLASS());
        g_task_controller.reset(dynamic_cast<ZOO_SM::TASK_CONTROLLER_INTERFACE*>
									(new ZOO_SM::TASK_CONTROLLER_CLASS()));
        g_processing_flow_facade.reset(dynamic_cast<ZOO_SM::PROCESSING_FLOW_FACADE_INTERFACE*>
											(new ZOO_SM::PROCESSING_FLOW_FACADE_CALSS()));
        g_processing_flow_facade->set_task_controller(g_task_controller);
		g_processing_flow_facade->set_task_scheduler(g_task_scheduler);

		/**
		 *@brief Startup threadpool for monitor tasks
		**/ 
		ZOO_COMMON::THREAD_POOL::get_instance()->startup();
		ZOO_COMMON::THREAD_POOL::get_instance()->queue_working(boost::bind(&ZOO_SM::TASK_SCHEDULER_CLASS::start,
			boost::dynamic_pointer_cast<ZOO_SM::TASK_SCHEDULER_CLASS>(g_task_scheduler)));

        g_task_controller->initialize();
        
		/**
		 *@brief Startup btrokers
		**/	
        //g_processing_flow_facade->shutdown_local_broker();

        /**
		 *@brief Startup btrokers
		**/	
        //g_processing_flow_facade->startup_local_broker();			
    }
    __SM_CATCH_ALL(error_code,ZOO_FALSE)
}

/**
 *@brief This function response to release instance or memory 
**/ 
void SMMA_shutdown(void)
{
    /** User add */
    ZOO_INT32 error_code = OK;
    __SM_TRY
    {
        g_processing_flow_facade->stop_all_tasks();
        g_processing_flow_facade->shutdown_local_broker();
    }
    __SM_CATCH_ALL(error_code,ZOO_FALSE)
    g_task_controller.reset();
    g_processing_flow_facade.reset();
	ZOO_COMMON::THREAD_POOL::get_instance()->shutdown();
}

/**
 *@brief Subscribe events published from hardware drivers 
**/
void SMMA_subscribe_driver_event(void)
{

    /** Subscribe events */
}

