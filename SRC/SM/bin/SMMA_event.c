/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : SMMA_event.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/
#include <SMMA_event.h>

/**
 *@brief SMMA_raise_4A_initialize
**/
ZOO_INT32 SMMA_raise_4A_initialize(IN ZOO_INT32 error_code,
                                       IN SM4I_REPLY_HANDLE reply)
{
    SM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply == NULL)
    {
        rtn = SM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply->reply_wanted == ZOO_TRUE)
        {
            rtn = SM4I_get_reply_message_length(SM4A_INITIALIZE_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (SM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = SM4A_INITIALIZE_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = SM4I_send_reply_message(reply->reply_addr,reply->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply != NULL)
    {
        MM4A_free(reply);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return rtn;
}

/**
 *@brief SMMA_raise_4A_start_all_tasks
**/
ZOO_INT32 SMMA_raise_4A_start_all_tasks(IN ZOO_INT32 error_code,
                                            IN SM4I_REPLY_HANDLE reply)
{
    SM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply == NULL)
    {
        rtn = SM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply->reply_wanted == ZOO_TRUE)
        {
            rtn = SM4I_get_reply_message_length(SM4A_START_ALL_TASKS_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (SM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = SM4A_START_ALL_TASKS_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = SM4I_send_reply_message(reply->reply_addr,reply->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply != NULL)
    {
        MM4A_free(reply);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return rtn;
}

/**
 *@brief SMMA_raise_4A_stop_all_tasks
**/
ZOO_INT32 SMMA_raise_4A_stop_all_tasks(IN ZOO_INT32 error_code,
                                           IN SM4I_REPLY_HANDLE reply)
{
    SM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply == NULL)
    {
        rtn = SM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply->reply_wanted == ZOO_TRUE)
        {
            rtn = SM4I_get_reply_message_length(SM4A_STOP_ALL_TASKS_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (SM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = SM4A_STOP_ALL_TASKS_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = SM4I_send_reply_message(reply->reply_addr,reply->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply != NULL)
    {
        MM4A_free(reply);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return rtn;
}

/**
 *@brief SMMA_raise_4A_start_single_task
 *@param name[SM_TASK_NAME_LENGHT]
 *@param stack_size
 *@param pid
**/
ZOO_INT32 SMMA_raise_4A_start_single_task(IN ZOO_INT32 error_code,
                                              IN ZOO_UINT32 pid,
                                              IN SM4I_REPLY_HANDLE reply)
{
    SM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply == NULL)
    {
        rtn = SM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply->reply_wanted == ZOO_TRUE)
        {
            rtn = SM4I_get_reply_message_length(SM4A_START_SINGLE_TASK_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (SM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = SM4A_START_SINGLE_TASK_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.start_single_task_rep_msg.pid,&pid,sizeof(ZOO_UINT32));
                rtn = SM4I_send_reply_message(reply->reply_addr,reply->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply != NULL)
    {
        MM4A_free(reply);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return rtn;
}

/**
 *@brief SMMA_raise_4A_stop_single_task
 *@param name[SM_TASK_NAME_LENGHT]
**/
ZOO_INT32 SMMA_raise_4A_stop_single_task(IN ZOO_INT32 error_code,
                                             IN SM4I_REPLY_HANDLE reply)
{
    SM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply == NULL)
    {
        rtn = SM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply->reply_wanted == ZOO_TRUE)
        {
            rtn = SM4I_get_reply_message_length(SM4A_STOP_SINGLE_TASK_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (SM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = SM4A_STOP_SINGLE_TASK_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = SM4I_send_reply_message(reply->reply_addr,reply->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply != NULL)
    {
        MM4A_free(reply);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return rtn;
}

/**
 *@brief SMMA_raise_4A_get_task_state
 *@param name[SM_TASK_NAME_LENGHT]
 *@param state
**/
ZOO_INT32 SMMA_raise_4A_get_task_state(IN ZOO_INT32 error_code,
                                           IN ZOO_TASK_STATE_ENUM state,
                                           IN SM4I_REPLY_HANDLE reply)
{
    SM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply == NULL)
    {
        rtn = SM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply->reply_wanted == ZOO_TRUE)
        {
            rtn = SM4I_get_reply_message_length(SM4A_GET_TASK_STATE_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (SM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = SM4A_GET_TASK_STATE_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.get_task_state_rep_msg.state,&state,sizeof(ZOO_TASK_STATE_ENUM));
                rtn = SM4I_send_reply_message(reply->reply_addr,reply->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply != NULL)
    {
        MM4A_free(reply);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return rtn;
}

/**
 *@brief SMMA_raise_4A_get_host_info
 *@param host_info
**/
ZOO_INT32 SMMA_raise_4A_get_host_info(IN ZOO_INT32 error_code,
                                          IN SM4A_HOST_INFO_STRUCT host_info,
                                          IN SM4I_REPLY_HANDLE reply)
{
    SM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply == NULL)
    {
        rtn = SM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply->reply_wanted == ZOO_TRUE)
        {
            rtn = SM4I_get_reply_message_length(SM4A_GET_HOST_INFO_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (SM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = SM4A_GET_HOST_INFO_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.get_host_info_rep_msg.host_info,&host_info,sizeof(SM4A_HOST_INFO_STRUCT));
                rtn = SM4I_send_reply_message(reply->reply_addr,reply->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = SM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply != NULL)
    {
        MM4A_free(reply);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return rtn;
}


