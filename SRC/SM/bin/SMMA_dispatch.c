/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : SMMA_dispatch.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/

#include <SMMA_dispatch.h>
#include <SMMA_implement.h>
#include "ZOO_if.h"
/**
 *@brief SMMA_local_4A_initialize
**/
static ZOO_INT32 SMMA_local_4A_initialize(const MQ4A_SERV_ADDR server,SM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    SM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = SM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (SM4I_REPLY_HANDLE)MM4A_malloc(sizeof(SM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = SM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        SMMA_implement_4A_initialize(reply_handle);
    }
    return rtn;
}

/**
 *@brief SMMA_local_4A_start_all_tasks
**/
static ZOO_INT32 SMMA_local_4A_start_all_tasks(const MQ4A_SERV_ADDR server,SM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    SM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = SM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (SM4I_REPLY_HANDLE)MM4A_malloc(sizeof(SM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = SM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        SMMA_implement_4A_start_all_tasks(reply_handle);
    }
    return rtn;
}

/**
 *@brief SMMA_local_4A_stop_all_tasks
**/
static ZOO_INT32 SMMA_local_4A_stop_all_tasks(const MQ4A_SERV_ADDR server,SM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    SM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = SM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (SM4I_REPLY_HANDLE)MM4A_malloc(sizeof(SM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = SM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        SMMA_implement_4A_stop_all_tasks(reply_handle);
    }
    return rtn;
}

/**
 *@brief SMMA_local_4A_start_single_task
 *@param name[SM_TASK_NAME_LENGHT]
 *@param stack_size
 *@param pid
**/
static ZOO_INT32 SMMA_local_4A_start_single_task(const MQ4A_SERV_ADDR server,SM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    SM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = SM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (SM4I_REPLY_HANDLE)MM4A_malloc(sizeof(SM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = SM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        SMMA_implement_4A_start_single_task(request->request_body.start_single_task_req_msg.name,
                                           request->request_body.start_single_task_req_msg.stack_size,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief SMMA_local_4A_stop_single_task
 *@param name[SM_TASK_NAME_LENGHT]
**/
static ZOO_INT32 SMMA_local_4A_stop_single_task(const MQ4A_SERV_ADDR server,SM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    SM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = SM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (SM4I_REPLY_HANDLE)MM4A_malloc(sizeof(SM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = SM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        SMMA_implement_4A_stop_single_task(request->request_body.stop_single_task_req_msg.name,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief SMMA_local_4A_get_task_state
 *@param name[SM_TASK_NAME_LENGHT]
 *@param state
**/
static ZOO_INT32 SMMA_local_4A_get_task_state(const MQ4A_SERV_ADDR server,SM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    SM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = SM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (SM4I_REPLY_HANDLE)MM4A_malloc(sizeof(SM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = SM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        SMMA_implement_4A_get_task_state(request->request_body.get_task_state_req_msg.name,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief SMMA_local_4A_get_host_info
 *@param host_info
**/
static ZOO_INT32 SMMA_local_4A_get_host_info(const MQ4A_SERV_ADDR server,SM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    SM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = SM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (SM4I_REPLY_HANDLE)MM4A_malloc(sizeof(SM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = SM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        SMMA_implement_4A_get_host_info(reply_handle);
    }
    return rtn;
}

/**
*@brief Dispatch message from client to server internal interface
*@param context        
*@param server        address
*@param msg           request message to server
*@param len           request message length
*@param reply_msg     reply message length to caller
*@param reply_msg_len reply message length
**/
void SMMA_callback_handler(void * context,const MQ4A_SERV_ADDR server,void * msg,ZOO_UINT32 msg_id)
{
	//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION__,"server :%s,recv len:%d",server,len);
    ZOO_INT32 rtn = OK;
    ZOO_INT32 rep_length = 0;
    SM4I_REQUEST_STRUCT *request = (SM4I_REQUEST_STRUCT*)msg;
    SM4I_REPLY_STRUCT *reply = NULL;
    if(request == NULL)
    {
        rtn = SM4A_SYSTEM_ERR;
        return;
    }

    if(OK == rtn)
    {
        switch(request->request_header.function_code)
        {
            case SM4A_INITIALIZE_CODE:
            	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION__,"handle function id:%d",SM4A_INITIALIZE_CODE);
                SMMA_local_4A_initialize(server,request,msg_id);
                break;
            case SM4A_START_ALL_TASKS_CODE:
                SMMA_local_4A_start_all_tasks(server,request,msg_id);
                break;
            case SM4A_STOP_ALL_TASKS_CODE:
                SMMA_local_4A_stop_all_tasks(server,request,msg_id);
                break;
            case SM4A_START_SINGLE_TASK_CODE:
                SMMA_local_4A_start_single_task(server,request,msg_id);
                break;
            case SM4A_STOP_SINGLE_TASK_CODE:
                SMMA_local_4A_stop_single_task(server,request,msg_id);
                break;
            case SM4A_GET_TASK_STATE_CODE:
                SMMA_local_4A_get_task_state(server,request,msg_id);
                break;
            case SM4A_GET_HOST_INFO_CODE:
                SMMA_local_4A_get_host_info(server,request,msg_id);
                break;
            default:
                rtn = SM4A_SYSTEM_ERR;
                break;
        }
    }

    if(OK != rtn && request->request_header.need_reply)
    {
        rtn = SM4I_get_reply_message_length(request->request_header.function_code, &rep_length);
        if(OK != rtn)
        {
            rtn = SM4A_SYSTEM_ERR;
        }

        if(OK == rtn)
        {
            reply = (SM4I_REPLY_STRUCT *)MM4A_malloc(rep_length);
            if(reply == NULL)
            {
                rtn = SM4A_SYSTEM_ERR;
            }
        }

        if(OK == rtn)
        {
            memset(reply, 0x0, rep_length);
            reply->reply_header.execute_result = rtn;
            reply->reply_header.function_code = request->request_header.function_code;
            SM4I_send_reply_message(server,msg_id,reply);
        }
    }
    return;
}

