/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : SMMA_implement.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/
#include "SMMA_implement.h"
#include "SM_PROCESSING_FLOW_FACADE_WRAPPER.h"
/**
 *@brief SMMA_implement_4A_initialize
**/
void SMMA_implement_4A_initialize(IN SM4I_REPLY_HANDLE reply)
{
    ZOO_INT32 rtn = OK;
    /* User add ... BEGIN */
	rtn = SM_initialize();
    /* User add ... END*/

    SMMA_raise_4A_initialize(rtn,reply);
}

/**
 *@brief SMMA_implement_4A_start_all_tasks
**/
void SMMA_implement_4A_start_all_tasks(IN SM4I_REPLY_HANDLE reply)
{
    ZOO_INT32 rtn = OK;
    /* User add ... BEGIN */
    rtn = SM_start_all_tasks();
    /* User add ... END*/

    SMMA_raise_4A_start_all_tasks(rtn,reply);
}

/**
 *@brief SMMA_implement_4A_stop_all_tasks
**/
void SMMA_implement_4A_stop_all_tasks(IN SM4I_REPLY_HANDLE reply)
{
    ZOO_INT32 rtn = OK;
    /* User add ... BEGIN */

    rtn = SM_stop_all_tasks();
    
    /* User add ... END*/

    SMMA_raise_4A_stop_all_tasks(rtn,reply);
}

/**
 *@brief SMMA_implement_4A_start_single_task
 *@param name[SM_TASK_NAME_LENGHT]
 *@param stack_size
 *@param pid
**/
void SMMA_implement_4A_start_single_task(IN ZOO_CHAR name[SM_TASK_NAME_LENGHT],
                                             IN ZOO_INT32 stack_size,
                                             IN SM4I_REPLY_HANDLE reply)
{
    ZOO_INT32 rtn = OK;
    ZOO_UINT32 pid = 0;
    /* usr add ... BEGIN */
    rtn = SM_start_single_task(name,stack_size,&pid);
    
    /* User add ... END*/

    SMMA_raise_4A_start_single_task(rtn,pid,reply);
}

/**
 *@brief SMMA_implement_4A_stop_single_task
 *@param name[SM_TASK_NAME_LENGHT]
**/
void SMMA_implement_4A_stop_single_task(IN char name[SM_TASK_NAME_LENGHT],
                                            IN SM4I_REPLY_HANDLE reply)
{
    ZOO_INT32 rtn = OK;
    /* User add ... BEGIN */

    rtn = SM_stop_single_task(name);
    
    /* User add ... END*/

    SMMA_raise_4A_stop_single_task(rtn,reply);
}

/**
 *@brief SMMA_implement_4A_get_task_state
 *@param name[SM_TASK_NAME_LENGHT]
 *@param state
**/
void SMMA_implement_4A_get_task_state(IN char name[SM_TASK_NAME_LENGHT],
                                          IN SM4I_REPLY_HANDLE reply)
{
    ZOO_INT32 rtn = OK;
    ZOO_TASK_STATE_ENUM state = ZOO_TASK_STATE_NOT_STARTED;
    /* User add ... BEGIN */
    rtn = SM_get_task_state(name,&state);
    
    /* User add ... END*/

    SMMA_raise_4A_get_task_state(rtn,state,reply);
}

/**
 *@brief SMMA_implement_4A_get_host_info
 *@param host_info
**/
void SMMA_implement_4A_get_host_info(IN SM4I_REPLY_HANDLE reply)
{
    ZOO_INT32 rtn = OK;
    SM4A_HOST_INFO_STRUCT host_info ;
    memset(&host_info,0,sizeof(SM4A_HOST_INFO_STRUCT));
    
    /* User add ... BEGIN */
	rtn = SM_get_host_info(&host_info);
    
    /* User add ... END*/

    SMMA_raise_4A_get_host_info(rtn,host_info,reply);
}


