/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        :
 * Component id   :
 * File Name      : TASK_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018��9��14��       weiwang.sun      created
 *************************************************************/
#include "TASK_MODEL_CLASS.h"

namespace ZOO_SM
{
	const ZOO_CHAR * LINUX_KILL_CMD = "kill ";
	
	TASK_MODEL_CLASS::TASK_MODEL_CLASS()
	{
		// TODO Auto-generated constructor stub
        this->m_stop = ZOO_FALSE;
	}

	TASK_MODEL_CLASS::~TASK_MODEL_CLASS()
	{
		// TODO Auto-generated destructor stub
	}

	/**
     * @brief Start process
     * @return
     */
    void TASK_MODEL_CLASS::start()
	{		
		LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"> function entry ...");
		LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," process:%s",this->get_marking_code());
		pid_t pid = this->get_pid(this->get_marking_code());
		if(pid < 1)
		{
            std::string process = std::string(this->m_configure.bin_path) + "/" + this->m_configure.task_name;
    		boost::shared_ptr<EXECUTE_CLASS> execute(new EXECUTE_CLASS());	
    		execute->set_command(process);
    		boost::shared_ptr<EXECUTE_HALEPER_CLASS> executor(new EXECUTE_HALEPER_CLASS());
    		executor->invoke(execute,PUBLISHER_THREAD);
    		this->m_execute_helper = executor;
		}
		this->m_stop = ZOO_FALSE;
		this->set_running_state(ZOO_TASK_STATE_STARTED);
		LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"< function exit");
	}

    /**
     * @brief Kill process
     * @return
     */
    void TASK_MODEL_CLASS::stop()
	{
		LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"> function entry ...");
		LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," process:%s",this->get_marking_code());
		this->m_stop = ZOO_TRUE;
		pid_t pid = SM_INVALID_PID;
        pid = this->get_pid(this->get_marking_code());
        while(pid >= 1)
        {
            pid = this->get_pid(this->get_marking_code());
            if(pid < 1)
            {
                break;
            }
#if TARGET_MACHINE == ZOO_ARM_32
            std::ostringstream os;
            os << LINUX_KILL_CMD << " -9 " << pid;
			LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"kill:%s",os.str().c_str());			
		    system(os.str().c_str());

		    os.str("");
            os << "pkill " << this->get_marking_code();
			LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,":%s",os.str().c_str());	
			
		    system(os.str().c_str());
#else		    
			int error_code = kill(pid, SIGTERM);							
			std::this_thread::sleep_for(std::chrono::seconds(2));		
			error_code = kill(pid,SIGKILL);

			if(error_code == EINVAL)
			{   
                LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,":: %s","An invalid signal was specified");
			}
			
			if(error_code == EPERM)
			{  
                LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,":: %s","The calling process does not have permission to send the signal to any of the target processes");
			}
			
			if(error_code == ESRCH)
			{  
                LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,":: %s","The target process or process group does not exist. Note that an existing process might be a zombie, a process that has terminated execution, but has not yet been wait(2)ed for");
			}
#endif            
        }        
        this->m_execute_helper.reset();
        this->update_pid(SM_INVALID_PID);
        this->set_running_state(ZOO_TASK_STATE_NOT_STARTED);
		this->reset_restart_times();
		LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"< function exit");
	}

	/**
	* @brief Kill process
	* @return
	*/
	void TASK_MODEL_CLASS::restart()
	{
		if(this->m_stop)
		{
			return;
		}
		auto process_name = this->get_marking_code();
		pid_t pid = this->get_pid(process_name);
        if(pid <= SM_INVALID_PID)
        {     
        	if(!this->check_reach_max_restart_times())
        	{
				this->increase_restart_times();
				this->start();
				pid = this->get_pid(process_name);				
				if(pid != SM_INVALID_PID)
		        { 
					this->set_running_state(ZOO_TASK_STATE_STARTED);
		        }
				else
				{
		        	this->set_running_state(ZOO_TASK_STATE_NOT_STARTED);
				}
				this->update_pid(pid);
        	}
        }
	}
	
	/**
	 * @brief Get process pid
	 * @return
	 */
	ZOO_INT32 TASK_MODEL_CLASS::get_pid(std::string process_name)
	{
		return ENVIRONMENT_UTILITY_CLASS::get_current_user_pid(process_name);
	}
}
