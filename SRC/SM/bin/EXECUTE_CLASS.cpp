#include "EXECUTE_CLASS.h"

namespace ZOO_SM
{
    EXECUTE_CLASS::EXECUTE_CLASS()
    {
        //ctor
    }

    EXECUTE_CLASS::~EXECUTE_CLASS()
    {
        //dtor
    }

    /**
     * @brief Set command
     * @param command    the .exe name
    */
    void EXECUTE_CLASS::set_command(std::string command)
    {
        this->m_command = std::move(command);
    }

    /**
     * @brief Set the command parameters
     * @param parameters
    */
    void EXECUTE_CLASS::set_parameters(const std::vector<std::string> & parameters)
    {
        this->m_parameters = parameters;
    }

    /**
     * @brief Get command parameters
     * @return parameters
    */
    const std::vector<std::string> & EXECUTE_CLASS::get_parameters()
    {
        return this->m_parameters;
    }

    /**
     * @brief Get command
     * @return command name
    */
    const std::string & EXECUTE_CLASS::get_command()
    {
        return this->m_command;
    }
}

