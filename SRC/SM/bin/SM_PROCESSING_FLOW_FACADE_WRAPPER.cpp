/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : TM_PROCESSING_FLOW_FACADE_WRAPPER.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         20181001      mozeat.sun      created
 *************************************************************/
#include "SM_PROCESSING_FLOW_FACADE_WRAPPER.h"
#include "PROCESSING_FLOW_FACADE_INTERFACE.h"

extern ZOO_SM::PROCESSING_FLOW_FACADE_INTERFACE * g_processing_flow_facade;

/**
 * @brief Execute the business logic to start all tasks
 * @return Error code: OK
 *                     TM4A_SYSTEM_ERROR
 *                     TM4A_ILLEGAL_CALL_ERROR
 *                     TM4A_TIMEOUT_ERROR
 */
ZOO_EXPORT ZOO_INT32 SM_initialize()
{
    if(NULL != g_processing_flow_facade)
    {
        return g_processing_flow_facade->initialize();
    }
    return SM4A_SYSTEM_ERR;
}
    

/**
 * @brief Execute the business logic to start all tasks
 * @return Error code: OK
 *                     TM4A_SYSTEM_ERROR
 *                     TM4A_ILLEGAL_CALL_ERROR
 *                     TM4A_TIMEOUT_ERROR
 */
ZOO_EXPORT ZOO_INT32 SM_start_all_tasks()
{
    if(NULL != g_processing_flow_facade)
    {
        return g_processing_flow_facade->start_all_tasks();
    }
    return SM4A_SYSTEM_ERR;
}

/**
 * @brief Execute the business logic to stop all tasks
 * @return Error code: OK
 *                     TM4A_SYSTEM_ERROR
 *                     TM4A_ILLEGAL_CALL_ERROR
 *                     TM4A_TIMEOUT_ERROR
 */
ZOO_EXPORT ZOO_INT32 SM_stop_all_tasks()
{
    if(NULL != g_processing_flow_facade)
    {
        return g_processing_flow_facade->stop_all_tasks();
    }
    return SM4A_SYSTEM_ERR;
}

/**
 * @brief Execute the business logic to start a single tasks
 * @return Error code: OK
 *                     TM4A_SYSTEM_ERROR
 *                     TM4A_ILLEGAL_CALL_ERROR
 *                     TM4A_TIMEOUT_ERROR
 */
ZOO_EXPORT ZOO_INT32 SM_start_single_task(IN char name[SM_TASK_NAME_LENGHT],
                                          IN int stack_size,
                                          OUT ZOO_UINT32 * pid)
{
    if(NULL != g_processing_flow_facade)
    {
        return g_processing_flow_facade->start_single_task(name,stack_size,pid);
    }
    return SM4A_SYSTEM_ERR;
}

/**
 * @brief Execute the business logic to stop a single tasks
 * @return Error code: OK
 *                     TM4A_SYSTEM_ERROR
 *                     TM4A_ILLEGAL_CALL_ERROR
 *                     TM4A_TIMEOUT_ERROR
 */
ZOO_EXPORT ZOO_INT32 SM_stop_single_task(IN char name[SM_TASK_NAME_LENGHT])
{
    if(NULL != g_processing_flow_facade)
    {
        return g_processing_flow_facade->stop_single_task(name);
    }
    return SM4A_SYSTEM_ERR;
}


/**
 * @brief get all task running state
 * @return Error code: OK
 *                     TM4A_SYSTEM_ERROR
 *                     TM4A_ILLEGAL_CALL_ERROR
 *                     TM4A_TIMEOUT_ERROR
 */
ZOO_EXPORT ZOO_INT32 SM_get_task_state(IN char name[SM_TASK_NAME_LENGHT],OUT ZOO_TASK_STATE_ENUM * state)
{
    if(NULL != g_processing_flow_facade)
    {
        return g_processing_flow_facade->get_task_state(name,state);
    }
    return SM4A_SYSTEM_ERR;
}


/**
 * @brief get all task running state
 * @return Error code: OK
 *                     TM4A_SYSTEM_ERROR
 *                     TM4A_ILLEGAL_CALL_ERROR
 *                     TM4A_TIMEOUT_ERROR
 */
ZOO_EXPORT ZOO_INT32 SM_get_host_info(IN SM4A_HOST_INFO_STRUCT * host_info)
{
    if(NULL != g_processing_flow_facade)
    {
        return g_processing_flow_facade->get_host_info(host_info);
    }
    return SM4A_SYSTEM_ERR;
}

