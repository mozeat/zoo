/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        :
 * Component id   :
 * File Name      : TASK_CONTROLLER_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-16       weiwang.sun      created
 *************************************************************/
#include "TASK_CONTROLLER_CLASS.h"
namespace ZOO_SM
{
    /**
     * @brief  Constructor
     */
	TASK_CONTROLLER_CLASS::TASK_CONTROLLER_CLASS()
	{
		// TODO Auto-generated constructor stub
	}

	/**
     * @brief  Destructor
     */
	TASK_CONTROLLER_CLASS::~TASK_CONTROLLER_CLASS()
	{
		// TODO Auto-generated destructor stub
	}

	/**
     * @brief initialize
     * @param
     * @return void
     */
    void TASK_CONTROLLER_CLASS::initialize()
    {
        this->m_tasks_in_db.clear();
        this->create_all_models();
    }

	/**
     * @brief Create all configuration running tasks
     * @param
     * @return void
     */
	void TASK_CONTROLLER_CLASS::create_all_models()
	{
		LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__," > function entry");
        try
        {
			for( auto & task : SM_CONFIGURE::get_instance()->get_tasks())
			{
				this->create_model(task);
			}
			LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__," create tasks size: %lu",this->m_tasks_in_db.size());
        }
        catch(...)
        {
            __THROW_SM_EXCEPTION(SM4A_PARAMETER_ERR," Create task model failed ...ERROR",NULL);
        }
		LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__," < function exit");
	}

	/**
     * @brief startup all configuration running tasks
     * @param
     * @return void
     */
	void TASK_CONTROLLER_CLASS::startup_all_models()
	{
	    std::vector<boost::shared_future<int> > pending_data;
	    std::for_each(this->m_tasks_in_db.begin(),this->m_tasks_in_db.end(),[&](auto & t )->auto
        {
            boost::shared_ptr<TASK_MODEL_CLASS> model = t.second;
        	if(model)
        	{     	    
                ZOO_COMMON::THREAD_POOL::get_instance()->queue_working(
                                                            boost::bind(&TASK_MODEL_CLASS::start,model),
                                                            pending_data,
                                                            NULL);
        	}
        	std::this_thread::sleep_for(std::chrono::milliseconds(50));
        });
	}

	/**
     * @brief kill all configuration running tasks
     * @param
     * @return void
     */
	void TASK_CONTROLLER_CLASS::stop_all_models()
	{
		std::vector<boost::shared_future<int> > pending_data;
		
        std::for_each(this->m_tasks_in_db.begin(),this->m_tasks_in_db.end(),[&](auto & t )->auto
        { 
            boost::shared_ptr<TASK_MODEL_CLASS> model = t.second;
        	if(model)
        	{     	    
                ZOO_COMMON::THREAD_POOL::get_instance()->queue_working(
                                                            boost::bind(&TASK_MODEL_CLASS::stop,model),
                                                            pending_data,
                                                            NULL);
        	}
        	std::this_thread::sleep_for(std::chrono::milliseconds(100));
        });
	}

	/**
     * @brief startup a task by name
     * @param name       task file name
     * @return void
     */
	void TASK_CONTROLLER_CLASS::start(const std::string& name)
	{
	    auto found = std::find_if(this->m_tasks_in_db.begin(),this->m_tasks_in_db.end(),
        [&](auto & p )->auto
        {
        	if(!p.second) 
        		return false; 
        	else
        		return std::string(p.second->get_marking_code()) == name;
        });
        auto end = this->m_tasks_in_db.end();
        if(found != end)
        {
            auto task = found->second;
            if(task)
                task->start();
        }
	}

    /**
     * @brief kill a task by name
     * @param name       task name
     * @return void
     */
	void TASK_CONTROLLER_CLASS::stop(const std::string & name)
	{
	    auto found = std::find_if(this->m_tasks_in_db.begin(),this->m_tasks_in_db.end(),
        [&](auto & p )->auto
        {
        	if(!p.second) 
        		return false; 
        	else
        		return std::string(p.second->get_marking_code()) == name;
        });
        auto end = this->m_tasks_in_db.end();
        if(found != end)
        {
            auto task = found->second;
            if(task)
                task->stop();
        }
	}

	/**
	 * @brief set task running status
	 * @param name		 task name
	 * @param state 	 task state
	 * @return void
	 */
	void TASK_CONTROLLER_CLASS::set_running_state(std::string & name,ZOO_TASK_STATE_ENUM state)
	{
		auto found = std::find_if(this->m_tasks_in_db.begin(),this->m_tasks_in_db.end(),
        [&](auto & p )
        {
        	if(!p.second) 
        		return false; 
        	else
        		return std::string(p.second->get_marking_code())== (name);
        });
         
        auto end = this->m_tasks_in_db.end();
        if(found != end)
        {
            auto task = found->second;
            if(task)   
                task->set_running_state(state);
        }
        return ;
	}

	/**
     * @brief get task running status
     * @param name       task  name
     * @return ZOO_TASK_STATE_ENUM
     */
	ZOO_TASK_STATE_ENUM TASK_CONTROLLER_CLASS::get_running_state(std::string & name)
	{
	    auto found = std::find_if(this->m_tasks_in_db.begin(),this->m_tasks_in_db.end(),
        [&](auto & p )
        {
        	if(!p.second) 
        		return false; 
        	else
        		return std::string(p.second->get_marking_code()) == (name);
        });
        auto end = this->m_tasks_in_db.end();
        if(found != end)
        {
            auto task = found->second;
            if(task)
            {
                if(task->get_pid(name) > 0) 
                {
                    return ZOO_TASK_STATE_STARTED;
                }
            }
        }
        return ZOO_TASK_STATE_NOT_STARTED;
	}

    /**
     * @brief check watch tasks and stop them
     * @param
     * @return void
     */
    void TASK_CONTROLLER_CLASS::stop_all_watching()
    {

    }

    /**
     * @brief kill the task by name
     * @param name       task  name
     * @return void
     */
    void TASK_CONTROLLER_CLASS::stop_watching(std::string & /*name*/)
    {

    }

    /**
     * @brief get task instance by name
     * @param name       task name
     * @return object instance
     */
    boost::shared_ptr<TASK_INTERFACE> TASK_CONTROLLER_CLASS::get_task( std::string & name)
    {
        boost::shared_ptr<TASK_INTERFACE> task;
        auto found = std::find_if(this->m_tasks_in_db.begin(),this->m_tasks_in_db.end(),
                                    [&](auto & p )
                                    {
                                    	if(!p.second) 
                                    		return false; 
                                    	else
                                    		return std::string(p.second->get_marking_code())
                                            == (name);
                                    }
                                  );
        auto end = this->m_tasks_in_db.end();
        if(found != end)
        {
            task = found->second;
        }
        return task;
    }

	/**
	 * @brief get all enabled tasks
	 * @return list
	 */
	std::list<boost::shared_ptr<TASK_MODEL_CLASS> > TASK_CONTROLLER_CLASS::get_all_tasks()
	{
        std::list<boost::shared_ptr<TASK_MODEL_CLASS> > entiry_tasks;
        std::for_each(this->m_tasks_in_db.begin(),this->m_tasks_in_db.end(),
			[&](auto & v){entiry_tasks.push_back(v.second);});
        return entiry_tasks;
    }
	
	/**
	 * @brief get process pid
	 * @param name       process name
	 * @return ZOO_INT32
	 */
	ZOO_INT32  TASK_CONTROLLER_CLASS::get_pid(std::string & name)
	{
		boost::shared_ptr<TASK_INTERFACE> task;
        auto found = std::find_if(this->m_tasks_in_db.begin(),this->m_tasks_in_db.end(),[&](auto & p )
        {
        	if(!p.second) 
        		return false; 
        	else
        		return std::string(p.second->get_marking_code()) == name;
        });
        auto end = this->m_tasks_in_db.end();
		ZOO_INT32 pid = SM_INVALID_PID;
        if(found != end)
        {
        	task = found->second;
        	if(task)
            {    
                pid = task->get_pid(task->get_marking_code());
            }
        }
        return pid;
	}

	/**
	 * @brief set process pid
	 * @param pid		pid
	 * @return 
	 */
	void  TASK_CONTROLLER_CLASS::set_pid(std::string & name,ZOO_INT32 pid)
	{
		boost::shared_ptr<TASK_INTERFACE> task;
        auto found = std::find_if(this->m_tasks_in_db.begin(),this->m_tasks_in_db.end(),[&](auto & p )
        {
            return std::string(p.second->get_marking_code()) == (name);
        });
        auto end = this->m_tasks_in_db.end();
        if(found != end)
        {
        	task = found->second;
        	if(task)
            {
                boost::dynamic_pointer_cast<TASK_MODEL_CLASS>(task)->set_pid(pid);
            }
        }
        return ;
	}

	/**
     * @brief get task running status
     * @param name       task name
     * @return  boost::fusion
     */
	void TASK_CONTROLLER_CLASS::create_model(ZOO_TASK_STRUCT * task)
	{
	    LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__," > function entry ...");
	    if(task != NULL)
	    {
			LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__," Create task: %s",task->task_name);
	        boost::shared_ptr<TASK_MODEL_CLASS> model;
			model.reset(new TASK_MODEL_CLASS());
	        model->set_marking_code(task->task_name);
	        model->set_configuration(*task);
	        this->m_tasks_in_db[task->task_name] = model;
        }
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__," < function exit ...");
	}
} /* namespace ZOO_SM */
