#include "STRING_UTILITY_CLASS.h"
namespace ZOO_SM
{
    /**
     * @brief Define default singleton
     */
    boost::shared_ptr<STRING_UTILITY_CLASS> STRING_UTILITY_CLASS::m_instance;

    STRING_UTILITY_CLASS::~STRING_UTILITY_CLASS()
    {
        //dtor
    }

    /**
     * @brief Get instance
     * @return
     */
    boost::shared_ptr<STRING_UTILITY_CLASS> STRING_UTILITY_CLASS::get_instance()
    {
        if (NULL == STRING_UTILITY_CLASS::m_instance)
        {
            STRING_UTILITY_CLASS::m_instance.reset(new STRING_UTILITY_CLASS());
        }
        return STRING_UTILITY_CLASS::m_instance;
    }

    /**
     * @brief Convert ZOO_TASK_STATE_ENUM to string
     */
    const ZOO_CHAR* STRING_UTILITY_CLASS::to_string(ZOO_TASK_STATE_ENUM task_state)
    {
        return get_instance()->m_task_state_converter.to_string(task_state);
    }

    /**
     * @brief Convert ZOO_TRACE_MODE_ENUM to string
     */
    const ZOO_CHAR* STRING_UTILITY_CLASS::to_string(ZOO_TRACE_MODE_ENUM trace_mode)
    {
        return get_instance()->m_trace_mode_converter.to_string(trace_mode);
    }

    /**
     * @brief Convert ZOO_WATCH_MODE_ENUM to string
     */
    const ZOO_CHAR* STRING_UTILITY_CLASS::to_string(ZOO_WATCH_MODE_ENUM watch_mode)
    {
        return get_instance()->m_watch_mode_converter.to_string(watch_mode);
    }
}

