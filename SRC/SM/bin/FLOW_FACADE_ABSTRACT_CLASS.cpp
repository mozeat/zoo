/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : FLOW_FACADE_ABSTRACT_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#include "FLOW_FACADE_ABSTRACT_CLASS.h"

namespace ZOO_SM
{
    FLOW_FACADE_ABSTRACT_CLASS::FLOW_FACADE_ABSTRACT_CLASS()
    {
        //ctor
    }

    FLOW_FACADE_ABSTRACT_CLASS::~FLOW_FACADE_ABSTRACT_CLASS()
    {
        //dtor
    }

    /**
     * @brief Get reference to task controller
     * @return task_controller
     */
    boost::shared_ptr<TASK_CONTROLLER_INTERFACE> FLOW_FACADE_ABSTRACT_CLASS::get_task_controller()
    {
        return this->m_task_controller;
    }

    /**
     * @brief Set reference to task controller
     * @param device_controller    The pointer which is pointed to task controller
     */
    void FLOW_FACADE_ABSTRACT_CLASS::set_task_controller(
            boost::shared_ptr<TASK_CONTROLLER_INTERFACE> task_controller)
    {
        this->m_task_controller = task_controller;
    }

	/**
     * @brief Set reference to task scheduler controller
     * @param device_controller    The pointer which is pointed to task scheduler controller
     */
    void FLOW_FACADE_ABSTRACT_CLASS::set_task_scheduler(
            boost::shared_ptr<TASK_SCHEDULER_INTERFACE> task_scheduler)
	{
		this->m_task_scheduler = task_scheduler;
	}

	/**
     * @brief Get reference to task scheduler
     * @return task_controller
     */
    boost::shared_ptr<TASK_SCHEDULER_INTERFACE> FLOW_FACADE_ABSTRACT_CLASS::get_task_scheduler()
    {
    	return this->m_task_scheduler;
	}
}

