#include "ENVIRONMENT_UTILITY_CLASS.h"
#include <mutex>
#include <stdlib.h>
#include <boost/process.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
namespace ZOO_SM
{
	ENVIRONMENT_UTILITY_CLASS::ENVIRONMENT_UTILITY_CLASS()
    {
        //ctor
    }

    ENVIRONMENT_UTILITY_CLASS::~ENVIRONMENT_UTILITY_CLASS()
    {
        //dtor
    }
    
	/**
	 * @brief get username
	 */
	std::string ENVIRONMENT_UTILITY_CLASS::get_username()
    {
        std::string name = getenv("USER");
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__,"uname:%s",name.c_str());
        return name;
    }

    /**
     * @brief get process id by name
     */
    pid_t ENVIRONMENT_UTILITY_CLASS::get_pid(std::string process_name)
    {
    	std::string cmd = "pidof ";
        pid_t pid_value = SM_INVALID_PID;
        cmd += process_name;
        FILE *fp = popen(cmd.c_str(), "r");
        if(fp)
        {
            if(fscanf(fp, "%d", &pid_value) == EOF)
            {
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"fscanf:%s","fail");
            }
            pclose(fp);
        }
        return pid_value;
    }
    
    /**
     * @brief get pid's usrname
     */
    pid_t ENVIRONMENT_UTILITY_CLASS::get_current_user_pid(std::string process_name)
    {
    	pid_t pid = ENVIRONMENT_UTILITY_CLASS::get_pid(process_name);
    	if(pid != SM_INVALID_PID)
    	{
	    	boost::process::ipstream p;
	    	std::ostringstream exe;
	    	exe << "ps -o uname= -p " << pid;
	    	boost::process::child c(exe.str(),boost::process::std_out > p);
	    	std::string attached_usrname;
	    	std::getline(p,attached_usrname);
        	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__,
        										"pid:%d,attached uname:%s",pid,attached_usrname.c_str());
	    	if(c.wait_for(std::chrono::seconds(3)))
	    	{
		    	if(attached_usrname == ENVIRONMENT_UTILITY_CLASS::get_username())
		    	{
		    		return pid;
		    	}
		    	else
		    	{
		    		//return SM_ILLEGAL_PID;
		    	}
	    	}
    	}
        return SM_INVALID_PID;
    }
}
