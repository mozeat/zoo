/***********************************************************
 * Copyright (C) 2018, Shanghai XXXX CO. ,LTD
 * All rights reserved.
 * Product        :
 * Component id   :
 * File Name      : EXECUTE_HALEPER_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-16       weiwang.sun      created
 *************************************************************/
#include "EXECUTE_HALEPER_CLASS.h"
#include <TASK_INTERFACE.h>
#include <utils/THREAD_POOL.h>
#include <chrono>
#include <thread>

namespace ZOO_SM
{
    EXECUTE_HALEPER_CLASS::EXECUTE_HALEPER_CLASS(IN boost::shared_ptr<TASK_INTERFACE> task):m_task(task)
    {
        //ctor
    }

    EXECUTE_HALEPER_CLASS::~EXECUTE_HALEPER_CLASS()
    {
        //dtor
    }

    /**
     * @brief Invoke a process
     */
    void EXECUTE_HALEPER_CLASS::invoke(IN boost::shared_ptr<EXECUTE_CLASS> execute,THREAD_OPTION option)
    {
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__," >function entry ...");
        auto cmd = execute->get_command();
        auto parameters = execute->get_parameters();
        std::for_each(parameters.begin(),parameters.end(),
                        [&](auto & p){cmd.append(" ").append(p);});
        switch(option)
        {
        case BACKGROUND_THREAD:
            this->do_execute(cmd,*this);
            break;
        case PUBLISHER_THREAD:
            std::thread(std::bind(&EXECUTE_HALEPER_CLASS::do_execute,
                                        this,cmd,*this)).detach();
            break;
        default:break;
        
        }
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__," <function exit ...");
    }

    /**
     * @brief Terminate a process if running
     */
    void EXECUTE_HALEPER_CLASS::terminate()
    {
        if(this->m_system != NULL)
        {
            this->m_system->terminate();
        }
    }

    /**
     * @brief Check is running
     */
    bool EXECUTE_HALEPER_CLASS::running()
    {
        if(this->m_system == NULL)
        {
            return false;
        }
        return this->m_system->running();
    }

    /**
     * @brief Notify task state
     */
    void EXECUTE_HALEPER_CLASS::notify(ZOO_TASK_STATE_ENUM state)
    {
        if(this->m_task != NULL)
        {
            //this->m_task->set_state(state);
        }
    }

    /**
     * @brief Execute a process,this function will block
     * usually a process has an event loop,
     * it would not quit except abnormal behavior such as SIGSEGV or kill
     */
    void EXECUTE_HALEPER_CLASS::do_execute(std::string exe,boost::process::extend::handler & handle)
    {
    	std::error_code ec;
    	boost::process::child c(exe,ec);
        //boost::process::child c(exe,ec);
		c.wait();        
    }
}
