/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : MARKING_MODEL_INTERFACE.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#include "MARKING_MODEL_INTERFACE.h"

namespace ZOO_SM
{
    MARKING_MODEL_INTERFACE::MARKING_MODEL_INTERFACE()
    {
        //ctor
    }

    MARKING_MODEL_INTERFACE::~MARKING_MODEL_INTERFACE()
    {
        //dtor
    }

    /**
     * @brief Get the marking_code attribute value
     */
    const ZOO_CHAR* MARKING_MODEL_INTERFACE::get_marking_code()
    {
        return this->m_marking_code.get();
    }

    /**
     * @brief Set the marking_code attribute value
     * @param marking_code    The new marking_code attribute value
     */
    void MARKING_MODEL_INTERFACE::set_marking_code(const ZOO_CHAR* marking_code)
    {
        if (NULL == marking_code)
        {
            this->m_marking_code.reset();
        }
        else
        {
            ZOO_CHAR* buffer = new ZOO_CHAR[strlen(marking_code) + 1];
            strcpy(buffer, marking_code);
            this->m_marking_code.reset(buffer);
        }
    }
}
