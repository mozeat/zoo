/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : PROCESSING_FLOW_FACADE_CALSS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#include "PROCESSING_FLOW_FACADE_CALSS.h"
#include "TASK_CONTROLLER_CLASS.h"
#include "LOG_MANAGER_CLASS.h"

namespace ZOO_SM
{
    /**
     * @brief Constructor
     */
    PROCESSING_FLOW_FACADE_CALSS::PROCESSING_FLOW_FACADE_CALSS()
    {
        this->m_broker_task.reset(new TASK_MODEL_CLASS());
        const char * broker = "ZOO_BROKER";
    	this->m_broker_task->set_marking_code(broker);
    }

    /**
     * @brief Destructor
     */
    PROCESSING_FLOW_FACADE_CALSS::~PROCESSING_FLOW_FACADE_CALSS()
    {
        //dtor
    }
    
    /**
     * @brief Load platform configure file ,generate database file
     * @param
     * @return OK -- 正确
               TM4A_SYSTEM_ERR -- 系统错误
               SM4A_PARAMETER_ERR -- 参数错误
     */
    ZOO_INT32 PROCESSING_FLOW_FACADE_CALSS::initialize()
    {
    	LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"> function entry  ...");
        ZOO_INT32 error_code = OK;
        __SM_TRY
        {
	     	/**
	     	* @brief step 3 : stop all tasks
	     	*/
	     	this->stop_all_tasks();
	     	
	     	/**
	     	* @brief step 4 : create all tasks
	     	*/
	     	auto controller = this->get_task_controller();
			if(controller != nullptr)
			{
	     		controller->initialize();
	     	}
     	}
     	__SM_CATCH_ALL(error_code,ZOO_FALSE)
     	
        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"< function exit  ...");
        return error_code;
    }

    /**
     * @brief Start all configuration task
     * @param
     * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
     */
    ZOO_INT32 PROCESSING_FLOW_FACADE_CALSS::start_all_tasks()
    {
    	LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"> function entry  ...");
        ZOO_INT32 error_code = OK;
        __SM_TRY
        {
            boost::shared_ptr<TASK_CONTROLLER_CLASS> controller =
                boost::dynamic_pointer_cast<TASK_CONTROLLER_CLASS> (this->get_task_controller());
            if(NULL == controller)
            {
                __THROW_SM_EXCEPTION(SM4A_PARAMETER_ERR,"Could not found task controller",nullptr);
            }
			controller->stop_all_models();
			controller->startup_all_models();
        }
        __SM_CATCH_ALL(error_code,ZOO_FALSE)
        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"< function exit  ...");
        return error_code;
    }

    /**
     * @brief Stop all configuration task
     * @param
     * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
     */
    ZOO_INT32 PROCESSING_FLOW_FACADE_CALSS::stop_all_tasks()
    {
    	LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"> function entry  ...");
        ZOO_INT32 error_code = OK;
        __SM_TRY
        {
            boost::shared_ptr<TASK_CONTROLLER_CLASS> controller =
                boost::dynamic_pointer_cast<TASK_CONTROLLER_CLASS> (this->get_task_controller());
            if(NULL == controller)
            {
                __THROW_SM_EXCEPTION(SM4A_PARAMETER_ERR,
                						"Could not found task controller",nullptr);
            }
            
			std::list<boost::shared_ptr<TASK_MODEL_CLASS> > entity = controller->get_all_tasks();
			std::for_each(entity.begin(),entity.end(),
			[&](auto & t)
			{
				t->stop();
				this->get_task_scheduler()->remove_observer(t);
			});
        }
        __SM_CATCH_ALL(error_code,ZOO_FALSE)
        LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"< function exit  ...");
        return error_code;
    }

    /**
     * @brief Start single task
     * @param name
     * @param stack_size
     * @param pid         the process id
     * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
     */
    ZOO_INT32 PROCESSING_FLOW_FACADE_CALSS::start_single_task(IN std::string name,
					        										  IN ZOO_INT32 stack_size,
					        										  OUT ZOO_UINT32 * pid)
    {
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " > Task[%s] ",name.data());
        ZOO_INT32 error_code = OK;
        __SM_TRY
        {
            boost::shared_ptr<TASK_CONTROLLER_CLASS> controller =
                boost::dynamic_pointer_cast<TASK_CONTROLLER_CLASS> (this->get_task_controller());
            if(NULL == controller)
            {
                __THROW_SM_EXCEPTION(SM4A_PARAMETER_ERR,
                						"Task controller pointer is NULL ...ERROR",nullptr);
            }
            *pid = controller->get_pid(name);
			if(*pid == SM_INVALID_PID)
			{
				ZOO_TASK_STATE_ENUM running_state = ZOO_TASK_STATE_NOT_STARTED;
            	controller->start(name);
				*pid = controller->get_pid(name);
				running_state = ZOO_TASK_STATE_STARTED;
				controller->set_running_state(name,running_state);				
			}
			controller->set_pid(name,static_cast<pid_t>(*pid));
			this->get_task_scheduler()->add_observer(controller->get_task(name));
            LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " Task[%s] start successfully ...pid[%d]",name.data(),*pid);
        }
        __SM_CATCH_ALL(error_code,ZOO_FALSE)
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " < task[%s] error_code[%d]",name.data(),error_code);
        return error_code;
    }

    /**
     * @brief Stop single task
     * @param
     * @return OK -- 正确
                   TM4A_SYSTEM_ERR -- 系统错误
                   SM4A_PARAMETER_ERR -- 参数错误
     */
    ZOO_INT32 PROCESSING_FLOW_FACADE_CALSS::stop_single_task(IN std::string name)
    {
    	LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " > Task[%s] ",name.data());
        ZOO_INT32 error_code = OK;
        __SM_TRY
        {
            boost::shared_ptr<TASK_CONTROLLER_CLASS> controller =
                boost::dynamic_pointer_cast<TASK_CONTROLLER_CLASS> (this->get_task_controller());
            if(NULL == controller)
            {
                __THROW_SM_EXCEPTION(SM4A_PARAMETER_ERR,"Task controller pointer is NULL ...ERROR",nullptr);
            }
            controller->stop(name);
			this->get_task_scheduler()->remove_observer(controller->get_task(name));
            LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, "Task[%s] kill successfully ...",name.data());
        }
        __SM_CATCH_ALL(error_code,ZOO_FALSE)
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " < task[%s] error_code[%d]",name.data(),error_code);
        return error_code;
    }

    /**
     * @brief Stop single task
     * @param name
     * @param state
     * @return OK -- 正确
               TM4A_SYSTEM_ERR -- 系统错误
               SM4A_PARAMETER_ERR -- 参数错误
     */
    ZOO_INT32 PROCESSING_FLOW_FACADE_CALSS::get_task_state(IN std::string name,
        															OUT ZOO_TASK_STATE_ENUM * state)
    {
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " > Task[%s] ",name.data());
        ZOO_INT32 error_code = OK;
        __SM_TRY
        {
            boost::shared_ptr<TASK_CONTROLLER_CLASS> controller =
                boost::dynamic_pointer_cast<TASK_CONTROLLER_CLASS> (this->get_task_controller());
            if(NULL == controller)
            {
                __THROW_SM_EXCEPTION(SM4A_PARAMETER_ERR,"Task controller pointer is NULL ...ERROR",nullptr);
            }
            *state = controller->get_running_state(name);
        }
        __SM_CATCH_ALL(error_code,ZOO_FALSE)
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " < state[%d],error_code[%d]",*state,error_code);
        return error_code;
    }
    
    /**
     * @brief Get host info
     * @param name
     * @param state
     * @return OK -- 正确
               TM4A_SYSTEM_ERR -- 系统错误
               SM4A_PARAMETER_ERR -- 参数错误
     */
    ZOO_INT32 PROCESSING_FLOW_FACADE_CALSS::get_host_info(IN SM4A_HOST_INFO_STRUCT * host_info)
    {
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " > function entry ... ");
        ZOO_INT32 error_code = OK;
        SM4A_HOST_INFO_STRUCT info = SM4A_HOST_INFO_STRUCT();//= SM_CONFIGURE::get_instance()->get_host_info();
        info.host_present = ZOO_TRUE;
        memcpy(host_info,&info,sizeof(SM4A_HOST_INFO_STRUCT));
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " < function exit ... ");
        return error_code;
    }	

   	/**
     * @brief Startup local brokers before process communication
     * @return void
     */
	void PROCESSING_FLOW_FACADE_CALSS::startup_local_broker()
    {
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " > function entry ... ");
		this->m_broker_task->start();
		std::this_thread::sleep_for(std::chrono::seconds(2));
		if(this->m_broker_task->get_pid(this->m_broker_task->get_marking_code()) == SM_INVALID_PID)
		{
			LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " can not startup broker ...ERROR");
		}
		LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " < function exit ... ");
    }
    
    /**
     * @brief Shutdown local brokers 
     * @return void
     */
	void PROCESSING_FLOW_FACADE_CALSS::shutdown_local_broker()
    {
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " > function entry ... ");
    	while(this->m_broker_task->get_pid(this->m_broker_task->get_marking_code()) != SM_INVALID_PID)
		{
			this->m_broker_task->stop();
			std::this_thread::sleep_for(std::chrono::seconds(2));
		}
		LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__, " < function exit ... ");
    }
}

