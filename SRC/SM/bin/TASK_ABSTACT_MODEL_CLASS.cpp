/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : TASK_ABSTACT_MODEL_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#include "TASK_ABSTACT_MODEL_CLASS.h"

namespace ZOO_SM
{
    /**
     * @brief Constructor
     */
    TASK_ABSTACT_MODEL_CLASS::TASK_ABSTACT_MODEL_CLASS()
    {
		this->m_configure.trace_mode   = ZOO_TRACE_DISABLE;
        this->m_configure.stack_size   = 20;
		this->m_configure.sim_mode     = ZOO_SIM_DISABLE;
        this->m_configure.monitor_mode = ZOO_WATCH_MODE_DISABLE;
		this->m_configure.priority     = 150;
		this->m_pid                    = SM_INVALID_PID;
		this->m_restart_times          = 0;
		this->m_running_state          = ZOO_TASK_STATE_NOT_STARTED;
    }

    /**
     * @brief Destructor
     */
    TASK_ABSTACT_MODEL_CLASS::~TASK_ABSTACT_MODEL_CLASS()
    {
        //dtor
    }

	/**
	 * @brief Get the task start order
	 * @param
	 * @return alias
	 */
	const ZOO_CHAR * TASK_ABSTACT_MODEL_CLASS::get_componet_id()
	{
		return this->m_configure.component;
	}
	
	/**
	 * @brief Set the task start order
	 * @param order
	 * @return void
	 */
	void TASK_ABSTACT_MODEL_CLASS::set_componet_id(const ZOO_CHAR * componet_id)
	{
		memcpy(this->m_configure.component,componet_id,strlen(componet_id) + 1);
	}

	/**
	* @brief Get the task pid
	* @param
	* @return pid
	*/
	const ZOO_INT32 & TASK_ABSTACT_MODEL_CLASS::get_pid()
	{
		return this->m_pid;
	}
	

	/**
	* @brief Set the task pid
	* @param pid
	* @return void
	*/
	void TASK_ABSTACT_MODEL_CLASS::set_pid(ZOO_INT32    pid)
	{
		this->m_pid = pid;
	}
	
	/**
	 * @brief get task priority
	 * @param
	 * @return priority
	 */
	ZOO_INT32 TASK_ABSTACT_MODEL_CLASS::get_priority()
	{
		return this->m_configure.priority;
	}

	/**
	 * @brief set task priority
	 * @param priority
	 * @return void
	 */
	void TASK_ABSTACT_MODEL_CLASS::set_priority(ZOO_INT32 priority)
	{
		this->m_configure.priority = priority;
	}

	/**
	 * @brief get task running status : started or not started
	 * @param
	 * @return ZOO_TASK_STATE_ENUM
	 */
	const ZOO_TASK_STATE_ENUM & TASK_ABSTACT_MODEL_CLASS::get_running_state()
	{
		return this->m_running_state;
	}

	/**
	 * @brief set task running status
	 * @param running_status started or not started
	 * @return ZOO_TASK_STATE_ENUM
	 */
	void TASK_ABSTACT_MODEL_CLASS::set_running_state(ZOO_TASK_STATE_ENUM running_state)
	{
		this->m_running_state = running_state;
	}

	/**
	 * @brief get task stack configuration size
	 * @param
	 * @return ZOO_INT32
	 */
	ZOO_INT32 TASK_ABSTACT_MODEL_CLASS::get_stack_size()
	{
		return this->m_configure.stack_size;
	}

	/**
	 * @brief set task stack configuration size
	 * @param stack_size    the size of a process stack
	 * @return void
	 */
	void TASK_ABSTACT_MODEL_CLASS::set_stack_size(ZOO_INT32 stack_size)
	{
		this->m_configure.stack_size = stack_size;
	}

	/**
	 * @brief get task trace mode
	 * @param
	 * @return ZOO_TRACE_MODE_ENUM
	 */
	const ZOO_TRACE_MODE_ENUM & TASK_ABSTACT_MODEL_CLASS::get_trace_mode()
	{
		return this->m_configure.trace_mode;
	}

	/**
	 * @brief set the task trace mode
	 * @param trace_mode          TR4A_TRACE_DISABLE:TR4A_TRACE_ENABLE
	 * @return void
	 */
	void TASK_ABSTACT_MODEL_CLASS::set_trace_mode(ZOO_TRACE_MODE_ENUM trace_mode)
	{
		this->m_configure.trace_mode = trace_mode;
	}

	/**
	 * @brief set task watch configuration
	 * @param
	 * @return TM4A_WATCH_CONFIGURATION_STRUCT
	 */
	const ZOO_WATCH_MODE_ENUM & TASK_ABSTACT_MODEL_CLASS::get_watch_mode()
	{
		return this->m_configure.monitor_mode;
	}

	/**
	 * @brief set task watch configuration
	 * @param watch_config   watch configuration
	 * @return void
	 */
	void TASK_ABSTACT_MODEL_CLASS::set_watch_mode(
			const ZOO_WATCH_MODE_ENUM & watch_mode)
	{
		this->m_configure.monitor_mode = watch_mode;
	}
	/**
     * @brief Set task  configuration
     * @param watch_config   configuration
     * @return void
     */
    void TASK_ABSTACT_MODEL_CLASS::set_configuration(ZOO_TASK_STRUCT        & config)
    {
        memcpy(&this->m_configure,&config,sizeof(ZOO_TASK_STRUCT));
    }

    /**
     * @brief Get task  configuration
     * @param watch_config   configuration
     * @return void
     */
    ZOO_TASK_STRUCT TASK_ABSTACT_MODEL_CLASS::get_configuration()
    {
        return this->m_configure;
    }
	
	/**
	 * @brief Update process pid
	 * @param pid
	 * @return void
	 */
	void TASK_ABSTACT_MODEL_CLASS::update_pid(ZOO_INT32 pid)
	{
		this->m_pid = pid;
	}

	/**
	 * @brief Update restart times
	 * @param pid
	 * @return void
	 */
	void TASK_ABSTACT_MODEL_CLASS::increase_restart_times()
	{
		this->m_restart_times ++;
	}
	
	/**
	 * @brief Update restart times
	 * @return void
	 */
	void TASK_ABSTACT_MODEL_CLASS::reset_restart_times()
	{
		this->m_restart_times = 0;
	}

	/**
	 * @brief Check current restart times out of range
	 * @return void
	 */
	ZOO_BOOL TASK_ABSTACT_MODEL_CLASS::check_reach_max_restart_times()
	{
		return this->m_restart_times >= 3;
	}
}
