/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : DATABASE_MANAGER_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#include "DATABASE_MANAGER_CLASS.h"
#include "LOG_MANAGER_CLASS.h"
namespace ZOO_SM
{
    /**
     * @brief Initialize NULL for static member instance
     */
    boost::shared_ptr<DATABASE_MANAGER_CLASS> DATABASE_MANAGER_CLASS::m_instance = NULL;

	/**
	 * @brief Tasks configure file db
	 */
	//static const ZOO_CHAR* ZOO_CFG_SQL_FILE = "ZOO_PL_CFG";

	/**
	 * @brief Tasks configure file db
	 */
	//static const ZOO_CHAR* ZOO_EH_SQL_FILE = "ZOO_EH";

	/**
	 * @brief Tasks configure file db
	 */
	//static const ZOO_CHAR* ERROR_CODE = "_ERROR_DEFINE";
	
	/**
	 * @brief Tasks configure file xlxs
	 */
	//static const ZOO_CHAR* XLSX = ".xlsx";
	
	/**
	 * @brief EH configure file
	 */
	//static const ZOO_CHAR* SQL = ".db";

	/**
	 * @brief Tasks configure relative path
	 */
	//static const ZOO_CHAR* SLASH = "/";
	
    /**
     * @brief Constructor
     */
    DATABASE_MANAGER_CLASS::DATABASE_MANAGER_CLASS()
    {
        //ctor
        this->m_host_info.host_present = ZOO_FALSE;
    }

    /**
     * @brief Destructor
     */
    DATABASE_MANAGER_CLASS::~DATABASE_MANAGER_CLASS()
    {
        //dtor
    }

    /**
     * @brief Get unique instance
     * @return instance object
     */
    boost::shared_ptr<DATABASE_MANAGER_CLASS>  DATABASE_MANAGER_CLASS::get_instance()
    {
        /**
        * @brief call only once
        */
        if(DATABASE_MANAGER_CLASS::m_instance == NULL)
        {
        	DATABASE_MANAGER_CLASS::m_instance.reset(new DATABASE_MANAGER_CLASS());
        }
        return DATABASE_MANAGER_CLASS::m_instance;
    }

    /**
     * @brief Initialize
     */
    void DATABASE_MANAGER_CLASS::initialize()
    {
    	/**
	     * @step 1: base path
	     */
		//std::string cfg_path       = SM_CONFIGURE::get_instance()->get_config_base_path();
		//std::string db_output_path = SM_CONFIGURE::get_instance()->get_db_base_path();
		
		/**
	     * @step 2: check output path exist
	     */
		//this->create_database_output_path(db_output_path);

		/**
	     * @step 3: create ZOO_PL_CFG database by xlsx
	     */
		//std::string pl_cfg_file = cfg_path + SLASH + "ZOO" + SLASH + ZOO_CFG_SQL_FILE + XLSX;
        //std::string output      = db_output_path + SLASH + ZOO_CFG_SQL_FILE + SQL;
        //LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    	//								"pl_cfg_file[%s] ...",
    	//								pl_cfg_file.data());
        //LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    	//								"output[%s] ...",
    	//								output.data());
    	//BOOST_ASSERT_MSG(this->exist(output),"$HOME/DB/ZOO_PL_CFG.db is not existence");								
    	/*if(!this->exist(output))
    	{        								
             this->create_database_by_xlsx(pl_cfg_file,output);            
     	}*/
     	
        /**
	     * @step 4: load platform cfg database
	     */
	    ZOO_CHAR resource_id[ZOO_RESOURCE_ID_LENGHT]; 
		if(OK == ZOO_get_resource_id(resource_id))
		{
			this->m_resource_id = resource_id;
		}
	    //auto tasks = SM_CONFIGURE::get_instance()->get_tasks();	

	    /**
	     * @step 5: create exception handler subscribe database
	     */
		//pl_cfg_file = cfg_path    + SLASH + "EH" + SLASH + ZOO_EH_SQL_FILE + XLSX;
        //output      = db_output_path + SLASH + ZOO_EH_SQL_FILE + SQL;
        /*LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"EH xlsx file[%s] ...",
    									pl_cfg_file.data());
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"output[%s] ...",
    									output.data());
        BOOST_ASSERT_MSG(this->exist(output), "$HOME/DB/ZOO_EH.db is not existence");*/
    	/*if(!this->exist(output))
    	{
            this->create_database_by_xlsx(pl_cfg_file,output);
        }*/
	     
		/**
	     * @step 6: create component error_code database by xlsx
	     */
	    //this->create_error_code_database(cfg_path,db_output_path,tasks);
    }

   /**
	* @brief Get resource_id
	* @return
	*/
	std::string DATABASE_MANAGER_CLASS::get_resource_id()
	{
		return this->m_resource_id;
	}

	/**
	 * @brief Get host info
	 */
	const SM4A_HOST_INFO_STRUCT & DATABASE_MANAGER_CLASS::get_host_info()
	{
		return this->m_host_info;
	}

	
    /**
     * @brief Create database by xlsx
     */
	void DATABASE_MANAGER_CLASS::create_database_by_xlsx(IN std::string xlsx_file, 
																	IN std::string output)
	{
	    LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"::xlsx is %s  ...",xlsx_file.data());
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"::output is %s  ...",output.data());
		SQLITERBITER_CLASS(xlsx_file,output);
	}
	
	/**
     * @brief Check file exist
     */										
	ZOO_BOOL DATABASE_MANAGER_CLASS::exist(IN std::string db_file)
	{
	    LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"::input file is %s  ...",db_file.data());
						
        boost::system::error_code error_code;
        auto file_status = boost::filesystem::status(db_file, error_code);
        if(error_code)
        {
            LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,"::error_code-%s",error_code.message().data());
            return ZOO_FALSE;
        }
        
		if(!boost::filesystem::exists(file_status))
    	{
    		LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,"::[%s] is exist ...",db_file.data());
    		return ZOO_FALSE;							
    	}
    	else
    	{
    	    std::string extension = boost::filesystem::extension(db_file);	
	        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"::file extension is %s  ...",extension.data());	
    	}

    	if(boost::filesystem::is_directory(file_status)) 
    	{
    	    LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,"::[%s] is a directory ...",db_file.data());
            return ZOO_FALSE;
        }
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,"::[%s] is existence ...",db_file.data());
    	return ZOO_TRUE;							   	
	}	
	
	/**
	 * @brief Check file exist
	 */ 
	ZOO_BOOL DATABASE_MANAGER_CLASS::rename(IN std::string xlsx_file, 
											IN std::string & output)
	{
		if(boost::filesystem::exists(output))
    	{
    		LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"output[%s] exist ...",
    									output.data());
    		output = xlsx_file.replace(xlsx_file.end() - 4,xlsx_file.end(),"sql");						
    		return ZOO_TRUE;							
    	}
    	else
    	{
    		LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"output[%s] not exist ...",
    									output.data());
				
    		return ZOO_FALSE;							
    	}
	}	
	
	/**
	 * @brief Create output path exist
	 */
	void DATABASE_MANAGER_CLASS::create_database_output_path(IN std::string path)
	{
		boost::filesystem::path db_output_path(path); 
    	if(boost::filesystem::exists(db_output_path))
    	{
    		//boost::filesystem::remove_all(db_output_path);
    	}
    	boost::filesystem::create_directory(db_output_path);
	}

	/**
	 * @brief create database
	 */ 										
	void DATABASE_MANAGER_CLASS::create_error_code_database(IN std::string cfg_path,
																		IN std::string output_path,
																		IN std::vector<ZOO_TASK_STRUCT*> & tasks)
	{
		/*for(const auto & task : tasks)
		{
			LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"task[%s] enable[%d] ...",
    									task->component,task->error_config);
			if(task->error_config)
			{
				std::string error_code_xlsx = cfg_path + SLASH + "EH" + SLASH + task->component + ERROR_CODE + XLSX;
        		std::string output      = output_path + SLASH + task->component + ERROR_CODE + SQL;
        		LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,
    									"error_code_xlsx[%s] output[%s] ...",
    									error_code_xlsx.data(),output.data());
    			this->create_database_by_xlsx(error_code_xlsx,output);
			}
		}*/
	}
}

