/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : NOTIFY_CONTEXT_INTERFACE.cpp
 * Description  : The observer class to listen property change
 * History :
 * Version      Date				User			Comments
 * V1.0.0.0     2018-05-20			MOZEAT			Initialize
 ***************************************************************/
#include "NOTIFY_CONTEXT_INTERFACE.h"
namespace ZOO_SM
{
    NOTIFY_CONTEXT_INTERFACE::NOTIFY_CONTEXT_INTERFACE()
	{
	
	}
	
    NOTIFY_CONTEXT_INTERFACE::~NOTIFY_CONTEXT_INTERFACE()
	{
	
	}

} /* namespace SCHEDULER */

