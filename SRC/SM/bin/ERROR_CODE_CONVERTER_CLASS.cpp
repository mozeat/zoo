/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TM
 * File Name      : ERROR_CODE_CONVERTER_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#include "ERROR_CODE_CONVERTER_CLASS.h"

namespace ZOO_SM
{
    boost::shared_ptr<ERROR_CODE_CONVERTER_CLASS> ERROR_CODE_CONVERTER_CLASS::m_instance = NULL;

    /**
     * @brief Constructor
     */
    ERROR_CODE_CONVERTER_CLASS::ERROR_CODE_CONVERTER_CLASS()
    {
        //ctor
    }

    /**
     * @brief Destructor
     */
    ERROR_CODE_CONVERTER_CLASS::~ERROR_CODE_CONVERTER_CLASS()
    {
        //dtor
    }
    /**
     * @brief Get singleton in instance
     */
    boost::shared_ptr<ERROR_CODE_CONVERTER_CLASS> ERROR_CODE_CONVERTER_CLASS::get_instance()
    {
        if (ERROR_CODE_CONVERTER_CLASS::m_instance == NULL)
        {
            ERROR_CODE_CONVERTER_CLASS::m_instance.reset(new ERROR_CODE_CONVERTER_CLASS());
        }

        return ERROR_CODE_CONVERTER_CLASS::m_instance;
    }

    /**
     * @brief Convert exception to error code
     * @param error_code Current error code
     * @return the error code of RH
     */
    ZOO_INT32 ERROR_CODE_CONVERTER_CLASS::convert_error_code_exception(ZOO_INT32 error_code)
    {

        ZOO_INT32 tm_error_code = error_code;
        switch (error_code)
        {
            case SM4A_PARAMETER_ERR:
                tm_error_code = SM4A_PARAMETER_ERR;
                break;
            default:
                tm_error_code = SM4A_SYSTEM_ERR;
                break;
        }
        return tm_error_code;
    }
}

