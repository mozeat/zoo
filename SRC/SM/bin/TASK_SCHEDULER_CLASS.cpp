/**************************************************************
 * Copyright (C) 2015, SHANGHAI MICRO ELECTRONICS EQUIPMENT CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : TM
 * File Name    : TASK_SHULDULER_CLASS.h
 * Description  : 
 * History :
 * Version      Date				User			Comments
 * V1.0.0.0     2018-06-19			WEIWANG			Initialize
 ***************************************************************/
#include "TASK_SCHEDULER_CLASS.h"
namespace ZOO_SM
{
	TASK_SCHEDULER_CLASS::TASK_SCHEDULER_CLASS()
	{
		this->m_observer_changed = ZOO_FALSE;
		this->m_stop =  ZOO_FALSE;
	}
	
	TASK_SCHEDULER_CLASS::~TASK_SCHEDULER_CLASS()
	{
		
	}

	/**
	 * @brief Add observer will be notified when property changed
	 * @param observer  Property changed observer
	 */
	void TASK_SCHEDULER_CLASS::start()
	{
		while(this->is_stop() != ZOO_TRUE)
		{
			std::unique_lock<std::mutex> lock(this->m_sync_lock);
			while(this->m_observer_changed)
			{
				this->m_write_observer_list_cv.wait(lock);
			}
			
			std::list<boost::shared_ptr<TASK_INTERFACE> >::iterator ite 
										= this->m_notify_observer.begin();
			while(ite != this->m_notify_observer.end())
			{
				if(this->is_stop())
				{
					break;
				}
				auto p = *ite;
				boost::shared_ptr<TASK_MODEL_CLASS> task =
					boost::dynamic_pointer_cast<TASK_MODEL_CLASS>(p);
				if(task->get_running_state() != ZOO_TASK_STATE_NOT_STARTED)
					task->restart();
				ite ++;
			}
			lock.unlock();
			this->m_read_observer_list_cv.notify_all();
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
	}

	/**
	 * @brief Add observer will be notified when property changed
	 * @param observer  Property changed observer
	 */
	void TASK_SCHEDULER_CLASS::stop()
	{
		this->m_stop = ZOO_TRUE;
		this->m_notify_observer.clear();
	}	
	
	/**
	 * @brief Add observer will be notified when property changed
	 * @param observer  Property changed observer
	 */
	void TASK_SCHEDULER_CLASS::add_observer(boost::shared_ptr<TASK_INTERFACE> observer)
	{
		LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"function entry > ...");
		std::unique_lock<std::mutex> lock(this->m_sync_lock);
		this->m_read_observer_list_cv.wait(lock);
		this->m_observer_changed = ZOO_TRUE;
		std::list<boost::shared_ptr<TASK_INTERFACE> >::iterator ite 
										= this->m_notify_observer.begin();
		while(ite != this->m_notify_observer.end())
		{
			auto p = *ite;
			if(std::string(p->get_marking_code()) == 
				std::string(observer->get_marking_code()))
			{
				break;
			}
			ite ++;
		}
		if(ite == this->m_notify_observer.end())
			this->m_notify_observer.push_back(observer);
		this->m_write_observer_list_cv.notify_all();
		this->m_observer_changed = ZOO_FALSE;
		LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"function exit < ...");
	}

	/**
	 * @brief Remove observer from subscribe list
	 */
	void TASK_SCHEDULER_CLASS::remove_observer(boost::shared_ptr<TASK_INTERFACE> observer)
	{		
		LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"function entry > ...");
		std::unique_lock<std::mutex> lock(this->m_sync_lock);
		this->m_read_observer_list_cv.wait(lock);
		this->m_observer_changed = ZOO_TRUE;
		std::list<boost::shared_ptr<TASK_INTERFACE> >::iterator ite 
										= this->m_notify_observer.begin();
		while(ite != this->m_notify_observer.end())
		{
			auto p = *ite;
			if(std::string(p->get_marking_code()) == 
				std::string(observer->get_marking_code()))
			{
				this->m_notify_observer.erase(ite);
				break;
			}
			ite ++;
		}
		this->m_write_observer_list_cv.notify_all();
		this->m_observer_changed = ZOO_FALSE;
		LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"function exit < ...");
	}

	/**
	 * @brief Remove observer from subscribe list
	 */
	void TASK_SCHEDULER_CLASS::clean()
	{
		this->m_notify_observer.clear();
	}

	/**
	 * @brief Notify property has been changed
	 * @param property_name     The property has been changed
	 */
	void TASK_SCHEDULER_CLASS::notify_of_property_changed(const ZOO_INT32 property_name)
	{
		
	}

	/**
     * @brief Is stopped.
     * @return stop dispatcher
     */
	ZOO_BOOL TASK_SCHEDULER_CLASS::is_stop()
	{
		return this->m_stop;
	}
}