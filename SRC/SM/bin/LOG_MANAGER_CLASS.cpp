/***********************************************************
* Copyright (C) 2018, Shanghai XXXX CO. ,LTD
* All rights reserved.
* Product        : ZOO
* Component id   : TM
* File Name      : LOG_MANAGER_CLASS.cpp
* Description    : {Summary Description}
* History        :
* Version        date          author         context
* V1.0.0         20181001      mozeat.sun      created
*************************************************************/
#include "LOG_MANAGER_CLASS.h"
#include <stdarg.h>
#include <string>
#include <vector>

namespace ZOO_SM
{
	ZOO_BOOL SM4A_ENABLE_DEBUG = ZOO_TRUE;
    LOG_MANAGER_CLASS::LOG_MANAGER_CLASS()
    {
        //ctor
    }

    LOG_MANAGER_CLASS::~LOG_MANAGER_CLASS()
    {
        //dtor
    }

    void LOG_MANAGER_CLASS::initialize()
    {

    }

    void LOG_MANAGER_CLASS::log(const char* function_name,const char* format, ...)
    {
        char buffer[256];
        va_list args;
        if(format != nullptr)
        {
            va_start(args, format);
            vsprintf(buffer, format, args);
            va_end(args);
        }
        buffer[255] = '\0';
		char timestamp[32];
		ZOO_get_current_timestamp(timestamp);
		timestamp[31] = '\0';
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,function_name,buffer); 
    }
	void LOG_MANAGER_CLASS::debug(const char* function_name,const char* format, ...)
	{
		if(SM4A_ENABLE_DEBUG)
		{
			char buffer[256];
	        va_list args;
	        if(format != nullptr)
	        {
	            va_start(args, format);
	            vsprintf(buffer, format, args);
	            va_end(args);
	        }
        	buffer[255] = '\0';
			char timestamp[32];
			ZOO_get_current_timestamp(timestamp);
			timestamp[31] = '\0';
        	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,function_name,buffer); 
		}
	}

    void LOG_MANAGER_CLASS::add_sink(int level,std::string log_file)
    {
        level = level;
        log_file = log_file;
    }
}

