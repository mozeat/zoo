/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : SM_CONFIGURE.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#include "SM_CONFIGURE.h"
#include "LOG_MANAGER_CLASS.h"

namespace ZOO_SM
{
    /**
     * @brief Call one for instance
     */
    static boost::once_flag g_instance_once_flag = BOOST_ONCE_INIT;

    /**
     * @brief Initialize NULL for static member instance
     */
    boost::shared_ptr<SM_CONFIGURE> SM_CONFIGURE::m_instance = NULL;


	/**
	 * @brief Tasks configure relative path
	 */
	//static const ZOO_CHAR* SLASH = "/";
	
    /**
     * @brief Constructor
     */
    SM_CONFIGURE::SM_CONFIGURE()
    {
    
    }

    /**
     * @brief Destructor
     */
    SM_CONFIGURE::~SM_CONFIGURE()
    {
        //dtor
    }

    /**
     * @brief Get unique instance
     * @return instance object
     */
    boost::shared_ptr<SM_CONFIGURE>  SM_CONFIGURE::get_instance()
    {
        /**
        * @brief call only once
        */
        boost::call_once([&]{SM_CONFIGURE::m_instance.reset(new SM_CONFIGURE());},g_instance_once_flag);
        return SM_CONFIGURE::m_instance;
    }

    /**
     * @brief Initialize
     */
    void SM_CONFIGURE::initialize()
    {
        boost::call_once([&]{SM_CONFIGURE::m_instance.reset(new SM_CONFIGURE());},g_instance_once_flag);
        SM_CONFIGURE::m_instance->reload();
    }


    /**
     * @brief Get execute path
     * @return
     */
    std::string SM_CONFIGURE::get_execute_path()
    {
        return this->m_execute_path;
    }
	
	/**
	 * @brief Get execute path
	 * @return
	 */
	std::string SM_CONFIGURE::get_log_path()
    {
        return this->m_user_path.output.log;
    }

    /**
     * @brief Get task configure database path
     * @return
     */
    std::string SM_CONFIGURE::get_task_configure_path()
    {
        return ZOO_get_pl_cfg_base_dir();
    }
       
    /**
     * @brief Get tasks 
     * @return
     */
    std::vector<ZOO_TASK_STRUCT *> SM_CONFIGURE::get_tasks()
    {
    	std::vector<ZOO_TASK_STRUCT *> tasks;
        for(int i = 0; i < this->m_cfg_tasks.number ; i++)
        {
            tasks.push_back(&this->m_cfg_tasks.task[i]);
        }    
		return tasks;
    }

	/**
     * @brief get_config_base_path
     * @return
     */
	std::string SM_CONFIGURE::get_config_base_path()
    {
        return ZOO_get_pl_cfg_base_dir();
    }

    /**
     * @brief Get db base 
     * @return
     */
    std::string SM_CONFIGURE::get_db_base_path()
    {
        return this->m_user_path.output.db;
    }
    
    /**
     * @brief Reload configure
     * @return
     */
    void SM_CONFIGURE::reload()
    {
        this->m_execute_path = ZOO_COMMON::ENVIRONMENT_UTILITY_CLASS::get_execute_path();
        if(OK != ZOO_get_user_files_path(&this->m_user_path))
        {
            LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__," :: get user file path ...ERROR ");
		}
		else
		{
		    this->load_usr_library_path(this->m_user_path.configure.lib);
		}

		if(OK != ZOO_get_entity_tasks(&this->m_cfg_tasks))
		{
            LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,"::get configure task failed ");
		}
    }
    
    void SM_CONFIGURE::load_usr_library_path(std::string lib_path)
    {
        /*LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,"::input parameter lib_path is %s",lib_path.c_str());
        this->m_lib_path_env = getenv("LD_LIBRARY_PATH");
        if(this->m_lib_path_env.empty())
        {   
            this->m_lib_path_env = "LD_LIBRARY_PATH=";
            this->m_lib_path_env.append(lib_path);
        }
        else if(this->m_lib_path_env == "LD_LIBRARY_PATH")
        {
            this->m_lib_path_env.append("=").append(lib_path);
        }
        else if(this->m_lib_path_env.find(lib_path) == std::string::npos)
        {
            this->m_lib_path_env.append(lib_path);
        }
        else
        {
            
        }
        
        LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,"::set LD_LIBRARY_PATH is %s",this->m_lib_path_env.c_str());
        if(ENOMEM == putenv((char*)this->m_lib_path_env.data()))
        {
            LOG_MANAGER_CLASS::debug(__FUNCTION_NAME__,"::set LD_LIBRARY_PATH failed");
        }*/
    }
}
