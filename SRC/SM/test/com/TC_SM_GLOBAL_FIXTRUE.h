/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : SM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/
extern "C"
{
    #include <SM4A_if.h>
    #include <SM4A_type.h>
    #include <MM4A_if.h>
}
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/output_test_stream.hpp>
#include <boost/test/results_reporter.hpp>
#include <boost/test/unit_test_log.hpp>
#include <string>
/**
* @brief Define a test suite entry/exit,so that the setup function is called only once
* upon entering the test suite.
*/
struct TC_SM_GOLBAL_FIXTURE
{
    TC_SM_GOLBAL_FIXTURE()
    {
        BOOST_TEST_MESSAGE("TC global fixture initialize ...");
        MM4A_initialize();
    }

    ~TC_SM_GOLBAL_FIXTURE()
    {
        BOOST_TEST_MESSAGE("TC teardown ...");
        MM4A_terminate();
    }
};

BOOST_TEST_GLOBAL_FIXTURE(TC_SM_GOLBAL_FIXTURE);


/**
 * @brief Define test report output formate, default --log_level=message.
*/
struct TC_SM_REPORTER
{
    TC_SM_REPORTER():reporter("../../../reporter/SM_TC_result.reporter")
    {
        boost::unit_test::unit_test_log.set_stream( reporter );
        boost::unit_test::unit_test_log.set_threshold_level(boost::unit_test::log_test_units);
    }

    ~TC_SM_REPORTER()
    {
        boost::unit_test::unit_test_log.set_stream( std::cout );;
    }
   std::ofstream reporter;
};

BOOST_TEST_GLOBAL_CONFIGURATION(TC_SM_REPORTER);

