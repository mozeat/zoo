/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : SM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/
#define BOOST_TEST_MODULE SM_test_module 
#define BOOST_TEST_DYN_LINK 
#include <boost/test/unit_test.hpp>
#include <boost/test/unit_test_log.hpp>
#include <boost/test/unit_test_suite.hpp>
#include <boost/test/framework.hpp>
#include <boost/test/unit_test_parameters.hpp>
#include <boost/test/utils/nullstream.hpp>
/**
 * @brief Execute <SM> UTMF ...
 * @brief <Global Fixture> header file can define the global variable.
 * @brief modify the include header files sequence to change the unit test execution sequence but not case SHUTDOWN.
 */
#include <TC_SM_GLOBAL_FIXTRUE.h>
#include <TC_SM_INITIALIZE_001.h>
//#include <TC_SM_START_ALL_TASKS_002.h>
//#include <TC_SM_STOP_ALL_TASKS_003.h>
//#include <TC_SM_START_SINGLE_TASK_004.h>
//#include <TC_SM_STOP_SINGLE_TASK_005.h>
//#include <TC_SM_GET_TASK_STATE_006.h>
#include <TC_SM_GET_HOST_INFO_007.h>

/**
 * @brief This case should be executed at the end of all test cases to exit current process.
 * @brief Close all this process events subscribe.
 * @brief Close message queue context thread.
 */
//#include <TC_SM_SHUTDOWN.h>
