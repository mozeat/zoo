/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : SM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/
extern "C"
{
    #include <SM4A_if.h>
    #include <SM4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_SM_START_SINGLE_TASK_004)

/**
 *@brief 
 *@param name[SM_TASK_NAME_LENGHT]
 *@param stack_size
 *@param pid
**/
    BOOST_AUTO_TEST_CASE( TC_SM_START_SINGLE_TASK_004_001 )
    {
        ZOO_CHAR *name = "TR";
        ZOO_INT32 stack_size = 20;
        ZOO_UINT32  pid = -1;
        BOOST_TEST(SM4A_start_single_task(name,stack_size,&pid) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()