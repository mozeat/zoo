/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : SM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/
extern "C"
{
    #include <SM4A_if.h>
    #include <SM4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_SM_GET_TASK_STATE_006)

/**
 *@brief 
 *@param name[SM_TASK_NAME_LENGHT]
 *@param state
**/
    BOOST_AUTO_TEST_CASE( TC_SM_GET_TASK_STATE_006_001 )
    {
        char name[SM_TASK_NAME_LENGHT] = "TR";
        ZOO_TASK_STATE_ENUM state;
        BOOST_TEST(SM4A_get_task_state(name,&state) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()