/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : SM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/
extern "C"
{
    #include <SM4A_if.h>
    #include <SM4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_SM_STOP_ALL_TASKS_003)

/**
 *@brief 
**/
    BOOST_AUTO_TEST_CASE( TC_SM_STOP_ALL_TASKS_003_001 )
    {
        BOOST_TEST(SM4A_stop_all_tasks() == OK);
    }

BOOST_AUTO_TEST_SUITE_END()