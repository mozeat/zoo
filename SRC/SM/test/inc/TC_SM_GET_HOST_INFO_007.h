/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : SM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/
extern "C"
{
    #include <SM4A_if.h>
    #include <SM4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_SM_GET_HOST_INFO_007)

/**
 *@brief 
 *@param host_info
**/
    BOOST_AUTO_TEST_CASE( TC_SM_GET_HOST_INFO_007_001 )
    {
        SM4A_HOST_INFO_STRUCT host_info = SM4A_HOST_INFO_STRUCT();
        
        BOOST_TEST(SM4A_get_host_info(&host_info) == OK);
        BOOST_TEST_MESSAGE("host_id:" << host_info.host_id);
        BOOST_TEST_MESSAGE("host_name:" << host_info.host_name);
    }

BOOST_AUTO_TEST_SUITE_END()