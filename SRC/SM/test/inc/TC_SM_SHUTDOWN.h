/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : SM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/
extern "C"
{
    #include <SM4A_if.h>
    #include <SM4A_type.h>
    #include <MM4A_if.h>
    #include <MQ4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_SM_SHUTDOWN_001)

    BOOST_AUTO_TEST_CASE( TC_SM_SHUTDOWN_001_001 )
    {
        BOOST_TEST(MQ4A_shutdown_all_clients() == OK);
    }

BOOST_AUTO_TEST_SUITE_END()