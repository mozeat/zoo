include ../Makefile_tpl_cov
include ../Project_config

TARGET   := utmf_SMMA
SRCEXTS  := .cpp .c 
INCDIRS  := ./inc ./com ./test/inc ./test/com
SOURCES  := 
SRCDIRS  := ./test/lib ./test/bin
CFLAGS   := 
CXXFLAGS := -std=c++14 -fstack-protector-all
CPPFLAGS := -DBOOST_ALL_DYN_LINK
LDFPATH  := -L$(THIRD_PARTY_LIBRARY_PATH)
LDFLAGS  := $(GCOV_LINK) $(LDFPATH) -lSM4A -lMM4A -lMQ4A -lZOO  -lboost_unit_test_framework

include ../Makefile_tpl_linux