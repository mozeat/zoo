/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : TM
 * File Name    : SM4A_if.h
 * Description  :
 * HistTMy :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef SM4A_IF_H
#define SM4A_IF_H
#include <ZOO.h>
#include <ZOO_tc.h>
#include <SM4A_type.h>

/**************************************************************************
INTERFACE <ZOO_INT32 SM4A_load_platform_configurations>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
                none
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                SM4A_SYSTEM_ERR -- 系统错误
                SM4A_PARAMETER_ERR -- 参数错误
<Description>:  1.根据zoo.json & project_for_zoo.json，创建任务表
			    2.创建配置路径等；
			    3.自动终止配置的所有进程
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 SM4A_initialize();

/**************************************************************************
INTERFACE <ZOO_INT32 SM4A_start_all_tasks>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
                none
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                SM4A_SYSTEM_ERR -- 系统错误
                SM4A_PARAMETER_ERR -- 参数错误
<Description>:  启动平台配置的所有的任务或者进程，如果任务已经启动则先停止，再重启
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 SM4A_start_all_tasks();

/**************************************************************************
INTERFACE <ZOO_INT32 SM4A_stop_all_tasks>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
                none
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                SM4A_SYSTEM_ERR -- 系统错误
                SM4A_PARAMETER_ERR -- 参数错误
<Description>:  终止平台配置的所有的任务或者进程
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 SM4A_stop_all_tasks();

/**************************************************************************
INTERFACE <ZOO_INT32 SM4A_start_single_task>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
                none
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                SM4A_SYSTEM_ERR    -- 系统错误
                SM4A_PARAMETER_ERR -- 参数错误
				SM4A_TIMEOUT_ERR   -- 超时错误
<Description>:  启动单个进程或者任务
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 SM4A_start_single_task(IN ZOO_CHAR name[SM_TASK_NAME_LENGHT],IN ZOO_INT32 stack_size,OUT ZOO_UINT32 * pid);

/**************************************************************************
INTERFACE <ZOO_INT32 SM4A_stop_single_task>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
                none
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                SM4A_SYSTEM_ERR    -- 系统错误
                SM4A_PARAMETER_ERR -- 参数错误
				SM4A_TIMEOUT_ERR   -- 超时错误
<Description>:  终止单个进程或者任务
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 SM4A_stop_single_task(IN char name[SM_TASK_NAME_LENGHT]);

/**************************************************************************
INTERFACE <ZOO_INT32 SM4A_get_task_state>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
                none
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                SM4A_SYSTEM_ERR    -- 系统错误
                SM4A_PARAMETER_ERR -- 参数错误
				SM4A_TIMEOUT_ERR   -- 超时错误
<Description>:  获取任务的状态
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 SM4A_get_task_state(IN char name[SM_TASK_NAME_LENGHT],OUT ZOO_TASK_STATE_ENUM * state);

/**************************************************************************
INTERFACE <ZOO_INT32 SM4A_get_host_info>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
                none
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功
                SM4A_SYSTEM_ERR    -- 系统错误
                SM4A_PARAMETER_ERR -- 参数错误
				SM4A_TIMEOUT_ERR   -- 超时错误
<Description>:  获取工作站信息
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 SM4A_get_host_info(INOUT SM4A_HOST_INFO_STRUCT * host_info);

#endif
