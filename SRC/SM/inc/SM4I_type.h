/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : SM4I_type.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-04-02    Generator      created
*************************************************************/
#ifndef SM4I_TYPE_H
#define SM4I_TYPE_H
#include <MQ4A_type.h>
#include "SM4A_type.h"
#include "SM4A_if.h"


/**
 *@brief Macro Definitions
**/
#define SM4I_COMPONET_ID "SM"
#define SM4A_SERVER     "SM4A_SERVER"
#define SM4I_BUFFER_LENGTH    256
#define SM4I_RETRY_INTERVAL   3

/**
 *@brief Function Code Definitions
**/
#define SM4A_INITIALIZE_CODE 0x534dff00
#define SM4A_START_ALL_TASKS_CODE 0x534dff01
#define SM4A_STOP_ALL_TASKS_CODE 0x534dff02
#define SM4A_START_SINGLE_TASK_CODE 0x534dff03
#define SM4A_STOP_SINGLE_TASK_CODE 0x534dff04
#define SM4A_GET_TASK_STATE_CODE 0x534dff05
#define SM4A_GET_HOST_INFO_CODE 0x534dff06

/*Request and reply header struct*/
typedef struct
{
    MQ4A_SERV_ADDR reply_addr;
    ZOO_UINT32 msg_id;
    ZOO_BOOL reply_wanted;
    ZOO_UINT32 func_id;
}SM4I_REPLY_HANDLER_STRUCT;

typedef  SM4I_REPLY_HANDLER_STRUCT * SM4I_REPLY_HANDLE;

/*Request message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL need_reply;
}SM4I_REQUEST_HEADER_STRUCT;

/*Reply message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL execute_result;
}SM4I_REPLY_HEADER_STRUCT;

/**
*@brief SM4I_INITIALIZE_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}SM4I_INITIALIZE_CODE_REQ_STRUCT;

/**
*@brief SM4I_START_ALL_TASKS_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}SM4I_START_ALL_TASKS_CODE_REQ_STRUCT;

/**
*@brief SM4I_STOP_ALL_TASKS_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}SM4I_STOP_ALL_TASKS_CODE_REQ_STRUCT;

/**
*@brief SM4I_START_SINGLE_TASK_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_INT32 stack_size;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR name[SM_TASK_NAME_LENGHT];
    ZOO_CHAR g_filler[8];
}SM4I_START_SINGLE_TASK_CODE_REQ_STRUCT;

/**
*@brief SM4I_STOP_SINGLE_TASK_CODE_REQ_STRUCT
**/
typedef struct 
{
    char name[SM_TASK_NAME_LENGHT];
    ZOO_CHAR g_filler[8];
}SM4I_STOP_SINGLE_TASK_CODE_REQ_STRUCT;

/**
*@brief SM4I_GET_TASK_STATE_CODE_REQ_STRUCT
**/
typedef struct 
{
    char name[SM_TASK_NAME_LENGHT];
    ZOO_CHAR g_filler[8];
}SM4I_GET_TASK_STATE_CODE_REQ_STRUCT;

/**
*@brief SM4I_GET_HOST_INFO_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}SM4I_GET_HOST_INFO_CODE_REQ_STRUCT;

/**
*@brief SM4I_INITIALIZE_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}SM4I_INITIALIZE_CODE_REP_STRUCT;

/**
*@brief SM4I_START_ALL_TASKS_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}SM4I_START_ALL_TASKS_CODE_REP_STRUCT;

/**
*@brief SM4I_STOP_ALL_TASKS_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}SM4I_STOP_ALL_TASKS_CODE_REP_STRUCT;

/**
*@brief SM4I_START_SINGLE_TASK_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_UINT32 pid;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}SM4I_START_SINGLE_TASK_CODE_REP_STRUCT;

/**
*@brief SM4I_STOP_SINGLE_TASK_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}SM4I_STOP_SINGLE_TASK_CODE_REP_STRUCT;

/**
*@brief SM4I_GET_TASK_STATE_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_TASK_STATE_ENUM state;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}SM4I_GET_TASK_STATE_CODE_REP_STRUCT;

/**
*@brief SM4I_GET_HOST_INFO_CODE_REP_STRUCT
**/
typedef struct 
{
    SM4A_HOST_INFO_STRUCT host_info;
    ZOO_CHAR g_filler[8];
}SM4I_GET_HOST_INFO_CODE_REP_STRUCT;

typedef struct
{
    SM4I_REQUEST_HEADER_STRUCT request_header;
    union
    {
        SM4I_INITIALIZE_CODE_REQ_STRUCT initialize_req_msg;
        SM4I_START_ALL_TASKS_CODE_REQ_STRUCT start_all_tasks_req_msg;
        SM4I_STOP_ALL_TASKS_CODE_REQ_STRUCT stop_all_tasks_req_msg;
        SM4I_START_SINGLE_TASK_CODE_REQ_STRUCT start_single_task_req_msg;
        SM4I_STOP_SINGLE_TASK_CODE_REQ_STRUCT stop_single_task_req_msg;
        SM4I_GET_TASK_STATE_CODE_REQ_STRUCT get_task_state_req_msg;
        SM4I_GET_HOST_INFO_CODE_REQ_STRUCT get_host_info_req_msg;
     }request_body;
}SM4I_REQUEST_STRUCT;


typedef struct
{
    SM4I_REPLY_HEADER_STRUCT reply_header;
    union
    {
        SM4I_INITIALIZE_CODE_REP_STRUCT initialize_rep_msg;
        SM4I_START_ALL_TASKS_CODE_REP_STRUCT start_all_tasks_rep_msg;
        SM4I_STOP_ALL_TASKS_CODE_REP_STRUCT stop_all_tasks_rep_msg;
        SM4I_START_SINGLE_TASK_CODE_REP_STRUCT start_single_task_rep_msg;
        SM4I_STOP_SINGLE_TASK_CODE_REP_STRUCT stop_single_task_rep_msg;
        SM4I_GET_TASK_STATE_CODE_REP_STRUCT get_task_state_rep_msg;
        SM4I_GET_HOST_INFO_CODE_REP_STRUCT get_host_info_rep_msg;
    }reply_body;
}SM4I_REPLY_STRUCT;


#endif //SM4I_type.h
