include ../Makefile_tpl_cov
include ../Project_config

TARGET   := RCMA
SRCEXTS  := .cpp .c 
INCDIRS  := ./inc ./com
SOURCES  := 
SRCDIRS  := ./bin ./lib
CFLAGS   := -std=gnu11
CXXFLAGS := -std=c++14 -fstack-protector-all
CPPFLAGS := -DBOOST_ALL_DYN_LINK
LDFPATH  := -L$(THIRD_PARTY_LIBRARY_PATH)
LDFLAGS  := $(GCOV_LINK) $(LDFPATH) -lTR4A -lEH4A -lMM4A -lMQ4A -lDB4A -lZOO -lpaho-mqtt3cs -lssl -lcrypto

include ../Makefile_tpl_linux
