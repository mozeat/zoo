/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : RCMA_event.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
#ifndef RCMA_EVENT_H
#define RCMA_EVENT_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
//#include <EH4A_if.h>
//#include <TR4A_if.h>
#include "RC4I_type.h"
#include "RC4I_if.h"
#include "RC4A_type.h"

/**
 *@brief RCMA_raise_4A_connect
 *@param option
 *@param fd
**/
ZOO_EXPORT ZOO_INT32 RCMA_raise_4A_connect(IN ZOO_INT32 error_code,
                                               IN ZOO_FD fd,
                                               IN RC4I_REPLY_HANDLE reply_handle);

/**
 *@brief RCMA_raise_4A_disconnect
 *@param fd
**/
ZOO_EXPORT ZOO_INT32 RCMA_raise_4A_disconnect(IN ZOO_INT32 error_code,
                                                  IN RC4I_REPLY_HANDLE reply_handle);

/**
 *@brief RCMA_raise_4A_get_fd
 *@param client_id
 *@param fd
**/
ZOO_EXPORT ZOO_INT32 RCMA_raise_4A_get_fd(IN ZOO_INT32 error_code,
                                              IN ZOO_FD fd,
                                              IN RC4I_REPLY_HANDLE reply_handle);

/**
 *@brief RCMA_raise_4A_get_connected
 *@param fd
 *@param is_connected
**/
ZOO_EXPORT ZOO_INT32 RCMA_raise_4A_get_connected(IN ZOO_INT32 error_code,
                                                     IN ZOO_BOOL is_connected,
                                                     IN RC4I_REPLY_HANDLE reply_handle);

/**
 *@brief RCMA_raise_4A_set_subscribe_topic
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
**/
ZOO_EXPORT ZOO_INT32 RCMA_raise_4A_set_subscribe_topic(IN ZOO_INT32 error_code,
                                                           IN RC4I_REPLY_HANDLE reply_handle);

/**
 *@brief RCMA_raise_4A_remove_subscribe_topic
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
**/
ZOO_EXPORT ZOO_INT32 RCMA_raise_4A_remove_subscribe_topic(IN ZOO_INT32 error_code,
                                                              IN RC4I_REPLY_HANDLE reply_handle);

/**
 *@brief RCMA_raise_4A_clear_db
 *@param fd
**/
ZOO_EXPORT ZOO_INT32 RCMA_raise_4A_clear_db(IN ZOO_INT32 error_code,
                                                IN RC4I_REPLY_HANDLE reply_handle);

/**
 *@brief RCMA_raise_4A_publish
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
 *@param message[RC4A_MESSAGE_LENGTH]
 *@param message_length
 *@param message_id
 *@param message_number
**/
ZOO_EXPORT ZOO_INT32 RCMA_raise_4A_publish(IN ZOO_INT32 error_code,
                                               IN RC4I_REPLY_HANDLE reply_handle);

/**
 *@brief RC4A_connect_status_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
ZOO_EXPORT void RCMA_raise_4A_connect_status_subscribe(IN RC4A_CONNECTION_STATUS_STRUCT status,IN ZOO_INT32 error_code,IN void *context);

/**
 *@brief RC4A_message_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
ZOO_EXPORT void RCMA_raise_4A_message_subscribe(IN RC4A_MESSAGE_STRUCT * status,IN ZOO_INT32 error_code,IN void *context);


#endif // RCMA_event.h
