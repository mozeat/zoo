/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : MARKING_MODEL_INTERFACE.hpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef MARKING_MODEL_INTERFACE_HPP
#define MARKING_MODEL_INTERFACE_HPP
#include "NOTIFY_PROPERTY_CHANGED_INTERFACE.h"
#include "PROPERTY_CHANGED_OBSERVER_INTERFACE.h"
#include "RC_COMMON_MACRO_DEFINE.h"
#include <boost/shared_ptr.hpp>

namespace RC
{
    class MARKING_MODEL_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       MARKING_MODEL_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~MARKING_MODEL_INTERFACE(){}
    public:
        /*
         * @brief Get marking code
         * @return marking code
        **/
        std::string get_marking_code()
        {
            return this->m_marking_code;
        }

        /*
         * @brief Set marking code
         * @return marking code
        **/
        void set_marking_code(std::string marking_code)
        {
            this->m_marking_code = marking_code;
        }

        /*
         * @brief Get pointer
         * @return pointer
        **/
        template<typename T>
        boost::shared_ptr<T> get_pointer()
        {
            if (NULL == this->m_pointer)
            {
                this->m_pointer.reset(this);
            }
            return boost::dynamic_pointer_cast<T>(this->m_pointer);
        }

        /*
         * @brief Get pointer
         * @return pointer
        **/
        template<typename T>
        static boost::shared_ptr<T> get_pointer(MARKING_MODEL_INTERFACE* marking_model)
        {
            return boost::dynamic_pointer_cast<T>(marking_model->get_pointer<T>());
        }

    protected:
        /*
         * @brief The marking_code attribute.
        **/
        std::string m_marking_code;

        /*
         * @brief Current pointer instance.
        **/
         boost::shared_ptr<MARKING_MODEL_INTERFACE> m_pointer;

    };
}
#endif
