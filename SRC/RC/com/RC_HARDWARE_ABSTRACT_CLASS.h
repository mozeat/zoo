/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_HARDWARE_ABSTRACT_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef RC_HARDWARE_ABSTRACT_CLASS_H
#define RC_HARDWARE_ABSTRACT_CLASS_H

extern "C" 
{
    #include <ZOO.h>
    #include <RC4A_type.h>
}
#include "RC_COMMON_MACRO_DEFINE.h"
#include "MARKING_MODEL_INTERFACE.hpp"
#include "RC_HARDWARE_SERVICE_INTERFACE.h"
#include <boost/shared_ptr.hpp>

namespace RC
{
    class RC_HARDWARE_ABSTRACT_CLASS: public virtual RC_HARDWARE_SERVICE_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       RC_HARDWARE_ABSTRACT_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~RC_HARDWARE_ABSTRACT_CLASS();
    public:
       /*
        * @brief brief Add observer will be notified when property changed
        * @param observer  Property changed observer
       **/ 
       void add_observer(IN PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer);

       /*
        * @brief Add observer will be notified when property changed
        * @param observer  Property changed observer
       **/ 
       void remove_observer(IN PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer);

       /*
        * @brief Clean
       **/ 
       virtual void clean();

      /*
       * @brief This method is executed when property changed value
       * @param model The source object contains property changed
       * @param property_name The property has been changed value
       **/ 
      void notify_of_property_changed(IN const ZOO_UINT32 property_name);

       /*
        * @brief Initialize the hardware.
       **/ 
       virtual void initialize();

       /*
        * @brief Terminate the hardware.
       **/ 
       virtual void terminate();

       void set_fd(ZOO_INT32 fd);
   protected:
        ZOO_INT32 m_fd;
    private:
       /*
        * @brief The list of observer instances will be notified when property has been changed
       **/ 
       std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>*> m_observers;

    };
}// namespace RC

#endif
