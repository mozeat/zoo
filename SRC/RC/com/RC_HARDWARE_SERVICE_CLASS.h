/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_HARDWARE_SERVICE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef RC_HARDWARE_SERVICE_CLASS_H
#define RC_HARDWARE_SERVICE_CLASS_H

#include "RC_HARDWARE_SERVICE_INTERFACE.h"
#include "RC_HARDWARE_ABSTRACT_CLASS.h"

namespace RC
{

    
    class RC_HARDWARE_SERVICE_CLASS : public virtual RC_HARDWARE_SERVICE_INTERFACE,public virtual RC_HARDWARE_ABSTRACT_CLASS 
    {
    public:
       /*
        * @brief Constructor
       **/ 
       RC_HARDWARE_SERVICE_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~RC_HARDWARE_SERVICE_CLASS();
    public:

    
       /*
        * @brief Create an instance.
        * @brief url
        * @brief client_id
       **/ 
       void create(std::string url,std::string client_id);
        
       /*
        * @brief connect to broker.
        * @brief connect_option
        * @brief ssl_option
       **/ 
       void connect(RC4A_CONNECT_OPTION_STRUCT * connect_option,RC4A_SSL_OPTION_STRUCT * ssl_option);

       /*
        * @brief Disconnect.
        **/ 
       void disconnect();

       /*
        * @brief The connect state.
        **/ 
       ZOO_BOOL is_connected();
       
       /*
        * @brief publish.
        * @brief topic
        * @brief message
        * @brief qos
        **/ 
       void publish(std::string & topic,std::string & message); 

       /*
        * @brief Get subscribe message.
        * @brief topic
       **/
       boost::shared_ptr<MESSAGE_CLASS> get_message();
       
       /*
        * @brief set subscribe topic
        **/ 
       void set_subscribe_topic(IN std::string & topic);

       /*
        * @brief remove subscribe topic
        **/ 
       void remove_subscribe_topic(IN std::string & topic);

       /*
        * @brief the handler for execute connection lost callback
        * @param casue 
        **/ 
       void connection_lost_callback(char* cause);

       /*
        * @brief the handler for execute message_arrived callback
        **/ 
       void message_arrived_callback(std::string topic, std::string & message);
    private:
        
        /*
         * @brief the callback function for connection lost.
         * @brief context
         * @brief cause
         **/ 
        friend void on_connection_lost(void *context, char* cause);

        /*
         * @brief the callback function for subscribe message
         * @brief context
         * @brief topicName
         * @brief topicLen
         * @brief message
         **/ 
        friend int on_message_arrived(void* context, char* topicName, int topicLen, MQTTClient_message* message);

        /*
         * @brief the callback function for trace.
         * @brief level
         * @brief message
         **/ 
        void on_trace_message(MQTTCLIENT_TRACE_LEVELS level, char* message);

        /*
         * @brief the callback function for trace.
         * @brief success   TRUE - connection  success,FALSE -lost
         **/ 
        void notify_connection_state(ZOO_BOOL online = ZOO_TRUE);

        /*
         * @brief the callback function for trace.
         * @brief success
         **/ 
        void reconnect();
    private:

        /**
         * @brief mqtt instance attribute
         */
        MQTTClient m_mqtt_handle;

        /**
         * @brief client id attribute
         */
        std::string m_client_id;

        /**
         * @brief Message quality of service
         */
        ZOO_INT32 m_qos;

        /**
         * @brief Mqtt connect
         */
        MQTTClient_connectOptions  m_connect_option;
    };

    /**
     * @brief the callback for mqtt connection lost 
     * @param context    the usr data
     * @param cause 
     */
    void on_connection_lost(void *context, char* cause);

    /**
     * @brief the callback for mqtt message arrivated  
     * @param context    the usr data
     * @param topicName  
     * @param topicLen  
     * @param message   
     */
    int on_message_arrived(void* context, char* topicName, int topicLen, MQTTClient_message* message);
} //namespace RC
#endif
