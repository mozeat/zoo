/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : DEVICE_CONTROLLER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef DEVICE_CONTROLLER_CLASS_H
#define DEVICE_CONTROLLER_CLASS_H

#include "CONTROLLER_ABSTRACT_CLASS.h"
#include "DEVICE_CONTROLLER_INTERFACE.h"

namespace RC
{
    class RC_MODEL_CLASS;
    class DEVICE_CONTROLLER_CLASS: public virtual CONTROLLER_ABSTRACT_CLASS, public virtual DEVICE_CONTROLLER_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       DEVICE_CONTROLLER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~DEVICE_CONTROLLER_CLASS();
    public:
        /*
         * @brief RC4A_connect
         **/
        void connect(IN RC4A_OPTION_STRUCT * option, INOUT ZOO_FD * fd);

        /*
         * @brief RC4A_disconnect
         **/
        void disconnect(IN ZOO_FD fd);

         /*
         * @brief get fd
        **/
        void get_fd(ZOO_CHAR * client_id,ZOO_FD *fd);
         
        /*
         * @brief RC4A_get_connected
         **/
        ZOO_BOOL get_connected(IN ZOO_FD fd);

        /*
         * @brief RC4A_set_subscribe_topic
         **/
        void set_subscribe_topic(IN ZOO_FD fd,IN std::string & topic);

        /*
         * @brief RC4A_remove_subscribe_topic
         **/
        void remove_subscribe_topic(IN ZOO_FD fd,IN std::string & topic);
        
        /*
         * @brief RC4A_clear_db
         **/
        void clear_db(IN ZOO_FD fd);

        /*
         * @brief RC4A_publish
         **/
        void publish(IN ZOO_FD fd,IN std::string & topic,
        									    IN std::string & message,
        									    IN ZOO_INT32 message_id,
        									    IN ZOO_INT32 message_number);
       /*
        * @brief Notify property has been changed
        * @property_name     The property has been changed
        **/ 
        OVERRIDE
        void on_property_changed(MARKING_MODEL_INTERFACE* model,const ZOO_UINT32 property_name);
    private:
        ZOO_FD create_model(IN RC4A_OPTION_STRUCT * option);
        
        boost::shared_ptr<RC_MODEL_CLASS>  get_model(IN ZOO_FD fd);
    private:
       /*
        * @brief Define device entities.
       **/ 
       std::map<ZOO_FD,boost::shared_ptr<RC_MODEL_CLASS> > m_entity_devices;
    };
} //namespace RC
#endif
