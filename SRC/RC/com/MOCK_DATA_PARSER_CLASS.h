/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : MOCK_DATA_PARSER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef MOCK_DATA_PARSER_CLASS_H
#define MOCK_DATA_PARSER_CLASS_H
#include "INI_PARSER_ABSTRACT_CLASS.h"
#include "MOCK_DATA_CLASS.h"
#include <map>
namespace RC
{
    class MOCK_DATA_PARSER_CLASS: public virtual INI_PARSER_ABSTRACT_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       MOCK_DATA_PARSER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~MOCK_DATA_PARSER_CLASS();
    public:
       /*
        * @brief Parse mock data from config file
        * @param file   the ini file location
       **/
       std::map<SECTION_TYPE,boost::shared_ptr<MOCK_DATA_CLASS> > parse_mock_data(IN std::string file);

       /*
        * @brief add section name
        * @param name   section_name
       **/
       void add_section_name(std::string name);

       std::string  get_mock_file();
    private: 
       /*
        * @brief Section names 
       **/
       std::vector<std::string> m_section_names;
    };
}
#endif
