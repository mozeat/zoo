/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_FLOW_FACADE_WRAPPER.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef RC_FLOW_FACADE_WRAPPER_H
#define RC_FLOW_FACADE_WRAPPER_H

#ifdef __cplusplus
extern "C" 
{
#endif
    #include "RC4A_type.h"
    #include "RC4I_type.h"
    /*
     * @brief RC4A_connect
    **/
     ZOO_INT32 RC_connect(IN RC4A_OPTION_STRUCT * option, INOUT ZOO_FD * fd);

    /*
     * @brief RC4A_disconnect
    **/
    ZOO_INT32 RC_disconnect(IN ZOO_FD fd);

    ZOO_INT32 RC_get_fd(ZOO_CHAR * client_id,ZOO_FD *fd);
    
    /*
     * @brief RC4A_get_connected
    **/
     ZOO_INT32 RC_get_connected(IN ZOO_FD fd,INOUT ZOO_BOOL * is_connected);

    /*
     * @brief RC4A_set_subscribe_topic
    **/
     ZOO_INT32 RC_set_subscribe_topic(IN ZOO_FD fd,
												                      IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH]);

    /*
     * @brief RC4A_remove_subscribe_topic
    **/
     ZOO_INT32 RC_remove_subscribe_topic(IN ZOO_FD fd,
														                    IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH]);

    /*
     * @brief RC4A_clear_db
    **/
     ZOO_INT32 RC_clear_db(IN ZOO_FD fd);

    /*
     * @brief RC4A_publish
    **/
     ZOO_INT32 RC_publish(IN ZOO_FD fd,
											    IN ZOO_CHAR topic[RC4A_BUFFER_LENGTH],
											    IN ZOO_CHAR message[RC4A_MESSAGE_LENGTH],
											    IN ZOO_INT32 message_length,
											    IN ZOO_INT32 message_index,
											    IN ZOO_INT32 total_messages);

#ifdef __cplusplus
}
#endif
#endif
