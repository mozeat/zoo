/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : PROPERTY_CHANGE_KEY_DEFINE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef PROPERTY_CHANGE_KEY_DEFINE_H
#define PROPERTY_CHANGE_KEY_DEFINE_H
#include <ZOO.h>

/******************************************************************
* Define property key 
*****************************************************************/
#define PROPERTY_CONNECTION_LOST   (0) 
#define PROPERTY_MESSAGE_ARRIVED   (1) 
#define PROPERTY_CONNECTION_SUCCESS (2) 
#define PROPERTY_CHANGE_KEY_4      (3) 
#define PROPERTY_CHANGE_KEY_5      (4) 
#define PROPERTY_CHANGE_KEY_6      (5) 
#define PROPERTY_CHANGE_KEY_7      (6) 
#define PROPERTY_CHANGE_KEY_8      (7) 
#endif
