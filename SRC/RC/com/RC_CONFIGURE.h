/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_CONFIGURE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef RC_CONFIGURE_H
#define RC_CONFIGURE_H

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <utils/ENVIRONMENT_UTILITY_CLASS.h>
#include "ZOO_if.h"

namespace RC
{
    class RC_CONFIGURE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       RC_CONFIGURE();

       /*
        * @brief Destructor
       **/ 
       virtual ~RC_CONFIGURE();
    public:
        /*
         * @brief Get instance
        **/
        static boost::shared_ptr<RC_CONFIGURE> get_instance();

        /*
         * @brief Initialize
        **/
        void initialize();

        /*
         * @brief Reload configurations
        **/
        void reload();

        /*
         * @brief Get execute path.
        **/
        std::string get_execute_path();

        /*
         * @brief Get simulation file path.
        **/
        std::string get_mock_file();

    private:
        /*
         * @brief The instance
        **/
        static boost::shared_ptr<RC_CONFIGURE> m_instance;

        /*
         * @brief Execute base path
        **/
        std::string m_execute_path;

        /*
         * @brief Simulation data file path
        **/
        std::string m_simulation_file_path;

    };
} //namespace RC
#endif
