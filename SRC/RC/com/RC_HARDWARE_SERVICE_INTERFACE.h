/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_HARDWARE_SERVICE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef RC_HARDWARE_SERVICE_INTERFACE_H
#define RC_HARDWARE_SERVICE_INTERFACE_H

extern "C" 
{
    #include "ZOO.h"
    #include "RC4A_type.h"
    #include "MQTTClient.h"
    #include "MQTTClientPersistence.h"
    #include "RCMA_event.h"
}
#include "MESSAGE_CLASS.h"
#include "RC_COMMON_MACRO_DEFINE.h"
#include "MARKING_MODEL_INTERFACE.hpp"
#include "PROPERTY_CHANGE_KEY_DEFINE.h"
#include <boost/shared_ptr.hpp>

namespace RC
{
    class RC_HARDWARE_SERVICE_INTERFACE: public virtual MARKING_MODEL_INTERFACE, public virtual NOTIFY_PROPERTY_CHANGED_INTERFACE<MARKING_MODEL_INTERFACE>
    {
    public:
       /*
        * @brief Constructor
       **/ 
       RC_HARDWARE_SERVICE_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~RC_HARDWARE_SERVICE_INTERFACE(){}
    public:
       virtual void set_fd(ZOO_INT32 fd) = 0;
        
       /*
        * @brief Create an instance.
        * @brief url
        * @brief client_id
       **/ 
       virtual void create(std::string url,std::string client_id) = 0;
        
       /*
        * @brief connect to broker.
        * @brief connect_option
        * @brief ssl_option
       **/ 
       virtual void connect(RC4A_CONNECT_OPTION_STRUCT * connect_option,RC4A_SSL_OPTION_STRUCT * ssl_option) = 0;

       /*
        * @brief Disconnect.
       **/ 
       virtual void disconnect() = 0;

       /*
        * @brief publish.
        * @brief topic
        * @brief message
        * @brief qos
       **/ 
       virtual void publish(std::string & topic,std::string & message) = 0; 

       /*
        * @brief The connect state.
        **/ 
       virtual ZOO_BOOL is_connected() = 0;

       /*
        * @brief set subscribe topic
        **/ 
       virtual void set_subscribe_topic(IN std::string & topic) = 0;

       /*
        * @brief remove subscribe topic
        **/ 
       virtual void remove_subscribe_topic(IN std::string & topic) = 0;
       
       /*
        * @brief Get subscribe message.
        * @brief topic
       **/
       virtual boost::shared_ptr<MESSAGE_CLASS> get_message() = 0;
    };
}// namespace RC
#endif
