/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : PROCESSING_FLOW_FACADE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
#ifndef PROCESSING_FLOW_FACADE_INTERFACE_H
#define PROCESSING_FLOW_FACADE_INTERFACE_H

extern "C" 
{
    #include <ZOO.h>
    #include <RC4A_type.h>
}
#include "RC_COMMON_MACRO_DEFINE.h"
#include "MARKING_MODEL_INTERFACE.hpp"
#include <boost/shared_ptr.hpp>
#include "FLOW_FACADE_INTERFACE.h"

namespace RC
{
    class PROCESSING_FLOW_FACADE_INTERFACE : public virtual FLOW_FACADE_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       PROCESSING_FLOW_FACADE_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~PROCESSING_FLOW_FACADE_INTERFACE(){}
    public:
        /*
         * @brief RC4A_connect
        **/
        virtual ZOO_INT32 connect(IN RC4A_OPTION_STRUCT * option, INOUT ZOO_FD * fd) = 0;

        /*
         * @brief RC4A_disconnect
        **/
        virtual ZOO_INT32 disconnect(IN ZOO_FD fd) = 0;

        /*
         * @brief get fd
        **/
        virtual ZOO_INT32 get_fd(ZOO_CHAR * client_id,ZOO_FD *fd) = 0;
        
        /*
         * @brief RC4A_get_connected
        **/
        virtual ZOO_INT32 get_connected(IN ZOO_FD fd,INOUT ZOO_BOOL * is_connected) = 0;

        /*
         * @brief RC4A_set_subscribe_topic
        **/
        virtual ZOO_INT32 set_subscribe_topic(IN ZOO_FD fd,
												                      IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH]) = 0;

        /*
         * @brief RC4A_remove_subscribe_topic
        **/
        virtual ZOO_INT32 remove_subscribe_topic(IN ZOO_FD fd,
														                    IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH]) = 0;

        /*
         * @brief RC4A_clear_db
        **/
        virtual ZOO_INT32 clear_db(IN ZOO_FD fd) = 0;

        /*
         * @brief RC4A_publish
        **/
        virtual ZOO_INT32 publish(IN ZOO_FD fd,
											    IN ZOO_CHAR topic[RC4A_BUFFER_LENGTH],
											    IN ZOO_CHAR message[RC4A_MESSAGE_LENGTH],
											    IN ZOO_INT32 message_length,
											    IN ZOO_INT32 message_id,
											    IN ZOO_INT32 message_number) = 0;

    };
}// namespace RC

#endif
