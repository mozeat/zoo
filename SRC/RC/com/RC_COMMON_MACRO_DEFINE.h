/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_COMMON_MACRO_DEFINE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef RC_COMMON_MACRO_DEFINE_H
#define RC_COMMON_MACRO_DEFINE_H
#include "ZOO_if.hpp"
#include <exceptions/PARAMETER_EXCEPTION_CLASS.h>

/*
 * @brief Wrapper try
 **/
#define __RC_TRY try

/*
* @brief Define a macro to throw RC exception
* @param error_code        The error code
* @param error_message     The error message
* @param inner_exception   The inner exception,& std::exception
**/
#define __THROW_RC_EXCEPTION(error_code,error_message,inner_exception) \
ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,error_message,NULL); \
throw ZOO_COMMON::PARAMETER_EXCEPTION_CLASS(error_code, error_message, inner_exception)

/*
* @brief Define a macro catch model exception
* @param continue        continue throw exception
**/
#define __RC_CATCH(continue) catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & e) \
{\
    if(continue) throw ZOO_COMMON::PARAMETER_EXCEPTION_CLASS(e.get_error_code(), e.get_error_message(),NULL);\
}

/*
* @brief Define a macro to catch all exception
* @param result
**/
#define __RC_CATCH_ALL(result) catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & e) \
{\
    result = e.get_error_code();\
}\
catch(...){}

#endif
