/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_HARDWARE_MOCKER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef RC_HARDWARE_MOCKER_CLASS_H
#define RC_HARDWARE_MOCKER_CLASS_H

extern "C" 
{
    #include <ZOO.h>
    #include <RC4A_type.h>
}
#include "MOCK_DATA_PARSER_CLASS.h"
#include "RC_CONFIGURE.h"
#include "RC_HARDWARE_SERVICE_INTERFACE.h"
#include "RC_HARDWARE_ABSTRACT_CLASS.h"
namespace RC
{
    class RC_HARDWARE_MOCKER_CLASS: public virtual RC_HARDWARE_SERVICE_INTERFACE,public virtual RC_HARDWARE_ABSTRACT_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       RC_HARDWARE_MOCKER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~RC_HARDWARE_MOCKER_CLASS();
    public:
       /*
        * @brief reload mock data.
       **/ 
       void intialize();

       /*
        * @brief terminate the hardware mock.
       **/ 
       void terminate();

       /*
        * @brief Get mock data.
        * @param section_name       the section name
        * @return mock data
        **/ 
       boost::shared_ptr<MOCK_DATA_CLASS> get_mock_data(IN std::string section_name);
    private:
       /*
        * @brief Mock data map attribute.
       **/ 
       std::map<std::string,boost::shared_ptr<MOCK_DATA_CLASS> > m_mock_datas;
    };
}// namespace RC
#endif
