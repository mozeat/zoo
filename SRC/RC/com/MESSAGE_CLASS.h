/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : MESSAGE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef MESSAGE_CLASS_H
#define MESSAGE_CLASS_H
extern "C"
{
    #include <ZOO.h>
}

#include <string>
namespace RC
{
    class MESSAGE_CLASS
    {
    public:
        MESSAGE_CLASS();
        MESSAGE_CLASS(const std::string & topic,void * message,ZOO_INT32 message_lenght);
        virtual ~MESSAGE_CLASS();
    public:
        void set_message(std::string & topic,void * message,ZOO_INT32 message_lenght);
        const std::string & get_topic();
        void * get_message();
        ZOO_INT32 get_message_length();
    private:     
        std::string m_topic;
        void * m_ptr;
        ZOO_INT32 m_len;
    };
}
#endif