/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : EVENT_PUBLISHER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef EVENT_PUBLISHER_CLASS_H
#define EVENT_PUBLISHER_CLASS_H

extern "C" 
{
    #include <ZOO.h>
    #include <RC4A_type.h>
}
#include "RC_COMMON_MACRO_DEFINE.h"
#include <boost/shared_ptr.hpp>

namespace RC
{
    /*
     *@brief Define publish event to subsriber class.
    **/
    class EVENT_PUBLISHER_CLASS 
    {
    public:
       /*
        * @brief Constructor
       **/ 
       EVENT_PUBLISHER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~EVENT_PUBLISHER_CLASS();
    public:
       /*
        * @brief Publish connect_status changed event to subscriber.
        * @param status
        * @param error_code
        * @param *context
       **/ 
       void publish_RCMA_raise_4A_connect_status_subscribe(RC4A_CONNECTION_STATUS_STRUCT status,ZOO_INT32 error_code,void *context);

       /*
        * @brief Publish message changed event to subscriber.
        * @param status
        * @param error_code
        * @param *context
       **/ 
       void publish_RCMA_raise_4A_message_subscribe(RC4A_MESSAGE_STRUCT *status,ZOO_INT32 error_code,void *context);

    };
} // namespace RC

#endif
