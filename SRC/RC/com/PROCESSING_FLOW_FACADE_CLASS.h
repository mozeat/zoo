/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : PROCESSING_FLOW_FACADE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
#ifndef PROCESSING_FLOW_FACADE_CLASS_H
#define PROCESSING_FLOW_FACADE_CLASS_H

#include "PROCESSING_FLOW_FACADE_INTERFACE.h"
#include "FLOW_FACADE_ABSTRACT_CLASS.h"

namespace RC
{
    class PROCESSING_FLOW_FACADE_CLASS : public virtual PROCESSING_FLOW_FACADE_INTERFACE,
                                         public FLOW_FACADE_ABSTRACT_CLASS 
    {
    public:
       /*
        * @brief Constructor
       **/ 
       PROCESSING_FLOW_FACADE_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~PROCESSING_FLOW_FACADE_CLASS();
    public:
        /*
         * @brief Connect
         * @param option
         * @param fd
        **/
         ZOO_INT32 connect(IN RC4A_OPTION_STRUCT * option, INOUT ZOO_FD * fd);

        /*
         * @brief Disconnect
         * @param fd
        **/
         ZOO_INT32 disconnect(IN ZOO_FD fd);

         /*
         * @brief get fd
        **/
        ZOO_INT32 get_fd(ZOO_CHAR * client_id,ZOO_FD *fd);
         
        /*
         * @brief Get connected
         * @param fd
         * @param is_connected
        **/
         ZOO_INT32 get_connected(IN ZOO_FD fd,INOUT ZOO_BOOL * is_connected);

        /*
         * @brief Set subscribe topic
         * @param fd
         * @param topic[RC4A_BUFFER_LENGTH]
        **/
         ZOO_INT32 set_subscribe_topic(IN ZOO_FD fd,
												                      IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH]);

        /*
         * @brief Remove subscribe topic
         * @param fd
         * @param topic[RC4A_BUFFER_LENGTH]
        **/
         ZOO_INT32 remove_subscribe_topic(IN ZOO_FD fd,
														                    IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH]);

        /*
         * @brief Clear db
         * @param fd
        **/
         ZOO_INT32 clear_db(IN ZOO_FD fd);

        /*
         * @brief Publish
         * @param fd
         * @param topic[RC4A_BUFFER_LENGTH]
         * @param message[RC4A_MESSAGE_LENGTH]
         * @param message_length
         * @param message_id
         * @param message_number
        **/
         ZOO_INT32 publish(IN ZOO_FD fd,
											    IN ZOO_CHAR topic[RC4A_BUFFER_LENGTH],
											    IN ZOO_CHAR message[RC4A_MESSAGE_LENGTH],
											    IN ZOO_INT32 message_length,
											    IN ZOO_INT32 message_id,
											    IN ZOO_INT32 message_number);

       /*
        * @brief This method is executed when property changed value
        * @param model             The model type
        * @param property_name     The property has been changed value
       **/ 
       OVERRIDE
       void on_property_changed(CONTROLLER_INTERFACE * model,const ZOO_UINT32 property_name);

    };
}//namespace RC

#endif
