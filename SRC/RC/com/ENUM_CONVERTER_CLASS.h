/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : ENUM_CONVERTER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef ENUM_CONVERTER_CLASS_H
#define ENUM_CONVERTER_CLASS_H

extern "C" 
{
    #include <RC4A_type.h>
}
#include "converters/ENUM_BASE_CONVERTER_CLASS.h"
#include "PROPERTY_CHANGE_KEY_DEFINE.h"
namespace RC
{
    REGISTER_ENUM_BEGIN(ZOO_DRIVER_STATE_ENUM)
    {
        REGISTER_ENUM(ZOO_DRIVER_STATE_MIN);
        REGISTER_ENUM(ZOO_DRIVER_STATE_UNKNOWN);
        REGISTER_ENUM(ZOO_DRIVER_STATE_IDLE);
        REGISTER_ENUM(ZOO_DRIVER_STATE_BUSY);
        REGISTER_ENUM(ZOO_DRIVER_STATE_TERMINATED);
        REGISTER_ENUM(ZOO_DRIVER_STATE_MAX);
    }
    REGISTER_ENUM_END;

    REGISTER_ENUM_BEGIN(ZOO_SIM_MODE_ENUM)
    {
        REGISTER_ENUM(ZOO_SIM_MODE_MIN);
        REGISTER_ENUM(ZOO_SIM_DISABLE);
        REGISTER_ENUM(ZOO_SIM_MODE_1);
        REGISTER_ENUM(ZOO_SIM_MODE_2);
        REGISTER_ENUM(ZOO_SIM_MODE_3);
        REGISTER_ENUM(ZOO_SIM_MODE_4);
        REGISTER_ENUM(ZOO_SIM_MODE_MAX);
    }
    REGISTER_ENUM_END;

    REGISTER_ENUM_BEGIN(ZOO_RUNNING_MODE_ENUM)
    {
        REGISTER_ENUM(ZOO_RUNNING_MODE_MIN);
        REGISTER_ENUM(ZOO_RUNNING_MODE_DEBUG);
        REGISTER_ENUM(ZOO_RUNNING_MODE_WORK);
        REGISTER_ENUM(ZOO_RUNNING_MODE_MAX);
    }
    REGISTER_ENUM_END;
}//namespace RC
#endif
