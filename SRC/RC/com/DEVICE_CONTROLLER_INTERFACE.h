/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : DEVICE_CONTROLLER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef DEVICE_CONTROLLER_INTERFACE_H
#define DEVICE_CONTROLLER_INTERFACE_H

#include "RC_COMMON_MACRO_DEFINE.h"
#include "RC_MODEL_INTERFACE.h"
#include "CONTROLLER_INTERFACE.h"
#include <map>
namespace RC
{
    class DEVICE_CONTROLLER_INTERFACE: public virtual CONTROLLER_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       DEVICE_CONTROLLER_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~DEVICE_CONTROLLER_INTERFACE(){}
    public:
        /*
         * @brief RC4A_connect
        **/
        virtual void connect(IN RC4A_OPTION_STRUCT * option, INOUT ZOO_FD * fd) = 0;

        /*
         * @brief RC4A_disconnect
        **/
        virtual void disconnect(IN ZOO_FD fd) = 0;

         /*
         * @brief get fd
        **/
        virtual void get_fd(ZOO_CHAR * client_id,ZOO_FD *fd) = 0;
         
        /*
         * @brief RC4A_get_connected
        **/
        virtual ZOO_BOOL get_connected(IN ZOO_FD fd) = 0;

        /*
         * @brief RC4A_set_subscribe_topic
        **/
        virtual void set_subscribe_topic(IN ZOO_FD fd,
												                      IN std::string & topic) = 0;

        /*
         * @brief RC4A_remove_subscribe_topic
        **/
        virtual void remove_subscribe_topic(IN ZOO_FD fd,
														                    IN std::string & topic) = 0;
        /*
         * @brief RC4A_clear_db
        **/
        virtual void clear_db(IN ZOO_FD fd) = 0;

        /*
         * @brief RC4A_publish
        **/
        virtual void publish(IN ZOO_FD fd,
											    IN std::string & topic,
											    IN std::string & message,
											    IN ZOO_INT32 message_id,
											    IN ZOO_INT32 message_number) = 0;
    };
} // namespace RC
#endif
