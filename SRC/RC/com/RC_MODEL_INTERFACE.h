/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_MODEL_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef RC_MODEL_INTERFACE_H
#define RC_MODEL_INTERFACE_H

extern "C" 
{
    #include <ZOO.h>
    #include <RC4A_type.h>
}
#include "RC_COMMON_MACRO_DEFINE.h"
#include "DEVICE_INTERFACE.h"
#include "STATE_MANAGER_CLASS.h"
#include <boost/shared_ptr.hpp>
#include <algorithm>

namespace RC
{
    class RC_MODEL_INTERFACE: public virtual DEVICE_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       RC_MODEL_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~RC_MODEL_INTERFACE(){}
    public:

       virtual void set_fd(ZOO_INT32 fd) = 0;

       virtual ZOO_FD get_fd() = 0;
       
       virtual std::string get_client_id() = 0;
       
       /*
        * @brief Create an mqtt client instance
        * @param address
        * @param port
        * @param persistence_option 
        **/ 
       virtual void create(IN std::string address,
                                    IN ZOO_INT32 port,std::string client_id,
                                    IN RC4A_OFFLINE_PERSISTENCE_OPTION_STRUCT * persistence_option) = 0;
       /*
        * @brief Connect to broker
        * @param connect_option
        * @param ssl_option
        **/ 
       virtual void connect(IN RC4A_CONNECT_OPTION_STRUCT * connect_option,
                                        IN RC4A_SSL_OPTION_STRUCT * ssl_option) = 0;
       
       /*
        * @brief disconnect from broker.
        **/ 
       virtual void disconnect() = 0;

       /*
        * @brief set subscribe topic
        **/ 
       virtual void set_subscribe_topic(IN std::string & topic) = 0;

       /*
        * @brief remove subscribe topic
       **/ 
       virtual void remove_subscribe_topic(IN std::string & topic) = 0;

       /*
        * @brief Is broker online
       **/ 
       virtual  ZOO_BOOL is_online() = 0;
       
       /*
        * @brief Publish message to broker
        * @param topic
        * @param message
        * @param message_id
        * @param message_number
        **/ 
       virtual void publish(IN std::string & topic,
        									    IN std::string & message,
        									    IN ZOO_INT32 message_id,
        									    IN ZOO_INT32 message_number) = 0;
    };
}// namespace RC
#endif
