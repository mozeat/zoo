/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : RCMA_implement.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
#ifndef RCMA_IMPLEMENT_H
#define RCMA_IMPLEMENT_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
//#include <EH4A_if.h>
//#include <TR4A_if.h>
#include <RCMA_event.h>
#include "RC4I_type.h"
#include "RC4A_type.h"


/**
 *@brief RCMA_implement_4A_connect
 *@param option
 *@param fd
**/
ZOO_EXPORT void RCMA_implement_4A_connect(IN RC4A_OPTION_STRUCT* option,
                                              IN RC4I_REPLY_HANDLE reply_handle);
/**
 *@brief RCMA_implement_4A_disconnect
 *@param fd
**/
ZOO_EXPORT void RCMA_implement_4A_disconnect(IN ZOO_FD fd,
                                                 IN RC4I_REPLY_HANDLE reply_handle);
/**
 *@brief RCMA_implement_4A_get_fd
 *@param client_id
 *@param fd
**/
ZOO_EXPORT void RCMA_implement_4A_get_fd(IN ZOO_CHAR* client_id,
                                             IN RC4I_REPLY_HANDLE reply_handle);
/**
 *@brief RCMA_implement_4A_get_connected
 *@param fd
 *@param is_connected
**/
ZOO_EXPORT void RCMA_implement_4A_get_connected(IN ZOO_FD fd,
                                                    IN RC4I_REPLY_HANDLE reply_handle);
/**
 *@brief RCMA_implement_4A_set_subscribe_topic
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
**/
ZOO_EXPORT void RCMA_implement_4A_set_subscribe_topic(IN ZOO_FD fd,
                                                          IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH],
                                                          IN RC4I_REPLY_HANDLE reply_handle);
/**
 *@brief RCMA_implement_4A_remove_subscribe_topic
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
**/
ZOO_EXPORT void RCMA_implement_4A_remove_subscribe_topic(IN ZOO_FD fd,
                                                             IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH],
                                                             IN RC4I_REPLY_HANDLE reply_handle);
/**
 *@brief RCMA_implement_4A_clear_db
 *@param fd
**/
ZOO_EXPORT void RCMA_implement_4A_clear_db(IN ZOO_FD fd,
                                               IN RC4I_REPLY_HANDLE reply_handle);
/**
 *@brief RCMA_implement_4A_publish
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
 *@param message[RC4A_MESSAGE_LENGTH]
 *@param message_length
 *@param message_id
 *@param message_number
**/
ZOO_EXPORT void RCMA_implement_4A_publish(IN ZOO_FD fd,
                                              IN ZOO_CHAR topic[RC4A_BUFFER_LENGTH],
                                              IN ZOO_CHAR message[RC4A_MESSAGE_LENGTH],
                                              IN ZOO_INT32 message_length,
                                              IN ZOO_INT32 message_id,
                                              IN ZOO_INT32 message_number,
                                              IN RC4I_REPLY_HANDLE reply_handle);

#endif // RCMA_implement.h
