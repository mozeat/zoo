/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : DEVICE_INTERFACE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef DEVICE_INTERFACE_H
#define DEVICE_INTERFACE_H

extern "C" 
{
    #include "ZOO.h"
    #include "RC4A_type.h"
    #include "RC4I_type.h"
}
#include "RC_COMMON_MACRO_DEFINE.h"
#include "ENUM_CONVERTER_CLASS.h"
#include "MARKING_MODEL_INTERFACE.hpp"

namespace RC
{
    class DEVICE_INTERFACE: public virtual MARKING_MODEL_INTERFACE
                             ,public virtual PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>
                             ,public virtual NOTIFY_PROPERTY_CHANGED_INTERFACE<MARKING_MODEL_INTERFACE>
    {
    public:
       /*
        * @brief Constructor
       **/ 
       DEVICE_INTERFACE();

       /*
        * @brief Destructor
       **/ 
       virtual ~DEVICE_INTERFACE();
    public:
       /*
        * @brief Add observer will be notified when property changed.
        * @param observer  Property changed observer
       **/ 
       virtual void add_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer);

       /*
        * @brief Remove observer will be notified when property changed.
        * @param observer  Property changed observer
       **/ 
       virtual void remove_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer);

       /*
        * @brief Clean.
       **/ 
       void clean();
       /*
        * @brief Notify property has been changed.
        * @param property_name     The property has been changed
       **/ 
       virtual void notify_of_property_changed(const ZOO_UINT32 property_name);

    protected:
       /*
        * @brief The list of observer instances will be notified when property has been changed.
       **/ 
       std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>*> m_observers;
    };
}// namespace RC

#endif
