/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_MODEL_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef RC_MODEL_CLASS_H
#define RC_MODEL_CLASS_H

#include "RC_MODEL_INTERFACE.h"
#include "DEVICE_MODEL_ABSTRCT_CLASS.h"
#include "RC_HARDWARE_SERVICE_INTERFACE.h"
#include "RC_HARDWARE_MOCKER_CLASS.h"

namespace RC
{
    class RC_MODEL_CLASS : public virtual RC_MODEL_INTERFACE,public virtual DEVICE_MODEL_ABSTRCT_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       RC_MODEL_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~RC_MODEL_CLASS();
    public:

        
        void set_fd(ZOO_INT32 fd);

        ZOO_FD get_fd();

        std::string get_client_id();
        
       /*
        * @brief Create an mqtt client instance
        * @param address
        * @param port
        * @param persistence_option 
        **/ 
       void create(IN std::string address,
                                    IN ZOO_INT32 port,std::string client_id,
                                    IN RC4A_OFFLINE_PERSISTENCE_OPTION_STRUCT * persistence_option);
       /*
        * @brief Connect to broker
        * @param connect_option
        * @param ssl_option
        **/ 
       void connect(IN RC4A_CONNECT_OPTION_STRUCT * connect_option,
                                        IN RC4A_SSL_OPTION_STRUCT * ssl_option);
       
       /*
        * @brief disconnect from broker.
        **/ 
       void disconnect();

       /*
        * @brief Is broker online
       **/ 
       ZOO_BOOL is_online();
       
       /*
        * @brief Publish message to broker
        * @param topic
        * @param message
        * @param message_id
        * @param message_number
        **/ 
       void publish(IN std::string & topic,
        									    IN std::string & message,
        									    IN ZOO_INT32 message_id,
        									    IN ZOO_INT32 message_number);
       /*
        * @brief Notify property has been changed.
        * @param property_name     The property has been changed
        **/ 
       OVERRIDE
       void on_property_changed(MARKING_MODEL_INTERFACE * model,const ZOO_UINT32 property_name);

       /*
        * @brief set subscribe topic
        **/ 
       void set_subscribe_topic(std::string & topic);

       /*
        * @brief remove subscribe topic
       **/ 
       void remove_subscribe_topic(std::string & topic);
    private:
       /*
        * @brief The model hardware service.
        **/ 
       boost::shared_ptr<RC_HARDWARE_SERVICE_INTERFACE> m_hardware_service;

       /*
        * @brief The client id
        **/ 
       std::string m_client_id;

       /*
        * @brief 
        **/ 
       std::map<ZOO_INT32,std::string> m_message_segments;
       
    };
} //namespace RC
#endif
