/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : STATE_MANAGER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef STATE_MANAGER_CLASS_H
#define STATE_MANAGER_CLASS_H

extern "C" 
{
    #include <ZOO.h>
    #include <ZOO_tc.h>
    #include <RC4A_type.h>
}
#include <boost/shared_ptr.hpp>

namespace RC
{
    class STATE_MANAGER_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       STATE_MANAGER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~STATE_MANAGER_CLASS();
    public:
       /*
        * @brief Set driver state
       **/ 
       void set_driver_state(IN ZOO_DRIVER_STATE_ENUM state);

       /*
        * @brief Get driver state
       **/ 
       ZOO_DRIVER_STATE_ENUM  get_driver_state();

       /*
        * @brief Update driver state
       **/ 
       void update_driver_state(IN ZOO_DRIVER_STATE_ENUM state);

       /*
        * @brief Set running mode
       **/ 
       void set_running_mode(IN ZOO_RUNNING_MODE_ENUM running_mode);

       /*
        * @brief Get running mode
       **/ 
       ZOO_RUNNING_MODE_ENUM  get_running_mode();

    private:
       /*
        * @brief The driver state.
       **/ 
       ZOO_DRIVER_STATE_ENUM  m_driver_state;

       /*
        * @brief The running mode.
       **/ 
       ZOO_RUNNING_MODE_ENUM  m_running_mode;

    };
}// namespace RC

#endif
