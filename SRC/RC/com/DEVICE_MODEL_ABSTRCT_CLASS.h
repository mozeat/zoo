/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : DEVICE_MODEL_ABSTRCT_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#ifndef DEVICE_MODEL_ABSTRCT_CLASS_H
#define DEVICE_MODEL_ABSTRCT_CLASS_H
#include "RC_MODEL_INTERFACE.h"
#include "RC_COMMON_MACRO_DEFINE.h"
#include <boost/shared_ptr.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

namespace RC
{
    class DEVICE_MODEL_ABSTRCT_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       DEVICE_MODEL_ABSTRCT_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~DEVICE_MODEL_ABSTRCT_CLASS();
    public:
        std::string create_uuid();
        ZOO_INT32 get_fd();
    protected:
        ZOO_INT32 m_fd;
    };
}
#endif
