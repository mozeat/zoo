/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RCMA_executor_wrapper.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
#ifndef RCMA_EXECUTOR_WRAPPER_H
#define RCMA_EXECUTOR_WRAPPER_H

#ifdef __cplusplus 
extern "C" 
{
#endif

#include <ZOO.h>
#include "RC4I_type.h"

    /**
    *@brief This function response to create factory instance or load environment configurations 
    **/
    ZOO_EXPORT void RCMA_startup(void);

    /**
    *@brief This function response to release instance or memory 
    **/
    ZOO_EXPORT void RCMA_shutdown(void);

    /**
    *@brief Subscribe events published from hardware drivers 
    **/
    ZOO_EXPORT void RCMA_subscribe_driver_event(void);


#ifdef __cplusplus 
 }
#endif

#endif // RCMA_executor_wrapper.h
