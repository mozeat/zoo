include ../Makefile_tpl_cov
include ../Project_config

TARGET   := utmf_RCMA
SRCEXTS  := .cpp .c 
INCDIRS  := ./inc ./com ./test/inc ./test/com
SOURCES  := ./bin/MOCK_DATA_PARSER_CLASS.cpp ./bin/MOCK_DATA_CLASS.cpp ./test/bin/RC_unit_test_main.cpp
SRCDIRS  := 
CFLAGS   := 
CXXFLAGS := -std=c++14 -fstack-protector-all
CPPFLAGS := -DBOOST_ALL_DYN_LINK
LDFPATH  := -L$(THIRD_PARTY_LIBRARY_PATH)
LDFLAGS  := $(GCOV_LINK) $(LDFPATH) -lRC4A -lTR4A -lEH4A -lMM4A -lMQ4A -lZOO -lboost_unit_test_framework

include ../Makefile_tpl_linux