/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#define BOOST_TEST_MODULE RC_test_module 
#define BOOST_TEST_DYN_LINK 
#include <boost/test/unit_test.hpp>
#include <boost/test/unit_test_log.hpp>
#include <boost/test/unit_test_suite.hpp>
#include <boost/test/framework.hpp>
#include <boost/test/unit_test_parameters.hpp>
#include <boost/test/utils/nullstream.hpp>
/**
 * @brief Execute <RC> UTMF ...
 * @brief <Global Fixture> header file can define the global variable.
 * @brief modify the include header files sequence to change the unit test execution sequence but not case SHUTDOWN.
 */
#include <TC_RC_GLOBAL_FIXTRUE.h>
//#include <TC_RC_CONNECT_STATUS_SUBSCRIBE_008.h>
//#include <TC_RC_MESSAGE_SUBSCRIBE_0010.h>
#include <TC_RC_CONNECT_001.h>
//#include <TC_RC_GET_CONNECTED_003.h>
//#include <TC_RC_SET_SUBSCRIBE_TOPIC_004.h>
//#include <TC_RC_REMOVE_SUBSCRIBE_TOPIC_005.h>
//#include <TC_RC_PUBLISH_007.h>
//#include <TC_RC_CLEAR_DB_006.h>
//#include <TC_RC_DISCONNECT_002.h>
//#include <TC_RC_CONNECT_STATUS_UNSUBSCRIBE_009.h>
//#include <TC_RC_MESSAGE_UNSUBSCRIBE_0011.h>

/**
 * @brief This case should be executed at the end of all test cases to exit current process.
 * @brief Close all this process events subscribe.
 * @brief Close message queue context thread.
 */
#include <TC_RC_SHUTDOWN.h>
