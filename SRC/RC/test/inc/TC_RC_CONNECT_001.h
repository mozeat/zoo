/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
extern "C"
{
    #include <RC4A_if.h>
    #include <RC4A_type.h>
    #include <MM4A_if.h>
    #include <stdlib.h>
    #include <stdio.h>
}
#include <ZOO_if.h>
#include <numeric>
#include <future>
#include "MOCK_DATA_PARSER_CLASS.h"
BOOST_AUTO_TEST_SUITE(TC_RC_CONNECT_001)

    ZOO_BOOL g_message_arrived = ZOO_FALSE;
    std::string message_all;
    void RC_message_arrived_callback(IN RC4A_MESSAGE_STRUCT * status,
														 IN ZOO_INT32 error_code,
														 IN void *context)
    {
        BOOST_TEST_MESSAGE("message total number:" << status->number);
        BOOST_TEST_MESSAGE("message_id:" << status->index);
        BOOST_TEST_MESSAGE("message_len:" << status->length);
        if(status->number == (status->index+1))
        {
            g_message_arrived = ZOO_TRUE;
            BOOST_TEST_MESSAGE("message:" << message_all);
            sleep(5);
            g_message_arrived = ZOO_FALSE;
        }    
        else
        {
            message_all = message_all + status->data;
        }
    }
                                                         
    /**
     * @brief test connect
     * @param option
     * @param fd
     **/
    BOOST_AUTO_TEST_CASE( TC_RC_CONNECT_001_001 )
    {       
        //step1 : create ini parser
        RC::MOCK_DATA_PARSER_CLASS mock_parser;

        //step2 : use config case1 section data 
        mock_parser.add_section_name("case1");

        //step3 : use the default mock file: config/mock_data/HTTP_mock_data.ini  
        std::string mock_file = mock_parser.get_mock_file();        

        //step4 : Parse all section datas and mapping section with  MOCK_DATA_CLASS
        std::map<std::string,boost::shared_ptr<RC::MOCK_DATA_CLASS> >  datas = 
                            mock_parser.parse_mock_data(mock_file);

        //step5 : Get case1 section data                    
        boost::shared_ptr<RC::MOCK_DATA_CLASS> section = datas["case1"];
        BOOST_ASSERT(section != nullptr);

        //step6 : Get data by name  
        std::string url        = section->get_data("url"); 
        std::string port       = section->get_data("port"); 
        std::string keepalive_interval = section->get_data("keepalive_interval"); 
        std::string qos        = section->get_data("qos"); 
        std::string topic      = section->get_data("topic"); 
        std::string message    = section->get_data("message");   
        std::string client_id  = section->get_data("client_id");  
        std::string user       = section->get_data("user_name");   
        std::string password   = section->get_data("user_password");    
                
        RC4A_OPTION_STRUCT option;
        memset(&option,0x0,sizeof(RC4A_OPTION_STRUCT));
        sprintf(option.connection.addr, "%s",url.data());
        sprintf(option.connection.client_id, "%s",client_id.data());
        sprintf(option.connection.user, "%s",user.data());
        sprintf(option.connection.password, "%s",password.data());
        sscanf(port.data(),"%d",&option.connection.port);
        sscanf(keepalive_interval.data(),"%d",&option.connection.keepalive);
        sscanf(qos.data(),"%d",&option.connection.qos);
        option.ssl.enable_ssl = ZOO_FALSE;
        
        ZOO_FD fd = 0;
        BOOST_ASSERT(RC4A_connect(&option,&fd) == OK);
        BOOST_TEST_MESSAGE("fd:" << fd);
        BOOST_TEST(RC4A_set_subscribe_topic(fd,topic.data()) == OK);
        ZOO_UINT32 handle = 0;
        BOOST_TEST(RC4A_message_subscribe(RC_message_arrived_callback,&handle,NULL) == OK);              
        BOOST_TEST(RC4A_publish(fd,topic.data(),topic.size(),message.data(),message.size()) == OK);   
        int retry = 5;
        while(!g_message_arrived)
        {
            sleep(2);
            if(retry < 0)
            {
                break;
            }
            retry --;
        }
        BOOST_TEST(RC4A_remove_subscribe_topic(fd,topic.data()) == OK);
        BOOST_TEST(RC4A_disconnect(fd) == OK);
    }

    /**
     * @brief test connect
     * @param option
     * @param fd
     **/
    BOOST_AUTO_TEST_CASE( TC_RC_CONNECT_001_002 )
    {
        //step1 : create ini parser
        RC::MOCK_DATA_PARSER_CLASS mock_parser;

        //step2 : use config case2 section data 
        mock_parser.add_section_name("case2");

        //step3 : use the default mock file: config/mock_data/RC_mock_data.ini  
        std::string mock_file = mock_parser.get_mock_file();        

        //step4 : Parse all section datas and mapping section with  MOCK_DATA_CLASS
        std::map<std::string,boost::shared_ptr<RC::MOCK_DATA_CLASS> >  datas = 
                            mock_parser.parse_mock_data(mock_file);

        //step5 : Get case2 section data                    
        boost::shared_ptr<RC::MOCK_DATA_CLASS> section = datas["case2"];
        BOOST_ASSERT(section != nullptr);

        //step6 : Get data by name  
        std::string url        = section->get_data("url"); 
        std::string port       = section->get_data("port"); 
        std::string keepalive_interval = section->get_data("keepalive_interval"); 
        std::string qos        = section->get_data("qos"); 
        std::string topic      = section->get_data("topic"); 
        std::string message    = section->get_data("message");   
        std::string client_id  = section->get_data("client_id");    
        std::string client_pem_file  = section->get_data("public_key_file"); 
        std::string client_private_file  = section->get_data("private_key_file");
        std::string trust_chain  = section->get_data("trust_chain");  
        std::string tls_version  = section->get_data("tls_version");
        std::string user    = section->get_data("user_name");   
        std::string password  = section->get_data("user_password");
        
        RC4A_OPTION_STRUCT option;
        memset(&option,0x0,sizeof(RC4A_OPTION_STRUCT));
        sprintf(option.connection.addr, "%s",url.data());
        sprintf(option.connection.client_id, "%s",client_id.data());
        sscanf(port.data(),"%d",&option.connection.port);
        sscanf(keepalive_interval.data(),"%d",&option.connection.keepalive);
        sscanf(qos.data(),"%d",&option.connection.qos);
        sprintf(option.connection.user, "%s",user.data());
        sprintf(option.connection.password, "%s",password.data());
        option.ssl.enable_ssl = ZOO_TRUE;
        option.ssl.tls_v = (RC4A_TLS_V_ENUM)atoi(tls_version.data());
        
        std::string execute_dir = ZOO_get_current_exec_path();
        std::string config_dir  = ZOO_get_parent_dir(execute_dir.data(),3);
        std::string ssl_dir     = config_dir + "/config/" + "ssl";
        std::string client_public_key_file  = ssl_dir + "/" + client_pem_file;
        std::string server_trust_train_cert = ssl_dir + "/" + trust_chain;
        std::string client_private_key_file = ssl_dir + "/" + client_private_file;
        sprintf(option.ssl.public_key_file,"%s",client_public_key_file.data());
        sprintf(option.ssl.private_key_file,"%s",client_private_key_file.data());
        sprintf(option.ssl.trust_chain_file,"%s",server_trust_train_cert.data());
            
        ZOO_FD fd = 0;
        BOOST_ASSERT(RC4A_connect(&option,&fd) == OK);
        BOOST_TEST_MESSAGE("fd:" << fd);
        BOOST_TEST(RC4A_set_subscribe_topic(fd,topic.data()) == OK);
        ZOO_UINT32 handle = 0;
        BOOST_TEST(RC4A_message_subscribe(RC_message_arrived_callback,&handle,NULL) == OK);
        BOOST_TEST(RC4A_publish(fd,topic.data(),topic.size(),message.data(),message.size()) == OK);   
        int retry = 5;
        while(!g_message_arrived)
        {
            sleep(2);
            if(retry < 0)
            {
                break;
            }
            retry --;
        }
        BOOST_TEST(RC4A_remove_subscribe_topic(fd,topic.data()) == OK);
        BOOST_TEST(RC4A_disconnect(fd) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()
