/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
extern "C"
{
    #include <RC4A_if.h>
    #include <RC4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_RC_REMOVE_SUBSCRIBE_TOPIC_005)

/**
 *@brief 
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
**/
    BOOST_AUTO_TEST_CASE( TC_RC_REMOVE_SUBSCRIBE_TOPIC_005_001 )
    {
        ZOO_FD fd = 0;
        ZOO_CHAR topic[RC4A_BUFFER_LENGTH] = "hello";
        BOOST_TEST(RC4A_remove_subscribe_topic(fd,topic) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()