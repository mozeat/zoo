/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
extern "C"
{
    #include <RC4A_if.h>
    #include <RC4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_RC_MESSAGE_UNSUBSCRIBE_0011)

/**
 *@brief 
 *@param handle
**/
    BOOST_AUTO_TEST_CASE( TC_RC_MESSAGE_UNSUBSCRIBE_0011_001 )
    {
        ZOO_UINT32 handle;
        BOOST_TEST(RC4A_message_unsubscribe(handle) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()