/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
extern "C"
{
    #include <RC4A_if.h>
    #include <RC4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_RC_PUBLISH_007)

/**
 *@brief 
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
 *@param message[RC4A_MESSAGE_LENGTH]
 *@param message_length
 *@param message_id
 *@param message_number
**/
    BOOST_AUTO_TEST_CASE( TC_RC_PUBLISH_007_001 )
    {
        ZOO_FD fd = 0;
        ZOO_CHAR topic[RC4A_BUFFER_LENGTH];
        ZOO_CHAR message[RC4A_MESSAGE_LENGTH];
        ZOO_INT32 message_length = 0;
        ZOO_INT32 message_id = 0;
        ZOO_INT32 message_number = 0;
        BOOST_TEST(RC4A_publish(fd,topic,message,message_length,message_id,message_number) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()