/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
extern "C"
{
    #include <RC4A_if.h>
    #include <RC4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_RC_MESSAGE_SUBSCRIBE_0010)

    void RC_message_arrived_callback(IN RC4A_MESSAGE_STRUCT status,
														 IN ZOO_INT32 error_code,
														 IN void *context)
    {
        BOOST_TEST_MESSAGE("message:" << status.data);
        BOOST_TEST_MESSAGE("message_number:" << status.number);
    }
                                                         
    /**
     *@brief 
     *@param callback_function
     *@param handle
     *@param context
     **/
    BOOST_AUTO_TEST_CASE( TC_RC_MESSAGE_SUBSCRIBE_0010_001 )
    {
        ZOO_UINT32 handle = 0;
        void* context = NULL;
        BOOST_TEST(RC4A_message_subscribe(RC_message_arrived_callback,&handle,context) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()