/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : RC4I_type.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
#ifndef RC4I_TYPE_H
#define RC4I_TYPE_H
#include <MQ4A_type.h>
#include "RC4A_type.h"
#include "RC4A_if.h"


/**
 *@brief Macro Definitions
**/
#define RC4I_COMPONET_ID "RC"
#define RC4A_SERVER     "RC4A_SERVER"
#define RC4I_BUFFER_LENGTH    256
#define RC4I_RETRY_INTERVAL   3

/**
 *@brief Function Code Definitions
**/
#define RC4A_CONNECT_CODE 0x5243ff01
#define RC4A_DISCONNECT_CODE 0x5243ff02
#define RC4A_GET_FD_CODE 0x5243ff03
#define RC4A_GET_CONNECTED_CODE 0x5243ff04
#define RC4A_SET_SUBSCRIBE_TOPIC_CODE 0x5243ff05
#define RC4A_REMOVE_SUBSCRIBE_TOPIC_CODE 0x5243ff06
#define RC4A_CLEAR_DB_CODE 0x5243ff07
#define RC4A_PUBLISH_CODE 0x5243ff08
#define RC4A_CONNECT_STATUS_SUBSCRIBE_CODE 0x5243ff09
#define RC4A_MESSAGE_SUBSCRIBE_CODE 0x5243ff0b

/*Request and reply header struct*/
typedef struct
{
    MQ4A_SERV_ADDR reply_addr;
    ZOO_UINT32 msg_id;
    ZOO_BOOL reply_wanted;
    ZOO_UINT32 func_id;
}RC4I_REPLY_HANDLER_STRUCT;

typedef  RC4I_REPLY_HANDLER_STRUCT * RC4I_REPLY_HANDLE;

/*Request message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL need_reply;
}RC4I_REQUEST_HEADER_STRUCT;

/*Reply message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL execute_result;
}RC4I_REPLY_HEADER_STRUCT;

/**
*@brief RC4I_CONNECT_CODE_REQ_STRUCT
**/
typedef struct 
{
    RC4A_OPTION_STRUCT option;
    ZOO_CHAR g_filler[8];
}RC4I_CONNECT_CODE_REQ_STRUCT;

/**
*@brief RC4I_DISCONNECT_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_FD fd;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}RC4I_DISCONNECT_CODE_REQ_STRUCT;

/**
*@brief RC4I_GET_FD_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_CHAR client_id[RC4I_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}RC4I_GET_FD_CODE_REQ_STRUCT;

/**
*@brief RC4I_GET_CONNECTED_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_FD fd;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}RC4I_GET_CONNECTED_CODE_REQ_STRUCT;

/**
*@brief RC4I_SET_SUBSCRIBE_TOPIC_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_FD fd;
    ZOO_CHAR c_filler[4];
     ZOO_CHAR topic[RC4A_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}RC4I_SET_SUBSCRIBE_TOPIC_CODE_REQ_STRUCT;

/**
*@brief RC4I_REMOVE_SUBSCRIBE_TOPIC_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_FD fd;
    ZOO_CHAR c_filler[4];
     ZOO_CHAR topic[RC4A_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}RC4I_REMOVE_SUBSCRIBE_TOPIC_CODE_REQ_STRUCT;

/**
*@brief RC4I_CLEAR_DB_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_FD fd;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}RC4I_CLEAR_DB_CODE_REQ_STRUCT;

/**
*@brief RC4I_PUBLISH_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_FD fd;
    ZOO_INT32 message_length;
    ZOO_INT32 message_id;
    ZOO_INT32 message_number;
    ZOO_CHAR topic[RC4A_BUFFER_LENGTH];
    ZOO_CHAR message[RC4A_MESSAGE_LENGTH];
    ZOO_CHAR g_filler[8];
}RC4I_PUBLISH_CODE_REQ_STRUCT;

/**
*@brief RC4I_CONNECT_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_FD fd;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}RC4I_CONNECT_CODE_REP_STRUCT;

/**
*@brief RC4I_DISCONNECT_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}RC4I_DISCONNECT_CODE_REP_STRUCT;

/**
*@brief RC4I_GET_FD_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_FD fd;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}RC4I_GET_FD_CODE_REP_STRUCT;

/**
*@brief RC4I_GET_CONNECTED_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_BOOL is_connected;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}RC4I_GET_CONNECTED_CODE_REP_STRUCT;

/**
*@brief RC4I_SET_SUBSCRIBE_TOPIC_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}RC4I_SET_SUBSCRIBE_TOPIC_CODE_REP_STRUCT;

/**
*@brief RC4I_REMOVE_SUBSCRIBE_TOPIC_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}RC4I_REMOVE_SUBSCRIBE_TOPIC_CODE_REP_STRUCT;

/**
*@brief RC4I_CLEAR_DB_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}RC4I_CLEAR_DB_CODE_REP_STRUCT;

/**
*@brief RC4I_PUBLISH_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}RC4I_PUBLISH_CODE_REP_STRUCT;

/**
*@brief RC4I_CONNECT_STATUS_SUBSCRIBE_CODE_REP_STRUCT
**/
typedef struct 
{
    RC4A_CONNECTION_STATUS_STRUCT status;
    ZOO_CHAR g_filler[8];
}RC4I_CONNECT_STATUS_SUBSCRIBE_CODE_REP_STRUCT;

/**
*@brief RC4I_MESSAGE_SUBSCRIBE_CODE_REP_STRUCT
**/
typedef struct 
{
    RC4A_MESSAGE_STRUCT status;
    ZOO_CHAR g_filler[8];
}RC4I_MESSAGE_SUBSCRIBE_CODE_REP_STRUCT;

typedef struct
{
    RC4I_REQUEST_HEADER_STRUCT request_header;
    union
    {
        RC4I_CONNECT_CODE_REQ_STRUCT connect_req_msg;
        RC4I_DISCONNECT_CODE_REQ_STRUCT disconnect_req_msg;
        RC4I_GET_FD_CODE_REQ_STRUCT get_fd_req_msg;
        RC4I_GET_CONNECTED_CODE_REQ_STRUCT get_connected_req_msg;
        RC4I_SET_SUBSCRIBE_TOPIC_CODE_REQ_STRUCT set_subscribe_topic_req_msg;
        RC4I_REMOVE_SUBSCRIBE_TOPIC_CODE_REQ_STRUCT remove_subscribe_topic_req_msg;
        RC4I_CLEAR_DB_CODE_REQ_STRUCT clear_db_req_msg;
        RC4I_PUBLISH_CODE_REQ_STRUCT publish_req_msg;
     }request_body;
}RC4I_REQUEST_STRUCT;


typedef struct
{
    RC4I_REPLY_HEADER_STRUCT reply_header;
    union
    {
        RC4I_CONNECT_CODE_REP_STRUCT connect_rep_msg;
        RC4I_DISCONNECT_CODE_REP_STRUCT disconnect_rep_msg;
        RC4I_GET_FD_CODE_REP_STRUCT get_fd_rep_msg;
        RC4I_GET_CONNECTED_CODE_REP_STRUCT get_connected_rep_msg;
        RC4I_SET_SUBSCRIBE_TOPIC_CODE_REP_STRUCT set_subscribe_topic_rep_msg;
        RC4I_REMOVE_SUBSCRIBE_TOPIC_CODE_REP_STRUCT remove_subscribe_topic_rep_msg;
        RC4I_CLEAR_DB_CODE_REP_STRUCT clear_db_rep_msg;
        RC4I_PUBLISH_CODE_REP_STRUCT publish_rep_msg;
        RC4I_CONNECT_STATUS_SUBSCRIBE_CODE_REP_STRUCT connect_status_subscribe_code_rep_msg;
        RC4I_MESSAGE_SUBSCRIBE_CODE_REP_STRUCT message_subscribe_code_rep_msg;
    }reply_body;
}RC4I_REPLY_STRUCT;


/**
*@brief RC4I_CONNECT_STATUS_SUBSCRIBE_CODE_CALLBACK_STRUCT
**/
typedef struct 
{
    RC4A_CONNECT_STATUS_CALLBACK_FUNCTION *callback_function;
    void * parameter;
}RC4I_CONNECT_STATUS_SUBSCRIBE_CODE_CALLBACK_STRUCT;

/**
*@brief RC4I_MESSAGE_SUBSCRIBE_CODE_CALLBACK_STRUCT
**/
typedef struct 
{
    RC4A_MESSAGE_CALLBACK_FUNCTION *callback_function;
    void * parameter;
}RC4I_MESSAGE_SUBSCRIBE_CODE_CALLBACK_STRUCT;

#endif //RC4I_type.h
