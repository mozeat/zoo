/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : RC4I_if.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
#ifndef RC4I_IF_H
#define RC4I_IF_H

#include <ZOO.h>
#include <ZOO_tc.h>
#include <stdio.h>
#include <stdlib.h>
#include <EH4A_if.h>
#include <TR4A_if.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include "RC4I_type.h"
#include "RC4A_type.h"
/*
@brief Get request message length[bytes]
*@param function_code   function id
*@param *message_length  message length 
*@precondition:
*@postcondition:
*/
ZOO_EXPORT ZOO_INT32 RC4I_get_request_message_length(IN ZOO_INT32 function_code,
													INOUT ZOO_INT32 *message_length );

/*
@brief Get request message length[bytes]
*@param function_code    function id
*@param *message_length  message length 
*@precondition:
*@postcondition:
*/
ZOO_EXPORT ZOO_INT32 RC4I_get_reply_message_length(IN ZOO_INT32 function_code,
													INOUT ZOO_INT32 *message_length );

/*
*@brief Send message to server and wait response,it is a sync api
*@param MQ4A_SERV_ADDR   server address
*@param *request_message request message
*@param *reply_message   reply message
*@param timeout          timeout value for waiting reply[milliseconds] 
*@precondition:
*@postcondition: 
*/
ZOO_EXPORT ZOO_INT32 RC4I_send_request_and_reply(IN const MQ4A_SERV_ADDR server,
													IN RC4I_REQUEST_STRUCT  *request_message,
													INOUT RC4I_REPLY_STRUCT *reply_message,
													IN ZOO_INT32 timeout);

/*
*@brief Send message to server and wait response,it is a async apiRC4I_receive_reply_message
*@param MQ4A_SERV_ADDR   server address
*@param *request_message request message 
*@precondition:
*@postcondition:
*/
ZOO_EXPORT ZOO_INT32 RC4I_send_request_message(IN const MQ4A_SERV_ADDR server,
													IN RC4I_REQUEST_STRUCT *request_message);

/*
*@brief Recieve message from server
	*@param server         server address
*@param function_code  message id
*@param *reply_message reply message
*@param timeout        reply timeout value[milliseconds]
*@description:         async interface, use in combination with send_request_message
*@precondition:
*@postcondition:
*/
ZOO_EXPORT ZOO_INT32 RC4I_receive_reply_message(IN const MQ4A_SERV_ADDR server,
													IN ZOO_INT32 function_code,
													INOUT RC4I_REPLY_STRUCT *reply_message,
													IN ZOO_INT32 timeout);

/*
*@brief Response message to client after has a requestRC4I_send_reply_message
*@param MQ4A_SERV_ADDR    server address
*@param msg_id            message id
*@param *reply_message    response message 
*@precondition:
*@postcondition:
*/
ZOO_EXPORT ZOO_INT32 RC4I_send_reply_message(IN const MQ4A_SERV_ADDR server,
								                IN ZOO_INT32 msg_id,
                                                IN RC4I_REPLY_STRUCT *reply_message);

/*
*@brief Publish message to subscribers
*@param event_id       message id
*@param *reply_message response message
*@description:          
*@precondition:
*@postcondition:
*/
ZOO_EXPORT ZOO_INT32 RC4I_publish_event(IN const MQ4A_SERV_ADDR server,
													IN ZOO_INT32 event_id,
													INOUT RC4I_REPLY_STRUCT *reply_message);

/*
*@brief Subscribe messages
*@param server            
*@param callback_function server message handler 
*@param callback_struct   hanle context
*@param event_id          message
*@param *handle           the subscriber id
*@param *context          context
 *@precondition:
*@postcondition:
*/
ZOO_EXPORT ZOO_INT32 RC4I_send_subscribe(IN const MQ4A_SERV_ADDR server,
													IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
													IN MQ4A_CALLBACK_STRUCT *callback_struct,
													IN ZOO_INT32 event_id,
													INOUT ZOO_HANDLE *handle,
													INOUT void *context);

/*
*@brief Unsubsribe message
*@param server   address
*@param event_id 
*@param handle   
*@precondition:  
*@postcondition: 
*/
ZOO_EXPORT ZOO_INT32 RC4I_send_unsubscribe(IN const MQ4A_SERV_ADDR server,
													IN ZOO_INT32 event_id,
													IN ZOO_HANDLE handle);

#endif // RC4I_if.h

