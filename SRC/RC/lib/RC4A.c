/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : RC4A.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/

#include <ZOO.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include "RC4I_type.h"
#include "RC4A_type.h"
#include "RC4I_if.h"
#include <pthread.h>

static pthread_mutex_t rc4a_multi_message_sync_publish_lock = PTHREAD_MUTEX_INITIALIZER;

/**
 *@brief Publish message if message length less than RC4A_MESSAGE_LENGTH
**/
ZOO_INT32 RC4A_local_publish(IN ZOO_FD fd,
											    IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH],
											    IN const ZOO_CHAR message[RC4A_MESSAGE_LENGTH],
											    IN ZOO_INT32 message_length,
											    IN ZOO_INT32 message_id,
											    IN ZOO_INT32 message_number);


/**
 *@brief RC4A_connect
 *@param option
 *@param fd
**/
ZOO_INT32 RC4A_connect(IN RC4A_OPTION_STRUCT * option, INOUT ZOO_FD * fd)
{
    ZOO_INT32 result = OK;
    RC4I_REQUEST_STRUCT *request_message = NULL; 
    RC4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = RC4A_CONNECT_CODE;
    result = RC4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (RC4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = RC4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (RC4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.connect_req_msg.option),option,sizeof(RC4A_OPTION_STRUCT));
    }

    if(OK == result)
    {
        result = RC4I_send_request_and_reply(RC4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(fd,&reply_message->reply_body.connect_rep_msg.fd,sizeof(ZOO_FD));
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief RC4A_disconnect
 *@param fd
**/
ZOO_INT32 RC4A_disconnect(IN ZOO_FD fd)
{
    ZOO_INT32 result = OK;
    RC4I_REQUEST_STRUCT *request_message = NULL; 
    RC4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = RC4A_DISCONNECT_CODE;
    result = RC4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (RC4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = RC4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (RC4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.disconnect_req_msg.fd),&fd,sizeof(ZOO_FD));
    }

    if(OK == result)
    {
        result = RC4I_send_request_and_reply(RC4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief RC4A_get_fd
 *@param client_id
 *@param fd
**/
ZOO_INT32 RC4A_get_fd(IN ZOO_CHAR * client_id,INOUT ZOO_FD * fd)
{
    ZOO_INT32 result = OK;
    RC4I_REQUEST_STRUCT *request_message = NULL; 
    RC4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = RC4A_GET_FD_CODE;
    result = RC4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (RC4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = RC4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (RC4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.get_fd_req_msg.client_id[0]),client_id,sizeof(ZOO_CHAR) * RC4I_BUFFER_LENGTH);
    }

    if(OK == result)
    {
        result = RC4I_send_request_and_reply(RC4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(fd,&reply_message->reply_body.get_fd_rep_msg.fd,sizeof(ZOO_FD));
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief RC4A_get_connected
 *@param fd
 *@param is_connected
**/
ZOO_INT32 RC4A_get_connected(IN ZOO_FD fd,INOUT ZOO_BOOL * is_connected)
{
    ZOO_INT32 result = OK;
    RC4I_REQUEST_STRUCT *request_message = NULL; 
    RC4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = RC4A_GET_CONNECTED_CODE;
    result = RC4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (RC4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = RC4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (RC4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.get_connected_req_msg.fd),&fd,sizeof(ZOO_FD));
    }

    if(OK == result)
    {
        result = RC4I_send_request_and_reply(RC4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(is_connected,&reply_message->reply_body.get_connected_rep_msg.is_connected,sizeof(ZOO_BOOL));
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief RC4A_set_subscribe_topic
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
**/
ZOO_INT32 RC4A_set_subscribe_topic(IN ZOO_FD fd,
												                      IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH])
{
    ZOO_INT32 result = OK;
    RC4I_REQUEST_STRUCT *request_message = NULL; 
    RC4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = RC4A_SET_SUBSCRIBE_TOPIC_CODE;
    result = RC4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (RC4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = RC4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (RC4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.set_subscribe_topic_req_msg.fd),&fd,sizeof(ZOO_FD));
        memcpy((void *)(&request_message->request_body.set_subscribe_topic_req_msg.topic[0]),&topic[0],sizeof(ZOO_CHAR) * RC4A_BUFFER_LENGTH);
    }

    if(OK == result)
    {
        result = RC4I_send_request_and_reply(RC4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief RC4A_remove_subscribe_topic
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
**/
ZOO_INT32 RC4A_remove_subscribe_topic(IN ZOO_FD fd,
														                    IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH])
{
    ZOO_INT32 result = OK;
    RC4I_REQUEST_STRUCT *request_message = NULL; 
    RC4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = RC4A_REMOVE_SUBSCRIBE_TOPIC_CODE;
    result = RC4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (RC4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = RC4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (RC4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.remove_subscribe_topic_req_msg.fd),&fd,sizeof(ZOO_FD));
        memcpy((void *)(&request_message->request_body.remove_subscribe_topic_req_msg.topic[0]),&topic[0],sizeof(ZOO_CHAR) * RC4A_BUFFER_LENGTH);
    }

    if(OK == result)
    {
        result = RC4I_send_request_and_reply(RC4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief RC4A_clear_db
 *@param fd
**/
ZOO_INT32 RC4A_clear_db(IN ZOO_FD fd)
{
    ZOO_INT32 result = OK;
    RC4I_REQUEST_STRUCT *request_message = NULL; 
    RC4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = RC4A_CLEAR_DB_CODE;
    result = RC4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (RC4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = RC4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (RC4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.clear_db_req_msg.fd),&fd,sizeof(ZOO_FD));
    }

    if(OK == result)
    {
        result = RC4I_send_request_and_reply(RC4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief RC4A_publish
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
 *@param message[RC4A_MESSAGE_LENGTH]
 *@param message_length
 *@param message_id
 *@param message_number
**/
ZOO_INT32 RC4A_local_publish(IN ZOO_FD fd,
											    IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH],
											    IN const ZOO_CHAR message[RC4A_MESSAGE_LENGTH],
											    IN ZOO_INT32 message_length,
											    IN ZOO_INT32 message_id,
											    IN ZOO_INT32 message_number)
{
    ZOO_INT32 result = OK;
    RC4I_REQUEST_STRUCT *request_message = NULL; 
    RC4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = RC4A_PUBLISH_CODE;
    result = RC4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (RC4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
        if(request_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = RC4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (RC4I_REPLY_STRUCT *)MM4A_malloc(reply_length); 
        if(reply_message == NULL)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.publish_req_msg.fd),&fd,sizeof(ZOO_FD));
        memcpy((void *)(&request_message->request_body.publish_req_msg.topic[0]),&topic[0],sizeof(ZOO_CHAR) * RC4A_BUFFER_LENGTH);
        memcpy((void *)(&request_message->request_body.publish_req_msg.message[0]),&message[0],sizeof(ZOO_CHAR) * message_length);
        memcpy((void *)(&request_message->request_body.publish_req_msg.message_length),&message_length,sizeof(ZOO_INT32));
        memcpy((void *)(&request_message->request_body.publish_req_msg.message_id),&message_id,sizeof(ZOO_INT32));
        memcpy((void *)(&request_message->request_body.publish_req_msg.message_number),&message_number,sizeof(ZOO_INT32));
    }

    if(OK == result)
    {
        result = RC4I_send_request_and_reply(RC4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
        request_message = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return result;
}

ZOO_INT32 RC4A_publish(IN ZOO_FD fd,
											    IN const ZOO_CHAR *topic,
											    IN ZOO_CHAR topic_length,
											    IN const ZOO_CHAR *message,
											    IN ZOO_INT32 message_length)
											    

{
    pthread_mutex_lock(&rc4a_multi_message_sync_publish_lock);
    ZOO_INT32 error_code = OK;
    int i = 0;
    if(topic_length > RC4A_BUFFER_LENGTH)
    {
        error_code = RC4A_PARAMETER_ERR;
    }

    if(OK == error_code)
    {
        if(message_length <= RC4A_MESSAGE_LENGTH)
        {
            error_code = RC4A_local_publish(fd,topic,message,message_length,0,1);
        }

        else
        {
            ZOO_INT32 integer_msg_number = message_length / RC4A_MESSAGE_LENGTH;
            ZOO_INT32 remain_msg_number = message_length % RC4A_MESSAGE_LENGTH; 
            ZOO_INT32 total_msg_number = integer_msg_number;
            if(remain_msg_number > 0)
            {
                total_msg_number = total_msg_number + 1;
            }
            
            for(; i < integer_msg_number; i ++)
            {
                error_code = RC4A_local_publish(fd,topic,(ZOO_CHAR *) &message[i * RC4A_MESSAGE_LENGTH],RC4A_MESSAGE_LENGTH,i,total_msg_number);
                if(OK != error_code)
                {
                    break;
                }
            }

            if(OK == error_code)
            {
                if(remain_msg_number > 0)
                {
                    error_code = RC4A_local_publish(fd,topic,
                        (ZOO_CHAR *) &message[message_length - remain_msg_number],
                        remain_msg_number,
                        total_msg_number - 1,
                        total_msg_number);
                }
            }
        }
    }
    pthread_mutex_unlock(&rc4a_multi_message_sync_publish_lock);
    return error_code;
}

                                                
static void RC4A_connect_status_callback(void *context_p, MQ4A_CALLBACK_STRUCT *local_proc, void *msg)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 error_code = OK;
    RC4I_REPLY_STRUCT *reply_msg = NULL;
    RC4I_CONNECT_STATUS_SUBSCRIBE_CODE_CALLBACK_STRUCT * callback_struct = NULL;
    ZOO_INT32 rep_length = 0;
    RC4A_CONNECTION_STATUS_STRUCT status;
    memset(&status,0,sizeof(RC4A_CONNECTION_STATUS_STRUCT));
    if(msg == NULL)
    {
        result = RC4A_PARAMETER_ERR;
    }

    if(OK == result)
    {
        result = RC4I_get_reply_message_length(RC4A_CONNECT_STATUS_SUBSCRIBE_CODE, &rep_length);
        if(OK != result)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        reply_msg = (RC4I_REPLY_STRUCT * )MM4A_malloc(rep_length);
        if(NULL == reply_msg)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        memcpy(reply_msg, msg, rep_length);
        if (RC4A_CONNECT_STATUS_SUBSCRIBE_CODE != reply_msg->reply_header.function_code)
        {
            result = RC4A_PARAMETER_ERR;
        }
        error_code = reply_msg->reply_header.execute_result;
        memcpy((void*)&status, &reply_msg->reply_body.connect_status_subscribe_code_rep_msg.status, sizeof(RC4A_CONNECTION_STATUS_STRUCT));
        callback_struct = (RC4I_CONNECT_STATUS_SUBSCRIBE_CODE_CALLBACK_STRUCT*) local_proc;
        ((RC4A_CONNECT_STATUS_CALLBACK_FUNCTION)callback_struct->callback_function)(status,error_code,context_p);
    }

    if(reply_msg != NULL)
    {
        MM4A_free(reply_msg);
    }
}


 ZOO_INT32 RC4A_connect_status_subscribe(IN RC4A_CONNECT_STATUS_CALLBACK_FUNCTION callback_function,
																OUT ZOO_UINT32 *handle,
																INOUT void *context)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = 0;
    MQ4A_CALLBACK_STRUCT* callback = NULL;
    if (NULL == callback_function)
    {
        result = RC4A_PARAMETER_ERR;
        return result;
    }

    callback = (MQ4A_CALLBACK_STRUCT*)MM4A_malloc(sizeof(RC4I_CONNECT_STATUS_SUBSCRIBE_CODE_CALLBACK_STRUCT));
    if (NULL == callback)
    {
        result = RC4A_PARAMETER_ERR;
    }

    if (OK == result)
    {
        callback->callback_function = callback_function;
        event_id = RC4A_CONNECT_STATUS_SUBSCRIBE_CODE;
        result = RC4I_send_subscribe(RC4A_SERVER,
                                      RC4A_connect_status_callback,
                                      callback,
                                      event_id,
                                      (ZOO_HANDLE*)handle,
                                      context);
        if (OK != result)
        {
           result = RC4A_PARAMETER_ERR;
           MM4A_free(callback);
        }
    }

    return result;
}

ZOO_INT32 RC4A_connect_status_unsubscribe(IN ZOO_UINT32 handle)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = RC4A_CONNECT_STATUS_SUBSCRIBE_CODE;
    if(OK == result)
    {
        result = RC4I_send_unsubscribe(RC4A_SERVER,event_id,handle);
    }
    return result;
}

static void RC4A_message_callback(void *context_p, MQ4A_CALLBACK_STRUCT *local_proc, void *msg)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 error_code = OK;
    RC4I_REPLY_STRUCT *reply_msg = NULL;
    RC4I_MESSAGE_SUBSCRIBE_CODE_CALLBACK_STRUCT * callback_struct = NULL;
    ZOO_INT32 rep_length = 0;
    if(msg == NULL)
    {
        result = RC4A_PARAMETER_ERR;
    }

    if(OK == result)
    {
        result = RC4I_get_reply_message_length(RC4A_MESSAGE_SUBSCRIBE_CODE, &rep_length);
        if(OK != result)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        reply_msg = (RC4I_REPLY_STRUCT * )MM4A_malloc(rep_length);
        if(NULL == reply_msg)
        {
            result = RC4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        memcpy(reply_msg, msg, rep_length);
        if (RC4A_MESSAGE_SUBSCRIBE_CODE != reply_msg->reply_header.function_code)
        {
            result = RC4A_PARAMETER_ERR;
        }
        error_code = reply_msg->reply_header.execute_result;
        callback_struct = (RC4I_MESSAGE_SUBSCRIBE_CODE_CALLBACK_STRUCT*) local_proc;
        ((RC4A_MESSAGE_CALLBACK_FUNCTION)callback_struct->callback_function)(&reply_msg->reply_body.message_subscribe_code_rep_msg.status,error_code,context_p);
    }

    if(reply_msg != NULL)
    {
        MM4A_free(reply_msg);
    }
}


 ZOO_INT32 RC4A_message_subscribe(IN RC4A_MESSAGE_CALLBACK_FUNCTION callback_function,
																OUT ZOO_UINT32 *handle,
																INOUT void *context)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = 0;
    MQ4A_CALLBACK_STRUCT* callback = NULL;
    if (NULL == callback_function)
    {
        result = RC4A_PARAMETER_ERR;
        return result;
    }

    callback = (MQ4A_CALLBACK_STRUCT*)MM4A_malloc(sizeof(RC4I_MESSAGE_SUBSCRIBE_CODE_CALLBACK_STRUCT));
    if (NULL == callback)
    {
        result = RC4A_PARAMETER_ERR;
    }

    if (OK == result)
    {
        callback->callback_function = callback_function;
        event_id = RC4A_MESSAGE_SUBSCRIBE_CODE;
        result = RC4I_send_subscribe(RC4A_SERVER,
                                      RC4A_message_callback,
                                      callback,
                                      event_id,
                                      (ZOO_HANDLE*)handle,
                                      context);
        if (OK != result)
        {
           result = RC4A_PARAMETER_ERR;
           MM4A_free(callback);
        }
    }

    return result;
}

ZOO_INT32 RC4A_message_unsubscribe(IN ZOO_UINT32 handle)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = RC4A_MESSAGE_SUBSCRIBE_CODE;
    if(OK == result)
    {
        result = RC4I_send_unsubscribe(RC4A_SERVER,event_id,handle);
    }
    return result;
}

