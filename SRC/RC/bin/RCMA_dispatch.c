/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : RCMA_dispatch.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/

#include <RCMA_dispatch.h>
#include <RCMA_implement.h>

/**
 *@brief RCMA_local_4A_connect
 *@param option
 *@param fd
**/
static ZOO_INT32 RCMA_local_4A_connect(const MQ4A_SERV_ADDR server,RC4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    RC4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = RC4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (RC4I_REPLY_HANDLE)MM4A_malloc(sizeof(RC4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = RC4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        RCMA_implement_4A_connect(&request->request_body.connect_req_msg.option,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief RCMA_local_4A_disconnect
 *@param fd
**/
static ZOO_INT32 RCMA_local_4A_disconnect(const MQ4A_SERV_ADDR server,RC4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    RC4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = RC4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (RC4I_REPLY_HANDLE)MM4A_malloc(sizeof(RC4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = RC4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        RCMA_implement_4A_disconnect(request->request_body.disconnect_req_msg.fd,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief RCMA_local_4A_get_fd
 *@param client_id
 *@param fd
**/
static ZOO_INT32 RCMA_local_4A_get_fd(const MQ4A_SERV_ADDR server,RC4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    RC4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = RC4A_SYSTEM_ERR;
        EH4A_show_exception(RC4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,RC4A_PARAMETER_ERR,0,"request pointer is NULL ...ERROR");
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (RC4I_REPLY_HANDLE)MM4A_malloc(sizeof(RC4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = RC4A_SYSTEM_ERR;
            EH4A_show_exception(RC4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,RC4A_SYSTEM_ERR,0,"malloc reply_handle failed ...ERROR");
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        RCMA_implement_4A_get_fd(request->request_body.get_fd_req_msg.client_id,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief RCMA_local_4A_get_connected
 *@param fd
 *@param is_connected
**/
static ZOO_INT32 RCMA_local_4A_get_connected(const MQ4A_SERV_ADDR server,RC4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    RC4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = RC4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (RC4I_REPLY_HANDLE)MM4A_malloc(sizeof(RC4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = RC4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        RCMA_implement_4A_get_connected(request->request_body.get_connected_req_msg.fd,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief RCMA_local_4A_set_subscribe_topic
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
**/
static ZOO_INT32 RCMA_local_4A_set_subscribe_topic(const MQ4A_SERV_ADDR server,RC4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    RC4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = RC4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (RC4I_REPLY_HANDLE)MM4A_malloc(sizeof(RC4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = RC4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        RCMA_implement_4A_set_subscribe_topic(request->request_body.set_subscribe_topic_req_msg.fd,
                                           request->request_body.set_subscribe_topic_req_msg.topic,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief RCMA_local_4A_remove_subscribe_topic
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
**/
static ZOO_INT32 RCMA_local_4A_remove_subscribe_topic(const MQ4A_SERV_ADDR server,RC4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    RC4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = RC4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (RC4I_REPLY_HANDLE)MM4A_malloc(sizeof(RC4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = RC4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        RCMA_implement_4A_remove_subscribe_topic(request->request_body.remove_subscribe_topic_req_msg.fd,
                                           request->request_body.remove_subscribe_topic_req_msg.topic,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief RCMA_local_4A_clear_db
 *@param fd
**/
static ZOO_INT32 RCMA_local_4A_clear_db(const MQ4A_SERV_ADDR server,RC4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    RC4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = RC4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (RC4I_REPLY_HANDLE)MM4A_malloc(sizeof(RC4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = RC4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        RCMA_implement_4A_clear_db(request->request_body.clear_db_req_msg.fd,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief RCMA_local_4A_publish
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
 *@param message[RC4A_MESSAGE_LENGTH]
 *@param message_length
 *@param message_id
 *@param message_number
**/
static ZOO_INT32 RCMA_local_4A_publish(const MQ4A_SERV_ADDR server,RC4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    RC4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = RC4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (RC4I_REPLY_HANDLE)MM4A_malloc(sizeof(RC4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = RC4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        RCMA_implement_4A_publish(request->request_body.publish_req_msg.fd,
                                           request->request_body.publish_req_msg.topic,
                                           request->request_body.publish_req_msg.message,
                                           request->request_body.publish_req_msg.message_length,
                                           request->request_body.publish_req_msg.message_id,
                                           request->request_body.publish_req_msg.message_number,
                                           reply_handle);
    }
    return rtn;
}

/**
*@brief Dispatch message from client to server internal interface
*@param context        
*@param server        address
*@param msg           request message to server
*@param len           request message length
*@param reply_msg     reply message length to caller
*@param reply_msg_len reply message length
**/
void RCMA_callback_handler(void * context,const MQ4A_SERV_ADDR server,void * msg,ZOO_UINT32 msg_id)
{
    ZOO_INT32 rtn = OK;
    ZOO_INT32 rep_length = 0;
    RC4I_REQUEST_STRUCT *request = (RC4I_REQUEST_STRUCT*)msg;
    RC4I_REPLY_STRUCT *reply = NULL;
    if(request == NULL)
    {
        rtn = RC4A_SYSTEM_ERR;
        return;
    }

    if(OK == rtn)
    {
        switch(request->request_header.function_code)
        {
            case RC4A_CONNECT_CODE:
                RCMA_local_4A_connect(server,request,msg_id);
                break;
            case RC4A_DISCONNECT_CODE:
                RCMA_local_4A_disconnect(server,request,msg_id);
                break;
            case RC4A_GET_FD_CODE:
                RCMA_local_4A_get_fd(server,request,msg_id);
                break;
            case RC4A_GET_CONNECTED_CODE:
                RCMA_local_4A_get_connected(server,request,msg_id);
                break;
            case RC4A_SET_SUBSCRIBE_TOPIC_CODE:
                RCMA_local_4A_set_subscribe_topic(server,request,msg_id);
                break;
            case RC4A_REMOVE_SUBSCRIBE_TOPIC_CODE:
                RCMA_local_4A_remove_subscribe_topic(server,request,msg_id);
                break;
            case RC4A_CLEAR_DB_CODE:
                RCMA_local_4A_clear_db(server,request,msg_id);
                break;
            case RC4A_PUBLISH_CODE:
                RCMA_local_4A_publish(server,request,msg_id);
                break;
            default:
                rtn = RC4A_SYSTEM_ERR;
                break;
        }
    }

    if(OK != rtn && request->request_header.need_reply)
    {
        rtn = RC4I_get_reply_message_length(request->request_header.function_code, &rep_length);
        if(OK != rtn)
        {
            rtn = RC4A_SYSTEM_ERR;
        }

        if(OK == rtn)
        {
            reply = (RC4I_REPLY_STRUCT *)MM4A_malloc(rep_length);
            if(reply == NULL)
            {
                rtn = RC4A_SYSTEM_ERR;
            }
        }

        if(OK == rtn)
        {
            memset(reply, 0x0, rep_length);
            reply->reply_header.execute_result = rtn;
            reply->reply_header.function_code = request->request_header.function_code;
            rtn = RC4I_send_reply_message(server,msg_id,reply);
        }
    }
    return;
}

