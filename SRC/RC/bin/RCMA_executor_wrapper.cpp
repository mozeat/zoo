/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RCMA_executor_wrapper.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include <RCMA_executor_wrapper.h>
#include <ZOO_COMMON_MACRO_DEFINE.h>

#include "utils/THREAD_POOL.h"
#include "PROCESSING_FLOW_FACADE_CLASS.h"
#include "STATE_MANAGER_CLASS.h"
#include "DEVICE_CONTROLLER_CLASS.h"
#include "EVENT_PUBLISHER_CLASS.h"
#include "RC_CONFIGURE.h"
/*
 * @brief Define the device controller.
**/
boost::shared_ptr<RC::DEVICE_CONTROLLER_INTERFACE> g_device_controller;

/*
 * @brief Define processing flow instance.
**/
boost::shared_ptr<RC::PROCESSING_FLOW_FACADE_INTERFACE> g_processing_flow;

/*
 * @brief Define the state manager.
**/
boost::shared_ptr<RC::STATE_MANAGER_CLASS> g_state_manager;

/*
 * @brief The event publisher instance.
**/
boost::shared_ptr<RC::EVENT_PUBLISHER_CLASS> g_event_publisher;

/**  
 * @brief Register system signal handler,throw PARAMETER_EXCEPTION_CLASS if register fail,
 * the default signal handling is save stack trace to the log file and generate a dump file at execute path.
 * register a self-defined callback to SYSTEM_SIGNAL_HANDLER::resgister_siganl will change the default behavior.
**/
static void RCMA_register_system_signals()
{
    __ZOO_TRACE(RC4I_COMPONET_ID," function entry ...");
    __ZOO_SYSTEM_SIGNAL_REGISTER(RC4I_COMPONET_ID,SIGSEGV); 
    __ZOO_SYSTEM_SIGNAL_REGISTER(RC4I_COMPONET_ID,SIGABRT);

    /* Add more signals if needs,or register self-defined callback function
       to change the default behavior... */
    __ZOO_TRACE(RC4I_COMPONET_ID,"< function exit ...");
}

/**
 *@brief Execute the start up flow.
 * This function is executed in 3 steps: 
 * Step 1: Load configurations 
 * Step 2: Create controllers
 * Step 3: Create facades and set controllers to created facades
**/ 
void RCMA_startup(void)
{
    __ZOO_TRACE(RC4I_COMPONET_ID,"> function entry ...");
    try
    {
        /**
         * @brief Signal handler for system behavior 
        */
        RCMA_register_system_signals();

        /** 
         *@brief Step 1: Load configurations 
        */
        RC::RC_CONFIGURE::get_instance()->initialize();
        /** 
         *@brief Startup thread pool. 
        */
        ZOO_COMMON::THREAD_POOL::get_instance()->startup();

        /** 
         *@brief Step 2: Create controllers 
        */
        g_state_manager.reset(new RC::STATE_MANAGER_CLASS());
        g_device_controller.reset(new RC::DEVICE_CONTROLLER_CLASS());
        g_event_publisher.reset(new RC::EVENT_PUBLISHER_CLASS());
        g_device_controller->set_event_publisher(g_event_publisher);

        /** 
         *@brief Step 3: Create facades and set controllers to created facades 
        */
        g_processing_flow.reset(new RC::PROCESSING_FLOW_FACADE_CLASS());
        g_processing_flow->set_device_controller(g_device_controller);
        g_processing_flow->set_state_manager(g_state_manager);
        g_device_controller->add_observer(g_processing_flow.get());

    }
    catch(...)
    {
        __ZOO_TRACE(RC4I_COMPONET_ID,":: unhandle exception ...ERROR");
    }
    __ZOO_TRACE(RC4I_COMPONET_ID,"< function exit ...");
}

/**
 *@brief This function response to release instance or memory 
**/ 
void RCMA_shutdown(void)
{
    __ZOO_TRACE(RC4I_COMPONET_ID,"> function entry ...");
    try
    {
        /** User add */
        g_processing_flow.reset();
        g_device_controller.reset();
        g_state_manager.reset();
        ZOO_COMMON::THREAD_POOL::get_instance()->shutdown();
    }
    catch(...)
    {
        __ZOO_TRACE(RC4I_COMPONET_ID,":: unhandle exception ...ERROR");
    }
    __ZOO_TRACE(RC4I_COMPONET_ID,"< function exit ...");
}

/**
 *@brief Subscribe events published from hardware drivers 
**/
void RCMA_subscribe_driver_event(void)
{
    __ZOO_TRACE(RC4I_COMPONET_ID,"> function entry ...");

    __ZOO_TRACE(RC4I_COMPONET_ID,"< function exit ...");
}

