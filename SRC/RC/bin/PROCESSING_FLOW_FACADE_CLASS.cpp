/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : PROCESSING_FLOW_FACADE_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
#include "PROCESSING_FLOW_FACADE_CLASS.h"
namespace RC
{
    /*
     * @brief Constructor
    **/ 
    PROCESSING_FLOW_FACADE_CLASS::PROCESSING_FLOW_FACADE_CLASS()
    {

    }

    /*
     * @brief Constructor
    **/ 
    PROCESSING_FLOW_FACADE_CLASS::~PROCESSING_FLOW_FACADE_CLASS()
    {

    }

    /*
     * @brief Connect
     * @param option
     * @param fd
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::connect(IN RC4A_OPTION_STRUCT * option, INOUT ZOO_FD * fd)
    {
        ZOO_INT32 error_code = OK;
        __RC_TRY
        {
           /*
            * @step1 : check state is valid to do
           **/
           this->m_device_controller->connect(option,fd);     
        }
        __RC_CATCH_ALL(error_code)
        return error_code;
    }

    /*
     * @brief Disconnect
     * @param fd
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::disconnect(IN ZOO_FD fd)
    {
        ZOO_INT32 error_code = OK;
        __RC_TRY
        {
            this->m_device_controller->disconnect(fd);     
        }
        __RC_CATCH_ALL(error_code)
        return error_code;
    }

    /*
     * @brief get fd
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::get_fd(ZOO_CHAR * client_id,ZOO_FD *fd)
    {
        ZOO_INT32 error_code = OK;
        __RC_TRY
        {
            this->m_device_controller->get_fd(client_id,fd);     
        }
        __RC_CATCH_ALL(error_code)
        return error_code;
    }

    /*
     * @brief Get connected
     * @param fd
     * @param is_connected
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::get_connected(IN ZOO_FD fd,INOUT ZOO_BOOL * is_connected)
    {
        ZOO_INT32 error_code = OK;
        __RC_TRY
        {
           /*
            * @step1 : check state is valid to do
           **/
           *is_connected = this->m_device_controller->get_connected(fd);      
        }
        __RC_CATCH_ALL(error_code)
        return error_code;
    }

    /*
     * @brief Set subscribe topic
     * @param fd
     * @param topic[RC4A_BUFFER_LENGTH]
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::set_subscribe_topic(IN ZOO_FD fd,
												                      IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH])
    {
        ZOO_INT32 error_code = OK;
        __RC_TRY
        {
            std::string t = topic;
            this->m_device_controller->set_subscribe_topic(fd,t);
        }
        __RC_CATCH_ALL(error_code)
        return error_code;
    }

    /*
     * @brief Remove subscribe topic
     * @param fd
     * @param topic[RC4A_BUFFER_LENGTH]
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::remove_subscribe_topic(IN ZOO_FD fd,
														                    IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH])
    {
        ZOO_INT32 error_code = OK;
        __RC_TRY
        {
            std::string t = topic;
            this->m_device_controller->remove_subscribe_topic(fd,t);
        }
        __RC_CATCH_ALL(error_code)
        return error_code;
    }

    /*
     * @brief Clear db
     * @param fd
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::clear_db(IN ZOO_FD fd)
    {
        ZOO_INT32 error_code = OK;
        __RC_TRY
        {
            this->m_device_controller->clear_db(fd);
        }
        __RC_CATCH_ALL(error_code)
        return error_code;
    }

    /*
     * @brief Publish
     * @param fd
     * @param topic[RC4A_BUFFER_LENGTH]
     * @param message[RC4A_MESSAGE_LENGTH]
     * @param message_length
     * @param message_id
     * @param message_number
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::publish(IN ZOO_FD fd,
											    IN ZOO_CHAR topic[RC4A_BUFFER_LENGTH],
											    IN ZOO_CHAR message[RC4A_MESSAGE_LENGTH],
											    IN ZOO_INT32 message_length,
											    IN ZOO_INT32 message_id,
											    IN ZOO_INT32 message_number)
    {
        ZOO_INT32 error_code = OK;
        __RC_TRY
        {
            std::string t(topic,RC4A_BUFFER_LENGTH);
            std::string m(message,message_length);
            this->m_device_controller->publish(fd,t,m,message_id,message_number);  
        }
        __RC_CATCH_ALL(error_code)
        return error_code;
    }

    /*
     * @brief This method is executed when property changed value
     * @param model             The model type
     * @param property_name     The property has been changed value
    **/ 
    void PROCESSING_FLOW_FACADE_CLASS::on_property_changed(CONTROLLER_INTERFACE * model,const ZOO_UINT32 property_name)
    {
         ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");
         //TODO: ...
         ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit ...");
    }
} //namespace RC
