/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : DEVICE_INTERFACE.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/

#include "DEVICE_INTERFACE.h"
namespace RC
{
    /*
     * @brief Constructor
    **/ 
    DEVICE_INTERFACE::DEVICE_INTERFACE()
    {
    }

    /*
     * @brief Destructor
    **/ 
    DEVICE_INTERFACE::~DEVICE_INTERFACE()
    {
    }

    /*
     * @brief Add observer will be notified when property changed.
     * @param observer  Property changed observer
    **/ 
    void DEVICE_INTERFACE::add_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer)
    {
        this->remove_observer(observer);
        this->m_observers.push_back(observer);
    }

    /*
     * @brief Remove observer will be notified when property changed.
     * @param observer  Property changed observer
    **/ 
    void DEVICE_INTERFACE::remove_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer)
    {
        std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>*>::iterator observer_itr =
            this->m_observers.begin();
        while (observer_itr != this->m_observers.end())
        {
            PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* find_observer = *observer_itr;
            if (observer == find_observer)
            {
                this->m_observers.erase(observer_itr);
                break;
            }
        }
    }

    /*
     * @brief Clean.
    **/
    void DEVICE_INTERFACE::clean()
    {
        this->m_observers.clear();
    }

    /*
     * @brief Notify property has been changed.
     * @param property_name     The property has been changed
    **/ 
    void DEVICE_INTERFACE::notify_of_property_changed(const ZOO_UINT32 property_name)
    {
        for (vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>*>::iterator observer_itr =
            this->m_observers.begin(); observer_itr != this->m_observers.end(); observer_itr++)
        {
            PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer = *observer_itr;
            if (observer != nullptr)
            {
                observer->on_property_changed(this,property_name);
            }
        }
    }

} // namespace RC

