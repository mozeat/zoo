/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : FLOW_FACADE_ABSTRACT_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include "FLOW_FACADE_ABSTRACT_CLASS.h"
namespace RC
{
    /*
     * @brief Constructor
    **/ 
    FLOW_FACADE_ABSTRACT_CLASS::FLOW_FACADE_ABSTRACT_CLASS()
    {

    }

    /*
     * @brief Constructor
    **/ 
    FLOW_FACADE_ABSTRACT_CLASS::~FLOW_FACADE_ABSTRACT_CLASS()
    {

    }

    /*
     * @brief Set device controller instance
    **/ 
    void FLOW_FACADE_ABSTRACT_CLASS::set_device_controller(IN boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE> device_controller)
    {
        this->m_device_controller = device_controller;
    }

    /*
     * @brief Get device controller instance
    **/ 
    boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE>  FLOW_FACADE_ABSTRACT_CLASS::get_device_controller()
    {
        return this->m_device_controller;
    }

    /*
     * @brief Set state manager instance
    **/ 
    void FLOW_FACADE_ABSTRACT_CLASS::set_state_manager(IN boost::shared_ptr<STATE_MANAGER_CLASS> state_manager)
    {
        this->m_state_manager = state_manager;
    }

    /*
     * @brief Get state manager instance
    **/ 
    boost::shared_ptr<STATE_MANAGER_CLASS>  FLOW_FACADE_ABSTRACT_CLASS::get_state_manager()
    {
        return this->m_state_manager;
    }

    /*
     * @brief This method is executed when property changed value
     * @param model The source object contains property changed
     * @param property_name The property has been changed value
    **/ 
    void FLOW_FACADE_ABSTRACT_CLASS::on_property_changed(IN CONTROLLER_INTERFACE* model,IN const ZOO_UINT32 property_name)
    {
        // The implementation for this method will be written in concrete classes
    }

    /*
     * @brief Check current driver state is ...
     * @param count    the driver state need to be verified
     * @exceptions     __THROW_XX_EXCEPTION
    **/
    void FLOW_FACADE_ABSTRACT_CLASS::check_driver_state_is(ZOO_INT32 count, ...)
    {
         __ZOO_TRACE(RC4I_COMPONET_ID,"> function entry ...");
         ZOO_BOOL valid = ZOO_FALSE;
         ENUM_CONVERTER(ZOO_DRIVER_STATE_ENUM) driver_state_converter;
         va_list ap;
         va_start(ap,count);
         ZOO_DRIVER_STATE_ENUM current_driver_state = this->m_state_manager->get_driver_state();
         for(ZOO_INT32 i = 0; i < count ; ++i)
         {
             int state = static_cast<ZOO_DRIVER_STATE_ENUM>(va_arg(ap,ZOO_INT32));
             if(state == current_driver_state)
             {
                  valid =  ZOO_TRUE;
                  break;
             }
         }

         if(!valid)
         {
             ZOO_CHAR std_err[128];
             sprintf(std_err,"the driver state[%s] is not valid;",driver_state_converter.to_string(current_driver_state));
             __THROW_RC_EXCEPTION(RC4A_ILLEGAL_CALL_ERR,std_err,NULL);
         }
         va_end(ap);
         __ZOO_TRACE(RC4I_COMPONET_ID,"< function exit ...");
    }

} //namespace RC

