/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : MOCK_DATA_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include "MOCK_DATA_PARSER_CLASS.h"
namespace RC
{
   /*
    * @brief Constructor
    **/ 
    MOCK_DATA_CLASS::MOCK_DATA_CLASS()

    {
    }

    /*
     * @brief Destructor
    **/ 
    MOCK_DATA_CLASS::~MOCK_DATA_CLASS()
    {
    }

    /*
     * @brief Add data
     * @param name
     * @param value
     **/
    void MOCK_DATA_CLASS::add_data(std::string name,std::string value)
    {
        this->m_data_map[name] = value;
    }

    /*
     * @brief Get data
     * @param name
     **/
    std::string MOCK_DATA_CLASS::get_data(std::string name)
    {
        std::map<std::string,std::string>::iterator ite = this->m_data_map.begin();
        while( ite != this->m_data_map.end())
        {
            if(ite->first == name)
            {
                return ite->second;
            }
            ite ++;
        }
        return "";
    }
}
