/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_HARDWARE_ABSTRACT_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include "RC_HARDWARE_ABSTRACT_CLASS.h"
namespace RC
{
    /*
     * @brief Constructor
    **/ 
    RC_HARDWARE_ABSTRACT_CLASS::RC_HARDWARE_ABSTRACT_CLASS():m_fd(0)
    {

    }

    /*
     * @brief Constructor
    **/ 
    RC_HARDWARE_ABSTRACT_CLASS::~RC_HARDWARE_ABSTRACT_CLASS()
    {

    }

    /*
     * @brief brief Add observer will be notified when property changed
     * @param observer  Property changed observer
    **/ 
    void RC_HARDWARE_ABSTRACT_CLASS::add_observer(IN PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer)
    {
        this->m_observers.push_back(observer);
    }

    /*
     * @brief Add observer will be notified when property changed
     * @param observer  Property changed observer
    **/ 
    void RC_HARDWARE_ABSTRACT_CLASS::remove_observer(IN PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer)
    {
        // TODO: implement clean up later
    }

    /*
     * @brief Clean.
    **/ 
    void RC_HARDWARE_ABSTRACT_CLASS::clean()
    {
        // TODO: implement clean up later
    }

    /*
     * @brief This method is executed when property changed value
     * @param model The source object contains property changed
     * @param property_name The property has been changed value
    **/ 
    void RC_HARDWARE_ABSTRACT_CLASS::notify_of_property_changed(IN const ZOO_UINT32 property_name)
    {
        std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>*>::iterator observer_itr =
            this->m_observers.begin();
        while (observer_itr != this->m_observers.end())
        {
            PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer = *observer_itr;
            observer->on_property_changed(this, property_name);
            observer_itr ++;
        }
    }

    /*
     * @brief initialize.
    **/ 
    void RC_HARDWARE_ABSTRACT_CLASS::initialize()
    {
        // TODO: Implemented in an inheritance model
    }

    /*
     * @brief terminate.
    **/ 
    void RC_HARDWARE_ABSTRACT_CLASS::terminate()
    {
        // TODO: Implemented in an inheritance model
    }

    void RC_HARDWARE_ABSTRACT_CLASS::set_fd(ZOO_INT32 fd)
    {
        this->m_fd = fd;
    }
} //namespace RC

