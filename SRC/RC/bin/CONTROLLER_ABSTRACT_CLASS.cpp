/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : CONTROLLER_ABSTRACT_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include "CONTROLLER_ABSTRACT_CLASS.h"
namespace RC
{
    /*
     * @brief Constructor
    **/ 
    CONTROLLER_ABSTRACT_CLASS::CONTROLLER_ABSTRACT_CLASS()
    {

    }

    /*
     * @brief Constructor
    **/ 
    CONTROLLER_ABSTRACT_CLASS::~CONTROLLER_ABSTRACT_CLASS()
    {
  
    }

    /*
     * @brief Get the event_publisher attribute value
    **/ 
    boost::shared_ptr<EVENT_PUBLISHER_CLASS> CONTROLLER_ABSTRACT_CLASS::get_event_publisher()
    {
        return this->m_event_publisher;
    }

    /*
     * @brief Set the event_publisher attribute value
     * @param event_publisher    The new event_publisher attribute value
    **/ 
    void CONTROLLER_ABSTRACT_CLASS::set_event_publisher(IN boost::shared_ptr<EVENT_PUBLISHER_CLASS> event_publisher)
    {
        this->m_event_publisher = event_publisher;
    }

    /*
      * @brief brief Add observer will be notified when property changed
      * @param observer  Property changed observer
    **/ 
    void CONTROLLER_ABSTRACT_CLASS::add_observer(IN PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* observer)
    {
        this->m_observers.push_back(observer);
    }

    /*
      * @brief Add observer will be notified when property changed
      * @param observer  Property changed observer
    **/ 
    void CONTROLLER_ABSTRACT_CLASS::remove_observer(IN  PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* observer)
    {
        // TODO: implement clean up later
    }

    /*
     * @brief Clean.
    **/ 
    void CONTROLLER_ABSTRACT_CLASS::clean()
    {
        // TODO: implement clean up later
    }

    /*
     * @brief This method is executed when property changed value
     * @param model The source object contains property changed
     * @param property_name The property has been changed value
    **/ 
    void CONTROLLER_ABSTRACT_CLASS::notify_of_property_changed(IN const ZOO_UINT32 property_name)
    {
        std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>*>::iterator observer_itr =
            this->m_observers.begin();
        while (observer_itr != this->m_observers.end())
        {
            PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* observer = *observer_itr;;
            observer->on_property_changed(this, property_name);
            observer_itr ++;
        }
    }
} //namespace RC

