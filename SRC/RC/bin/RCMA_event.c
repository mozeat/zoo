/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : RCMA_event.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
#include <RCMA_event.h>

/**
 *@brief RCMA_raise_4A_connect
 *@param option
 *@param fd
**/
ZOO_INT32 RCMA_raise_4A_connect(IN ZOO_INT32 error_code,
                                    IN ZOO_FD fd,
                                    IN RC4I_REPLY_HANDLE reply_handle)
{
    RC4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = RC4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = RC4I_get_reply_message_length(RC4A_CONNECT_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (RC4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = RC4A_CONNECT_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.connect_rep_msg.fd,&fd,sizeof(ZOO_FD));
                rtn = RC4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief RCMA_raise_4A_disconnect
 *@param fd
**/
ZOO_INT32 RCMA_raise_4A_disconnect(IN ZOO_INT32 error_code,
                                       IN RC4I_REPLY_HANDLE reply_handle)
{
    RC4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = RC4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = RC4I_get_reply_message_length(RC4A_DISCONNECT_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (RC4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = RC4A_DISCONNECT_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = RC4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief RCMA_raise_4A_get_fd
 *@param client_id
 *@param fd
**/
ZOO_INT32 RCMA_raise_4A_get_fd(IN ZOO_INT32 error_code,
                                   IN ZOO_FD fd,
                                   IN RC4I_REPLY_HANDLE reply_handle)
{
    RC4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = RC4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = RC4I_get_reply_message_length(RC4A_GET_FD_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (RC4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = RC4A_GET_FD_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.get_fd_rep_msg.fd,&fd,sizeof(ZOO_FD));
                rtn = RC4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief RCMA_raise_4A_get_connected
 *@param fd
 *@param is_connected
**/
ZOO_INT32 RCMA_raise_4A_get_connected(IN ZOO_INT32 error_code,
                                          IN ZOO_BOOL is_connected,
                                          IN RC4I_REPLY_HANDLE reply_handle)
{
    RC4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = RC4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = RC4I_get_reply_message_length(RC4A_GET_CONNECTED_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (RC4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = RC4A_GET_CONNECTED_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.get_connected_rep_msg.is_connected,&is_connected,sizeof(ZOO_BOOL));
                rtn = RC4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief RCMA_raise_4A_set_subscribe_topic
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
**/
ZOO_INT32 RCMA_raise_4A_set_subscribe_topic(IN ZOO_INT32 error_code,
                                                IN RC4I_REPLY_HANDLE reply_handle)
{
    RC4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = RC4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = RC4I_get_reply_message_length(RC4A_SET_SUBSCRIBE_TOPIC_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (RC4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = RC4A_SET_SUBSCRIBE_TOPIC_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = RC4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief RCMA_raise_4A_remove_subscribe_topic
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
**/
ZOO_INT32 RCMA_raise_4A_remove_subscribe_topic(IN ZOO_INT32 error_code,
                                                   IN RC4I_REPLY_HANDLE reply_handle)
{
    RC4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = RC4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = RC4I_get_reply_message_length(RC4A_REMOVE_SUBSCRIBE_TOPIC_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (RC4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = RC4A_REMOVE_SUBSCRIBE_TOPIC_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = RC4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief RCMA_raise_4A_clear_db
 *@param fd
**/
ZOO_INT32 RCMA_raise_4A_clear_db(IN ZOO_INT32 error_code,
                                     IN RC4I_REPLY_HANDLE reply_handle)
{
    RC4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = RC4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = RC4I_get_reply_message_length(RC4A_CLEAR_DB_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (RC4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = RC4A_CLEAR_DB_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = RC4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief RCMA_raise_4A_publish
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
 *@param message[RC4A_MESSAGE_LENGTH]
 *@param message_length
 *@param message_id
 *@param message_number
**/
ZOO_INT32 RCMA_raise_4A_publish(IN ZOO_INT32 error_code,
                                    IN RC4I_REPLY_HANDLE reply_handle)
{
    RC4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = RC4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = RC4I_get_reply_message_length(RC4A_PUBLISH_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (RC4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = RC4A_PUBLISH_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = RC4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = RC4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief RC4A_connect_status_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
void RCMA_raise_4A_connect_status_subscribe(IN RC4A_CONNECTION_STATUS_STRUCT status,IN ZOO_INT32 error_code,IN void *context)
{
    ZOO_INT32 rtn = OK;
    RC4I_REPLY_STRUCT * reply_message = NULL;
    reply_message = (RC4I_REPLY_STRUCT * ) MM4A_malloc(sizeof(RC4I_REPLY_STRUCT));
    if(NULL == reply_message)
    {
        rtn = RC4A_PARAMETER_ERR;
    }
    
    if(OK == rtn)
    {
        reply_message->reply_header.function_code = RC4A_CONNECT_STATUS_SUBSCRIBE_CODE;
        reply_message->reply_header.execute_result = error_code;
        memcpy(&reply_message->reply_body.connect_status_subscribe_code_rep_msg.status,&status,sizeof(RC4A_CONNECTION_STATUS_STRUCT));
    }

    if(OK == rtn)
    {
        rtn = RC4I_publish_event(RC4A_SERVER,
                                            RC4A_CONNECT_STATUS_SUBSCRIBE_CODE,
                                            reply_message);
    }

    if(OK != rtn)
    {
        
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }

    return;
}
/**
 *@brief RC4A_message_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
void RCMA_raise_4A_message_subscribe(IN RC4A_MESSAGE_STRUCT * status,IN ZOO_INT32 error_code,IN void *context)
{
    ZOO_INT32 rtn = OK;
    RC4I_REPLY_STRUCT * reply_message = NULL;
    reply_message = (RC4I_REPLY_STRUCT * ) MM4A_malloc(sizeof(RC4I_REPLY_STRUCT));
    if(NULL == reply_message)
    {
        rtn = RC4A_PARAMETER_ERR;
    }
    
    if(OK == rtn)
    {
        reply_message->reply_header.function_code = RC4A_MESSAGE_SUBSCRIBE_CODE;
        reply_message->reply_header.execute_result = error_code;
        memcpy(&reply_message->reply_body.message_subscribe_code_rep_msg.status,status,sizeof(RC4A_MESSAGE_STRUCT));
    }

    if(OK == rtn)
    {
        rtn = RC4I_publish_event(RC4A_SERVER,
                                            RC4A_MESSAGE_SUBSCRIBE_CODE,
                                            reply_message);
    }

    if(OK != rtn)
    {
        
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }

    return;
}

