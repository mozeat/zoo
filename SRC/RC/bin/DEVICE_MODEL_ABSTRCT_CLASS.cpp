/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : DEVICE_MODEL_ABSTRCT_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include "DEVICE_MODEL_ABSTRCT_CLASS.h"
namespace RC
{
    /*
     * @brief Constructor
     **/ 
    DEVICE_MODEL_ABSTRCT_CLASS::DEVICE_MODEL_ABSTRCT_CLASS():m_fd(0)
    {
        
    }
    
    /*
     * @brief Destructor
     **/ 
    DEVICE_MODEL_ABSTRCT_CLASS::~DEVICE_MODEL_ABSTRCT_CLASS()
    {
    
    }
    std::string DEVICE_MODEL_ABSTRCT_CLASS::create_uuid()
    {
        boost::uuids::uuid a_uuid = boost::uuids::random_generator()();
        const string tmp_uuid = boost::uuids::to_string(a_uuid);
        return tmp_uuid;
    }
        
    ZOO_INT32 DEVICE_MODEL_ABSTRCT_CLASS::get_fd()
    {
        return this->m_fd;
    }
}
