/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : RCMA_implement.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
#include <RCMA_implement.h>
#include "RC_FLOW_FACADE_WRAPPER.h"

/**
 *@brief RCMA_implement_4A_connect
 *@param option
 *@param fd
**/
void RCMA_implement_4A_connect(IN RC4A_OPTION_STRUCT* option,
                                   IN RC4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    ZOO_FD fd;
    /* User add some code here if had special need. */
    rtn = RC_connect(option,&fd);

    /* User add ... END*/
    RCMA_raise_4A_connect(rtn,fd,reply_handle);
}

/**
 *@brief RCMA_implement_4A_disconnect
 *@param fd
**/
void RCMA_implement_4A_disconnect(IN ZOO_FD fd,
                                      IN RC4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = RC_disconnect(fd);

    /* User add ... END*/
    RCMA_raise_4A_disconnect(rtn,reply_handle);
}

/**
 *@brief RCMA_implement_4A_get_fd
 *@param client_id
 *@param fd
**/
void RCMA_implement_4A_get_fd(IN ZOO_CHAR* client_id,
                                  IN RC4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    ZOO_FD fd = 0;
    /* User add some code here if had special need. */
    rtn = RC_get_fd(client_id,&fd);

    /* User add ... END*/
    RCMA_raise_4A_get_fd(rtn,fd,reply_handle);
}

/**
 *@brief RCMA_implement_4A_get_connected
 *@param fd
 *@param is_connected
**/
void RCMA_implement_4A_get_connected(IN ZOO_FD fd,
                                         IN RC4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    ZOO_BOOL is_connected  = ZOO_FALSE;
    /* User add some code here if had special need. */
    rtn = RC_get_connected(fd,&is_connected);

    /* User add ... END*/
    RCMA_raise_4A_get_connected(rtn,is_connected,reply_handle);
}

/**
 *@brief RCMA_implement_4A_set_subscribe_topic
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
**/
void RCMA_implement_4A_set_subscribe_topic(IN ZOO_FD fd,
                                               IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH],
                                               IN RC4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = RC_set_subscribe_topic(fd,topic);

    /* User add ... END*/
    RCMA_raise_4A_set_subscribe_topic(rtn,reply_handle);
}

/**
 *@brief RCMA_implement_4A_remove_subscribe_topic
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
**/
void RCMA_implement_4A_remove_subscribe_topic(IN ZOO_FD fd,
                                                  IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH],
                                                  IN RC4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = RC_remove_subscribe_topic(fd,topic);

    /* User add ... END*/
    RCMA_raise_4A_remove_subscribe_topic(rtn,reply_handle);
}

/**
 *@brief RCMA_implement_4A_clear_db
 *@param fd
**/
void RCMA_implement_4A_clear_db(IN ZOO_FD fd,
                                    IN RC4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = RC_clear_db(fd);

    /* User add ... END*/
    RCMA_raise_4A_clear_db(rtn,reply_handle);
}

/**
 *@brief RCMA_implement_4A_publish
 *@param fd
 *@param topic[RC4A_BUFFER_LENGTH]
 *@param message[RC4A_MESSAGE_LENGTH]
 *@param message_length
 *@param message_id
 *@param message_number
**/
void RCMA_implement_4A_publish(IN ZOO_FD fd,
                                   IN ZOO_CHAR topic[RC4A_BUFFER_LENGTH],
                                   IN ZOO_CHAR message[RC4A_MESSAGE_LENGTH],
                                   IN ZOO_INT32 message_length,
                                   IN ZOO_INT32 message_id,
                                   IN ZOO_INT32 message_number,
                                   IN RC4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = RC_publish(fd,topic,message,message_length,message_id,message_number);

    /* User add ... END*/
    RCMA_raise_4A_publish(rtn,reply_handle);
}
