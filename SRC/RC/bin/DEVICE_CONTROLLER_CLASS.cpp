/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : DEVICE_CONTROLLER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include "DEVICE_CONTROLLER_CLASS.h"
#include "RC_MODEL_CLASS.h"
namespace RC
{
    /*
     * @brief Constructor
    **/ 
    DEVICE_CONTROLLER_CLASS::DEVICE_CONTROLLER_CLASS()
    {

    }

    /*
     * @brief Destructor
    **/ 
    DEVICE_CONTROLLER_CLASS::~DEVICE_CONTROLLER_CLASS()
    {

    }

    /*
     * @brief Initialize the model
     * @return throw exception 
    **/ 
    void DEVICE_CONTROLLER_CLASS::connect(IN RC4A_OPTION_STRUCT * option, INOUT ZOO_FD * fd)
    {
        *fd = this->create_model(option);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: fd:%02x",*fd);
        boost::shared_ptr<RC_MODEL_CLASS> model = this->get_model(*fd);
        if(model)
        {      
            model->create(option->connection.addr,option->connection.port,option->connection.client_id,&option->persistence);
            model->connect(&option->connection,&option->ssl);
        }
        else
        {
            __THROW_RC_EXCEPTION(RC4A_PARAMETER_ERR,"create mqtt client fail", NULL);
        }
    }

    /*
     * @brief Terminate the model
     * @return throw exception 
    **/ 
    void DEVICE_CONTROLLER_CLASS::disconnect(IN ZOO_FD fd) 
    {
        boost::shared_ptr<RC_MODEL_CLASS> model = this->get_model(fd);
        if(model)
        {           
            model->disconnect();
        }
    }

    /*
     * @brief get fd
    **/
    void DEVICE_CONTROLLER_CLASS::get_fd(ZOO_CHAR * client_id,ZOO_FD *fd)
    {
        *fd = -1;
        std::map<ZOO_FD,boost::shared_ptr<RC_MODEL_CLASS> >::iterator ite = this->m_entity_devices.begin();
        while(ite != this->m_entity_devices.end())
        { 
            boost::shared_ptr<RC_MODEL_CLASS> model = ite->second;
            if(model)
            {
                if(model->get_client_id() == client_id)
                {
                    *fd = model->get_fd();
                    break;
                }
            }
            ++ ite;
        }
    }

    /*
     * @brief Change to work mode
    **/ 
    ZOO_BOOL DEVICE_CONTROLLER_CLASS::get_connected(IN ZOO_FD fd)
    {
        boost::shared_ptr<RC_MODEL_CLASS> model = this->get_model(fd);
        if(model)
        {           
            return model->is_online();
        }
        return ZOO_FALSE;
    }

    /*
     * @brief Create all models
    **/ 
    void DEVICE_CONTROLLER_CLASS::set_subscribe_topic(IN ZOO_FD fd,
												                      IN std::string & topic)
    {
        boost::shared_ptr<RC_MODEL_CLASS> model = this->get_model(fd);
        if(model)
        {
            model->set_subscribe_topic(topic);
        }
    }

    /*
     * @brief Load device from config database or file.
     **/ 
    void DEVICE_CONTROLLER_CLASS::remove_subscribe_topic(IN ZOO_FD fd,
														                    IN std::string & topic)
    {
        boost::shared_ptr<RC_MODEL_CLASS> model = this->get_model(fd);
        if(model)
        {
            model->remove_subscribe_topic(topic);
        }
    }

    /*
     * @brief Create one model by the given type.
     **/ 
    void DEVICE_CONTROLLER_CLASS::clear_db(IN ZOO_FD fd)
    {
         ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");
        //create device model and insert into m_entity_devices container.
        //TODO: 
         ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit ...");
    }

    /*
     * @brief Create one model by the given type.
     **/ 
    void DEVICE_CONTROLLER_CLASS::publish(IN ZOO_FD fd,
                                                    IN std::string & topic,
        									        IN std::string & message,
        									        IN ZOO_INT32 message_id,
        									        IN ZOO_INT32 message_number)
    {
        boost::shared_ptr<RC_MODEL_CLASS> model = this->get_model(fd);
        if(model)
        {
            model->publish(topic,message,message_id,message_number);
        }
    }

    /*
     * @brief Notify property has been changed
     * @property_name     The property has been changed
    **/ 
    void DEVICE_CONTROLLER_CLASS::on_property_changed(MARKING_MODEL_INTERFACE* model,const ZOO_UINT32 property_name)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");
        //example : ...
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit ...");
    }

    ZOO_FD DEVICE_CONTROLLER_CLASS::create_model(IN RC4A_OPTION_STRUCT * option)
    {
        static ZOO_FD fd = 0x52430000;
        boost::shared_ptr<RC_MODEL_CLASS> model = boost::make_shared<RC_MODEL_CLASS>();
        model->set_fd(++fd);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: fd:%02x",fd);
        this->m_entity_devices[fd] = model;
        
        return fd;
    }

    boost::shared_ptr<RC_MODEL_CLASS> DEVICE_CONTROLLER_CLASS::get_model(IN ZOO_FD fd)
    {
        boost::shared_ptr<RC_MODEL_CLASS> model;
        std::map<ZOO_FD,boost::shared_ptr<RC_MODEL_CLASS> >::iterator ite = this->m_entity_devices.find(fd);
        if(ite != this->m_entity_devices.end())
        { 
            model = ite->second;
        }
        return model;
    }
} //namespace RC
