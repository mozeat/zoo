/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_CONFIGURE.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include "RC_CONFIGURE.h"
namespace RC
{
    /*
     * @brief Define base relative path.
    **/ 
    static const char * CFG_RELATIVE_PATH = "";

    /*
     * @brief Number level forward back from execute base to relative path.
    **/ 
    static const ZOO_INT32 NUMBER_LEVEL = 3;

    /*
     * @brief Define base relative path.
    **/ 
    static const char * SLASH = "/";

    /*
     * @brief Define base relative path.
    **/ 
    static const char * SIMULATE_DATA_FILE = "RC/mock_data/RC_mock_data.ini";

    /*
     * @brief The instance
    **/
    boost::shared_ptr<RC_CONFIGURE> RC_CONFIGURE::m_instance = NULL;

    /*
     * @brief Constructor
    **/ 
    RC_CONFIGURE::RC_CONFIGURE()
    {

    }

    /*
     * @brief Destructor
    **/ 
    RC_CONFIGURE::~RC_CONFIGURE()
    {

    }

    /*
     * @brief Get instance
    **/
    boost::shared_ptr<RC_CONFIGURE> RC_CONFIGURE::get_instance()
    {
        if(RC_CONFIGURE::m_instance == nullptr)
        {
            RC_CONFIGURE::m_instance.reset(new RC_CONFIGURE());
        }
        return RC_CONFIGURE::m_instance;
    }

    /*
     * @brief Initialize
    **/
    void RC_CONFIGURE::initialize()
    {
        RC_CONFIGURE::m_instance->reload();
    }

    /*
     * @brief Get execute path
    **/
    std::string RC_CONFIGURE::get_execute_path()
    {
        return this->m_execute_path;
    }

    /*
     * @brief Get simulation file path
    **/
    std::string RC_CONFIGURE::get_mock_file()
    {
        return this->m_simulation_file_path;
    }

    /*
     * @brief Reload configurations
    **/
    void RC_CONFIGURE::reload()
    {
        this->m_execute_path = ZOO_COMMON::ENVIRONMENT_UTILITY_CLASS::get_execute_path();
        std::string configure_base = ZOO_get_pl_cfg_base_dir();
        this->m_simulation_file_path = 
            ZOO_COMMON::ENVIRONMENT_UTILITY_CLASS::combine_path(configure_base,std::string(CFG_RELATIVE_PATH) + SLASH + SIMULATE_DATA_FILE);
    }
} //namespace RC

