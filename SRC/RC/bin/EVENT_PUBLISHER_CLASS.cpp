/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : EVENT_PUBLISHER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include "EVENT_PUBLISHER_CLASS.h"
#include "RCMA_event.h"

namespace RC
{
    /*
     * @brief Constructor
    **/ 
    EVENT_PUBLISHER_CLASS::EVENT_PUBLISHER_CLASS()
    {

    }

    /*
     * @brief Constructor
    **/ 
    EVENT_PUBLISHER_CLASS::~EVENT_PUBLISHER_CLASS()
    {

    }

    /*
    * @brief Publish connect_status changed event to subscriber.
    * @param status
    * @param error_code
    * @param *context
    **/ 
    void EVENT_PUBLISHER_CLASS::publish_RCMA_raise_4A_connect_status_subscribe(RC4A_CONNECTION_STATUS_STRUCT status,ZOO_INT32 error_code,void *context)
    {
        RCMA_raise_4A_connect_status_subscribe(status,error_code,context);
    }

    /*
    * @brief Publish message changed event to subscriber.
    * @param status
    * @param error_code
    * @param *context
    **/ 
    void EVENT_PUBLISHER_CLASS::publish_RCMA_raise_4A_message_subscribe(RC4A_MESSAGE_STRUCT *status,ZOO_INT32 error_code,void *context)
    {
        RCMA_raise_4A_message_subscribe(status,error_code,context);
    }

} //namespace RC

