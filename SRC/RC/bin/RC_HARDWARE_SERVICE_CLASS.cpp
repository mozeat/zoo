/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_HARDWARE_SERVICE_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include "RC_HARDWARE_SERVICE_CLASS.h"
namespace RC
{
    /*
     * @brief Constructor
     **/ 
    RC_HARDWARE_SERVICE_CLASS::RC_HARDWARE_SERVICE_CLASS()
    {
        this->m_mqtt_handle = NULL;
        this->m_connect_option = MQTTClient_connectOptions_initializer;
    }

    /*
     * @brief Constructor
     **/ 
    RC_HARDWARE_SERVICE_CLASS::~RC_HARDWARE_SERVICE_CLASS()
    {
        if(this->m_mqtt_handle)
            MQTTClient_destroy(&this->m_mqtt_handle);
    }

    /*
    * @brief Create an instance.
    * @brief url
    * @brief client_id
    **/ 
    void RC_HARDWARE_SERVICE_CLASS::create(std::string url,std::string client_id)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: url:%s",url.data());
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: client_id:%s",client_id.data());
        if(NULL != this->m_mqtt_handle)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,":: already create client");
            return;
        }
        
        ZOO_INT32 error_code = MQTTClient_create(&this->m_mqtt_handle,url.data(),client_id.data(),MQTTCLIENT_PERSISTENCE_NONE,NULL);
        if(MQTTCLIENT_SUCCESS != error_code)
        {
            __THROW_RC_EXCEPTION(RC4A_SYSTEM_ERR,"create mqtt instance fail",NULL);
        }
        
        this->m_client_id = client_id;        
        error_code = MQTTClient_setCallbacks(this->m_mqtt_handle,this,
                                                on_connection_lost,
                                                on_message_arrived,
                                                NULL);
        if(MQTTCLIENT_SUCCESS != error_code)
        {
            __THROW_RC_EXCEPTION(RC4A_SYSTEM_ERR,"set mqtt callback fail",NULL);
        }  
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit ...");
    }
    
    /*
     * @brief connect to broker.
     * @brief connect_option
     * @brief ssl_option
     **/ 
    void RC_HARDWARE_SERVICE_CLASS::connect(RC4A_CONNECT_OPTION_STRUCT * connect_option,RC4A_SSL_OPTION_STRUCT * ssl_option)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: username:%s",connect_option->user);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: password:%s",connect_option->password);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: keepalive:%d",connect_option->keepalive);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: qos:%d",connect_option->qos);
        if(this->m_mqtt_handle == NULL)
        {
            __THROW_RC_EXCEPTION(RC4A_PARAMETER_ERR,"mqtt instance is not be created",NULL);
        }
        
        {
            this->m_connect_option.username = connect_option->user;
            this->m_connect_option.password = connect_option->password;
            this->m_connect_option.keepAliveInterval = connect_option->keepalive;
            this->m_connect_option.retryInterval = connect_option->reconnect_delay;
            this->m_connect_option.MQTTVersion = MQTTVERSION_DEFAULT;
            this->m_connect_option.connectTimeout = 30;
            this->m_connect_option.cleansession = 1;
        }
        
        MQTTClient_willOptions wopts = MQTTClient_willOptions_initializer;
        {
            this->m_connect_option.will = &wopts;
            this->m_connect_option.will->message = "hello world";
            this->m_connect_option.will->qos = connect_option->qos;
            this->m_connect_option.will->retained = 0;
            this->m_connect_option.will->topicName = "bye";
        }
        
        MQTTClient_SSLOptions sslopts = MQTTClient_SSLOptions_initializer;
        if(ssl_option != NULL)
        {
            if(ssl_option->enable_ssl)
            {
                this->m_connect_option.ssl = & sslopts;
                this->m_connect_option.ssl->trustStore = ssl_option->trust_chain_file;
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: trustStore:%s",this->m_connect_option.ssl->trustStore);
                this->m_connect_option.ssl->keyStore = ssl_option->public_key_file;
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: keyStore:%s",this->m_connect_option.ssl->keyStore);
                this->m_connect_option.ssl->privateKey = ssl_option->private_key_file;
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: privateKey:%s",this->m_connect_option.ssl->privateKey);
                this->m_connect_option.ssl->sslVersion = ssl_option->tls_v;
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: sslVersion:%d",this->m_connect_option.ssl->sslVersion);
            }
        }
        
        this->m_qos = connect_option->qos;
        ZOO_INT32 error_code = MQTTClient_connect(this->m_mqtt_handle,&this->m_connect_option);
        if(MQTTCLIENT_SUCCESS != error_code)
        {
            const char * strerror = MQTTClient_strerror(error_code);
            ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,":: error_code:%d,why:%s",error_code,strerror);
            __THROW_RC_EXCEPTION(RC4A_SYSTEM_ERR,"connect fail",NULL);
        }
        
        this->notify_connection_state(ZOO_TRUE);
        
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit ...");
    }

    /*
     * @brief Disconnect.
     **/ 
    void RC_HARDWARE_SERVICE_CLASS::disconnect()
    {
        if(NULL != this->m_mqtt_handle)
        {
            ZOO_INT32 error_code = MQTTClient_disconnect(this->m_mqtt_handle,1000);
            if(MQTTCLIENT_SUCCESS != error_code)
            {
                __THROW_RC_EXCEPTION(RC4A_SYSTEM_ERR,"disconnect fail",NULL);
            }
            this->m_connect_option = MQTTClient_connectOptions_initializer;
            MQTTClient_destroy(&this->m_mqtt_handle);
        }
    }

    /*
     * @brief The connect state.
     **/ 
    ZOO_BOOL RC_HARDWARE_SERVICE_CLASS::is_connected()
    {
        if(NULL != this->m_mqtt_handle)
        {
            return MQTTClient_isConnected(this->m_mqtt_handle);
        }
        return ZOO_FALSE;
    }
   
    /*
     * @brief publish.
     * @brief topic
     * @brief message
     * @brief qos
     **/ 
    void RC_HARDWARE_SERVICE_CLASS::publish(std::string & topic,std::string & message)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: topic:%s",topic.data());
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: message size:%ld",message.size());
        if(this->m_mqtt_handle == NULL)
        {
            __THROW_RC_EXCEPTION(RC4A_PARAMETER_ERR,"mqtt instance is not be created",NULL);
        }
        
        ZOO_INT32 error_code = 
            MQTTClient_publish(this->m_mqtt_handle,topic.c_str(),message.size(),message.data(),1,0,NULL);
        if(MQTTCLIENT_SUCCESS != error_code)
        {
            const char * strerror = MQTTClient_strerror(error_code);            
            __THROW_RC_EXCEPTION(RC4A_SYSTEM_ERR,strerror,NULL);
        }
    }

   /*
    * @brief Get subscribe message.
    * @brief topic
    **/
    boost::shared_ptr<MESSAGE_CLASS>  RC_HARDWARE_SERVICE_CLASS::get_message()
    {
        return NULL;
    }

   /*
    * @brief set subscribe topic
    **/ 
    void RC_HARDWARE_SERVICE_CLASS::set_subscribe_topic(IN std::string & topic)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: topic:%s",topic.data());
        if(this->m_mqtt_handle == NULL)
        {
            __THROW_RC_EXCEPTION(RC4A_PARAMETER_ERR,"mqtt instance is not be created",NULL);
        }
        ZOO_INT32 error_code = MQTTClient_subscribe(this->m_mqtt_handle,topic.data(),1);
        if(MQTTCLIENT_SUCCESS != error_code)
        {
            const char * strerror = MQTTClient_strerror(error_code);
            __THROW_RC_EXCEPTION(RC4A_SYSTEM_ERR,strerror,NULL);
        }
    }

   /*
    * @brief remove subscribe topic
    **/ 
    void RC_HARDWARE_SERVICE_CLASS::remove_subscribe_topic(IN std::string & topic)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: topic:%s",topic.data());
        if(this->m_mqtt_handle == NULL)
        {
            __THROW_RC_EXCEPTION(RC4A_PARAMETER_ERR,"mqtt instance is not be created",NULL);
        }
        
        ZOO_INT32 error_code = MQTTClient_unsubscribe(this->m_mqtt_handle,topic.data());
        if(MQTTCLIENT_SUCCESS != error_code)
        {
            const char * strerror = MQTTClient_strerror(error_code);
            __THROW_RC_EXCEPTION(RC4A_SYSTEM_ERR,strerror,NULL);
        }
    }
       
    /*
     * @brief the callback function for connection lost.
     * @brief context
     * @brief cause
     **/ 
    void RC_HARDWARE_SERVICE_CLASS::connection_lost_callback(char* cause)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: why-%s",cause);
        this->notify_connection_state(ZOO_FALSE);
    }

    /*
     * @brief the handler for execute message_arrived callback
     **/  
    void RC_HARDWARE_SERVICE_CLASS::message_arrived_callback(std::string topic, std::string & message)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: topic-%s",topic.data());
        int message_number = message.size() / RC4A_MESSAGE_LENGTH;
        int message_remainder = message.size() % RC4A_MESSAGE_LENGTH;
        int total_message_number =  message_remainder > 0 ? 1 : 0;
        total_message_number += message_number;
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
                    ":: message_number=%d,message_remainder=%d,total_message_number=%d",
                    message_number,message_remainder,total_message_number);
        RC4A_MESSAGE_STRUCT * status = (RC4A_MESSAGE_STRUCT *)MM4A_malloc(sizeof(RC4A_MESSAGE_STRUCT));
        if(status == NULL)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: MM4A_malloc failed");
            return;
        }
        
        status->fd = this->m_fd;   
        status->number = total_message_number;
        
        if(topic.size() < RC4A_BUFFER_LENGTH)
        {
            sprintf(status->topic,"%s",topic.c_str());
        }
        
        if(message_number == 0)
        {
            status->index = message_number;
            status->length = message.size();
            memcpy(status->data,message.data(),message.size());
            RCMA_raise_4A_message_subscribe(status,OK,NULL);
        }
        else
        {
            for(int i = 0; i < message_number; ++i)
            {
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: send %i/%i message",i,total_message_number);
                memset(status,0x0,sizeof(RC4A_MESSAGE_STRUCT));
                status->fd = this->m_fd;
                const ZOO_CHAR * msg =  message.data();
                status->index = i;
                status->length =  RC4A_MESSAGE_LENGTH;
                status->number = total_message_number;
                memcpy(status->data,&msg[i * RC4A_MESSAGE_LENGTH],RC4A_MESSAGE_LENGTH);
                RCMA_raise_4A_message_subscribe(status,OK,NULL);
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: index-%d,message_number-%d",i,message_number);
            }
            
            if(message_remainder > 0)
            {
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: send %i/%i message",total_message_number,total_message_number);
                memset(status,0x0,sizeof(RC4A_MESSAGE_STRUCT));
                status->fd = this->m_fd;
                const ZOO_CHAR * msg = message.data();
                status->index = message_number;
                status->length = message_remainder;
                status->number = total_message_number;
                memcpy(status->data,&msg[message_number * RC4A_MESSAGE_LENGTH],message_remainder);
                RCMA_raise_4A_message_subscribe(status,OK,NULL);
            }
        }  
        
        if(status != NULL)
        {
            MM4A_free(status);
            status  = NULL;
        }
    }

    /*
     * @brief the callback function for trace.
     * @brief level
     * @brief message
     **/ 
    void RC_HARDWARE_SERVICE_CLASS::on_trace_message(MQTTCLIENT_TRACE_LEVELS level, char* message)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: level-%d,message-%s",level,message);
    }

    /*
     * @brief the callback function for trace.
     * @brief success   TRUE - connection  success,FALSE -lost
     **/ 
    void RC_HARDWARE_SERVICE_CLASS::notify_connection_state(ZOO_BOOL online)
    {
        RC4A_CONNECTION_STATUS_STRUCT status;
        memset(&status,0x0,sizeof(RC4A_CONNECTION_STATUS_STRUCT));
        status.fd = this->m_fd;
        status.online = online;
        RCMA_raise_4A_connect_status_subscribe(status,OK,NULL);
    }

    /*
     * @brief the callback function for trace.
     * @brief success
     **/ 
    void RC_HARDWARE_SERVICE_CLASS::reconnect()
    {
        if(this->m_mqtt_handle)
        {
            ZOO_INT32 error_code = MQTTClient_connect(this->m_mqtt_handle,&this->m_connect_option);
            if(MQTTCLIENT_SUCCESS != error_code)
            {
                ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,":: error_code:%d",error_code);
                //__THROW_RC_EXCEPTION(RC4A_SYSTEM_ERR,"connect fail",NULL);
            }
            this->notify_connection_state(ZOO_TRUE);
        }
    }

    /**
     * @brief the callback for mqtt connection lost 
     * @param context    the usr data
     * @param cause 
     */
    void on_connection_lost(void *context, char* cause)
    {
        static_cast<RC_HARDWARE_SERVICE_CLASS *>(context)->connection_lost_callback(cause); 
    }

    /**
     * @brief the callback for mqtt message arrivated  
     * @param context    the usr data
     * @param topicName  
     * @param topicLen  
     * @param message   
     */
    int on_message_arrived(void* context, char* topicName, int topicLen, MQTTClient_message* message)
    {
        std::string topic(topicName,topicLen);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION,__ZOO_FUNC__,"::topicName:%s,topicLen:%d",topic.data(),topicLen);
        if(message)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION,__ZOO_FUNC__,"::msgid[%d]",message->msgid);
            std::string msg((char *)message->payload,message->payloadlen);
            static_cast<RC_HARDWARE_SERVICE_CLASS *>(context)->message_arrived_callback(topic,msg);
            MQTTClient_free(topicName);
    	    MQTTClient_freeMessage(&message);
	    }
	    else
	    {
	        ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,"::message is null");
	    }
        return ZOO_TRUE;
    }
} //namespace RC
