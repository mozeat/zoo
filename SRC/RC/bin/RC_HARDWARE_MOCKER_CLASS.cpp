/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_HARDWARE_MOCKER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include "RC_HARDWARE_MOCKER_CLASS.h"
namespace RC
{
    /*
     * @brief Constructor
    **/ 
    RC_HARDWARE_MOCKER_CLASS::RC_HARDWARE_MOCKER_CLASS()
    {

    }

    /*
     * @brief Constructor
    **/ 
    RC_HARDWARE_MOCKER_CLASS::~RC_HARDWARE_MOCKER_CLASS()
    {

    }

    /*
     * @brief reload mock data.
    **/ 
    void RC_HARDWARE_MOCKER_CLASS::intialize()
    {
        this->m_mock_datas.clear();
        boost::shared_ptr<MOCK_DATA_PARSER_CLASS> data_parser = boost::make_shared<MOCK_DATA_PARSER_CLASS>();
        std::string mock_file = RC_CONFIGURE::get_instance()->get_mock_file();
        this->m_mock_datas = data_parser->parse_mock_data(mock_file);
    }

    /*
     * @brief terminate the hardware mock.
    **/ 
    void RC_HARDWARE_MOCKER_CLASS::terminate()
    {
        this->m_mock_datas.clear();
    }

    /*
     * @brief Get mock data.
     * @param section_name       the section name
     * @return mock data
     **/ 
    boost::shared_ptr<MOCK_DATA_CLASS> RC_HARDWARE_MOCKER_CLASS::get_mock_data(IN std::string section_name)
    {
        boost::shared_ptr<MOCK_DATA_CLASS> mock_data_model;
        std::map<std::string,boost::shared_ptr<MOCK_DATA_CLASS> >::iterator ite = this->m_mock_datas.begin();
        while(ite != this->m_mock_datas.end())
        {
            if(ite->first == section_name)
            {
                mock_data_model = ite->second;
                break;
            }
            ite++;
        }
        return mock_data_model;
    }
} //namespace RC
