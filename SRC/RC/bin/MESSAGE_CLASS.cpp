/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : MESSAGE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include "MESSAGE_CLASS.h"
namespace RC
{
    MESSAGE_CLASS::MESSAGE_CLASS():m_ptr(NULL),m_len(0)
    {
        
    }

    MESSAGE_CLASS::MESSAGE_CLASS(const std::string & topic,void * message,ZOO_INT32 message_lenght):m_topic(topic),m_ptr(NULL),m_len(0)
    {
        
    }
    
    MESSAGE_CLASS::~MESSAGE_CLASS()
    {
        
    }

    void MESSAGE_CLASS::set_message(std::string & topic,void * message,ZOO_INT32 message_lenght)
    {
        this->m_topic = topic;
        this->m_ptr = message;
        this->m_len =  message_lenght;
    }
    const std::string & MESSAGE_CLASS::get_topic()
    {
        return this->m_topic;
    }
    void * MESSAGE_CLASS::get_message()
    {
        return this->m_ptr;
    }
    ZOO_INT32 MESSAGE_CLASS::get_message_length()
    {
        return this->m_len;
    }
}
