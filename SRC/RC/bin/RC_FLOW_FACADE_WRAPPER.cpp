/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_FLOW_FACADE_WRAPPER.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include "RC_FLOW_FACADE_WRAPPER.h"
#include "PROCESSING_FLOW_FACADE_CLASS.h"


/*
 * @brief extern g_processing_flow defined from executor_wrapper.cpp
**/
extern RC::PROCESSING_FLOW_FACADE_CLASS * g_processing_flow;

/*
 * @brief RC4A_connect
**/
ZOO_INT32 RC_connect(IN RC4A_OPTION_STRUCT * option, INOUT ZOO_FD * fd)
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->connect(option,fd);
    }
    return RC4A_SYSTEM_ERR;
}

/*
 * @brief RC4A_disconnect
**/
ZOO_INT32 RC_disconnect(IN ZOO_FD fd)
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->disconnect(fd);
    }
    return RC4A_SYSTEM_ERR;
}

ZOO_INT32 RC_get_fd(ZOO_CHAR * client_id,ZOO_FD *fd)
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->get_fd(client_id,fd);
    }
    return RC4A_SYSTEM_ERR;
}


/*
 * @brief RC4A_get_connected
**/
ZOO_INT32 RC_get_connected(IN ZOO_FD fd,INOUT ZOO_BOOL * is_connected)
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->get_connected(fd,is_connected);
    }
    return RC4A_SYSTEM_ERR;
}

/*
 * @brief RC4A_set_subscribe_topic
**/
ZOO_INT32 RC_set_subscribe_topic(IN ZOO_FD fd,
												                      IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH])
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->set_subscribe_topic(fd,topic);
    }
    return RC4A_SYSTEM_ERR;
}

/*
 * @brief RC4A_remove_subscribe_topic
**/
ZOO_INT32 RC_remove_subscribe_topic(IN ZOO_FD fd,
														                    IN const ZOO_CHAR topic[RC4A_BUFFER_LENGTH])
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->remove_subscribe_topic(fd,topic);
    }
    return RC4A_SYSTEM_ERR;
}

/*
 * @brief RC4A_clear_db
**/
ZOO_INT32 RC_clear_db(IN ZOO_FD fd)
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->clear_db(fd);
    }
    return RC4A_SYSTEM_ERR;
}

/*
 * @brief RC4A_publish
**/
ZOO_INT32 RC_publish(IN ZOO_FD fd,
											    IN ZOO_CHAR topic[RC4A_BUFFER_LENGTH],
											    IN ZOO_CHAR message[RC4A_MESSAGE_LENGTH],
											    IN ZOO_INT32 message_length,
											    IN ZOO_INT32 message_index,
											    IN ZOO_INT32 total_messages)
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->publish(fd,topic,message,message_length,message_index,total_messages);
    }
    return RC4A_SYSTEM_ERR;
}

