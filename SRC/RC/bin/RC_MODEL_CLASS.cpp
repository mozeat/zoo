/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : RC
 * File Name      : RC_MODEL_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-08    Generator      created
*************************************************************/
#include "RC_MODEL_CLASS.h"
#include "RC_HARDWARE_SERVICE_CLASS.h"
namespace RC
{
    /*
     * @brief Constructor
    **/ 
    RC_MODEL_CLASS::RC_MODEL_CLASS()
    {
        this->m_hardware_service.reset(new RC_HARDWARE_SERVICE_CLASS());
    }

    /*
     * @brief Constructor
    **/ 
    RC_MODEL_CLASS::~RC_MODEL_CLASS()
    {
        
    }

    void RC_MODEL_CLASS::set_fd(ZOO_INT32 fd)
    {
        this->m_fd = fd;
    }

    ZOO_FD RC_MODEL_CLASS::get_fd()
    {
        return this->m_fd;
    }

    std::string RC_MODEL_CLASS::get_client_id()
    {
        return this->m_client_id;
    }
    
   /*
    * @brief Create an mqtt client instance
    * @param address
    * @param port
    * @param persistence_option 
    **/ 
    void RC_MODEL_CLASS::create(IN std::string address,IN ZOO_INT32 port,std::string client_id,
                                IN RC4A_OFFLINE_PERSISTENCE_OPTION_STRUCT * persistence_option)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: address:%s",address.data());
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: port:%d",port);
        std::string url = address + ":" + std::to_string(port);
        this->m_client_id = client_id;
        this->m_hardware_service->create(url,client_id);
        this->m_hardware_service->set_fd(this->m_fd);
    }
    
   /*
    * @brief Connect to broker
    * @param connect_option
    * @param ssl_option
    **/ 
    void RC_MODEL_CLASS::connect(IN RC4A_CONNECT_OPTION_STRUCT * connect_option,
                                    IN RC4A_SSL_OPTION_STRUCT * ssl_option)
    {
        this->m_hardware_service->connect(connect_option,ssl_option);
    }

   /*
    * @brief disconnect from broker.
    **/ 
    void RC_MODEL_CLASS::disconnect()
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");
        this->m_hardware_service->disconnect();
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit ...");
    }

    /*
    * @brief Is broker online
    **/ 
    ZOO_BOOL RC_MODEL_CLASS::is_online()
    {
        return this->m_hardware_service->is_connected();
    }
   
   /*
    * @brief Publish message to broker
    * @param topic
    * @param message
    * @param message_id
    * @param message_number
    **/ 
    void RC_MODEL_CLASS::publish(IN std::string & topic,
    									    IN std::string & message,
    									    IN ZOO_INT32 message_id,
    									    IN ZOO_INT32 message_number)
    {   
        if(message_number <= 1)
        {
            this->m_hardware_service->publish(topic,message);
            this->m_message_segments.clear();
        }
        else
        {
            this->m_message_segments[message_id] = message;        
            if(message_id + 1 == message_number)
            {
                std::string msg;
                std::for_each(this->m_message_segments.begin(),this->m_message_segments.end(),
                [&](auto & v)
                { 
                    msg.append(v.second);
                });                
                this->m_hardware_service->publish(topic,msg);
                this->m_message_segments.clear();
            }
        }
    }

    /*
     * @brief Notify property has been changed
     * @property_name     The property has been changed
    **/ 
    void RC_MODEL_CLASS::on_property_changed(MARKING_MODEL_INTERFACE* model,const ZOO_UINT32 property_name)
    {
        
    }

    /*
    * @brief set subscribe topic
    **/ 
    void RC_MODEL_CLASS::set_subscribe_topic(std::string & topic)
    {
        this->m_hardware_service->set_subscribe_topic(topic);
    }

   /*
    * @brief remove subscribe topic
    **/ 
    void RC_MODEL_CLASS::remove_subscribe_topic(std::string & topic)
    {
        this->m_hardware_service->remove_subscribe_topic(topic);
    }
}//namespace RC
