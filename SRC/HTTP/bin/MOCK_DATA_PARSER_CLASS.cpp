/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : MOCK_DATA_PARSER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-17    Generator      created
*************************************************************/
#include "MOCK_DATA_PARSER_CLASS.h"
#include <boost/make_shared.hpp>

namespace HTTP
{
    /*
     * @brief Constructor
     **/ 
    MOCK_DATA_PARSER_CLASS::MOCK_DATA_PARSER_CLASS()
    {
    }

    /*
     * @brief Destructor
     **/ 
    MOCK_DATA_PARSER_CLASS::~MOCK_DATA_PARSER_CLASS()
    {
    }

    /*
     * @brief Parse mock data from config file
     * @param file   the ini file location
     **/ 
    std::map<std::string,boost::shared_ptr<MOCK_DATA_CLASS> > MOCK_DATA_PARSER_CLASS::parse_mock_data(IN std::string file)
    {
        std::map<std::string,boost::shared_ptr<MOCK_DATA_CLASS> > sections;
        // TODO: parse field value 
        for(auto & section_name: this->m_section_names)
        {
           boost::shared_ptr<MOCK_DATA_CLASS> mock_data_model = boost::make_shared<MOCK_DATA_CLASS>();
           std::map<std::string,std::string> section = this->parse_section(file,section_name);
           std::for_each(section.begin(),section.end(),[mock_data_model](auto & v)
           {
               mock_data_model->add_data(v.first,v.second);
           });
           sections[section_name] = mock_data_model;
        }
        return sections;
    }

    std::string  MOCK_DATA_PARSER_CLASS::get_mock_file()
    {
        std::string exe_dir = ZOO_get_current_exec_path();
        std::string config_dir = ZOO_get_parent_dir(exe_dir.data(),3);
        std::string file = config_dir + "/config/" + "mock_data" +"/" + "HTTP_mock_data.ini";
        return file;
    }

    /*
    * @brief add section name
    * @param name   section_name
    **/
   void MOCK_DATA_PARSER_CLASS::add_section_name(std::string name)
   {
        this->m_section_names.push_back(name);
   }
}
