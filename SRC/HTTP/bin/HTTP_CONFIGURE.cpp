/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_CONFIGURE.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "HTTP_CONFIGURE.h"
#include "CERT_MANAGER_CLASS.h"
#include <boost/property_ptree/ptree.hpp>
#include <boost/property_ptree/json_parser.hpp>
namespace HTTP
{
    /*
     * @brief Number level forward back from execute base to relative path.
    **/ 
    static const ZOO_INT32 NUMBER_LEVEL = 3;

    /*
     * @brief The instance
    **/
    boost::shared_ptr<HTTP_CONFIGURE> HTTP_CONFIGURE::m_instance = NULL;

    /*
     * @brief Constructor
    **/ 
    HTTP_CONFIGURE::HTTP_CONFIGURE()
    {

    }

    /*
     * @brief Destructor
    **/ 
    HTTP_CONFIGURE::~HTTP_CONFIGURE()
    {

    }

    /*
     * @brief Get instance
    **/
    boost::shared_ptr<HTTP_CONFIGURE> HTTP_CONFIGURE::get_instance()
    {
        if(HTTP_CONFIGURE::m_instance == NULL)
        {
            HTTP_CONFIGURE::m_instance.reset(new HTTP_CONFIGURE());
        }
        return HTTP_CONFIGURE::m_instance;
    }

    /*
     * @brief Initialize
    **/
    void HTTP_CONFIGURE::initialize()
    {
        HTTP_CONFIGURE::m_instance->reload();
    }

    /*
     * @brief Reload configurations
    **/
    void HTTP_CONFIGURE::reload()
    {
        this->m_execute_path = ZOO_COMMON::ENVIRONMENT_UTILITY_CLASS::get_execute_path();
		this->parse_configuration(file);
    }

    std::string HTTP_CONFIGURE::get_listen_addr()
    {
        return this->m_listen_addr;
    }
    
    int HTTP_CONFIGURE::get_listen_port()
    {
        return this->m_listen_port;
    }
    
    int HTTP_CONFIGURE::get_server_threads()
    {
        return this->m_server_threads;
    }
    
    boost::shared_ptr<INTERNAL_PROCESS_TARGET_CLASS> HTTP_CONFIGURE::get_internal_target()
    {
        return this->m_internal_target;
    }
    
    boost::shared_ptr<EXTERNAL_PROCESS_TARGET_CLASS> HTTP_CONFIGURE::get_external_target()
    {
        return this->m_external_target;
    }

    std::string HTTP_CONFIGURE::get_root_doc()
    {
        return this->m_root_doc;
    }

    /*
     * @brief parse configurations
     * @param file      the config file path
    **/
    void HTTP_CONFIGURE::parse_configuration(std::string file)
    {
        
    }
} //namespace HTTP
