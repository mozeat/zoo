/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : INTERNAL_PROCESS_TARGET_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "INTERNAL_PROCESS_TARGET_CLASS.h"
namespace HTTP
{
    /*
     * @brief Constructor
    **/ 
    INTERNAL_PROCESS_TARGET_CLASS::INTERNAL_PROCESS_TARGET_CLASS()
    {
        
    }

    /*
     * @brief Destructor
    **/ 
    INTERNAL_PROCESS_TARGET_CLASS::~INTERNAL_PROCESS_TARGET_CLASS()
    {
        
    }

    std::vector<std::string> INTERNAL_PROCESS_TARGET_CLASS::get_targets()
    {
        return this->m_targets;
    }

    void INTERNAL_PROCESS_TARGET_CLASS::add_target(std::string target)
    {
        this->m_targets.push_back(target);
    }

    void INTERNAL_PROCESS_TARGET_CLASS::set_root_doc(std::string root_doc)
    {
        this->m_root_doc = root_doc;
    }

    std::string INTERNAL_PROCESS_TARGET_CLASS::get_root_doc()
    {
        return this->m_root_doc;
    }

    void INTERNAL_PROCESS_TARGET_CLASS::set_database_type(std::string type)
    {
        return this->m_database_type = type;
    }

    std::string INTERNAL_PROCESS_TARGET_CLASS::get_database_type()
    {
        return this->m_database_type;
    }

    bool INTERNAL_PROCESS_TARGET_CLASS::contain(std::string target)
    {
        bool result = false;
        std::vector<std::string>::iterator ite = this->m_targets.begin();
        while(ite != this->m_targets.end())
        {
            if(*ite == target)
            {
                result = true;
                break;
            }
            ++ite;
        }
        return result;
    }
} //namespace HTTP
