/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : DEVICE_CONTROLLER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "DEVICE_CONTROLLER_CLASS.h"
#include "HTTP_DOWNLOADER_CLASS.h"
#include "HTTP_CLIENT_MODEL_CLASS.h"
#include "HTTP_SSL_CLIENT_MODEL_CLASS.h"
#include <boost/filesystem.hpp>
namespace HTTP
{
    /*
     * @brief Constructor
    **/ 
    DEVICE_CONTROLLER_CLASS::DEVICE_CONTROLLER_CLASS():m_ioc(5),m_once_flag(BOOST_ONCE_INIT)
    {
    }

    /*
     * @brief Constructor
    **/ 
    DEVICE_CONTROLLER_CLASS::~DEVICE_CONTROLLER_CLASS()
    {

    }

    /*
     * @brief Initialize the model
     * @return throw exception 
    **/ 
    void DEVICE_CONTROLLER_CLASS::initialize()
    {
    }

    /*
     * @brief Terminate the model
     * @return throw exception 
    **/ 
    void DEVICE_CONTROLLER_CLASS::terminate()
    {
    }

    /*
     * @brief Get device status.
     * @return error code 
    **/ 
    void DEVICE_CONTROLLER_CLASS::get_status(INOUT HTTP4A_STATUS_STRUCT * status)
    {
        
        boost::ignore_unused(status);
    }

    /*
     * @brief Create all models
    **/ 
    void DEVICE_CONTROLLER_CLASS::create_all_models()
    {

    }

	/*
	 * @brief Create an http client instance
	 * @param fd
	 * @return error_code
	 **/
	ZOO_INT32 DEVICE_CONTROLLER_CLASS::create(INOUT ZOO_HANDLE * fd)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__," > entry ...");
    	static boost::mt19937 engine(boost::random_device{}());
        static boost::random::uniform_int_distribution<ZOO_HANDLE> distribution;
        *fd = static_cast<ZOO_HANDLE> (distribution(engine));
        this->m_sessions[*fd] = NULL;
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__," :: fd:%u",*fd);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__," < exit ...");
        return OK;
    }

	/*
	 * @brief HTTP4A_cleanup
	 * @param fd
	 * @return error_code
	**/
	ZOO_INT32 DEVICE_CONTROLLER_CLASS::cleanup(IN ZOO_HANDLE fd)
    {
        auto session = this->m_sessions.find(fd);
        if(session != this->m_sessions.end())
        {
        	this->m_sessions.erase(session);
        }
        return OK;
    }

	/*
	 * @brief Send a http request
	 * @param req
	 * @return error_code
	**/
	ZOO_INT32 DEVICE_CONTROLLER_CLASS::request(IN HTTP4A_REQUEST_STRUCT * req)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__," >");
        
        if(req == NULL)
        {
        	__THROW_HTTP_EXCEPTION(HTTP4A_SYSTEM_ERR,"req pointer is null",NULL);
        }

        if(this->m_sessions.find(req->fd) == this->m_sessions.end())
    	{
    		__THROW_HTTP_EXCEPTION(HTTP4A_SYSTEM_ERR,"session has been removed",NULL);
    	}
        
    	auto session = this->get_model(req->fd);
    	if(session == NULL)
    	{
    		session.reset(new HTTP_CLIENT_MODEL_CLASS());
    		session->set_event_publisher(this->m_event_publisher);
    		session->set_fd(req->fd);
    		this->m_sessions[req->fd] = session;
    	}
        session->run(req);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__," <");
        return OK;
    }


    /*
	 * @brief Send a http request
	 * @param req
	 * @return error_code
	**/
	ZOO_INT32 DEVICE_CONTROLLER_CLASS::download(IN ZOO_HANDLE fd,
														IN HTTP4A_DOWNLOAD_OPTION_STRUCT * dl_option)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," >");
        
        if(this->m_sessions.find(fd) == this->m_sessions.end())
    	{
    		__THROW_HTTP_EXCEPTION(HTTP4A_SYSTEM_ERR,"session has been removed",NULL);
    	}

        if(dl_option == NULL)
        {
        	__THROW_HTTP_EXCEPTION(HTTP4A_SYSTEM_ERR,"req pointer is null",NULL);
        }
    	
    	auto session = this->get_model(fd);
    	if(session == NULL)
    	{
    		session.reset(new HTTP_DOWNLOADER_CLASS());
    		session->set_event_publisher(this->m_event_publisher);
    		session->set_fd(fd);
    		this->m_sessions[fd] = session;
    	}
    	boost::dynamic_pointer_cast<HTTP_DOWNLOADER_CLASS>(session)->run(dl_option);
    	
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," <");
        return OK;
    }

	/*
	 * @brief Send a http request
	 * @param req
	 * @param ssl_option
	 * @return error_code
	**/
	ZOO_INT32 DEVICE_CONTROLLER_CLASS::request(IN HTTP4A_REQUEST_STRUCT* req,
													IN HTTP4A_SSL_OPTION_STRUCT* ssl_option)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," >");
        
        if(ssl_option == NULL)
        {
        	__THROW_HTTP_EXCEPTION(HTTP4A_SYSTEM_ERR,"Session has been removed",NULL);
        }
        if(req == NULL)
        {
        	__THROW_HTTP_EXCEPTION(HTTP4A_SYSTEM_ERR,"req pointer is null",NULL);
        }
        if(this->m_sessions.find(req->fd) == this->m_sessions.end())
    	{
    		__THROW_HTTP_EXCEPTION(HTTP4A_SYSTEM_ERR,"session has been removed",NULL);
    	}
    	auto session = this->get_model(req->fd);
    	if(session == NULL)
    	{
    	    std::string cert_dir = ssl_option->cert_path;
    	    std::string pub_key = cert_dir + "/" + ssl_option->public_key;
    	    if(!boost::filesystem::exists(pub_key))
    	    {   
    	        __THROW_HTTP_EXCEPTION(HTTP4A_PARAMETER_ERR,"public key file not exist",NULL);
    	    }
    	    
    	    std::string pri_key = cert_dir + "/" + ssl_option->private_key;
    	    if(!boost::filesystem::exists(pri_key))
    	    {   
    	        __THROW_HTTP_EXCEPTION(HTTP4A_PARAMETER_ERR,"private key file not exist",NULL);
    	    }
    	    
    	    std::string trust_train = cert_dir + "/" + ssl_option->trust_chain;
    	    if(!boost::filesystem::exists(trust_train))
    	    {   
    	        __THROW_HTTP_EXCEPTION(HTTP4A_PARAMETER_ERR,"trust train file not exist",NULL);
    	    }
   	        
    		boost::asio::ssl::context ctx{boost::asio::ssl::context::tlsv12_client};
    		auto pem = boost::asio::ssl::context_base::file_format::pem;
    		ctx.set_verify_mode(boost::asio::ssl::verify_fail_if_no_peer_cert);
    		boost::system::error_code ec;
    		ctx.use_certificate_file(pub_key,pem,ec);
    		if(ec)
    		{
    		    __THROW_HTTP_EXCEPTION(HTTP4A_PARAMETER_ERR,"pub_key file is invalid",NULL);
    		    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"::%s",ec.message().data());
    		}
    		ctx.use_private_key_file(pri_key,pem,ec);
    		ctx.load_verify_file(trust_train,ec);
    		session.reset(new HTTP_SSL_CLIENT_MODEL_CLASS(ctx));
    		session->set_event_publisher(this->m_event_publisher);
    		session->set_fd(req->fd);
    		this->m_sessions[req->fd] = session;
    	}
        boost::dynamic_pointer_cast<HTTP_SSL_CLIENT_MODEL_CLASS>(session)->run(req);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," <");
        return OK;
    }

    /*
     * @brief This method is executed when property changed value
     * @param model             The model type
     * @param property_name     The property has been changed value
    **/
    void DEVICE_CONTROLLER_CLASS::on_property_changed(MARKING_MODEL_INTERFACE* model,
															    const ZOO_UINT32 property_name)
    {

    }

	/*
	 * @brief Get an instance of device by fd
	**/ 
	boost::shared_ptr<HTTP_CLIENT_INTERFACE> DEVICE_CONTROLLER_CLASS::get_model(IN ZOO_HANDLE fd)
    {
        boost::shared_ptr<HTTP_CLIENT_INTERFACE> session;
		if(this->m_sessions.find(fd) != this->m_sessions.end())
		{
			session = this->m_sessions[fd];
		}
		return session;
    }
} //namespace HTTP

