/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : SESSION_ABSTRACT_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "SESSION_ABSTRACT_CLASS.h"

namespace HTTP
{
	/*
	 * @brief Constructor
	**/
	SESSION_ABSTRACT_CLASS::SESSION_ABSTRACT_CLASS()
	{
		this->m_fd = 0;
	}

	/*
	 * @brief Destructor
	**/ 
	SESSION_ABSTRACT_CLASS::~SESSION_ABSTRACT_CLASS()
	{
		
	}
	
	/*
	 * @brief Run the eventloop.
	**/ 
	void SESSION_ABSTRACT_CLASS::run()
	{
		//reimplement in Derived class
	}
	
	/*
	 * @brief Read request informations.
	**/ 
	void SESSION_ABSTRACT_CLASS::do_write(std::string && body)
	{
		//reimplement in Derived class
	}
	
	/*
	 * @brief Set file descriptor of the socket.
	**/ 
	void SESSION_ABSTRACT_CLASS::set_fd(ZOO_HANDLE fd)
	{
		this->m_fd = fd;
	}
	
	/*
	 * @brief Get file descriptor of the socket.
	**/ 
	ZOO_HANDLE SESSION_ABSTRACT_CLASS::get_fd()
	{
		//reimplement in Derived class
		return this->m_fd;
	}

	/*
	 * @brief Get file descriptor of the socket.
	**/ 
	boost::shared_ptr<HTTP4A_REQUEST_STRUCT> SESSION_ABSTRACT_CLASS::get_message()
	{
		return this->m_message;
	}
	
	/*
	 * @brief Add observer will be notified when property changed
	 * @param observer	Property changed observer
	**/
	void SESSION_ABSTRACT_CLASS::add_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer)
	{
		//this->remove_observer(observer);
		this->m_observers.emplace_back(observer);
	}

	/*
	 * @brief Remove observer from subscribe list
	**/
	void SESSION_ABSTRACT_CLASS::remove_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer)
	{
		// TODO implement clean up later
	}

	/*
	 * @brief Remove observer from subscribe listd
	**/
	void SESSION_ABSTRACT_CLASS::clean()
	{
		this->m_observers.clear();
	}

	/*
	 * @brief Notify property has been changed
	 * @param property_name 	The property has been changed
	**/
	void SESSION_ABSTRACT_CLASS::notify_of_property_changed(const ZOO_UINT32 property_name)
	{
		std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>*>::iterator observer_itr =
            this->m_observers.begin();
        while (observer_itr != this->m_observers.end())
        {
            PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer = *observer_itr;
            observer->on_property_changed(this, property_name);
            observer_itr++;
        }
	}

	/*
	 * @brief Rebuild request message to HTTP4A_REQUEST_STRUCT.
	 * @param req   the request  string body
	 * @param message 
	**/ 
	void SESSION_ABSTRACT_CLASS::build_message(IN boost::beast::http::request<boost::beast::http::string_body>      req)
	{
	    this->m_message.reset(new HTTP4A_REQUEST_STRUCT());
	    this->m_message->method = static_cast<HTTP4A_METHOD_ENUM>(req.method());
		this->m_message->keep_alive = req.keep_alive();
		this->m_message->fd = this->m_fd;
		
	    if(req.method() == boost::beast::http::verb::post)
	    {
	    	if(req.body().size() < HTTP4A_BODY_LENGTH)
				memcpy(this->m_message->body,req.body().data(),req.body().size() + 1);
			else
				memcpy(this->m_message->body,req.body().data(),HTTP4A_BODY_LENGTH);
		}
		this->m_message->body[HTTP4A_BODY_LENGTH - 1] = '\0';

		if(req.target().to_string().size() > 0)
		{
			if(req.target().size() < HTTP4A_FEILD_DATA_LENGTH)
				sprintf(this->m_message->target,"%s",req.target().to_string().c_str());
			else
				memcpy(this->m_message->target,req.target().data(),HTTP4A_FEILD_DATA_LENGTH);
		}
		this->m_message->target[HTTP4A_FEILD_DATA_LENGTH - 1] = '\0';
	}
	
	/**
	 * @brief
	 */
	ZOO_HANDLE SESSION_ABSTRACT_CLASS::make_unique_fd()
	{
		static boost::mt19937 engine(boost::random_device{}());
        static boost::random::uniform_int_distribution<ZOO_HANDLE> distribution;
        return static_cast<ZOO_HANDLE> (distribution(engine));
	}
} //namespace HTTP

