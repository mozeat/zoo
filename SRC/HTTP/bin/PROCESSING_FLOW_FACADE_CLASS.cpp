/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : PROCESSING_FLOW_FACADE_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "PROCESSING_FLOW_FACADE_CLASS.h"
#include <iostream>
namespace HTTP
{
    /*
     * @brief Constructor
    **/ 
    PROCESSING_FLOW_FACADE_CLASS::PROCESSING_FLOW_FACADE_CLASS()
    {

    }

    /*
     * @brief Constructor
    **/ 
    PROCESSING_FLOW_FACADE_CLASS::~PROCESSING_FLOW_FACADE_CLASS()
    {

    }

    /*
     * @brief Create an http client instance
     * @param fd
     * @return error_code
     **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::create(INOUT ZOO_HANDLE * fd)
    {
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION_NAME__,">() ");
        ZOO_INT32 error_code = OK;
        __HTTP_TRY
        {
        	this->m_device_controller->create(fd);
        	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION_NAME__,":: fd = %u",*fd);
        }
        __HTTP_CATCH_ALL(error_code)
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION_NAME__,"<() ");
        return error_code;
    }

    /*
     * @brief HTTP4A_cleanup
     * @param fd
     * @return error_code
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::cleanup(IN ZOO_HANDLE fd)
    {
        ZOO_INT32 error_code = OK;
        __HTTP_TRY
        {
        	this->m_device_controller->cleanup(fd);
        }
        __HTTP_CATCH_ALL(error_code)

        return error_code;
    }

    /*
     * @brief Send a http request
     * @param req
     * @return error_code
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::on_request_sig(IN HTTP4A_REQUEST_STRUCT* req)
    {
        ZOO_INT32 error_code = OK;
       	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION_NAME__,"> target:%s",req->target);
       	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION_NAME__,"> content_type:%s",req->content_type);
        __HTTP_TRY
        {
        	this->m_device_controller->request(req);
        }
        __HTTP_CATCH_ALL(error_code)

        return error_code;
    }

    /*
     * @brief HTTP4A_on_download_file_sig
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::on_download_file_sig(IN ZOO_HANDLE fd,
															IN HTTP4A_DOWNLOAD_OPTION_STRUCT * dl_option)
    {
		ZOO_INT32 error_code = OK;
       	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION_NAME__,"> fd:%d",fd);
       	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION_NAME__,"> target:%s",dl_option->target);
        __HTTP_TRY
        {
        	this->m_device_controller->download(fd,dl_option);
        }
        __HTTP_CATCH_ALL(error_code)
        return error_code;
    }

    /*
     * @brief Send a http request
     * @param req
     * @param ssl_option
     * @return error_code
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::on_ssl_request_sig(IN HTTP4A_REQUEST_STRUCT* req,
																	IN HTTP4A_SSL_OPTION_STRUCT* ssl_option)
    {
        ZOO_INT32 error_code = OK;
		__HTTP_TRY
		{
			this->m_device_controller->request(req,ssl_option);
		}
		__HTTP_CATCH_ALL(error_code)

        return error_code;
    }

    /*
     * @brief HTTP4A_reply_server
     * @param id
     * @param message
     * @return error_code
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::handle_request(IN ZOO_INT32 id,
												 				IN HTTP4A_RESPONSE_STRUCT * message)
    {
        ZOO_INT32 error_code = OK;
		__HTTP_TRY
		{
			if(message == NULL)
			{
				__THROW_HTTP_EXCEPTION(HTTP4A_PARAMETER_ERR,"message pointer is null",NULL);
			}
			std::string body(message->body);
			this->m_http_server->response(id,std::move(body));
		}
		__HTTP_CATCH_ALL(error_code)

        return error_code;
    }
    
	/*
	 * @brief Server start to listen incoming request
	 * @param id
	 * @param message
	 * @return error_code
	**/
	ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::run()
    {
        ZOO_INT32 error_code = OK;
		__HTTP_TRY
		{
			this->m_http_server->run();
		}
		__HTTP_CATCH_ALL(error_code)

        return error_code;
    }

    /*
	 * @brief Server stop to listen incoming request
	 * @return error_code
	**/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::stop()
    {
        ZOO_INT32 error_code = OK;
		__HTTP_TRY
		{
			this->m_http_server->stop();
		}
		__HTTP_CATCH_ALL(error_code)

        return error_code;
    }

    /*
     * @brief This method is executed when property changed value
     * @param model             The model type
     * @param property_name     The property has been changed value
    **/ 
    void PROCESSING_FLOW_FACADE_CLASS::on_property_changed(CONTROLLER_INTERFACE * model,
																    const ZOO_UINT32 property_name)
    {
    	switch(property_name)
    	{
    		/*case REMOTE_CLINET_REQ:
    		{
    			
    			break;
    		}
    		case REMOTE_SERVER_REP:
    		{
    			HTTP4A_RESPONSE_STRUCT res = boost::any_cast<HTTP4A_RESPONSE_STRUCT>(*property_value.get());
    			break;
    		}
    		case SERVER_SHUTDOWN:
    		{
    			
    			break;
    		}
    		case SERVER_STARTUP:
    		{
    			
    			break;
    		}
    		case SERVER_HANDSHAKE_FAIL:
    		{
    			
    			break;
    		}
    		case CLIENT_HANDSHAKE_FAIL:
    		{
    			
    			break;
    		}*/
    		default:break;
    	}
    }
} //namespace HTTP

