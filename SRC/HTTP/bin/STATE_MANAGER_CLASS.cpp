/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : STATE_MANAGER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "STATE_MANAGER_CLASS.h"
namespace HTTP
{
    /*
     * @brief Constructor
    **/ 
    STATE_MANAGER_CLASS::STATE_MANAGER_CLASS()
    :m_driver_state(ZOO_DRIVER_STATE_IDLE)
    ,m_running_mode(ZOO_RUNNING_MODE_WORK)
    {

    }

    /*
     * @brief Constructor
    **/ 
    STATE_MANAGER_CLASS::~STATE_MANAGER_CLASS()
    {

    }

    /*
     * @brief Set driver state
    **/ 
    void STATE_MANAGER_CLASS::set_driver_state(IN ZOO_DRIVER_STATE_ENUM state)
    {
        this->m_driver_state = state;
    }

    /*
     * @brief Get driver state
    **/ 
    ZOO_DRIVER_STATE_ENUM STATE_MANAGER_CLASS::get_driver_state()
    {
        return this->m_driver_state;
    }

    /*
     * @brief Update driver state
    **/ 
    void STATE_MANAGER_CLASS::update_driver_state(IN ZOO_DRIVER_STATE_ENUM state)
    {
        if(this->m_driver_state != state)
        {
            this->m_driver_state = state;
        }
    }

    /*
     * @brief Set running mode
    **/ 
    void STATE_MANAGER_CLASS::set_running_mode(IN ZOO_RUNNING_MODE_ENUM running_mode)
    {
        this->m_running_mode = running_mode;
    }

    /*
     * @brief Get running mode
    **/ 
    ZOO_RUNNING_MODE_ENUM STATE_MANAGER_CLASS::get_running_mode()
    {

        return this->m_running_mode;
    }

} //namespace HTTP

