/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_MODEL_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "HTTP_SERVER_MODEL_CLASS.h"
#include "LISTENER_CLASS.h"
namespace HTTP
{
    /*
     * @brief Constructor
    **/ 
    HTTP_SERVER_MODEL_CLASS::HTTP_SERVER_MODEL_CLASS()
    :m_ioc(HTTP_THREADS_MAX)
    {
    	memset(&this->m_status,0x0,sizeof(HTTP4A_STATUS_STRUCT));
    }

    /*
     * @brief Destructor
    **/ 
    HTTP_SERVER_MODEL_CLASS::~HTTP_SERVER_MODEL_CLASS()
    {

    }

	/*
	 * @brief Load configurations for cfg.sql.
	**/
	void HTTP_SERVER_MODEL_CLASS::initialize()
    {
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> ...");
        HTTP_CONFIGURE::get_instance()->reload();

        auto addr = HTTP_CONFIGURE::get_instance()->get_listen_addr();
		auto port = static_cast<unsigned short>(HTTP_CONFIGURE::get_instance()->get_listen_port());
		auto root_doc = HTTP_CONFIGURE::get_instance()->get_root_doc();
		
		ssl::context ctx{ssl::context::tlsv12};
		this->load_server_certificate(ctx);
        this->m_http_listener.reset(new LISTENER_CLASS(this->m_ioc,ctx,tcp::endpoint{addr,port},root_doc));
        
		net::signal_set signals(this->m_ioc, SIGINT, SIGTERM);
        signals.async_wait(
        [&](beast::error_code const&, int)
        {
            // Stop the `io_context`. This will cause `run()`
            // to return immediately, eventually destroying the
            // `io_context` and all of the sockets in it.
            this->m_ioc.stop();
        });
        
		
		    
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< ...");
    }

	/*
	 * @brief Run the eventloop.
	**/ 
	void HTTP_SERVER_MODEL_CLASS::run()
    {
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> ...");
				
    	for(auto i = HTTP_THREADS_MAX - 1; i > 0; --i)
	    {
	        this->m_thread_pool.create_thread([&]
	        {
	            this->m_ioc.run();
	        });
	    }
	    
	    this->update_status(ZOO_TRUE,OK);
    	this->m_ioc.run();
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< ...");
    }

    /*
	 * @brief Stop the eventloop.
	**/ 
	void HTTP_SERVER_MODEL_CLASS::stop()
	{
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> ...");
		this->m_ioc.stop();
		this->m_thread_pool.join_all();
		this->update_status(ZOO_FALSE,OK);
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< ...");
	}
	
	/*
	 * @brief Load ssl context.
	**/
	void HTTP_SERVER_MODEL_CLASS::load_server_certificate(boost::asio::ssl::context & ctx)
	{
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> ...");
		boost::shared_ptr<CERT_MANAGER_CLASS> cert_manager = CERT_MANAGER_CLASS::get_instance();
		std::string cert_path = cert_manager->get_verify_path() + "/";
		std::string public_key     = cert_path + cert_manager->get_public_key_file(CERT_SEVER);
		std::string private_key    = cert_path + cert_manager->get_private_key_file(CERT_SEVER);
		std::string trust_chain    = cert_path + cert_manager->get_trust_chain_file(CERT_SEVER);
		int verify_mode  = cert_manager->get_verify_mode();
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
										":: cert path = %s",cert_path.c_str());
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
										":: cert= %s",public_key.c_str());
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
										":: private_key = %s",private_key.c_str());
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
										":: trust_chain = %s",trust_chain.c_str());
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
										":: verify mode = %d",verify_mode);
		try
		{
			auto pem = boost::asio::ssl::context::pem;			
			ctx.set_verify_mode(verify_mode);
			ctx.set_verify_callback(
						std::bind(&HTTP_SERVER_MODEL_CLASS::ssl_verify_callback,
							this,std::placeholders::_1,std::placeholders::_2));
			ctx.set_options(boost::asio::ssl::context::default_workarounds);
			ctx.use_certificate_file(public_key,pem);
			ctx.use_private_key_file(private_key,pem);
			ctx.load_verify_file(trust_chain);;
		}
		catch(std::exception & e)
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__," :: %s",e.what());
		}
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< ...");
	}

		
	/*
     * @brief This method is executed when property changed value
     * @param model             The model type
     * @param property_name     The property has been changed value
    **/
    OVERRIDE
    void HTTP_SERVER_MODEL_CLASS::on_property_changed(MARKING_MODEL_INTERFACE * model,
								        						const ZOO_UINT32 property_name)
    {
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry...");
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: property_name:%d",property_name);
    	
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: current peers: %d",this->m_status.current_peers);
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit...");
    }
    
	
	/*
	 * @brief Get acceptor.
	**/
	void HTTP_SERVER_MODEL_CLASS::update_status(ZOO_BOOL active,ZOO_INT32 error_code)
	{
		this->m_status.actived = active;
		this->m_status.max_connections = boost::asio::socket_base::max_listen_connections;
		this->m_status.ssl_enabled = ZOO_TRUE;
		this->m_event_publisher->publish_HTTP4A_status(&this->m_status,error_code);
	}

	/**
	 * This function is used to specify a callback function that will be called
	 * by the implementation when it needs to verify a peer certificate.
	 *
	 * @param callback The function object to be used for verifying a certificate.
	 * The function signature of the handler must be:
		 * @code bool verify_callback(
		 *   bool preverified, // True if the certificate passed pre-verification.
	 *   verify_context& ctx // The peer certificate and other context.
	 * ); @endcode
	 * The return value of the callback is true if the certificate has passed
	 * verification, false otherwise.
	 *
	 * @throws boost::system::system_error Thrown on failure.
	 *
	 * @note Calls @c SSL_CTX_set_verify.
	 */
	bool HTTP_SERVER_MODEL_CLASS::ssl_verify_callback(bool preverified,
																boost::asio::ssl::verify_context & ctx)
	{
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> fucntion entry...");
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: preverified: %d",preverified);
		if(!preverified)
		{
			char  cn[256] = "";
			X509_STORE_CTX * x509_ctx  = ctx.native_handle();
			X509* cert = X509_STORE_CTX_get_current_cert(x509_ctx);
			X509_NAME_get_text_by_NID(X509_get_subject_name(cert),NID_commonName,cn,sizeof(cn));
			ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: issuer CN: %s",cn);
		}
		ZOO_slog( ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< fucntion exit...");
		return true;
	}
} //namespace HTTP
