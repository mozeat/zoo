/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : EXTERNAL_PROCESS_TARGET_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "EXTERNAL_PROCESS_TARGET_CLASS.h"

namespace HTTP
{
 
    /*
    * @brief Constructor
    **/ 
    EXTERNAL_PROCESS_TARGET_CLASS::EXTERNAL_PROCESS_TARGET_CLASS()
    {

    }

    /*
    * @brief Destructor
    **/ 
    EXTERNAL_PROCESS_TARGET_CLASS::~EXTERNAL_PROCESS_TARGET_CLASS()
    {

    }

    std::vector<std::string> EXTERNAL_PROCESS_TARGET_CLASS::get_targets()
    {
        return this->m_targets;
    }

    void EXTERNAL_PROCESS_TARGET_CLASS::add_target(std::string target)
    {
        this->m_targets.push_back(target);
    }

    void EXTERNAL_PROCESS_TARGET_CLASS::set_process_timeout_value(int seconds)
    {
        this->m_process_timeout_value = seconds;
    }

    int EXTERNAL_PROCESS_TARGET_CLASS::get_process_timeout_value()
    {
        return this->m_process_timeout_value;
    }

    std::string EXTERNAL_PROCESS_TARGET_CLASS::get_root_path()
    {
        return this->m_root_path;
    }
    void EXTERNAL_PROCESS_TARGET_CLASS::set_root_path(std::string path)
    {
        return this->m_root_path;
    }

    std::string EXTERNAL_PROCESS_TARGET_CLASS::get_sql_type()
    {
        return this->m_sql_type;
    }
    void EXTERNAL_PROCESS_TARGET_CLASS::set_sql_type(std::string type)
    {
        this->m_sql_type = type;
    }

    bool EXTERNAL_PROCESS_TARGET_CLASS::contain(std::string target)
    {
        bool result = false;
        std::vector<std::string>::iterator ite = this->m_targets.begin();
        while(ite != this->m_targets.end())
        {
            if(*ite == target)
            {
                result = true;
                break;
            }
            ++ite;
        }
        return result;
    }
} //namespace HTTP
