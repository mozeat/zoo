/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_DOWNLOADER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "HTTP_DOWNLOADER_CLASS.h"
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
namespace HTTP
{
	/*
	 * @brief Constructor
	**/ 
	HTTP_DOWNLOADER_CLASS::HTTP_DOWNLOADER_CLASS():
        m_resolver(boost::asio::make_strand(m_ioc))
        ,m_stream(boost::asio::make_strand(m_ioc))
	{
		
	}

	/*
	 * @brief Destructor
	**/ 
	HTTP_DOWNLOADER_CLASS::~HTTP_DOWNLOADER_CLASS()
	{
		
	}

	/*
	 * @brief run.
	**/ 
	void HTTP_DOWNLOADER_CLASS::run(IN void * req_option)
	{
		HTTP4A_DOWNLOAD_OPTION_STRUCT * dl_option = (HTTP4A_DOWNLOAD_OPTION_STRUCT *)req_option;
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > ");
		if(dl_option == NULL)
		{
			this->fail(HTTP4A_PARAMETER_ERR,"dl_option pointer is null.");
			return;
		}
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
									":: set target:%s,content_type:%s,auth:%s,accept:%s",
									dl_option->target,dl_option->content_type,
									dl_option->auth,dl_option->accept);								
		network::uri url{dl_option->target};
		std::string target = url.path().to_string() ;
		if(url.has_query())
		    target.append(url.query().to_string());
		if(url.has_fragment())    
		    target.append(url.fragment().to_string());
		    
		this->m_req.method(boost::beast::http::verb::get);
        this->m_req.target(url.path().empty() ? "/" : target);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"::%s",target.data());
		this->m_req.set(boost::beast::http::field::host,url.host().to_string());
		if(strlen(dl_option->content_type) > 0 )
            this->m_req.set(boost::beast::http::field::content_type, dl_option->content_type);
        if(strlen(dl_option->auth) > 0 )    
            this->m_req.set(boost::beast::http::field::authorization, dl_option->auth);
        if(strlen(dl_option->accept) > 0 )     
            this->m_req.set(boost::beast::http::field::accept, dl_option->accept);
        this->m_req.set(boost::beast::http::field::user_agent, "ZOO");
        this->m_req.keep_alive(true);
        this->m_req_timeout = dl_option->timeout;
		std::string port = url.port().to_string();
		if(port.empty())
		{
		    port = (url.scheme().to_string() == "https") ? "443" : "80";
		    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"::port:%s",port.data());
		}
		
		this->m_download_file = std::string(dl_option->storage_dir) + "/" + dl_option->file_name;
        this->m_resolver.async_resolve(url.host().to_string(),port,
            boost::beast::bind_front_handler(&HTTP_DOWNLOADER_CLASS::on_resolve,this));
        ZOO_COMMON::THREAD_POOL::get_instance()->queue_working
        (
	        [&]
	        {
	        	this->m_ioc.run();
	        }
        );
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < ");    
	}

	/*
	 * @brief resolve host info.
	 * @param ec      error_code
	 * @param results   
	**/
	void HTTP_DOWNLOADER_CLASS::on_resolve(boost::beast::error_code ec,
												boost::asio::ip::tcp::resolver::results_type results)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > ");
		if(ec)
            return fail(HTTP4A_CLIENT_RESOLVE_ERR, ec.message());

        // Make the connection on the IP address we get from a lookup
        this->m_stream.async_connect(results,boost::beast::bind_front_handler(
                						&HTTP_DOWNLOADER_CLASS::on_connect,this));
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < ");
	}

	/*
	 * @brief connect host.
	 * @param ec      error_code
	 * @param results   
	**/
	void HTTP_DOWNLOADER_CLASS::on_connect(boost::beast::error_code ec,boost::asio::ip::tcp::resolver::results_type::endpoint_type)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > ");
		if(ec)
            return fail(HTTP4A_CONNECT_ERR, ec.message());
        // Set a timeout on the operation
        //boost::beast::get_lowest_layer(this->m_stream).expires_after(std::chrono::seconds(this->m_req_timeout));
		this->m_stream.expires_after(std::chrono::seconds(this->m_req_timeout));
    	// Send the HTTP request to the remote host
    	boost::beast::http::async_write(this->m_stream, this->m_req,
        	boost::beast::bind_front_handler(
            	&HTTP_DOWNLOADER_CLASS::on_write,this));
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < ");
	}

	/*
	 * @brief perform http request
	 * @param ec      error_code
	 * @param bytes_transferred   
	**/	
	void HTTP_DOWNLOADER_CLASS::on_write(boost::beast::error_code ec,std::size_t bytes_transferred)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > ");
		boost::ignore_unused(bytes_transferred);

        if(ec)
            return fail(HTTP4A_CLIENT_SEND_MESSAGE_ERR, ec.message());

        // Receive the HTTP response
        boost::beast::http::async_read(this->m_stream, this->m_buffer, this->m_res,
            boost::beast::bind_front_handler(
                &HTTP_DOWNLOADER_CLASS::on_read,this));
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < ");
	}
	
	/*
	 * @brief read reply from host.
	 * @param ec      error_code
	 * @param bytes_transferred   
	**/
	void HTTP_DOWNLOADER_CLASS::on_read(boost::beast::error_code ec,
											std::size_t bytes_transferred)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > ");
		boost::ignore_unused(bytes_transferred);
        if(ec)
        {
            return fail(HTTP4A_CLIENT_RECV_MESSAGE_ERR, ec.message());
		}
		auto result = this->m_res.result();
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
        									":: error_code: %d,reason:%s",result,
        									this->m_res.reason().to_string().c_str());									
        auto content_type = this->m_res.at(boost::beast::http::field::content_type).to_string();
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
        									":: content_type: %s",content_type.c_str());        									
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
        									":: file size: %lu",this->m_res.body().size());
		auto message = this->build_response_message(this->m_fd,OK,content_type,this->m_download_file);
		//auto location = this->m_res.at(boost::beast::http::field::location).to_string();
		auto save_to_file = [&](std::string file_name)
		{
		    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: save to file : %s",file_name.data());

            if(boost::filesystem::exists(file_name))
            {
                boost::system::error_code ec;
                boost::filesystem::remove(file_name,ec);
            }
            
			std::ofstream file(file_name,std::ios::in | std::ios::app);
           	if(file.is_open())
           	{
           		file << this->m_res.body();
           		file.close();
           	}
		};

		auto publish_inprogress_event = [&](std::string file_name,int speed,int total_time,int error_code)
		{
		    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
		                    ":: publish_inprogress_event: file_name:%s,speed:%d,total_time:%d,",
		                    file_name.data(),speed,total_time);
			auto message = this->build_message(this->m_fd,file_name,speed,total_time);      										
           	this->m_event_publisher->publish_HTTP4A_inprogress(message.get(),error_code);
		};

		auto need_relocate_url = [&](int status,std::string & location)->bool
		{
			return ((status == 301 || status == 302) && !location.empty());
		};

		//bool need_relocate = need_relocate_url(static_cast<int>(result),location);
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
		//                    ":: need_relocate: %s",need_relocate ? "true" : "false");
		/*if(need_relocate)
		{
    		network::uri url(location);
    		auto host = url.host().to_string();
    		auto scheme = url.scheme().to_string() == "https" ? "443" : "80";
    		this->m_resolver.async_resolve(host,scheme,
           		 				boost::beast::bind_front_handler
           		 							(&HTTP_DOWNLOADER_CLASS::on_resolve,this));
    	}*/
    	
    	if(result == boost::beast::http::status::ok)
    	{
    		save_to_file(this->m_download_file);
    		publish_inprogress_event(this->m_download_file,0,0,OK);
			this->m_stream.socket().shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);     										
        }

        if(ec && ec != boost::beast::errc::not_connected)
        {
            return fail(HTTP4A_DISCONNECT_ERR, "shutdown");
        }
        
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < ");
	}
	
	/*
	 * @brief Shutdown connection.
	 * @param ec      error_code
	**/
	void HTTP_DOWNLOADER_CLASS::on_shutdown(boost::beast::error_code ec)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > ");
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
											" :: error_code: %d (eof=2)",ec.value());
		if(ec == boost::asio::error::eof)
        {
            ec = {};
        }
        
        if(ec)
        {
            return fail(HTTP4A_CONNECT_ERR, "shutdown");
        }
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < ");
	}
	
	/*
	 * @brief print error informations.
	 * @param ec	  error_code
	 * @param why	
	**/
	void HTTP_DOWNLOADER_CLASS::fail(ZOO_INT32 error_code,std::string why)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > ");
		if(!why.empty())
			ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,"error_code:%d,why:%s",error_code,why.c_str());
		auto message = this->build_message(this->m_fd,this->m_download_file,0,0);		
		this->m_event_publisher->publish_HTTP4A_inprogress(message.get(),HTTP4A_SYSTEM_ERR);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < ");
	}
} //namespace HTTP
