/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_CLIENT_ABSTRACT_MODEL_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "HTTP_CLIENT_ABSTRACT_MODEL_CLASS.h"
#include <iostream>
namespace HTTP
{
	/*
	 * @brief Constructor
	**/ 
	HTTP_CLIENT_ABSTRACT_MODEL_CLASS::HTTP_CLIENT_ABSTRACT_MODEL_CLASS()
        :m_once_flag(BOOST_ONCE_INIT),m_fd(0)
        ,m_req_timeout(30)
	{
		
	}

	/*
	 * @brief Destructor
	**/ 
	HTTP_CLIENT_ABSTRACT_MODEL_CLASS::~HTTP_CLIENT_ABSTRACT_MODEL_CLASS()
	{
		
	}
	
	/*
	 * @brief Set event publisher.
	**/ 
	void HTTP_CLIENT_ABSTRACT_MODEL_CLASS::set_event_publisher(boost::shared_ptr<EVENT_PUBLISHER_CLASS> event_publisher)
	{
		this->m_event_publisher = event_publisher;
	}

	/*
	 * @brief Set file descriptor.
    **/ 
	void HTTP_CLIENT_ABSTRACT_MODEL_CLASS::set_fd(IN ZOO_HANDLE fd)
	{
		this->m_fd = fd;
	}
	
	/*
	 * @brief Get file descriptor.
	 * @param fd   
	**/
	ZOO_HANDLE HTTP_CLIENT_ABSTRACT_MODEL_CLASS::get_fd()
	{
		return this->m_fd;
	}

	/*
	 * @brief run this request.
    **/ 
	void HTTP_CLIENT_ABSTRACT_MODEL_CLASS::run(void * req_option)
	{
		//reimplement 
	}	

	/*
     * @brief This method is executed when property changed value
     * @param model             The model type
     * @param property_name     The property has been changed value
    **/
	void HTTP_CLIENT_ABSTRACT_MODEL_CLASS::on_property_changed(DEVICE_INTERFACE * model,
																const ZOO_UINT32 property_name)
	{
		//this->notify_of_property_changed(property_name,property_value);
	}
	
} //namespace HTTP

