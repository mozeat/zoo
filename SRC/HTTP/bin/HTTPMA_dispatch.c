/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : HTTPMA_dispatch.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-05    Generator      created
*************************************************************/

#include "HTTPMA_dispatch.h"
#include "HTTPMA_implement.h"

/**
 *@brief HTTPMA_local_4A_create
 *@param fd
**/
static ZOO_INT32 HTTPMA_local_4A_create(const MQ4A_SERV_ADDR server,HTTP4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    HTTP4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = HTTP4A_SYSTEM_ERR;
        EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_PARAMETER_ERR,0,"request pointer is NULL ...ERROR");
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (HTTP4I_REPLY_HANDLE)MM4A_malloc(sizeof(HTTP4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = HTTP4A_SYSTEM_ERR;
            EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_SYSTEM_ERR,0,"malloc reply_handle failed ...ERROR");
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        HTTPMA_implement_4A_create(reply_handle);
    }
    return rtn;
}

/**
 *@brief HTTPMA_local_4A_cleanup
 *@param fd
**/
static ZOO_INT32 HTTPMA_local_4A_cleanup(const MQ4A_SERV_ADDR server,HTTP4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    HTTP4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = HTTP4A_SYSTEM_ERR;
        EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_PARAMETER_ERR,0,"request pointer is NULL ...ERROR");
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (HTTP4I_REPLY_HANDLE)MM4A_malloc(sizeof(HTTP4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = HTTP4A_SYSTEM_ERR;
            EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_SYSTEM_ERR,0,"malloc reply_handle failed ...ERROR");
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        HTTPMA_implement_4A_cleanup(request->request_body.cleanup_req_msg.fd,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief HTTPMA_local_4A_on_request_sig
 *@param req
**/
static ZOO_INT32 HTTPMA_local_4A_on_request_sig(const MQ4A_SERV_ADDR server,HTTP4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    HTTP4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = HTTP4A_SYSTEM_ERR;
        EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_PARAMETER_ERR,0,"request pointer is NULL ...ERROR");
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (HTTP4I_REPLY_HANDLE)MM4A_malloc(sizeof(HTTP4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = HTTP4A_SYSTEM_ERR;
            EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_SYSTEM_ERR,0,"malloc reply_handle failed ...ERROR");
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        HTTPMA_implement_4A_on_request_sig(&request->request_body.on_request_sig_msg.req,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief HTTPMA_local_4A_on_download_file_sig
 *@param fd
 *@param dl_option
**/
static ZOO_INT32 HTTPMA_local_4A_on_download_file_sig(const MQ4A_SERV_ADDR server,HTTP4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    HTTP4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = HTTP4A_SYSTEM_ERR;
        EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_PARAMETER_ERR,0,"request pointer is NULL ...ERROR");
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (HTTP4I_REPLY_HANDLE)MM4A_malloc(sizeof(HTTP4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = HTTP4A_SYSTEM_ERR;
            EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_SYSTEM_ERR,0,"malloc reply_handle failed ...ERROR");
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        HTTPMA_implement_4A_on_download_file_sig(request->request_body.on_download_file_sig_req_msg.fd,
                                           &request->request_body.on_download_file_sig_req_msg.dl_option,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief HTTPMA_local_4A_on_ssl_request_sig
 *@param req
 *@param ssl_option
**/
static ZOO_INT32 HTTPMA_local_4A_on_ssl_request_sig(const MQ4A_SERV_ADDR server,HTTP4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    HTTP4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = HTTP4A_SYSTEM_ERR;
        EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_PARAMETER_ERR,0,"request pointer is NULL ...ERROR");
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (HTTP4I_REPLY_HANDLE)MM4A_malloc(sizeof(HTTP4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = HTTP4A_SYSTEM_ERR;
            EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_SYSTEM_ERR,0,"malloc reply_handle failed ...ERROR");
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        HTTPMA_implement_4A_on_ssl_request_sig(&request->request_body.on_ssl_request_sig_msg.req,
                                           &request->request_body.on_ssl_request_sig_msg.ssl_option,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief HTTPMA_local_4A_handle_request
 *@param id
 *@param message
**/
static ZOO_INT32 HTTPMA_local_4A_handle_request(const MQ4A_SERV_ADDR server,HTTP4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    HTTP4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = HTTP4A_SYSTEM_ERR;
        EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_PARAMETER_ERR,0,"request pointer is NULL ...ERROR");
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (HTTP4I_REPLY_HANDLE)MM4A_malloc(sizeof(HTTP4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = HTTP4A_SYSTEM_ERR;
            EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_SYSTEM_ERR,0,"malloc reply_handle failed ...ERROR");
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        HTTPMA_implement_4A_handle_request(request->request_body.handle_request_msg.id,
                                           &request->request_body.handle_request_msg.message,
                                           reply_handle);
    }
    return rtn;
}

/**
*@brief Dispatch message from client to server internal interface
*@param context        
*@param server        address
*@param msg           request message to server
*@param len           request message length
*@param reply_msg     reply message length to caller
*@param reply_msg_len reply message length
**/
void HTTPMA_callback_handler(void * context,const MQ4A_SERV_ADDR server,void * msg,ZOO_INT32 len)
{
    ZOO_INT32 rtn = OK;
    ZOO_INT32 rep_length = 0;
    HTTP4I_REQUEST_STRUCT *request = (HTTP4I_REQUEST_STRUCT*)msg;
    HTTP4I_REPLY_STRUCT *reply = NULL;
    if(request == NULL)
    {
        rtn = HTTP4A_SYSTEM_ERR;
        EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_PARAMETER_ERR,0,"request_message pointer is NULL ...ERROR");
        return;
    }

    if(OK == rtn)
    {
        switch(request->request_header.function_code)
        {
            case HTTP4A_CREATE_CODE:
                HTTPMA_local_4A_create(server,request,request->request_header.function_code);
                break;
            case HTTP4A_CLEANUP_CODE:
                HTTPMA_local_4A_cleanup(server,request,request->request_header.function_code);
                break;
            case HTTP4A_ON_REQUEST_SIG_CODE:
                HTTPMA_local_4A_on_request_sig(server,request,request->request_header.function_code);
                break;
            case HTTP4A_ON_DOWNLOAD_FILE_SIG_CODE:
                HTTPMA_local_4A_on_download_file_sig(server,request,request->request_header.function_code);
                break;
            case HTTP4A_ON_SSL_REQUEST_SIG_CODE:
                HTTPMA_local_4A_on_ssl_request_sig(server,request,request->request_header.function_code);
                break;
            case HTTP4A_HANDLE_REQUEST_CODE:
                HTTPMA_local_4A_handle_request(server,request,request->request_header.function_code);
                break;
            default:
                EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_SYSTEM_ERR,0,"invalid function code ...ERROR");
                rtn = HTTP4A_SYSTEM_ERR;
                break;
        }
    }

    if(OK != rtn && request->request_header.need_reply)
    {
        rtn = HTTP4I_get_reply_message_length(request->request_header.function_code, &rep_length);
        if(OK != rtn)
        {
            EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_SYSTEM_ERR,0,"Get message length failed ...ERROR");
            rtn = HTTP4A_SYSTEM_ERR;
        }

        if(OK == rtn)
        {
            reply = (HTTP4I_REPLY_STRUCT *)MM4A_malloc(rep_length);
            if(reply == NULL)
            {
                EH4A_show_exception(HTTP4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,HTTP4A_SYSTEM_ERR,0,"malloc failed ...ERROR");
                rtn = HTTP4A_SYSTEM_ERR;
            }
        }

        if(OK == rtn)
        {
            memset(reply, 0x0, rep_length);
            reply->reply_header.execute_result = rtn;
            reply->reply_header.function_code = request->request_header.function_code;
            rtn = HTTP4I_send_reply_message(server,reply->reply_header.function_code,reply);
        }
    }
    return;
}

