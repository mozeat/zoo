/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : HTTPMA_main.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-05    Generator      created
*************************************************************/

#include <string.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include "HTTP4I_type.h"
#include "HTTPMA_executor_wrapper.h"
#include "HTTPMA_dispatch.h"


/* Task or Process Entrance */
/* Accept one parameter for server address or use default server address by task name*/
ZOO_INT32 main(int argc,char *argv[])
{
    ZOO_INT32 rtn = OK;
    /* Server address */
    MQ4A_SERV_ADDR server_addr = {0};
    if(argc >= 2 )
    {
        strncpy(server_addr, argv[1], strlen(argv[1]));
        server_addr[31]= '\0';
    }
    else
    {
        strncpy(server_addr,HTTP4A_SERVER,strlen(HTTP4A_SERVER));
    }

    /* Initialize memory pool */
    MM4A_initialize();

    /* Initliaze system and register info */
    HTTPMA_startup();

    /* Subscribe messages */
    HTTPMA_subscribe_driver_event();

    if(OK == rtn)
    {
        /* Initialize server to prepare recv messages*/
        rtn = MQ4A_server_initialize(server_addr);
    }

    if(OK == rtn)
    {
        /* Register handler for recieve and response messages */
        rtn = MQ4A_register_event_handler(server_addr,HTTPMA_callback_handler);
    }

    if(OK == rtn)
    {
        /* Enter in listen state ,it is a sync interface*/
        rtn = MQ4A_enter_event_loop(server_addr);
    }

    /* Close server */
    MQ4A_server_terminate(server_addr);

    /* Shutdown memory pool */
    MM4A_terminate();

    /* Task cleanup */
    HTTPMA_shutdown();
    return rtn;
}
