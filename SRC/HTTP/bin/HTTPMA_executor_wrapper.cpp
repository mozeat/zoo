/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTPMA_executor_wrapper.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "HTTPMA_executor_wrapper.h"
#include <ZOO_COMMON_MACRO_DEFINE.h>

#include "utils/THREAD_POOL.h"
#include "COMMON_FLOW_FACADE_CLASS.h"
#include "PROCESSING_FLOW_FACADE_CLASS.h"
#include "STATE_MANAGER_CLASS.h"
#include "HTTP_CONFIGURE.h"
#include "HTTP_SERVER_MODEL_CLASS.h"
#include "EVENT_PUBLISHER_CLASS.h"
#include "DEVICE_CONTROLLER_CLASS.h"


/*
 * @brief Define the device controller.
**/
boost::shared_ptr<HTTP::DEVICE_CONTROLLER_INTERFACE> g_device_controller;

/*
 * @brief Define common flow instance.
**/
boost::shared_ptr<HTTP::COMMON_FLOW_FACADE_INTERFACE> g_common_flow;

/*
 * @brief Define processing flow instance.
**/
boost::shared_ptr<HTTP::PROCESSING_FLOW_FACADE_INTERFACE> g_processing_flow;

/*
 * @brief Define the state manager.
**/
boost::shared_ptr<HTTP::STATE_MANAGER_CLASS> g_state_manager;

/*
 * @brief Define the state manager.
**/
boost::shared_ptr<HTTP::HTTP_SERVER_INTERFACE> g_http_server;

/*
 * @brief Define the state manager.
**/
boost::shared_ptr<HTTP::EVENT_PUBLISHER_CLASS> g_event_publisher;


/**  
 * @brief Register system signal handler,throw PARAMETER_EXCEPTION_CLASS if register fail,
 * the default signal handling is save stack trace to the log file and generate a dump file at execute path.
 * register a self-defined callback to SYSTEM_SIGNAL_HANDLER::resgister_siganl will change the default behavior.
**/
static void HTTPMA_register_system_signals()
{
    TR4A_trace(HTTP4I_COMPONET_ID,__ZOO_FUNC__," > function entry ...");
    __ZOO_SYSTEM_SIGNAL_REGISTER(HTTP4I_COMPONET_ID,SIGSEGV); 
    __ZOO_SYSTEM_SIGNAL_REGISTER(HTTP4I_COMPONET_ID,SIGABRT);

    /* Add more signals if needs,or register self-defined callback function
       to change the default behavior... */
    TR4A_trace(HTTP4I_COMPONET_ID,__ZOO_FUNC__," < function exit ...");
}

/**
 *@brief Execute the start up flow.
 * This function is executed in 3 steps: 
 * Step 1: Load configurations 
 * Step 2: Create controllers
 * Step 3: Create facades and set controllers to created facades
**/ 
void HTTPMA_startup(void)
{
    TR4A_trace(HTTP4I_COMPONET_ID,__ZOO_FUNC__," > function entry ...");
    ZOO_INT32 error_code = OK;
    /**
     * @brief Signal handler for system behavior 
    */
    HTTPMA_register_system_signals();

	__HTTP_TRY
	{
	    /** 
	     *@brief Step 1: Load configurations 
	    */
	    HTTP::HTTP_CONFIGURE::get_instance()->initialize();
	    
	    /** 
	     *@brief Startup thread pool. 
	    */
	    ZOO_COMMON::THREAD_POOL::get_instance()->startup();

	    /** 
	     *@brief Step 2: Create controllers 
	    */
	    g_http_server = (boost::dynamic_pointer_cast<HTTP::HTTP_SERVER_INTERFACE>
	    								(boost::make_shared<HTTP::HTTP_SERVER_MODEL_CLASS>()));
	    g_state_manager.reset(new HTTP::STATE_MANAGER_CLASS());
	    g_device_controller.reset(new HTTP::DEVICE_CONTROLLER_CLASS());
	    					
		g_event_publisher.reset(new HTTP::EVENT_PUBLISHER_CLASS());
	    /** 
	     *@brief Step 3: Create facades and set controllers to created facades 
	    */
	    
	    g_processing_flow.reset(new HTTP::PROCESSING_FLOW_FACADE_CLASS());
	    g_processing_flow->set_device_controller(g_device_controller);
	    g_processing_flow->set_state_manager(g_state_manager);
	    g_processing_flow->set_http_server(g_http_server);
	    g_http_server->add_observer(g_processing_flow.get()); 
	    g_http_server->set_event_publisher(g_event_publisher);
	    g_device_controller->add_observer(g_processing_flow.get());
	    g_device_controller->set_event_publisher(g_event_publisher);
	    
	    g_common_flow.reset(new HTTP::COMMON_FLOW_FACADE_CLASS());
	    g_common_flow->set_device_controller(g_device_controller);
	    g_common_flow->set_state_manager(g_state_manager);
	    g_common_flow->set_http_server(g_http_server);
	    g_http_server->add_observer(g_common_flow.get());
	    g_device_controller->add_observer(g_processing_flow.get()); 
	    
	    /** 
	     *@brief Step 4: run server
	    */
	    g_http_server->initialize();
	    g_http_server->run();
    }
    __HTTP_CATCH_ALL(error_code)
    
    TR4A_trace(HTTP4I_COMPONET_ID,__ZOO_FUNC__," < function exit ...");
}

/**
 *@brief This function response to release instance or memory 
**/ 
void HTTPMA_shutdown(void)
{
    TR4A_trace(HTTP4I_COMPONET_ID,__ZOO_FUNC__," > function entry ...");
    ZOO_INT32 error_code = OK;
    /** User add */
    __HTTP_TRY
	{
	    g_http_server->stop();
    	g_processing_flow.reset();
    	g_device_controller.reset();
    	g_state_manager.reset();
    	ZOO_COMMON::THREAD_POOL::get_instance()->shutdown();
    }
    __HTTP_CATCH_ALL(error_code)
    TR4A_trace(HTTP4I_COMPONET_ID,__ZOO_FUNC__," < function exit ...");
}

/**
 *@brief Subscribe events published from hardware drivers 
**/
void HTTPMA_subscribe_driver_event(void)
{
    TR4A_trace(HTTP4I_COMPONET_ID,__ZOO_FUNC__," > function entry ...");
    /** Subscribe events */

    TR4A_trace(HTTP4I_COMPONET_ID,__ZOO_FUNC__," < function exit ...");
}

