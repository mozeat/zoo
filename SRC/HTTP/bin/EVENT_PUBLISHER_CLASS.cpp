/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : EVENT_PUBLISHER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "EVENT_PUBLISHER_CLASS.h"
namespace HTTP
{
    /*
     * @brief Constructor
    **/ 
    EVENT_PUBLISHER_CLASS::EVENT_PUBLISHER_CLASS()
    {

    }

    /*
     * @brief Constructor
    **/ 
    EVENT_PUBLISHER_CLASS::~EVENT_PUBLISHER_CLASS()
    {

    }
    
    /*
	 * @brief Publish server req to subscriber
	**/ 
	void EVENT_PUBLISHER_CLASS::publish_HTTP4A_server_request(IN HTTP4A_REQUEST_STRUCT *req,
																IN ZOO_INT32 error_code)
    {
		HTTPMA_raise_4A_server_request_subscribe(req,error_code,NULL);
    }

	/*
	 * @brief Publish download/upload inprogress to subscriber
	**/ 
	void EVENT_PUBLISHER_CLASS::publish_HTTP4A_inprogress(IN HTTP4A_INPROGRESS_STRUCT *inprogress,
																IN ZOO_INT32 error_code)
    {
		HTTPMA_raise_4A_inprogress_subscribe(inprogress,error_code,NULL);
    }
	
	/*
	 * @brief Publish reply messgae from remote server
	**/ 
	void EVENT_PUBLISHER_CLASS::publish_HTTP4A_reply(IN HTTP4A_RESPONSE_STRUCT *res,
																IN ZOO_INT32 error_code)
    {
		HTTPMA_raise_4A_server_reply_subscribe(res,error_code,NULL);
    }

	/*
	 * @brief Publish server running status
	**/ 
	void EVENT_PUBLISHER_CLASS::publish_HTTP4A_status(IN HTTP4A_STATUS_STRUCT *status,
																IN ZOO_INT32 error_code)
    {
		HTTPMA_raise_4A_status_subscribe(status,error_code,NULL);
    }
} //namespace HTTP

