/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_CLIENT_MODEL_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "HTTP_CLIENT_MODEL_CLASS.h"
#include <iostream>
namespace HTTP
{
	/*
	 * @brief Constructor
	**/ 
	HTTP_CLIENT_MODEL_CLASS::HTTP_CLIENT_MODEL_CLASS()
        :m_ioc(1),m_resolver(boost::asio::make_strand(m_ioc))
        ,m_stream(boost::asio::make_strand(m_ioc))
    {
		this->m_fd = 0;
	}

	/*
	 * @brief Destructor
	**/ 
	HTTP_CLIENT_MODEL_CLASS::~HTTP_CLIENT_MODEL_CLASS()
	{
	    try
	    {
		    this->m_ioc.stop();
		}
		catch(...)
		{
		    ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,":: unhandle expection ...");
		}
	}

	/*
	 * @brief run.
	**/ 
	void HTTP_CLIENT_MODEL_CLASS::run(IN void * req_option)
	{
		HTTP4A_REQUEST_STRUCT * req = (HTTP4A_REQUEST_STRUCT *) req_option;
		if(req == NULL)
		{
			this->fail(HTTP4A_PARAMETER_ERR,"req_option is null.");
			return ;
		}
		
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," function entry ... ");
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"set content_type: %s ",req->content_type);	
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"set connection timeout: %d",req->timeout);
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"set keep_alive: %d",req->keep_alive);
		network::uri url{req->target};	
		std::string target = url.path().to_string() ;
		if(url.has_query())
		    target.append(url.query().to_string());
		if(url.has_fragment())    
		    target.append(url.fragment().to_string());
		    
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"set target: %s",target.c_str());
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"set host: %s,port:%s",
                                                        url.host().to_string().c_str(),url.port().to_string().c_str());
	    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"req body: %s",req->body);
									
		this->m_req.method(static_cast<boost::beast::http::verb>(req->method));
        this->m_req.target(url.path().empty() ? "/" : target);
		this->m_req.set(boost::beast::http::field::host, url.host().to_string());
		this->m_req.set(boost::beast::http::field::user_agent,BOOST_BEAST_VERSION_STRING);
		this->m_req.set(boost::beast::http::field::content_type, req->content_type);
        this->m_req.keep_alive(req->keep_alive == ZOO_TRUE ? true:false);
        this->m_req_timeout = req->timeout;		
        if(req->method == HTTP4A_METHOD_POST)
        {
            //std::string body(req->body,strlen(req->body));
        	this->m_req.body() = std::string(req->body);
        	this->m_req.prepare_payload();
		    /*ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
		                            "payload_size: %lu",
									this->m_req.payload_size().);*/
        }
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
		                            "req method: %s",
									this->m_req.method_string().data());
        std::string port = url.port().to_string();
        if(url.port().empty())
        {
            port = "80";
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
                                                "set default port: %s",port.data());
        }
		// Look up the domain name
        this->m_resolver.async_resolve(url.host().to_string(),port,
            boost::beast::bind_front_handler(&HTTP_CLIENT_MODEL_CLASS::on_resolve,this));

        ZOO_COMMON::THREAD_POOL::get_instance()->queue_working
        (
	        [&]
	        {
	        	this->m_ioc.run();
	        }
        );
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < ");    
	}

	/*
	 * @brief resolve host info.
	 * @param ec      error_code
	 * @param results   
	**/
	void HTTP_CLIENT_MODEL_CLASS::on_resolve(boost::beast::error_code ec,
												boost::asio::ip::tcp::resolver::results_type results)
	{
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," function entry ... ");
		if(ec)
            return fail(HTTP4A_CLIENT_RESOLVE_ERR, ec.message());

        // Make the connection on the IP address we get from a lookup
        this->m_stream.async_connect(results,boost::beast::bind_front_handler(
                						&HTTP_CLIENT_MODEL_CLASS::on_connect,this));
        //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," function exit ...");
	}

	/*
	 * @brief connect host.
	 * @param ec      error_code
	 * @param results   
	**/
	void HTTP_CLIENT_MODEL_CLASS::on_connect(boost::beast::error_code ec,boost::asio::ip::tcp::resolver::results_type::endpoint_type)
	{
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," function entry ... ");
		if(ec)
            return fail(HTTP4A_CONNECT_ERR, ec.message());
        // Set a timeout on the operation
        //boost::beast::get_lowest_layer(this->m_stream).expires_after(std::chrono::seconds(this->m_req_timeout));
		this->m_stream.expires_after(std::chrono::seconds(this->m_req_timeout));
    	// Send the HTTP request to the remote host
    	boost::beast::http::async_write(this->m_stream, this->m_req,
        	boost::beast::bind_front_handler(
            	&HTTP_CLIENT_MODEL_CLASS::on_write,this));
        //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," function exit ...");
	}

	/*
	 * @brief perform http request
	 * @param ec      error_code
	 * @param bytes_transferred   
	**/	
	void HTTP_CLIENT_MODEL_CLASS::on_write(boost::beast::error_code ec,std::size_t bytes_transferred)
	{
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," function entry ... ");
		boost::ignore_unused(bytes_transferred);

        if(ec)
            return fail(HTTP4A_CLIENT_SEND_MESSAGE_ERR, ec.message());

        // Receive the HTTP response
        boost::beast::http::async_read(this->m_stream, this->m_buffer, this->m_res,
            boost::beast::bind_front_handler(
                &HTTP_CLIENT_MODEL_CLASS::on_read,this));
        //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," function exit ...");
	}
	
	/*
	 * @brief read reply from host.
	 * @param ec      error_code
	 * @param bytes_transferred   
	**/
	void HTTP_CLIENT_MODEL_CLASS::on_read(boost::beast::error_code ec,std::size_t bytes_transferred)
	{
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," function entry ... ");
		boost::ignore_unused(bytes_transferred);

        if(ec)
            return fail(HTTP4A_CLIENT_RECV_MESSAGE_ERR, ec.message());

        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,
        					__ZOO_FUNC__,
        					" response: %s",this->m_res.body().c_str());
        auto content_type = this->m_req.at(boost::beast::http::field::content_type).to_string();
        const std::string & body = this->m_res.body();
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,
        					__ZOO_FUNC__,
        					" body.size(): %lu",body.size());
        if(body.size() <= HTTP4A_BODY_LENGTH)
        {
            auto message = this->build_response_message(this->m_fd,
    														OK,content_type.c_str(),this->m_res.body());
    		this->m_event_publisher->publish_HTTP4A_reply(message.get(),OK);
        }
        else
        {        
            for(int i = 0 ; i < body.size() / HTTP4A_BODY_LENGTH ; ++i)
            {
                std::string part = body.substr(i * HTTP4A_BODY_LENGTH,HTTP4A_BODY_LENGTH);
        		auto message = this->build_response_message(this->m_fd,
        														OK,content_type.c_str(),part);
        		this->m_event_publisher->publish_HTTP4A_reply(message.get(),OK);
    		}

            ZOO_INT32 remain_part_length = body.size() % HTTP4A_BODY_LENGTH;
    		if(remain_part_length > 0)
    		{
    		    std::string part(body.size() - remain_part_length,remain_part_length);
        		auto message = this->build_response_message(this->m_fd,
        														OK,content_type.c_str(),part);
        		this->m_event_publisher->publish_HTTP4A_reply(message.get(),OK);
    		}
		}
		this->m_stream.socket().shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
        	// not_connected happens sometimes so don't bother reporting it.
        if(ec && ec != boost::beast::errc::not_connected)
            fail(HTTP4A_DISCONNECT_ERR, "shutdown");
        //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"  function exit ... ");
	}
	
	/*
	 * @brief Shutdown connection.
	 * @param ec      error_code
	**/
	void HTTP_CLIENT_MODEL_CLASS::on_shutdown(boost::beast::error_code ec)
	{
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," function entry ... ");
		if(ec == boost::asio::error::eof)
        {
            ec = {};
        }
        
        if(ec)
        {
            fail(HTTP4A_CONNECT_ERR, "shutdown");
        }
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," function exit ... ");
	}
	
	/*
	 * @brief print error informations.
	 * @param ec	  error_code
	 * @param why	
	**/
	void HTTP_CLIENT_MODEL_CLASS::fail(ZOO_INT32 error_code,std::string why)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," function entry ...");
		if(!why.empty())
			ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,"error_code:%d,why:%s",error_code,why.c_str());
		auto message = this->build_response_message(this->m_fd,
													error_code,"",why);
		
		this->m_event_publisher->publish_HTTP4A_reply(message.get(),HTTP4A_SYSTEM_ERR);

        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," function exit ... ");
	}
} //namespace HTTP
