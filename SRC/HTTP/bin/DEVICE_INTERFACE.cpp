/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : DEVICE_INTERFACE.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "DEVICE_INTERFACE.h"
namespace HTTP
{
    /*
     * @brief Constructor
    **/ 
    DEVICE_INTERFACE::DEVICE_INTERFACE()
    {
    }

    /*
     * @brief Destructor
    **/ 
    DEVICE_INTERFACE::~DEVICE_INTERFACE()
    {
    }

    /*
     * @brief Add observer will be notified when property changed.
     * @param observer  Property changed observer
    **/ 
    void DEVICE_INTERFACE::add_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer)
    {
        this->remove_observer(observer);
        this->m_observers.push_back(observer);
    }

    /*
     * @brief Remove observer will be notified when property changed.
     * @param observer  Property changed observer
    **/ 
    void DEVICE_INTERFACE::remove_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer)
    {
        std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>*>::iterator observer_itr =
            this->m_observers.begin();
        while (observer_itr != this->m_observers.end())
        {
            PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* find_observer = *observer_itr;
            if (observer == find_observer)
            {
                this->m_observers.erase(observer_itr);
                break;
            }
        }
    }

    /*
     * @brief Clean.
    **/
    void DEVICE_INTERFACE::clean()
    {
        this->m_observers.clear();
    }

    /*
     * @brief Notify property has been changed.
     * @param property_name     The property has been changed
     * @param property_value     The property value
    **/ 
    void DEVICE_INTERFACE::notify_of_property_changed(const ZOO_UINT32 property_name)
    {
        for (vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>*>::iterator observer_itr =
            this->m_observers.begin(); observer_itr != this->m_observers.end(); observer_itr++)
        {
            PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer = *observer_itr;
            if (observer != NULL)
            {
                observer->on_property_changed(this,property_name);
            }
        }
    }
    
    /*
	 *@brief build_response_message.
     *@param fd     
     *@param error_code     The error code
     *@param content_type     
     *@param body
     *@return response message
    **/ 
    boost::shared_ptr<HTTP4A_RESPONSE_STRUCT> DEVICE_INTERFACE::build_response_message(IN ZOO_HANDLE fd,
																			  IN ZOO_INT32 error_code,
																			  IN std::string content_type,
																			  IN std::string & body)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__," error_code: %d",error_code);
        boost::shared_ptr<HTTP4A_RESPONSE_STRUCT> message(new HTTP4A_RESPONSE_STRUCT());
        message->fd = fd;
        message->error_code = error_code;
        if(body.size() < HTTP4A_BODY_LENGTH)
        {
        	memcpy(message->body,body.c_str(),body.size() + 1);
        }
        else
        {
        	memcpy(message->body,body.c_str(),HTTP4A_BODY_LENGTH);
        }
        message->body[HTTP4A_BODY_LENGTH - 1] = '\0';
        return message;
    }

    /*
	 *@brief build_response_message.
     *@param inprogress     
     *@return response message
    **/ 
    boost::shared_ptr<HTTP4A_INPROGRESS_STRUCT> DEVICE_INTERFACE::build_message(IN ZOO_HANDLE fd,
	    																IN std::string file_name,
	    																IN ZOO_INT32 speed,
	    																IN ZOO_INT32 total_time)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__PRETTY_FUNCTION__," file_name: %s",file_name.c_str());
        boost::shared_ptr<HTTP4A_INPROGRESS_STRUCT> message(new HTTP4A_INPROGRESS_STRUCT());
        message->fd = fd;
        message->speed = speed;
        message->total_time = total_time;
        if(file_name.size() < HTTP4A_FILE_NAME_LENGTH)
        {
        	memcpy(message->file_name,file_name.c_str(),file_name.size() + 1);
        }
        else
        {
        	memcpy(message->file_name,file_name.c_str(),HTTP4A_FILE_NAME_LENGTH);
        }
        message->file_name[HTTP4A_FILE_NAME_LENGTH - 1] ='\0';
        return message;
    }		
} // namespace HTTP

