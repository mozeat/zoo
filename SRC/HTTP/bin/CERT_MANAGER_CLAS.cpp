/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : CERT_MANAGER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "CERT_MANAGER_CLASS.h"
namespace HTTP
{
	boost::shared_ptr<CERT_MANAGER_CLASS> CERT_MANAGER_CLASS::m_instance;
	boost::once_flag CERT_MANAGER_CLASS::m_once_flag = BOOST_ONCE_INIT;
	CERT_MANAGER_CLASS::CERT_MANAGER_CLASS()
	{
	    this->m_verify_mode= boost::asio::ssl::verify_fail_if_no_peer_cert;
	    this->m_tls_version = boost::asio::ssl::context::tlsv12;
	}

	CERT_MANAGER_CLASS::~CERT_MANAGER_CLASS()
	{
	
	}
	
	/*
	* @brief get instance
    **/ 
    boost::shared_ptr<CERT_MANAGER_CLASS>  CERT_MANAGER_CLASS::get_instance()
	{
		boost::call_once
		(
			CERT_MANAGER_CLASS::m_once_flag,
			[&]
			{
				CERT_MANAGER_CLASS::m_instance.reset(new CERT_MANAGER_CLASS());
			}
		);
		return CERT_MANAGER_CLASS::m_instance;
	}
	
	/*
	* @brief get_verify_path
	**/
	void CERT_MANAGER_CLASS::set_verify_path(std::string path)
	{
		this->m_path = path;
	}

	/*
	* @brief get_verify_path
	**/ 
	std::string CERT_MANAGER_CLASS::get_verify_path()
	{
		return this->m_path;
	}

	/*
	* @brief Get public_key
	**/ 
	void CERT_MANAGER_CLASS::set_public_key_file(CERT_TYEP_ENUM cert_type,std::string public_key_file)
	{
		switch(cert_type)
		{
			case CERT_CLIENT:
				this->m_client_public_key_file = public_key_file;
				break;
			case CERT_SEVER:
				this->m_server_public_key_file = public_key_file;
				break;	
		}
	}

	/*
	* @brief Set public_key
	**/ 
	std::string CERT_MANAGER_CLASS::get_public_key_file(CERT_TYEP_ENUM cert_type)
	{
		switch(cert_type)
		{
			case CERT_CLIENT:
				return this->m_client_public_key_file;
			case CERT_SEVER:
				return this->m_server_public_key_file;
		}
		return "invalid";
	}

	/*
	* @brief Set public_key
	**/ 
	void CERT_MANAGER_CLASS::set_private_key_file(CERT_TYEP_ENUM cert_type,std::string private_key_file)
	{
		switch(cert_type)
		{
			case CERT_CLIENT:
				this->m_client_private_key_file = private_key_file;
				break;
			case CERT_SEVER:
				this->m_server_private_key_file = private_key_file;
				break;	
		}
	}

	/*
	* @brief Get public_key
	**/ 
	std::string CERT_MANAGER_CLASS::get_private_key_file(CERT_TYEP_ENUM cert_type)
	{
		switch(cert_type)
		{
			case CERT_CLIENT:
				return this->m_client_private_key_file;
			case CERT_SEVER:
				return this->m_server_private_key_file;
		}
		return "invalid";
	}
   
	 /*
	 * @brief Set trust_chain file
	**/ 
	void CERT_MANAGER_CLASS::set_trust_chain_file(CERT_TYEP_ENUM cert_type,std::string trust_chain_file)
	{
		switch(cert_type)
		{
			case CERT_CLIENT:
				this->m_client_trust_chain_file = trust_chain_file;
				break;
			case CERT_SEVER:
				this->m_server_trust_chain_file = trust_chain_file;
				break;	
		}
	}

	 /*
	 * @brief Get trust_chain file
	**/ 
	std::string CERT_MANAGER_CLASS::get_trust_chain_file(CERT_TYEP_ENUM cert_type)
	{
		switch(cert_type)
		{
			case CERT_CLIENT:
				return this->m_client_trust_chain_file;
			case CERT_SEVER:
				return this->m_server_trust_chain_file;
		}
		return "invalid";
	}
	void CERT_MANAGER_CLASS::set_verify_mode(int mode)
	{
		this->m_verify_mode = mode;
	}
    int CERT_MANAGER_CLASS::get_verify_mode()
	{
		return this->m_verify_mode;
	}
    void CERT_MANAGER_CLASS::set_tls_version(int version)
	{
		this->m_tls_version = version;
	}
    int CERT_MANAGER_CLASS::get_tls_version()
	{
		return this->m_tls_version;
	}
}

