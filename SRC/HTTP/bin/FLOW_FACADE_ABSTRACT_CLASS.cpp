/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : FLOW_FACADE_ABSTRACT_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "FLOW_FACADE_ABSTRACT_CLASS.h"
namespace HTTP
{
    /*
     * @brief Constructor
    **/ 
    FLOW_FACADE_ABSTRACT_CLASS::FLOW_FACADE_ABSTRACT_CLASS()
    {

    }

    /*
     * @brief Constructor
    **/ 
    FLOW_FACADE_ABSTRACT_CLASS::~FLOW_FACADE_ABSTRACT_CLASS()
    {

    }

    /*
     * @brief Set device controller instance
    **/ 
    void FLOW_FACADE_ABSTRACT_CLASS::set_device_controller(IN boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE> device_controller)
    {
        this->m_device_controller = device_controller;
    }

    /*
     * @brief Get device controller instance
    **/ 
    boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE>  FLOW_FACADE_ABSTRACT_CLASS::get_device_controller()
    {
        return this->m_device_controller;
    }

    /*
     * @brief Set state manager instance
    **/ 
    void FLOW_FACADE_ABSTRACT_CLASS::set_state_manager(IN boost::shared_ptr<STATE_MANAGER_CLASS> state_manager)
    {
        this->m_state_manager = state_manager;
    }

    /*
     * @brief Get state manager instance
    **/ 
    boost::shared_ptr<STATE_MANAGER_CLASS>  FLOW_FACADE_ABSTRACT_CLASS::get_state_manager()
    {
        return this->m_state_manager;
    }
    
	/*
	 * @brief Set http server instance
	**/ 
	void FLOW_FACADE_ABSTRACT_CLASS::set_http_server(IN boost::shared_ptr<HTTP_SERVER_INTERFACE> hs_model)
    {
        this->m_http_server = hs_model;
    }

    /*
     * @brief This method is executed when property changed value
     * @param model The source object contains property changed
     * @param property_name The property has been changed value
    **/ 
    void FLOW_FACADE_ABSTRACT_CLASS::on_property_changed(IN CONTROLLER_INTERFACE* model,
    								IN const ZOO_UINT32 property_name)
    {
        // The implementation for this method will be written in concrete classes
    }

} //namespace HTTP

