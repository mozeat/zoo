/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : HTTPMA_implement.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-05    Generator      created
*************************************************************/
#include "HTTPMA_implement.h"
#include "HTTP_FLOW_FACADE_WRAPPER.h"

/**
 *@brief HTTPMA_implement_4A_create
 *@param fd
**/
void HTTPMA_implement_4A_create(IN HTTP4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    ZOO_HANDLE fd;
    /* User add some code here if had special need. */
    rtn = HTTP_create(&fd);

    /* User add ... END*/
    HTTPMA_raise_4A_create(rtn,fd,reply_handle);
}

/**
 *@brief HTTPMA_implement_4A_cleanup
 *@param fd
**/
void HTTPMA_implement_4A_cleanup(IN ZOO_HANDLE fd,
                                     IN HTTP4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = HTTP_cleanup(fd);

    /* User add ... END*/
    HTTPMA_raise_4A_cleanup(rtn,reply_handle);
}

/**
 *@brief HTTPMA_implement_4A_on_request_sig
 *@param req
**/
void HTTPMA_implement_4A_on_request_sig(IN HTTP4A_REQUEST_STRUCT* req,
                                            IN HTTP4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = HTTP_on_request_sig(req);

    /* Do not reply a signal request.*/
    if(reply_handle != NULL)
    {
        /* Ignore unused parameter warnning.*/
        rtn = rtn;
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }
}

/**
 *@brief HTTPMA_implement_4A_on_download_file_sig
 *@param fd
 *@param dl_option
**/
void HTTPMA_implement_4A_on_download_file_sig(IN ZOO_HANDLE fd,
                                                  IN HTTP4A_DOWNLOAD_OPTION_STRUCT* dl_option,
                                                  IN HTTP4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = HTTP_on_download_file_sig(fd,dl_option);

    /* Do not reply a signal request.*/
    if(reply_handle != NULL)
    {
        /* Ignore unused parameter warnning.*/
        rtn = rtn;
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }
}

/**
 *@brief HTTPMA_implement_4A_on_ssl_request_sig
 *@param req
 *@param ssl_option
**/
void HTTPMA_implement_4A_on_ssl_request_sig(IN HTTP4A_REQUEST_STRUCT* req,
                                                IN HTTP4A_SSL_OPTION_STRUCT* ssl_option,
                                                IN HTTP4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = HTTP_on_ssl_request_sig(req,ssl_option);

    /* Do not reply a signal request.*/
    if(reply_handle != NULL)
    {
        /* Ignore unused parameter warnning.*/
        rtn = rtn;
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }
}

/**
 *@brief HTTPMA_implement_4A_handle_request
 *@param id
 *@param message
**/
void HTTPMA_implement_4A_handle_request(IN ZOO_INT32 id,
                                            IN HTTP4A_RESPONSE_STRUCT* message,
                                            IN HTTP4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = HTTP_handle_request(id,message);

    /* User add ... END*/
    HTTPMA_raise_4A_handle_request(rtn,reply_handle);
}


