/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_FLOW_FACADE_WRAPPER.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-05    Generator      created
*************************************************************/
#include "HTTP_FLOW_FACADE_WRAPPER.h"
//#include "COMMON_FLOW_FACADE_CLASS.h"
#include "PROCESSING_FLOW_FACADE_CLASS.h"
/*
 * @brief extern g_common_flow defined from executor_wrapper.cpp
**/
//extern HTTP::COMMON_FLOW_FACADE_CLASS * g_common_flow;
/*
 * @brief extern g_processing_flow defined from executor_wrapper.cpp
**/
extern HTTP::PROCESSING_FLOW_FACADE_CLASS * g_processing_flow;
/*
 * @brief HTTP4A_create
**/
ZOO_INT32 HTTP_create(INOUT ZOO_HANDLE * fd)
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->create(fd);
    }
    return HTTP4A_SYSTEM_ERR;
}

/*
 * @brief HTTP4A_cleanup
**/
ZOO_INT32 HTTP_cleanup(IN ZOO_HANDLE fd)
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->cleanup(fd);
    }
    return HTTP4A_SYSTEM_ERR;
}

/*
 * @brief HTTP4A_on_request_sig
**/
ZOO_INT32 HTTP_on_request_sig(IN HTTP4A_REQUEST_STRUCT * req)
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->on_request_sig(req);
    }
    return HTTP4A_SYSTEM_ERR;
}

/*
 * @brief HTTP4A_on_download_file_sig
**/
ZOO_INT32 HTTP_on_download_file_sig(IN ZOO_HANDLE fd,
															IN HTTP4A_DOWNLOAD_OPTION_STRUCT * dl_option)
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->on_download_file_sig(fd,dl_option);
    }
    return HTTP4A_SYSTEM_ERR;
}

/*
 * @brief HTTP4A_on_ssl_request_sig
**/
ZOO_INT32 HTTP_on_ssl_request_sig(IN HTTP4A_REQUEST_STRUCT * req,
																	IN HTTP4A_SSL_OPTION_STRUCT * ssl_option)
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->on_ssl_request_sig(req,ssl_option);
    }
    return HTTP4A_SYSTEM_ERR;
}

/*
 * @brief HTTP4A_handle_request
**/
ZOO_INT32 HTTP_handle_request(IN ZOO_INT32 id,
												 		IN HTTP4A_RESPONSE_STRUCT * message)
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->handle_request(id,message);
    }
    return HTTP4A_SYSTEM_ERR;
}

