/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : COMMON_FLOW_FACADE_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#include "COMMON_FLOW_FACADE_CLASS.h"
namespace HTTP
{
    /*
     * @brief Constructor
    **/ 
    COMMON_FLOW_FACADE_CLASS::COMMON_FLOW_FACADE_CLASS()
    {

    }

    /*
     * @brief Constructor
    **/ 
    COMMON_FLOW_FACADE_CLASS::~COMMON_FLOW_FACADE_CLASS()
    {

    }

    /*
     * @brief Initialize the model
     * @return error_code 
    **/ 
    void COMMON_FLOW_FACADE_CLASS::initialize()
    {
    	this->m_state_manager->update_driver_state(ZOO_DRIVER_STATE_IDLE);
    }

    /*
     * @brief Terminate the model
     * @return error_code 
    **/ 
    void COMMON_FLOW_FACADE_CLASS::terminate()
    {
    	this->m_state_manager->update_driver_state(ZOO_DRIVER_STATE_TERMINATED);
    }

    /*
     * @brief Get driver state
     * @return state 
    **/ 
    ZOO_DRIVER_STATE_ENUM COMMON_FLOW_FACADE_CLASS::get_driver_state()
    {
    	return this->m_state_manager->get_driver_state();
    }

    /*
     * @brief Get device status.
     * @return error code 
    **/ 
    ZOO_INT32 COMMON_FLOW_FACADE_CLASS::get_status(INOUT HTTP4A_STATUS_STRUCT * status)
    {
    	return OK;
    }

    /*
     * @brief Change to debug mode
    **/ 
    ZOO_INT32 COMMON_FLOW_FACADE_CLASS::change_to_debug_mode()
    {
    	this->m_state_manager->update_driver_state(ZOO_DRIVER_STATE_TERMINATED);
    	return OK;
    }

    /*
     * @brief Change to work mode
    **/ 
    ZOO_INT32 COMMON_FLOW_FACADE_CLASS::change_to_work_mode()
    {
    	this->m_state_manager->update_driver_state(ZOO_DRIVER_STATE_BUSY);
    	return OK;
    }

    /*
     * @brief This method is executed when property changed value
     * @param model             The model type
     * @param property_name     The property has been changed value
    **/ 
    void COMMON_FLOW_FACADE_CLASS::on_property_changed(CONTROLLER_INTERFACE * model,
    															const ZOO_UINT32 property_name)
    {
    	switch(property_name)
    	{
    		case CONNECTION_CLOSE:
    		{
    			break;
    		}
    		default:
    			break;
    	}
    }

} //namespace HTTP

