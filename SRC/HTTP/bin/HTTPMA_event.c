/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : HTTPMA_event.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-05    Generator      created
*************************************************************/
#include "HTTPMA_event.h"

/**
 *@brief HTTPMA_raise_4A_create
 *@param fd
**/
ZOO_INT32 HTTPMA_raise_4A_create(IN ZOO_INT32 error_code,
                                     IN ZOO_HANDLE fd,
                                     IN HTTP4I_REPLY_HANDLE reply_handle)
{
    HTTP4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = HTTP4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = HTTP4I_get_reply_message_length(HTTP4A_CREATE_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (HTTP4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = HTTP4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = HTTP4A_CREATE_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.create_rep_msg.fd,&fd,sizeof(ZOO_HANDLE));
                rtn = HTTP4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = HTTP4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief HTTPMA_raise_4A_cleanup
 *@param fd
**/
ZOO_INT32 HTTPMA_raise_4A_cleanup(IN ZOO_INT32 error_code,
                                      IN HTTP4I_REPLY_HANDLE reply_handle)
{
    HTTP4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = HTTP4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = HTTP4I_get_reply_message_length(HTTP4A_CLEANUP_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (HTTP4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = HTTP4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = HTTP4A_CLEANUP_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = HTTP4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = HTTP4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief HTTPMA_raise_4A_on_request_sig
 *@param req
**/
ZOO_INT32 HTTPMA_raise_4A_on_request_sig(IN ZOO_INT32 error_code,
                                             IN HTTP4I_REPLY_HANDLE reply_handle)
{
    HTTP4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = HTTP4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = HTTP4I_get_reply_message_length(HTTP4A_ON_REQUEST_SIG_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (HTTP4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = HTTP4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = HTTP4A_ON_REQUEST_SIG_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = HTTP4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = HTTP4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief HTTPMA_raise_4A_on_download_file_sig
 *@param fd
 *@param dl_option
**/
ZOO_INT32 HTTPMA_raise_4A_on_download_file_sig(IN ZOO_INT32 error_code,
                                                   IN HTTP4I_REPLY_HANDLE reply_handle)
{
    HTTP4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = HTTP4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = HTTP4I_get_reply_message_length(HTTP4A_ON_DOWNLOAD_FILE_SIG_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (HTTP4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = HTTP4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = HTTP4A_ON_DOWNLOAD_FILE_SIG_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = HTTP4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = HTTP4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief HTTPMA_raise_4A_on_ssl_request_sig
 *@param req
 *@param ssl_option
**/
ZOO_INT32 HTTPMA_raise_4A_on_ssl_request_sig(IN ZOO_INT32 error_code,
                                                 IN HTTP4I_REPLY_HANDLE reply_handle)
{
    HTTP4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = HTTP4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = HTTP4I_get_reply_message_length(HTTP4A_ON_SSL_REQUEST_SIG_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (HTTP4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = HTTP4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = HTTP4A_ON_SSL_REQUEST_SIG_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = HTTP4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = HTTP4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief HTTPMA_raise_4A_handle_request
 *@param id
 *@param message
**/
ZOO_INT32 HTTPMA_raise_4A_handle_request(IN ZOO_INT32 error_code,
                                             IN HTTP4I_REPLY_HANDLE reply_handle)
{
    HTTP4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = HTTP4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = HTTP4I_get_reply_message_length(HTTP4A_HANDLE_REQUEST_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (HTTP4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = HTTP4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = HTTP4A_HANDLE_REQUEST_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = HTTP4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = HTTP4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief HTTP4A_server_request_subscribe
 *@param req
 *@param error_code
 *@param *context
**/
void HTTPMA_raise_4A_server_request_subscribe(IN HTTP4A_REQUEST_STRUCT *req,IN ZOO_INT32 error_code,IN void *context)
{
    ZOO_INT32 rtn = OK;
    HTTP4I_REPLY_STRUCT * reply_message = NULL;
    reply_message = (HTTP4I_REPLY_STRUCT * ) MM4A_malloc(sizeof(HTTP4I_REPLY_STRUCT));
    if(NULL == reply_message)
    {
        rtn = HTTP4A_PARAMETER_ERR;
    }
    
    if(OK == rtn)
    {
        reply_message->reply_header.function_code = HTTP4A_SERVER_REQUEST_SUBSCRIBE_CODE;
        reply_message->reply_header.execute_result = error_code;
        memcpy(&reply_message->reply_body.server_request_subscribe_code_rep_msg.req,req,sizeof(HTTP4A_REQUEST_STRUCT));
    }

    if(OK == rtn)
    {
        rtn = HTTP4I_publish_event(HTTP4A_SERVER,
                                            HTTP4A_SERVER_REQUEST_SUBSCRIBE_CODE,
                                            reply_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }

    return;
}
/**
 *@brief HTTP4A_inprogress_subscribe
 *@param inprogress
 *@param error_code
 *@param *context
**/
void HTTPMA_raise_4A_inprogress_subscribe(IN HTTP4A_INPROGRESS_STRUCT *inprogress,IN ZOO_INT32 error_code,IN void *context)
{
    ZOO_INT32 rtn = OK;
    HTTP4I_REPLY_STRUCT * reply_message = NULL;
    reply_message = (HTTP4I_REPLY_STRUCT * ) MM4A_malloc(sizeof(HTTP4I_REPLY_STRUCT));
    if(NULL == reply_message)
    {
        rtn = HTTP4A_PARAMETER_ERR;
    }
    
    if(OK == rtn)
    {
        reply_message->reply_header.function_code = HTTP4A_INPROGRESS_SUBSCRIBE_CODE;
        reply_message->reply_header.execute_result = error_code;
        memcpy(&reply_message->reply_body.inprogress_subscribe_code_rep_msg.inprogress,inprogress,sizeof(HTTP4A_INPROGRESS_STRUCT));
    }

    if(OK == rtn)
    {
        rtn = HTTP4I_publish_event(HTTP4A_SERVER,
                                            HTTP4A_INPROGRESS_SUBSCRIBE_CODE,
                                            reply_message);
    }


    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }

    return;
}
/**
 *@brief HTTP4A_server_reply_subscribe
 *@param reply
 *@param error_code
 *@param *context
**/
void HTTPMA_raise_4A_server_reply_subscribe(IN HTTP4A_RESPONSE_STRUCT *reply,IN ZOO_INT32 error_code,IN void *context)
{
    ZOO_INT32 rtn = OK;
    HTTP4I_REPLY_STRUCT * reply_message = NULL;
    reply_message = (HTTP4I_REPLY_STRUCT * ) MM4A_malloc(sizeof(HTTP4I_REPLY_STRUCT));
    if(NULL == reply_message)
    {
        rtn = HTTP4A_PARAMETER_ERR;
    }
    
    if(OK == rtn)
    {
        reply_message->reply_header.function_code = HTTP4A_SERVER_REPLY_SUBSCRIBE_CODE;
        reply_message->reply_header.execute_result = error_code;
        memcpy(&reply_message->reply_body.server_reply_subscribe_code_rep_msg.reply,reply,sizeof(HTTP4A_RESPONSE_STRUCT));
    }

    if(OK == rtn)
    {
        rtn = HTTP4I_publish_event(HTTP4A_SERVER,
                                            HTTP4A_SERVER_REPLY_SUBSCRIBE_CODE,
                                            reply_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }

    return;
}
/**
 *@brief HTTP4A_status_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
void HTTPMA_raise_4A_status_subscribe(IN HTTP4A_STATUS_STRUCT *status,IN ZOO_INT32 error_code,IN void *context)
{
    ZOO_INT32 rtn = OK;
    HTTP4I_REPLY_STRUCT * reply_message = NULL;
    reply_message = (HTTP4I_REPLY_STRUCT * ) MM4A_malloc(sizeof(HTTP4I_REPLY_STRUCT));
    if(NULL == reply_message)
    {
        rtn = HTTP4A_PARAMETER_ERR;
    }
    
    if(OK == rtn)
    {
        reply_message->reply_header.function_code = HTTP4A_STATUS_SUBSCRIBE_CODE;
        reply_message->reply_header.execute_result = error_code;
        memcpy(&reply_message->reply_body.status_subscribe_code_rep_msg.status,status,sizeof(HTTP4A_STATUS_STRUCT));
    }

    if(OK == rtn)
    {
        rtn = HTTP4I_publish_event(HTTP4A_SERVER,
                                            HTTP4A_STATUS_SUBSCRIBE_CODE,
                                            reply_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }

    return;
}

