include ../Makefile_tpl_cov
include ../Project_config

TARGET   := HTTPMA
SRCEXTS  := .cpp .c 
INCDIRS  := ./inc ./com 
SOURCES  := 
SRCDIRS  := ./bin ./lib 
CFLAGS   := 
CXXFLAGS := -std=c++14 -fstack-protector-all
CPPFLAGS := -DBOOST_ALL_DYN_LINK
LDFPATH  := -L$(THIRD_PARTY_LIBRARY_PATH)
LDFLAGS  := $(GCOV_LINK) $(LDFPATH) -lTR4A -lEH4A -lMM4A -lMQ4A -lZOO -lboost_context -lURI -lssl -lcrypto 

include ../Makefile_tpl_linux