/**************************************************************
 * Copyright (C) 2017, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : PS
 * Component ID : HTTP
 * File Name    : HTTP4A_if.h
 * Description  :
 * History :
 * Version      Date            User        Comments
 * V1.0         2019-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef HTTP4A_IF_H_
#define HTTP4A_IF_H_
#include <ZOO.h>
#include "HTTP4A_type.h"
		
/**************************************************************************
INTERFACE <ZOO_INT32 HTTP4A_create>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
    OUT:        none
    INOUT:      ZOO_HANDLE * fd
<Timeout>:      60
<Server>:       none
<Returns>:      OK -- 成功
<Description>:  创建HTTP句柄，获得句柄
<PRECONDITION>: 
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 HTTP4A_create(INOUT ZOO_HANDLE * fd);

/**************************************************************************
INTERFACE <ZOO_INT32 HTTP4A_cleanup>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         OO_HANDLE fd
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       none
<Returns>:      OK -- 成功
<Description>:  清除Http句柄
<PRECONDITION>: 
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 HTTP4A_cleanup(IN ZOO_HANDLE fd);
								
/**************************************************************************
INTERFACE <ZOO_INT32 HTTP4A_on_request_sig>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         IN HTTP4A_REQUEST_STRUCT req
				ZOO_INT32 timeout
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       none
<Returns>:      OK -- 成功
<Description>:  HTTP请求
<PRECONDITION>: 需创建会话成功
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 HTTP4A_on_request_sig(IN HTTP4A_REQUEST_STRUCT * req);

/**************************************************************************
INTERFACE <ZOO_INT32 HTTP4A_on_download_file_sig>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         IN HTTP4A_REQUEST_STRUCT req
				ZOO_INT32 timeout
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       none
<Returns>:      OK -- 成功
<Description>:  下载文件接口
<PRECONDITION>: 需创建会话成功
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 HTTP4A_on_download_file_sig(IN ZOO_HANDLE fd,
															IN HTTP4A_DOWNLOAD_OPTION_STRUCT * dl_option);

/**************************************************************************
INTERFACE <ZOO_INT32 HTTP4A_on_ssl_request_sig>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         std::string url
    			ZOO_INT32 session_id
    OUT:        none
    INOUT:      std::string & reply_message
<Timeout>:      60
<Server>:       none
<Returns>:      OK -- 成功
<Description>:  请求HTTP连接
<PRECONDITION>: 需创建会话成功
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 HTTP4A_on_ssl_request_sig(IN HTTP4A_REQUEST_STRUCT * req,
																	IN HTTP4A_SSL_OPTION_STRUCT * ssl_option);
						
/**************************************************************************
INTERFACE <ZOO_INT32 HTTP4A_handle_request>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_INT32 id
                HTTP4A_RESPONSE_STRUCT * message
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       none
<Returns>:      OK -- 成功
                
<Description>:  回答服务端转发的客户端请求消息，和HTTP4A_server_request_subscribe配合使用。
				只有收到了，订阅信号之后，才能回答客户端消息。
<PRECONDITION>: 需创建会话成功
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 HTTP4A_handle_request(IN ZOO_INT32 id,
												 		IN HTTP4A_RESPONSE_STRUCT * message);

/**************************************************************************
INTERFACE <ZOO_INT32 HTTP4A_client_request_subscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         HTTP4A_CLIENT_REQ_CALLBACK_FUNCTION callback_function
    OUT:        ZOO_HANDLE *handle
    INOUT:      void *context
<Timeout>:      60
<Server>:       none
<Returns>:      OK -- 成功
                
<Description>:  http server收到外部客户端请求，并将请求转发给后端处理。
<PRECONDITION>: 需创建会话成功
}
**************************************************************************/
typedef void(*HTTP4A_SERVER_REQUEST_CALLBACK_FUNCTION)(IN HTTP4A_REQUEST_STRUCT * req,
													IN ZOO_INT32 error_code,
													IN void *context);

ZOO_EXPORT ZOO_INT32 HTTP4A_server_request_subscribe(IN HTTP4A_SERVER_REQUEST_CALLBACK_FUNCTION callback_function,
																OUT ZOO_HANDLE *handle,
																INOUT void *context);
ZOO_EXPORT ZOO_INT32 HTTP4A_server_request_unsubscribe(IN ZOO_HANDLE handle);

/**************************************************************************
INTERFACE <ZOO_INT32 HTTP4A_inprogress_subscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         HTTP4A_INPROGRESS_CALLBACK_FUNCTION callback_function
    OUT:        ZOO_HANDLE *handle
    INOUT:      void *context
<Timeout>:      60
<Server>:       none
<Returns>:      OK -- 成功
                
<Description>:  下载或者上传文件进度条回调函数
<PRECONDITION>: 
}
**************************************************************************/
typedef void(*HTTP4A_INPROGRESS_CALLBACK_FUNCTION)(IN HTTP4A_INPROGRESS_STRUCT * inprogress,
													IN ZOO_INT32 error_code,
													IN void *context);

ZOO_EXPORT ZOO_INT32 HTTP4A_inprogress_subscribe(IN HTTP4A_INPROGRESS_CALLBACK_FUNCTION callback_function,
																OUT ZOO_HANDLE *handle,
																INOUT void *context);
ZOO_EXPORT ZOO_INT32 HTTP4A_inprogress_unsubscribe(IN ZOO_HANDLE handle);

/**************************************************************************
INTERFACE <ZOO_INT32 HTTP4A_reply_subscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         HTTP4A_REPLY_CALLBACK_FUNCTION callback_function
    OUT:        ZOO_HANDLE *handle
    INOUT:      void *context
<Timeout>:      60
<Server>:       none
<Returns>:      OK -- 成功
                
<Description>:  收到本地发送到远程服务端的http请求应答消息，本地为客户端
<PRECONDITION>: 
}
**************************************************************************/
typedef void(*HTTP4A_SERVER_REPLY_CALLBACK_FUNCTION)(IN HTTP4A_RESPONSE_STRUCT * reply,
													IN ZOO_INT32 error_code,
													IN void *context);

ZOO_EXPORT ZOO_INT32 HTTP4A_server_reply_subscribe(IN HTTP4A_SERVER_REPLY_CALLBACK_FUNCTION callback_function,
																OUT ZOO_HANDLE *handle,
																INOUT void *context);
ZOO_EXPORT ZOO_INT32 HTTP4A_server_reply_unsubscribe(IN ZOO_HANDLE handle);

/**************************************************************************
INTERFACE <ZOO_INT32 HTTP4A_status_subscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         HTTP4A_REPLY_CALLBACK_FUNCTION callback_function
    OUT:        ZOO_HANDLE *handle
    INOUT:      void *context
<Timeout>:      60
<Server>:       none
<Returns>:      OK -- 成功
                
<Description>:  订阅服务器当前状态，包含：服务器启动状态、连接状态等
<PRECONDITION>: 
}
**************************************************************************/
typedef void(*HTTP4A_STATUS_CALLBACK_FUNCTION)(IN HTTP4A_STATUS_STRUCT status,
													IN ZOO_INT32 error_code,
													IN void *context);

ZOO_EXPORT ZOO_INT32 HTTP4A_status_subscribe(IN HTTP4A_STATUS_CALLBACK_FUNCTION callback_function,
																OUT ZOO_HANDLE *handle,
																INOUT void *context);
ZOO_EXPORT ZOO_INT32 HTTP4A_status_unsubscribe(IN ZOO_HANDLE handle);
#endif