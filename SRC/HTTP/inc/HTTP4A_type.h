/**************************************************************
 * Copyright (C) 2017, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : PS
 * Component ID : HTTP
 * File Name    : HTTP4A_if.h
 * Description  :
 * History :
 * Version      Date            User        Comments
 * V1.0         2019-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef HTTP4A_TYPE_H_
#define HTTP4A_TYPE_H_
#include "ZOO.h"
/*************************************************************
 @brief 定义HTTP模块错误码
*************************************************************/
#define HTTP4A_BASE_ERR               0x1000
#define HTTP4A_SYSTEM_ERR   	      (HTTP4A_BASE_ERR + 0x01)
#define HTTP4A_PARAMETER_ERR 	      (HTTP4A_BASE_ERR + 0x02)
#define HTTP4A_TIMEOUT_ERR  	      (HTTP4A_BASE_ERR + 0x03)
#define HTTP4A_ILLEGAL_CALL_ERR       (HTTP4A_BASE_ERR + 0x04)
#define HTTP4A_CONNECT_ERR            (HTTP4A_BASE_ERR + 0x05)
#define HTTP4A_DISCONNECT_ERR              (HTTP4A_BASE_ERR + 0x06)
#define HTTP4A_CLIENT_SEND_MESSAGE_ERR     (HTTP4A_BASE_ERR + 0x07)
#define HTTP4A_CLIENT_RECV_MESSAGE_ERR     (HTTP4A_BASE_ERR + 0x08)
#define HTTP4A_CLIENT_RESOLVE_ERR     (HTTP4A_BASE_ERR + 0x09) 
#define HTTP4A_SSL_ERR                (HTTP4A_BASE_ERR + 0x0A)
#define HTTP4A_HANDSHAKE_ERR          (HTTP4A_BASE_ERR + 0x0B)
#define HTTP4A_SERVER_RESPONE_ERR     (HTTP4A_BASE_ERR + 0x0C)

/*************************************************************
 @brief 定义宏名
*************************************************************/
#define HTTP4A_FEILD_NAME_LENGTH     16
#define HTTP4A_FEILD_DATA_LENGTH     256
#define HTTP4A_FILE_PATH_LENGTH      64
#define HTTP4A_FILE_NAME_LENGTH      64
#define HTTP4A_DOMAIN_LENGTH         64
#define HTTP4A_BODY_LENGTH           1024
#define HTTP4A_TARGET_LENGTH         512


/*************************************************************
 @brief 定义数据枚举或者数据结构
*************************************************************/
/**
 *@brief 定义HTTP请求方法枚举
 **/
typedef enum
{
	HTTP4A_METHOD_MIN     = -1,
	HTTP4A_METHOD_UNKNOWN = 0,
	HTTP4A_METHOD_DELETE  = 1,//请求服务器删除由 Request-URI 所标识的资源
	HTTP4A_METHOD_GET     = 2,//请求获取由 Request-URI 所标识的资源  请求参数在 请求行中
	HTTP4A_METHOD_HEAD    = 3,//请求获取由 Request-URI 所标识的资源的响应消息报头
	HTTP4A_METHOD_POST    = 4,//请求服务器接收在请求中封装的实体
	HTTP4A_METHOD_PUT     = 5,//请求服务器存储一个资源，并用 Request-URI 作为其标识符
	HTTP4A_METHOD_CONNECT = 6,//reserve
	HTTP4A_METHOD_OPTIONS = 7,//请求查询服务器的性能，或者查询与资源相关的选项和需求
	HTTP4A_METHOD_TRACE   = 8,//请求服务器回送到的请求信息，主要用于测试或诊断
	HTTP4A_METHOD_MAX
}HTTP4A_METHOD_ENUM;

/**
 *@brief 定义请求消息结构体
 **/
typedef struct
{
	ZOO_BOOL filled;//是否填充header
	ZOO_CHAR name[HTTP4A_FEILD_NAME_LENGTH];
	ZOO_CHAR content[HTTP4A_FEILD_DATA_LENGTH];
}HTTP4A_FIELD_OPTION_STRUCT;
 
/**
 *@brief 定义SSL数组结构体
 **/
typedef struct
{
	ZOO_CHAR cert_path[HTTP4A_FILE_PATH_LENGTH];
	ZOO_CHAR public_key[HTTP4A_FILE_NAME_LENGTH];
	ZOO_CHAR private_key[HTTP4A_FILE_NAME_LENGTH];
	ZOO_CHAR trust_chain[HTTP4A_FILE_NAME_LENGTH];
    ZOO_INT32 verify_mode;//
    ZOO_INT32 tls_method;
}HTTP4A_SSL_OPTION_STRUCT;

/**
 *@brief 定义文件信息结构体
 **/
typedef struct
{
	ZOO_CHAR name[HTTP4A_FILE_NAME_LENGTH];
	ZOO_CHAR path[HTTP4A_FILE_PATH_LENGTH];
}HTTP4A_FILE_OPTION_STRUCT;

/**
 *@brief 定义进度消息结构体
 **/
typedef struct
{
	ZOO_HANDLE fd;//request id
	ZOO_INT32 speed;//kb/s
	ZOO_INT32 total_time;//milliseconds
	ZOO_INT32 filler;
	ZOO_CHAR  file_name[HTTP4A_FILE_NAME_LENGTH];
}HTTP4A_INPROGRESS_STRUCT;

/**
 *@brief 定义请求消息结构体
 **/
typedef struct
{
	ZOO_HANDLE fd;//request id
	ZOO_INT32 method;//http 请求方法
	ZOO_INT32 timeout;//seconds
	ZOO_BOOL keep_alive;
	ZOO_CHAR target[HTTP4A_FEILD_DATA_LENGTH];
	ZOO_CHAR content_type[HTTP4A_FEILD_DATA_LENGTH];
	ZOO_CHAR body[HTTP4A_BODY_LENGTH];
}HTTP4A_REQUEST_STRUCT;

/**
 *@brief 定义download结构体，默认GET
 **/
typedef struct
{
	ZOO_INT32 timeout;//seconds
	ZOO_CHAR target[HTTP4A_FEILD_DATA_LENGTH];//http请求URL
	ZOO_CHAR auth[HTTP4A_FEILD_DATA_LENGTH];//header option
	ZOO_CHAR accept[HTTP4A_FEILD_DATA_LENGTH];//header option
	ZOO_CHAR content_type[HTTP4A_FEILD_DATA_LENGTH];//header option
	ZOO_CHAR file_name[HTTP4A_FEILD_DATA_LENGTH];//保存的文件名
	ZOO_CHAR storage_dir[HTTP4A_FEILD_DATA_LENGTH];//下载路径
}HTTP4A_DOWNLOAD_OPTION_STRUCT;

/**
 *@brief 定义回复消息结构体
 **/
typedef struct
{
	ZOO_HANDLE fd;//request id
	ZOO_INT32 error_code;
    ZOO_INT32 total_body_parts; // total parts , the body split multipart 
    ZOO_INT32 cur_part_index;   // the index of body parts
    ZOO_INT32 cur_part_length;  // the current part length
    ZOO_BOOL is_last_part;      // whether is the last part
	ZOO_CHAR body[HTTP4A_BODY_LENGTH];
}HTTP4A_RESPONSE_STRUCT;

/**
 *@brief 定义回复消息结构体
 **/
typedef struct
{
	ZOO_BOOL  actived;//服务端是否激活
	ZOO_BOOL  ssl_enabled;//当前最大连接数
	ZOO_INT32 current_peers;//当前连接数
	ZOO_INT32 max_connections;//当前最大连接数
}HTTP4A_STATUS_STRUCT;

#endif
