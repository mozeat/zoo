/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : HTTP4I_type.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-05    Generator      created
*************************************************************/
#ifndef HTTP4I_TYPE_H
#define HTTP4I_TYPE_H
#include <MQ4A_type.h>
#include "HTTP4A_type.h"
#include "HTTP4A_if.h"


/**
 *@brief Macro Definitions
**/
#define HTTP4I_COMPONET_ID "HTTP"
#define HTTP4A_SERVER     "HTTP4A_SERVER"
#define HTTP4I_BUFFER_LENGTH    256
#define HTTP4I_RETRY_INTERVAL   3

/**
 *@brief Function Code Definitions
**/
#define HTTP4A_CREATE_CODE 0x4854ff00
#define HTTP4A_CLEANUP_CODE 0x4854ff01
#define HTTP4A_ON_REQUEST_SIG_CODE 0x4854ff02
#define HTTP4A_ON_DOWNLOAD_FILE_SIG_CODE 0x4854ff03
#define HTTP4A_ON_SSL_REQUEST_SIG_CODE 0x4854ff04
#define HTTP4A_HANDLE_REQUEST_CODE 0x4854ff05
#define HTTP4A_SERVER_REQUEST_SUBSCRIBE_CODE 0x4854ff06
#define HTTP4A_INPROGRESS_SUBSCRIBE_CODE 0x4854ff08
#define HTTP4A_SERVER_REPLY_SUBSCRIBE_CODE 0x4854ff0a
#define HTTP4A_STATUS_SUBSCRIBE_CODE 0x4854ff0c

/*Request and reply header struct*/
typedef struct
{
    MQ4A_SERV_ADDR reply_addr;
    ZOO_UINT32 msg_id;
    ZOO_BOOL reply_wanted;
    ZOO_UINT32 func_id;
}HTTP4I_REPLY_HANDLER_STRUCT;

typedef  HTTP4I_REPLY_HANDLER_STRUCT * HTTP4I_REPLY_HANDLE;

/*Request message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL need_reply;
}HTTP4I_REQUEST_HEADER_STRUCT;

/*Reply message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL execute_result;
}HTTP4I_REPLY_HEADER_STRUCT;

/**
*@brief HTTP4I_CREATE_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}HTTP4I_CREATE_CODE_REQ_STRUCT;

/**
*@brief HTTP4I_CLEANUP_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE fd;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}HTTP4I_CLEANUP_CODE_REQ_STRUCT;

/**
*@brief HTTP4I_ON_REQUEST_SIG_CODE_REQ_STRUCT
**/
typedef struct 
{
    HTTP4A_REQUEST_STRUCT req;
    ZOO_CHAR g_filler[8];
}HTTP4I_ON_REQUEST_SIG_CODE_REQ_STRUCT;

/**
*@brief HTTP4I_ON_DOWNLOAD_FILE_SIG_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE fd;
    ZOO_CHAR c_filler[4];
    HTTP4A_DOWNLOAD_OPTION_STRUCT dl_option;
    ZOO_CHAR g_filler[8];
}HTTP4I_ON_DOWNLOAD_FILE_SIG_CODE_REQ_STRUCT;

/**
*@brief HTTP4I_ON_SSL_REQUEST_SIG_CODE_REQ_STRUCT
**/
typedef struct 
{
    HTTP4A_REQUEST_STRUCT req;
    HTTP4A_SSL_OPTION_STRUCT ssl_option;
    ZOO_CHAR g_filler[8];
}HTTP4I_ON_SSL_REQUEST_SIG_CODE_REQ_STRUCT;

/**
*@brief HTTP4I_HANDLE_REQUEST_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_INT32 id;
    ZOO_CHAR c_filler[4];
    HTTP4A_RESPONSE_STRUCT message;
    ZOO_CHAR g_filler[8];
}HTTP4I_HANDLE_REQUEST_CODE_REQ_STRUCT;

/**
*@brief HTTP4I_CREATE_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE fd;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}HTTP4I_CREATE_CODE_REP_STRUCT;

/**
*@brief HTTP4I_CLEANUP_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}HTTP4I_CLEANUP_CODE_REP_STRUCT;

/**
*@brief HTTP4I_ON_REQUEST_SIG_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}HTTP4I_ON_REQUEST_SIG_CODE_REP_STRUCT;

/**
*@brief HTTP4I_ON_DOWNLOAD_FILE_SIG_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}HTTP4I_ON_DOWNLOAD_FILE_SIG_CODE_REP_STRUCT;

/**
*@brief HTTP4I_ON_SSL_REQUEST_SIG_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}HTTP4I_ON_SSL_REQUEST_SIG_CODE_REP_STRUCT;

/**
*@brief HTTP4I_HANDLE_REQUEST_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}HTTP4I_HANDLE_REQUEST_CODE_REP_STRUCT;

/**
*@brief HTTP4I_SERVER_REQUEST_SUBSCRIBE_CODE_REP_STRUCT
**/
typedef struct 
{
    HTTP4A_REQUEST_STRUCT req;
    ZOO_CHAR g_filler[8];
}HTTP4I_SERVER_REQUEST_SUBSCRIBE_CODE_REP_STRUCT;

/**
*@brief HTTP4I_INPROGRESS_SUBSCRIBE_CODE_REP_STRUCT
**/
typedef struct 
{
    HTTP4A_INPROGRESS_STRUCT inprogress;
    ZOO_CHAR g_filler[8];
}HTTP4I_INPROGRESS_SUBSCRIBE_CODE_REP_STRUCT;

/**
*@brief HTTP4I_SERVER_REPLY_SUBSCRIBE_CODE_REP_STRUCT
**/
typedef struct 
{
    HTTP4A_RESPONSE_STRUCT reply;
    ZOO_CHAR g_filler[8];
}HTTP4I_SERVER_REPLY_SUBSCRIBE_CODE_REP_STRUCT;

/**
*@brief HTTP4I_STATUS_SUBSCRIBE_CODE_REP_STRUCT
**/
typedef struct 
{
    HTTP4A_STATUS_STRUCT status;
    ZOO_CHAR g_filler[8];
}HTTP4I_STATUS_SUBSCRIBE_CODE_REP_STRUCT;

typedef struct
{
    HTTP4I_REQUEST_HEADER_STRUCT request_header;
    union
    {
        HTTP4I_CREATE_CODE_REQ_STRUCT create_req_msg;
        HTTP4I_CLEANUP_CODE_REQ_STRUCT cleanup_req_msg;
        HTTP4I_ON_REQUEST_SIG_CODE_REQ_STRUCT on_request_sig_msg;
        HTTP4I_ON_DOWNLOAD_FILE_SIG_CODE_REQ_STRUCT on_download_file_sig_req_msg;
        HTTP4I_ON_SSL_REQUEST_SIG_CODE_REQ_STRUCT on_ssl_request_sig_msg;
        HTTP4I_HANDLE_REQUEST_CODE_REQ_STRUCT handle_request_msg;
     }request_body;
}HTTP4I_REQUEST_STRUCT;


typedef struct
{
    HTTP4I_REPLY_HEADER_STRUCT reply_header;
    union
    {
        HTTP4I_CREATE_CODE_REP_STRUCT create_rep_msg;
        HTTP4I_CLEANUP_CODE_REP_STRUCT cleanup_rep_msg;
        HTTP4I_ON_REQUEST_SIG_CODE_REP_STRUCT on_request_sig_msg;
        HTTP4I_ON_DOWNLOAD_FILE_SIG_CODE_REP_STRUCT on_download_file_sig_rep_msg;
        HTTP4I_ON_SSL_REQUEST_SIG_CODE_REP_STRUCT on_ssl_request_sig_msg;
        HTTP4I_HANDLE_REQUEST_CODE_REP_STRUCT handle_request_msg;
        HTTP4I_SERVER_REQUEST_SUBSCRIBE_CODE_REP_STRUCT server_request_subscribe_code_rep_msg;
        HTTP4I_INPROGRESS_SUBSCRIBE_CODE_REP_STRUCT inprogress_subscribe_code_rep_msg;
        HTTP4I_SERVER_REPLY_SUBSCRIBE_CODE_REP_STRUCT server_reply_subscribe_code_rep_msg;
        HTTP4I_STATUS_SUBSCRIBE_CODE_REP_STRUCT status_subscribe_code_rep_msg;
    }reply_body;
}HTTP4I_REPLY_STRUCT;


/**
*@brief HTTP4I_SERVER_REQUEST_SUBSCRIBE_CODE_CALLBACK_STRUCT
**/
typedef struct 
{
    HTTP4A_SERVER_REQUEST_CALLBACK_FUNCTION *callback_function;
    void * parameter;
}HTTP4I_SERVER_REQUEST_SUBSCRIBE_CODE_CALLBACK_STRUCT;

/**
*@brief HTTP4I_INPROGRESS_SUBSCRIBE_CODE_CALLBACK_STRUCT
**/
typedef struct 
{
    HTTP4A_INPROGRESS_CALLBACK_FUNCTION *callback_function;
    void * parameter;
}HTTP4I_INPROGRESS_SUBSCRIBE_CODE_CALLBACK_STRUCT;

/**
*@brief HTTP4I_SERVER_REPLY_SUBSCRIBE_CODE_CALLBACK_STRUCT
**/
typedef struct 
{
    HTTP4A_SERVER_REPLY_CALLBACK_FUNCTION *callback_function;
    void * parameter;
}HTTP4I_SERVER_REPLY_SUBSCRIBE_CODE_CALLBACK_STRUCT;

/**
*@brief HTTP4I_STATUS_SUBSCRIBE_CODE_CALLBACK_STRUCT
**/
typedef struct 
{
    HTTP4A_STATUS_CALLBACK_FUNCTION *callback_function;
    void * parameter;
}HTTP4I_STATUS_SUBSCRIBE_CODE_CALLBACK_STRUCT;

#endif //HTTP4I_type.h
