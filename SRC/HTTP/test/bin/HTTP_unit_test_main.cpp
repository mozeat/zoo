/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-06-17    Generator      created
*************************************************************/
#define BOOST_TEST_MODULE HTTP_test_module 
#define BOOST_TEST_DYN_LINK 
#include <boost/test/unit_test.hpp>
#include <boost/test/unit_test_log.hpp>
#include <boost/test/unit_test_suite.hpp>
#include <boost/test/framework.hpp>
#include <boost/test/unit_test_parameters.hpp>
#include <boost/test/utils/nullstream.hpp>

/**
 * @brief Execute <HTTP> UTMF ...
 * @brief <Global Fixture> header file can define the global variable.
 * @brief modify the include header files sequence to change the unit test execution sequence but not case SHUTDOWN.
 */
//#include <TC_HTTP_STATUS_SUBSCRIBE_0012.h>
#include <TC_HTTP_GLOBAL_FIXTRUE.h>
//#include <TC_HTTP_ON_REQUEST_SIG_003.h>
//#include <TC_HTTP_ON_DOWNLOAD_FILE_SIG_0014.h>
#include <TC_HTTP_HANDLE_REQUEST_005.h>
//#include <TC_HTTP_ON_SSL_REQUEST_SIG_004.h>