/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-06-17    Generator      created
*************************************************************/
extern "C"
{
    #include <HTTP4A_if.h>
    #include <HTTP4A_type.h>
    #include <MM4A_if.h>
}
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/output_test_stream.hpp>
#include <boost/test/results_reporter.hpp>
#include <boost/test/unit_test_log.hpp>
#include <string>
#include <condition_variable>
#include <mutex>
#include <thread>

ZOO_HANDLE g_req_fd;
ZOO_BOOL   g_reply_flag = ZOO_FALSE;
std::mutex g_sync_lock;
std::condition_variable g_cv;

std::mutex g_sync_slock;
std::condition_variable g_scv;
ZOO_BOOL   g_reply_sflag = ZOO_FALSE;


static void server_request_callback(IN HTTP4A_REQUEST_STRUCT *request,
												IN ZOO_INT32 error_code,
												IN void *context)
{
	BOOST_TEST_MESSAGE("id:" << request->fd);
	BOOST_TEST_MESSAGE("request:" << request->target);
	BOOST_TEST_MESSAGE("error_code:" << error_code);
	g_req_fd = request->fd;
	std::unique_lock<std::mutex> lk(g_sync_slock);
	g_reply_sflag = ZOO_TRUE;
	g_scv.notify_one();
}

static void inprogress_callback(IN HTTP4A_INPROGRESS_STRUCT *inprogress,
												IN ZOO_INT32 error_code,
												IN void *context)
{
	BOOST_TEST_MESSAGE("total_time:" << inprogress->total_time);
	BOOST_TEST_MESSAGE("file_name:" << inprogress->file_name);
	BOOST_TEST_MESSAGE("error_code:" << error_code);
	BOOST_TEST(error_code == OK);
	std::unique_lock<std::mutex> lk(g_sync_lock);
	g_reply_flag = ZOO_TRUE;
	g_cv.notify_one();
}

static void server_reply_callback(IN HTTP4A_RESPONSE_STRUCT *reply,
												IN ZOO_INT32 error_code,
												IN void *context)
{
	BOOST_TEST_MESSAGE("body:" << reply->body);
	BOOST_TEST_MESSAGE("error_code:" << error_code);
	std::unique_lock<std::mutex> lk(g_sync_lock);
	g_reply_flag = ZOO_TRUE;
	g_cv.notify_one();
}

/**
* @brief Define a test suite entry/exit,so that the setup function is called only once
* upon entering the test suite.
*/
struct TC_HTTP_GOLBAL_FIXTURE
{
    TC_HTTP_GOLBAL_FIXTURE()
    {
        BOOST_TEST_MESSAGE("TC global fixture initialize ...");
        MM4A_initialize();
        
    	/*
    	 *@brief 注册服务应答回调函数
    	 */
    	BOOST_ASSERT(HTTP4A_inprogress_subscribe(inprogress_callback,&g_inprogress_fd,NULL) == OK);
        BOOST_ASSERT(HTTP4A_server_request_subscribe(server_request_callback,&g_server_request_fd,NULL) == OK); 
    	BOOST_ASSERT(HTTP4A_server_reply_subscribe(server_reply_callback,&g_server_reply_fd,NULL) == OK);
    }

    ~TC_HTTP_GOLBAL_FIXTURE()
    {
        BOOST_TEST_MESSAGE("TC teardown ...");
         /*
    	 *@brief step4: 释放资源
    	 */
    	BOOST_ASSERT(HTTP4A_server_request_unsubscribe(g_server_request_fd) == OK);
    	BOOST_ASSERT(HTTP4A_inprogress_unsubscribe(g_inprogress_fd) == OK); 
    	BOOST_ASSERT(HTTP4A_server_reply_unsubscribe(g_server_reply_fd) == OK);
        MM4A_terminate();
    }
	ZOO_HANDLE g_server_reply_fd;
    ZOO_HANDLE g_server_request_fd;
    ZOO_HANDLE g_inprogress_fd;
};

BOOST_TEST_GLOBAL_FIXTURE(TC_HTTP_GOLBAL_FIXTURE);


/**
 * @brief Define test report output formate, default --log_level=message.
*/
struct TC_HTTP_REPORTER
{
    TC_HTTP_REPORTER():reporter("../../../reporter/HTTP_TC_result.reporter")
    {
        boost::unit_test::unit_test_log.set_stream( reporter );
        boost::unit_test::unit_test_log.set_threshold_level(boost::unit_test::log_test_units);
    }

    ~TC_HTTP_REPORTER()
    {
        boost::unit_test::unit_test_log.set_stream( std::cout );;
    }
   std::ofstream reporter;
};


BOOST_TEST_GLOBAL_CONFIGURATION(TC_HTTP_REPORTER);
