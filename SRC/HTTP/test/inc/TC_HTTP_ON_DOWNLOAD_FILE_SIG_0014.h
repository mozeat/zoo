/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-05    Generator      created
*************************************************************/
extern "C"
{
    #include <HTTP4A_if.h>
    #include <HTTP4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_HTTP_ON_DOWNLOAD_FILE_SIG_004)

	/**
	 *@brief 
	 *@param fd
	 *@param dl_option
	**/
    BOOST_AUTO_TEST_CASE( TC_HTTP_ON_DOWNLOAD_FILE_SIG_004_001 )
    {	
    	g_reply_flag = ZOO_FALSE;
    	ZOO_HANDLE fd;
    	
    	/*
    	 *@brief step1: 创建session
    	 */
    	BOOST_ASSERT(HTTP4A_create(&fd) == OK);

		/*
    	 *@brief step2: 消息填充
    	 */
    	HTTP4A_DOWNLOAD_OPTION_STRUCT dl_option;
        memset(&dl_option,0x0,sizeof(HTTP4A_DOWNLOAD_OPTION_STRUCT));
        sprintf(dl_option.file_name,"%s","SSL_TEST.deb");
        sprintf(dl_option.storage_dir,"%s", "/home/nio/Downloads");
        sprintf(dl_option.target,"%s", "http://minio.nioint.com:9000/release/Software_Platform/ZOO/T_ZOO_1.0.0_20190411.tar.gz");
        sprintf(dl_option.content_type,"%s", "application/gzip");
        //sprintf(dl_option.accept,"%s", "*/*");
        //sprintf(dl_option.auth,"%s", "b4f06bc1f3fe0a8ec7cfaf8fe5028e8e");
        dl_option.timeout = 30;
        BOOST_TEST(HTTP4A_on_download_file_sig(fd,&dl_option) == OK);

    	/*
    	 *@brief step3: 等待回调返回，超时30秒
    	 */
    	std::unique_lock<std::mutex> lk(g_sync_lock);
    	BOOST_TEST(g_cv.wait_for(lk, std::chrono::seconds(30), []{return g_reply_flag;}) == true);
		lk.unlock();
		
    	/*
    	 *@brief step4: 释放资源
    	 */
		BOOST_TEST(HTTP4A_cleanup(fd) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()