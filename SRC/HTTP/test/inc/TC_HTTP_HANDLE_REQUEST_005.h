/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-06-17    Generator      created
*************************************************************/
extern "C"
{
    #include <HTTP4A_if.h>
    #include <HTTP4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_HTTP_HANDLE_REQUEST_005)


	
	/**
	 *@brief 
	 *@param id
	 *@param message
	**/
    BOOST_AUTO_TEST_CASE( TC_HTTP_HANDLE_REQUEST_005_001 )
    {	
		g_reply_flag = ZOO_FALSE;
		BOOST_TEST_MESSAGE("waiting ...");
		
        /*
    	 *@brief step2: 等待回调返回，超时120秒
    	 */
    	std::unique_lock<std::mutex> lk(g_sync_slock);
    	BOOST_ASSERT(g_scv.wait_for(lk, std::chrono::seconds(120), []{return g_reply_sflag;}) == true);
		lk.unlock();

		/*
    	 *@brief step3: 等待回调返回，超时30秒
    	 */
		HTTP4A_RESPONSE_STRUCT message;
        memset(&message,0x0,sizeof(HTTP4A_RESPONSE_STRUCT));
        message.fd = g_req_fd;
        message.error_code = OK;
        sprintf(message.body,"%s","{\"is\":\"ture\"}");
        BOOST_TEST(HTTP4A_handle_request(g_req_fd,&message) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()