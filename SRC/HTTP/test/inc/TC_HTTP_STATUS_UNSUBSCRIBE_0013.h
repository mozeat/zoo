/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-06-17    Generator      created
*************************************************************/
extern "C"
{
    #include <HTTP4A_if.h>
    #include <HTTP4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_HTTP_STATUS_UNSUBSCRIBE_0013)

/**
 *@brief 
 *@param handle
**/
    BOOST_AUTO_TEST_CASE( TC_HTTP_STATUS_UNSUBSCRIBE_0013_001 )
    {
        BOOST_TEST(HTTP4A_status_unsubscribe(g_status_fd) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()