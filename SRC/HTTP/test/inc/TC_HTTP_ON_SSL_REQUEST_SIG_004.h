/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-06-17    Generator      created
*************************************************************/
extern "C"
{
    #include <HTTP4A_if.h>
    #include <HTTP4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_HTTP_ON_SSL_REQUEST_SIG_004)

/**
 *@brief 
 *@param req
 *@param ssl_option
**/
    BOOST_AUTO_TEST_CASE( TC_HTTP_ON_SSL_REQUEST_SIG_004_001 )
    {
        g_reply_flag = ZOO_FALSE;
        HTTP4A_REQUEST_STRUCT req;
        memset(&req,0x0,sizeof(HTTP4A_REQUEST_STRUCT));

        BOOST_ASSERT(HTTP4A_create(&req.fd) == OK);
        sprintf(req.target,"%s","");
        req.method = HTTP4A_METHOD_GET;
        req.timeout = 30;
        sprintf(req.content_type,"%s","application/json");
        
        HTTP4A_SSL_OPTION_STRUCT ssl_option;
        memset(&ssl_option,0x0,sizeof(HTTP4A_SSL_OPTION_STRUCT));
        sprintf(ssl_option.cert_path,"%s","/usr/local/");
        sprintf(ssl_option.public_key,"%s","server_cert.pem");
        sprintf(ssl_option.private_key,"%s","private_key.pem");
        sprintf(ssl_option.trust_chain,"%s","trust_cert_chain_server.pem");
        BOOST_TEST(HTTP4A_on_ssl_request_sig(&req,&ssl_option) == OK);
        std::unique_lock<std::mutex> lk(g_sync_lock);
        BOOST_TEST(g_cv.wait_for(lk, std::chrono::seconds(30), []{return g_reply_flag;}) == true);
		lk.unlock();
        g_reply_flag = false;
		BOOST_TEST(HTTP4A_cleanup(req.fd) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()