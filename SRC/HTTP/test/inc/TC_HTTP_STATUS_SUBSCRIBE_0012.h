/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-06-17    Generator      created
*************************************************************/
extern "C"
{
    #include <HTTP4A_if.h>
    #include <HTTP4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_HTTP_STATUS_SUBSCRIBE_0012)

	
	ZOO_HANDLE g_status_fd;
	
	void status_update_callback(IN HTTP4A_STATUS_STRUCT status,
													IN ZOO_INT32 error_code,
													IN void *context)
	{
		BOOST_TEST_MESSAGE("status:" << status.actived);
	}
	
	/**
	 *@brief 
	 *@param callback_function
	 *@param handle
	 *@param context
	**/
    BOOST_AUTO_TEST_CASE( TC_HTTP_STATUS_SUBSCRIBE_0012_001 )
    {
        void* context = NULL;
        BOOST_TEST(HTTP4A_status_subscribe(status_update_callback,&g_status_fd,context) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()