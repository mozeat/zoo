/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-06-17    Generator      created
*************************************************************/
extern "C"
{
    #include <HTTP4A_if.h>
    #include <HTTP4A_type.h>
    #include <MM4A_if.h>
}
#include "MOCK_DATA_PARSER_CLASS.h"


BOOST_AUTO_TEST_SUITE(TC_HTTP_ON_REQUEST_SIG_003)


    /**
	 *@brief http 请求并等待服务端返回，设置超时时间为30秒
	 *@param req
	**/
    BOOST_AUTO_TEST_CASE( TC_HTTP_ON_REQUEST_SIG_003_001 )
    {
        //std::string case_name = "TC_HTTP_ON_REQUEST_SIG_003_001";
            
        //step1 : create ini parser
        HTTP::MOCK_DATA_PARSER_CLASS mock_parser;

        //step2 : use config case1 section data 
        mock_parser.add_section_name("case1");

        //step3 : use the default mock file: config/mock_data/HTTP_mock_data.ini  
        std::string mock_file = mock_parser.get_mock_file();        

        //step4 : Parse all section datas and mapping section with  MOCK_DATA_CLASS
        std::map<std::string,boost::shared_ptr<HTTP::MOCK_DATA_CLASS> >  datas = 
                            mock_parser.parse_mock_data(mock_file);

        //step5 : Get case1 section data                    
        boost::shared_ptr<HTTP::MOCK_DATA_CLASS> section = datas["case1"];
        BOOST_ASSERT(section != nullptr);

        //step6 : Get data by name  
        std::string url          = section->get_data("url"); 
        std::string mothed       = section->get_data("method"); 
        std::string content_type = section->get_data("content_type"); 
        std::string body         = section->get_data("body"); 

        //step7 : use the datas
    	g_reply_flag = ZOO_FALSE;
    	ZOO_HANDLE fd = 0;
    	BOOST_ASSERT(HTTP4A_create(&fd) == OK);
    	HTTP4A_REQUEST_STRUCT req;
        memset(&req,0x0,sizeof(HTTP4A_REQUEST_STRUCT));
        req.fd = fd;
        req.timeout = 30;
        req.keep_alive = ZOO_FALSE;
        if(mothed == "get")
            req.method = HTTP4A_METHOD_GET;
        if(mothed == "post")
            req.method = HTTP4A_METHOD_POST;
        sprintf(req.target,"%s",url.data());
        sprintf(req.content_type,"%s",content_type.data());
        BOOST_TEST(HTTP4A_on_request_sig(&req) == OK);   
    	std::unique_lock<std::mutex> lk(g_sync_lock);
    	BOOST_TEST(g_cv.wait_for(lk, std::chrono::seconds(30), []{return g_reply_flag;}) == true);
		lk.unlock();
        g_reply_flag = false;
		BOOST_TEST(HTTP4A_cleanup(fd) == OK);
    }

    	
	/**
	 *@brief http 请求并等待服务端返回，设置超时时间为30秒
	 *@param req
	**/
    BOOST_AUTO_TEST_CASE( TC_HTTP_ON_REQUEST_SIG_003_002 )
    {
        //step1 : create ini parser
        HTTP::MOCK_DATA_PARSER_CLASS mock_parser;

        //step2 : use config case2 section data 
        mock_parser.add_section_name("case2");

        //step3 : use the default mock file: config/mock_data/HTTP_mock_data.ini  
        std::string mock_file = mock_parser.get_mock_file();        

        //step4 : Parse all section datas to MOCK_DATA_CLASS
        std::map<std::string,boost::shared_ptr<HTTP::MOCK_DATA_CLASS> >  datas = 
                            mock_parser.parse_mock_data(mock_file);

        //step5 : Get case2 section data                    
        boost::shared_ptr<HTTP::MOCK_DATA_CLASS> section = datas["case2"];
        BOOST_ASSERT(section != nullptr);

        //step6 : Get data by name  
        std::string url = section->get_data("url"); 
        std::string mothed = section->get_data("method"); 
        std::string content_type = section->get_data("content_type"); 
        std::string body = section->get_data("body"); 
        
    	g_reply_flag = ZOO_FALSE;
    	ZOO_HANDLE fd = 0;
       
    	/*
    	 *@创建session
    	 */
    	BOOST_ASSERT(HTTP4A_create(&fd) == OK);

		/*
    	 *@消息填充
    	 */
    	HTTP4A_REQUEST_STRUCT req;
        memset(&req,0x0,sizeof(HTTP4A_REQUEST_STRUCT));
        req.fd = fd;
        req.timeout = 30;
        req.keep_alive = ZOO_FALSE;
        if(mothed == "get")
            req.method = HTTP4A_METHOD_GET;
        if(mothed == "post")
            req.method = HTTP4A_METHOD_POST;
        sprintf(req.target,"%s",url.data());
        sprintf(req.content_type,"%s",content_type.data());
        sprintf(req.body,"%s",body.data());
    	BOOST_TEST(HTTP4A_on_request_sig(&req) == OK);
    
    	/*
    	 *@ 等待回调返回，超时30秒
    	 */
    	std::unique_lock<std::mutex> lk(g_sync_lock);
    	BOOST_TEST(g_cv.wait_for(lk, std::chrono::seconds(30), []{return g_reply_flag;}) == true);
		lk.unlock();
        g_reply_flag = false;
    	/*
    	 *@ 释放资源
    	 */
		BOOST_TEST(HTTP4A_cleanup(fd) == OK);
    }
BOOST_AUTO_TEST_SUITE_END()