/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : SESSION_DETECTER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef SESSION_DETECTER_CLASS_H
#define SESSION_DETECTER_CLASS_H
#include "PLAIN_HTTP_SESSION_CLASS.h"
#include "SSL_HTTP_SESSION_CLASS.h"
#include <vector>
#include <memory>
#include <boost/beast.hpp>
namespace HTTP
{
    class SESSION_DETECTER_CLASS : public std::enable_shared_from_this<SESSION_DETECTER_CLASS>
    {
    public:
        explicit SESSION_DETECTER_CLASS(
            tcp::socket&& socket,
            ssl::context& ctx,
            std::shared_ptr<std::string const> const& doc_root)
            : m_stream(std::move(socket))
            , m_ctx(ctx)
            , m_doc_root(doc_root)
        {
             
        }

        // Launch the detector
        void run()
        {
            // Set the timeout.
            this->m_stream.expires_after(std::chrono::seconds(30));
            beast::async_detect_ssl(
                this->m_stream,
                this->m_buffer,
                beast::bind_front_handler(
                    &SESSION_DETECTER_CLASS::on_detect,
                    this->shared_from_this()));
        }

        void on_detect(beast::error_code ec, boost::tribool result)
        {
            if(ec)
                __THROW_HTTP_EXCEPTION(HTTP4A_SYSTEM_ERR,"detect session error",NULL);

            if(result)
            {
                // Launch SSL session
                std::make_shared<SSL_HTTP_SESSION_CLASS>(
                    std::move(this->m_stream),
                    this->m_ctx,
                    std::move(this->m_buffer),
                    this->m_doc_root)->run();
            }
            else
            {
                // Launch plain session
                std::make_shared<PLAIN_HTTP_SESSION_CLASS>(
                    std::move(this->m_stream),
                    std::move(this->m_buffer),
                    this->m_doc_root)->run();
            }
        }
    private:        
        beast::tcp_stream m_stream;
        ssl::context& m_ctx;
        std::shared_ptr<std::string const> m_doc_root;
        beast::flat_buffer m_buffer;
    };
}
