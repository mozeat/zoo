/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : DEVICE_CONTROLLER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef CONTROLLER_INTERFACE_H
#define CONTROLLER_INTERFACE_H

extern "C" 
{
    #include <ZOO.h>
    #include <ZOO_if.h>
    #include <HTTP4A_type.h>
}
#include <boost/shared_ptr.hpp>
#include "MARKING_MODEL_INTERFACE.hpp"
#include "EVENT_PUBLISHER_CLASS.h"

namespace HTTP
{
    class CONTROLLER_INTERFACE: public virtual PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE> 
                                             ,public virtual NOTIFY_PROPERTY_CHANGED_INTERFACE<CONTROLLER_INTERFACE>
    {
    public:
       /*
        * @brief Constructor
       **/ 
       CONTROLLER_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~CONTROLLER_INTERFACE(){}
    public:
       /*
        * @brief Get the event_publisher attribute value
       **/ 
       virtual boost::shared_ptr<EVENT_PUBLISHER_CLASS> get_event_publisher() = 0;

       /*
        * @brief Set the event_publisher attribute value
        * @param event_publisher  The new event_publisher attribute value 
       **/ 
       virtual void set_event_publisher(boost::shared_ptr<EVENT_PUBLISHER_CLASS> event_publisher) = 0;

    };
}// namespace HTTP

#endif
