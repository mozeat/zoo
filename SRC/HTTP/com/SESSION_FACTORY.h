/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : SESSION_FACTORY_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef SESSION_FACTORY_H
#define SESSION_FACTORY_H
#include "SSL_WEBSOCKET_SESSION_CLASS.h"
#include "PLAIN_WEBSOCKET_SESSION_CLASS.h"
namespace HTTP
{

	template<class Body, class Allocator>
    void make_websocket_session(beast::tcp_stream stream,
                                                        http::request<Body, http::basic_fields<Allocator>> req)
    {
        std::make_shared<PLAIN_WEBSOCKET_SESSION_CLASS>(
            std::move(stream))->run(std::move(req));
    }

    template<class Body, class Allocator>
    void make_websocket_session(beast::ssl_stream<beast::tcp_stream> stream,
                                                        http::request<Body, http::basic_fields<Allocator>> req)
    {
        std::make_shared<SSL_WEBSOCKET_SESSION_CLASS>(
            std::move(stream))->run(std::move(req));
    }
    
} //namespace HTTP
#endif
