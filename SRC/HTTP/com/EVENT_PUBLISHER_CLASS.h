/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : EVENT_PUBLISHER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef EVENT_PUBLISHER_CLASS_H
#define EVENT_PUBLISHER_CLASS_H

extern "C" 
{
    #include <ZOO.h>
    #include <HTTP4A_type.h>
    #include <HTTPMA_event.h>
}
#include "HTTP_COMMON_MACRO_DEFINE.h"
#include <boost/shared_ptr.hpp>
#include "HTTPMA_event.h"
namespace HTTP
{
    class EVENT_PUBLISHER_CLASS 
    {
    public:
       /*
        * @brief Constructor
       **/ 
       EVENT_PUBLISHER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~EVENT_PUBLISHER_CLASS();
    public:
		/*
		 * @brief Publish server req to subscriber
		**/ 
		void publish_HTTP4A_server_request(IN HTTP4A_REQUEST_STRUCT *req,
																IN ZOO_INT32 error_code);

		/*
		 * @brief Publish download/upload inprogress to subscriber
		**/ 
		void publish_HTTP4A_inprogress(IN HTTP4A_INPROGRESS_STRUCT *inprogress,
																IN ZOO_INT32 error_code);
		
		/*
		 * @brief Publish reply messgae from remote server
		**/ 
		void publish_HTTP4A_reply(IN HTTP4A_RESPONSE_STRUCT *res,
																IN ZOO_INT32 error_code);

		/*
		 * @brief Publish server running status
		**/ 
		void publish_HTTP4A_status(IN HTTP4A_STATUS_STRUCT *status,IN ZOO_INT32 error_code);
    };
} // namespace HTTP
#endif
