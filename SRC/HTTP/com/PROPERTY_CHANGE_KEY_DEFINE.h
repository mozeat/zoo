/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : PROPERTY_CHANGE_KEY_DEFINE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef PROPERTY_CHANGE_KEY_DEFINE_H
#define PROPERTY_CHANGE_KEY_DEFINE_H
#include <ZOO.h>

/******************************************************************
* Define property key 
*****************************************************************/
#define REMOTE_CLINET_REQ      (0) 
#define REMOTE_SERVER_REP      (1) 
#define CONNECTION_CLOSE       (2) 
#define SERVER_SHUTDOWN        (3) 
#define SERVER_STARTUP         (4)  
#define SERVER_HANDSHAKE_FAIL  (5)
#define CLIENT_HANDSHAKE_FAIL  (6)
#define CLIENT_DISCONNECT      (7)
#define CLIENT_CONNECT_FAIL    (8)
#define CLIENT_WRITE_FAIL      (9)
#define CLIENT_READ_FAIL       (10)
#define CLIENT_RESOLVE_FAIL    (11)
#define SERVER_REPLY_OK        (12)
#define SERVER_REPLY_FAIL      (13)
#define REMOTE_CLINET_SSL_REQ  (14) 

#endif
