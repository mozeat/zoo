/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_CLIENT_ABSTRACT_MODEL_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef HTTP_CLIENT_ABSTRACT_MODEL_CLASS_H
#define HTTP_CLIENT_ABSTRACT_MODEL_CLASS_H
#include "HTTP_CLIENT_INTERFACE.h"

namespace HTTP
{
	class HTTP_CLIENT_ABSTRACT_MODEL_CLASS :  public virtual HTTP_CLIENT_INTERFACE
	{
	public:
	   /*
	    * @brief Default constructor
	   **/ 
	   HTTP_CLIENT_ABSTRACT_MODEL_CLASS();

	   /*
	    * @brief Destructor
	   **/ 
	   virtual ~HTTP_CLIENT_ABSTRACT_MODEL_CLASS();
	public:

		/*
		 * @brief Set event publisher.
		**/ 
		void set_event_publisher(boost::shared_ptr<EVENT_PUBLISHER_CLASS>
																			event_publisher);

		/*
		 * @brief Set file descriptor.
	    **/ 
		void set_fd(IN ZOO_HANDLE fd);

		/*
		 * @brief Get file descriptor.
	    **/ 
		ZOO_HANDLE get_fd();

		/*
		 * @brief run this request.
	    **/ 
		virtual void run(void * req_option);
		
		/*
		 * @brief This method is executed when property changed value
		 * @param model 			The model type
		 * @param property_name 	The property has been changed value
		**/
		virtual void on_property_changed(DEVICE_INTERFACE * model,
														const ZOO_UINT32 property_name);
		
	protected:
    	
		/*
         * @brief The event publisher.
       	**/ 
       	boost::shared_ptr<EVENT_PUBLISHER_CLASS> m_event_publisher;

	    /*
	     * @brief once flag.
	    **/ 
	    boost::once_flag m_once_flag;

        /*
	     * @brief The fake file descriptor.
	    **/ 
	    ZOO_HANDLE m_fd;

	    /*
	     * @brief Req timeout.
	    **/ 
	    ZOO_INT32 m_req_timeout;
	};
} //namespace HTTP
#endif
