/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_DOWNLOADER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef HTTP_DOWNLOADER_CLASS_H
#define HTTP_DOWNLOADER_CLASS_H
#include "HTTP_CLIENT_ABSTRACT_MODEL_CLASS.h"

namespace HTTP
{
	class HTTP_DOWNLOADER_CLASS :  public virtual HTTP_CLIENT_ABSTRACT_MODEL_CLASS
	{
	public:
	   /*
	    * @brief Default constructor
	   **/ 
	   HTTP_DOWNLOADER_CLASS();

	   /*
	    * @brief Destructor
	   **/ 
	   virtual ~HTTP_DOWNLOADER_CLASS();
	public:
	
		/*
		 * @brief connect ready .
		 * @param req   details
	    **/ 
		void run(IN void * req_option);

	private:
		/*
		 * @brief resolve host info.
		 * @param ec      error_code
		 * @param results   
		**/
		void on_resolve(IN boost::beast::error_code ec,IN boost::asio::ip::tcp::resolver::results_type results);
		
		/*
		 * @brief connect host.
		 * @param ec      error_code
		 * @param results   
		**/
		void on_connect(IN boost::beast::error_code ec,IN boost::asio::ip::tcp::resolver::results_type::endpoint_type);

		/*
		 * @brief perform http request
		 * @param ec      error_code
		 * @param bytes_transferred   
		**/
		void on_write(IN boost::beast::error_code ec,IN std::size_t bytes_transferred);

		/*
		 * @brief read reply from host.
		 * @param ec      error_code
		 * @param bytes_transferred   
		**/
		void on_read(IN boost::beast::error_code ec,IN std::size_t bytes_transferred);

		/*
		 * @brief Shutdown connection.
		 * @param ec      error_code
		**/
		void on_shutdown(IN boost::beast::error_code ec);

		/*
		 * @brief print error informations.
		 * @param ec      error_code
		 * @param why   
		**/
		void fail(ZOO_INT32 error_code,std::string why);
	private:
	
		/*
	    * @brief The io context attribute.
	    **/ 
        boost::asio::io_context m_ioc;
       
		/*
	     * @brief net attribute.
	    **/ 
		boost::asio::ip::tcp::resolver m_resolver;
		
		/*
	     * @brief tcp stream attribute.
	    **/ 
	    boost::beast::tcp_stream m_stream;
		       	
	    /*
	     * @brief flat buffer attribute.
	    **/ 
	    boost::beast::flat_buffer m_buffer;

	    /*
	     * @brief request attribute.
	    **/ 
	    boost::beast::http::request<boost::beast::http::empty_body> m_req;

	    /*
	     * @brief response attribute.
	    **/ 
	    boost::beast::http::response<boost::beast::http::string_body> m_res;
	    
		/*
	     * @brief Dwonload file .
	    **/ 
	    std::string m_download_file;
	};
} //namespace HTTP
#endif
