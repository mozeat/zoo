/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : EXTERNAL_PROCESS_TARGET_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef EXTERNAL_PROCESS_TARGET_CLASS_H
#define EXTERNAL_PROCESS_TARGET_CLASS_H

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <utils/ENVIRONMENT_UTILITY_CLASS.h>

namespace HTTP
{
    class EXTERNAL_PROCESS_TARGET_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       EXTERNAL_PROCESS_TARGET_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~EXTERNAL_PROCESS_TARGET_CLASS();
    public:

        std::vector<std::string> get_targets();
        
        void add_target(std::string target);

        void set_process_timeout_value(int seconds);

        int get_process_timeout_value();

        std::string get_root_path();
        void set_root_path(std::string path);

        std::string get_sql_type();
        void set_sql_type(std::string type);
        
        bool contain(std::string target);
    private:

        /*
         * @brief targets
        **/
        std::vector<std::string> m_targets;

        int m_process_timeout_value;

        std::string m_root_path;
        
        std::string m_sql_type;
    };
} //namespace HTTP
#endif
