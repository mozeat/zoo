/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : PLAIN_WEBSOCKET_SESSION_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef PLAIN_WEBSOCKET_SESSION_CLASS_H
#define PLAIN_WEBSOCKET_SESSION_CLASS_H
#include "WEBSOCKET_SESSION_CLASS.h"
namespace HTTP
{
    class PLAIN_WEBSOCKET_SESSION_CLASS :  public WEBSOCKET_SESSION_CLASS<PLAIN_WEBSOCKET_SESSION_CLASS>
                                                             public std::enable_shared_from_this<PLAIN_WEBSOCKET_SESSION_CLASS>
    {
    public:
		/*
		 * @brief Constructor
		**/
		explicit PLAIN_WEBSOCKET_SESSION_CLASS(beast::tcp_stream && stream)
		:m_ws(std::move(stream))
        {
            
        }
							
        /*
         * @brief Destructor
        **/ 
        virtual ~PLAIN_WEBSOCKET_SESSION_CLASS()
        {
        
        }
    public:
    
		// Called by the base class
        websocket::stream<beast::tcp_stream> &  ws()
        {
            return this->m_ws;
        }

    private:
   	
    	/*
         * @brief The stream.
       	**/ 
		websocket::stream<beast::tcp_stream> m_ws;
    };
} //namespace HTTP
#endif
