/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_COMMON_MACRO_DEFINE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef HTTP_COMMON_MACRO_DEFINE_H
#define HTTP_COMMON_MACRO_DEFINE_H
#include <ZOO.h>
#include <ZOO_if.h>
#include <exceptions/PARAMETER_EXCEPTION_CLASS.h>
#include <boost/format.hpp>
#define HTTP_THREADS_MAX 500

/*
 * @brief Wrapper try
 **/
#define __HTTP_TRY try

/*
* @brief Define a macro to throw HTTP exception
* @param error_code        The error code
* @param error_message     The error message
* @param inner_exception   The inner exception,& std::exception
**/
#define __THROW_HTTP_EXCEPTION(error_code,error_message,inner_exception) \
ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__PRETTY_FUNCTION__,"%s",error_message); \
throw ZOO_COMMON::PARAMETER_EXCEPTION_CLASS(error_code, error_message, inner_exception);

/*
* @brief Define a macro catch model exception
* @param continue        continue throw exception
**/
#define __HTTP_CATCH(continue) catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & e) \
{\
    if(continue) throw ZOO_COMMON::PARAMETER_EXCEPTION_CLASS(e.get_error_code(), e.get_error_message(),NULL);\
}

/*
* @brief Define a macro to catch all exception
* @param result
**/
#define __HTTP_CATCH_ALL(result) catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & e) \
{\
    result = e.get_error_code();\
}\
catch(...){}

/*
 * @brief Wrapper log
 **/
#define __HTTP_LOG(format,arg1) {\
boost::format f_(format);\
f_ % arg1;\
ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"%s",std::move(f_.str()).c_str());\
}

/*
 * @brief Wrapper log
 **/
#define __HTTP_LOG(format,arg1,arg2) {\
boost::format f_(format);\
f_ % arg1 % arg2;\
ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"%s",std::move(f_.str()).c_str());\
}

/*
 * @brief Wrapper log
 **/
#define __HTTP_LOG(format,arg1,arg2,arg3) {\
boost::format f_(format);\
f_ % arg1 % arg2 % arg3;\
ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"%s",std::move(f_.str()).c_str());\
}

/*
 * @brief Wrapper log
 **/
#define __HTTP_LOG(format,arg1,arg2,arg3,arg4) {\
boost::format f_(format);\
f_ % arg1 % arg2 % arg3 % arg4;\
ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"%s",std::move(f_.str()).c_str());\
}

/*
 * @brief Wrapper log
 **/
#define __HTTP_LOG(format,arg1,arg2,arg3,arg4,arg5) {\
boost::format f_(format);\
f_ % arg1 % arg2 % arg3 % arg4 % arg5;\
ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"%s",std::move(f_.str()).c_str());\
}

/*
 * @brief Wrapper log
 **/
#define __HTTP_LOG(format,arg1,arg2,arg3,arg4,arg5,arg6) {\
boost::format f_(format);\
f_ % arg1 % arg2 % arg3 % arg4 % arg5 % arg6;\
ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"%s",std::move(f_.str()).c_str());\
}

/*
 * @brief Wrapper log
 **/
#define __HTTP_LOG(format,arg1,arg2,arg3,arg4,arg5,arg6,arg7) {\
boost::format f_(format);\
f_ % arg1 % arg2 % arg3 % arg4 % arg5 % arg6 % arg7;\
ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"%s",std::move(f_.str()).c_str());\
}

/*
 * @brief Wrapper log
 **/
#define __HTTP_LOG_ERROR(format,arg1) {\
boost::format f_(format);\
f_ % arg1;\
ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,"%s",std::move(f_.str()).c_str());\
}

/*
 * @brief Wrapper log
 **/
#define __HTTP_LOG_ERROR(format,arg1,arg2) {\
boost::format f_(format);\
f_ % arg1 % arg2;\
ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,"%s",std::move(f_.str()).c_str());\
}

#endif
