/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_MODEL_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef HTTP_SERVER_MODEL_CLASS_H
#define HTTP_SERVER_MODEL_CLASS_H
#include "HTTP_SERVER_INTERFACE.h"
#include "CERT_MANAGER_CLASS.h"
#include "LISTENER_CLASS.h"
#include <vector>
#include <boost/algorithm/string.hpp>
namespace HTTP
{
    class HTTP_SERVER_MODEL_CLASS: public          virtual HTTP_SERVER_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       HTTP_SERVER_MODEL_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~HTTP_SERVER_MODEL_CLASS();
    public:
		/*
		 * @brief Load configurations.
		**/
    	void initialize();
    	
		/*
		 * @brief Run the eventloop.
		**/ 
		void run();

		/*
		 * @brief Stop the eventloop.
		**/ 
		void stop();

		/*
         * @brief This method is executed when property changed value
         * @param model             The model type
         * @param property_name     The property has been changed value
        **/
        OVERRIDE
        void on_property_changed(MARKING_MODEL_INTERFACE * model,
									        const ZOO_UINT32 property_name);
	private:
		
		/*
		 * @brief Load ssl context.
		**/
		void load_server_certificate(boost::asio::ssl::context & ctx);

		/*
		 * @brief Get acceptor.
		**/
		void update_status(ZOO_BOOL active,ZOO_INT32 error_code);
		
		/**
		 * This function is used to specify a callback function that will be called
		 * by the implementation when it needs to verify a peer certificate.
		 *
		 * @param callback The function object to be used for verifying a certificate.
		 * The function signature of the handler must be:
 		 * @code bool verify_callback(
 		 *   bool preverified, // True if the certificate passed pre-verification.
		 *   verify_context& ctx // The peer certificate and other context.
		 * ); @endcode
		 * The return value of the callback is true if the certificate has passed
		 * verification, false otherwise.
		 *
		 * @throws boost::system::system_error Thrown on failure.
		 *
		 * @note Calls @c SSL_CTX_set_verify.
		 */
		bool ssl_verify_callback(bool preverified,boost::asio::ssl::verify_context & ctx);
    private:	
		/*
		 * @brief The server status.
		**/
		HTTP4A_STATUS_STRUCT m_status;
        net::io_context m_ioc;
        boost::thread_group m_thread_pool;
        boost::shared_ptr<LISTENER_CLASS> m_http_listener;
    };
} //namespace HTTP
#endif
