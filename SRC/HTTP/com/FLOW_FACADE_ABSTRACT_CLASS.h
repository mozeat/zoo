/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : FLOW_FACADE_ABSTRACT_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef FLOW_FACADE_ABSTRACT_CLASS_H
#define FLOW_FACADE_ABSTRACT_CLASS_H

extern "C" 
{
    #include <ZOO.h>
    #include <HTTP4A_type.h>
}
#include "HTTP_COMMON_MACRO_DEFINE.h"
#include "FLOW_FACADE_INTERFACE.h"
#include <boost/shared_ptr.hpp>

namespace HTTP
{
    class FLOW_FACADE_ABSTRACT_CLASS : public virtual FLOW_FACADE_INTERFACE
    {
	public:
		/*
		 * @brief Constructor
		**/ 
		FLOW_FACADE_ABSTRACT_CLASS();

		/*
		 * @brief Destructor
		**/ 
		virtual ~FLOW_FACADE_ABSTRACT_CLASS();
	public:
		/*
		* @brief Set device controller instance
		**/ 
		void set_device_controller(IN boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE> device_controller);

		/*
		 * @brief Get device controller instance
		**/ 
		boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE>  get_device_controller();

		/*
		 * @brief Set state manager instance
		**/ 
		void set_state_manager(IN boost::shared_ptr<STATE_MANAGER_CLASS> state_manager);

		/*
		 * @brief Get state manager instance
		**/ 
		boost::shared_ptr<STATE_MANAGER_CLASS>  get_state_manager();

		/*
		 * @brief Set http server instance
		**/ 
		void set_http_server(IN boost::shared_ptr<HTTP_SERVER_INTERFACE> hs_model);

		/*
		 * @brief This method is executed when property changed value
		 * @param model The source object contains property changed
		 * @param property_name The property has been changed value
		**/ 
		virtual void on_property_changed(IN CONTROLLER_INTERFACE* model,
												IN const ZOO_UINT32 property_name);

    protected:
		/*
		 * @brief The device controller instance
		**/ 
		boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE>  m_device_controller;

		/*
		 * @brief The state manager instance
		**/ 
		boost::shared_ptr<STATE_MANAGER_CLASS>  m_state_manager;

		/*
		 * @brief The state manager instance
		**/ 
		boost::shared_ptr<HTTP_SERVER_INTERFACE>  m_http_server;
    };
} // namespace HTTP

#endif
