/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : MOCK_DATA_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-17    Generator      created
*************************************************************/
#ifndef MOCK_DATA_CLASS_H
#define MOCK_DATA_CLASS_H
#include <vector>
#include <map>
#include <string>
#include <boost/shared_ptr.hpp>

namespace HTTP
{
    class MOCK_DATA_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       MOCK_DATA_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~MOCK_DATA_CLASS();
    public:
       /*
        * @brief Add data
        * @param name
        * @param value
       **/
       void add_data(std::string name,std::string value);

       /*
        * @brief Get data
        * @param name
       **/
       std::string get_data(std::string name);

    private:
       /*
        * @brief Test data map
       **/
       std::map<std::string,std::string> m_data_map;
    };
}
#endif
