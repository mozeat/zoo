/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : PROCESSING_FLOW_FACADE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef PROCESSING_FLOW_FACADE_CLASS_H
#define PROCESSING_FLOW_FACADE_CLASS_H

#include "PROCESSING_FLOW_FACADE_INTERFACE.h"
#include "FLOW_FACADE_ABSTRACT_CLASS.h"

namespace HTTP
{
    class PROCESSING_FLOW_FACADE_CLASS : virtual public PROCESSING_FLOW_FACADE_INTERFACE,
                                         public FLOW_FACADE_ABSTRACT_CLASS 
    {
    public:
       /*
        * @brief Constructor
       **/ 
       PROCESSING_FLOW_FACADE_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~PROCESSING_FLOW_FACADE_CLASS();
    public:
        /*
         * @brief Create an http client instance
         * @param fd
         * @return error_code
         **/
        ZOO_INT32 create(INOUT ZOO_HANDLE * fd);

        /*
         * @brief HTTP4A_cleanup
         * @param fd
         * @return error_code
        **/
        ZOO_INT32 cleanup(IN ZOO_HANDLE fd);

        /*
         * @brief Send a http request
         * @param req
         * @return error_code
        **/
        ZOO_INT32 on_request_sig(IN HTTP4A_REQUEST_STRUCT *req);

        /*
         * @brief HTTP4A_on_download_file_sig
        **/
         ZOO_INT32 on_download_file_sig(IN ZOO_HANDLE fd,
															IN HTTP4A_DOWNLOAD_OPTION_STRUCT * dl_option);

        /*
         * @brief Send a http request
         * @param req
         * @param ssl_option
         * @return error_code
        **/
        ZOO_INT32 on_ssl_request_sig(IN HTTP4A_REQUEST_STRUCT *req,
													IN HTTP4A_SSL_OPTION_STRUCT *ssl_option);

        /*
         * @brief HTTP4A_reply_server
         * @param id
         * @param message
         * @return error_code
        **/
        ZOO_INT32 handle_request(IN ZOO_INT32 id,
												 IN HTTP4A_RESPONSE_STRUCT * message);

												 
		/*
		 * @brief Server start to listen incoming request
		 * @return error_code
		**/
        ZOO_INT32 run();

        /*
		 * @brief Server stop to listen incoming request
		 * @return error_code
		**/
        ZOO_INT32 stop();
        
        /*
         * @brief This method is executed when property changed value
         * @param model             The model type
         * @param property_name     The property has been changed value
        **/ 
        OVERRIDE
        void on_property_changed(CONTROLLER_INTERFACE * model,const ZOO_UINT32 property_name);

    };
}//namespace HTTP

#endif
