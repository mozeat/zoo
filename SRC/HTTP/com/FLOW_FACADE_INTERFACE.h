/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : FLOW_FACADE_INTERFACE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef FLOW_FACADE_INTERFACE_H
#define FLOW_FACADE_INTERFACE_H

extern "C" 
{
    #include <ZOO.h>
    #include <HTTP4A_type.h>
}
#include "HTTP_COMMON_MACRO_DEFINE.h"
#include "MARKING_MODEL_INTERFACE.hpp"
#include "DEVICE_CONTROLLER_INTERFACE.h"
#include "STATE_MANAGER_CLASS.h"
#include "HTTP_SERVER_INTERFACE.h"

namespace HTTP
{
    class FLOW_FACADE_INTERFACE: virtual public PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>
    {
    public:
       /*
        * @brief Constructor
       **/ 
       FLOW_FACADE_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~FLOW_FACADE_INTERFACE(){}
    public:
       /*
        * @brief Set device controller instance
       **/ 
       virtual void set_device_controller(IN boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE> device_controller) = 0;

       /*
        * @brief Set state manager instance
       **/ 
       virtual void set_state_manager(IN boost::shared_ptr<STATE_MANAGER_CLASS> state_manager) = 0;
       
	   /*
        * @brief Set http server instance
       **/ 
       virtual void set_http_server(IN boost::shared_ptr<HTTP_SERVER_INTERFACE> hs_model) = 0;
       
    };
} // namespace HTTP

#endif
