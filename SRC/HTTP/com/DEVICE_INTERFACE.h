/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : DEVICE_INTERFACE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef DEVICE_INTERFACE_H
#define DEVICE_INTERFACE_H

extern "C" 
{
    #include <ZOO.h>
    #include <HTTP4A_type.h>
}
#include "HTTP_COMMON_MACRO_DEFINE.h"
#include "MARKING_MODEL_INTERFACE.hpp"
namespace HTTP
{
    class DEVICE_INTERFACE: public virtual MARKING_MODEL_INTERFACE
                             ,public virtual PROPERTY_CHANGED_OBSERVER_INTERFACE<DEVICE_INTERFACE>
                             ,public virtual NOTIFY_PROPERTY_CHANGED_INTERFACE<MARKING_MODEL_INTERFACE>
    {
    public:
       /*
        * @brief Constructor
       **/ 
       DEVICE_INTERFACE();

       /*
        * @brief Destructor
       **/ 
       virtual ~DEVICE_INTERFACE();
    public:
       /*
        * @brief Add observer will be notified when property changed.
        * @param observer  Property changed observer
       **/ 
       virtual void add_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer);

       /*
        * @brief Remove observer will be notified when property changed.
        * @param observer  Property changed observer
       **/ 
       virtual void remove_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer);

       /*
        * @brief Clean.
       **/ 
       void clean();
       /*
        * @brief Notify property has been changed.
        * @param property_name     The property has been changed
        * @param property_value     The property value
       **/ 
       virtual void notify_of_property_changed(const ZOO_UINT32 property_name);

		/*
		 *@brief build_response_message.
         *@param fd     
         *@param error_code     The error code
         *@param content_type     
         *@param body
         *@return response message
	    **/ 
	    boost::shared_ptr<HTTP4A_RESPONSE_STRUCT> build_response_message(IN ZOO_HANDLE fd,
																				  IN ZOO_INT32 error_code,
																				  IN std::string content_type,
																				  IN std::string & body);
		/*
		 *@brief build_response_message.
         *@param inprogress     
         *@return response message
	    **/ 
	    boost::shared_ptr<HTTP4A_INPROGRESS_STRUCT> build_message(IN ZOO_HANDLE fd,
	    																IN std::string file_name,
	    																IN ZOO_INT32 speed,
	    																IN ZOO_INT32 total_time);																		  
    protected:
    	
    	boost::shared_ptr<HTTP4A_RESPONSE_STRUCT> m_reply;
    	
       /*
        * @brief The list of observer instances will be notified when property has been changed.
       **/ 
       std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>*> m_observers;
    };
}// namespace HTTP

#endif
