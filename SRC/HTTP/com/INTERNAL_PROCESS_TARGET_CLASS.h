/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : INTERNAL_PROCESS_TARGET_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef INTERNAL_PROCESS_TARGET_CLASS_H
#define INTERNAL_PROCESS_TARGET_CLASS_H

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <utils/ENVIRONMENT_UTILITY_CLASS.h>

namespace HTTP
{
    class INTERNAL_PROCESS_TARGET_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       INTERNAL_PROCESS_TARGET_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~INTERNAL_PROCESS_TARGET_CLASS();
    public:

        std::vector<std::string> get_targets();
        
        void add_target(std::string target);

        void set_root_doc(std::string root_doc);

        std::string get_root_doc();

        void set_database_type(std::string type);

        std::string get_database_type();
        
        bool contain(std::string target);
    private:

        std::string m_root_doc;
        std::string m_database_type;
        /*
         * @brief targets
        **/
        std::vector<std::string> m_targets;
    };
} //namespace HTTP
#endif
