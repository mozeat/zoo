/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : LISTENER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef LISTENER_CLASS_H
#define LISTENER_CLASS_H
#include "SESSION_DETECTER_CLASS.h"
#include <vector>
#include <memory>
#include <boost/beast.hpp>
namespace HTTP
{
    class LISTENER_CLASS 
    {
    public:
        LISTENER_CLASS(
            net::io_context & ioc,
            ssl::context & ctx,
            tcp::endpoint endpoint,
            std::shared_ptr<std::string const> const& doc_root)
            : this->m_ioc(ioc)
            , this->m_ctx(ctx)
            , this->m_acceptor(net::make_strand(ioc))
            , this->m_doc_root(doc_root)
        {
            beast::error_code ec;

            // Open the acceptor
            this->m_acceptor.open(endpoint.protocol(), ec);
            if(ec)
            {
                __THROW_HTTP_EXCEPTION(HTTP4A_SYSTEM_ERR,"open endpoint error",NULL);
            }

            // Allow address reuse
            this->m_acceptor.set_option(net::socket_base::reuse_address(true), ec);
            if(ec)
            {
                __THROW_HTTP_EXCEPTION(HTTP4A_SYSTEM_ERR,"set option error",NULL);
            }

            // Bind to the server address
            this->m_acceptor.bind(endpoint, ec);
            if(ec)
            {
                __THROW_HTTP_EXCEPTION(HTTP4A_SYSTEM_ERR,"bind error",NULL);
            }

            // Start listening for connections
            this->m_acceptor.listen(
                net::socket_base::max_listen_connections, ec);
            if(ec)
            {
                __THROW_HTTP_EXCEPTION(HTTP4A_SYSTEM_ERR,"listen error",NULL);
            }
        }

        // Start accepting incoming connections
        void run()
        {
            this->do_accept();
        }

    private:
        void do_accept()
        {
            // The new connection gets its own strand
            this->m_acceptor.async_accept(net::make_strand(this->m_ioc),
                beast::bind_front_handler(&LISTENER_CLASS::on_accept,this);
        }

        void on_accept(beast::error_code ec, tcp::socket socket)
        {
            if(ec)
            {
                __THROW_HTTP_EXCEPTION(HTTP4A_SYSTEM_ERR,"accept error",NULL);
            }
            else
            {
                // Create the detector http_session and run it
                std::make_shared<SESSION_DETECTER_CLASS>(std::move(socket),
                                                        this->m_ctx,
                                                            this->m_doc_root)->run();
            }

            // Accept another connection
            this->do_accept();
        }
    private:
        net::io_context& m_ioc;
        ssl::context& m_ctx;
        tcp::acceptor m_acceptor;
        std::shared_ptr<std::string const> m_doc_root;
    };
}
