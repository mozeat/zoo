/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_SESSION_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef HTTP_SESSION_CLASS_H
#define HTTP_SESSION_CLASS_H
#include "WEBSOCKET_SESSION_CLASS.h"
#include "SESSION_FACTORY.h"
namespace HTTP
{
    template <typename T>
    class HTTP_SESSION_CLASS
    {
    public:
		/*
		 * @brief Constructor
		**/
		HTTP_SESSION_CLASS(beast::flat_buffer&& buffer,
                                             std::shared_ptr<std::string const> const& doc_root)
                                             :m_doc_root(doc_root)
                                             ,m_process_queue(*this)
                                             ,m_buffer(std::move(buffer))
        {
            
        }
							
        /*
         * @brief Destructor
        **/ 
        virtual ~HTTP_SESSION_CLASS()
        {
            
        }
    public:
    
        /*
         * @brief Start the asynchronous operation.
        **/ 
        void do_read()
        {
            // Construct a new parser for each message
            this->m_parser.emplace();

            // Apply a reasonable limit to the allowed size
            // of the body in bytes to prevent abuse.
            this->m_parser->body_limit(10000);

            // Set the timeout.
            beast::get_lowest_layer(
                this->derived().stream()).expires_after(std::chrono::seconds(30));

            // Read a request using the parser-oriented interface
            http::async_read(
                derived().stream(),
                this->m_buffer,
                *this->m_parser,
                beast::bind_front_handler(
                    &HTTP_SESSION_CLASS::on_read,
                    this->derived().shared_from_this()));
        }

        void on_read(beast::error_code ec, std::size_t bytes_transferred)
        {
            boost::ignore_unused(bytes_transferred);

            // This means they closed the connection
            if(ec == http::error::end_of_stream)
            {
                return this->derived().do_eof();
            }

            if(ec)
            {
                __HTTP_LOG_ERROR("%1%",ec.message());
                return ;
            }

            // See if it is a WebSocket Upgrade
            if(websocket::is_upgrade(this->m_parser->get()))
            {
                // Disable the timeout.
                // The websocket::stream uses its own timeout settings.
                beast::get_lowest_layer(this->derived().stream()).expires_never();

                // Create a websocket session, transferring ownership
                // of both the socket and the HTTP request.
                return make_websocket_session(
                    this->derived().release_stream(),
                    this->m_parser->release());
            }

            // Send the response
            //handle_request(*this->m_doc_root, this->m_parser->release(), this->m_process_queue);

            // If we aren't at the queue limit, try to pipeline another request
            if(!this->m_process_queue.is_full())
            {
                this->do_read();
            }
        }

        void on_write(bool close, beast::error_code ec, std::size_t bytes_transferred)
        {
            boost::ignore_unused(bytes_transferred);

            if(ec)
            {
                __HTTP_LOG_ERROR("%1%",ec.message());
                return;
            }

            if(close)
            {
                // This means we should close the connection, usually because
                // the response indicated the "Connection: close" semantic.
                return this->derived().do_eof();
            }

            // Inform the queue that a write completed
            if(this->m_process_queue.on_write())
            {
                // Read another request
                this->do_read();
            }
        }
    private:
        // Access the derived class, this is part of
        // the Curiously Recurring Template Pattern idiom.
        T & derived()
        {
            return static_cast<T&>(*this);
        }
        
    protected:
        beast::flat_buffer m_buffer;
        
    private:
        std::shared_ptr<std::string const> m_doc_root;

        // The parser is stored in an optional container so we can
        // construct it from scratch it at the beginning of each new message.
        boost::optional<http::request_parser<http::string_body>> m_parser;
    };
} //namespace HTTP
#endif
