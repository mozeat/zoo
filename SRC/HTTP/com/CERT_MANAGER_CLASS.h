/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : CERT_MANAGER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef CERT_MANAGER_CLASS_H
#define CERT_MANAGER_CLASS_H

extern "C" 
{
    #include <ZOO.h>
    #include <ZOO_if.h>
    #include <HTTP4A_type.h>
}
#include <boost/shared_ptr.hpp>
#include <boost/thread/once.hpp>
#include "MARKING_MODEL_INTERFACE.hpp"
#include "EVENT_PUBLISHER_CLASS.h"

namespace HTTP
{

	typedef enum
	{
		CERT_CLIENT = 0,
		CERT_SEVER
	}CERT_TYEP_ENUM;
	
    class CERT_MANAGER_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       CERT_MANAGER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~CERT_MANAGER_CLASS();

	   /*
        * @brief get instance
       **/ 
       static boost::shared_ptr<CERT_MANAGER_CLASS> get_instance();
    public:

    	void set_verify_path(std::string path);
		
    	std::string get_verify_path();
    	
		/*
		* @brief Get public_key
		**/ 
		void set_public_key_file(CERT_TYEP_ENUM cert_type,std::string public_key_file);

		/*
		* @brief Set public_key
		**/ 
		std::string get_public_key_file(CERT_TYEP_ENUM cert_type);

		/*
		* @brief Set public_key
		**/ 
		void set_private_key_file(CERT_TYEP_ENUM cert_type,std::string private_key_file);

		/*
		* @brief Get public_key
		**/ 
		std::string get_private_key_file(CERT_TYEP_ENUM cert_type);
       
		 /*
		 * @brief Set trust_chain file
		**/ 
		void set_trust_chain_file(CERT_TYEP_ENUM cert_type,std::string trust_chain_file);

		 /*
		 * @brief Get trust_chain file
		**/ 
		std::string get_trust_chain_file(CERT_TYEP_ENUM cert_type);

        void set_verify_mode(int mode);
        int get_verify_mode();
        void set_tls_version(int version);
        int get_tls_version();
   	private:
   		std::string m_path;
   		/*
		 * @brief client public_key_file attribute.
		**/
   		std::string m_client_public_key_file;

   		/*
		 * @brief client private_key_file attribute.
		**/
   		std::string m_client_private_key_file;
		
   		/*
		 * @brief server public_key_file attribute.
		**/
   		std::string m_server_public_key_file;

   		/*
		 * @brief server private_key_file attribute.
		**/
   		std::string m_server_private_key_file;
  		
   		/*
		 * @brief client trust_chain_file attribute.
		**/
   		std::string m_client_trust_chain_file;

   		/*
		 * @brief server trust_chain_file attribute.
		**/
   		std::string m_server_trust_chain_file;

        int m_tls_version;
        int m_verify_mode;
   		static boost::once_flag m_once_flag;

   		static boost::shared_ptr<CERT_MANAGER_CLASS> m_instance;
    };
}// namespace HTTP

#endif
