/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : DEVICE_CONTROLLER_INTERFACE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef DEVICE_CONTROLLER_INTERFACE_H
#define DEVICE_CONTROLLER_INTERFACE_H

#include "HTTP_COMMON_MACRO_DEFINE.h"
#include "HTTP_CLIENT_ABSTRACT_MODEL_CLASS.h"
#include "CONTROLLER_INTERFACE.h"
namespace HTTP
{
    class DEVICE_CONTROLLER_INTERFACE: virtual public CONTROLLER_INTERFACE
    {
    public:
        /*
         * @brief Constructor
        **/ 
        DEVICE_CONTROLLER_INTERFACE(){}

        /*
         * @brief Destructor
        **/ 
        virtual ~DEVICE_CONTROLLER_INTERFACE(){}
    public:
		/*
		* @brief Initialize the model
		* @return throw exception 
		**/ 
		virtual void initialize() = 0;

		/*
		* @brief Terminate the model
		* @return throw exception 
		**/ 
		virtual void terminate() = 0;

		/*
		* @brief Get device status.
		* @return throw exception 
		**/ 
		virtual void get_status(INOUT HTTP4A_STATUS_STRUCT * status) = 0;

       	/*
		 * @brief Create an http client instance
		 * @param fd
		 * @return error_code
		 **/
		virtual ZOO_INT32 create(INOUT ZOO_HANDLE * fd) = 0;

		/*
		 * @brief HTTP4A_cleanup
		 * @param fd
		 * @return error_code
		**/
		virtual ZOO_INT32 cleanup(IN ZOO_HANDLE fd) = 0;

		/*
		 * @brief Send a http request
		 * @param req
		 * @return error_code
		**/
		virtual ZOO_INT32 request(IN HTTP4A_REQUEST_STRUCT * req) = 0;

		/*
		 * @brief Send a http request
		 * @param req
		 * @return error_code
		**/
		virtual ZOO_INT32 download(IN ZOO_HANDLE fd,
															IN HTTP4A_DOWNLOAD_OPTION_STRUCT * dl_option) = 0;
		/*
		 * @brief Send a http request
		 * @param req
		 * @param ssl_option
		 * @return error_code
		**/
		virtual ZOO_INT32 request(IN HTTP4A_REQUEST_STRUCT * req,
													IN HTTP4A_SSL_OPTION_STRUCT *ssl_option) = 0;
							 
    };
} // namespace HTTP

#endif
