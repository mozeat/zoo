/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : SESSION_ABSTRACT_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef SESSION_ABSTRACT_CLASS_H
#define SESSION_ABSTRACT_CLASS_H
#include "SESSION_INTERFACE.h"
#include <boost/nondet_random.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/random.hpp>
#include <boost/random/uniform_int_distribution.hpp>
namespace HTTP
{
    class SESSION_ABSTRACT_CLASS : public virtual SESSION_INTERFACE
    {
    public:
		/*
		 * @brief Constructor
		**/
		SESSION_ABSTRACT_CLASS();
							
    
        /*
         * @brief Destructor
        **/ 
        virtual ~SESSION_ABSTRACT_CLASS();
    public:
		/*
		 * @brief Start the asynchronous operation.
		**/ 
		virtual void run();

		/*
		 * @brief Response to the req.
		 * @param error_code      reply error_code 
		 * @param reply_message   data
		**/ 
		virtual void do_write(IN std::string && body);

		/*
		 * @brief Set file descriptor of the socket.
		**/ 
		void set_fd(IN ZOO_HANDLE fd);
		
		/*
		 * @brief Get file descriptor of the socket.
		**/ 
		virtual ZOO_HANDLE get_fd();

		/*
		 * @brief Get file descriptor of the socket.
		**/ 
		boost::shared_ptr<HTTP4A_REQUEST_STRUCT> get_message();
		
		/*
		 * @brief Add observer will be notified when property changed
		 * @param observer	Property changed observer
		**/
		void add_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer);

		/*
		 * @brief Remove observer from subscribe list
		**/
		void remove_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer);

		/*
		 * @brief Remove observer from subscribe listd
		**/
		void clean();
		
		/*
		 * @brief Notify property has been changed
		 * @param property_name 	The property has been changed
		**/
		void notify_of_property_changed(IN const ZOO_UINT32 property_name);

		/*
		 * @brief Rebuild request message to HTTP4A_REQUEST_STRUCT.
		 * @param req   the request  string body
		 * @param message 
		**/ 
		void build_message(IN boost::beast::http::request<boost::beast::http::string_body> req);

		/**
		 * @brief
		 */
		ZOO_HANDLE make_unique_fd();
    protected:

		/*
		 * @brief The file descriptor of the socket.
		**/ 
		ZOO_HANDLE m_fd;
		
   		/*
         * @brief Declare a container to hold the request message
         **/
        boost::shared_ptr<HTTP4A_REQUEST_STRUCT> m_message;
        
		/*
         * @brief The observers.
       	**/ 
       	std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* > m_observers;
    };
} //namespace HTTP
#endif

