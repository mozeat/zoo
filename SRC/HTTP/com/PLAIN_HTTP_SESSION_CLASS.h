/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : PLAIN_HTTP_SESSION_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef PLAIN_HTTP_SESSION_CLASS_H
#define PLAIN_HTTP_SESSION_CLASS_H
#include "HTTP_SESSION_CLASS.h"
namespace HTTP
{
    class PLAIN_HTTP_SESSION_CLASS :public HTTP_SESSION_CLASS<PLAIN_HTTP_SESSION_CLASS>
                                                    public std::enable_shared_from_this<PLAIN_HTTP_SESSION_CLASS>
    {
    public:
		/*
		 * @brief Constructor
		**/
		PLAIN_HTTP_SESSION_CLASS(beast::tcp_stream&& stream,
                                                beast::flat_buffer&& buffer,
                                                std::shared_ptr<std::string const> const& doc_root)
                                                :HTTP_SESSION_CLASS<PLAIN_HTTP_SESSION_CLASS>(std::move(buffer),doc_root)
                                                ,m_stream(std::move(stream))
        {
        }
							
        /*
         * @brief Destructor
        **/ 
        virtual ~PLAIN_HTTP_SESSION_CLASS()
        {
            
        }
    public:
    
		// Start the session
        void run()
        {
            this->do_read();
        }

        // Called by the base class
        beast::tcp_stream & stream()
        {
            return this->m_stream;
        }

        // Called by the base class
        beast::tcp_stream release_stream()
        {
            return std::move(this->m_stream);
        }

        // Called by the base class
        void  do_eof()
        {
            // Send a TCP shutdown
            beast::error_code ec;
            this->m_stream.socket().shutdown(tcp::socket::shutdown_send, ec);
            // At this point the connection is closed gracefully
        }

    private:
   	
    	/*
         * @brief The stream.
       	**/ 
		beast::tcp_stream m_stream;
    };
} //namespace HTTP
#endif
