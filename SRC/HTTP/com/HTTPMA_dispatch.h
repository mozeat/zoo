/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : HTTPMA_dispatch.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-05    Generator      created
*************************************************************/

#ifndef HTTPMA_DISPATCH_H
#define HTTPMA_DISPATCH_H
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include "HTTP4I_type.h"
#include "HTTP4I_if.h"
#include "HTTP4A_type.h"
#include "HTTPMA_implement.h"

/**
*@brief Dispatch message from client to server internal interface
*@param context        
*@param server        address
*@param msg           request message to server
*@param len           request message length
*@param reply_msg     reply message length to caller
*@param reply_msg_len reply message length
**/
ZOO_EXPORT void HTTPMA_callback_handler(void * context,const MQ4A_SERV_ADDR server,void * msg,ZOO_INT32 len);

#endif // HTTPMA_dispatch.h
