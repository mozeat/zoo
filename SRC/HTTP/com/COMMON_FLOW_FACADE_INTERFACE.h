/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : COMMON_FLOW_FACADE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef COMMON_FLOW_FACADE_INTERFACE_H
#define COMMON_FLOW_FACADE_INTERFACE_H

extern "C" 
{
    #include <ZOO.h>
    #include <HTTP4A_type.h>
}
#include <boost/shared_ptr.hpp>
#include "HTTP_COMMON_MACRO_DEFINE.h"
#include "FLOW_FACADE_INTERFACE.h"

namespace HTTP
{
    class COMMON_FLOW_FACADE_INTERFACE : virtual public FLOW_FACADE_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       COMMON_FLOW_FACADE_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~COMMON_FLOW_FACADE_INTERFACE(){}
    public:
       /*
        * @brief Initialize the model
        * @return error_code 
       **/ 
       virtual void initialize() = 0;

       /*
        * @brief Terminate the model
        * @return error_code 
       **/ 
       virtual void terminate() = 0;

       /*
        * @brief Get driver state
        * @return state 
       **/ 
       virtual ZOO_DRIVER_STATE_ENUM get_driver_state() = 0;

       /*
        * @brief Get device status.
        * @return error code 
       **/ 
       virtual ZOO_INT32 get_status(INOUT HTTP4A_STATUS_STRUCT * status) = 0;

       /*
        * @brief Change to debug mode
       **/ 
       virtual ZOO_INT32 change_to_debug_mode() = 0;

       /*
        * @brief Change to work mode
       **/ 
       virtual ZOO_INT32 change_to_work_mode() = 0;

    };
}// namespace HTTP

#endif
