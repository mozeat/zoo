/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : HTTPMA_event.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-05    Generator      created
*************************************************************/
#ifndef HTTPMA_EVENT_H
#define HTTPMA_EVENT_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include "HTTP4I_type.h"
#include "HTTP4I_if.h"
#include "HTTP4A_type.h"

/**
 *@brief HTTPMA_raise_4A_create
 *@param fd
**/
ZOO_EXPORT ZOO_INT32 HTTPMA_raise_4A_create(IN ZOO_INT32 error_code,
                                                IN ZOO_HANDLE fd,
                                                IN HTTP4I_REPLY_HANDLE reply_handle);

/**
 *@brief HTTPMA_raise_4A_cleanup
 *@param fd
**/
ZOO_EXPORT ZOO_INT32 HTTPMA_raise_4A_cleanup(IN ZOO_INT32 error_code,
                                                 IN HTTP4I_REPLY_HANDLE reply_handle);

/**
 *@brief HTTPMA_raise_4A_on_request_sig
 *@param req
**/
ZOO_EXPORT ZOO_INT32 HTTPMA_raise_4A_on_request_sig(IN ZOO_INT32 error_code,
                                                        IN HTTP4I_REPLY_HANDLE reply_handle);

/**
 *@brief HTTPMA_raise_4A_on_download_file_sig
 *@param fd
 *@param dl_option
**/
ZOO_EXPORT ZOO_INT32 HTTPMA_raise_4A_on_download_file_sig(IN ZOO_INT32 error_code,
                                                              IN HTTP4I_REPLY_HANDLE reply_handle);

/**
 *@brief HTTPMA_raise_4A_on_ssl_request_sig
 *@param req
 *@param ssl_option
**/
ZOO_EXPORT ZOO_INT32 HTTPMA_raise_4A_on_ssl_request_sig(IN ZOO_INT32 error_code,
                                                            IN HTTP4I_REPLY_HANDLE reply_handle);

/**
 *@brief HTTPMA_raise_4A_handle_request
 *@param id
 *@param message
**/
ZOO_EXPORT ZOO_INT32 HTTPMA_raise_4A_handle_request(IN ZOO_INT32 error_code,
                                                        IN HTTP4I_REPLY_HANDLE reply_handle);

/**
 *@brief HTTP4A_server_request_subscribe
 *@param req
 *@param error_code
 *@param *context
**/
ZOO_EXPORT void HTTPMA_raise_4A_server_request_subscribe(IN HTTP4A_REQUEST_STRUCT* req,IN ZOO_INT32 error_code,IN void *context);

/**
 *@brief HTTP4A_inprogress_subscribe
 *@param inprogress
 *@param error_code
 *@param *context
**/
ZOO_EXPORT void HTTPMA_raise_4A_inprogress_subscribe(IN HTTP4A_INPROGRESS_STRUCT *inprogress,IN ZOO_INT32 error_code,IN void *context);

/**
 *@brief HTTP4A_server_reply_subscribe
 *@param reply
 *@param error_code
 *@param *context
**/
ZOO_EXPORT void HTTPMA_raise_4A_server_reply_subscribe(IN HTTP4A_RESPONSE_STRUCT *reply,IN ZOO_INT32 error_code,IN void *context);

/**
 *@brief HTTP4A_status_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
ZOO_EXPORT void HTTPMA_raise_4A_status_subscribe(IN HTTP4A_STATUS_STRUCT *status,IN ZOO_INT32 error_code,IN void *context);


#endif // HTTPMA_event.h
