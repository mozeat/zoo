/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EX
 * File Name      : INI_PARSER_ABSTRACT_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-17    Generator      created
*************************************************************/
#ifndef INI_PARSER_ABSTRACT_CLASS_H
#define INI_PARSER_ABSTRACT_CLASS_H
extern "C" 
{
    #include "ZOO.h"
    #include "ZOO_if.h"
}

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>
#include <map>
#include <string>

namespace HTTP
{
    typedef std::string SECTION_TYPE;
    class INI_PARSER_ABSTRACT_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       INI_PARSER_ABSTRACT_CLASS(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~INI_PARSER_ABSTRACT_CLASS(){}
    public:
       /*
        * @brief Parse data from config file
       **/
       template<typename T>
       T parse_data(IN std::string file,IN std::string section,IN std::string field_name)
       {
           boost::property_tree::ptree pt;
           boost::property_tree::ini_parser::read_ini(file, pt);
           return pt.get<T>(section.append(".") + field_name);
       }

       /*
        * @brief Parse section from config file
       **/
       std::map<std::string,std::string> parse_section(IN std::string file,IN std::string section)
       {
           std::map<std::string,std::string> result;
           boost::property_tree::ptree pt;
           boost::property_tree::ini_parser::read_ini(file, pt);
           BOOST_FOREACH(const boost::property_tree::ptree::value_type & v, pt.get_child(section))
           {
               result[v.first] = v.second.get_value<std::string>();
           }
           return result;
       }
    };
}
#endif
