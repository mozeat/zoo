/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_FLOW_FACADE_WRAPPER.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-05    Generator      created
*************************************************************/
#ifndef HTTP_FLOW_FACADE_WRAPPER_H
#define HTTP_FLOW_FACADE_WRAPPER_H

#ifdef __cplusplus
extern "C" 
{
#endif
    #include "HTTP4A_type.h"
    /*
     * @brief HTTP4A_create
    **/
     ZOO_INT32 HTTP_create(INOUT ZOO_HANDLE * fd);

    /*
     * @brief HTTP4A_cleanup
    **/
     ZOO_INT32 HTTP_cleanup(IN ZOO_HANDLE fd);

    /*
     * @brief HTTP4A_on_request_sig
    **/
     ZOO_INT32 HTTP_on_request_sig(IN HTTP4A_REQUEST_STRUCT * req);

    /*
     * @brief HTTP4A_on_download_file_sig
    **/
     ZOO_INT32 HTTP_on_download_file_sig(IN ZOO_HANDLE fd,
															IN HTTP4A_DOWNLOAD_OPTION_STRUCT * dl_option);

    /*
     * @brief HTTP4A_on_ssl_request_sig
    **/
     ZOO_INT32 HTTP_on_ssl_request_sig(IN HTTP4A_REQUEST_STRUCT * req,
																	IN HTTP4A_SSL_OPTION_STRUCT * ssl_option);

    /*
     * @brief HTTP4A_handle_request
    **/
     ZOO_INT32 HTTP_handle_request(IN ZOO_INT32 id,
												 		IN HTTP4A_RESPONSE_STRUCT * message);

#ifdef __cplusplus
}
#endif
#endif
