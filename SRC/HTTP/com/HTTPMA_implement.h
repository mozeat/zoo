/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : HTTPMA_implement.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-05    Generator      created
*************************************************************/
#ifndef HTTPMA_IMPLEMENT_H
#define HTTPMA_IMPLEMENT_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include <EH4A_if.h>
#include <TR4A_if.h>
#include "HTTPMA_event.h"
#include "HTTP4I_type.h"
#include "HTTP4A_type.h"


/**
 *@brief HTTPMA_implement_4A_create
 *@param fd
**/
ZOO_EXPORT void HTTPMA_implement_4A_create(IN HTTP4I_REPLY_HANDLE reply_handle);
/**
 *@brief HTTPMA_implement_4A_cleanup
 *@param fd
**/
ZOO_EXPORT void HTTPMA_implement_4A_cleanup(IN ZOO_HANDLE fd,
                                                IN HTTP4I_REPLY_HANDLE reply_handle);
/**
 *@brief HTTPMA_implement_4A_on_request_sig
 *@param req
**/
ZOO_EXPORT void HTTPMA_implement_4A_on_request_sig(IN HTTP4A_REQUEST_STRUCT* req,
                                                       IN HTTP4I_REPLY_HANDLE reply_handle);
/**
 *@brief HTTPMA_implement_4A_on_download_file_sig
 *@param fd
 *@param dl_option
**/
ZOO_EXPORT void HTTPMA_implement_4A_on_download_file_sig(IN ZOO_HANDLE fd,
                                                             IN HTTP4A_DOWNLOAD_OPTION_STRUCT* dl_option,
                                                             IN HTTP4I_REPLY_HANDLE reply_handle);
/**
 *@brief HTTPMA_implement_4A_on_ssl_request_sig
 *@param req
 *@param ssl_option
**/
ZOO_EXPORT void HTTPMA_implement_4A_on_ssl_request_sig(IN HTTP4A_REQUEST_STRUCT* req,
                                                           IN HTTP4A_SSL_OPTION_STRUCT* ssl_option,
                                                           IN HTTP4I_REPLY_HANDLE reply_handle);
/**
 *@brief HTTPMA_implement_4A_handle_request
 *@param id
 *@param message
**/
ZOO_EXPORT void HTTPMA_implement_4A_handle_request(IN ZOO_INT32 id,
                                                       IN HTTP4A_RESPONSE_STRUCT* message,
                                                       IN HTTP4I_REPLY_HANDLE reply_handle);

#endif // HTTPMA_implement.h
