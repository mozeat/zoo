/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_SERVER_INTERFACE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef HTTP_SERVER_INTERFACE_H
#define HTTP_SERVER_INTERFACE_H
#include "utils/THREAD_POOL.h"
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/strand.hpp>
#include <boost/config.hpp> 
#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>
#include <thread>
#include <boost/thread.hpp>
#include "SESSION_CLASS.h"
#include "NOTIFY_PROPERTY_CHANGED_INTERFACE.h"
#include "PROPERTY_CHANGED_OBSERVER_INTERFACE.h"
#include "EVENT_PUBLISHER_CLASS.h"

namespace HTTP
{
	class SESSION_CLASS;
}

namespace HTTP
{
    class HTTP_SERVER_INTERFACE: public virtual MARKING_MODEL_INTERFACE,
    									  public virtual CONTROLLER_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       HTTP_SERVER_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~HTTP_SERVER_INTERFACE(){}
    public:
		/*
		 * @brief Run the eventloop.
		**/ 
		virtual void run() = 0;

		/*
		 * @brief Stop the eventloop.
		**/ 
		virtual void stop() = 0;

		/*
		 * @brief Load configuration.
		**/ 
		virtual void initialize() = 0;

		/*
		 * @brief Set event publisher.
		**/ 
		void set_event_publisher(boost::shared_ptr<EVENT_PUBLISHER_CLASS> event_publisher)
		{
			this->m_event_publisher = event_publisher;
		}

		/*
		 * @brief Get evetn publisher.
		**/
		boost::shared_ptr<EVENT_PUBLISHER_CLASS> get_event_publisher()
		{
			return this->m_event_publisher;
		}
		
		/*
		 * @brief Response remote request.
		 * @param session_id   fd
		 * @param message      body
		**/
		virtual void response(IN ZOO_HANDLE session_id,
									IN std::string && message) = 0;

		/*
		 * @brief Add observer will be notified when property changed
		 * @param observer	Property changed observer
		**/
		virtual void add_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* observer)
		{
			//this->remove_observer(observer);
			this->m_observers.emplace_back(observer);
		}

		/*
		 * @brief Remove observer from subscribe list
		**/
		virtual void remove_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* observer)
		{
			// TODO implement clean up later
		}

		/*
		 * @brief Remove observer from subscribe listd
		**/
		virtual void clean()
		{
			this->m_observers.clear();
		}

		/*
		 * @brief Notify property has been changed
		 * @param property_name 	The property has been changed
		**/
		virtual void notify_of_property_changed(const ZOO_UINT32 property_name)
		{
			std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>*>::iterator observer_itr =
                this->m_observers.begin();
	        while (observer_itr != this->m_observers.end())
	        {
	            PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* observer = *observer_itr;
	            observer->on_property_changed(this, property_name);
	            observer_itr++;
	        }
		}

		/*
         * @brief This method is executed when property changed value
         * @param model             The model type
         * @param property_name     The property has been changed value
        **/
        virtual void on_property_changed(MARKING_MODEL_INTERFACE* model,
        											const ZOO_UINT32 property_name) 
        {
        	
        }
    protected:
       	/*
         * @brief The observers.
       	**/ 
       	std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* > m_observers;
		
       	/*
         * @brief The event publisher.
       	**/ 
       	boost::shared_ptr<EVENT_PUBLISHER_CLASS> m_event_publisher;
    };
} //namespace HTTP
#endif
