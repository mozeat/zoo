/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : PROCESSING_FLOW_FACADE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef PROCESSING_FLOW_FACADE_INTERFACE_H
#define PROCESSING_FLOW_FACADE_INTERFACE_H

extern "C" 
{
    #include <ZOO.h>
    #include <HTTP4A_type.h>
}
#include "HTTP_COMMON_MACRO_DEFINE.h"
#include "MARKING_MODEL_INTERFACE.hpp"
#include <boost/shared_ptr.hpp>
#include "FLOW_FACADE_INTERFACE.h"

namespace HTTP
{
    class PROCESSING_FLOW_FACADE_INTERFACE : public virtual FLOW_FACADE_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       PROCESSING_FLOW_FACADE_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~PROCESSING_FLOW_FACADE_INTERFACE(){}
    public:
        /*
         * @brief Create an http client instance
         * @param fd
         * @return error_code
         **/
        virtual ZOO_INT32 create(INOUT ZOO_HANDLE * fd) = 0;

        /*
         * @brief HTTP4A_cleanup
         * @param fd
         * @return error_code
        **/
        virtual ZOO_INT32 cleanup(IN ZOO_HANDLE fd) = 0;

        /*
         * @brief Send a http request
         * @param req
         * @return error_code
        **/
        virtual ZOO_INT32 on_request_sig(IN HTTP4A_REQUEST_STRUCT * req) = 0;

        /*
         * @brief HTTP4A_on_download_file_sig
        **/
        virtual ZOO_INT32 on_download_file_sig(IN ZOO_HANDLE fd,
															IN HTTP4A_DOWNLOAD_OPTION_STRUCT * dl_option) = 0;

        /*
         * @brief Send a http request
         * @param req
         * @param ssl_option
         * @return error_code
        **/
        virtual ZOO_INT32 on_ssl_request_sig(IN HTTP4A_REQUEST_STRUCT* req,
													IN HTTP4A_SSL_OPTION_STRUCT* ssl_option) = 0;

        /*
         * @brief HTTP4A_reply_server
         * @param id
         * @param message
         * @return error_code
        **/
        virtual ZOO_INT32 handle_request(IN ZOO_INT32 id,
												 IN HTTP4A_RESPONSE_STRUCT * message) = 0;
												 
	    /*
		 * @brief Server start to listen incoming request
		 * @return error_code
		**/
        virtual ZOO_INT32 run() = 0;

        /*
		 * @brief Server stop to listen incoming request
		 * @return error_code
		**/
        virtual ZOO_INT32 stop() = 0;
    };
}// namespace HTTP

#endif
