/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : SSL_WEBSOCKET_SESSION_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef SSL_WEBSOCKET_SESSION_CLASS_H
#define SSL_WEBSOCKET_SESSION_CLASS_H
#include "WEBSOCKET_SESSION_CLASS.h"
namespace HTTP
{
    class SSL_WEBSOCKET_SESSION_CLASS :public WEBSOCKET_SESSION_CLASS<SSL_WEBSOCKET_SESSION_CLASS>,
                                                         public std::enable_shared_from_this<SSL_WEBSOCKET_SESSION_CLASS>
    {
    public:
		/*
		 * @brief Constructor
		**/
		explicit SSL_WEBSOCKET_SESSION_CLASS(beast::ssl_stream<beast::tcp_stream> && stream)
		:m_ws(std::move(stream))
        {
        
        }
							
        /*
         * @brief Destructor
        **/ 
        virtual ~SSL_WEBSOCKET_SESSION_CLASS()
        {
            
        }
    public:
    
		// Called by the base class
        websocket::stream<beast::ssl_stream<beast::tcp_stream> > & ws()
        {
            return this->m_ws;
        }

    private:
    	/*
         * @brief The stream.
       	**/ 
		websocket::stream<beast::ssl_stream<beast::tcp_stream> > m_ws;
    };
} //namespace HTTP
#endif
