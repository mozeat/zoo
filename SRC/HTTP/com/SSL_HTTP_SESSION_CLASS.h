/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : SSL_HTTP_SESSION_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef SSL_HTTP_SESSION_CLASS_H
#define SSL_HTTP_SESSION_CLASS_H
#include "HTTP_SESSION_CLASS.h"
namespace HTTP
{
    class SSL_HTTP_SESSION_CLASS : public HTTP_SESSION_CLASS<SSL_HTTP_SESSION_CLASS> 
                                                  public std::enable_shared_from_this<SSL_HTTP_SESSION_CLASS> 
    {
    public:
		/*
		 * @brief Constructor
		**/
		SSL_HTTP_SESSION_CLASS(beast::tcp_stream&& stream,
                                            ssl::context& ctx,
                                            beast::flat_buffer&& buffer,
                                            std::shared_ptr<std::string const> const& doc_root):
                                            HTTP_SESSION_CLASS<SSL_HTTP_SESSION_CLASS>(std::move(buffer),doc_root)
        ,m_stream(std::move(stream), ctx)
        {
        
        }
							
        /*
         * @brief Destructor
        **/ 
        virtual ~SSL_HTTP_SESSION_CLASS()
        {
        
        }
    public:
		// Start the session
        void run()
        {
            // Set the timeout.
            beast::get_lowest_layer(stream_).expires_after(std::chrono::seconds(30));

            // Perform the SSL handshake
            // Note, this is the buffered version of the handshake.
            this->m_stream.async_handshake(
                ssl::stream_base::server,
                this->m_buffer.data(),
                beast::bind_front_handler(
                    &SSL_HTTP_SESSION_CLASS::on_handshake,
                    shared_from_this()));
        }

        // Called by the base class
        beast::ssl_stream<beast::tcp_stream>& stream()
        {
            return this->m_stream;
        }

        // Called by the base class
        beast::ssl_stream<beast::tcp_stream> release_stream()
        {
            return std::move(this->m_stream);
        }

        // Called by the base class
        void  do_eof()
        {
            // Set the timeout.
            beast::get_lowest_layer(this->m_stream).expires_after(std::chrono::seconds(30));

            // Perform the SSL shutdown
            this->m_stream.async_shutdown(
                beast::bind_front_handler(
                    &SSL_HTTP_SESSION_CLASS::on_shutdown,
                    shared_from_this()));
        }

    private:
        void on_handshake(beast::error_code ec,std::size_t bytes_used)
        {
            if(ec)
            {
                __HTTP_LOG_ERROR("%1%",ec.message());
                return;
            }
                

            // Consume the portion of the buffer used by the handshake
            this->m_buffer.consume(bytes_used);

            this->do_read();
        }

        void on_shutdown(beast::error_code ec)
        {
            if(ec)
            {
                __HTTP_LOG_ERROR("%1%",ec.message());
                return;
            }
            // At this point the connection is closed gracefully
        }
		
    private:
   	
    	/*
         * @brief The stream.
       	**/ 
		beast::ssl_stream<beast::tcp_stream> m_stream;
    };
} //namespace HTTP
#endif
