/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_CLIENT_INTERFACE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef HTTP_CLIENT_INTERFACE_H
#define HTTP_CLIENT_INTERFACE_H

extern "C" 
{
    #include <ZOO.h>
    #include <HTTP4A_type.h>
    #include <ZOO_if.h>
}
#include "HTTP_COMMON_MACRO_DEFINE.h"
#include "EVENT_PUBLISHER_CLASS.h"
#include "PROPERTY_CHANGE_KEY_DEFINE.h"
#include "DEVICE_INTERFACE.h"
#include <boost/shared_ptr.hpp>
#include <boost/beast.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/error.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>
#include <memory>
#include "utils/THREAD_POOL.h"
#include "network/uri.hpp"
namespace HTTP
{
    class HTTP_CLIENT_INTERFACE:  public virtual DEVICE_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       HTTP_CLIENT_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~HTTP_CLIENT_INTERFACE(){}
    
    public:

    	/*
		 * @brief Set event publisher.
		**/ 
  		virtual void set_event_publisher(boost::shared_ptr<EVENT_PUBLISHER_CLASS> event_publisher) = 0;

		/*
		 * @brief Set file descriptor.
	    **/ 
		virtual void set_fd(IN ZOO_HANDLE fd) = 0;

		/*
		 * @brief Get file descriptor.
	    **/ 
		virtual ZOO_HANDLE get_fd() = 0;

		/*
		 * @brief run this request.
	    **/ 
		virtual void run(void * req_option) = 0;
    };
}// namespace HTTP

#endif
