/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : DEVICE_CONTROLLER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef DEVICE_CONTROLLER_CLASS_H
#define DEVICE_CONTROLLER_CLASS_H

#include "CONTROLLER_ABSTRACT_CLASS.h"
#include "HTTP_CLIENT_ABSTRACT_MODEL_CLASS.h"
#include "DEVICE_CONTROLLER_INTERFACE.h"
#include <map>
#include <boost/core/ignore_unused.hpp>
#include <boost/nondet_random.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/random.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/thread.hpp>
namespace HTTP
{
    class DEVICE_CONTROLLER_CLASS : public virtual DEVICE_CONTROLLER_INTERFACE 
    										,public virtual CONTROLLER_ABSTRACT_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       DEVICE_CONTROLLER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~DEVICE_CONTROLLER_CLASS();
    public:
		/*
		 * @brief Initialize the model
		 * @return throw exception 
		**/ 
		void initialize();

		/*
		 * @brief Terminate the model
		 * @return throw exception 
		**/ 
		void terminate();

		/*
		 * @brief Get device status.
 		 * @return throw exception 
 		**/ 
		void get_status(INOUT HTTP4A_STATUS_STRUCT * status);

		/*
		 * @brief Create all models by configurations.
		**/ 
		void create_all_models();

		/*
		 * @brief Create an http client instance
		 * @param fd
		 * @return error_code
		 **/
		ZOO_INT32 create(INOUT ZOO_HANDLE * fd);

		/*
		 * @brief HTTP4A_cleanup
		 * @param fd
		 * @return error_code
		**/
		ZOO_INT32 cleanup(IN ZOO_HANDLE fd);

		/*
		 * @brief Send a http request
		 * @param req
		 * @return error_code
		**/
		ZOO_INT32 request(IN HTTP4A_REQUEST_STRUCT *req);

		/*
		 * @brief Send a http request
		 * @param req
		 * @return error_code
		**/
		ZOO_INT32 download(IN ZOO_HANDLE fd,
															IN HTTP4A_DOWNLOAD_OPTION_STRUCT * dl_option);
		/*
		 * @brief Send a http request
		 * @param req
		 * @param ssl_option
		 * @return error_code
		**/
		ZOO_INT32 request(IN HTTP4A_REQUEST_STRUCT * req,
													IN HTTP4A_SSL_OPTION_STRUCT * ssl_option);		

		/*
         * @brief This method is executed when property changed value
         * @param model             The model type
         * @param property_name     The property has been changed value
        **/
        OVERRIDE
        void on_property_changed( MARKING_MODEL_INTERFACE* model,const ZOO_UINT32 property_name);											
    private:
		/*
         * @brief Get an instance of device by fd
        **/ 
        boost::shared_ptr<HTTP_CLIENT_INTERFACE> get_model(IN ZOO_HANDLE fd);
    private:
       /*
        *@brief Define device entities.
       **/ 
       std::map<ZOO_HANDLE,boost::shared_ptr<HTTP_CLIENT_INTERFACE> > m_sessions;

	   /*
	    *@brief The io context attribute.
	   **/ 
       boost::asio::io_context m_ioc;

       /*
	    *@brief once flag.
	   **/ 
	   boost::once_flag m_once_flag;
	   
       /*
	    *@brief Thread pool.
	   **/ 
	   boost::thread_group m_threads;
    };
} //namespace HTTP

#endif
