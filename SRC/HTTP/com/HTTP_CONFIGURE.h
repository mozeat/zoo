/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : HTTP_CONFIGURE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef HTTP_CONFIGURE_H
#define HTTP_CONFIGURE_H

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <utils/ENVIRONMENT_UTILITY_CLASS.h>
#include "EXTERNAL_PROCESS_TARGET_CLASS.h"
#include "INTERNAL_PROCESS_TARGET_CLASS.h"
namespace HTTP
{
    class HTTP_CONFIGURE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       HTTP_CONFIGURE();

       /*
        * @brief Destructor
       **/ 
       virtual ~HTTP_CONFIGURE();
    public:
        /*
         * @brief Get instance
        **/
        static boost::shared_ptr<HTTP_CONFIGURE> get_instance();

        /*
         * @brief Initialize
        **/
        void initialize();

        /*
         * @brief Reload configurations
        **/
        void reload();

        std::string get_listen_addr();
        int get_listen_port();
        int get_server_threads();
        boost::shared_ptr<INTERNAL_PROCESS_TARGET_CLASS> get_internal_target();
        boost::shared_ptr<EXTERNAL_PROCESS_TARGET_CLASS> get_external_target();
        std::string get_root_doc();
    private:
        /*
         * @brief parse configurations
         * @param file      the config file path
        **/
        void parse_configuration(std::string file);
    private:
        /*
         * @brief The instance
        **/
        static boost::shared_ptr<HTTP_CONFIGURE> m_instance;

        /*
         * @brief Execute base path
        **/
        std::string m_execute_path;

        /*
         * @brief address
        **/
        std::string m_listen_addr;
        
        /*
         * @brief address
        **/
        std::string m_root_doc;

        /*
         * @brief port
        **/
        int m_listen_port;
        
        /*
         * @brief Server threads
        **/
        int m_server_threads;

        boost::shared_ptr<INTERNAL_PROCESS_TARGET_CLASS> m_external_target;
        
        boost::shared_ptr<EXTERNAL_PROCESS_TARGET_CLASS> m_internal_target;
    };
} //namespace HTTP
#endif
