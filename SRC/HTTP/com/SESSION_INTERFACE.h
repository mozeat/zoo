/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : SESSION_INTERFACE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef SESSION_INTERFACE_H
#define SESSION_INTERFACE_H
extern "C" 
{
    #include <ZOO.h>
    #include <HTTP4A_type.h>
}
#include <boost/shared_ptr.hpp>
#include <boost/beast.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>
#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>
#include "MARKING_MODEL_INTERFACE.h"
#include "CONTROLLER_INTERFACE.h"
#include "PROPERTY_CHANGE_KEY_DEFINE.h"
#include "network/uri.hpp"

namespace HTTP
{
    class SESSION_INTERFACE : public      MARKING_MODEL_INTERFACE
    								,public virtual NOTIFY_PROPERTY_CHANGED_INTERFACE<MARKING_MODEL_INTERFACE>
    {
    public:
		/*
		 * @brief Constructor
		**/
		SESSION_INTERFACE(){}
							
    
        /*
         * @brief Destructor
        **/ 
        virtual ~SESSION_INTERFACE(){}
    public:
		/*
		 * @brief Start the asynchronous operation.
		**/ 
		virtual void run() = 0;

		/*
		 * @brief Response to the req.
		 * @param error_code      reply error_code 
		 * @param reply_message   data
		**/ 
		virtual  void do_write(std::string && body) = 0;

		/*
		 * @brief Get file descriptor of the socket.
		**/ 
		virtual  ZOO_HANDLE get_fd() = 0;

		/*
		 * @brief Get file descriptor of the socket.
		**/ 
		virtual boost::shared_ptr<HTTP4A_REQUEST_STRUCT> get_message() = 0;
    };
} //namespace HTTP
#endif
