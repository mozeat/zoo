/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : COMMON_FLOW_FACADE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef COMMON_FLOW_FACADE_CLASS_H
#define COMMON_FLOW_FACADE_CLASS_H

#include "COMMON_FLOW_FACADE_INTERFACE.h"
#include "FLOW_FACADE_ABSTRACT_CLASS.h"

namespace HTTP
{
    class COMMON_FLOW_FACADE_CLASS : virtual public COMMON_FLOW_FACADE_INTERFACE,
                                         public FLOW_FACADE_ABSTRACT_CLASS 
    {
    public:
       /*
        * @brief Constructor
       **/ 
       COMMON_FLOW_FACADE_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~COMMON_FLOW_FACADE_CLASS();
    public:
       /*
        * @brief Initialize the model
        * @return error_code 
       **/ 
       void initialize();

       /*
        * @brief Terminate the model
        * @return error_code 
       **/ 
       void terminate();

       /*
        * @brief Get driver state
        * @return state 
       **/ 
       ZOO_DRIVER_STATE_ENUM get_driver_state();

       /*
        * @brief Get device status.
        * @return error code 
       **/ 
       ZOO_INT32 get_status(INOUT HTTP4A_STATUS_STRUCT * status);

       /*
        * @brief Change to debug mode
       **/ 
       ZOO_INT32 change_to_debug_mode();

       /*
        * @brief Change to work mode.The driver state is BUSY.
       **/ 
       ZOO_INT32 change_to_work_mode();

       /*
        * @brief This method is executed when property changed value
        * @param model             The model type
        * @param property_name     The property has been changed value
       **/ 
       OVERRIDE
       void on_property_changed(CONTROLLER_INTERFACE * model,
								       const ZOO_UINT32 property_name);

    };
}//namespace HTTP

#endif
