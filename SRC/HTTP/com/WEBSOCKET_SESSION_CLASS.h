/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : HTTP
 * File Name      : WEBSOCKET_SESSION_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-31    Generator      created
*************************************************************/
#ifndef WEBSOCKET_SESSION_CLASS_H
#define WEBSOCKET_SESSION_CLASS_H
#include "HTTP_COMMON_MACRO_DEFINE.h"
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/strand.hpp>
#include <boost/make_unique.hpp>
#include <boost/optional.hpp>
#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>

namespace beast = boost::beast;                 // from <boost/beast.hpp>
namespace http = beast::http;                   // from <boost/beast/http.hpp>
namespace websocket = beast::websocket;         // from <boost/beast/websocket.hpp>
namespace net = boost::asio;                    // from <boost/asio.hpp>
namespace ssl = boost::asio::ssl;               // from <boost/asio/ssl.hpp>
using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>

namespace HTTP
{
    // Echoes back all received WebSocket messages.
    // This uses the Curiously Recurring Template Pattern so that
    // the same code works with both SSL streams and regular sockets.
    template<class T>
    class WEBSOCKET_SESSION_CLASS
    {
    public:
        // Start the asynchronous operation
        template<class Body, class Allocator>
        void run(http::request<Body, http::basic_fields<Allocator>> req)
        {
            // Accept the WebSocket upgrade request
            this->do_accept(std::move(req));
        }
        
    private:
        // Access the derived class, this is part of
        // the Curiously Recurring Template Pattern idiom.
        T & derived()
        {
            return static_cast<T&>(*this);
        }

        // Start the asynchronous operation
        template<class Body, class Allocator>
        void do_accept(http::request<Body, http::basic_fields<Allocator>> req)
        {
            // Set suggested timeout settings for the websocket
            this->derived().ws().set_option(
                websocket::stream_base::timeout::suggested(
                    beast::role_type::server));

            // Set a decorator to change the Server of the handshake
            this->derived().ws().set_option(
                websocket::stream_base::decorator(
                [](websocket::response_type& res)
                {
                    res.set(http::field::server,std::string("ZOO logistics center"));
                }));

            // Accept the websocket handshake
            this->derived().ws().async_accept(
                req,
                beast::bind_front_handler(
                    &WEBSOCKET_SESSION_CLASS::on_accept,
                    derived().shared_from_this()));
        }

        void on_accept(beast::error_code ec)
        {
            if(ec)
            {
                __HTTP_LOG_ERROR("%1%",ec.message());
            }
            else
            {
                // Read a message
                this->do_read();
            }
        }

        void do_read()
        {
            // Read a message into our buffer
            this->derived().ws().async_read(
                this->m_buffer,
                beast::bind_front_handler(
                    &WEBSOCKET_SESSION_CLASS::on_read,
                    derived().shared_from_this()));
        }

        void on_read(beast::error_code ec,std::size_t bytes_transferred)
        {
            boost::ignore_unused(bytes_transferred);

            // This indicates that the WEBSOCKET_SESSION_CLASS was closed
            if(ec == websocket::error::closed)
                return;

            if(ec)
                __HTTP_LOG_ERROR("%1%",ec.message());

            // Echo the message
            derived().ws().text(derived().ws().got_text());
            derived().ws().async_write(
                this->m_buffer.data(),
                beast::bind_front_handler(
                    &WEBSOCKET_SESSION_CLASS::on_write,
                    derived().shared_from_this()));
        }

        void on_write(beast::error_code ec,std::size_t bytes_transferred)
        {
            boost::ignore_unused(bytes_transferred);

            if(ec)
            {
                __HTTP_LOG_ERROR("%1%",ec.message());
                return;
            }

            // Clear the buffer
            this->m_buffer.consume(this->m_buffer.size());

            // Do another read
            this->do_read();
        }
    private:
    
        beast::flat_buffer m_buffer;
    };
} //namespace HTTP
#endif
