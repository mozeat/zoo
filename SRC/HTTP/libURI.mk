include ../Makefile_tpl_cov
TARGET   := libURI.so
SRCEXTS  := .cpp 
INCDIRS  := ./com ./lib
SOURCES  := ./lib/network/uri.cpp \
			./lib/network/uri_builder.cpp \
			./lib/network/uri_errors.cpp \
			./lib/network/detail/uri_advance_parts.cpp \
			./lib/network/detail/uri_normalize.cpp \
			./lib/network/detail/uri_parse.cpp \
			./lib/network/detail/uri_parse_authority.cpp \
			./lib/network/detail/uri_resolve.cpp 
			
SRCDIRS  :=  
CFLAGS   := 
CXXFLAGS := -std=c++14
CPPFLAGS := $(GCOV_FLAGS) -fPIC
LDFLAGS  := $(GCOV_LINK)  -lnsl -shared

include ../Project_config
include ../Makefile_tpl_linux