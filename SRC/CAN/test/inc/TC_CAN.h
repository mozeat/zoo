/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : CC
 * File Name      : CC_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-02-15    Generator      created
*************************************************************/
extern "C"
{
    #include <CAN4A_if.h>
    #include <CAN4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_CAN_001)

    BOOST_AUTO_TEST_CASE( TC_CAN_001_001 )
    {
        CAN4A_CHANNEL_ID_ENUM channel = CAN_CHANNEL_ID_0;
		BOOST_TEST(CAN4A_get_state(channel) == CAN4A_STATE_TERMINATE);
        BOOST_TEST(CAN4A_initialize(channel,50000,1000) == OK);
        BOOST_TEST(CAN4A_get_state(channel) == CAN4A_STATE_IDLE);
		BOOST_TEST(CAN4A_terminate(channel));
		BOOST_TEST(CAN4A_get_state(channel) == CAN4A_STATE_TERMINATE);
    }

BOOST_AUTO_TEST_SUITE_END()