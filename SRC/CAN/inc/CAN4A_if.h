/*******************************************************************************
* Copyright (C) 2017, 上海蔚来汽车有限公司
* All rights reserved.
* 产品号 : ZOO
* 所属组件 : CC
* 模块名称 : 
* 文件名称 : CC_if.h
* 概要描述 : can通道使用描述
* 历史记录 :
* 版本		日期		    作者			    内容
* V1.0		2018.12.18	weiwang.sun		    创建
*******************************************************************************/
#ifndef CAN4A_IF_H_
#define CAN4A_IF_H_

#include <ZOO.h>
#include "CAN4A_type.h"

/**************************************************************************
INTERFACE <ZOO_INT32 CAN4A_initialize>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/callback_function>
<Parameters>
    IN:         CAN4A_CHANNEL_ID_ENUM channel -can通道
    IN:         ZOO_INT32 bitrate  -- 波特率
    IN:         ZOO_INT32 bus_off_restart_time --can bus off 时 重启超时时间
    OUT:        notype none
    INOUT:      notype none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- SUCCESS
                CAN4A_SYSTEM_ERR            -- system error
                CAN4A_PARAMETER_ERR         -- parameters error
<Description>:  初始化can口，打开can通道，建立socket连接
				PRECONDITION: 无
				POSTCONDITION: 无
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CAN4A_initialize(IN CAN4A_CHANNEL_ID_ENUM channel,
											 IN ZOO_INT32 bitrate,
											 IN ZOO_INT32 bus_off_restart_time);

/**************************************************************************
INTERFACE <ZOO_INT32 CAN4A_terminate>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/callback_function>
<Parameters>
    IN:         CAN4A_CHANNEL_ID_ENUM channel -can通道
    OUT:        notype none
    INOUT:      notype none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- SUCCESS
                CAN4A_SYSTEM_ERR            -- system error
                CAN4A_PARAMETER_ERR         -- parameters error
<Description>:  关闭can通道，停止收发数据，并设置状态机为CAN4A_STATE_TERMINATE
				PRECONDITION:
				POSTCONDITION:
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CAN4A_terminate(IN CAN4A_CHANNEL_ID_ENUM channel);

/**************************************************************************
INTERFACE <ZOO_INT32 CAN4A_get_state>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/callback_function>
<Parameters>
    IN:         CAN4A_CHANNEL_ID_ENUM channel -can通道
    OUT:        notype none
    INOUT:      notype none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- SUCCESS
                CAN4A_SYSTEM_ERR            -- system error
                CAN4A_PARAMETER_ERR         -- parameters error
<Description>:  关闭can通道，停止收发数据，并设置状态机为CAN4A_STATE_TERMINATE
				PRECONDITION:
				POSTCONDITION:
**************************************************************************/
ZOO_EXPORT CAN4A_STATE_ENUM CAN4A_get_state(IN CAN4A_CHANNEL_ID_ENUM channel);

/**************************************************************************
INTERFACE <ZOO_INT32 CAN4A_set_filter>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/callback_function>
<Parameters>
    IN:         CAN4A_CHANNEL_ID_ENUM channel       -can通道
    IN:         const CAN4A_FILTER_STRUCT * filter  -can过滤结构体
    IN:         ZOO_INT32 filter_len                -can过滤结构体长度
    OUT:        notype none
    INOUT:      notype none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- SUCCESS
                CAN4A_SYSTEM_ERR            
                CAN4A_PARAMETER_ERR         
<Description>:  设置can口数据过滤参数
				PRECONDITION:can已经初始化成功
				POSTCONDITION:
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CAN4A_set_filter(IN CAN4A_CHANNEL_ID_ENUM channel,IN const CAN4A_FILTER_STRUCT * filter,IN ZOO_INT32 filter_len);

/**************************************************************************
INTERFACE <ZOO_INT32 CAN4A_disable_filter>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/callback_function>
<Parameters>
    IN:         CAN4A_CHANNEL_ID_ENUM channel
    OUT:        notype none
    INOUT:      notype none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- SUCCESS
                CAN4A_SYSTEM_ERR         -- system error
                CAN4A_PARAMETER_ERR      -- parameters error
<Description>:  禁止过滤can消息  
				PRECONDITION: can已经初始化成功
				POSTCONDITION:
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CAN4A_disable_filter(IN CAN4A_CHANNEL_ID_ENUM channel);

/**************************************************************************
INTERFACE <ZOO_INT32 CAN4A_subscribe_message>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/callback_function>
<Parameters>
    IN:         CAN4A_CHANNEL_ID_ENUM id
    OUT:        notype none
    INOUT:      notype none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- SUCCESS
				CAN4A_ILLEGAL_CALL_ERROR --illegal call
                CAN4A_SYSTEM_ERR -- system error
                CAN4A_TIMEOUT_ERR -- timeout error
                CAN4A_PARAMETER_ERR -- parameters error
<Description>:  订阅can主动上送的数据      
				PRECONDITION: can已经初始化成功
				POSTCONDITION:
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CAN4A_subscribe_message(IN CAN4A_CHANNEL_ID_ENUM channel,
														IN CAN4A_MESSAGE_CALLBACK callback,
														INOUT ZOO_HANDLE * handle);

/**************************************************************************
INTERFACE <ZOO_INT32 CAN4A_subscribe_message>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/callback_function>
<Parameters>
    IN:         CAN4A_CHANNEL_ID_ENUM id
    OUT:        notype none
    INOUT:      notype none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- SUCCESS
				CAN4A_ILLEGAL_CALL_ERROR --illegal call
                CAN4A_SYSTEM_ERR -- system error
                CAN4A_TIMEOUT_ERR -- timeout error
                CAN4A_PARAMETER_ERR -- parameters error
<Description>:  订阅can主动上送的数据      
				PRECONDITION: can已经初始化成功
				POSTCONDITION:
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CAN4A_unsubscribe_message(IN CAN4A_CHANNEL_ID_ENUM channel,IN ZOO_HANDLE  handle);

/**************************************************************************
INTERFACE <ZOO_INT32 CAN4A_start_event_loop>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/callback_function>
<Parameters>
    IN:         CAN4A_CHANNEL_ID_ENUM id
    OUT:        notype none
    INOUT:      notype none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- SUCCESS
				CAN4A_ILLEGAL_CALL_ERROR --illegal call
                CAN4A_SYSTEM_ERR -- system error
                CAN4A_PARAMETER_ERR -- parameters error
<Description>:  开始进入接收can消息事件循环    
				PRECONDITION: can已经初始化成功
				POSTCONDITION:
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CAN4A_start_event_loop(IN CAN4A_CHANNEL_ID_ENUM channel);

/**************************************************************************
INTERFACE <ZOO_INT32 CAN4A_start_event_loop>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/callback_function>
<Parameters>
    IN:         CAN4A_CHANNEL_ID_ENUM id
    OUT:        notype none
    INOUT:      notype none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- SUCCESS
                CAN4A_SYSTEM_ERR -- system error
                CAN4A_PARAMETER_ERR -- parameters error
<Description>:  停止接收can消息事件循环    
				PRECONDITION: can已经初始化成功
				POSTCONDITION:
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CAN4A_stop_event_loop(IN CAN4A_CHANNEL_ID_ENUM channel);	
				
/**************************************************************************
INTERFACE <ZOO_INT32 CAN4A_recv_message>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/callback_function>
<Parameters>
    IN:         CAN4A_CHANNEL_ID_ENUM id
    OUT:        notype none
    INOUT:      notype none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- SUCCESS
				CAN4A_ILLEGAL_CALL_ERROR --illegal call
                CAN4A_SYSTEM_ERR -- system error
                CAN4A_TIMEOUT_ERR -- timeout error
                CAN4A_PARAMETER_ERR -- parameters error
<Description>:  主动接收can消息      
				PRECONDITION: can已经初始化成功
				POSTCONDITION:
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CAN4A_recv_message(IN CAN4A_CHANNEL_ID_ENUM channel,INOUT CAN4A_MESSAGE_STRUCT * msg);
								 
/*************************************************************************
INTERFACE <ZOO_INT32 CAN4A_stop_charging>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/callback_function>
<Parameters>
    IN:         CAN4A_CHANNEL_ID_ENUM id
				const CAN4A_MESSAGE_STRUCT * msg  -can消息结构体
    OUT:        notype none
    INOUT:      notype none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- SUCCESS
				CAN4A_ILLEGAL_CALL_ERROR --illegal call
                CAN4A_SYSTEM_ERR -- system error
                CAN4A_PARAMETER_ERR -- parameters error
<Description>:  发送can消息      
				PRECONDITION: can已经初始化成功
				POSTCONDITION:
**************************************************************************/
ZOO_EXPORT ZOO_INT32 CAN4A_send_message(IN CAN4A_CHANNEL_ID_ENUM channel,IN const CAN4A_MESSAGE_STRUCT * msg);
			
#endif 
