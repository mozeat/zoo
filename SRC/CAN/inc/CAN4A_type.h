/*******************************************************************************
* Copyright (C) 2017, 上海蔚来汽车有限公司
* All rights reserved.
* 产品号 : ZOO
* 所属组件 : CAN4A
* 模块名称 : 
* 文件名称 : CAN4A_type.h
* 概要描述 : 接口描述
* 历史记录 :
* 版本		日期		    作者			    内容
* V1.0		2018.3.23	weiwang.sun		    创建
*******************************************************************************/
#ifndef CAN4A_TYPE_H_
#define CAN4A_TYPE_H_
#include <ZOO.h>

/**
 *@brief Error Code Definition
*/
#define CAN4A_BASE_ERR                     (0x43430000)
#define CAN4A_SYSTEM_ERR                   ((CAN4A_BASE_ERR) + 0x01)
#define CAN4A_PARAMETER_ERR                ((CAN4A_BASE_ERR) + 0x02)
#define CAN4A_IILEGAL_CALL_ERR             ((CAN4A_BASE_ERR) + 0x03)



/**
 *@brief Definition
*/
#define CAN4A_BUFFER_LENGTH                32
#define CAN4A_BMS_MODULE_NUBMER_MAX        32

/* can通道枚举定义 */
typedef enum
{
	CAN_CHANNEL_ID_MIN  = -1,
	CAN_CHANNEL_ID_0 = 0,
	CAN_CHANNEL_ID_1 = 1,
	CAN_CHANNEL_ID_2 = 2,
	CAN_CHANNEL_ID_MAX
}CAN4A_CHANNEL_ID_ENUM;

/* can通道枚举定义 */
typedef enum
{
	CAN4A_STATE_MIN  = -1,
	CAN4A_STATE_TERMINATE = 0,//刚上电的状态
	CAN4A_STATE_IDLE = 1,//初始化成功后的状态，并且没有数据收发
	CAN4A_STATE_ERROR = 2,//can或者socket报错状态，无法完成收发数据
	CAN4A_STATE_BUSY = 3,//正在进行收发数据
	CAN4A_STATE_MAX
}CAN4A_STATE_ENUM;

/* can过滤数据结构定义 */
typedef struct 
{
	ZOO_UINT32 can_id;
	ZOO_UINT32 can_mask;
}CAN4A_FILTER_STRUCT;

/* can过滤数据结构定义 */
typedef struct 
{
	ZOO_UINT32 can_id;
	ZOO_CHAR *  data;
	ZOO_UINT32  payload;
}CAN4A_MESSAGE_STRUCT;


typedef void(*CAN4A_MESSAGE_CALLBACK)(IN CAN4A_MESSAGE_STRUCT msg,IN CAN4A_STATE_ENUM state);
#endif 
