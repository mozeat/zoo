/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CC
 * File Name    : MARKING_CODE_INTERFACE.h
 * Description  : This class is used to support synchronization between
 *                  thread via events.
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-05-18			MOZEAT		Initialize
 ***************************************************************/
#ifndef MARKING_CODE_INTERFACE_H_
#define MARKING_CODE_INTERFACE_H_

#include <string>
namespace ZOO_CAN
{
    /**
     * @brief This class is used to connect with can interface.
     */
    class MARKING_CODE_INTERFACE
    {
    public:

        /**
         * @brief Deconstructor
         */
        virtual ~MARKING_CODE_INTERFACE(){}
   public:
        /**
         * @brief Set marking code 
         */
        virtual void set_marking_code(std::string marking_code)
    	{
    		this->m_marking_code = marking_code;
    	}
		
        /**
         * @brief Get marking code 
         */
        virtual const std::string & get_marking_code()
    	{
    		return this->m_marking_code;
    	}
	private:
		std::string m_marking_code;
    };
} /* namespace ZOO_CAN */
#endif /* MARKING_CODE_INTERFACE_H_ */

