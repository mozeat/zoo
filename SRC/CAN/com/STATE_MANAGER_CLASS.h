/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CC
 * File Name    : STATE_MANAGER_CLASS.h
 * Description  : This class is used to manage can model state.
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-05-18			MOZEAT		Initialize
 ***************************************************************/
#ifndef STATE_MANAGER_CLASS_H_
#define STATE_MANAGER_CLASS_H_
extern "C"
{
#include <ZOO.h>
#include <CAN4A_type.h>
}
#include <boost/thread.hpp>
namespace ZOO_CAN
{
    /**
     * @brief This class is used to manage can model state.
     */
    class STATE_MANAGER_CLASS
    {
    public:
		/**
         * @brief Constructor
         */
        STATE_MANAGER_CLASS();
		
        /**
         * @brief Destructor
         */
        virtual ~STATE_MANAGER_CLASS();
    public:
        /**
         * @brief Set state 
         */
        void set_state(IN CAN4A_STATE_ENUM state);
		
		/**
	 	 * @brief Get state 
	 	 */
	 	const CAN4A_STATE_ENUM & get_state();
	private:
		/**
		 * @brief The sync lock for subscriber
		 */
		 boost::mutex m_sync_lock;
		
		/**
	 	 * @brief The state of can model
	 	 */
		CAN4A_STATE_ENUM m_state;
    };
} /* namespace ZOO_CAN */
#endif /* EVENT_NOTIFY_INTERFACE_H_ */
