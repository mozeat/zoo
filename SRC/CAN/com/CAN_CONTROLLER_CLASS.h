/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CC
 * File Name    : CAN_CONTROLLER_CLASS.h
 * Description  : This class is used to shandle model event.
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-05-18			MOZEAT		Initialize
 ***************************************************************/
#ifndef CAN_CONTROLLER_CLASS_H_
#define CAN_CONTROLLER_CLASS_H_
#include "CAN_HARDWARE_SERVICE_CLASS.h"
#include "MARKING_CODE_INTERFACE.h"
#include <map>
namespace ZOO_CAN
{
	
    /**
     * @brief This class is used to handle model event.
     */
    class CAN_CONTROLLER_CLASS
    {
    public:

		/**
         * @brief Constructor
         */
		CAN_CONTROLLER_CLASS();
		
        /**
         * @brief Deconstructor
         */
        virtual ~CAN_CONTROLLER_CLASS();
    public:
    
        /**
         * @brief Initialize can ,set can up and birate prepare for recving msg
         * @param channel              -can0 can1 ...
         * @param bitrate              -communication baudrate
         * @param bus_off_restart_time -reboot can driver timeout
         * @return error code 
         */
         ZOO_INT32 initialize(IN CAN4A_CHANNEL_ID_ENUM channel,
											 IN ZOO_INT32 bitrate,
											 IN ZOO_INT32 bus_off_restart_time);
		/**
         * @brief Shutdown can channel,and set the state  to TERMINATE
         * @param channel              -can0 can1 ...
         * @return error code 
         */
         ZOO_INT32 terminate(IN CAN4A_CHANNEL_ID_ENUM channel);
		
		/**
         * @brief Get can channel curretn state
         * @param channel              -can0 can1 ...
         * @return state
         */
		 CAN4A_STATE_ENUM get_state(IN CAN4A_CHANNEL_ID_ENUM channel);
		 
		/**
         * @brief Set filter param for recving can message
         * @param channel              -can0 can1 ...
         * @param filter               -filter
         * @param filter_len           -number of filters
         * @return error code 
         */
		 ZOO_INT32 set_filter(IN CAN4A_CHANNEL_ID_ENUM channel,
		 							IN const CAN4A_FILTER_STRUCT * filter,
		 							IN ZOO_INT32 filter_len);
		 
		/**
         * @brief Disable filter
         * @param channel              -can0 can1 ...
         * @return error code 
         */
		 ZOO_INT32 disable_filter(IN CAN4A_CHANNEL_ID_ENUM channel);

		/**
         * @brief Subscribe can message
         * @param channel              -can0 can1 ...
         * @param callback             -callback function 
         * @return error code 
         */
		 ZOO_HANDLE subscribe(IN CAN4A_CHANNEL_ID_ENUM channel,
		 							IN CAN4A_MESSAGE_CALLBACK callback);

		/**
         * @brief Subscribe can message
         * @param channel              -can0 can1 ...
         * @param callback             -callback function 
         * @return error code 
         */
		 ZOO_INT32 unsubscribe(IN CAN4A_CHANNEL_ID_ENUM channel,IN ZOO_HANDLE handle);

		/**
         * @brief Start to recv can message from socket
         * @param channel              -can0 can1 ...
         * @return error code 
         */							
		 ZOO_INT32 start_event_loop(IN CAN4A_CHANNEL_ID_ENUM channel);

		/**
         * @brief Stop to recv can message from socket
         * @param channel              -can0 can1 ...
         * @return error code 
         */							
		 ZOO_INT32 stop_event_loop(IN CAN4A_CHANNEL_ID_ENUM channel);	

		/**
         * @brief Stop to recv can message from socket
         * @param channel              -can0 can1 ...
         * @param msg                  
         * @return error code 
         */							
		 ZOO_INT32 recv_message(IN CAN4A_CHANNEL_ID_ENUM channel,INOUT CAN4A_MESSAGE_STRUCT * msg);

		/**
         * @brief Stop to recv can message from socket
         * @param channel              -can0 can1 ...
         * @return error code 
         */							
		 ZOO_INT32 send_message(IN CAN4A_CHANNEL_ID_ENUM channel,IN CAN4A_MESSAGE_STRUCT * msg);
	private:
		/**
         * @brief Create an can  model by channel
         * @param channel              -can0 can1 ...        
         * @return  
         */	
		void create_model(IN CAN4A_CHANNEL_ID_ENUM channel);
		
		/**
         * @brief Check can model existence
         * @param channel              -can0 can1 ...        
         * @return  
         */		
		ZOO_BOOL contain_model(IN CAN4A_CHANNEL_ID_ENUM channel);

		/**
         * @brief Get can model instance
         * @param channel              -can0 can1 ...        
         * @return  can model
         */	
		boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> get_model(IN CAN4A_CHANNEL_ID_ENUM channel);
		
	private:
		 std::map<int,boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> > m_can_models;	
    };
} /* namespace ZOO_CAN */
#endif /* CAN_CONTROLLER_CLASS_H_ */
