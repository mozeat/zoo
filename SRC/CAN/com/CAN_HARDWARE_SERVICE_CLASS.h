/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CC
 * File Name    : CAN_HARDWARE_SERVICE_CLASS.h
 * Description  : This class is used to support synchronization between
 *                  thread via events.
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-05-18			MOZEAT		Initialize
 ***************************************************************/
#ifndef CAN_HARDWARE_SERVICE_CLASS_H_
#define CAN_HARDWARE_SERVICE_CLASS_H_
extern "C"
{
    #include <ZOO.h>
    #include <CAN4A_type.h>
}

#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>
#include <sys/time.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/select.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <chrono>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/nondet_random.hpp>
#include <boost/random.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/process.hpp>
#include "CAN_COMMON_MACRO_DEFINE.h"
#include "STATE_MANAGER_CLASS.h"
namespace ZOO_CAN
{
    /**
     * @brief This class is used to connect with can interface.
     */
    class CAN_HARDWARE_SERVICE_CLASS 
    {
    public:
		 /**
         * @brief Default constructor
         */
        CAN_HARDWARE_SERVICE_CLASS(CAN4A_CHANNEL_ID_ENUM channel);
		 
        /**
         * @brief Deconstructor
         */
        virtual ~CAN_HARDWARE_SERVICE_CLASS();
   public:
        /**
         * @brief Startup can channel
         * @param bitrate
         * @param bus_off_restart_time 
         */
        void initialize(IN ZOO_INT32 bitrate,
							IN ZOO_INT32 bus_off_restart_time);

		/**
         * @brief Shutdown can channel and stop event loop
         */
        void terminate();

		/**
	 	 * @brief Get state 
	 	 */
		CAN4A_STATE_ENUM get_state();

		/**
         * @brief Add subscriber
         * @param callback
         * @return the identity of subscriber
         */
		ZOO_HANDLE add_subsriber(CAN4A_MESSAGE_CALLBACK callback);

		/**
         * @brief Add subscriber
         * @param fd           --the identity of subscriber
         */
		void remove_subsriber(IN ZOO_HANDLE       handle);
		
		/**
         * @brief Set filter option
         * @param can_mask   - 
         */
		void set_filter_option(IN CAN4A_FILTER_STRUCT * filter,
		 							IN ZOO_INT32 filter_counts);

		/**
         * @brief Disable filter 
         */
		void disable_filter();
		
        /**
         * @brief Write message to can device
         * @param msg        - message
         */
        void send(IN boost::shared_ptr<CAN4A_MESSAGE_STRUCT> message); 

		/**
         * @brief Recv message from can device
         * @param msg        - message
         */
        void recv(IN boost::shared_ptr<CAN4A_MESSAGE_STRUCT> message);

		/**
         * @brief Start recv message from can device
         */
		void start_event_loop();

		/**
         * @brief Stop recv message from can device
         */
		void stop_event_loop();
	private:
		/**
         * @brief Startup can
         */
		void startup();

		/**
         * @brief Shutdown can
         */
		void shutdown();	

		/**
         * @brief Set can option
         */
		void set_option(IN ZOO_INT32 bitrate,
									IN ZOO_INT32 bus_off_restart_time);	
		
		/**
         * @brief Create socket
         */
		ZOO_FD create_socket();

		/**
         * @brief Close socket
         */
		void close_socket(IN ZOO_FD fd);

		/**
         * @brief Create ssid
         */
		ZOO_HANDLE create_ssid();
		
		/**
         * @brief converter can channel to string
         */
		std::string to_str(CAN4A_CHANNEL_ID_ENUM channel);
    private:
		
		/**
         * @brief The can channel
         */
		CAN4A_CHANNEL_ID_ENUM m_channel;
		
		/**
         * @brief The socket fd
         */
        ZOO_FD m_socket_fd;

		/**
         * @brief The recv message stop flag
         */
        ZOO_BOOL m_stopped;

		/**
         * @brief The can channel string
         */
		std::string m_channel_string;
			
		/**
         * @brief The state manager
         */
		boost::shared_ptr<STATE_MANAGER_CLASS> m_state_manager;

		/**
		 * @brief The sync lock for subscriber
		 */
		 boost::mutex m_sync_lock;
		
		/**
         * @brief The subcribers map
         */
		std::map<ZOO_HANDLE,CAN4A_MESSAGE_CALLBACK> m_subscribers;
    };
} /* namespace ZOO_CAN */
#endif /* CAN_HARDWARE_SERVICE_CLASS_H_ */
