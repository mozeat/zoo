/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CC
 * File Name    : STATE_MANAGER_CLASS.cpp
 * Description  : This class is used to manage can model state.
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-05-18			MOZEAT		Initialize
 ***************************************************************/
#include "STATE_MANAGER_CLASS.h"
namespace ZOO_CAN
{
    /**
     * @brief Default constructor
     */
    STATE_MANAGER_CLASS::STATE_MANAGER_CLASS():m_state(CAN4A_STATE_IDLE)
	{
		
	}

    /**
     * @brief Destructor
     */
    STATE_MANAGER_CLASS::~STATE_MANAGER_CLASS()
	{
		
	}
	
    /**
     * @brief Set state 
     */
    void STATE_MANAGER_CLASS::set_state(IN CAN4A_STATE_ENUM state)
	{
		boost::unique_lock<boost::mutex> lock(this->m_sync_lock);
		if(this->m_state != state)
			this->m_state = state;
	}
		
	/**
 	 * @brief Get state 
 	 */
 	const CAN4A_STATE_ENUM & STATE_MANAGER_CLASS::get_state()
	{
		return this->m_state;
	}
} /* namespace ZOO_CC */
