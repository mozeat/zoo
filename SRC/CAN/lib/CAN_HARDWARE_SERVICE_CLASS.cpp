/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CC
 * File Name    : CAN_HARDWARE_SERVICE_CLASS.h
 * Description  : This class is used to support synchronization between
 *                  thread via events.
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-05-18			MOZEAT		Initialize
 ***************************************************************/
#include "CAN_HARDWARE_SERVICE_CLASS.h"

namespace ZOO_CAN
{
	/**
     * @brief Define the invalid file descriptor
     */
	static const ZOO_INT32 INVALID_FD = -1;
	
	/**
     * @brief Default constructor
     */
    CAN_HARDWARE_SERVICE_CLASS::CAN_HARDWARE_SERVICE_CLASS(CAN4A_CHANNEL_ID_ENUM channel)
    :m_channel(channel),m_socket_fd(INVALID_FD),m_stopped(ZOO_FALSE)
	{
		this->m_channel_string = this->to_str(channel);
		this->m_state_manager.reset(new STATE_MANAGER_CLASS());
	}

    /**
     * @brief Deconstructor
     */
    CAN_HARDWARE_SERVICE_CLASS::~CAN_HARDWARE_SERVICE_CLASS()
	{
		try
		{
			this->terminate();
		}
		catch(...)
		{
			
		}
	}

    /**
     * @brief Connect to can interface
     * @param channel       - can interface
     * @param channel       - communication baudrate
     */
    void CAN_HARDWARE_SERVICE_CLASS::initialize(IN ZOO_INT32 bitrate,
													IN ZOO_INT32 bus_off_restart_time)
	{
		/*TR4A_trace("can", __FUNCTION_NAME__,
						"function entry > can[%s],bitrate[%d],restart_timeout[%d]",
						this->m_channel_string.data(),bitrate,bus_off_restart_time);*/
		this->stop_event_loop();
		this->close_socket(this->m_socket_fd);
		this->shutdown();
		this->set_option(bitrate,bus_off_restart_time);
		this->startup();
		this->m_socket_fd = this->create_socket();
		if(this->m_socket_fd == INVALID_FD)
		{
			__THROW_CAN_EXCEPTION(CAN4A_SYSTEM_ERR, "Creat socket failed.", nullptr);	
		}
		this->m_state_manager->set_state(CAN4A_STATE_IDLE);
		/*TR4A_trace("can", __FUNCTION_NAME__,"function exit <");*/
	}

	/**
     * @brief Connect to can interface
     */
    void CAN_HARDWARE_SERVICE_CLASS::terminate()
	{
		/*TR4A_trace("can", __FUNCTION_NAME__,
						"function entry > can[%s]",this->m_channel_string.data());*/
		this->stop_event_loop();
		this->close_socket(this->m_socket_fd);
		this->shutdown();
		this->m_subscribers.clear();
		this->m_state_manager->set_state(CAN4A_STATE_TERMINATE);
		/*TR4A_trace("can", __FUNCTION_NAME__,"function exit <");*/
	}

	/**
 	 * @brief Get state 
 	 */
	CAN4A_STATE_ENUM CAN_HARDWARE_SERVICE_CLASS::get_state()
	{
		return this->m_state_manager->get_state();
	}

	/**
     * @brief Add subscriber
     * @param callback
     * @return the identity of subscriber
     */
	ZOO_HANDLE CAN_HARDWARE_SERVICE_CLASS::add_subsriber(CAN4A_MESSAGE_CALLBACK callback)
	{
		/*TR4A_trace("can", __FUNCTION_NAME__,"function entry > can[%s]",
											this->m_channel_string.data());*/
		boost::unique_lock<boost::mutex> add_lock(this->m_sync_lock);
		ZOO_HANDLE fd = this->create_ssid();
		this->m_subscribers[fd] = callback;
		/*TR4A_trace("can", __FUNCTION_NAME__,"function exit <");*/
		return fd;
	}

	/**
     * @brief Add subscriber
     * @param fd           --the identity of subscriber
     */
	void CAN_HARDWARE_SERVICE_CLASS::remove_subsriber(IN ZOO_HANDLE       handle)
	{
		/*TR4A_trace("can", __FUNCTION_NAME__,"function entry > can[%s],handle[%d]",
											this->m_channel_string.data(),handle);*/
		boost::unique_lock<boost::mutex> remove_lock(this->m_sync_lock);
		auto ite = this->m_subscribers.begin();
		
		while(ite != this->m_subscribers.end())
		{
			if(ite->first == handle)
			{
				this->m_subscribers.erase(ite);
			}
			ite ++;
		}
		/*TR4A_trace("can", __FUNCTION_NAME__,"function exit <");*/
	}

	/**
     * @brief Set filter rules
     * @param can_id     - can device address
     * @param can_mask   - 
     */
	void CAN_HARDWARE_SERVICE_CLASS::set_filter_option(IN        CAN4A_FILTER_STRUCT * filter,
		 														IN ZOO_INT32 filter_counts)
    {
    	/*TR4A_trace("can", __FUNCTION_NAME__,"function entry > can[%s],filter_counts[%d]",
											this->m_channel_string.data(),filter_counts);*/
    	if(this->m_socket_fd == INVALID_FD)
		{
			__THROW_CAN_EXCEPTION(CAN4A_SYSTEM_ERR, "Can does not initialize.", nullptr);	
		}
		struct can_filter * rfilter = (can_filter*) realloc(filter,
                               			sizeof(struct can_filter) * (filter_counts + 1));
		for(int i = 0;i < filter_counts; i++)
		{
			rfilter[i].can_id   = filter[i].can_id;
			rfilter[i].can_mask = filter[i].can_mask;
		}
		
		if(-1 != setsockopt(this->m_socket_fd, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter)))
		{
			/*TR4A_trace("can", __FUNCTION_NAME__,":: setsockopt failed");*/
		}
		free(rfilter);
		rfilter = NULL;
		/*TR4A_trace("can", __FUNCTION_NAME__,"function exit <");*/
	}

	/**
     * @brief Disable filter 
     */
	void CAN_HARDWARE_SERVICE_CLASS::disable_filter()
	{	
		if(this->m_socket_fd == INVALID_FD)
		{
			__THROW_CAN_EXCEPTION(CAN4A_SYSTEM_ERR, "Can does not initialize.", nullptr);	
		}
		
		setsockopt(this->m_socket_fd, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0);
	}

	/**
     * @brief Start receive can data from can interface
     */
	void CAN_HARDWARE_SERVICE_CLASS::start_event_loop()
	{
		if(this->m_state_manager->get_state() != CAN4A_STATE_IDLE)
		{
			__THROW_CAN_EXCEPTION(CAN4A_SYSTEM_ERR, "Can does not initialize.", nullptr);	
		}
		this->m_state_manager->set_state(CAN4A_STATE_BUSY);
		fd_set rfds;
    	struct timeval timeout;
		FD_ZERO(&rfds);
        FD_SET(this->m_socket_fd,&rfds);
        timeout.tv_sec  = 0;
        timeout.tv_usec = 200000;
		this->m_stopped = ZOO_FALSE;
		while(!this->m_stopped)
		{	
			ZOO_INT32 result = select(this->m_socket_fd + 1, &rfds, NULL, NULL, &timeout);
			switch(result)
			{
				case 0:
					break;
				case -1:
					break;
				default:
					boost::shared_ptr<CAN4A_MESSAGE_STRUCT> message;
					this->recv(message);
					if(message != NULL)
					{	
						boost::unique_lock<boost::mutex> remove_lock(this->m_sync_lock);
						for(auto & s : this->m_subscribers)
						{
							const auto & publish = s.second;
							auto state =  this->m_state_manager->get_state();
							if( publish != nullptr)
								publish(*message.get(),state);
						}
					}
					break;
			}
		}
	}

	/**
     * @brief Stop receive can data from can interface
     */
	void CAN_HARDWARE_SERVICE_CLASS::stop_event_loop()
	{
		this->m_stopped = ZOO_TRUE;
		if(this->m_state_manager->get_state() == CAN4A_STATE_BUSY)
			this->m_state_manager->set_state(CAN4A_STATE_IDLE);
	}
	
	/**
     * @brief Write message to can device
     * @param msg        - message
     */
    void CAN_HARDWARE_SERVICE_CLASS::send(IN boost::shared_ptr<CAN4A_MESSAGE_STRUCT> message)
	{
		if(this->m_socket_fd == INVALID_FD)
		{
			__THROW_CAN_EXCEPTION(CAN4A_SYSTEM_ERR, "Socket id is invalid ...ERROR", nullptr);
		}
		
		if(message != NULL && this->m_socket_fd != INVALID_FD)
		{
			struct can_frame frame;
	    	memset((void*)&frame, 0, sizeof(can_frame));
			frame.can_id = message->can_id;
			frame.can_dlc = message->payload;
			memcpy(frame.data,message->data,sizeof(char) * message->payload);  
			ZOO_INT32 nbytes = write(this->m_socket_fd, &frame, sizeof(frame));
			if(nbytes != sizeof(frame))
			{
				__THROW_CAN_EXCEPTION(CAN4A_SYSTEM_ERR, "Write can failed ...ERROR", nullptr);
			}
		}
	} 
		
	/**
     * @brief Add observer 
     */
    void CAN_HARDWARE_SERVICE_CLASS::startup()
	{
		namespace bp = boost::process;
		bp::child ip_link_set_down(bp::search_path("ip"),
									bp::args={"set",this->m_channel_string,"up"});
	}

	/**
 	 * @brief Remove observer 
 	 */
 	void CAN_HARDWARE_SERVICE_CLASS::shutdown()
	{
		namespace bp = boost::process;
		bp::child ip_link_set_down(bp::search_path("ip"),
									bp::args={"set",this->m_channel_string,"down"});
	}

	
	/**
     * @brief Set can option
     */
	void CAN_HARDWARE_SERVICE_CLASS::set_option(IN ZOO_INT32 bitrate,
								IN ZOO_INT32 bus_off_restart_time)
	{
		namespace bp = boost::process;
		std::ostringstream timeout;
		timeout << bus_off_restart_time;
		std::ostringstream br;
		br << bitrate;
		bp::child ip_link_set_up(bp::search_path("ip"),bp::args={"set",this->m_channel_string,
														"type","can","bitrate",br.str(),
														"triple-sampling",
														"on","restart-ms",timeout.str()});
	}	

	/**
     * @brief Recv message from can device
     * @param msg        - message
     */
    void CAN_HARDWARE_SERVICE_CLASS::recv(INOUT boost::shared_ptr<CAN4A_MESSAGE_STRUCT> message)
	{
		struct can_frame frame;
    	memset((void*)&frame, 0, sizeof(can_frame));		
		ZOO_INT32 nbytes = read(this->m_socket_fd, &frame, sizeof(frame));		
		if(nbytes > 0)
		{
			message.reset(new CAN4A_MESSAGE_STRUCT());
			memcpy(&message->can_id,&frame.can_id,sizeof(frame.can_id));
			memcpy(&message->payload,&frame.can_dlc,sizeof(frame.can_dlc));
			memcpy(&message->data,&frame.data,sizeof(char) * frame.can_dlc);
		}
	} 

	/**
     * @brief Close socket
     */
	void CAN_HARDWARE_SERVICE_CLASS::close_socket(IN ZOO_FD fd)
	{
		if(this->m_socket_fd != INVALID_FD)
		{
			close(this->m_socket_fd);
			this->m_socket_fd = INVALID_FD;
		}
	} 
	
	/**
     * @brief Create ssid
     */
	ZOO_HANDLE CAN_HARDWARE_SERVICE_CLASS::create_ssid()
	{
		static boost::mt19937 engine(boost::random_device{}());
        static boost::random::uniform_int_distribution<uint32_t> distribution;
        return static_cast<ZOO_HANDLE> (distribution(engine));
	} 

	/**
     * @brief Create socket
     */
	ZOO_FD CAN_HARDWARE_SERVICE_CLASS::create_socket()
	{
		struct sockaddr_can addr;
    	struct ifreq ifr;
		ZOO_FD fd = socket(PF_CAN, SOCK_RAW, CAN_RAW);
		if(INVALID_FD == fd)
		{
			__THROW_CAN_EXCEPTION(CAN4A_SYSTEM_ERR, "Create socket failed.", nullptr);
		}
		strcpy(ifr.ifr_name, this->m_channel_string.c_str());
		ioctl(fd, SIOCGIFINDEX, &ifr);
		addr.can_family = AF_CAN;
        addr.can_ifindex = ifr.ifr_ifindex;
		if(-1 == bind(fd, (struct sockaddr *)&addr, sizeof(addr)))
		{
			__THROW_CAN_EXCEPTION(CAN4A_SYSTEM_ERR, "bind socket failed.", nullptr);
		}
		return fd;
	}
	
	/**
	 * @brief convert can channel to string
	 */
	std::string CAN_HARDWARE_SERVICE_CLASS::to_str(CAN4A_CHANNEL_ID_ENUM channel)
	{
		std::string result;
		switch(channel)
		{
			case CAN_CHANNEL_ID_0:
				result = "can0";
				break;
			case CAN_CHANNEL_ID_1:
				result = "can1";
				break;
			case CAN_CHANNEL_ID_2:
				result = "can2";
				break;
			default:
				result = "unknown";
				break;
		}
		return result;
	}
}
