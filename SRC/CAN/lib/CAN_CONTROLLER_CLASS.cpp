/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CC
 * File Name    : CAN_CONTROLLER_CLASS.h
 * Description  : This class is used to unpack can data.
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-05-18			MOZEAT		Initialize
 ***************************************************************/
#include "CAN_CONTROLLER_CLASS.h"
namespace ZOO_CAN
{

	/**
     * @brief Constructor
     */
	CAN_CONTROLLER_CLASS::CAN_CONTROLLER_CLASS()
	{
	}
	
    /**
     * @brief Deconstructor
     */
    CAN_CONTROLLER_CLASS::~CAN_CONTROLLER_CLASS()
	{
	}
		
	/**
	* @brief Initialize can ,set can up and birate prepare for recving msg
	* @param channel              -can0 can1 ...
	* @param bitrate              -communication baudrate
	* @param bus_off_restart_time -reboot can driver timeout
	* @return error code 
	*/
	ZOO_INT32 CAN_CONTROLLER_CLASS::initialize(IN CAN4A_CHANNEL_ID_ENUM channel,
									 			IN ZOO_INT32 bitrate,
									 			IN ZOO_INT32 bus_off_restart_time)
	{	
		ZOO_INT32 result = OK;
		__CAN_TRY
		{
			if(!this->contain_model(channel))
				this->create_model(channel);
			boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> service = 
			    this->get_model(channel);
			service->initialize(bitrate,bus_off_restart_time);
		}
		__CAN_CATCH_ALL(result)
		return result;
	}
									 
	/**
	* @brief Shutdown can channel,and set the state  to TERMINATE
	* @param channel              -can0 can1 ...
	* @return error code 
	*/
	ZOO_INT32 CAN_CONTROLLER_CLASS::terminate(IN CAN4A_CHANNEL_ID_ENUM channel)
	{
		ZOO_INT32 result = OK;
		__CAN_TRY
		{
			if(!this->contain_model(channel))
				this->create_model(channel);
			boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> service = 
			    this->get_model(channel);
			service->terminate();
		}
		__CAN_CATCH_ALL(result)
		return result;
	}

	/**
     * @brief Get can channel curretn state
     * @param channel              -can0 can1 ...
     * @return state
     */
	 CAN4A_STATE_ENUM CAN_CONTROLLER_CLASS::get_state(IN CAN4A_CHANNEL_ID_ENUM channel)
	 {
		CAN4A_STATE_ENUM result = CAN4A_STATE_MAX;
		if(this->contain_model(channel))
		{	
			boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> service = 
			    this->get_model(channel);
			result = service->get_state();
		}
		return result;
	}
	
	/**
	* @brief Set filter param for recving can message
	* @param channel              -can0 can1 ...
	* @param filter               -filter
	* @param filter_len           -number of filters
	* @return error code 
	*/
	ZOO_INT32 CAN_CONTROLLER_CLASS::set_filter(IN CAN4A_CHANNEL_ID_ENUM channel,
													IN const CAN4A_FILTER_STRUCT * filter,
													IN ZOO_INT32 filter_len)
	{
		ZOO_INT32 result = OK;
		__CAN_TRY
		{
			if(!this->contain_model(channel))
				this->create_model(channel);
			boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> service = 
			    this->get_model(channel);
			service->set_filter_option((CAN4A_FILTER_STRUCT *)filter,filter_len);
		}
		__CAN_CATCH_ALL(result)
		return result;
	}

	/**
	* @brief Disable filter
	* @param channel              -can0 can1 ...
	* @return error code 
	*/
	ZOO_INT32 CAN_CONTROLLER_CLASS::disable_filter(IN CAN4A_CHANNEL_ID_ENUM channel)
	{
		ZOO_INT32 result = OK;
		__CAN_TRY
		{
			if(!this->contain_model(channel))
				this->create_model(channel);
			boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> service = 
			    this->get_model(channel);
			service->disable_filter();
		}
		__CAN_CATCH_ALL(result)
		return result;
	}

	/**
	* @brief Subscribe can message
	* @param channel              -can0 can1 ...
	* @param callback             -callback function 
	* @return error code 
	*/
	ZOO_HANDLE CAN_CONTROLLER_CLASS::subscribe(IN CAN4A_CHANNEL_ID_ENUM channel,
													IN CAN4A_MESSAGE_CALLBACK callback)
	{
		ZOO_INT32 result = OK;
		__CAN_TRY
		{
			if(!this->contain_model(channel))
				this->create_model(channel);
			boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> service = 
			    this->get_model(channel);
			service->add_subsriber(callback);
		}
		__CAN_CATCH_ALL(result)
		return result;
	}
								
	/**
     * @brief Subscribe can message
     * @param channel              -can0 can1 ...
     * @param callback             -callback function 
     * @return error code 
     */
	ZOO_INT32 CAN_CONTROLLER_CLASS::unsubscribe(IN CAN4A_CHANNEL_ID_ENUM channel,IN ZOO_HANDLE handle)
	{
		ZOO_INT32 result = OK;
		__CAN_TRY
		{
			if(!this->contain_model(channel))
				this->create_model(channel);
			boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> service = 
			    this->get_model(channel);
			service->remove_subsriber(handle);
		}
		__CAN_CATCH_ALL(result)
		return result;
	}
	 
	/**
	* @brief Start to recv can message from socket
	* @param channel              -can0 can1 ...
	* @return error code 
	*/							
	ZOO_INT32 CAN_CONTROLLER_CLASS::start_event_loop(IN CAN4A_CHANNEL_ID_ENUM channel)
	{
		ZOO_INT32 result = OK;
		__CAN_TRY
		{
			if(!this->contain_model(channel))
				this->create_model(channel);
			boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> service = 
			    this->get_model(channel);
			service->start_event_loop();
		}
		__CAN_CATCH_ALL(result)
		return result;
	}

	/**
	* @brief Stop to recv can message from socket
	* @param channel              -can0 can1 ...
	* @return error code 
	*/							
	ZOO_INT32 CAN_CONTROLLER_CLASS::stop_event_loop(IN CAN4A_CHANNEL_ID_ENUM channel)
	{
		ZOO_INT32 result = OK;
		__CAN_TRY
		{
			if(!this->contain_model(channel))
				this->create_model(channel);
			boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> service = 
			    this->get_model(channel);
			service->stop_event_loop();
		}
		__CAN_CATCH_ALL(result)
		return result;
	}

	/**
	* @brief Stop to recv can message from socket
	* @param channel              -can0 can1 ...
	* @param msg                  
	* @return error code 
	*/							
	ZOO_INT32 CAN_CONTROLLER_CLASS::recv_message(IN CAN4A_CHANNEL_ID_ENUM channel,
															INOUT CAN4A_MESSAGE_STRUCT * msg)
	{
		ZOO_INT32 result = OK;
		__CAN_TRY
		{
			if(!this->contain_model(channel))
				this->create_model(channel);
			boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> service = 
			    this->get_model(channel);
			boost::shared_ptr<CAN4A_MESSAGE_STRUCT> message;
			service->recv(message);
			memcpy((void *)msg,message.get(),sizeof(CAN4A_MESSAGE_STRUCT));
		}
		__CAN_CATCH_ALL(result)
		return result;
	}

	/**
	* @brief Stop to recv can message from socket
	* @param channel              -can0 can1 ...
	* @return error code 
	*/							
	ZOO_INT32 CAN_CONTROLLER_CLASS::send_message(IN CAN4A_CHANNEL_ID_ENUM channel,IN CAN4A_MESSAGE_STRUCT * msg)
	{
		ZOO_INT32 result = OK;
		__CAN_TRY
		{
			if(!this->contain_model(channel))
				this->create_model(channel);
			boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> service = 
			    this->get_model(channel);
			boost::shared_ptr<CAN4A_MESSAGE_STRUCT> message(msg);
			service->send(message);
		}
		__CAN_CATCH_ALL(result)
		return result;
	}

	/**
	* @brief Create an can  model by channel
	* @param channel              -can0 can1 ...        
	* @return  
	*/	
	void CAN_CONTROLLER_CLASS::create_model(IN CAN4A_CHANNEL_ID_ENUM channel)
	{
		std::map<int,boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> >::iterator ite =
			this->m_can_models.find(channel);
		if(ite == this->m_can_models.end())
		{
			boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> can_service(new CAN_HARDWARE_SERVICE_CLASS(channel));
				
			this->m_can_models[channel] = can_service; 
		}
	}

	/**
	* @brief Check can model existence
	* @param channel              -can0 can1 ...        
	* @return  
	*/		
	ZOO_BOOL CAN_CONTROLLER_CLASS::contain_model(IN CAN4A_CHANNEL_ID_ENUM channel)
	{
		std::map<int,boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> >::iterator ite =
			this->m_can_models.find(channel);
		if(ite == this->m_can_models.end())
		{
			return ZOO_FALSE;
		}
		return ZOO_TRUE;
	}

	/**
	* @brief Get can model instance
	* @param channel              -can0 can1 ...        
	* @return  can model
	*/	
	boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> CAN_CONTROLLER_CLASS::get_model(IN CAN4A_CHANNEL_ID_ENUM channel)
	{
		boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> result;
		std::map<int,boost::shared_ptr<CAN_HARDWARE_SERVICE_CLASS> >::iterator ite =
			this->m_can_models.find(channel);
		if(ite != this->m_can_models.end())
		{
			result =  this->m_can_models[channel];
		}
		return result;
	}
} /* namespace ZOO_CC */
