/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : ss_stop.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-01-09    Generator      created
*************************************************************/
#include <string.h>
#include "SM4A_if.h"
#include "SM4A_type.h"
#include "MM4A_if.h"
#include "MQ4A_if.h"


/* 进程入库函数main */
ZOO_INT32 main(int argc,char *argv[])
{;
    ZOO_INT32 rtn = OK;
    int i = 1;
    
	/**
	 *@brief Initialze memory pool for communicaiton.
	 */
    MM4A_initialize();

    if(argc >= 2)
    {
        for(; i < argc ; i ++)
        {
            if(strlen(argv[i]) <= SM_TASK_NAME_LENGHT)
            {
                if(OK != SM4A_stop_single_task(argv[i]))
                    break;
            }
        }
    }
    else
    {
    	if(OK == rtn)
    	{
    		rtn = SM4A_stop_all_tasks();
    	}
    }
    
	MQ4A_shutdown_all_clients();
    
	/**
	 *@brief Release memory pool.
	 */
	MM4A_terminate();
    return rtn;
}
