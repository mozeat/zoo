/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : ss_start.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-01-09    Generator      created
*************************************************************/
#include <string.h>
#include <stdio.h>
#include "SM4A_if.h"
#include "SM4A_type.h"
#include "MM4A_if.h"
#include "MQ4A_if.h"
#include "ZOO_if.h"

/* 进程入库函数main */
ZOO_INT32 main(int argc,char *argv[])
{
	/**
	 *@brief Initialze memory pool for communicaiton.
	 */
	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION__,"initialize memory pool ...");
    ZOO_INT32 i = 2;
    ZOO_VERSION_STRUCT  version_info;
    ZOO_INT32 rtn = ZOO_get_pl_version_info(&version_info);
    if(OK != rtn)
    {
        printf("read version info  failed \n");
        return rtn;
    }
    
    if(argc >= 2)
    {
        if(strcmp(argv[1],"-v") == 0)
        {
            printf("  \n");
            printf("  ZOO release info:\n");
            printf("    a). ZOO version : %s ;\n",version_info.zoo);
            printf("    b). kernal version : %s ;\n",version_info.kernel);
            printf("    c). TR is applied to log message to file or console according to settings.ini; \n");
            printf("    d). EH is applied to handle exceptions,it could save alarm with material id to the database file; \n");
            printf("    d). DB is an high level application database access object; \n");
            printf("    e). SM is a system manager for control tasks; \n");
            printf("    f). CM is use to read and write configure file; \n");
            printf("    g). HTTP is response for handle http client request and http server request; \n");
            return OK;
        }
    }
    
	MM4A_initialize();
	
	/**
	 *@brief Create tasks from db
	 */
	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION__,"initialize SM  ...");
	rtn = SM4A_initialize();
	if(OK != rtn)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__FUNCTION__,"initialize failed [%d] ...ERROR",rtn);
	}
	/**
	 *@brief Start tasks
	 */
    if(OK == rtn)
	{
        if(argc > 2)
        {
            if(strcmp(argv[1],"-p") == 0)
            {
                 for(;i < argc; ++i)  
                 {
                     ZOO_UINT32 pid;
                     rtn = SM4A_start_single_task(argv[i],20,&pid);
    	             ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION__,"pid : %ud",pid);
                 }
            }
        }
        else
    	{
	        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__FUNCTION__,"Start all tasks ...");
    		rtn = SM4A_start_all_tasks();
    	}
    }

	/**
	 *@brief Shutdown background communication thread.
	 */
	if(OK == rtn)
	{
		MQ4A_shutdown_all_clients();
	}
	
	/**
	 *@brief Release memory pool.
	 */
	MM4A_terminate();
    return rtn;
}
