#include ../Makefile_tpl_cov
include ../Project_config

TARGET   := zstart
SRCEXTS  :=  .c 
INCDIRS  := ./inc ./com
SOURCES  := ./bin/ss_start.c
SRCDIRS  := 
CFLAGS   := 
CXXFLAGS := -std=c++14 -fstack-protector-all
CPPFLAGS := -DBOOST_ALL_DYN_LINK
LDFPATH  := -L$(THIRD_PARTY_LIBRARY_PATH)
LDFLAGS  := $(GCOV_LINK) $(LDFPATH) -lSM4A -lMM4A -lMQ4A -lZOO 


include ../Makefile_tpl_linux
