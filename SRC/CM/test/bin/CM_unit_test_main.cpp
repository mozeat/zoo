/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : CM
 * File Name      : CM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
#define BOOST_TEST_MODULE CM_test_module 
#define BOOST_TEST_DYN_LINK 
#include <boost/test/unit_test.hpp>
#include <boost/test/unit_test_log.hpp>
#include <boost/test/unit_test_suite.hpp>
#include <boost/test/framework.hpp>
#include <boost/test/unit_test_parameters.hpp>
#include <boost/test/utils/nullstream.hpp>
/**
 * @brief Execute <CM> UTMF ...
 * @brief <Global Fixture> header file can define the global variable.
 * @brief modify the include header files sequence to change the unit test execution sequence but not case SHUTDOWN.
 */
#include <TC_CM_GLOBAL_FIXTRUE.h>
#include <TC_CM_COMPONENT_CHANGE_SUBSCRIBE_005.h>
#include <TC_CM_READ_DATA_002.h>
#include <TC_CM_WRITE_DATA_001.h>
#include <TC_CM_COMPONENT_CHANGE_UNSUBSCRIBE_006.h>
#include <TC_CM_READ_CONFIG_003.h>
#include <TC_CM_WRITE_CONFIG_004.h>

/**
 * @brief This case should be executed at the end of all test cases to exit current process.
 * @brief Close all this process events subscribe.
 * @brief Close message queue context thread.
 */
#include <TC_CM_SHUTDOWN.h>
