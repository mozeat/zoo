/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : CM
 * File Name      : CM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
extern "C"
{
    #include <CM4A_if.h>
    #include <CM4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_CM_COMPONENT_CHANGE_UNSUBSCRIBE_006)

/**
 *@brief 
 *@param handle
**/
    BOOST_AUTO_TEST_CASE( TC_CM_COMPONENT_CHANGE_UNSUBSCRIBE_006_001 )
    {
        BOOST_TEST(CM4A_component_change_unsubscribe(TC_CM_GOLBAL_FIXTURE::handle) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()