/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : CM
 * File Name      : CM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
extern "C"
{
    #include <CM4A_if.h>
    #include <CM4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_CM_COMPONENT_CHANGE_SUBSCRIBE_005)

/**
 *@brief 
 *@param callback_function
 *@param handle
 *@param context
**/

	void test_subscribe_callback(IN CM4A_STATUS_STRUCT status,
				                  IN ZOO_INT32 error_code,
				                  IN void *context)
	{
		BOOST_TEST_MESSAGE( "component id: " << status.component);
	}

    BOOST_AUTO_TEST_CASE( TC_CM_COMPONENT_CHANGE_SUBSCRIBE_005_001 )
    {
        BOOST_TEST(CM4A_component_change_subscribe(test_subscribe_callback,&TC_CM_GOLBAL_FIXTURE::handle,NULL) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()