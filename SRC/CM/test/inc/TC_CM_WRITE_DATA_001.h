/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : CM
 * File Name      : CM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
extern "C"
{
    #include <CM4A_if.h>
    #include <CM4A_type.h>
    #include <MM4A_if.h>
	#include <CCCM_data.h>
}
BOOST_AUTO_TEST_SUITE(TC_CM_WRITE_DATA_001)

/**
 *@brief 
 *@param component_id
 *@param data
 *@param length
**/
    BOOST_AUTO_TEST_CASE( TC_CM_WRITE_DATA_001_001 )
    {
        const char component_id[256] = "CC";
        size_t length = sizeof(CCCM_FILE_STRUCT);
        CCCM_FILE_STRUCT data;
		memset(&data, 0x00, sizeof(CCCM_FILE_STRUCT));
        CM4A_read_data(component_id,(void *)(&data), length);
		data.branch_4.current = 100;
		data.can.baud_rate = 9600;
		data.can.loopback = true;
		data.can.channel = CCCM_CAN_CHANNEL_3;
		data.branch_num = 3;
		data.branch_4.limit.level_1.soc_range_max = 80;
		data.branch_4.limit.level_1.soc_range_min = 11;
		data.branch_4.protection.protection_voltage = 301;
		data.branch_4.protection.protection_current = 123;
		data.branch_4.acdc_modules = 3;
        BOOST_TEST(CM4A_write_data(component_id,&data,length) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()