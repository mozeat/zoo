/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : CM
 * File Name      : CM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
extern "C"
{
    #include <CM4A_if.h>
    #include <CM4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_CM_WRITE_CONFIG_004)

/**
 *@brief 
 *@param component_id
 *@param conf
**/
    BOOST_AUTO_TEST_CASE( TC_CM_WRITE_CONFIG_004_001 )
    {
        const char component_id[256] = "CC";
        CM4A_GROUP_STRUCT conf;
		memset(&conf, 0x00, sizeof(CM4A_GROUP_STRUCT));
        CM4A_read_config(component_id,&conf);
	    memcpy(conf.members[1].default_value.content, "101", sizeof("101"));
	    memcpy(conf.members[1].value.content, "1234", sizeof("1234"));
	    memcpy(conf.members[1].description.content, "test description", sizeof("test description"));
        BOOST_TEST(CM4A_write_config(component_id,&conf) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()