/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : CM
 * File Name      : CM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
extern "C"
{
    #include <CM4A_if.h>
    #include <CM4A_type.h>
    #include <MM4A_if.h>
	#include <CCCM_data.h>
}
BOOST_AUTO_TEST_SUITE(TC_CM_READ_DATA_002)

/**
 *@brief 
 *@param component_id
 *@param length
 *@param data
**/
    BOOST_AUTO_TEST_CASE( TC_CM_READ_DATA_002_001 )
    {
        const char component_id[256] = "CC";
        size_t length = sizeof(CCCM_FILE_STRUCT);
        CCCM_FILE_STRUCT data;
		memset(&data, 0x00, sizeof(CCCM_FILE_STRUCT));
		BOOST_TEST_MESSAGE( "length: " << length);
        BOOST_TEST(CM4A_read_data(component_id,(void *)(&data), length) == OK);
		BOOST_TEST_MESSAGE( "branch_1 current: " << data.branch_1.current);
		BOOST_TEST_MESSAGE( "branch_2 current: " << data.branch_2.current);
		BOOST_TEST_MESSAGE( "branch_3 current: " << data.branch_3.current);
		BOOST_TEST_MESSAGE( "branch_4 current: " << data.branch_4.current);
		BOOST_TEST_MESSAGE( "branch_5 current: " << data.branch_5.current);
		BOOST_TEST_MESSAGE( "branch_6 current: " << data.branch_6.current);
		BOOST_TEST_MESSAGE( "branch_7 current: " << data.branch_7.current);
		BOOST_TEST_MESSAGE( "branch_8 current: " << data.branch_8.current);
		BOOST_TEST_MESSAGE( "branch_9 current: " << data.branch_9.current);	
		BOOST_TEST_MESSAGE( "branch_10 current: " << data.branch_10.current);		
    }

BOOST_AUTO_TEST_SUITE_END()