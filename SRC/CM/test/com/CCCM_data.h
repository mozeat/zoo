/*******************************************************************************
* Copyright (C) 2017, 上海蔚来汽车有限公司
* All rights reserved.
* 产品号 : ZOO
* 所属组件 : CCCM
* 模块名称 : 
* 文件名称 : CCCM_type.h
* 概要描述 : 接口描述
* 历史记录 :
* 版本		日期		    作者			    内容
* V1.0		2018.3.23	weiwang.sun		    创建
*******************************************************************************/
#ifndef CCCM_DATA_H_
#define CCCM_DATA_H_
#include <ZOO.h>

/* can通道枚举定义 */
typedef enum
{
	CCCM_CAN_CHANNEL_MIN  = -1,
	CCCM_CAN_CHANNEL_0 = 0,
	CCCM_CAN_CHANNEL_1 = 1,
	CCCM_CAN_CHANNEL_2 = 2,
	CCCM_CAN_CHANNEL_3 = 3,
	CCCM_CAN_CHANNEL_4 = 4,
	CCCM_CAN_CHANNEL_5 = 5,
	CCCM_CAN_CHANNEL_MAX
}CCCM_CAN_CHANNEL_ENUM;

/* can通道枚举定义 */
typedef enum
{
	CCCM_DATA_MIN ,
	CCCM_DATA,
	CCCM_DATA_MAX
}CCCM_DATA_CHANNEL_ENUM;
	
/* 充电限流系数 */
typedef struct
{
	ZOO_INT32 soc_range_min;//default[1],current[2],unit[],range[1,100],@起始SOC
	ZOO_INT32 soc_range_max;//default[100],current[2],unit[],range[1,100],@截止SOC
	ZOO_FLOAT current_limit_coefficient;//default[0.5],current[2],unit[],range[0,1],@充电限流系数
}CCCM_CURRENT_LIMIT_PARAM_STRUCT;

/* 充电限流等级 */
typedef struct
{
	CCCM_CURRENT_LIMIT_PARAM_STRUCT level_1;
	CCCM_CURRENT_LIMIT_PARAM_STRUCT level_2;
	CCCM_CURRENT_LIMIT_PARAM_STRUCT level_3;
}CCCM_CURRENT_LIMIT_LEVEL_STRUCT;

/* 充电保护参数 */
typedef struct
{
	ZOO_INT32 protection_current;//default[1],current[2],unit[],range[1,5],@can充电保护电流
	ZOO_INT32 protection_voltage;//default[1],current[2],unit[],range[1,5],@can充电保护电压
	ZOO_INT32 deadline_soc;//default[1],unit[],current[2],range[1,5],@can截止SOC
	ZOO_INT32 monomer_voltage_max;//default[1],current[2],unit[],range[1,5],@can最大单体电压
	ZOO_INT32 monomer_voltage_min;//default[1],current[2],unit[],range[1,5],@can最小单体电压
	ZOO_INT32 monomer_voltage_diff;//default[1],current[2],unit[],range[1,5],@can最大单体电压压差
	ZOO_INT32 monomer_temperature_max;//default[1],current[2],unit[],range[1,5],@can最大单体温度
	ZOO_INT32 monomer_temperature_min;//default[1],current[2],unit[],range[1,5],@can最小单体温度
	ZOO_INT32 monomer_temperature_diff;//default[1],current[2],unit[],range[1,5],@can允许最大单体温差
}CCCM_BMS_PROTECTION_PARAM_STRUCT;

/* 充电处方 */
typedef struct
{
	ZOO_INT32 current;//default[1],current[2],unit[],range[1,5],@can充电电流
	ZOO_INT32 acdc_modules;//default[1],current[2],unit[],range[1,5],@can充电模块个数
	CCCM_BMS_PROTECTION_PARAM_STRUCT protection;//default[1],current[2],unit[],range[1,5],@can充电保护参数
	CCCM_CURRENT_LIMIT_LEVEL_STRUCT limit;
}CCCM_BRANCH_STRUCT;	

/* 一个支路软件版本号结构体 */
typedef struct
{
	CCCM_CAN_CHANNEL_ENUM channel;//default[1],current[2],unit[],range[1,5],@can通道
	ZOO_INT32 baud_rate;//default[500000],current[2],unit[],range[1,5],@can通信波特率
	ZOO_BOOL loopback;//default[1],current[2],unit[],range[0,1],@can通信回环控制，默认1是打开,0是关闭
}CCCM_CAN_STRUCT;

/* 文件结构体 */
typedef struct
{
	ZOO_INT32 branch_num;//default[5],current[2],unit[],range[1,10],@支路数
	CCCM_CAN_STRUCT can;
	CCCM_BRANCH_STRUCT branch_1;
	CCCM_BRANCH_STRUCT branch_2;
	CCCM_BRANCH_STRUCT branch_3;
	CCCM_BRANCH_STRUCT branch_4;
	CCCM_BRANCH_STRUCT branch_5;
	CCCM_BRANCH_STRUCT branch_6;
	CCCM_BRANCH_STRUCT branch_7;
	CCCM_BRANCH_STRUCT branch_8;
	CCCM_BRANCH_STRUCT branch_9;
	CCCM_BRANCH_STRUCT branch_10;
}CCCM_FILE_STRUCT;

#endif 
