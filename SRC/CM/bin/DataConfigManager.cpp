/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CM
 * File Name    : DataConfigManager.cpp
 * Description  : config file write and read by struct
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-11-19      chaoluo       Create
 ***************************************************************/
extern "C"
{
	#include "CM4A_type.h"
	#include "ZOO_type.h"
	#include "ZOO_if.h"
}
 
#include "DataTypeDefine.h"
#include "DataConfigManager.h"
#include "ReadDataParser.h"
#include "ReadDataParseArray.h"
#include "WriteDataParse.h"
#include <utils/ENVIRONMENT_UTILITY_CLASS.h>

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;
namespace ZOO_CM
{

	const std::string str_regex_line = "\\s*#([1-9])<(\\w+)>\\s+"
	                                   "@(\\w+)\\s+"
	                                   "%(\\w+\\[\\d+\\]|\\w+)\\s+"
	                                   "(\\(([\\w\\W]*)\\)\\s+"
	                                   "!([\\w\\W]*)\\s+"
	                                   "~\\{\\[([\\w\\W]*)\\]\\}\\s+"
	                                   "&([\\w\\W]*)\\s+)?"
	                                   "\\$([\\w\\W]*)";

	DATA_CONFIG_MANAGER::DATA_CONFIG_MANAGER()
	{

	}

	DATA_CONFIG_MANAGER::~DATA_CONFIG_MANAGER()
	{

	}

	ZOO_INT32 DATA_CONFIG_MANAGER::readConf(const char *component_id, CM4A_GROUP_STRUCT *conf)
	{
	    std::string file = DATA_CONFIG_MANAGER::generatorFile(component_id);

	    boost::smatch match;
	    boost::regex regex_line(str_regex_line);

	    boost::filesystem::path path(file);
	    boost::filesystem::fstream read_fs(path, std::ios_base::in);

	    if (!read_fs.is_open())
	    {
	        return CM4A_FILE_OPEN_ERR;
	    }

	    char line[1024] = {0};
	    memset(conf, 0x00, sizeof (CM4A_GROUP_STRUCT));
	    while(read_fs.getline(line, 1024))
	    {
	        std::string line_text(line);
	        boost::smatch match_line;
	        bool flag = boost::regex_match(line_text, match_line, regex_line);
	        if (!flag)
	        {
	            continue;
	        }

	        if (match_line.size() > 4)
	        {
	            if (0 == match_line[4].str().compare(STR_ENUM))
	            {
	                continue;
	            }
        	}

	        for (unsigned int i = 1; i < match_line.size(); ++i)
	        {
	            std::string str = match_line[i].str();
	            std::string::size_type size = str.size();

	            switch (i)
	            {
	            case TIER_ATTRIBUTE:
	            {
	                std::istringstream is(str);
	                ZOO_INT32 tier = 0;
	                is >> tier;
	                conf->members[conf->rows].tier = tier;
	            }
	                break;
	            case NAME_ATTRIBUTE:
	            {
	                std::string::size_type len = sizeof (conf->members[conf->rows].name);
	                if (size > len - 1)
	                {
	                    return CM4A_NAME_SIZE_ERR;
	                }
	                memcpy(conf->members[conf->rows].name, str.c_str(), size);
	            }
	                break;
	            case MEMBER_ATTRIBUTE:
	            {
	                conf->members[conf->rows].type = CM4A_FIELD_TYPE_GROUP;
	                if (0 == str.compare("Member"))
	                {
	                    conf->members[conf->rows].type = CM4A_FIELD_TYPE_MEMBER;
	                }
	            }
	                break;
	            case DATA_TYPE_ATTRIBUTE:
	            {
	                std::string::size_type len = sizeof (conf->members[conf->rows].data_type);
	                if (size > len - 1)
	                {
	                    return CM4A_DATA_TYPE_SIZE_ERR;
	                }

	                memcpy(conf->members[conf->rows].data_type, str.c_str(), size);
	            }
	                break;
	            case INVALID_ATTRIBUTE://unuse ep:(2) !100 ~{[1,100]} &
	                break;
	            case CONTENT_ATTRIBUTE:
	            {
	                conf->members[conf->rows].value.filled = size > 0 ? true : false;
	                std::string::size_type len = sizeof (conf->members[conf->rows].value.content);
	                if (size > len - 1)
	                {
	                    return CM4A_VALUE_SIZE_ERR;
	                }

	                memcpy(conf->members[conf->rows].value.content, str.c_str(), size);
	            }
	                break;
	            case DEFAULT_CONTENT_ATTRIBUTE:
	            {
	                conf->members[conf->rows].default_value.filled = size > 0 ? true : false;
	                std::string::size_type len = sizeof (conf->members[conf->rows].default_value.content);
	                if (size > len - 1)
	                {
	                    return CM4A_DEFAULT_VALUE_SIZE_ERR;
	                }

	                memcpy(conf->members[conf->rows].default_value.content, str.c_str(), size);
	            }
	                break;
	            case RANGE_ATTRIBUTE:
	            {
	                if (size > 0)
	                {
	                    std::vector<std::string> ranges;
	                    boost::split(ranges, str, boost::is_any_of(","), boost::token_compress_on);
	                    for (unsigned int index = 0; index < ranges.size(); ++index)
	                    {
	                        std::string::size_type len = sizeof (conf->members[conf->rows].ranges[index].content);
	                        std::string::size_type size = ranges[index].size();
	                        if (size > len - 1)
	                        {
	                            return CM4A_RANGES_SIZE_ERR;
	                        }
	                        memcpy(conf->members[conf->rows].ranges[index].content, ranges[index].c_str(), size);
	                    }
	                }
	            }
	                break;
	            case UNIT_ATTRIBUTE:
	            {
	                conf->members[conf->rows].unit.filled = size > 0 ? true : false;
	                std::string::size_type len = sizeof (conf->members[conf->rows].unit.content);
	                if (size > len - 1)
	                {
	                    return CM4A_UINT_SIZE_ERR;
	                }

	                memcpy(conf->members[conf->rows].unit.content, str.c_str(), size);
	            }
	                break;
	            case DESCRIPTION_ATTRIBUTE:
	            {
	                conf->members[conf->rows].description.filled = size > 0 ? true : false;
	                std::string::size_type len = sizeof (conf->members[conf->rows].description.content);
	                if (size > len - 1)
	                {
	                    return CM4A_DESCRIPTION_SIZE_ERR;
	                }

	                memcpy(conf->members[conf->rows].description.content, str.c_str(), size);
	            }
	                break;
	            default:
	                break;
	            }
	        }
	        ++conf->rows;
	    }

	    read_fs.close();

	    return OK;
	}

	ZOO_INT32 DATA_CONFIG_MANAGER::writeConf(const char *component_id, CM4A_GROUP_STRUCT *conf)
	{
	    std::string file = DATA_CONFIG_MANAGER::generatorFile(component_id);

	    boost::smatch match;
	    boost::regex regex_line(str_regex_line);

	    boost::filesystem::path path(file);
	    boost::filesystem::fstream read_fs(path, std::ios_base::in);

	    if (!read_fs.is_open())
	    {
	        return CM4A_FILE_OPEN_ERR;
	    }

	    int index = 0;
	    char line[1024] = {0};
	    std::vector<std::string> lines;
	    while(read_fs.getline(line, 1024))
	    {
	        std::string line_text(line);
	        boost::smatch match_line;
	        bool flag = boost::regex_match(line_text, match_line, regex_line);
	        if (!flag)
	        {
	            lines.push_back(line_text);
	            continue;
	        }

	        std::string data_type;
	        if (match_line.size() <= DATA_TYPE_ATTRIBUTE)
	        {            
	            lines.push_back(line_text);
	            continue;
	        }

	        data_type = match_line[DATA_TYPE_ATTRIBUTE].str();
	        if (0 == data_type.compare(STR_ENUM))
	        {
	            lines.push_back(line_text);
	            continue;
	        }

	        std::string name = match_line[2].str();
	        std::string tier = match_line[1].str();
	        ZOO_INT32 iTier = boost::lexical_cast<int>(tier);

	        if (0 == name.compare(conf->members[index].name)
	                && 0 == data_type.compare(conf->members[index].data_type)
	                && iTier == conf->members[index].tier)
	        {
	            std::string _old;
	            std::string _new;
	            std::string new_line = line_text;
	            if (match_line.size() > CONTENT_ATTRIBUTE)
	            {
	                _old = "(" + match_line[CONTENT_ATTRIBUTE].str() + ")";
	                _new =  "(" + std::string(conf->members[index].value.content) + ")";
	                new_line = find_replace(_old, _new, new_line);
	            }

	            if (match_line.size() > DEFAULT_CONTENT_ATTRIBUTE)
	            {
	                _old = "!" + match_line[DEFAULT_CONTENT_ATTRIBUTE].str();
	                _new =  "!" + std::string(conf->members[index].default_value.content);
	                new_line = find_replace(_old, _new, new_line);
	            }

	            if (match_line.size() > DESCRIPTION_ATTRIBUTE)
	            {
	                _old = "$" + match_line[DESCRIPTION_ATTRIBUTE].str();
	                _new =  "$" + std::string(conf->members[index].description.content);
	                new_line = find_replace(_old, _new, new_line);
	            }

	            lines.push_back(new_line);
	            ++index;
	        }
	        else
	        {
	            return CM4A_WRITE_LINE_ERR;
	        }
	    }

	    read_fs.close();

	    std::ofstream ofilestream(file, ios::trunc);
	    if (!ofilestream.is_open())
	    {
	        return CM4A_FILE_OPEN_ERR;
	    }

	    for (auto line : lines)
	    {
	        ofilestream << line;
	        ofilestream << "\n";
	    }

	    ofilestream.close();

	    return OK;
	}

	int DATA_CONFIG_MANAGER::writeData(const char *component_id, void *data, size_t length)
	{
	    std::string file = DATA_CONFIG_MANAGER::generatorFile(component_id);
	    std::ifstream ifilestream(file);
	    if (!ifilestream.is_open())
	    {
	        return CM4A_FILE_OPEN_ERR;
	    }

	    if (length <= 0)
	    {
	        return CM4A_PARAMETER_ERR;
	    }

	    int ret = matchSize(component_id, length);
	    if (OK != ret)
	    {
	        return ret;
	    }

	    char *pValue = reinterpret_cast<char*>(data);
	    if (nullptr == data)
	    {
	        return CM4A_PARAMETER_ERR;
	    }

	    const int lineLength = 1024;
	    char line[lineLength] = {0};
		
	    boost::regex regex_line(str_regex_line);

	    std::vector<std::string> fileLineVector;
	    fileLineVector.clear();
	    while (ifilestream.getline(line, lineLength))
	    {
			std::string line_text(line);
			boost::smatch match_line;
			bool flag = boost::regex_match(line_text, match_line, regex_line);
			if (!flag)
			{
				fileLineVector.push_back(line_text);
				continue;
			}

			std::string type;
			if (match_line.size() > 4)
			{
				type = match_line[4].str();
				if (0 == type.compare(STR_ENUM))
				{
					fileLineVector.push_back(line_text);
					continue;
				}

				int index = type.find(STR_STRUCT);
				if (index >= 0)
				{
					fileLineVector.push_back(line_text);
					continue;
				}
			}

			if (match_line.size() >= 7)
			{
				std::string strLen;
				std::string value;

				value = match_line[6].str();
				
				matchType(type, strLen);

				if (strLen.empty())
	            {
	                WriteDataParse writeParse(line_text, type);
	                fileLineVector.push_back(writeParse.parseData(value, pValue));
	            }
	            else
	            {
	                WriteDataParse writeParse(line_text, type);
	                fileLineVector.push_back(writeParse.parseData(value, strLen, pValue));
	            }
			}
	    }

	    ifilestream.close();
	    std::ofstream ofilestream(file, ios::trunc);
	    if (!ofilestream.is_open())
	    {
	        return CM4A_FILE_OPEN_ERR;
	    }

	    for (auto line : fileLineVector)
	    {
	        ofilestream << line;
	        ofilestream << "\n";
	    }

	    ofilestream.close();
	    return OK;
	}

	int DATA_CONFIG_MANAGER::readData(const char *component_id, void *data, size_t length)
	{
	    std::string file = DATA_CONFIG_MANAGER::generatorFile(component_id);
	    std::ifstream ifilestream(file);
	    if (!ifilestream.is_open())
	    {
	        return CM4A_FILE_OPEN_ERR;
	    }

	    if (length <= 0)
	    {
	        return CM4A_PARAMETER_ERR;
	    }

	    int ret = matchSize(component_id, length);
	    if (OK != ret)
	    {
	        return ret;
	    }

	    char *pValue = reinterpret_cast<char*>(data);
	    if (nullptr == pValue)
	    {
	        return CM4A_PARAMETER_ERR;
	    }

	    boost::regex regex_line(str_regex_line);

	    const int lineLength = 1024;
	    char line[lineLength] = {0};

	    memset(pValue, 0x00, length);
	    while (ifilestream.getline(line, lineLength))
	    {
	   		std::string line_text(line);
	        boost::smatch match_line;
	        bool flag = boost::regex_match(line_text, match_line, regex_line);
	        if (!flag)
	        {
	            continue;
	        }

			std::string type;
	        if (match_line.size() > 4)
	        {
		        type = match_line[4].str();
	            if (0 == type.compare(STR_ENUM))
	            {
	                continue;
	            }

				int index = type.find(STR_STRUCT);
	            if (index >= 0)
	            {
	                continue;
	            }
	        }
			else
			{
				return CM4A_FILE_PARSE_ERR;
			}

			if (match_line.size() >= 7)
			{
				std::string value = match_line[6].str();
				int index = type.find(STR_ENUM);
				if (index >= 0)
				{
					ReadDataParser<ZOO_INT32> readParse(value, pValue);
					continue;
				}

				if (0 == type.compare(STR_ZOO_CHAR)
						|| 0 == type.compare(STR_ZOO_INT8)
						|| 0 == type.compare(STR_ZOO_UINT8))
				{
					ReadDataParser<ZOO_CHAR> readParse(value, pValue);
				}
				else if (0 == type.compare(STR_ZOO_INT16))
				{
					ReadDataParser<ZOO_INT16> readParse(value, pValue);
				}
				else if (0 == type.compare(STR_ZOO_UINT16))
				{
					ReadDataParser<ZOO_UINT16> readParse(value, pValue);
				}
				else if (0 == type.compare(STR_ZOO_FLOAT))
				{
					ReadDataParser<ZOO_FLOAT> readParse(value, pValue);
				}
				else if (0 == type.compare(STR_ZOO_DOUBLE))
				{
					ReadDataParser<ZOO_DOUBLE> readParse(value, pValue);
				}
				else if (0 == type.compare(STR_ZOO_LDOUBLE))
				{
					ReadDataParser<ZOO_LDOUBLE> readParse(value, pValue);
				}
				else if (0 == type.compare(STR_INT)
						 || 0 == type.compare(STR_ZOO_INT32))
				{
					ReadDataParser<ZOO_INT32> readParse(value, pValue);
				}
				else if (0 == type.compare(STR_ZOO_UINT32))
				{
					ReadDataParser<ZOO_UINT32> readParse(value, pValue);
				}
				else if (0 == type.compare(STR_ZOO_INT64))
				{
					ReadDataParser<ZOO_INT64> readParse(value, pValue);
				}
				else if (0 == type.compare(STR_ZOO_UINT64))
				{
					ReadDataParser<ZOO_UINT64> readParse(value, pValue);
				}
				else if (0 == type.compare(STR_ZOO_ULONG32))
				{
					ReadDataParser<ZOO_ULONG32> readParse(value, pValue);
				}
				else if (0 == type.compare(STR_ZOO_LONG))
				{
					ReadDataParser<ZOO_LONG> readParse(value, pValue);
				}
				else if (0 == type.compare(STR_ZOO_BOOL))
				{
					ReadDataParser<ZOO_BOOL> readParse(value, pValue);
				}
				else //char[32] char[32][32]
				{
					std::string strLen;
					
					matchType(type, strLen);
					ReadDataParseArray readParse(strLen, value, pValue);
					int error = readParse.getError();
					if (OK != error)
					{
						return error;
					}
				}
			}
			else
			{
				return CM4A_FILE_PARSE_ERR;
			}
	    }

	    ifilestream.close();
	    return OK;
	}

	string DATA_CONFIG_MANAGER::getWorkDirectory()
	{
	    return getCurrentPath();
	}

	string DATA_CONFIG_MANAGER::getCurrentPath()
	{
		ZOO_USER_PATH_STRUCT files_path;
		ZOO_INT32  ret = ZOO_get_user_files_path(&files_path);
		if (OK != ret)
		{
			return std::string();
		}

	    return files_path.configure.configuration_manager;
	}

	string DATA_CONFIG_MANAGER::generatorFile(const char *component_id)
	{
		ZOO_USER_PATH_STRUCT files_path;
		ZOO_INT32  ret = ZOO_get_user_files_path(&files_path);
		if (OK != ret)
		{
			return std::string();
		}
			
	    std::string file = std::string(files_path.configure.configuration_manager) + "/" + std::string(component_id) + std::string(CM4A_SUFFIX);

	    return file;
	}

	int DATA_CONFIG_MANAGER::matchHead(const string &text)
	{
	    boost::smatch match_head;
	    boost::regex regex_head("(\\s*#[1-9]\\d*[\\w\\W]*\\$[\\w\\W]*)");
	    return boost::regex_match(text, match_head, regex_head);
	}

	int DATA_CONFIG_MANAGER::matchType(const string &text, string &charLen)
	{
	    boost::smatch match_type;
	    boost::regex reg_type("\\[(\\d+)\\]");
	    bool flag = boost::regex_search(text, match_type, reg_type);
	    if (flag)
	    {
	        if (match_type.size() > 1)
	        {
	            charLen = match_type[1].str();
	        }
	    }
	    return flag;
	}

	int DATA_CONFIG_MANAGER::matchValue(const string &text, string &matchText)
	{
	    boost::smatch match_value;
	    boost::regex reg_value("\\s*\\(([\\w\\W]*)\\)\\s");
    bool flag = boost::regex_search(text, match_value, reg_value);
    if (flag)
    {
        matchText = match_value[1].str();
    }

    return flag;
}

int DATA_CONFIG_MANAGER::matchSize(const char *component_id, unsigned int length)
{
    std::string file = DATA_CONFIG_MANAGER::generatorFile(component_id);
    std::ifstream ifilestream(file);
    if (!ifilestream.is_open())
    {
        return CM4A_FILE_OPEN_ERR;
    }

    std::ostringstream ostrstream;
    ostrstream << ifilestream.rdbuf();

    std::string strAllData = ostrstream.str();

    boost::regex reg_size("\\n\\s*\\*\\sFile_size\\s+:\\s*(\\d+)\\s*\\n");// * File_size      : 96
    boost::smatch match_size;
    bool flag = boost::regex_search(strAllData, match_size, reg_size);
    if (!flag)
    {
        return CM4A_SIZE_READ_ERR;
    }

    unsigned int matchSize = 0;
    if (match_size.size() > 1)
    {
        istringstream is(match_size[1].str());
        is >> matchSize;
    }

    if (matchSize == length)
    {
        return OK;
    }

    return CM4A_FILE_SIZE_ERR;
}

std::string DATA_CONFIG_MANAGER::find_replace(std::string &old, std::string &new_str, std::string &text)
{
    std::size_t _pos = text.find(old);
    if (_pos != std::string::npos)
    {
        text = text.replace(_pos, old.size(), new_str);
    }

    return text;
}

}

