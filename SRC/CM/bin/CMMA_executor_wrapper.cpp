/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : CM
 * File Name      : CMMA_executor_wrapper.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
#include <CMMA_executor_wrapper.h>
#include <ZOO_COMMON_MACRO_DEFINE.h>

/**  
 * @brief Register system signal handler,throw PARAMETER_EXCEPTION_CLASS if register fail,
 * the default signal handling is save stack trace to the log file and generate a dump file at execute path.
 * register a self-defined callback to SYSTEM_SIGNAL_HANDLER::resgister_siganl will change the default behavior.
**/
static void CMMA_register_system_signals()
{
    __ZOO_SYSTEM_SIGNAL_REGISTER(CM4I_COMPONET_ID,SIGSEGV); 
    __ZOO_SYSTEM_SIGNAL_REGISTER(CM4I_COMPONET_ID,SIGABRT);

    /* Add more signals if needs,or register self-defined callback function
       to change the default behavior... */
}

/**
 *@brief Execute the start up flow.
 * This function is executed in 3 steps: 
 * Step 1: Load configurations 
 * Step 2: Create controllers
 * Step 3: Create facades and set controllers to created facades
**/ 
void CMMA_startup(void)
{
    /**
     * @brief Signal handler for system behavior 
    */
    CMMA_register_system_signals();

    /** 
     *@brief Step 1: Load configurations 
    */

    /** 
     *@brief Step 2: Create controllers 
    */

    /** 
     *@brief Step 3: Create facades and set controllers to created facades 
    */
}

/**
 *@brief This function response to release instance or memory 
**/ 
void CMMA_shutdown(void)
{
    /** User add */
}

/**
 *@brief Subscribe events published from hardware drivers 
**/
void CMMA_subscribe_driver_event(void)
{
    /** Subscribe events */
}

