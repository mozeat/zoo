/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : CMMA_event.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
#include <CMMA_event.h>

/**
 *@brief CMMA_raise_4A_write_data
 *@param component_id
 *@param data
 *@param length
**/
ZOO_INT32 CMMA_raise_4A_write_data(IN ZOO_INT32 error_code,
                                       IN CM4I_REPLY_HANDLE reply_handle)
{
    CM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = CM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = CM4I_get_reply_message_length(CM4A_WRITE_DATA_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (CM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = CM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = CM4A_WRITE_DATA_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = CM4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = CM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief CMMA_raise_4A_read_data
 *@param component_id
 *@param length
 *@param data
**/
ZOO_INT32 CMMA_raise_4A_read_data(IN ZOO_INT32 error_code,
                                      IN void *data,
                                      size_t length,
                                      IN CM4I_REPLY_HANDLE reply_handle)
{
    CM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = CM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = CM4I_get_reply_message_length(CM4A_READ_DATA_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (CM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = CM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = CM4A_READ_DATA_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.read_data_rep_msg.data,data,length);
                rtn = CM4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = CM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief CMMA_raise_4A_read_config
 *@param component_id
 *@param conf
**/
ZOO_INT32 CMMA_raise_4A_read_config(IN ZOO_INT32 error_code,
                                        IN CM4A_GROUP_STRUCT conf,
                                        IN CM4I_REPLY_HANDLE reply_handle)
{
    CM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = CM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = CM4I_get_reply_message_length(CM4A_READ_CONFIG_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (CM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = CM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = CM4A_READ_CONFIG_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.read_config_rep_msg.conf,&conf,sizeof(CM4A_GROUP_STRUCT));
                rtn = CM4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = CM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief CMMA_raise_4A_write_config
 *@param component_id
 *@param conf
**/
ZOO_INT32 CMMA_raise_4A_write_config(IN ZOO_INT32 error_code,
                                         IN CM4I_REPLY_HANDLE reply_handle)
{
    CM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = CM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = CM4I_get_reply_message_length(CM4A_WRITE_CONFIG_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (CM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = CM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = CM4A_WRITE_CONFIG_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = CM4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = CM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief CM4A_component_change_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
void CMMA_raise_4A_component_change_subscribe(IN CM4A_STATUS_STRUCT status,IN ZOO_INT32 error_code,IN void *context)
{
    ZOO_INT32 rtn = OK;
    CM4I_REPLY_STRUCT * reply_message = NULL;
    reply_message = (CM4I_REPLY_STRUCT * ) MM4A_malloc(sizeof(CM4I_REPLY_STRUCT));
    if(NULL == reply_message)
    {
        rtn = CM4A_PARAMETER_ERR;
    }
    
    if(OK == rtn)
    {
        reply_message->reply_header.function_code = CM4A_COMPONENT_CHANGE_SUBSCRIBE_CODE;
        reply_message->reply_header.execute_result = error_code;
        memcpy(&reply_message->reply_body.component_change_subscribe_code_rep_msg.status,&status,sizeof(CM4A_STATUS_STRUCT));
    }

    if(OK == rtn)
    {
        rtn = CM4I_publish_event(CM4A_SERVER,
                                            CM4A_COMPONENT_CHANGE_SUBSCRIBE_CODE,
                                            reply_message);
    }

    if(OK != rtn)
    {
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }

    return;
}

