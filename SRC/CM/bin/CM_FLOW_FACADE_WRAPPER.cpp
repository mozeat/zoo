#include "CM_FLOW_FACADE_WRAPPER.h"
#include "DataConfigManager.h"

ZOO_INT32 CM_write_data(const char *component_id, void *data, size_t length)
{
    return ZOO_CM::DATA_CONFIG_MANAGER::writeData(component_id, data, length);
}

ZOO_INT32 CM_read_data(const char *component_id, size_t length, void *data)
{
    return ZOO_CM::DATA_CONFIG_MANAGER::readData(component_id, data, length);
}

ZOO_INT32 CM_read_config(const char *component_id, CM4A_GROUP_STRUCT *conf)
{
    return ZOO_CM::DATA_CONFIG_MANAGER::readConf(component_id, conf);
}

ZOO_INT32 CM_write_config(const char *component_id, CM4A_GROUP_STRUCT *conf)
{
    return ZOO_CM::DATA_CONFIG_MANAGER::writeConf(component_id, conf);
}
