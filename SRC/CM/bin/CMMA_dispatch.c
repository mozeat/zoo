/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : CMMA_dispatch.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/

#include <CMMA_dispatch.h>
#include <CMMA_implement.h>

/**
 *@brief CMMA_local_4A_write_data
 *@param component_id
 *@param data
 *@param length
**/
static ZOO_INT32 CMMA_local_4A_write_data(const MQ4A_SERV_ADDR server,CM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    CM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = CM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (CM4I_REPLY_HANDLE)MM4A_malloc(sizeof(CM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = CM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        CMMA_implement_4A_write_data(request->request_body.write_data_req_msg.component_id,
                                           request->request_body.write_data_req_msg.data,
                                           request->request_body.write_data_req_msg.length,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief CMMA_local_4A_read_data
 *@param component_id
 *@param length
 *@param data
**/
static ZOO_INT32 CMMA_local_4A_read_data(const MQ4A_SERV_ADDR server,CM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    CM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = CM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (CM4I_REPLY_HANDLE)MM4A_malloc(sizeof(CM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = CM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        CMMA_implement_4A_read_data(request->request_body.read_data_req_msg.component_id,
                                           request->request_body.read_data_req_msg.length,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief CMMA_local_4A_read_config
 *@param component_id
 *@param conf
**/
static ZOO_INT32 CMMA_local_4A_read_config(const MQ4A_SERV_ADDR server,CM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    CM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = CM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (CM4I_REPLY_HANDLE)MM4A_malloc(sizeof(CM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = CM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        CMMA_implement_4A_read_config(request->request_body.read_config_req_msg.component_id,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief CMMA_local_4A_write_config
 *@param component_id
 *@param conf
**/
static ZOO_INT32 CMMA_local_4A_write_config(const MQ4A_SERV_ADDR server,CM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    CM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = CM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (CM4I_REPLY_HANDLE)MM4A_malloc(sizeof(CM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = CM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        CMMA_implement_4A_write_config(request->request_body.write_config_req_msg.component_id,
                                           &request->request_body.write_config_req_msg.conf,
                                           reply_handle);
    }
    return rtn;
}

/**
*@brief Dispatch message from client to server internal interface
*@param context        
*@param server        address
*@param msg           request message to server
*@param len           request message length
*@param reply_msg     reply message length to caller
*@param reply_msg_len reply message length
**/
void CMMA_callback_handler(void * context, const MQ4A_SERV_ADDR server, void * msg, ZOO_UINT32 msg_id)
{
    ZOO_INT32 rtn = OK;
    ZOO_INT32 rep_length = 0;
    CM4I_REQUEST_STRUCT *request = (CM4I_REQUEST_STRUCT*)msg;
    CM4I_REPLY_STRUCT *reply = NULL;
    if(request == NULL)
    {
        rtn = CM4A_SYSTEM_ERR;
        return;
    }

    if(OK == rtn)
    {
        switch(request->request_header.function_code)
        {
            case CM4A_WRITE_DATA_CODE:
                CMMA_local_4A_write_data(server,request,msg_id);
                break;
            case CM4A_READ_DATA_CODE:
                CMMA_local_4A_read_data(server,request,msg_id);
                break;
            case CM4A_READ_CONFIG_CODE:
                CMMA_local_4A_read_config(server,request,msg_id);
                break;
            case CM4A_WRITE_CONFIG_CODE:
                CMMA_local_4A_write_config(server,request,msg_id);
                break;
            default:
                rtn = CM4A_SYSTEM_ERR;
                break;
        }
    }

    if(OK != rtn && request->request_header.need_reply)
    {
        rtn = CM4I_get_reply_message_length(request->request_header.function_code, &rep_length);
        if(OK != rtn)
        {
            rtn = CM4A_SYSTEM_ERR;
        }

        if(OK == rtn)
        {
            reply = (CM4I_REPLY_STRUCT *)MM4A_malloc(rep_length);
            if(reply == NULL)
            {
                rtn = CM4A_SYSTEM_ERR;
            }
        }

        if(OK == rtn)
        {
            memset(reply, 0x0, rep_length);
            reply->reply_header.execute_result = rtn;
            reply->reply_header.function_code = request->request_header.function_code;
            CM4I_send_reply_message(server,msg_id,reply);
        }
    }
    return;
}

