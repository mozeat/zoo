/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : CMMA_implement.c
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
#include <CMMA_implement.h>
#include "CM_FLOW_FACADE_WRAPPER.h"

/**
 *@brief CMMA_implement_4A_write_data
 *@param component_id
 *@param data
 *@param length
**/
void CMMA_implement_4A_write_data(IN const char* component_id,
                                  IN void* data,
                                  IN size_t length,
                                  IN CM4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = CM_write_data(component_id,data,length);

    /* User add ... END*/
    CMMA_raise_4A_write_data(rtn,reply_handle);

    CM4A_STATUS_STRUCT status;
    memset(&status, 0x00, sizeof (CM4A_STATUS_STRUCT));
    memcpy(status.component, component_id, strlen(component_id));
    CMMA_raise_4A_component_change_subscribe(status, rtn, NULL);
}

/**
 *@brief CMMA_implement_4A_read_data
 *@param component_id
 *@param length
 *@param data
**/
void CMMA_implement_4A_read_data(IN const char* component_id,
                                 IN size_t length,
                                 IN CM4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    void *data = MM4A_malloc(length);
    /* User add some code here if had special need. */
    rtn = CM_read_data(component_id,length,data);

    /* User add ... END*/
    CMMA_raise_4A_read_data(rtn,data,length,reply_handle);

    MM4A_free(data);
}

/**
 *@brief CMMA_implement_4A_read_config
 *@param component_id
 *@param conf
**/
void CMMA_implement_4A_read_config(IN const char* component_id,
                                   IN CM4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    CM4A_GROUP_STRUCT conf ;
    memset(&conf,0,sizeof(CM4A_GROUP_STRUCT));
    /* User add some code here if had special need. */
    rtn = CM_read_config(component_id,&conf);

    /* User add ... END*/
    CMMA_raise_4A_read_config(rtn,conf,reply_handle);
}

/**
 *@brief CMMA_implement_4A_write_config
 *@param component_id
 *@param conf
**/
void CMMA_implement_4A_write_config(IN const char* component_id,
                                    IN CM4A_GROUP_STRUCT* conf,
                                    IN CM4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = CM_write_config(component_id,conf);

    /* User add ... END*/
    CMMA_raise_4A_write_config(rtn,reply_handle);

    CM4A_STATUS_STRUCT status;
    memset(&status, 0x00, sizeof (CM4A_STATUS_STRUCT));
    memcpy(status.component, component_id, strlen(component_id));
    CMMA_raise_4A_component_change_subscribe(status, rtn, NULL);
}


