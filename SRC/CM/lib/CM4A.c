/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : CM4A.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/

#include <ZOO.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include <CM4I_type.h>
#include <CM4A_type.h>
#include <CM4I_if.h>
/**
 *@brief CM4A_write_data
 *@param component_id
 *@param data
 *@param length
**/
ZOO_INT32 CM4A_write_data(IN const char * component_id,IN void * data,IN size_t length)
{
    ZOO_INT32 result = OK;
    CM4I_REQUEST_STRUCT *request_message = NULL; 
    CM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = CM4A_WRITE_DATA_CODE;
    result = CM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (CM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = CM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = CM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (CM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = CM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.write_data_req_msg.component_id),component_id,sizeof(char) * CM4I_BUFFER_LENGTH);
        memcpy(request_message->request_body.write_data_req_msg.data,data,length);
        memcpy((void *)(&request_message->request_body.write_data_req_msg.length),&length,sizeof(size_t));
    }

    if(OK == result)
    {
        result = CM4I_send_request_and_reply(CM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief CM4A_read_data
 *@param component_id
 *@param length
 *@param data
**/
ZOO_INT32 CM4A_read_data(IN const char * component_id,INOUT void * data,IN size_t length)
{
    ZOO_INT32 result = OK;
    CM4I_REQUEST_STRUCT *request_message = NULL; 
    CM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = CM4A_READ_DATA_CODE;
    result = CM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (CM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = CM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = CM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (CM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = CM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.read_data_req_msg.component_id),component_id,sizeof(char) * CM4I_BUFFER_LENGTH);
        memcpy((void *)(&request_message->request_body.read_data_req_msg.length),&length,sizeof(size_t));
    }

    if(OK == result)
    {
        result = CM4I_send_request_and_reply(CM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(data,&reply_message->reply_body.read_data_rep_msg.data,length);
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief CM4A_read_config
 *@param component_id
 *@param conf
**/
ZOO_INT32 CM4A_read_config(IN const char *component_id, OUT CM4A_GROUP_STRUCT *conf)
{
    ZOO_INT32 result = OK;
    CM4I_REQUEST_STRUCT *request_message = NULL; 
    CM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = CM4A_READ_CONFIG_CODE;
    result = CM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (CM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = CM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = CM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (CM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = CM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.read_config_req_msg.component_id),component_id,sizeof(char) * CM4I_BUFFER_LENGTH);
    }

    if(OK == result)
    {
        result = CM4I_send_request_and_reply(CM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(conf,&reply_message->reply_body.read_config_rep_msg.conf,sizeof(CM4A_GROUP_STRUCT));
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }

	printf("CM4A_read_config\n");
    return result;
}

/**
 *@brief CM4A_write_config
 *@param component_id
 *@param conf
**/
ZOO_INT32 CM4A_write_config(IN const char *component_id, IN CM4A_GROUP_STRUCT *conf)
{
    ZOO_INT32 result = OK;
    CM4I_REQUEST_STRUCT *request_message = NULL; 
    CM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = CM4A_WRITE_CONFIG_CODE;
    result = CM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (CM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = CM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = CM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (CM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = CM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.write_config_req_msg.component_id),component_id,sizeof(char) * CM4I_BUFFER_LENGTH);
        memcpy((void *)(&request_message->request_body.write_config_req_msg.conf),conf,sizeof(CM4A_GROUP_STRUCT));
    }

    if(OK == result)
    {
        result = CM4I_send_request_and_reply(CM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

static void CM4A_component_change_callback(void *context_p, MQ4A_CALLBACK_STRUCT *local_proc, void *msg)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 error_code = OK;
    CM4I_REPLY_STRUCT *reply_msg = NULL;
    CM4I_COMPONENT_CHANGE_SUBSCRIBE_CODE_CALLBACK_STRUCT * callback_struct = NULL;
    ZOO_INT32 rep_length = 0;
    CM4A_STATUS_STRUCT status;
    memset(&status,0,sizeof(CM4A_STATUS_STRUCT));
    if(msg == NULL)
    {
        result = CM4A_PARAMETER_ERR;
    }

    if(OK == result)
    {
        result = CM4I_get_reply_message_length(CM4A_COMPONENT_CHANGE_SUBSCRIBE_CODE, &rep_length);
        if(OK != result)
        {
            result = CM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        reply_msg = (CM4I_REPLY_STRUCT * )MM4A_malloc(rep_length);
        if(OK != result)
        {
            result = CM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        memcpy(reply_msg, msg, rep_length);
        if (CM4A_COMPONENT_CHANGE_SUBSCRIBE_CODE != reply_msg->reply_header.function_code)
        {
            result = CM4A_PARAMETER_ERR;
        }
        error_code = reply_msg->reply_header.execute_result;
        memcpy((void*)&status, &reply_msg->reply_body.component_change_subscribe_code_rep_msg.status, sizeof(CM4A_STATUS_STRUCT));
        callback_struct = (CM4I_COMPONENT_CHANGE_SUBSCRIBE_CODE_CALLBACK_STRUCT*) local_proc;
        ((CM4A_COMPONENT_CHANGE_CALLBACK_FUNCKTION)callback_struct->callback_function)(status,error_code,context_p);
    }

    if(reply_msg != NULL)
    {
        MM4A_free(reply_msg);
    }
}

 ZOO_INT32 CM4A_component_change_subscribe(IN CM4A_COMPONENT_CHANGE_CALLBACK_FUNCKTION callback_function,
                                                     OUT ZOO_UINT32 *handle,
                                                     INOUT void *context)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = 0;
    MQ4A_CALLBACK_STRUCT* callback = NULL;
    if (NULL == callback_function)
    {
        result = CM4A_PARAMETER_ERR;
        return result;
    }

    /*function entry*/
    /*fill callback strcut*/
    callback = (MQ4A_CALLBACK_STRUCT*)MM4A_malloc(sizeof(CM4I_COMPONENT_CHANGE_SUBSCRIBE_CODE_CALLBACK_STRUCT));
    if (NULL == callback)
    {
        result = CM4A_PARAMETER_ERR;
    }

    if (OK == result)
    {
        callback->callback_function = callback_function;
        event_id = CM4A_COMPONENT_CHANGE_SUBSCRIBE_CODE;
        result = CM4I_send_subscribe(CM4A_SERVER,
                                      CM4A_component_change_callback,
                                      callback,
                                      event_id,
                                      (ZOO_HANDLE*)handle,
                                      context);
        if (OK != result)
        {
           result = CM4A_PARAMETER_ERR;
           MM4A_free(callback);
        }
    }

    return result;
}

ZOO_INT32 CM4A_component_change_unsubscribe(IN ZOO_UINT32 handle)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = CM4A_COMPONENT_CHANGE_SUBSCRIBE_CODE;
    if(OK == result)
    {
        result = CM4I_send_unsubscribe(CM4A_SERVER,event_id,handle);
    }
    return result;
}

