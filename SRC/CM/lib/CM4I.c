/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : CM4I.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
#include <CM4I_if.h>
/*
@brief Get request message length[bytes]
*@param function_code   function id
*@param *message_length  message length 
*@precondition:
*@postcondition:
*/
ZOO_INT32 CM4I_get_request_message_length(IN ZOO_INT32 function_code,
													INOUT ZOO_INT32 *message_length )
{    ZOO_INT32 result = OK;
    /* Check input parameter */
    if ( NULL == message_length )
    {
        result = CM4A_PARAMETER_ERR;
    }
    else
    {
        *message_length = 0;
    }
    /*Check result */
    if ( OK == result )
    {
        switch( function_code )
        {
        case CM4A_WRITE_DATA_CODE:
            *message_length = sizeof(CM4I_REQUEST_HEADER_STRUCT)+sizeof(CM4I_WRITE_DATA_CODE_REQ_STRUCT);
            break;
        case CM4A_READ_DATA_CODE:
            *message_length = sizeof(CM4I_REQUEST_HEADER_STRUCT)+sizeof(CM4I_READ_DATA_CODE_REQ_STRUCT);
            break;
        case CM4A_READ_CONFIG_CODE:
            *message_length = sizeof(CM4I_REQUEST_HEADER_STRUCT)+sizeof(CM4I_READ_CONFIG_CODE_REQ_STRUCT);
            break;
        case CM4A_WRITE_CONFIG_CODE:
            *message_length = sizeof(CM4I_REQUEST_HEADER_STRUCT)+sizeof(CM4I_WRITE_CONFIG_CODE_REQ_STRUCT);
            break;
        default:
               result = CM4A_PARAMETER_ERR;
               break;
        }
    }
    return result;
}


/*
@brief Get reply message length[bytes]
*@param function_code    function id
*@param *message_length  message length 
*@precondition:
*@postcondition:
*/
ZOO_INT32 CM4I_get_reply_message_length(IN ZOO_INT32 function_code,
													INOUT ZOO_INT32 *message_length )
{    ZOO_INT32 result = OK;
    /*  Check input parameter */
    if ( NULL == message_length )
    {
        result = CM4A_PARAMETER_ERR;
    }
    else
    {
        *message_length = 0;
    }
    /*Check result */
    if ( OK == result )
    {
        switch( function_code )
        {
        case CM4A_WRITE_DATA_CODE:
            *message_length = sizeof(CM4I_REPLY_HEADER_STRUCT)+sizeof(CM4I_WRITE_DATA_CODE_REP_STRUCT);
            break;
        case CM4A_READ_DATA_CODE:
            *message_length = sizeof(CM4I_REPLY_HEADER_STRUCT)+sizeof(CM4I_READ_DATA_CODE_REP_STRUCT);
            break;
        case CM4A_READ_CONFIG_CODE:
            *message_length = sizeof(CM4I_REPLY_HEADER_STRUCT)+sizeof(CM4I_READ_CONFIG_CODE_REP_STRUCT);
            break;
        case CM4A_WRITE_CONFIG_CODE:
            *message_length = sizeof(CM4I_REPLY_HEADER_STRUCT)+sizeof(CM4I_WRITE_CONFIG_CODE_REP_STRUCT);
            break;
        case CM4A_COMPONENT_CHANGE_SUBSCRIBE_CODE:
            *message_length = sizeof(CM4I_REPLY_HEADER_STRUCT)+sizeof(CM4I_COMPONENT_CHANGE_SUBSCRIBE_CODE_REP_STRUCT);
            break;
        default:
               result = CM4A_PARAMETER_ERR;
               break;
        }
    }
    return result;
}


/*
*@brief Send message to server and wait reply
*@param MQ4A_SERV_ADDR   server address
*@param *request_message 
*@param *reply_message   
*@param timeout          milliseconds
*@precondition:
*@postcondition: 
*/
ZOO_INT32 CM4I_send_request_and_reply(IN const MQ4A_SERV_ADDR server,
													IN CM4I_REQUEST_STRUCT  *request_message,
													INOUT CM4I_REPLY_STRUCT *reply_message,
													IN ZOO_INT32 timeout)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 actual_reply_length = 0;

    if(request_message == NULL)
    {
        result = CM4A_PARAMETER_ERR;
    }

    if(result == OK)
    {
        result = CM4I_get_request_message_length(request_message->request_header.function_code, &request_length);
    }

    if(result == OK)
    {
        result = CM4I_get_reply_message_length(reply_message->reply_header.function_code, &reply_length);
    }

    if(result == OK)
    {
        result = MQ4A_send_request_and_receive_reply(server,
												            request_message,
												            request_length,
												            reply_message,
												            reply_length,
												            &actual_reply_length,
												                CM4I_RETRY_INTERVAL,
												            timeout);
    }

 	return result;
}

/*
*@brief 4I_receive_reply_message
*@param MQ4A_SERV_ADDR   
*@param *request_message  
*@precondition:
*@postcondition:
*/
ZOO_INT32 CM4I_send_request_message(IN const MQ4A_SERV_ADDR server,
													IN CM4I_REQUEST_STRUCT *request_message)

{
    ZOO_INT32 result = OK;
    ZOO_INT32 request_length = 0;
    /*Get message length*/
    if ( OK == result )
    {
        result = CM4I_get_request_message_length(request_message->request_header.function_code, &request_length );

    }

    /*Send message to server*/
    if ( OK == result )
    {
        result = MQ4A_send_request( server,				 /*address*/
                                    request_message,					 /*message*/
                                    request_length,						 /*length*/
                                    CM4I_RETRY_INTERVAL );           /*retry  times*/
    }

    return result;
}

/*
*@brief reply message from server
	*@param server         
*@param function_code  
*@param *reply_message 
*@param timeout        
*@description:         4I_send_request_message
*@precondition:
*@postcondition:
*/
ZOO_INT32 CM4I_receive_reply_message(IN const MQ4A_SERV_ADDR server,
													IN ZOO_INT32 function_code,
													INOUT CM4I_REPLY_STRUCT *reply_message,
													IN ZOO_INT32 timeout)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 actual_replay_length = 0;    /*the actual reply message length*/
    ZOO_INT32 reply_length = 0; /*expect reply message length*/
    result = CM4I_get_reply_message_length( function_code, &reply_length );
    /*Get message*/
    while ( OK == result )
    {
        result = MQ4A_receive_reply( server, 
                  					reply_message,
                       				reply_length,
										&actual_replay_length,
								        timeout );
        if (OK == result && function_code == reply_message->reply_header.function_code)
        {
            break;
        }
    }

    return result;
}

/*
*@brief server send reply to clientCM4I_send_reply_message
*@param MQ4A_SERV_ADDR   
*@param *request_message  
*@precondition:
*@postcondition:
*/
ZOO_INT32 CM4I_send_reply_message(IN const MQ4A_SERV_ADDR server,
								IN ZOO_INT32 msg_id,
                              IN CM4I_REPLY_STRUCT *reply_message)

{
    ZOO_INT32 result = OK;
    ZOO_INT32 reply_length = 0;
    /*Get message length*/
    if ( OK == result )
    {
        result = CM4I_get_reply_message_length( reply_message->reply_header.function_code, &reply_length );
    }

    /**/
    if ( OK == result )
    {
        result = MQ4A_send_reply( server,				 /*address*/
                                    msg_id,				 /*id */
                                    reply_message,		 /*message*/
                                    reply_length );       /*length*/
    }

    return result;
}

/*
*@brief 
*@param event_id       
*@param *reply_message 
*@description:         
*@precondition:
*@postcondition:
*/
ZOO_INT32 CM4I_publish_event(IN const MQ4A_SERV_ADDR server,
													IN ZOO_INT32 event_id,
													INOUT CM4I_REPLY_STRUCT *reply_message)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 reply_length = 0;
    result = CM4I_get_reply_message_length(reply_message->reply_header.function_code, &reply_length);
    if (OK == result)
    {
         result = MQ4A_publish( server,
								    event_id,
 								reply_message,
								    reply_length );
    }

    return result;
}

/*
*@brief send subscribe message
*@param server            
*@param callback_function 
*@param callback_struct   
*@param event_id          
*@param *handle           
*@param *context          
 *@precondition:
*@postcondition:
*/
ZOO_INT32 CM4I_send_subscribe(IN const MQ4A_SERV_ADDR server,
													IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
													IN MQ4A_CALLBACK_STRUCT *callback_struct,
													IN ZOO_INT32 event_id,
													INOUT ZOO_HANDLE *handle,
													INOUT void *context)
{
    ZOO_INT32 result = OK;
    if(OK == result)
    {
        result = MQ4A_subscribe( server,
										    callback_function,
											callback_struct,
											event_id,
											handle,
											context);
    }
    return result;
}

/*
*@brief Cancel subscribe 
*@param server   
*@param event_id 
*@param handle    
*@precondition:
*@postcondition:
*/
ZOO_INT32 CM4I_send_unsubscribe(IN const MQ4A_SERV_ADDR server,
													IN ZOO_INT32 event_id,
													IN ZOO_HANDLE handle)
{
    ZOO_INT32 result = OK;
    result = MQ4A_unsubscribe( server,
									 event_id,
								   	 handle );
    return result;
}

