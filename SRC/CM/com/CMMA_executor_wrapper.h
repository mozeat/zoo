/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : CM
 * File Name      : CMMA_executor_wrapper.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
#ifndef CMMA_EXECUTOR_WRAPPER_H
#define CMMA_EXECUTOR_WRAPPER_H

#ifdef __cplusplus 
extern "C" 
{
#endif

#include <ZOO.h>
#include <CM4I_type.h>

    /**
    *@brief This function response to create factory instance or load environment configurations 
    **/
    ZOO_EXPORT void CMMA_startup(void);

    /**
    *@brief This function response to release instance or memory 
    **/
    ZOO_EXPORT void CMMA_shutdown(void);

    /**
    *@brief Subscribe events published from hardware drivers 
    **/
    ZOO_EXPORT void CMMA_subscribe_driver_event(void);


#ifdef __cplusplus 
 }
#endif

#endif // CMMA_executor_wrapper.h
