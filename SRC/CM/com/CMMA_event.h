/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : CMMA_event.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
#ifndef CMMA_EVENT_H
#define CMMA_EVENT_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include <CM4I_type.h>
#include <CM4I_if.h>
#include <CM4A_type.h>

/**
 *@brief CMMA_raise_4A_write_data
 *@param component_id
 *@param data
 *@param length
**/
ZOO_EXPORT ZOO_INT32 CMMA_raise_4A_write_data(IN ZOO_INT32 error_code,
                                                  IN CM4I_REPLY_HANDLE reply_handle);

/**
 *@brief CMMA_raise_4A_read_data
 *@param component_id
 *@param length
 *@param data
**/
ZOO_EXPORT ZOO_INT32 CMMA_raise_4A_read_data(IN ZOO_INT32 error_code,
                                                 IN void *data, size_t length,
                                                 IN CM4I_REPLY_HANDLE reply_handle);

/**
 *@brief CMMA_raise_4A_read_config
 *@param component_id
 *@param conf
**/
ZOO_EXPORT ZOO_INT32 CMMA_raise_4A_read_config(IN ZOO_INT32 error_code,
                                                   IN CM4A_GROUP_STRUCT conf,
                                                   IN CM4I_REPLY_HANDLE reply_handle);

/**
 *@brief CMMA_raise_4A_write_config
 *@param component_id
 *@param conf
**/
ZOO_EXPORT ZOO_INT32 CMMA_raise_4A_write_config(IN ZOO_INT32 error_code,
                                                    IN CM4I_REPLY_HANDLE reply_handle);

/**
 *@brief CM4A_component_change_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
ZOO_EXPORT void CMMA_raise_4A_component_change_subscribe(IN CM4A_STATUS_STRUCT status,IN ZOO_INT32 error_code,IN void *context);


#endif // CMMA_event.h
