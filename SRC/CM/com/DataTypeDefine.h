#ifndef DATATYPEDEFINE_H
#define DATATYPEDEFINE_H

/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CM
 * File Name    : DataTypeDefine.h
 * Description  : date type define
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-11-19      chaoluo       Create
 ***************************************************************/


#include <string>

const std::string STR_ZOO_CHAR = "ZOO_CHAR";
const std::string STR_ZOO_INT8 = "ZOO_INT8";
const std::string STR_ZOO_UINT8 = "ZOO_UINT8";
const std::string STR_ZOO_INT16 = "ZOO_INT16";
const std::string STR_ZOO_UINT16 = "ZOO_UINT16";
const std::string STR_ZOO_INT32 = "ZOO_INT32";
const std::string STR_ZOO_UINT32 = "ZOO_UINT32";
const std::string STR_ZOO_INT64 = "ZOO_INT64";
const std::string STR_ZOO_UINT64 = "ZOO_UINT64";
const std::string STR_ZOO_FLOAT = "ZOO_FLOAT";
const std::string STR_ZOO_DOUBLE = "ZOO_DOUBLE";
const std::string STR_ZOO_LDOUBLE = "ZOO_LDOUBLE";
const std::string STR_ZOO_ULONG32 = "ZOO_ULONG32";
const std::string STR_ZOO_STRING = "ZOO_STRING";
const std::string STR_ZOO_LONG = "ZOO_LONG";
const std::string STR_ZOO_BOOL = "ZOO_BOOL";
const std::string STR_INT = "ZOO_INT";
const std::string STR_ENUM = "ENUM";
const std::string STR_STRUCT = "STRUCT";


#endif // DATATYPEDEFINE_H
