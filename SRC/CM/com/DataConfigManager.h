#ifndef DATACONFIGMANAGER_H
#define DATACONFIGMANAGER_H

/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CM
 * File Name    : DataConfigManager.h
 * Description  : config file write and read by struct
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-11-19      chaoluo       Create
 ***************************************************************/

extern "C"
{
	#include "ZOO_if.h"
	#include "CM4A_type.h"
}

/***
 *
 * for example
 *
 *
 *
 *
 file name:
 file description:
 componet-id:
 size::104
 modify time:
 #<GroupParent> @parent %struct &GroupParent_struct
    #<m_itemA> @child %int !123 &
    #<m_itemB> @parent %struct &GroupItem_struct
        #<item> @child %char[8] !a &
        #<item2> @child %char[32] !abcd &
        #<item3> @child %enum !0 &
        -#<item> @parent %enum ! &
            -#<item> @parent %enum ! &
            -#<group> @parent %enum ! &
        #<filter> @child %char[4] ! &
    #<m_itemC> @child %float !0.12 &
    #<m_itemD> @parent %struct &GroupChild_struct
        #<child> @child %int !9042 &
        #<child2> @child %int !134056 &
        #<child3> @child %int !5 &
        #<filter> @child %char[4] ! &
    #<m_imteE> @child %char[8] !n.a &
    #<m_imteF> @child %int !2 &
    #<m_imteH> @child %double !1.234 &
    #<m_imteG> @child %bool !1 &
    #<filter> @child %char[3] ! &

enum ItemType
{
    item = 0,
    group = 1
};

class GroupItem
{
public:
    GroupItem() {}
    ~GroupItem() {}

    char item[8];
    char item2[32];
    ItemType item3;
    char filter[4];
};

class GroupChild
{
public:
    GroupChild() {}
    ~GroupChild() {}

    int child;
    int child2;
    int child3;
    char filter[4];
};

class GroupParent
{
public:
    GroupParent() {}
    ~GroupParent() {}

    int m_itemA;
    GroupItem m_itemB;
    float m_itemC;
    GroupChild m_itemD;
    char m_imteE[8];
    double m_itemH;
    int m_itemF;
    bool m_itemG;
    char filter[3];
};
 *
 *
 */

#include <string>

namespace ZOO_CM {

class DATA_CONFIG_MANAGER
{
public:
	enum ATTRIBUTE_TYPE_ENUM
	{
		TIER_ATTRIBUTE = 1,
		NAME_ATTRIBUTE = 2,
		MEMBER_ATTRIBUTE = 3,
		DATA_TYPE_ATTRIBUTE = 4,
		INVALID_ATTRIBUTE = 5,
		CONTENT_ATTRIBUTE = 6,
		DEFAULT_CONTENT_ATTRIBUTE = 7,
		RANGE_ATTRIBUTE = 8,
		UNIT_ATTRIBUTE = 9,
		DESCRIPTION_ATTRIBUTE = 10,
	};

    explicit DATA_CONFIG_MANAGER();
    ~DATA_CONFIG_MANAGER();

    static ZOO_INT32 writeData(const char *component_id, void *data, size_t length);

    static ZOO_INT32 readData(const char *component_id, void *data, size_t length);

    static ZOO_INT32 readConf(const char *component_id, CM4A_GROUP_STRUCT *conf);

    static ZOO_INT32 writeConf(const char *component_id, CM4A_GROUP_STRUCT *conf);

    static std::string getWorkDirectory();

    static std::string getCurrentPath();

private:
    static std::string generatorFile(const char *component_id);
    static int matchHead(const std::string &text);
    static int matchType(const std::string &text, std::string &charLen);
    static int matchValue(const std::string &text, std::string &matchText);
    static int matchSize(const char *component_id, unsigned int length);
	static std::string find_replace(std::string &old, std::string &new_str, std::string &text);
};

}

#endif // DATACONFIGMANAGER_H
