#ifndef CM_FLOW_FACADE_WRAPPER_H
#define CM_FLOW_FACADE_WRAPPER_H

#include "ZOO_type.h"
#include "ZOO.h"
#include "CM4A_type.h"

#include <stddef.h>


#ifdef __cplusplus
	extern "C" {
#endif

ZOO_INT32 CM_write_data(const char* component_id,
                          void* data,
                          size_t length);

ZOO_INT32 CM_read_data(const char* component_id,
                       size_t length,
                       void* data);

ZOO_INT32 CM_read_config(const char *component_id, CM4A_GROUP_STRUCT *conf);

ZOO_INT32 CM_write_config(const char *component_id, CM4A_GROUP_STRUCT *conf);

#ifdef __cplusplus
	}
#endif

#endif // CM_FLOW_FACADE_WRAPPER_H
