#ifndef READDATAPARSER_H
#define READDATAPARSER_H

/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CM
 * File Name    : ReadDataParse.h
 * Description  : read data parser
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-11-19      chaoluo       Create
 ***************************************************************/

#include <string>
#include <cstring>
#include <sstream>

template<class DataType>
class ReadDataParser
{
public:
    ReadDataParser(const std::string &value, char *&pValue)
    {
        std::istringstream is(value);
        DataType data = 0;
        is >> data;
        memcpy(pValue, &data, sizeof(DataType));
        pValue += sizeof(DataType);
    }
};

#endif // READDATAPARSER_H
