#ifndef WRITEDATAPARSE_H
#define WRITEDATAPARSE_H

/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CM
 * File Name    : WriteDataParse.h
 * Description  : write data parser
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-11-19      chaoluo       Create
 ***************************************************************/
 extern "C"
{
	#include "ZOO_type.h"
}
 
#include "DataTypeDefine.h"
#include <string>
#include <cstring>
#include <sstream>

template<class DataType>
class DataTypeToString
{
public:
    DataTypeToString(char *&pValue)
    {
        DataType cData;
        memset(&cData, 0x00, sizeof(DataType));
        memcpy(&cData, pValue, sizeof(DataType));

        std::stringstream ss;
        ss << cData;

        m_value = ss.str();
        pValue += sizeof(DataType);
    }

    std::string getValue() const {return m_value;}

private:
    std::string m_value;
};

class WriteDataParse
{
public:
    WriteDataParse(const std::string &text, const std::string &type)
    {
        m_text = text;
        m_type = type;
    }

    std::string parseData(std::string &value, std::string &strLen, char *&pValue)
    {
        int len = 0;
        std::istringstream is(strLen);
        is >> len;

        char cData[len];
		memset(cData, 0x00, len);
        memcpy(cData, pValue, len);

        std::string data = std::string(cData);
        replaceValue(value, data);

        pValue += len;

        return m_text;
    }

    std::string parseData(std::string &value, char *&pValue)
    {
        std::string str;

        if (m_type.find(STR_ENUM) != std::string::npos)
        {
            DataTypeToString<ZOO_INT32> d2value(pValue);
            str = d2value.getValue();
            replaceValue(value, str);
            return m_text;
        }
		
        if (0 == m_type.compare(STR_ZOO_CHAR)
            || 0 == m_type.compare(STR_ZOO_INT8)
            || 0 == m_type.compare(STR_ZOO_UINT8))
        {
            DataTypeToString<ZOO_CHAR> d2value(pValue);
            str = d2value.getValue();
        }
        else if (0 == m_type.compare(STR_ZOO_INT16))
        {
            DataTypeToString<ZOO_INT16> d2value(pValue);
            str = d2value.getValue();
        }
        else if (0 == m_type.compare(STR_ZOO_UINT16))
        {
            DataTypeToString<ZOO_UINT16> d2value(pValue);
            str = d2value.getValue();
        }
        else if (0 == m_type.compare(STR_ZOO_FLOAT))
        {
            DataTypeToString<ZOO_FLOAT> d2value(pValue);
            str = d2value.getValue();
        }
        else if (0 == m_type.compare(STR_ZOO_DOUBLE))
        {
            DataTypeToString<ZOO_DOUBLE> d2value(pValue);
            str = d2value.getValue();
        }
        else if (0 == m_type.compare(STR_ZOO_LDOUBLE))
        {
            DataTypeToString<ZOO_LDOUBLE> d2value(pValue);
            str = d2value.getValue();
        }
        else if (0 == m_type.compare(STR_ZOO_INT32)
                 || 0 == m_type.compare(STR_INT))
        {
            DataTypeToString<ZOO_INT32> d2value(pValue);
            str = d2value.getValue();
        }
        else if (0 == m_type.compare(STR_ZOO_UINT32))
        {
            DataTypeToString<ZOO_UINT32> d2value(pValue);
            str = d2value.getValue();
        }
        else if (0 == m_type.compare(STR_ZOO_INT64))
        {
            DataTypeToString<ZOO_INT64> d2value(pValue);
            str = d2value.getValue();
        }
        else if (0 == m_type.compare(STR_ZOO_UINT64))
        {
            DataTypeToString<ZOO_UINT64> d2value(pValue);
            str = d2value.getValue();
        }
        else if (0 == m_type.compare(STR_ZOO_ULONG32))
        {
            DataTypeToString<ZOO_ULONG32> d2value(pValue);
            str = d2value.getValue();
        }
        else if (0 == m_type.compare(STR_ZOO_LONG))
        {
            DataTypeToString<ZOO_LONG> d2value(pValue);
            str = d2value.getValue();
        }
        else if (0 == m_type.compare(STR_ZOO_BOOL))
        {
            DataTypeToString<ZOO_BOOL> d2value(pValue);
            str = d2value.getValue();
        }
        else
        {
            return m_text;
        }

        replaceValue(value, str);

        return m_text;
    }

private:

    void replaceValue(std::string &oldValue,
                      std::string &value)
    {
       	std::string _old = "(" + oldValue + ")";
        std::string::size_type startPos = m_text.find(_old);
		if (startPos == std::string::npos)
		{
			return;
		}
	
        std::string _new = "(" + value + ")";
        m_text = m_text.replace(startPos, _old.size(), _new);		
    }

private:
    std::string m_text;
    std::string m_type;

};

#endif // WRITEDATAPARSE_H
