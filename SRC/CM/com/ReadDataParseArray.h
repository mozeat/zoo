#ifndef READDATAPARSEARRAY_H
#define READDATAPARSEARRAY_H

/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CM
 * File Name    : ReadDataParseArray.h
 * Description  : read data, parse array
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-11-19      chaoluo       Create
 ***************************************************************/
 extern "C"
{
	#include "CM4A_type.h"
}
 
#include <cstring>
#include <string>
#include <sstream>

class ReadDataParseArray
{
public:
    ReadDataParseArray(std::string &strLen,
                       const std::string &value,
                       char *&pValue)
    {
        m_error = OK;

        int arrayLen = 0;
        int valueLen = value.length();
        std::istringstream is(strLen);
        is >> arrayLen;

        if (!value.empty())
        {
            if (valueLen >= arrayLen)
            {
                m_error = CM4A_CHAR_SIZE_ERR;
            }
            else
            {
                memcpy(pValue, (char*)(value.c_str()), valueLen);
            }
        }
        else
        {
            memcpy(pValue, "\0", arrayLen);
        }

        pValue += arrayLen;
    }

    inline int getError() const {return m_error;}

private:
    int m_error;
};

#endif // READDATAPARSEARRAY_H
