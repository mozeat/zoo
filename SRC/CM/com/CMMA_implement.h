/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : CMMA_implement.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
#ifndef CMMA_IMPLEMENT_H
#define CMMA_IMPLEMENT_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include <CMMA_event.h>
#include <CM4I_type.h>
#include <CM4A_type.h>


/**
 *@brief CMMA_implement_4A_write_data
 *@param component_id
 *@param data
 *@param length
**/
ZOO_EXPORT void CMMA_implement_4A_write_data(IN const char* component_id,
                                                 IN void* data,
                                                 IN size_t length,
                                                 IN CM4I_REPLY_HANDLE reply_handle);
/**
 *@brief CMMA_implement_4A_read_data
 *@param component_id
 *@param length
 *@param data
**/
ZOO_EXPORT void CMMA_implement_4A_read_data(IN const char* component_id,
                                                IN size_t length,
                                                IN CM4I_REPLY_HANDLE reply_handle);
/**
 *@brief CMMA_implement_4A_read_config
 *@param component_id
 *@param conf
**/
ZOO_EXPORT void CMMA_implement_4A_read_config(IN const char* component_id,
                                                  IN CM4I_REPLY_HANDLE reply_handle);
/**
 *@brief CMMA_implement_4A_write_config
 *@param component_id
 *@param conf
**/
ZOO_EXPORT void CMMA_implement_4A_write_config(IN const char* component_id,
                                                   IN CM4A_GROUP_STRUCT* conf,
                                                   IN CM4I_REPLY_HANDLE reply_handle);

#endif // CMMA_implement.h
