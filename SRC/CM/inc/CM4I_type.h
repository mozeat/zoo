/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : CM4I_type.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-01    Generator      created
*************************************************************/
#ifndef CM4I_TYPE_H
#define CM4I_TYPE_H
#include <MQ4A_type.h>
#include "CM4A_type.h"
#include "CM4A_if.h"


/**
 *@brief Macro Definitions
**/
#define CM4I_COMPONET_ID "CM"
#define CM4A_SERVER     "CM4A_SERVER"
#define CM4I_BUFFER_LENGTH    256
#define CM4I_RETRY_INTERVAL   3

/**
 *@brief Function Code Definitions
**/
#define CM4A_WRITE_DATA_CODE 0x434dff00
#define CM4A_READ_DATA_CODE 0x434dff01
#define CM4A_READ_CONFIG_CODE 0x434dff02
#define CM4A_WRITE_CONFIG_CODE 0x434dff03
#define CM4A_COMPONENT_CHANGE_SUBSCRIBE_CODE 0x434dff04

/*Request and reply header struct*/
typedef struct
{
    MQ4A_SERV_ADDR reply_addr;
    ZOO_UINT32 msg_id;
    ZOO_BOOL reply_wanted;
    ZOO_UINT32 func_id;
}CM4I_REPLY_HANDLER_STRUCT;

typedef  CM4I_REPLY_HANDLER_STRUCT * CM4I_REPLY_HANDLE;

/*Request message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL need_reply;
}CM4I_REQUEST_HEADER_STRUCT;

/*Reply message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL execute_result;
}CM4I_REPLY_HEADER_STRUCT;

/**
*@brief CM4I_WRITE_DATA_CODE_REQ_STRUCT
**/
typedef struct 
{
    char data[CM4A_DATA_MAX_SIZE];
    size_t length;
    char component_id[CM4I_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}CM4I_WRITE_DATA_CODE_REQ_STRUCT;

/**
*@brief CM4I_READ_DATA_CODE_REQ_STRUCT
**/
typedef struct 
{
    size_t length;
    ZOO_CHAR c_filler[4];
    char component_id[CM4I_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}CM4I_READ_DATA_CODE_REQ_STRUCT;

/**
*@brief CM4I_READ_CONFIG_CODE_REQ_STRUCT
**/
typedef struct 
{
    char component_id[CM4I_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}CM4I_READ_CONFIG_CODE_REQ_STRUCT;

/**
*@brief CM4I_WRITE_CONFIG_CODE_REQ_STRUCT
**/
typedef struct 
{
    char component_id[CM4I_BUFFER_LENGTH];
    CM4A_GROUP_STRUCT conf;
    ZOO_CHAR g_filler[8];
}CM4I_WRITE_CONFIG_CODE_REQ_STRUCT;

/**
*@brief CM4I_WRITE_DATA_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}CM4I_WRITE_DATA_CODE_REP_STRUCT;

/**
*@brief CM4I_READ_DATA_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR data[CM4A_DATA_MAX_SIZE];
    ZOO_CHAR g_filler[8];
}CM4I_READ_DATA_CODE_REP_STRUCT;

/**
*@brief CM4I_READ_CONFIG_CODE_REP_STRUCT
**/
typedef struct 
{
    CM4A_GROUP_STRUCT conf;
    ZOO_CHAR g_filler[8];
}CM4I_READ_CONFIG_CODE_REP_STRUCT;

/**
*@brief CM4I_WRITE_CONFIG_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}CM4I_WRITE_CONFIG_CODE_REP_STRUCT;

/**
*@brief CM4I_COMPONENT_CHANGE_SUBSCRIBE_CODE_REP_STRUCT
**/
typedef struct 
{
    CM4A_STATUS_STRUCT status;
    ZOO_CHAR g_filler[8];
}CM4I_COMPONENT_CHANGE_SUBSCRIBE_CODE_REP_STRUCT;

typedef struct
{
    CM4I_REQUEST_HEADER_STRUCT request_header;
    union
    {
        CM4I_WRITE_DATA_CODE_REQ_STRUCT write_data_req_msg;
        CM4I_READ_DATA_CODE_REQ_STRUCT read_data_req_msg;
        CM4I_READ_CONFIG_CODE_REQ_STRUCT read_config_req_msg;
        CM4I_WRITE_CONFIG_CODE_REQ_STRUCT write_config_req_msg;
     }request_body;
}CM4I_REQUEST_STRUCT;


typedef struct
{
    CM4I_REPLY_HEADER_STRUCT reply_header;
    union
    {
        CM4I_WRITE_DATA_CODE_REP_STRUCT write_data_rep_msg;
        CM4I_READ_DATA_CODE_REP_STRUCT read_data_rep_msg;
        CM4I_READ_CONFIG_CODE_REP_STRUCT read_config_rep_msg;
        CM4I_WRITE_CONFIG_CODE_REP_STRUCT write_config_rep_msg;
        CM4I_COMPONENT_CHANGE_SUBSCRIBE_CODE_REP_STRUCT component_change_subscribe_code_rep_msg;
    }reply_body;
}CM4I_REPLY_STRUCT;


/**
*@brief CM4I_COMPONENT_CHANGE_SUBSCRIBE_CODE_CALLBACK_STRUCT
**/
typedef struct 
{
    CM4A_COMPONENT_CHANGE_CALLBACK_FUNCKTION *callback_function;
    void * parameter;
}CM4I_COMPONENT_CHANGE_SUBSCRIBE_CODE_CALLBACK_STRUCT;

#endif //CM4I_type.h
