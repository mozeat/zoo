#ifndef CM4A_TYPE_H
#define CM4A_TYPE_H
/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CM
 * File Name    : CM4A_if.h
 * Description  : 数据追踪
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/

#include <ZOO.h>

#define CM4A_BUFFER_SIZE 32
#define CM4A_ARGC_NUMBER 4

#define CM4A_BASE_ERR                           (0x434d0000)
#define CM4A_SYSTEM_ERR                         (CM4A_BASE_ERR + 0x01)
#define CM4A_PARAMETER_ERR                      (CM4A_BASE_ERR + 0x02)
#define CM4A_FILE_OPEN_ERR                      (CM4A_BASE_ERR + 0x03)
#define CM4A_FILE_SIZE_ERR                      (CM4A_BASE_ERR + 0x04)
#define CM4A_SIZE_READ_ERR                      (CM4A_BASE_ERR + 0x05)
#define CM4A_CHAR_SIZE_ERR                      (CM4A_BASE_ERR + 0x06)
#define CM4A_NAME_SIZE_ERR                      (CM4A_BASE_ERR + 0x07)
#define CM4A_DATA_TYPE_SIZE_ERR                 (CM4A_BASE_ERR + 0x08)
#define CM4A_VALUE_SIZE_ERR                     (CM4A_BASE_ERR + 0x09)
#define CM4A_DEFAULT_VALUE_SIZE_ERR             (CM4A_BASE_ERR + 0x0a)
#define CM4A_RANGES_SIZE_ERR                    (CM4A_BASE_ERR + 0x0b)
#define CM4A_UINT_SIZE_ERR                      (CM4A_BASE_ERR + 0x0c)
#define CM4A_DESCRIPTION_SIZE_ERR               (CM4A_BASE_ERR + 0x0d)
#define CM4A_WRITE_LINE_ERR                     (CM4A_BASE_ERR + 0x0e)
#define CM4A_FILE_PARSE_ERR                     (CM4A_BASE_ERR + 0x10)



#define COMPONENT_ID_CM         ("CM")
#define CM4A_SLASH              ("/")
#define CM4A_SUFFIX             ("CM_data.ini")
#define CM4A_FIELD_LENGTH_MAX   (64)
#define CM4A_ROW_MAX            (512)
#define CM4A_COMPONENT_NUM_MAX  (100)
#define CM4A_RANGES_NUM_MAX     (30)   //枚举类型最大值
#define CM4A_DATA_MAX_SIZE		(4096)


/**
 * @brief 定义 域类型 枚举
 */
typedef enum
{
	CM4A_DATA_TYPE_MIN    = -1,
	CM4A_FIELD_TYPE_GROUP = 0,
	CM4A_FIELD_TYPE_MEMBER = 1,
	CM4A_FIELD_TYPE_MAX
}CM4A_FIELD_TYPE_ENUM;

/**
 * @brief 定义 域数据 结构体
 */
typedef struct
{
	ZOO_BOOL filled;//有填充
	ZOO_INT32 filler;//为了8字节对齐
	ZOO_CHAR  content[CM4A_FIELD_LENGTH_MAX];
}CM4A_FIELD_DATA_STRUCT;

/**
 * @brief 定义 域数据 结构体
 */
typedef struct
{
	ZOO_INT32 tier;//层级
	CM4A_FIELD_TYPE_ENUM type;//数据类型
	ZOO_INT32 filler;//为了8字节对齐
	CM4A_FIELD_DATA_STRUCT default_value;//默认值
    CM4A_FIELD_DATA_STRUCT value;//值
    CM4A_FIELD_DATA_STRUCT unit;//单位
    ZOO_CHAR data_type[CM4A_FIELD_LENGTH_MAX];
    ZOO_CHAR name[CM4A_FIELD_LENGTH_MAX];//name
	CM4A_FIELD_DATA_STRUCT ranges[CM4A_RANGES_NUM_MAX];//值范围
	CM4A_FIELD_DATA_STRUCT description;//说明文
}CM4A_ROW_DATA_STRUCT;

/**
 * @brief 定义 组数据 结构体
 */
typedef struct
{
	ZOO_INT32 rows;
	ZOO_INT32 filler;//为了8字节对齐
    ZOO_CHAR component[CM4A_FIELD_LENGTH_MAX];//component
	CM4A_ROW_DATA_STRUCT members[CM4A_ROW_MAX];
}CM4A_GROUP_STRUCT;

/**
 * @brief 定义 组件数据 结构体
 */
typedef struct
{
	CM4A_GROUP_STRUCT component[CM4A_COMPONENT_NUM_MAX];
}CM4A_COMPONENTS_STRUCT;

typedef  struct
{
    ZOO_CHAR component[CM4A_FIELD_LENGTH_MAX];
}CM4A_STATUS_STRUCT;

#endif // CM4A_TYPE_H
