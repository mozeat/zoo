include ../Makefile_tpl_cov
include ../Project_config

TARGET   := CMMA
SRCEXTS  := .cpp .c 
INCDIRS  := ./inc ./com
SOURCES  := 
SRCDIRS  := ./bin ./lib
CFLAGS   := 
CXXFLAGS := -std=c++14 -fstack-protector-all
CPPFLAGS := -DBOOST_ALL_DYN_LINK
LDFPATH  := -L$(THIRD_PARTY_LIBRARY_PATH)
LDFLAGS  := $(GCOV_LINK) $(LDFPATH) -lMM4A -lMQ4A -lDB4A -lZOO -lCM4A 

include ../Makefile_tpl_linux
