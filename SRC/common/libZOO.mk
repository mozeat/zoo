include ../Makefile_tpl_cov
TARGET   := libZOO.so
SRCEXTS  := .cpp
INCDIRS  := ./ext/inc \
            ./ext/inc/exceptions \
            ./ext/inc/converters \
            ./ext/inc/models \
            ./ext/inc/events \
			./ext/inc/persistences \
			./ext/inc/daos \
			./ext/inc/log \
            ./ext/inc/property_changes \
            ./ext/inc/utils \
            ./ext/typ \
			./int/typ \
            ../zinc
SOURCES  := 

SRCDIRS  := ./int/lib/exceptions \
            ./int/lib/converters \
            ./int/lib/models \
            ./int/lib/events \
			./int/lib/persistences \
			./int/lib/daos \
            ./int/lib/utils \
			./int/lib/log \
			./int/lib
			
CFLAGS   :=
CXXFLAGS :=  -std=c++14 
CPPFLAGS := $(GCOVER_COMP) -fPIC -DBOOST_ALL_DYN_LINK
LDFPATH  := 
LDFLAGS  := $(CPPFLAGS) $(LDFPATH) -lnsl -shared -lTR4A -lMQ4A -lMM4A

include ../Project_config
include ../Makefile_tpl_linux
