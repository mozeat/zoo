/*******************************************************************************
* Copyright (C) 2017, 上海蔚来汽车有限公司
* All rights reserved.
* 产品: ZOO
* 所属组件:
* 模块名称 :
* 文件名称 : ZOO_enum_to_str.h
* 概要描述 : 
* 历史记录 :
* 版本      日期        作者     内容
* 1.0    2017-12-7  孙伟旺    新建
******************************************************************************/
#ifndef ZOO_ENUM_TO_STR_H
#define ZOO_ENUM_TO_STR_H
#include "ZOO.h"
#include "ZOO_tc.h"
#include <string.h>
#ifdef __cplusplus
extern "C"
{
#endif

	/**
	 * @brief Converter sim mode to string.
	 * @param sim_mode
	 */
	const ZOO_CHAR * ZOO_sim_mode_enum_to_str(IN ZOO_SIM_MODE_ENUM sim_mode);
	
	/**
	 * @brief Converter trace mode to string.
	 * @param trace_mode
	 */
	const ZOO_CHAR * ZOO_trace_mode_enum_to_str(IN ZOO_TRACE_MODE_ENUM trace_mode);

	/**
	 * @brief Converter watch mode to string.
	 * @param watch_mode
	 */
	const ZOO_CHAR * ZOO_watch_mode_enum_to_str(IN ZOO_WATCH_MODE_ENUM watch_mode);

	/**
	 * @brief Converter driver state to string.
	 * @param state
	 */
	const ZOO_CHAR * ZOO_driver_state_enum_to_str(IN ZOO_DRIVER_STATE_ENUM state);

	/**
	 * @brief Converter running_mode to string.
	 * @param running_mode
	 */
	const ZOO_CHAR * ZOO_running_mode_enum_to_str(IN ZOO_RUNNING_MODE_ENUM running_mode);

	/**
	 * @brief Converter task_state to string.
	 * @param task_state
	 */
	const ZOO_CHAR * ZOO_task_state_enum_to_str(IN ZOO_TASK_STATE_ENUM task_state);

	/**
	 * @brief Converter server_type to string.
	 * @param server_type
	 */
	const ZOO_CHAR * ZOO_server_type_enum_to_str(IN ZOO_SERVER_TYPE_ENUM server_type);


	/**
	 * @brief Converter cfg_type to string.
	 * @param ota_state
	 */
	const ZOO_CHAR * ZOO_ota_state_enum_to_str(IN ZOO_OTA_STATE_ENUM ota_state);
	
/************************************************str to enum*********************************************/
	/**
	 * @brief Converter string to sim mode emum .
	 * @param sim_mode
	 */
	ZOO_SIM_MODE_ENUM ZOO_str_to_sim_mode(IN const ZOO_CHAR * sim_mode_enum_str);
	
	/**
	 * @brief Converter string to trace mode emum.
	 * @param trace_mode
	 */
	ZOO_TRACE_MODE_ENUM ZOO_str_to_trace_mode(IN const ZOO_CHAR * trace_mode_enum_str);

	/**
	 * @brief Converter string to watch mode emum.
	 * @param watch_mode
	 */
	ZOO_WATCH_MODE_ENUM ZOO_str_to_watch_mode(IN const ZOO_CHAR * watch_mode_enum_str);

	/**
	 * @brief Converter string to driver state emum.
	 * @param state
	 */
	ZOO_DRIVER_STATE_ENUM ZOO_str_to_driver_state(IN const ZOO_CHAR * state_enum_str);

	/**
	 * @brief Converter string to running_mode emum.
	 * @param running_mode
	 */
	ZOO_RUNNING_MODE_ENUM ZOO_str_to_running_mode(IN const ZOO_CHAR * running_mode_enum_str);

	/**
	 * @brief Converter string to task_state emum.
	 * @param task_state
	 */
	ZOO_TASK_STATE_ENUM ZOO_str_to_task_state(IN const ZOO_CHAR * task_state_enum_str);

	/**
	 * @brief Converter string to server_type emum.
	 * @param server_type_enum_str
	 */
	ZOO_SERVER_TYPE_ENUM ZOO_str_to_server_type(IN const ZOO_CHAR * server_type_enum_str);

	/**
	 * @brief Converter string to OTA STATE emum.
	 * @param ota_st_enum_str
	 */
	ZOO_OTA_STATE_ENUM ZOO_str_to_OTA_state(IN const ZOO_CHAR *ota_st_enum_str);
#ifdef __cplusplus
}
#endif

#endif
