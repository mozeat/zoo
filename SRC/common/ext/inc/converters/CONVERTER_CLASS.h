/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : CONVERTER_CLASS.h
 * Description  : The class defines attibutes and methods for converter.
 * History :
 * Version      Date            User              Comments
 * V1.0.0.0     2015-05-04      weiwang         Create file
 ***************************************************************/

#ifndef CONVERTER_CLASS_H_
#define CONVERTER_CLASS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <exception>
#include <sstream>
#include <time.h>

extern "C"
{
#include"ZOO.h"
}

#include <boost/shared_ptr.hpp>
using namespace std;

#define ITOA( x ) dynamic_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()

namespace ZOO_COMMON
{

    class CONVERTER_CLASS
    {
    public:
        virtual ~CONVERTER_CLASS();

        /**
         * @brief Get instance of CONVERTER_CLASS
         */
        static boost::shared_ptr<CONVERTER_CLASS> get_instance();

        /***
         * @brief Convert from long integer to string
         * @param value long integer value
         * @return string value
         */
        template<class T>
        void convert_from_integer_to_string(IN T value, OUT ZOO_CHAR* result)
        {
            std::string str;
            str = ITOA(value);
            strcpy(result, str.c_str());
        }
        /***
         * @brief Convert from boolean to string
         * @param value  boolean value
         * @return string value
         */
        void convert_from_time_t_to_string(IN time_t value, OUT ZOO_CHAR* result);
        /***
         * @brief Convert from string to long integer
         * @param value string value
         * @return long value
         */
        template<class T>
        T convert_from_string_to_integer(IN ZOO_CHAR* value)
        {

            if (value != NULL)
            {
                ZOO_INT32 is_equal = strcmp(value, "");
                if (is_equal != 0)
                {
                    return atoi(value);
                }
            }

            return 0;
        }
        /***
         * @brief Convert from string to time_t
         * @param value string value
         * @return time_t value
         */
        time_t convert_from_string_to_time_t(IN const ZOO_CHAR* value);

    private:
        CONVERTER_CLASS();
        /**
         * @brief Singleton instance of CONVERTER_CLASS
         */
        static boost::shared_ptr<CONVERTER_CLASS> ms_instance;
    };

} /* namespace ZOO_COMMON */

#endif /* CONVERTER_CLASS_H_ */
