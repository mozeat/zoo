/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : NOTIFY_PROPERTY_CHANGED_INTERFACE.h
 * Description  : Provides APIs to handle model's property changed
 * History :
 * Version      Date				User			Comments
 * V1.0.0.0     2018-05-19			MOZEAT			Initialize
 ***************************************************************/
#ifndef NOTIFY_PROPERTY_CHANGED_EX_INTERFACE_H_
#define NOTIFY_PROPERTY_CHANGED_EX_INTERFACE_H_

extern "C"
{
#include "ZOO.h"
}
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>
#include "PROPERTY_CHANGED_OBSERVER_EX_INTERFACE.h"

namespace ZOO_COMMON
{
    /**
     * @brief Provides APIs to handle model's property changed
     */
    template<class T>
    class NOTIFY_PROPERTY_CHANGED_EX_INTERFACE
    {
    public:
        /**
         * @brief Destructor
         */
        virtual ~NOTIFY_PROPERTY_CHANGED_EX_INTERFACE()
        {
        }

        /**
         * @brief Add observer will be notified when property changed
         * @param observer  Property changed observer
         */
        virtual void add_observer(PROPERTY_CHANGED_OBSERVER_EX_INTERFACE<T>* observer) = 0;

        /**
         * @brief Remove observer from subscribe list
         */
        virtual void remove_observer(PROPERTY_CHANGED_OBSERVER_EX_INTERFACE<T>* observer) = 0;

        /**
         * @brief Remove observer from subscribe list
         */
        virtual void clean() = 0;

        /**
         * @brief Notify property has been changed
         * @param property_name     The property has been changed
         */
        virtual void notify_of_property_changed(const ZOO_UINT32 property_name) = 0;
    };

} /* namespace ZOO_COMMON */

#endif /* NOTIFY_PROPERTY_CHANGED_EX_INTERFACE_H_ */
