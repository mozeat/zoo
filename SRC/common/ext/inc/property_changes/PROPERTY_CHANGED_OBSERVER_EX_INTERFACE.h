/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : PROPERTY_CHANGED_OBSERVER_INTERFACE.h
 * Description  : The observer class to listen property change
 * History :
 * Version      Date				User			Comments
 * V1.0.0.0     2018-05-20			MOZEAT			Initialize
 ***************************************************************/
#ifndef PROPERTY_CHANGED_OBSERVER_EX_INTERFACE_H_
#define PROPERTY_CHANGED_OBSERVER_EX_INTERFACE_H_

extern "C"
{
#include "ZOO.h"
}
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>

namespace ZOO_COMMON
{
    /**
     * @brief The observer class to listen property change
     */
    template<class T>
    class PROPERTY_CHANGED_OBSERVER_EX_INTERFACE
    {
    public:
        /**
         * @brief Destructor
         */
        virtual ~PROPERTY_CHANGED_OBSERVER_EX_INTERFACE()
        {
        }

        /**
         * @brief This method is executed when property changed value
         * @param model             The model type (robot, prealign unit, lot, carrier,...)
         * @param property_name     The property has been changed value
         */
        virtual void on_property_changed(boost::shared_ptr<T> model,
                                         const ZOO_UINT32 property_name) = 0;
    };

} /* namespace WH */

#endif /* PROPERTY_CHANGED_OBSERVER_EX_INTERFACE_H_ */
