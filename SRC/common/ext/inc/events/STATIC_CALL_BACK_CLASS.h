/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : STATIC_CALL_BACK_CLASS.h
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     20180-06-28			MOZEAT		Initialize
 ***************************************************************/
#ifndef STATIC_CALL_BACK_CLASS_H_
#define STATIC_CALL_BACK_CLASS_H_
#include "CALL_BACK_CLASS.h"
#include "ZOO_COMMON_MACRO_DEFINE.h"
namespace ZOO_COMMON
{

    /**
     * @brief Class description
     */
    template<typename EventType>
    class STATIC_CALL_BACK_CLASS : public CALL_BACK_CLASS<EventType>
    {
    public:
        /**
         * @brief Destructor
         */
        virtual ~STATIC_CALL_BACK_CLASS()
        {
        }

        /**
         * @brief Default constructor
         */
        STATIC_CALL_BACK_CLASS(void (*func)(boost::shared_ptr<EventType>))
                : func_(func)
        {

        }

        /**
         * @brief execute method
         */
        virtual void execute(boost::shared_ptr<EventType>    event_type)
        {
            (*func_)(event_type);
        }

    private:
        void (*func_)(boost::shared_ptr<EventType>);
    };

} /* namespace ZOO_COMMON */

#endif /* STATIC_CALL_BACK_CLASS_H_ */
