/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : STATIC_CALL_BACK_CLASS.h
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-06-28			MOZEAT		Initialize
 ***************************************************************/
#ifndef METHOD_CALL_BACK_CLASS_H_
#define METHOD_CALL_BACK_CLASS_H_
#include "CALL_BACK_CLASS.h"
#include "ZOO_COMMON_MACRO_DEFINE.h"
namespace ZOO_COMMON
{

    /**
     * @brief Class description
     */
    template<typename EventType, typename T, typename Method>
    class METHOD_CALL_BACK_CLASS : public CALL_BACK_CLASS<EventType>
    {
    public:
        /**
         * @brief Destructor
         */
        virtual ~METHOD_CALL_BACK_CLASS()
        {
        }

        /**
         * @brief Default constructor
         */
        METHOD_CALL_BACK_CLASS(void *object, Method method)
                : m_object(object), m_method(method)
        {
        }
        /**
         * @brief execute method
         */
        virtual void execute(boost::shared_ptr<EventType> event_type)
        {
            T *obj = static_cast<T *>(m_object);
            return (obj->*m_method)(event_type);
        }

        /**
         * @brief Check current is subcribe
         */
        bool is_register(T *object, Method method)
        {
            return this->m_object == object && this->m_method == method;
        }

        /**
         * @brief Check current is subcribe
         */
        bool is_register(T *object)
        {
            return this->m_object == object;
        }

    private:
        /**
         * @brief The object values
         */
        void *m_object;

        /**
         * @brief The method values
         */
        Method m_method;

    };

} /* namespace ZOO_COMMON */

#endif /* STATIC_CALL_BACK_CLASS_H_ */
