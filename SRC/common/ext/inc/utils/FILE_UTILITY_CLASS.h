/**************************************************************
 * Copyright (C) 2015, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : FILE_UTILITY_CLASS.h
 * Description  : The class provides APIs to execute file operations
 * History :
 * Version      Date				User			Comments
 * V1.0.0.0     2015-06-18			MOZEAT			Initialize
 ***************************************************************/
#ifndef FILE_UTILITY_CLASS_H_
#define FILE_UTILITY_CLASS_H_

extern "C"
{
#include "ZOO.h"
}
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <sys/stat.h>
#include <boost/shared_ptr.hpp>

using namespace std;

namespace ZOO_COMMON
{
    /**
     * @brief The class provides APIs to execute file operations
     */
    class FILE_UTILITY_CLASS
    {
    public:
        /**
         * @brief Destructor
         */
        virtual ~FILE_UTILITY_CLASS();

        /**
         * @brief get instance of FILE_UTILITY_CLASS
         */
        static boost::shared_ptr<FILE_UTILITY_CLASS> get_instance();

        /**
         * @brief Get all files located in specified directory recursively
         * @param dir       The directory contains files
         * @param files     The files located in specified directory
         * @return Error code for this function
         */
        ZOO_INT32 get_all_files(string dir, map<string, string>& files, string file_type = ".xml");

    private:
        /**
         * @brief Constructor
         */
        FILE_UTILITY_CLASS();

        /**
         * @brief Determine whether or not the specified one is a directory
         * @param dir       The specified one need to be checked
         * @return True: is directory, False: otherwise
         */
        ZOO_BOOL is_directory(string dir);

    private:
        /**
         * @brief The singleton instance
         */
        static boost::shared_ptr<FILE_UTILITY_CLASS> m_instance;
    };

} /* namespace ZOO_COMMON */

#endif /* FILE_UTILITY_CLASS_H_ */
