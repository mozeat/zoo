/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : STRING_UTIL_CLASS.h
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-06-28	    MOZEAT		Initialize
 ***************************************************************/
#ifndef COMMON_STRING_UTIL_CLASS_H_
#define COMMON_STRING_UTIL_CLASS_H_
extern "C"
{
#include <ZOO.h>
}

#include <string>
#include <cstring>
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>

using namespace std;

namespace ZOO_COMMON
{
    /**
     * @brief Class description
     */
    class COMMON_STRING_UTIL_CLASS
    {
    public:

        /**
         * @brief Destructor
         */
        virtual ~COMMON_STRING_UTIL_CLASS();

        /**
         * @brief Copy source string to destination string
         * @param OUT target
         * @param IN value
         */
        static void copy(OUT ZOO_CHAR* target, IN const ZOO_CHAR* value);

        /**
         * @brief Copy source string to destination string
         * @param OUT target
         * @param IN value
         */
        static void copy(OUT boost::shared_array<ZOO_CHAR>& target, IN const ZOO_CHAR* value);

        /**
         * @brief Format string
         * @param OUT target
         * @param IN format
         */
        static void format(OUT ZOO_CHAR* target, IN const ZOO_CHAR* format, ...);
    };

} /* namespace ZOO_COMMON */
#endif /* COMMON_STRING_UTIL_CLASS_H_ */
