/**************************************************************
 * Copyright (C) 2015, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : SYNCHRONIZATION_CLASS.h
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-06-28	        MOZEAT		        Initialize
 ***************************************************************/
#ifndef SYNCHRONIZATION_CLASS_H_
#define SYNCHRONIZATION_CLASS_H_
#include <map>
#include <string>
extern "C"
{
#include <ZOO.h>
}

#include "AUTO_RESET_EVENT_CLASS.h"
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/recursive_mutex.hpp>

namespace ZOO_COMMON
{

    /**
     * @brief Class description
     */
    class SYNCHRONIZATION_CLASS
    {
    public:

        /**
         * @brief Deconstructor
         */
        virtual ~SYNCHRONIZATION_CLASS();

        /**
         * @brief Get singleton instance
         * @return
         */
        static boost::shared_ptr<SYNCHRONIZATION_CLASS> get_instance();

        /**
         * @brief Set lock current object with key
         * @param key
         */
        void lock(std::string key);

        /**
         * @brief Lock current object by thread
         * @param key
         */
        void lock_by_thread(std::string key);

        /**
         * @brief Releave lock current object with key
         * @param key
         */
        void unlock(std::string key);

        /**
         * @brief Releave lock current object with key
         * @param key
         */
        void unlock_by_thread(std::string key);

        /**
         * @brief Reset lock
         * @param key
         */
        void reset_lock(std::string key);

        /**
        * @brief Get lock state
        * @param key
        * @return thread is locked or not
        */
        ZOO_BOOL get_lock_state(std::string key);

    private:
        /**
         * @brief Default constructor
         */
        SYNCHRONIZATION_CLASS();

        /**
         * @brief Lock current object by thread
         * @param key
         */
        void do_lock_thread(std::string key);

        /**
         * @brief Get mutex lock with key
         * @param key
         * @return
         */
        boost::shared_ptr<boost::recursive_mutex> get_lock(std::string key);

        /**
         * @brief Get mutex lock with key
         * @param key
         * @return
         */
        boost::shared_ptr<AUTO_RESET_EVENT_CLASS> get_wait_event(std::string key);

        /**
         * @brief Get mutex lock with key
         * @param key
         * @return
         */
        boost::shared_ptr<AUTO_RESET_EVENT_CLASS> get_lock_status_event(std::string key);

        /**
         * @brief The singleton instance
         */
        static boost::shared_ptr<SYNCHRONIZATION_CLASS> m_instance;

        /**
         * @brief The synchronize global value
         */
        boost::recursive_mutex m_syn_global;

        /**
         * @brief The stored locks
         */
        std::map<std::string, boost::shared_ptr<boost::recursive_mutex> > m_locks;

        /**
         * @brief The stored locks
         */
        std::map<std::string, boost::shared_ptr<AUTO_RESET_EVENT_CLASS> > m_wait_events;

        /**
         * @brief The stored locks
         */
        std::map<std::string, boost::shared_ptr<AUTO_RESET_EVENT_CLASS> > m_lock_status_events;

        /**
         * @brief The stored lock flags
         */
        std::map<std::string, ZOO_BOOL> m_lock_flags;
    };

} /* namespace ZOO_COMMON */

#endif /* SYNCHRONIZATION_CLASS_H_ */
