/**************************************************************
 * Copyright (C) 2018, SHANGHAI  CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : THREAD_POOL.h
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-09-16		    MOZEAT		        Initialize
 ***************************************************************/
#ifndef THREAD_POOL_H_
#define THREAD_POOL_H_

#include <stdio.h>
extern "C"
{
#include <ZOO.h>
}

#include <map>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/make_shared.hpp>
#include "ERROR_MESSAGE_HANDLER_INTERFACE.h"
#include <exception>      // std::set_terminate
#include <cstdlib>        // std::abort

#define MAX_THREAD 20
#define INIT_THREAD 5
#define INVAL_THREAD 2

namespace ZOO_COMMON
{
    typedef boost::packaged_task<int> task_t;
    typedef boost::shared_ptr<task_t> ptask_t;
    class THREAD_POOL
    {
    public:

        /**
         * @brief Deconstructor
         */
        virtual ~THREAD_POOL();

        /**
         * @brief Get singleton instance
         * @return
         */
        static boost::shared_ptr<THREAD_POOL> get_instance();

        /**
         * @brief Startup
         */
        void startup();

        /**
         * @brief Shutdown
         */
        void shutdown();

        /**
         * #@brief Update threads
         */
        void update_threads();

        /**
         * @brief Adds a task to the thread pool if a thread is currently available.
         * @param task
         */
        template<typename Task>
        void queue_working(Task task, std::vector<boost::shared_future<int> >& pending_data,
                           ERROR_MESSAGE_HANDLER_INTERFACE* m_error_handler = NULL)
        {
            boost::unique_lock<boost::mutex> lock(this->m_task_syn_lock);

            this->update_threads();
            if (this->m_available_thread == 0)
            {
                return;
            }

            // Decrement count, indicating thread is no longer available.
            --this->m_available_thread;

            // Post a wrapped task into the queue.
            ptask_t run_task = boost::make_shared<task_t>(
                    boost::bind(&THREAD_POOL::wrap_task, this, boost::function<void()>(task),
                                m_error_handler));
            boost::shared_future<int> fut(run_task->get_future());
            pending_data.push_back(fut);
            this->m_io_context.post(boost::bind(&task_t::operator(), run_task));
        }

        /**
         * @brief Adds a task to the thread pool if a thread is currently available.
         * @param task
         */
        template<typename Task>
        void queue_working(Task task, ERROR_MESSAGE_HANDLER_INTERFACE* m_error_handler = NULL)
        {
            std::vector<boost::shared_future<int> > pending_data;
            queue_working(task, pending_data, m_error_handler);
        }

        /**
         * @brief On handle signal
         * @param signal
         */
        static void on_handle_signal(int signal);

        /**
         * @brief On handle signal
         * @param signal
         */
        static void on_handle_signal();
    private:

        /**
         * @brief Create count
         * @param count
         */
        void create_threads(int count);

        /**
         * @brief Wrap a task so that the available count can be increased once
         * the user provided task has completed.
         * @param task
         */
        int wrap_task(boost::function<void()> task,
                      ERROR_MESSAGE_HANDLER_INTERFACE* m_error_handler);

        /**
         * @brief Deconstructor
         */
        THREAD_POOL();

        /**
         * @brief Run thread
         */
        void initialize_thread();

        /**
         * @brief Single ton instance variable
         */
        static boost::shared_ptr<THREAD_POOL> m_instance;

        /**
         * @brief The io context
         */
        boost::asio::io_context m_io_context;

        /**
         * @brief The work
         */
        boost::asio::io_context::work m_work;

        /**
         * @brief The thread group
         */
        boost::thread_group m_threads;

        /**
         * @brief The available thread
         */
        std::size_t m_available_thread;

        /**
         * @brief Create thread
         */
        int m_create_thread;

        /**
         * @brief The lock mutex
         */
        boost::mutex m_task_syn_lock;

        /**
         * @brief Is startup
         */
        ZOO_BOOL m_is_startup;

        /**
         * @brief The syn lock
         */
        boost::mutex m_syn_lock;
    };

} /* namespace ZOO_COMMON */

#endif /* THREAD_POOL_H_ */
