/**************************************************************
 * Copyright (C) 2018, SHANGHAI  CO., LTD
 * All rights reserved.
 * Product No.  : COMMON
 * Component ID : SCHEDULER
 * File Name    : AUTO_RESET_EVENT_CLASS.h
 * Description  : This class is used to support synchronization between
 *                  thread via events.
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-05-18			MOZEAT		Initialize
 ***************************************************************/
#ifndef AUTO_RESET_EVENT_CLASS_H_
#define AUTO_RESET_EVENT_CLASS_H_
extern "C"
{
#include <ZOO.h>
}

#include <ZOO_COMMON_MACRO_DEFINE.h>
#include "boost/thread.hpp"
#include "boost/thread/condition.hpp"
#include "boost/thread/condition_variable.hpp"
#include "boost/atomic.hpp"

namespace ZOO_COMMON
{
    /**
     * @brief This class is used to support synchronization between thread via events.
     */
    class AUTO_RESET_EVENT_CLASS
    {
    public:
        /**
         * @brief Default constructor
         */
        AUTO_RESET_EVENT_CLASS();

        /**
         * @brief Constructor with initialize lock state
         */
        AUTO_RESET_EVENT_CLASS(ZOO_BOOL initial);

        /**
         * @brief Deconstructor
         */
        virtual ~AUTO_RESET_EVENT_CLASS();

        /**
         * @brief Signal to resume other thread
         */
        void set();

        /**
         * @brief Reset current state
         */
        void reset();

        /**
         * @brief Suspend current thread and waiting signal resume from other thread
         * @return
         */
        void wait_one();

        /**
         * @brief Suspend current thread until duration of timer
         * Duration unit by milliseconds
         * @return
         */
        ZOO_BOOL wait_one(ZOO_UINT32 timer);

        /**
         * @brief The execution of the current thread (which shall have locked lock's mutex) is blocked during delay_time
         * @param The time span during which the thread will block waiting to be notified (delay_time in second)
         */
        void sleep_for(ZOO_UINT32 delay_time);

    private:

        /**
         * @brief Define none copy
         * @param
         */
        AUTO_RESET_EVENT_CLASS(const AUTO_RESET_EVENT_CLASS&);

        /**
         * @brief Define none copy
         * @param
         */
        AUTO_RESET_EVENT_CLASS& operator=(const AUTO_RESET_EVENT_CLASS&); // non-copyable

        /**
         * @brief The current state
         */
        boost::atomic<ZOO_BOOL> m_flag;

        /**
         * @brief The protect lib
         */
        boost::mutex m_protect;

        /**
         * @brief The concurrence signal to handler current thread
         */
        boost::condition_variable_any m_signal;

    };

} /* namespace ZOO_COMMON */

#endif /* AUTO_RESET_EVENT_CLASS_H_ */
