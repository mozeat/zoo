/**************************************************************
 * Copyright (C) 2018, SHANGHAI  CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : FILE_UTILITY_CLASS.h
 * Description  : The class provides APIs to execute file operations
 * History :
 * Version      Date				User			Comments
 * V1.0.0.0     2018-06-18			MOZEAT			Initialize
 ***************************************************************/

#ifndef MANUAL_RESET_EVENT_CLASS_H_
#define MANUAL_RESET_EVENT_CLASS_H_
extern "C"
{
#include <ZOO.h>
}

#include <ZOO_COMMON_MACRO_DEFINE.h>
#include "boost/thread.hpp"
#include "boost/thread/condition.hpp"
#include "boost/thread/condition_variable.hpp"
#include "boost/atomic.hpp"

namespace ZOO_COMMON
{

    class MANUAL_RESET_EVENT_CLASS
    {
    public:
        /**
         * @brief Default constructor
         */
        MANUAL_RESET_EVENT_CLASS();

        /**
         * @brief Constructor with initialize lock state
         */
        MANUAL_RESET_EVENT_CLASS(ZOO_BOOL initial);

        /**
         * @brief Deconstructor
         */
        virtual ~MANUAL_RESET_EVENT_CLASS();

        /**
         * @brief Signal to resume other thread
         */
        void set();

        /**
         * @brief Reset current state
         */
        void reset();

        /**
         * @brief Suspend current thread and waiting signal resume from other thread
         * @return
         */
        void wait();

    private:

        /**
         * @brief Define none copy
         * @param
         */
        MANUAL_RESET_EVENT_CLASS(const MANUAL_RESET_EVENT_CLASS&);

        /**
         * @brief Define none copy
         * @param
         */
        MANUAL_RESET_EVENT_CLASS& operator=(const MANUAL_RESET_EVENT_CLASS&); // non-copyable

        /**
         * @brief The current state
         */
        boost::atomic<ZOO_BOOL> m_flag;

        /**
         * @brief The protect lib
         */
        boost::mutex m_protect;

        /**
         * @brief The concurrence signal to handler current thread
         */
        boost::condition_variable_any m_signal;
    };

} /* namespace ZOO_COMMON */

#endif /* MANUAL_RESET_EVENT_CLASS_H_ */
