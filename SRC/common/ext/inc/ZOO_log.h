/*******************************************************************************
* Copyright (C) 2017, 上海蔚来汽车有限公司
* All rights reserved.
* 产品: ZOO
* 所属组件:
* 模块名称 :
* 文件名称 : ZOO_log.h
* 概要描述 : 
* 历史记录 :
* 版本      日期        作者     内容
* 1.0    2017-12-7  孙伟旺    新建
******************************************************************************/
#ifndef ZOO_LOG_H_
#define ZOO_LOG_H_
#include "ZOO_tc.h"

#ifdef __cplusplus
ZOO_EXPORT {
#endif

void ZOO_set_log_config_dir(const char * dir);
void ZOO_syslog_init(ZOO_LOG_SERVER_STRUCT* server);
void ZOO_log_init(const char * output_dir,const char * cfg_file);

/**
 *@brief print log 
 *@param level         the severity level
 *@param function_name     
 *@param formate_spec  the content ...
 */
void ZOO_log_t(IN ZOO_SEVERITY_LEVEL_ENUM level,
                        IN const char * resource_id,
						IN const char * component_id,
						IN const char * thread_id,
						IN const char * format_spec);
/**
 *@brief print log 
 *@param level         the severity level
 *@param function_name     
 *@param formate_spec  the content ...
 */
void ZOO_log_f(IN ZOO_SEVERITY_LEVEL_ENUM level,
                        IN const char * resource_id,
						IN const char * component_id,
						IN const char * format_spec);

/**
 *@brief print log 
 *@param level         the severity level
 *@param formate_spec  the content ...
 */
void ZOO_log_s(IN ZOO_SEVERITY_LEVEL_ENUM level,
                        IN const char * function_name,
                            IN const char * format_spec);	

#ifdef __cplusplus
}
#endif

#endif