/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : DAO
 * File Name    : DB_EXCEPTION_CLASS_H_
 * Description  : The exception in DAO component.
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-05-09			MOZEAT		Initialize
 ***************************************************************/
#ifndef DB_EXCEPTION_CLASS_H_
#define DB_EXCEPTION_CLASS_H_
#include "PARAMETER_EXCEPTION_CLASS.h"

using namespace ZOO_COMMON;

namespace DAO
{
    /**
     * @brief The exception in SCHEDULER component.
     */
    class DB_EXCEPTION_CLASS : public PARAMETER_EXCEPTION_CLASS
    {
    public:

        /**
         * @brief Constructor
         * @param error_code        Error code
         * @param error_message     Error message
         * @param inner_exception   Inner exception
         */
        DB_EXCEPTION_CLASS(ZOO_INT32 error_code, const ZOO_CHAR* error_message,
                           std::exception* inner_exception)
                : PARAMETER_EXCEPTION_CLASS(error_code, error_message, inner_exception)
        {
        }

        /**
         * @brief Destructor
         */
        virtual ~DB_EXCEPTION_CLASS() throw ()
        {
        }
    };

} /* namespace DAO */
#endif /* DB_EXCEPTION_CLASS_H_ */
