/**************************************************************
 * Copyright (C) 2015, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : EH
 * File Name    : ZOO_COMMON_MACRO_DEFINE.h
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0       2018-03-13	    weiwang.sun		Initialize
 ***************************************************************/
#ifndef ZOO_COMMON_MACRO_DEFINE_H_
#define ZOO_COMMON_MACRO_DEFINE_H_
#include <string>
#include <PARAMETER_EXCEPTION_CLASS.h>
#include <SYSTEM_SIGNAL_HANDLER.h>
extern "C"
{
    //#include <EH4A_if.h>
    //#include <TR4A_if.h>
    #include <stdio.h>
    #include "ZOO_if.h"
}

/**
 * @brief The default signal handler register
 */
#define __ZOO_SYSTEM_SIGNAL_REGISTER(componet_id,sig) ZOO_COMMON::SYSTEM_SIGNAL_HANDLER::get_instance()->register_signal_by_default_handler(componet_id,sig)

/**
 * @brief Get class name and function name like "class::function()"
 * @param pretty_function   Class name and function name
 */
inline std::string function_name(const std::string& pretty_function)
{
    size_t colons = pretty_function.find("(");
    size_t begin = pretty_function.substr(0, colons).rfind(" ") + 1;
    size_t end = pretty_function.rfind("(") - begin;

    std::string function_name_str = pretty_function.substr(begin, end) + "()";

    return function_name_str;
}

#define __FUNCTION_NAME__ function_name(__PRETTY_FUNCTION__).c_str()


/**
 * @brief Define a macro to handle try-catch exception caught in controller, facade layer
 * @param error_code                    The error code
 * @param continue_throw_exception      True: continue throw exception, False: otherwise
 */
#define __ZOO_TRY try

#define __ZOO_CATCH catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & ex)

/**
 * @brief Define a macro to throw ZOO exception
 * @param error_code        The error code
 * @param error_message     The error message
 * @param inner_exception   The inner exception
 */
#define __THROW_ZOO_EXCEPTION(component,error_code, error_message, inner_exception) \
    ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__FUNCTION__,"error_code:%d,error_message:%s",error_code,error_message); \
    throw ZOO_COMMON::PARAMETER_EXCEPTION_CLASS(error_code, error_message, inner_exception);
//TR4A_trace(component, __FUNCTION_NAME__, 
//			 ": function error - [%s]", error_message); 
//EH4A_show_exception(component,__FILE__,__FUNCTION_NAME__,__LINE__,error_code,0,error_message); 

#endif /* ZOO_COMMON_MACRO_DEFINE_H_ */
