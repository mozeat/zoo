/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : EVENT_AGGREGATOR_CLASS.cpp
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-08-22		MOZEAT		Initialize
 ***************************************************************/
#include "EVENT_AGGREGATOR_CLASS.h"

namespace ZOO_COMMON
{

    boost::shared_ptr<EVENT_AGGREGATOR_CLASS> EVENT_AGGREGATOR_CLASS::m_instance;

    EVENT_AGGREGATOR_CLASS::EVENT_AGGREGATOR_CLASS()
    {
        // TODO Auto-generated constructor stub

    }

    EVENT_AGGREGATOR_CLASS::~EVENT_AGGREGATOR_CLASS()
    {
        // TODO Auto-generated destructor stub
    }

    /**
     * @brief Get single ton instance
     */
    boost::shared_ptr<EVENT_AGGREGATOR_CLASS> EVENT_AGGREGATOR_CLASS::get_instance()
    {
        if (NULL == EVENT_AGGREGATOR_CLASS::m_instance)
        {
            EVENT_AGGREGATOR_CLASS::m_instance.reset(new EVENT_AGGREGATOR_CLASS());
        }

        return EVENT_AGGREGATOR_CLASS::m_instance;
    }

} /* namespace ZOO */
