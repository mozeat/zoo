/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : ENVIRONMENT_UTILITY_CLASS.cpp
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0       2018-07-02		MOZEAT		Initialize
 ***************************************************************/
#include "ENVIRONMENT_UTILITY_CLASS.h"
#include <boost/filesystem.hpp>
#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <stdio.h>

namespace ZOO_COMMON
{
    ZOO_CHAR COMMON_COMPONENT_ID[3] = "CM";

#define CONFIGURE_BASE_LEVEL 2

    /**
     * @brief Default constructor
     */
    ENVIRONMENT_UTILITY_CLASS::ENVIRONMENT_UTILITY_CLASS()
    {
        // TODO Auto-generated constructor stub

    }

    /**
     * @brief Deconstructor
     */
    ENVIRONMENT_UTILITY_CLASS::~ENVIRONMENT_UTILITY_CLASS()
    {
        // TODO Auto-generated destructor stub
    }

    /**
     * @brief Get current user name
     */
    string ENVIRONMENT_UTILITY_CLASS::get_current_user()
    {
        return "ZOO";
    }

    /**
     * @brief Combine path
     * @param dir
     * @param file_name
     * @return
     */
    string ENVIRONMENT_UTILITY_CLASS::combine_path(string dir, string file_name)
    {
        return dir.append(SLASH_CHAR).append(file_name);
    }

    /**
     * @brief Get full path
     * @param dir
     * @param file_name
     * @return
     */
    string ENVIRONMENT_UTILITY_CLASS::get_full_path(string file_name)
    {
        return combine_path(get_execute_path(), file_name);
    }

    /**
     * @brief Get dir
     * @param file_path
     * @return
     */
    string ENVIRONMENT_UTILITY_CLASS::get_dir(string file_path)
    {
        size_t last = file_path.find_last_of(SLASH_CHAR);
        return file_path.substr(0, last + 1);
    }

    /**
     * @brief Get parent dir
     * @param level
     * @return
     */
    string ENVIRONMENT_UTILITY_CLASS::get_parent_dir(string dir, int level)
    {
        boost::filesystem::path current_dir(dir);
        while (level > 0)
        {
            if (!current_dir.has_parent_path())
            {
                break;
            }

            current_dir = current_dir.parent_path();

            level--;
        }

        return current_dir.string();

    }

    /**
     * @brief Get configure base
     * @return
     */
    string ENVIRONMENT_UTILITY_CLASS::get_configure_base()
    {
        string execute_base = get_execute_path();
        int level = CONFIGURE_BASE_LEVEL;
        return get_parent_dir(execute_base, level);
    }

    /**
     * @brief Get the execute base
     * @return
     */
    string ENVIRONMENT_UTILITY_CLASS::get_execute_path()
    {
        string execute_base = boost::filesystem::current_path().string();
        return execute_base;
    }

    /**
     * @brief Get thread id
     * @return
     */
    string ENVIRONMENT_UTILITY_CLASS::get_thread_id()
    {
        std::string id = boost::lexical_cast<std::string>(boost::this_thread::get_id());
        return id;
    }

    /**
     * @brief Get thread number
     * @return
     */
    ZOO_INT32 ENVIRONMENT_UTILITY_CLASS::get_thread_number()
    {
        ZOO_INT32 id = boost::lexical_cast<ZOO_INT32>(boost::this_thread::get_id());
        return id;
    }

    /**
     * @brief Get all files located in specified directory recursively
     * @param dir       The directory contains files
     * @param files     The files located in specified directory
     * @return Error code for this function
     */
    ZOO_INT32 ENVIRONMENT_UTILITY_CLASS::get_all_files(string dir, map<string, string>& files,
                                                        std::string file_type)
    {
        boost::filesystem::path cur_dir(dir);
        if (!boost::filesystem::exists(cur_dir))
        {
            return errno;
        }

        boost::filesystem::recursive_directory_iterator it(cur_dir);
        boost::filesystem::recursive_directory_iterator endit;
        while (it != endit)
        {
            if (boost::filesystem::is_regular_file(*it) && it->path().extension() == file_type)
            {
                files[convert_path_to_string(it->path().filename())] = convert_path_to_string(
                        it->path());
            }
            ++it;
        }

        return OK;
    }

    /**
     * @brief Convert path to string
     */
    string ENVIRONMENT_UTILITY_CLASS::convert_path_to_string(boost::filesystem::path path)
    {
        // string result = boost::lexical_cast<string>(path);
        return path.string();
    }

    /**
     * @brief Get trace info
     * @param module
     * @param function
     * @param context
     * @return
     */
    string ENVIRONMENT_UTILITY_CLASS::get_trace_info(const ZOO_CHAR* module,
                                                     const ZOO_CHAR* function,
                                                     const ZOO_CHAR* context)
    {
        ZOO_CHAR buffer[255];
        int length = sprintf(buffer, "<%s> - [%s] -> marking_code[%s]", module, function, context);
        if (length < 255)
        {
            buffer[length] = '\0';
        }

        string trace_info(buffer);
        return trace_info;
    }

} /* namespace ZOO_COMMON */
