/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : MANUAL_RESET_EVENT_CLASS.cpp
 * Description  : The class provides APIs to execute file operations
 * History :
 * Version      Date				User			Comments
 * V1.0.0.0     2018-06-18			MOZEAT			Initialize
 ***************************************************************/
#include "MANUAL_RESET_EVENT_CLASS.h"
#include "ENVIRONMENT_UTILITY_CLASS.h"

namespace ZOO_COMMON
{
    /**
     * @brief Define component ID
     */
    extern ZOO_CHAR COMMON_COMPONENT_ID[3];

    /**
     * @brief Default constructor
     */
    MANUAL_RESET_EVENT_CLASS::MANUAL_RESET_EVENT_CLASS()
            : m_flag(ZOO_FALSE)
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": create instance from thread[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Constructor with initialize lock state
     */
    MANUAL_RESET_EVENT_CLASS::MANUAL_RESET_EVENT_CLASS(ZOO_BOOL initial)
            : m_flag(initial)
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": create instance from thread[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Deconstructor
     */
    MANUAL_RESET_EVENT_CLASS::~MANUAL_RESET_EVENT_CLASS()
    {
       /* TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": destroy instance from thread[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Signal to resume other thread
     */
    void MANUAL_RESET_EVENT_CLASS::set()
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call set from thread[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());*/
        {
            boost::lock_guard<boost::mutex> synchronize(m_protect);
            this->m_flag = ZOO_TRUE;
        }

        this->m_signal.notify_all();
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call set from thread[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Reset current state
     */
    void MANUAL_RESET_EVENT_CLASS::reset()
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call reset from thread[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());*/
        boost::lock_guard<boost::mutex> synchronize(m_protect);
        this->m_flag = ZOO_FALSE;
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call reset from thread[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/

    }

    /**
     * @brief Suspend current thread and waiting signal resume from other thread
     * @return
     */
    void MANUAL_RESET_EVENT_CLASS::wait()
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call wait from thread[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());*/
        boost::unique_lock<boost::mutex> synchronize(m_protect);
        while (!this->m_flag) // prevent spurious wakeups from doing harm
        {
            this->m_signal.wait(synchronize);
        }

        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call wait from thread[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

} /* namespace COMMON_UTILS */
