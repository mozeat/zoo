/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : SYNCHRONIZATION_CLASS.cpp
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-08-28			MOZEAT		Initialize
 ***************************************************************/
#include "SYNCHRONIZATION_CLASS.h"
#include "THREAD_POOL.h"
extern "C"
{
//#include "TR4A_if.h"
}

#include "ENVIRONMENT_UTILITY_CLASS.h"

namespace ZOO_COMMON
{
    /**
     * @brief Define component ID
     */
    extern ZOO_CHAR COMMON_COMPONENT_ID[3];

    /**
     * @brief Initialize default value
     */
    boost::shared_ptr<SYNCHRONIZATION_CLASS> SYNCHRONIZATION_CLASS::m_instance;

    /**
     * @brief Default constructor
     */
    SYNCHRONIZATION_CLASS::SYNCHRONIZATION_CLASS()
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": create instance from thread[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Deconstructor
     */
    SYNCHRONIZATION_CLASS::~SYNCHRONIZATION_CLASS()
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": destroy instance from thread[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Get singleton instance
     * @return
     */
    boost::shared_ptr<SYNCHRONIZATION_CLASS> SYNCHRONIZATION_CLASS::get_instance()
    {
        if (SYNCHRONIZATION_CLASS::m_instance == NULL)
        {
            SYNCHRONIZATION_CLASS::m_instance.reset(new SYNCHRONIZATION_CLASS());
        }

        return SYNCHRONIZATION_CLASS::m_instance;
    }

    /**
     * @brief Set lock current object with key
     * @param key
     */
    void SYNCHRONIZATION_CLASS::lock(std::string key)
    {
       /* TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call lock from thread[%s] with key[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());*/
        boost::shared_ptr<boost::recursive_mutex> lock = this->get_lock(key);
        lock->lock();
        this->m_lock_flags[key] = ZOO_TRUE;
       /* TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call lock from thread[%s] with key[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Lock current object by thread
     * @param key
     */
    void SYNCHRONIZATION_CLASS::lock_by_thread(std::string key)
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call lock_by_thread from thread[%s] with key[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());*/
        boost::shared_ptr<AUTO_RESET_EVENT_CLASS> wait_event = this->get_lock_status_event(key);

//        THREAD_POOL::get_instance()->queue_working(
//                boost::bind(&SYNCHRONIZATION_CLASS::do_lock_thread, this, key));

        wait_event->wait_one();
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call lock_by_thread from thread[%s] with key[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Releave lock current object with key
     * @param key
     */
    void SYNCHRONIZATION_CLASS::unlock(std::string key)
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call unlock from thread[%s] with key[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());*/
        if (this->m_lock_flags[key])
        {
            /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                       ": do unlock key[%s] from thread[%s]...", key.c_str(),
                       ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());*/
            boost::shared_ptr<boost::recursive_mutex> lock = this->get_lock(key);
            try
            {
                this->m_lock_flags[key] = ZOO_FALSE;
                lock->unlock();
            }
            catch (...)
            {/*
                TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                           "function error- exception when unlock");*/
            }
        }

        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call unlock from thread[%s] with key[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Release lock current object with key
     * @param key
     */
    void SYNCHRONIZATION_CLASS::unlock_by_thread(std::string key)
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call unlock_by_thread from thread[%s] with key[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());*/
        boost::shared_ptr<AUTO_RESET_EVENT_CLASS> cur_wait_event = this->get_wait_event(key);

        cur_wait_event->set();

        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call unlock_by_thread from thread[%s] with key[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Reset lock
     * @param key
     */
    void SYNCHRONIZATION_CLASS::reset_lock(std::string key)
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call reset_lock from thread[%s] with key[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());*/
        this->unlock_by_thread(key);
//        this->unlock(key);
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call reset_lock from thread[%s] with key[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
    * @brief Get lock state
    * @param key
    * @return thread is locked or not
    */
    ZOO_BOOL SYNCHRONIZATION_CLASS::get_lock_state(std::string key)
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call get_lock_state from thread[%s] with key[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());

        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   "The lock_state is %s.", this->m_lock_flags[key] ? "Locked":"Not Locked");

        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call get_lock_state from thread[%s] with key[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
        return this->m_lock_flags[key];
    }

    /**
     * @brief Get mutex lock with key
     * @param key
     * @return
     */
    boost::shared_ptr<boost::recursive_mutex> SYNCHRONIZATION_CLASS::get_lock(std::string key)
    {
       /* TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call get_lock from thread[%s] with key[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());*/
        boost::lock_guard<boost::recursive_mutex> lock(this->m_syn_global);
        boost::shared_ptr<boost::recursive_mutex> cur_lock = this->m_locks[key];
        if (cur_lock == NULL)
        {
            cur_lock.reset(new boost::recursive_mutex());
            this->m_locks[key] = cur_lock;
        }

        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call get_lock from thread[%s] with key[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
        return cur_lock;

    }

    /**
     * @brief Get mutex lock with key
     * @param key
     * @return
     */
    boost::shared_ptr<AUTO_RESET_EVENT_CLASS> SYNCHRONIZATION_CLASS::get_wait_event(std::string key)
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call get_wait_event from thread[%s] with key[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());*/
        boost::lock_guard<boost::recursive_mutex> lock(this->m_syn_global);
        boost::shared_ptr<AUTO_RESET_EVENT_CLASS> cur_wait_event = this->m_wait_events[key];
        if (cur_wait_event == NULL)
        {
            cur_wait_event.reset(new AUTO_RESET_EVENT_CLASS());
            this->m_wait_events[key] = cur_wait_event;
        }

        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call get_wait_event from thread[%s] with key[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
        return cur_wait_event;
    }

    /**
     * @brief Get mutex lock with key
     * @param key
     * @return
     */
    boost::shared_ptr<AUTO_RESET_EVENT_CLASS> SYNCHRONIZATION_CLASS::get_lock_status_event(
            std::string key)
    {
       /* TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call get_wait_event from thread[%s] with key[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());*/
        boost::lock_guard<boost::recursive_mutex> lock(this->m_syn_global);

        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": size of lock_status_events [%d]", m_lock_status_events.size());*/
        boost::shared_ptr<AUTO_RESET_EVENT_CLASS> cur_wait_event = this->m_lock_status_events[key];
        if (cur_wait_event == NULL)
        {
           /* TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                       ": create new wait_event for key [%s]", key.c_str());*/
            cur_wait_event.reset(new AUTO_RESET_EVENT_CLASS());
            this->m_lock_status_events[key] = cur_wait_event;
            this->m_wait_events[key] = cur_wait_event;
        }

        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call get_wait_event from thread[%s] with key[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
        return cur_wait_event;
    }

    /**
     * @brief Lock current object by thread
     * @param key
     */
    void SYNCHRONIZATION_CLASS::do_lock_thread(std::string key)
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call do_lock_thread from thread[%s] with key[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());*/
        boost::shared_ptr<boost::recursive_mutex> cur_lock = this->get_lock(key);
        boost::shared_ptr<AUTO_RESET_EVENT_CLASS> finish_lock_event = this->get_lock_status_event(
                key);
        boost::shared_ptr<AUTO_RESET_EVENT_CLASS> cur_wait_event = this->get_wait_event(key);
        cur_lock->lock();
        // Release waiting thread lock success
        finish_lock_event->set();
        // Keep locking from background thread and waiting for unlock
        cur_wait_event->wait_one();
        cur_lock->unlock();
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call do_lock_thread from thread[%s] with key[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str(), key.c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }
} /* namespace ZOO_COMMON */
