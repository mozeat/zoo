/**************************************************************
 * Copyright (C) 2015, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : THREAD_POOL.cpp
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0       2015-09-16		MOZEAT		Initialize
 ***************************************************************/
#include "THREAD_POOL.h"
#include <signal.h>
#include "PARAMETER_EXCEPTION_CLASS.h"
#include "ENVIRONMENT_UTILITY_CLASS.h"
#include "INVALID_ACCESS_MEMORY_EXCEPTION_CLASS.h"

namespace ZOO_COMMON
{
    boost::shared_ptr<THREAD_POOL> THREAD_POOL::m_instance;

    /**
     * @brief Deconstructor
     */
    THREAD_POOL::THREAD_POOL()
            :m_io_context(MAX_THREAD), m_work(m_io_context)
    {
        this->m_is_startup = ZOO_FALSE;
        this->m_create_thread = 0;
        this->m_available_thread = 0;
    }

    /**
     * @brief Run thread
     */
    void THREAD_POOL::initialize_thread()
    {
        this->m_io_context.run();
    }

    THREAD_POOL::~THREAD_POOL()
    {
        // TODO Auto-generated destructor stub
    }

    /**
     * @brief Get singleton instance
     * @return
     */
    boost::shared_ptr<THREAD_POOL> THREAD_POOL::get_instance()
    {
        if (NULL == THREAD_POOL::m_instance)
        {
            THREAD_POOL::m_instance.reset(new THREAD_POOL());
        }

        return THREAD_POOL::m_instance;
    }

    /**
     * @brief Startup
     */
    void THREAD_POOL::startup()
    {
        if (this->m_is_startup)
        {
            return;
        }

        this->create_threads(INIT_THREAD);
        this->m_is_startup = ZOO_TRUE;
    }

    /**
     * #@brief Update threads
     */
    void THREAD_POOL::update_threads()
    {
        // If no threads are available, then return.
        if (0 == this->m_available_thread)
        {
            int create = MAX_THREAD - this->m_create_thread;
            if (create > INVAL_THREAD)
            {
                create = INVAL_THREAD;
            }

            this->create_threads(create);
        }
    }

    /**
     * @brief Shutdown
     */
    void THREAD_POOL::shutdown()
    {

        if (!this->m_is_startup)
        {
            return;
        }

        // Force all threads to return from io_service::run().
        this->m_io_context.stop();

        // Suppress all exceptions.
        try
        {
            this->m_threads.join_all();
        }
        catch (...)
        {
        }

        this->m_is_startup = ZOO_FALSE;
    }

    /**
     * @brief Create count
     * @param count
     */
    void THREAD_POOL::create_threads(int count)
    {
        for (int i = 0; i < count; ++i)
        {
            this->m_threads.create_thread(boost::bind(&THREAD_POOL::initialize_thread, this));
            this->m_available_thread++;
            this->m_create_thread++;
        }
    }

    /**
     * @brief On handle signal
     * @param signal
     */
    void THREAD_POOL::on_handle_signal(int signal_num)
    {
        throw INVALID_ACCESS_MEMORY_EXCEPTION_CLASS(
                INVALID_ACCESS_MEMORY_EXCEPTION_ERROR,
                "Unhandle exception, please collect the log file and report this issue to administrator.",
                NULL);
    }

    /**
     * @brief On handle signal
     * @param signal
     */
    void THREAD_POOL::on_handle_signal()
    {
        on_handle_signal(SIGSEGV);
    }

    /**
     * @brief Wrap a task so that the available count can be increased once
     * the user provided task has completed.
     * @param task
     */
    int THREAD_POOL::wrap_task(boost::function<void()> task,
                               ERROR_MESSAGE_HANDLER_INTERFACE* m_error_handler)
    {
        int result = -1;
        try
        {
            signal(SIGSEGV, THREAD_POOL::on_handle_signal);

            task();
            result = 0;
        }
        catch (PARAMETER_EXCEPTION_CLASS& ex)
        {
            if (NULL != m_error_handler)
            {
                m_error_handler->handle_error(ex);
            }
        }

        // Suppress all exceptions.
        catch (...)
        {
            if (NULL != m_error_handler)
            {
                PARAMETER_EXCEPTION_CLASS error = PARAMETER_EXCEPTION_CLASS(
                        UNHANDLE_EXCEPTION_ERROR, "Unhandle exception", NULL);
                m_error_handler->handle_error(error);
            }
        }

        // Task has finished, so increment count of available threads.
        boost::unique_lock<boost::mutex> lock(this->m_task_syn_lock);
        ++this->m_available_thread;

        return result;
    }

} /* namespace ZOO_COMMON */
