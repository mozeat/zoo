/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : STRING_UTIL_CLASS.cpp
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-06-28			MOZEAT		Initialize
 ***************************************************************/
#include "COMMON_STRING_UTIL_CLASS.h"
#include <stdio.h>

namespace ZOO_COMMON
{
    /**
     * @brief Destructor
     */
    COMMON_STRING_UTIL_CLASS::~COMMON_STRING_UTIL_CLASS()
    {
        // TODO Auto-generated destructor stub
    }

    /**
     * @brief Copy source string to destination string
     * @param OUT target
     * @param IN value
     */
    void COMMON_STRING_UTIL_CLASS::copy(OUT ZOO_CHAR* target, IN const ZOO_CHAR* value)
    {
        if (NULL == value || strlen(value) == 0)
        {
            strcpy(target, "");
            target[0] = '\0';
        }
        else
        {
            int lenght = strlen(value);
            strcpy(target, value);
            target[lenght] = '\0';
        }
    }

    /**
     * @brief Copy source string to destination string
     * @param OUT target
     * @param IN value
     */
    void COMMON_STRING_UTIL_CLASS::copy(OUT boost::shared_array<ZOO_CHAR>& target,
                                        IN const ZOO_CHAR* value)
    {
        if (NULL == value || strlen(value) == 0)
        {
            target.reset();
        }
        else
        {
            int lenght = strlen(value);
            ZOO_CHAR* buffer = new ZOO_CHAR[lenght + 1];
            strcpy(buffer, value);
            buffer[lenght] = '\0';
            target.reset(buffer);
        }
    }

    /**
     * @brief Format string
     * @param OUT target
     * @param IN format
     */
    void COMMON_STRING_UTIL_CLASS::format(OUT ZOO_CHAR* target, IN const ZOO_CHAR* format, ...)
    {
//        va_list args;
//        int lenght = sprintf(target, format, args);
//        target[lenght] = '\0';
    }
} /* namespace ZOO_COMMON */
