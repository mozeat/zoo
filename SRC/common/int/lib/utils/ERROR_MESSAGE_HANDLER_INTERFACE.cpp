/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : ERROR_MESSAGE_HANDLER_INTERFACE.cpp
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0       2018-08-16		MOZEAT		Initialize
 ***************************************************************/
#include "ERROR_MESSAGE_HANDLER_INTERFACE.h"

namespace ZOO_COMMON
{
    /**
     * @brief Default constructor.
     */
    ERROR_MESSAGE_HANDLER_INTERFACE::ERROR_MESSAGE_HANDLER_INTERFACE()
    {
        this->m_has_error = ZOO_FALSE;
        this->m_error_code = 0;
    }

    /**
     * @brief Default descontructor.
     */
    ERROR_MESSAGE_HANDLER_INTERFACE::~ERROR_MESSAGE_HANDLER_INTERFACE()
    {
    }

    /**
     * @brief handler error code
     * @param exception The throw exception instance
     */
    void ERROR_MESSAGE_HANDLER_INTERFACE::handle_error(PARAMETER_EXCEPTION_CLASS& exception)
    {
        this->m_has_error = ZOO_TRUE;
        this->m_error_code = exception.get_error_code();
        if (NULL != exception.get_error_message())
        {
        	if(strlen(exception.get_error_message()) < MAX_MESSAGE_LENGTH)
        	{
           		strcpy(this->m_message, exception.get_error_message());
            }
            else
            {
            	std::string m(exception.get_error_message(),MAX_MESSAGE_LENGTH);
            	strcpy(this->m_message,m.c_str());
            }
        }
        else
        {
            strcpy(this->m_message, "");
        }
    }

    /**
     * @brief Has error
     * @return
     */
    ZOO_BOOL ERROR_MESSAGE_HANDLER_INTERFACE::has_error()
    {
        return this->m_has_error;
    }

    /**
     * @brief Get error code
     * @return
     */
    ZOO_INT32 ERROR_MESSAGE_HANDLER_INTERFACE::get_error_code()
    {
        return this->m_error_code;
    }

    /**
     * @brief Get message
     * @return
     */
    const ZOO_CHAR* ERROR_MESSAGE_HANDLER_INTERFACE::get_message()
    {
        return this->m_message;
    }

    /**
     * @brief Reset handle
     */
    void ERROR_MESSAGE_HANDLER_INTERFACE::reset_handler()
    {
        boost::lock_guard<boost::recursive_mutex> lock(this->m_syn_lock);
        this->m_has_error = ZOO_FALSE;
        this->m_error_code = 0;
        strcpy(this->m_message, "");
    }
}

