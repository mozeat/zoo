/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : SCHEDULER
 * File Name    : AUTO_RESET_EVENT_CLASS.cpp
 * Description  : This class is used to support synchronization between
 *                  thread via events.
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0     2018-05-18			mozeat				initialize
 ***************************************************************/
#include "AUTO_RESET_EVENT_CLASS.h"
#include "ENVIRONMENT_UTILITY_CLASS.h"

namespace ZOO_COMMON
{

    /**
     * @brief Define component ID
     */
    extern ZOO_CHAR COMMON_COMPONENT_ID[3];

    /**
     * @brief Default constructor.
     * @param initial_state
     *          true to set the initial state to signaled;
     *          false to set the initial state to non-signaled.
     */
    AUTO_RESET_EVENT_CLASS::AUTO_RESET_EVENT_CLASS()
            : m_flag(ZOO_FALSE)
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": create instance from thread[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Default constructor.
     * @param initial_state
     *          true to set the initial state to signaled;
     *          false to set the initial state to non-signaled.
     */
    AUTO_RESET_EVENT_CLASS::AUTO_RESET_EVENT_CLASS(ZOO_BOOL initial_state)
            : m_flag(initial_state)
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": create instance from thread[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Deconstruct
     */
    AUTO_RESET_EVENT_CLASS::~AUTO_RESET_EVENT_CLASS()
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": destroy instance from thread[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Sets the state of the event to signaled, allowing one or more waiting threads to proceed.
     */
    void AUTO_RESET_EVENT_CLASS::set()
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": flag:address-%d,value-%d, call set from thread[%s]...",
                   &this->m_flag, this->m_flag.load(), ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());*/
        {
            boost::lock_guard<boost::mutex> synchronize(m_protect);
            this->m_flag = ZOO_TRUE;
        }

        this->m_signal.notify_one();
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call set from thread[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Sets the state of the event to nonsignaled, causing threads to block.
     */
    void AUTO_RESET_EVENT_CLASS::reset()
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": flag:address-%d,value-%d, call reset from thread[%s]...",
                   &this->m_flag, this->m_flag.load(), ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());*/
        boost::lock_guard<boost::mutex> synchronize(m_protect);
        this->m_flag = ZOO_FALSE;
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call reset from thread[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Blocks the current thread until the current WaitHandle receives a signal.
     */
    void AUTO_RESET_EVENT_CLASS::wait_one()
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": flag:address-%d,value-%d, call wait_one from thread[%s]...",
                   &this->m_flag, this->m_flag.load(), ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());*/
        boost::unique_lock<boost::mutex> synchronize(m_protect);
        while (!this->m_flag) // prevent spurious wakeups from doing harm
        {
            this->m_signal.wait(synchronize);
        }

        this->m_flag = ZOO_FALSE; // waiting resets the flag
        /*TR4A_trace(COMMON_COMPONENT_ID, __FUNCTION_NAME__,
                   ": call wait_one from thread[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID, __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief Blocks the current thread until the current WaitHandle receives a signal.
     * @param delay_time    The delay time (ms)
     */
    ZOO_BOOL AUTO_RESET_EVENT_CLASS::wait_one(ZOO_UINT32 delay_time)
    {
        /*TR4A_trace(COMMON_COMPONENT_ID, __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID, __FUNCTION_NAME__,
                   ": input_parameters: delay_time[%d], call wait_one from thread[%s]...",
                   delay_time, ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());*/
        if (delay_time > 0)
        {
            boost::unique_lock<boost::mutex> synchronize(m_protect);
            boost::system_time const timeout = boost::get_system_time()
                    + boost::posix_time::milliseconds(delay_time);
            return this->m_signal.timed_wait(synchronize, timeout);
        }
		return ZOO_FALSE;
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call wait_one from thread[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

    /**
     * @brief The execution of the current thread (which shall have locked lock's mutex) is blocked during delay_time
     * @param The time span during which the thread will block waiting to be notified (delay_time in second)
     */
    void AUTO_RESET_EVENT_CLASS::sleep_for(ZOO_UINT32 delay_time)
    {
        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "> function entry...");
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call wait_one from thread[%s]...",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());*/
        if (delay_time > 0)
        {
            boost::unique_lock<boost::mutex> synchronize(m_protect);
            boost::system_time const timeout = boost::get_system_time()
                    + boost::posix_time::seconds(delay_time);
            this->m_signal.timed_wait(synchronize, timeout);
        }

        /*TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__,
                   ": call wait_one from thread[%s] - DONE",
                   ENVIRONMENT_UTILITY_CLASS::get_thread_id().c_str());
        TR4A_trace(COMMON_COMPONENT_ID,  __FUNCTION_NAME__, "< function exit...");*/
    }

} /* namespace ZOO_COMMON */
