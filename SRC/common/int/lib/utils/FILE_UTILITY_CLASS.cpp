/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : FILE_UTILITY_CLASS.cpp
 * Description  : The class provides APIs to execute file operations
 * History :
 * Version      Date				User			Comments
 * V1.0.0.0     2018-06-18			MOZEAT			Initialize
 ***************************************************************/
#include "FILE_UTILITY_CLASS.h"
#include <cstring>
#include <boost/filesystem.hpp>
#include <iostream>
#include <boost/lexical_cast.hpp>
#include "ENVIRONMENT_UTILITY_CLASS.h"

namespace ZOO_COMMON
{
    /**
     * Initialize null for m_instance
     */
    boost::shared_ptr<FILE_UTILITY_CLASS> FILE_UTILITY_CLASS::m_instance;

    /**
     * @brief Constructor
     */
    FILE_UTILITY_CLASS::FILE_UTILITY_CLASS()
    {

    }

    /**
     * @brief Destructor
     */
    FILE_UTILITY_CLASS::~FILE_UTILITY_CLASS()
    {

    }

    /**
     * @brief Get instance of FILE_UTILITY_CLASS
     */
    boost::shared_ptr<FILE_UTILITY_CLASS> FILE_UTILITY_CLASS::get_instance()
    {
        if (FILE_UTILITY_CLASS::m_instance == NULL)
        {
            FILE_UTILITY_CLASS::m_instance.reset(new FILE_UTILITY_CLASS());
        }

        return FILE_UTILITY_CLASS::m_instance;
    }

    /**
     * @brief Get all files located in specified directory recursively
     * @param dir       The directory contains files
     * @param files     The files located in specified directory
     * @return Error code for this function
     */
    ZOO_INT32 FILE_UTILITY_CLASS::get_all_files(string dir, map<string, string>& files,
                                                 string file_type)
    {
        ENVIRONMENT_UTILITY_CLASS::get_all_files(dir,files, file_type);
        return OK;
    }

    /**
     * @brief Determine whether or not the specified one is a directory
     * @param dir       The specified one need to be checked
     * @return True: is directory, False: otherwise
     */
    ZOO_BOOL FILE_UTILITY_CLASS::is_directory(string dir)
    {
        ZOO_BOOL result = ZOO_FALSE;
        boost::filesystem::path cur_dir(dir);
        if (!boost::filesystem::is_directory(cur_dir))
        {
            result = ZOO_TRUE;
        }

        return result;
    }

} /* namespace WH */
