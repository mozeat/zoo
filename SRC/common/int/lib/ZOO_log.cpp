/*******************************************************************************
* Copyright (C) 2017, 上海蔚来汽车有限公司
* All rights reserved.
* 产品: ZOO
* 所属组件:
* 模块名称 :
* 文件名称 : ZOO_log.h
* 概要描述 : 
* 历史记录 :
* 版本      日期        作者     内容
* 1.0    2017-12-7  孙伟旺    新建
******************************************************************************/
#include "ZOO_log.h" 
#include <stdarg.h>
#include <exception>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/log/common.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/utility/setup/from_stream.hpp>
#include <boost/log/utility/setup/settings.hpp>
#include <boost/log/utility/setup/from_settings.hpp>
#include <boost/log/utility/setup/settings_parser.hpp> 
#include <boost/log/expressions.hpp> 
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/syslog_backend.hpp>
#include <thread>
#include <mutex>
#include <errno.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/optional.hpp>


/**
 *@brief Define global initialize state. 
 */
std::once_flag g_log_init_flag;
static std::string g_zoo_log_config_dir;
static std::thread::id g_log_this_id = std::this_thread::get_id();

/**
 *@brief Define buffer size. 
 */
static const std::size_t BUFFER_SIZE_MAX = 1024;

namespace logging = boost::log;
namespace attrs = boost::log::attributes;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace expr = boost::log::expressions;
namespace keywords = boost::log::keywords;;
using boost::shared_ptr;


// The formatting logic for the severity level
template< typename CharT, typename TraitsT >
inline std::basic_ostream< CharT, TraitsT >& operator<< (
    std::basic_ostream< CharT, TraitsT >& strm, ZOO_SEVERITY_LEVEL_ENUM lvl)
{
    static const char* const str[] =
    {
        "Info",
        "Event",
        "Warning",
        "Error",
        "Critical"
    };
    if (static_cast< std::size_t >(lvl) < (sizeof(str) / sizeof(*str)))
        strm << str[lvl];
    else
        strm << static_cast< int >(lvl);
    return strm;
}

void ZOO_syslog_init(ZOO_LOG_SERVER_STRUCT* server)
{
    if(! server->enabled)
    {
        return;
    }
    
    try
    {
        // Create a syslog sink
        shared_ptr< sinks::synchronous_sink< sinks::syslog_backend > > sink(
            new sinks::synchronous_sink< sinks::syslog_backend >());

        sink->set_formatter
        (
            expr::format("%1%")
                //% expr::attr< ZOO_SEVERITY_LEVEL_ENUM >("Severity")
                % expr::smessage
        );

        // We'll have to map our custom levels to the syslog levels
        sinks::syslog::custom_severity_mapping< ZOO_SEVERITY_LEVEL_ENUM > mapping("Severity");
        mapping[ZOO_SEVERITY_LEVEL_NORMAL] = sinks::syslog::info;
        mapping[ZOO_SEVERITY_LEVEL_WARNING] = sinks::syslog::warning;
        mapping[ZOO_SEVERITY_LEVEL_ERROR] = sinks::syslog::critical;

        sink->locked_backend()->set_severity_mapper(mapping);

#if !defined(BOOST_LOG_NO_ASIO)
        // Set the remote address to sent syslog messages to
        sink->locked_backend()->set_target_address(server->addr,server->port);
#endif
        //logging::core::get()->add_global_attribute("RecordID", attrs::counter< unsigned int >());
        //logging::core::get()->add_global_attribute("ThreadID", attrs::current_thread_id());
        // Add the sink to the core
        logging::core::get()->add_sink(sink);
    }
    catch (std::exception& e)
    {
        std::cout << "FAILURE: " << e.what() << std::endl;
    }
}

void ZOO_log_init(const char * output_dir,const char * cfg_file)
{
    std::call_once(g_log_init_flag,
    [&]{
	try
    {
        if(!boost::filesystem::exists(cfg_file))
        {
            std::cout << "[Warning]: " << cfg_file <<  " is not exist." << std::endl;
            std::cout << "[Warning]: Default log settings set ..." << std::endl; 
        }

        if(!boost::filesystem::is_regular_file(cfg_file))
        {
            std::cout << "[Warning]: " << cfg_file <<  " is not exist." << std::endl;
            std::cout << "[Warning]: Default log settings set ..." << std::endl; 
        }
        
        /**
    	 * @brief load settings.ini 
    	 */
        std::string log_dir = output_dir;
        if(!boost::filesystem::exists(log_dir))
        {
            boost::system::error_code ec;
            boost::filesystem::create_directories(log_dir,ec);
            if(ec)
            {
                std::cout << "[ERROR]: create path:" << log_dir << " failed"<< std::endl; 
                return;
            }
        }

        std::ifstream file(cfg_file);
        logging::settings settings;
        if(file.is_open())
        {
        	settings = logging::parse_settings(file);
        	boost::optional<std::string> target = settings["Sinks.File.Target"];
        	std::string path = log_dir + + "/" + target.value();
        	if(!boost::filesystem::exists(path))
        	{
        		boost::filesystem::create_directory(path);
        	}
			settings["Sinks.File.Target"] = path;
			boost::optional<std::string> FileName = settings["Sinks.File.FileName"];
			settings["Sinks.File.FileName"] = path + "/" + FileName.value();
			boost::optional<std::string> TargetFileName = settings["Sinks.File.TargetFileName"];
			settings["Sinks.File.TargetFileName"] = path + "/" + TargetFileName.value();
			file.close();
        }
        else
        {

            std::string output_file = program_invocation_short_name;
	        //Logging core settings.
			settings["Core"]["Filter"] = "%Severity% >= 1";
			settings["Core"]["DisableLogging"] = false;
			//TextFile sink settings
			settings["Sinks.File.Destination"] = "TextFile";
			std::string target = log_dir + "/" + output_file;
			if(!boost::filesystem::exists(target))
        	{
        		boost::filesystem::create_directory(target);
        	}
			settings["Sinks.File.Target"] = target ;
			settings["Sinks.File.FileName"] = target + "/" + output_file + ".log";
			settings["Sinks.File.TargetFileName"] = target + "/" + output_file + "_%Y-%m-%d %H-%M-%S_%2N.log";
			settings["Sinks.File.AutoFlush"] = true;
			settings["Sinks.File.Append"] = true;
			settings["Sinks.File.RotationSize"] = 10 * 1024 * 1024; // 10 MiB
			settings["Sinks.File.MaxFiles"] = 5; // 50 files
			settings["Sinks.File.MaxSize"] = 10 * 10 * 1024 * 1024; // 100 MiB
			settings["Sinks.File.ScanForFiles"] = "All"; // 50 files
			settings["Sinks.File.Asynchronous"] = true; // 50 files
			settings["Sinks.File.Format"] = "[%TimeStamp%] %Message%"; 
        }
        // Read the settings and initialize logging library
        logging::init_from_settings(settings);

        // Add some attributes
        //logging::add_common_attributes();
        logging::core::get()->add_global_attribute("TimeStamp", attrs::local_clock());
        //logging::core::get()->add_global_attribute("ThreadID", attrs::current_thread_id());

    }
    catch (std::exception& e)
    {
        std::cout << "[error]: " << e.what() << std::endl;
    }});
}
void ZOO_set_log_config_dir(const char * dir)
{
    g_zoo_log_config_dir = dir;
}

/**
 *@brief print log 
 *@param level         the severity level
 *@param function_name     
 *@param formate_spec  the content ...
 */
void ZOO_log_t(IN ZOO_SEVERITY_LEVEL_ENUM level,
                        IN const char * resource_id,
						IN const char * component_id,
						IN const char * thread_id,
						IN const char * format_spec)
{
    src::severity_logger<> lg;
    BOOST_LOG_SEV(lg,level) << "[" << resource_id << "] " << "[" << component_id << "."<< thread_id << "] <" << level << "> " << format_spec;    
}


/**
 *@brief print log 
 *@param level         the severity level
 *@param function_name     
 *@param formate_spec  the content ...
 */
void ZOO_log_f(IN ZOO_SEVERITY_LEVEL_ENUM level,
                        IN const char * resource_id,
						IN const char * component_id,
						IN const char * format_spec)
{
    //src::severity_logger<ZOO_SEVERITY_LEVEL_ENUM> lg(keywords::severity = ZOO_SEVERITY_LEVEL_NORMAL);
    src::severity_logger<> lg;
    BOOST_LOG_SEV(lg,level) << "[" << resource_id << "] " << "[" << component_id << "] <" << level << "> " << format_spec;
}

/**
 *@brief print log 
 *@param level		   the severity level
 *@param formate_spec  the content ...
 */
void ZOO_log_s(IN ZOO_SEVERITY_LEVEL_ENUM level,
							const char * function_name,
							IN const char * format_spec)
{
	src::severity_logger<> lg;
	char thread_id[16] = {0};
	std::ostringstream oss;
	oss << std::this_thread::get_id();
	unsigned long long tid = std::stoull(oss.str());
	sprintf(thread_id,"0x%llx",tid);
    BOOST_LOG_SEV(lg,level) << "["<< program_invocation_short_name << "] [" 
                            << thread_id << "] <" << level << "> " 
                            << function_name << " " << format_spec;
}	
