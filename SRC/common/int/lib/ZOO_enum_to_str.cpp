/*******************************************************************************
* Copyright (C) 2017, 上海蔚来汽车有限公司
* All rights reserved.
* 产品: ZOO
* 所属组件:
* 模块名称 :
* 文件名称 : ZOO_enum_to_str.cpp
* 概要描述 : 
* 历史记录 :
* 版本      日期        作者     内容
* 1.0    2017-12-7  孙伟旺    新建
******************************************************************************/
#include "ZOO_enum_to_str.h"

/**
 * @brief Converter sim mode to string.
 * @param sim_mode
 */
const ZOO_CHAR * ZOO_sim_mode_enum_to_str(IN ZOO_SIM_MODE_ENUM sim_mode)
{
	const ZOO_CHAR * str = "ZOO_SIM_MODE_MAX";
	switch(sim_mode)
	{
		case ZOO_SIM_MODE_MIN:
			str = "ZOO_SIM_MODE_MIN";
			break;
		case ZOO_SIM_DISABLE: 
			str = "ZOO_SIM_DISABLE";
			break;
		case ZOO_SIM_MODE_1: 
			str = "ZOO_SIM_MODE_1";
			break;
		case ZOO_SIM_MODE_2: 
			str = "ZOO_SIM_MODE_2";
			break;
		case ZOO_SIM_MODE_3: 
			str = "ZOO_SIM_MODE_3";
			break;
		case ZOO_SIM_MODE_4: 
			str = "ZOO_SIM_MODE_4";
			break;
		case ZOO_SIM_MODE_MAX: 
			str = "ZOO_SIM_MODE_MAX";
			break;
		default:
			break;
	}
	return str;
}

/**
 * @brief Converter trace mode to string.
 * @param trace_mode
 */
const ZOO_CHAR * ZOO_trace_mode_enum_to_str(IN ZOO_TRACE_MODE_ENUM trace_mode)
{
	const ZOO_CHAR * str = "ZOO_TRACE_MODE_MAX";
	switch(trace_mode)
	{
		case ZOO_TRACE_MODE_MIN:
			str = "ZOO_TRACE_MODE_MIN";
			break;
		case ZOO_TRACE_DISABLE: 
			str = "ZOO_TRACE_DISABLE";
			break;
		case ZOO_TRACE_ENABLE: 
			str = "ZOO_TRACE_ENABLE";
			break;
		case ZOO_TRACE_MODE_MAX: 
			str = "ZOO_TRACE_MODE_MAX";
			break;
		default:break;
	}
	return str;
}

/**
 * @brief Converter watch mode to string.
 * @param watch_mode
 */
const ZOO_CHAR * ZOO_watch_mode_enum_to_str(IN ZOO_WATCH_MODE_ENUM watch_mode)
{
	const ZOO_CHAR * str = "ZOO_WATCH_MODE_MAX";
	switch(watch_mode)
	{
		case ZOO_WATCH_MODE_MIN:
			str = "ZOO_WATCH_MODE_MIN";
			break;
		case ZOO_WATCH_MODE_DISABLE: 
			str = "ZOO_WATCH_MODE_DISABLE";
			break;
		case ZOO_WATCH_MODE_TIMES_3: 
			str = "ZOO_WATCH_MODE_TIMES_3";
			break;
		case ZOO_WATCH_MODE_TIMES_5: 
			str = "ZOO_WATCH_MODE_TIMES_5";
			break;
		case ZOO_WATCH_MODE_MAX: 
			str = "ZOO_WATCH_MODE_MAX";
			break;
	}
	return str;
}

/**
 * @brief Converter driver state to string.
 * @param state
 */
const ZOO_CHAR * ZOO_driver_state_enum_to_str(IN ZOO_DRIVER_STATE_ENUM state)
{
	const ZOO_CHAR * str = "ZOO_DRIVER_STATE_MIN";
	switch(state)
	{
		case ZOO_DRIVER_STATE_MIN:
			str = "ZOO_DRIVER_STATE_MIN";
			break;
		case ZOO_DRIVER_STATE_UNKNOWN:
			str = "ZOO_DRIVER_STATE_UNKNOWN";
			break;
		case ZOO_DRIVER_STATE_IDLE: 
			str = "ZOO_DRIVER_STATE_IDLE";
			break;
		case ZOO_DRIVER_STATE_BUSY: 
			str = "ZOO_DRIVER_STATE_BUSY";
			break;
		case ZOO_DRIVER_STATE_TERMINATED: 
			str = "ZOO_DRIVER_STATE_TERMINATED";
			break;
		case ZOO_DRIVER_STATE_MAX: 
			str = "ZOO_DRIVER_STATE_MAX";
			break;
	}
	return str;
}

/**
 * @brief Converter running_mode to string.
 * @param running_mode
 */
const ZOO_CHAR * ZOO_running_mode_enum_to_str(IN ZOO_RUNNING_MODE_ENUM running_mode)
{
	const ZOO_CHAR * str = "ZOO_RUNNING_MODE_MIN";
	switch(running_mode)
	{
		case ZOO_RUNNING_MODE_DEBUG:
			str = "ZOO_RUNNING_MODE_DEBUG";
			break;
		case ZOO_RUNNING_MODE_WORK: 
			str = "ZOO_RUNNING_MODE_WORK";
			break;
		case ZOO_RUNNING_MODE_MAX: 
			str = "ZOO_RUNNING_MODE_MAX";
			break;
		case ZOO_RUNNING_MODE_MIN: 
			str = "ZOO_RUNNING_MODE_MIN";
			break;
	}
	return str;
}

/**
 * @brief Converter task_state to string.
 * @param task_state
 */
const ZOO_CHAR * ZOO_task_state_enum_to_str(IN ZOO_TASK_STATE_ENUM task_state)
{
	const ZOO_CHAR * str = "ZOO_TASK_STATE_MIN";
	switch(task_state)
	{
		case ZOO_TASK_STATE_MIN:
			str = "ZOO_TASK_STATE_MIN";
			break;
		case ZOO_TASK_STATE_SETUP: 
			str = "ZOO_TASK_STATE_SETUP";
			break;
		case ZOO_TASK_STATE_STARTED: 
			str = "ZOO_TASK_STATE_STARTED";
			break;
		case ZOO_TASK_STATE_NOT_STARTED: 
			str = "ZOO_TASK_STATE_NOT_STARTED";
			break;
		case ZOO_TASK_STATE_MAX: 
			str = "ZOO_TASK_STATE_MAX";
			break;
	}
	return str;
}

/**
 * @brief Converter server_type to string.
 * @param server_type
 */
const ZOO_CHAR * ZOO_server_type_enum_to_str(IN ZOO_SERVER_TYPE_ENUM server_type)
{
	const ZOO_CHAR * str = "ZOO_SERVER_TYPE_MIN";
	switch(server_type)
	{
		case ZOO_SERVER_TYPE_MIN:
			str = "ZOO_SERVER_TYPE_MIN";
			break;
		case ZOO_SERVER_TYPE_HTTP: 
			str = "ZOO_SERVER_TYPE_HTTP";
			break;
		case ZOO_SERVER_TYPE_HTTPS: 
			str = "ZOO_SERVER_TYPE_HTTPS";
			break;
		case ZOO_SERVER_TYPE_WEB: 
			str = "ZOO_SERVER_TYPE_WEB";
			break;
		case ZOO_SERVER_TYPE_WEBS: 
			str = "ZOO_SERVER_TYPE_WEBS";
			break;
		case ZOO_SERVER_TYPE_MAX: 
			str = "ZOO_SERVER_TYPE_MAX";
			break;
	}
	return str;
}

/**
 * @brief Converter cfg_type to string.
 * @param ota_state
 */
const ZOO_CHAR * ZOO_ota_state_enum_to_str(IN ZOO_OTA_STATE_ENUM ota_state)
{
	const ZOO_CHAR * str = "ZOO_OTA_STATE_MIN";
	switch(ota_state)
	{
		case ZOO_OTA_STATE_MIN:
			str = "ZOO_OTA_STATE_MIN";
			break;
		case ZOO_OTA_STATE_DOWNLOADING: 
			str = "ZOO_OTA_STATE_DOWNLOADING";
			break;
		case ZOO_OTA_STATE_IDLE: 
			str = "ZOO_OTA_STATE_IDLE";
			break;
		case ZOO_OTA_STATE_READY: 
			str = "ZOO_OTA_STATE_READY";
			break;
		case ZOO_OTA_STATE_INSTALLING: 
			str = "ZOO_OTA_STATE_INSTALLING";
			break;
		case ZOO_OTA_STATE_FINISHED: 
			str = "ZOO_OTA_STATE_FINISHED";
			break;
		case ZOO_OTA_STATE_MAX: 
			str = "ZOO_OTA_STATE_MAX";
			break;
	}
	return str;
}

/**
 * @brief Converter string to sim mode emum .
 * @param sim_mode
 */
ZOO_SIM_MODE_ENUM ZOO_str_to_sim_mode(IN const ZOO_CHAR * sim_mode_enum_str)
{
	ZOO_SIM_MODE_ENUM item = ZOO_SIM_MODE_MIN;
	if (strcmp("ZOO_SIM_MODE_MIN",sim_mode_enum_str) == 0)
	{
		item = ZOO_SIM_MODE_MIN;
	}
	else if (strcmp("ZOO_SIM_DISABLE",sim_mode_enum_str) == 0)
	{
		item = ZOO_SIM_DISABLE;
	}
	else if (strcmp("ZOO_SIM_MODE_1",sim_mode_enum_str) == 0)
	{
		item = ZOO_SIM_MODE_1;
	}
	else if (strcmp("ZOO_SIM_MODE_2",sim_mode_enum_str) == 0)
	{
		item = ZOO_SIM_MODE_2;
	}
	else if (strcmp("ZOO_SIM_MODE_3",sim_mode_enum_str) == 0)
	{
		item = ZOO_SIM_MODE_3;
	}
	else if (strcmp("ZOO_SIM_MODE_4",sim_mode_enum_str) == 0)
	{
		item = ZOO_SIM_MODE_4;
	}
	else
	{
		item = ZOO_SIM_MODE_MAX;
	}
	return item;
}

/**
 * @brief Converter tring to trace mode emum.
 * @param trace_mode
 */
ZOO_TRACE_MODE_ENUM ZOO_str_to_trace_mode(IN const ZOO_CHAR * trace_mode_enum_str)
{
	ZOO_TRACE_MODE_ENUM item = ZOO_TRACE_MODE_MIN;
	if (strcmp("ZOO_TRACE_MODE_MIN",trace_mode_enum_str) == 0)
	{
		item = ZOO_TRACE_MODE_MIN;
	}
	else if (strcmp("ZOO_TRACE_DISABLE",trace_mode_enum_str) == 0)
	{
		item = ZOO_TRACE_DISABLE;
	}
	else if (strcmp("ZOO_TRACE_ENABLE",trace_mode_enum_str) == 0)
	{
		item = ZOO_TRACE_ENABLE;
	}
	else
	{
		item = ZOO_TRACE_MODE_MAX;
	}
	return item;
}

/**
 * @brief Converter tring to watch mode emum.
 * @param watch_mode
 */
ZOO_WATCH_MODE_ENUM ZOO_str_to_watch_mode(IN const ZOO_CHAR * watch_mode_enum_str)
{
	ZOO_WATCH_MODE_ENUM item = ZOO_WATCH_MODE_MIN;
	if (strcmp("ZOO_WATCH_MODE_MIN",watch_mode_enum_str) == 0)
	{
		item = ZOO_WATCH_MODE_MIN;
	}
	else if (strcmp("ZOO_WATCH_MODE_DISABLE",watch_mode_enum_str) == 0)
	{
		item = ZOO_WATCH_MODE_DISABLE;
	}
	else if (strcmp("ZOO_WATCH_MODE_TIMES_3",watch_mode_enum_str) == 0)
	{
		item = ZOO_WATCH_MODE_TIMES_3;
	}
	else if (strcmp("ZOO_WATCH_MODE_TIMES_5",watch_mode_enum_str) == 0)
	{
		item = ZOO_WATCH_MODE_TIMES_5;
	}
	else
	{
		item = ZOO_WATCH_MODE_MAX;
	}
	return item;
}

/**
 * @brief Converter tring to driver state emum.
 * @param state
 */
ZOO_DRIVER_STATE_ENUM ZOO_str_to_driver_state(IN const ZOO_CHAR * state_enum_str)
{
	ZOO_DRIVER_STATE_ENUM item = ZOO_DRIVER_STATE_MIN;
	if (strcmp("ZOO_DRIVER_STATE_MIN",state_enum_str) == 0)
	{
		item = ZOO_DRIVER_STATE_MIN;
	}
	else if (strcmp("ZOO_DRIVER_STATE_UNKNOWN",state_enum_str) == 0)
	{
		item = ZOO_DRIVER_STATE_UNKNOWN;
	}
	else if (strcmp("ZOO_DRIVER_STATE_IDLE",state_enum_str) == 0)
	{
		item = ZOO_DRIVER_STATE_IDLE;
	}
	else if (strcmp("ZOO_DRIVER_STATE_BUSY",state_enum_str) == 0)
	{
		item = ZOO_DRIVER_STATE_BUSY;
	}
	else if (strcmp("ZOO_DRIVER_STATE_TERMINATED",state_enum_str) == 0)
	{
		item = ZOO_DRIVER_STATE_TERMINATED;
	}
	else
	{
		item = ZOO_DRIVER_STATE_MAX;
	}
	return item;
}

/**
 * @brief Converter tring to running_mode emum.
 * @param running_mode
 */
ZOO_RUNNING_MODE_ENUM ZOO_str_to_running_mode(IN const ZOO_CHAR * running_mode_enum_str)
{
	ZOO_RUNNING_MODE_ENUM item = ZOO_RUNNING_MODE_MIN;
	if (strcmp("ZOO_RUNNING_MODE_MIN",running_mode_enum_str) == 0)
	{
		item = ZOO_RUNNING_MODE_MIN;
	}
	else if (strcmp("ZOO_RUNNING_MODE_DEBUG",running_mode_enum_str) == 0)
	{
		item = ZOO_RUNNING_MODE_DEBUG;
	}
	else if (strcmp("ZOO_RUNNING_MODE_WORK",running_mode_enum_str) == 0)
	{
		item = ZOO_RUNNING_MODE_WORK;
	}
	else
	{
		item = ZOO_RUNNING_MODE_MAX;
	}
	return item;
}

/**
 * @brief Converter tring to task_state emum.
 * @param task_state
 */
ZOO_TASK_STATE_ENUM ZOO_str_to_task_state(IN const ZOO_CHAR * task_state_enum_str)
{
	ZOO_TASK_STATE_ENUM item = ZOO_TASK_STATE_MIN;
	if (strcmp("ZOO_TASK_STATE_MIN",task_state_enum_str) == 0)
	{
		item = ZOO_TASK_STATE_MIN;
	}
	else if (strcmp("ZOO_TASK_STATE_SETUP",task_state_enum_str) == 0)
	{
		item = ZOO_TASK_STATE_SETUP;
	}
	else if (strcmp("ZOO_TASK_STATE_STARTED",task_state_enum_str) == 0)
	{
		item = ZOO_TASK_STATE_STARTED;
	}
	else if (strcmp("ZOO_TASK_STATE_NOT_STARTED",task_state_enum_str) == 0)
	{
		item = ZOO_TASK_STATE_NOT_STARTED;
	}
	else
	{
		item = ZOO_TASK_STATE_MAX;
	}
	return item;
}

/**
 * @brief Converter string to server_type emum.
 * @param server_type_enum_str
 */
ZOO_SERVER_TYPE_ENUM ZOO_str_to_server_type(IN const ZOO_CHAR * server_type_enum_str)
{
	ZOO_SERVER_TYPE_ENUM item = ZOO_SERVER_TYPE_MIN;
	if (strcmp("ZOO_SERVER_TYPE_MIN",server_type_enum_str) == 0)
	{
		item = ZOO_SERVER_TYPE_MIN;
	}
	else if (strcmp("ZOO_SERVER_TYPE_HTTP",server_type_enum_str) == 0)
	{
		item = ZOO_SERVER_TYPE_HTTP;
	}
	else if (strcmp("ZOO_SERVER_TYPE_WEB",server_type_enum_str) == 0)
	{
		item = ZOO_SERVER_TYPE_WEB;
	}
	else if (strcmp("ZOO_SERVER_TYPE_HTTPS",server_type_enum_str) == 0)
	{
		item = ZOO_SERVER_TYPE_HTTPS;
	}
	else if (strcmp("ZOO_SERVER_TYPE_WEBS",server_type_enum_str) == 0)
	{
		item = ZOO_SERVER_TYPE_WEBS;
	}
	else
	{
		item = ZOO_SERVER_TYPE_MAX;
	}
	return item;
}

/**
 * @brief Converter string to OTA STATE emum.
 * @param ota_st_enum_str
 */
ZOO_OTA_STATE_ENUM ZOO_str_to_OTA_state(IN const ZOO_CHAR *ota_st_enum_str)
{
	ZOO_OTA_STATE_ENUM item = ZOO_OTA_STATE_MIN;
	if (strcmp("ZOO_OTA_STATE_MIN",ota_st_enum_str) == 0)
	{
		item = ZOO_OTA_STATE_MIN;
	}
	else if (strcmp("ZOO_OTA_STATE_IDLE",ota_st_enum_str) == 0)
	{
		item = ZOO_OTA_STATE_IDLE;
	}
	else if (strcmp("ZOO_OTA_STATE_DOWNLOADING",ota_st_enum_str) == 0)
	{
		item = ZOO_OTA_STATE_DOWNLOADING;
	}
	else if (strcmp("ZOO_OTA_STATE_READY",ota_st_enum_str) == 0)
	{
		item = ZOO_OTA_STATE_READY;
	}
	else if (strcmp("ZOO_OTA_STATE_INSTALLING",ota_st_enum_str) == 0)
	{
		item = ZOO_OTA_STATE_INSTALLING;
	}
	else if (strcmp("ZOO_OTA_STATE_FINISHED",ota_st_enum_str) == 0)
	{
		item = ZOO_OTA_STATE_FINISHED;
	}
	else
	{
		item = ZOO_OTA_STATE_MAX;
	}
	return item;
}

