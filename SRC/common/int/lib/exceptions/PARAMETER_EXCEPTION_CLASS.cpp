/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : PARAMETER_EXCEPTION_CLASS.h
 * Description  : Defines a custom exception which will be raised up to upper layer
 * History :
 * Version      Date            User            Comments
 * V1.0.0.0     2015-05-04      trongnp         Create file
 ***************************************************************/
#include "PARAMETER_EXCEPTION_CLASS.h"
#include <stdio.h>

namespace ZOO_COMMON
{
    /**
     * @brief Constructor
     * @param error_code        Error code
     * @param error_message     Error message
     * @param inner_exception   Inner exception
     */
    PARAMETER_EXCEPTION_CLASS::PARAMETER_EXCEPTION_CLASS(ZOO_INT32 error_code,
                                                         const ZOO_CHAR* error_message,
                                                         std::exception* inner_exception)
    {
        this->m_error_code = error_code;
        strcpy(this->m_error_message, error_message);
        this->m_inner_exception = inner_exception;
    }

    /**
     * @brief Destructor
     */
    PARAMETER_EXCEPTION_CLASS::~PARAMETER_EXCEPTION_CLASS() throw ()
    {
    }

    /**
     * @brief Get the error_code attribute value
     */
    ZOO_INT32 PARAMETER_EXCEPTION_CLASS::get_error_code()
    {
        return this->m_error_code;
    }

    /**
     * @brief Set the error_code attribute value
     * @param error_code    The new error_code attribute value
     */
    void PARAMETER_EXCEPTION_CLASS::set_error_code(ZOO_INT32 error_code)
    {
        this->m_error_code = error_code;
    }

    /**
     * @brief Get the error_message attribute value
     */
    const ZOO_CHAR* PARAMETER_EXCEPTION_CLASS::get_error_message()
    {
        return this->m_error_message;
    }

    /**
     * @brief Set the error_message attribute value
     * @param error_message    The new error_message attribute value
     */
    void PARAMETER_EXCEPTION_CLASS::set_error_message(const ZOO_CHAR* error_message)
    {
        strcpy(this->m_error_message, error_message);
    }

    /**
     * @brief Get the inner_exception attribute value
     */
    std::exception* PARAMETER_EXCEPTION_CLASS::get_inner_exception()
    {
        return this->m_inner_exception;
    }

    /**
     * @brief Set the inner_exception attribute value
     * @param inner_exception    The new inner_exception attribute value
     */
    void PARAMETER_EXCEPTION_CLASS::set_inner_exception(std::exception* inner_exception)
    {
        this->m_inner_exception = inner_exception;
    }

    /**
     * @brief Free
     */
    void PARAMETER_EXCEPTION_CLASS::free()
    {
//        PARAMETER_EXCEPTION_CLASS* inner_ex =
//                dynamic_cast<PARAMETER_EXCEPTION_CLASS*>(this->m_inner_exception);
//        if (NULL != inner_ex)
//        {
//            inner_ex->free();
//        }
//
//        SAFTY_DELETE_POINTER(this->m_inner_exception);
    }
}

