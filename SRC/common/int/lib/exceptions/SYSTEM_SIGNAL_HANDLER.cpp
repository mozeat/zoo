/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : COMMON
 * File Name      : SYSTEM_SIGNAL_HANDLER.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-09-14    Generator      created
*************************************************************/
#include "SYSTEM_SIGNAL_HANDLER.h"
#include <ZOO_COMMON_MACRO_DEFINE.h>
#include <boost/stacktrace.hpp>
#include <signal.h>
#include <sstream>
#include <utility>
#include <memory>
#include <cstdlib>      // std::exit
#include <boost/thread/once.hpp>
#include <boost/filesystem.hpp>
#include "ZOO_if.hpp"
namespace ZOO_COMMON
{
    /**
     * @brief The common id;
     */
    const ZOO_CHAR * COMMON_COMPONET_ID = "ZOO";

    /**
    * @brief The register componet id
    */
    static std::string g_componet_id;

    /**
     * @brief Initialize static instance
     */
    boost::shared_ptr<SYSTEM_SIGNAL_HANDLER> SYSTEM_SIGNAL_HANDLER::m_instance;

    /**
     * @brief Initialize once flag for multithread;
     */
    static boost::once_flag g_instance_once_falg = BOOST_ONCE_INIT;

    /**
     * @brief Constructor
     */
    SYSTEM_SIGNAL_HANDLER::SYSTEM_SIGNAL_HANDLER()
    {
        //ctor
    }

    /**
     * @brief Destructor
     */
    SYSTEM_SIGNAL_HANDLER::~SYSTEM_SIGNAL_HANDLER()
    {
        //dtor
    }
    /**
     * @brief instance
     */
    boost::shared_ptr<SYSTEM_SIGNAL_HANDLER> SYSTEM_SIGNAL_HANDLER::get_instance()
    {
        boost::call_once([]()->auto
                         {
                             if(NULL == SYSTEM_SIGNAL_HANDLER::m_instance)
                             {
                                SYSTEM_SIGNAL_HANDLER::m_instance.reset(new SYSTEM_SIGNAL_HANDLER());
                             }
                         },g_instance_once_falg);
        return SYSTEM_SIGNAL_HANDLER::m_instance;
    }

    /**
     * @brief Raise a system defined signal
     * @param sig_id           this is the system define signal id,such as SIGINT
     */
    void SYSTEM_SIGNAL_HANDLER::raise_signal(int sig_id) noexcept
    {
        raise(sig_id);
    }

    /**
     * @brief Reset system signal to default behavior
     * @param sig_id           this is the system define signal id,such as SIGINT
     */
    void SYSTEM_SIGNAL_HANDLER::reset_signal_behavior(int sig_id) noexcept
    {
        signal(sig_id, SIG_DFL);
    }

    /**
     * @brief Ignore system signal
     * @param sig_id           this is the system define signal id,such as SIGINT
     */
    void SYSTEM_SIGNAL_HANDLER::ignore_signal_behavior(int sig_id) noexcept
    {
        signal(sig_id, SIG_IGN);
    }

    /**
     * @brief Register system signal
     * @param sig_id           this is the system define signal id,such as SIGINT
     * @param handler          callback for signal process
     */
    void SYSTEM_SIGNAL_HANDLER::register_signal_handler(std::string componet_id,int sig_id,std::function<void(int)> & handler) throw()
    {
        /*TR4A_trace(componet_id.c_str(),__ZOO_FUNC__," > function entry ...");
        TR4A_trace(componet_id.c_str(),__ZOO_FUNC__," receive signal[%s] ...",
                                                        SYSTEM_SIGNAL_HANDLER::to_string(sig_id));*/

        #if defined _WIN64 || defined _WIN32
        void (*const* callback)(int) = handler.target<__p_sig_fn_t>();
        #else
        void (*const* callback)(int) = handler.target<__sighandler_t>();
        #endif // defined

        if(SIG_ERR == signal(sig_id,*callback))
        {
            std::ostringstream os;
            os << "register signal[" <<SYSTEM_SIGNAL_HANDLER::to_string(sig_id) <<"]";
            __THROW_ZOO_EXCEPTION(ZOO_get_process_name(),-1,os.str().data(),nullptr);
        }
        /*TR4A_trace(componet_id.c_str(),__ZOO_FUNC__," < function exit ...");*/
        return;
    }

    /**
     * @brief register system signal use default handler,the default handler
     * will record the stack informations to the log file when capture an associated signal
     * @param sig_id           this is the system define signal id,such as SIGINT
     */
    void SYSTEM_SIGNAL_HANDLER::register_signal_by_default_handler(std::string componet_id,int sig_id) throw()
    {
        /*TR4A_trace(componet_id.c_str(),__ZOO_FUNC__," > function entry ...");
        TR4A_trace(componet_id.c_str(),__ZOO_FUNC__," receive signal[%s] ...",
                                                        SYSTEM_SIGNAL_HANDLER::to_string(sig_id));*/
		g_componet_id = componet_id;
        if(SIG_ERR == signal(sig_id,&SYSTEM_SIGNAL_HANDLER::default_signal_handler))
        {
            std::ostringstream os;
            os << "register signal[" <<SYSTEM_SIGNAL_HANDLER::to_string(sig_id) <<"]";
            __THROW_ZOO_EXCEPTION("ZOO_COMMON",-1,os.str().data(),nullptr);
        }
        /*TR4A_trace(componet_id.c_str(),__ZOO_FUNC__," < function exit ...");*/
        return;
    }
    /**
     * @brief default handler,the default handler
     * will record the stack informations to the log file when capture an associated signal
     * @param sig_id           this is the system define signal id,such as SIGINT
     */
    void SYSTEM_SIGNAL_HANDLER::default_signal_handler(IN int signal_id)
    {      
        std::string core_file = boost::filesystem::current_path().string() +
                                        "/" + ZOO_get_process_name() + ".dump";
        if (boost::filesystem::exists(core_file))
        {
            boost::filesystem::remove(core_file);
        }
        boost::stacktrace::safe_dump_to(3,boost::stacktrace::detail::max_frames_dump,core_file.data());

        if (boost::filesystem::exists(core_file))
        {
            std::ifstream ifs(core_file);
            boost::stacktrace::stacktrace st = boost::stacktrace::stacktrace::from_dump(ifs);
            std::stringstream os;
            os << st;
            ZOO_slog(ZOO_SEVERITY_LEVEL_CRITICAL,__ZOO_FUNC__,"stacktrace: %s",os.str().c_str());
        }                                                            
        SYSTEM_SIGNAL_HANDLER::m_instance->reset_signal_behavior(signal_id);
        return;
    }

    /**
     * @brief Convert from signal to string format
     */
    const char * SYSTEM_SIGNAL_HANDLER::to_string(int signal_id)
    {
        const ZOO_CHAR * str = "";
        switch(signal_id)
        {
            case SIGSEGV: str = "SIGSEGV";break;
            case SIGABRT: str = "SIGABRT";break;
            case SIGINT: str = "SIGINT";break;
            case SIGILL: str = "SIGILL";break;
            //case SIGABRT_COMPAT: str = "SIGABRT_COMPAT";break;
            case SIGFPE: str = "SIGFPE";break;
            case SIGTERM: str = "SIGTERM";break;
            #ifdef _POSIX
            case SIGQUIT: str = "SIGQUIT";break;
            case SIGKILL: str = "SIGABRT";break;
            case SIGHUP: str = "SIGHUP";break;
            case SIGTRAP: str = "SIGTRAP";break;
            case SIGEMT: str = "SIGEMT";break;
            case SIGBUS: str = "SIGBUS";break;
            case SIGSYS: str = "SIGSYS";break;
            case SIGPIPE: str = "SIGPIPE";break;
            #ifdef __USE_MINGW_ALARM
            case SIGALRM: str = "SIGALRM";break;/* alarm clock */
            #endif
            #endif
            default:str = "Unknown signal";break;
        }
        return str;
    }
}
