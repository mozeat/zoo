/**************************************************************
 * Copyright (C) 2015, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : CONVERTER_CLASS.cpp
 * Description  : The class implements methods for converter.
 * History :
 * Version      Date            User              Comments
 * V1.0.0.0     2015-05-04      MOZEAT         Create file
 ***************************************************************/
#include "CONVERTER_CLASS.h"
namespace ZOO_COMMON
{

    /***
     * Initialize ms_instance
     */
    boost::shared_ptr<CONVERTER_CLASS> CONVERTER_CLASS::ms_instance;

    /***
     * @brief The constructor of this class
     */
    CONVERTER_CLASS::CONVERTER_CLASS()
    {
        // TODO Auto-generated constructor stub

    }

    /***
     * @brief The destructor of this class
     */
    CONVERTER_CLASS::~CONVERTER_CLASS()
    {
        // TODO Auto-generated destructor stub
    }

    /**
     * @brief Get instance of utility class
     * @return
     */
    boost::shared_ptr<CONVERTER_CLASS> CONVERTER_CLASS::get_instance()
    {
        if (CONVERTER_CLASS::ms_instance == NULL)
        {
            CONVERTER_CLASS::ms_instance.reset(new CONVERTER_CLASS());
        }

        return CONVERTER_CLASS::ms_instance;
    }

    /***
     * @brief Convert from datetime to string
     * @param value datetime value
     * @return string value
     */
    void CONVERTER_CLASS::convert_from_time_t_to_string(IN time_t value, OUT ZOO_CHAR* result)
    {
        struct tm * timeinfo = NULL;
        if (value != 0)
        {
            timeinfo = localtime(&value);
            strftime(result, 80, "%F %T", timeinfo);
            //strftime(result, 64, "%Y-%m-%d %H:%M:%S", timeinfo);
        }
        else
        {
            strcpy(result, "");
        }
    }

    /***
     * @brief Convert from string to time t
     * @param value
     * @return
     */
    time_t CONVERTER_CLASS::convert_from_string_to_time_t(IN const ZOO_CHAR* value)
    {
        time_t result;
        if (value != NULL)
        {
            struct tm tm = {};
            /*Format date time like as YYYY-MM-DD HH:MM:SS*/
//            strptime(value, "%F %T", &tm);
            strftime((ZOO_CHAR*)value, 64, "%Y-%m-%d %H:%M:%S", &tm);
            result = mktime(&tm);
        }
        else
        {
            return 0;
        }
        return result;
    }

}
/* namespace ZOO_COMMON */
