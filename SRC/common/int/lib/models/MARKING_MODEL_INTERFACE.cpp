/**************************************************************
 * Copyright (C) 2015, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : COMMON
 * File Name    : MARKING_MODEL_INTERFACE.cpp
 * Description  : {Summary Description}
 * History :
 * Version      Date				User			Comments
 * V1.0.0.0     2018-05-19			MOZEAT			Initialize
 ***************************************************************/
#include "MARKING_MODEL_INTERFACE.h"
#include "stdio.h"

namespace ZOO_COMMON
{
    /**
     * @brief Constructor
     */
    MARKING_MODEL_INTERFACE::MARKING_MODEL_INTERFACE()
    {
    }

    /**
     * @brief Destructor
     */
    MARKING_MODEL_INTERFACE::~MARKING_MODEL_INTERFACE()
    {
    }

    /**
     * @brief Get the marking_code attribute value
     */
    const ZOO_CHAR* MARKING_MODEL_INTERFACE::get_marking_code()
    {
        return this->m_marking_code.get();
    }

    /**
     * @brief Set the marking_code attribute value
     * @param marking_code    The new marking_code attribute value
     */
    void MARKING_MODEL_INTERFACE::set_marking_code(const ZOO_CHAR* marking_code)
    {
        if (NULL == marking_code)
        {
            this->m_marking_code.reset();
        }
        else
        {
            ZOO_CHAR* buffer = new ZOO_CHAR[strlen(marking_code) + 1];
            strcpy(buffer, marking_code);
            this->m_marking_code.reset(buffer);
        }
    }

    /**
     * @brief Clean mem
     */
    void MARKING_MODEL_INTERFACE::clean_mem()
    {
        this->m_pointer.reset();
    }

///// End region

} /* namespace WH */
