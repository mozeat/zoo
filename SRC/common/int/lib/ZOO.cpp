/*******************************************************************************
* Copyright (C) 2017, 上海蔚来汽车有限公司
* All rights reserved.
* 产品: ZOO
* 所属组�?�?
* 模块名称 :
* 文件名称 : ZOO_if.h
* 概要描述 : 
* 历史记录 :
* 版本      日期        作�?    内容
* 1.0    2017-12-7  孙伟�?   新建
******************************************************************************/
extern "C"
{
	#include "ZOO_if.h"
	#include "ZOO_enum_to_str.h"
	#include "ZOO_log.h"
}
#include <sys/time.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <errno.h>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <string>
#include <iostream>
#include <sstream>
#include <mutex>
#include <thread>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/date_time.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/random.hpp>
#include <boost/foreach.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/nondet_random.hpp>
#include <boost/lexical_cast.hpp>
#include <shared_mutex>

#define ZOO_CFG_BASE_DIR "/opt/ZOO"

/**
 @brief define brown dragon 
*/
static const char * g_zoo_base_configure_dir = ZOO_CFG_BASE_DIR;

/**
 @brief define brown dragon 
*/
static const char * ZOO_CFG_INFO_FILE = ZOO_CFG_BASE_DIR"/config/zoo.json";

/**
 * @brief Define timer lock.
*/
static std::mutex g_sync_lock_for_timer;
static std::recursive_mutex g_recursive_lock;
/**
* @brief Define timer state.
*/
static std::map<ZOO_HANDLE, ZOO_BOOL> g_timer_map;

/**
* @brief Define current_exec_path 
*/
static std::string g_current_exec_path = "";

/**
* @brief Define parent_path 
*/
static std::string g_parent_path = "";

ZOO_EXPORT const ZOO_CHAR * ZOO_get_pl_cfg_base_dir()
{
    return g_zoo_base_configure_dir;
}

/**
 @brief get ZOO version info
 @param ZOO_VERSION_STRUCT* version_info
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_pl_version_info(INOUT ZOO_VERSION_STRUCT* version_info)
{
    std::lock_guard<std::recursive_mutex> lock(g_recursive_lock);
    ZOO_INT32 error_code = OK;
    if(!boost::filesystem::exists(ZOO_CFG_INFO_FILE))
	{
		//ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,":: %s is missing",ZOO_CFG_INFO_FILE);
		error_code = ZOO_FILE_NOT_EXIST_ERR;
	}

    if(OK == error_code)
	{
    	if(NULL == version_info)
        {
        	//ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,":: input parameter version_info pointer is null");
            error_code = ZOO_PARAMETER_ERR;
        }
    }
    
	if(OK == error_code)
	{
	    boost::property_tree::ptree cfg_pt;
		boost::property_tree::read_json(ZOO_CFG_INFO_FILE,cfg_pt);
		boost::optional<boost::property_tree::ptree &> v_pt = cfg_pt.get_child_optional("version");
		if(v_pt)
		{
    		boost::optional<std::string> zoo_version = v_pt.get().get_optional<std::string>("ZOO");
    		if(zoo_version)
    		{
    		    sprintf(version_info->zoo,"%s",zoo_version.get().data());
    		}
    		boost::optional<std::string> kernal = v_pt.get().get_optional<std::string>("kernal");
    		if(kernal)
    		{
    		    sprintf(version_info->kernel,"%s",kernal.get().data());
    		}
    		boost::optional<std::string> uboot = v_pt.get().get_optional<std::string>("uboot");
    		if(uboot)
    		{
    		    sprintf(version_info->uboot,"%s",uboot.get().data());
    		}
    		boost::optional<std::string> release_date = v_pt.get().get_optional<std::string>("release_date");
    		if(release_date)
    		{
    		    sprintf(version_info->release_date,"%s",release_date.get().data());
    		}
		}
		else
		{
		    error_code = ZOO_READ_FILE_FAILED_ERR;
		}
	}
    return error_code;
}

/**
 @brief get user version info
 @param ZOO_USER_VERSION_STRUCT user_version_info
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_user_version_info(INOUT ZOO_USER_VERSION_STRUCT* user_version_info)
{
    std::lock_guard<std::recursive_mutex> lock(g_recursive_lock);
    ZOO_INT32 error_code = OK;
    ZOO_CHAR file[64]= {0x0};
    error_code = ZOO_get_user_cfg_file(file);
    if(OK == error_code)
    {   
        if(!boost::filesystem::exists(file))
    	{
    		error_code = ZOO_FILE_NOT_EXIST_ERR;
    	}
    }
    
    if(OK == error_code)
	{
        if(NULL == user_version_info)
        {
            error_code = ZOO_PARAMETER_ERR;
        }
        memset(user_version_info,0x0,sizeof(ZOO_USER_VERSION_STRUCT));
    }
    
	if(OK == error_code)
	{
	    try 
	    {
    	    boost::property_tree::ptree cfg_pt;
    		boost::property_tree::read_json(file,cfg_pt);
    		std::string project = cfg_pt.get<std::string>("project");
            sprintf(user_version_info->project,"%s",project.data());
    		boost::property_tree::ptree version_pt = cfg_pt.get_child("version");
    		std::string major = version_pt.get<std::string>("major");
    		sprintf(user_version_info->major,"%s",major.data());
    		BOOST_FOREACH(const boost::property_tree::ptree::value_type & v, version_pt.get_child("modules"))
    		{
    		    std::string name = v.first; 
    		    std::string version = v.second.get_value<std::string>(); 
    		    sprintf(user_version_info->modules[user_version_info->module_numbers].name,"%s",name.data());
    		    sprintf(user_version_info->modules[user_version_info->module_numbers].version,"%s",version.data());
    		    user_version_info->module_numbers ++;
    		}
		}
		catch(boost::property_tree::ptree_error & e)
		{
		    ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,":: %s",e.what());
		}
	}
    return error_code;
}

/**
 @brief get user cfg file
 @param ZOO_CHAR* cfg_file
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_user_cfg_file(INOUT ZOO_CHAR cfg_file[ZOO_PATH_LENGHT])
{
    std::lock_guard<std::recursive_mutex> lock(g_recursive_lock);
    ZOO_INT32 error_code = OK;
    if(!boost::filesystem::exists(ZOO_CFG_INFO_FILE))
	{
		std::cout << "ZOO_get_user_cfg_file:: <" << ZOO_CFG_INFO_FILE << "> not exist ...ERROR" << std::endl;
		error_code = ZOO_FILE_NOT_EXIST_ERR;
	}

    if(OK == error_code)
	{
        if(NULL == cfg_file)
        {
        	std::cout << "ZOO_get_user_cfg_file:: input parameter cfg_path pointer is null ...ERROR" << std::endl;
            error_code = ZOO_PARAMETER_ERR;
        }
    }
    
	if(OK == error_code)
	{
	    boost::property_tree::ptree cfg_pt;
		boost::property_tree::read_json(ZOO_CFG_INFO_FILE,cfg_pt);
		boost::optional<boost::property_tree::ptree&> v_pt = cfg_pt.get_child_optional("path.configuration");
		if(v_pt)
		{
    		boost::optional<std::string> file = v_pt.get().get_optional<std::string>("project_cfg_file");
            if(file)
            {
                sprintf(cfg_file,"%s",file.get().data());
            }
		}
		else
		{
		    std::cout << "ZOO_get_user_cfg_file:: <" << ZOO_CFG_INFO_FILE << "> node <project_cfg_file> not exist ...ERROR" << std::endl;
		    error_code = ZOO_READ_FILE_FAILED_ERR;
		}
	}
    return error_code;
}

/**
 @brief get user cfg path
 @param ZOO_CHAR* path
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_user_cfg_path(INOUT ZOO_CHAR cfg_path[ZOO_PATH_LENGHT])
{
    std::lock_guard<std::recursive_mutex> lock(g_recursive_lock);
    ZOO_INT32 error_code = OK;
    if(!boost::filesystem::exists(ZOO_CFG_INFO_FILE))
	{
		std::cout << "ZOO_get_user_cfg_path:: <" << ZOO_CFG_INFO_FILE << "> not exist ...ERROR" << std::endl;
		error_code = ZOO_FILE_NOT_EXIST_ERR;
	}

    if(OK == error_code)
	{
        if(NULL == cfg_path)
        {
        	std::cout << "ZOO_get_user_cfg_path:: input parameter cfg_path pointer is null ...ERROR" << std::endl;
            error_code = ZOO_PARAMETER_ERR;
        }
    }
    
	if(OK == error_code)
	{
	    boost::property_tree::ptree cfg_pt;
		boost::property_tree::read_json(ZOO_CFG_INFO_FILE,cfg_pt);
		boost::optional<boost::property_tree::ptree&> v_pt = cfg_pt.get_child_optional("path.configuration");
		if(v_pt)
		{
    		boost::optional<std::string> file = v_pt.get().get_optional<std::string>("project_cfg_file");
            if(file)
            {
                std::string path = ZOO_get_parent_dir(file.get().data(),1);
                sprintf(cfg_path,"%s",path.data());
            }
		}
		else
		{
		    std::cout << "ZOO_get_user_cfg_path:: <" << ZOO_CFG_INFO_FILE << "> node <project_cfg_file> not exist ...ERROR" << std::endl;
		    error_code = ZOO_READ_FILE_FAILED_ERR;
		}
	}
    return error_code;
}

/**
 @brief get user files path
 @param ZOO_USER_FILE_PATH_STRUCT* path
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_user_files_path(INOUT ZOO_USER_PATH_STRUCT* files_path )
{
    std::lock_guard<std::recursive_mutex> lock(g_recursive_lock);
    ZOO_INT32 error_code = OK;
    ZOO_CHAR file[64]= {0x0};
    error_code = ZOO_get_user_cfg_file(file);
    if(OK == error_code)
    {   
        if(!boost::filesystem::exists(file))
    	{
		    std::cout << "ZOO_get_user_files_path:: <" << file << "> not exist ...ERROR" << std::endl;
    		error_code = ZOO_FILE_NOT_EXIST_ERR;
    	}
    }

    if(OK == error_code)
	{
        if(NULL == files_path)
        {
        	std::cout << "ZOO_get_user_files_path:: input parameter files_path pointer is nul ...ERROR" << std::endl;
            error_code = ZOO_PARAMETER_ERR;
        }
    }
    
	if(OK == error_code)
	{
	    boost::property_tree::ptree cfg_pt;
		boost::property_tree::read_json(file,cfg_pt);
		boost::optional<boost::property_tree::ptree&> configuration_tree = cfg_pt.get_child_optional("path.configuration");
		if(configuration_tree)
		{
    		/*boost::optional<std::string> project = configuration_tree.get().get_optional<std::string>("project_cfg_file");   
            if(project)
            {
                std::string file = project.get();
                sprintf(files_path->configure.project_cfg_file,"%s",file.data());
            }*/

            boost::optional<std::string> bin_path = configuration_tree.get().get_optional<std::string>("bin");   
            if(bin_path)
            {
                std::string path = bin_path.get();
                sprintf(files_path->configure.bin,"%s",path.data());
            }

            boost::optional<std::string> lib_path = configuration_tree.get().get_optional<std::string>("lib");   
            if(lib_path)
            {
                std::string path = lib_path.get();
                sprintf(files_path->configure.lib,"%s",path.data());
            }

            boost::optional<std::string> log_path = configuration_tree.get().get_optional<std::string>("log");   
            if(log_path)
            {
                std::string path = log_path.get();
                sprintf(files_path->configure.log,"%s",path.data());
            }

            boost::optional<std::string> error_code_path = configuration_tree.get().get_optional<std::string>("error_code");   
            if(error_code_path)
            {
                std::string path = error_code_path.get();
                sprintf(files_path->configure.error_code,"%s",path.data());
            }

            boost::optional<std::string> cert_path = configuration_tree.get().get_optional<std::string>("cert");   
            if(cert_path)
            {
                std::string path = cert_path.get();
                sprintf(files_path->configure.cert,"%s",path.data());
            }

            boost::optional<std::string> configuration_manager_path = configuration_tree.get().get_optional<std::string>("configuration_manager");   
            if(configuration_manager_path)
            {
                std::string path = configuration_manager_path.get();
                sprintf(files_path->configure.configuration_manager,"%s",path.data());
            }
		}
		else
		{
		    std::cout << "ZOO_get_user_files_path:: <" << file << ">: path.configuration node not exist ...ERROR" << std::endl;
		    error_code = ZOO_READ_FILE_FAILED_ERR;
		}

		boost::optional<boost::property_tree::ptree&> output_tree = cfg_pt.get_child_optional("path.output");if(configuration_tree)
		{
    		boost::optional<std::string> log = output_tree.get().get_optional<std::string>("log");   
            if(log)
            {
                std::string file = log.get();
                sprintf(files_path->output.log,"%s",file.data());
            }

            boost::optional<std::string> db = output_tree.get().get_optional<std::string>("database");   
            if(db)
            {
                std::string file = db.get();
                sprintf(files_path->output.db,"%s",file.data());
            }

            boost::optional<std::string> download = output_tree.get().get_optional<std::string>("download");   
            if(download)
            {
                std::string file = download.get();
                sprintf(files_path->output.download,"%s",file.data());
            }

            boost::optional<std::string> utils = output_tree.get().get_optional<std::string>("utils");   
            if(utils)
            {
                std::string file = utils.get();
                sprintf(files_path->output.utils,"%s",file.data());
            }
        }
        else
        {
		    error_code = ZOO_READ_FILE_FAILED_ERR;
		    std::cout << "ZOO_get_user_files_path:: <" << file << ">: path.output node not exist ...ERROR" << std::endl;
        }
	}
    return error_code;
}

/**
 @brief get platform files path
 @param ZOO_PL_PATH_STRUCT* path
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_pl_files_path(INOUT ZOO_PL_PATH_STRUCT* files_path)
{
    std::lock_guard<std::recursive_mutex> lock(g_recursive_lock);
    ZOO_INT32 error_code = OK;
    if(!boost::filesystem::exists(ZOO_CFG_INFO_FILE))
	{
		std::cout << "ZOO_get_pl_files_path:: <" << ZOO_CFG_INFO_FILE << "> not exist ...ERROR" << std::endl;
		error_code = ZOO_FILE_NOT_EXIST_ERR;
	}

    if(OK == error_code)
    {
    	if(!boost::filesystem::is_regular_file(ZOO_CFG_INFO_FILE))
    	{
    	    std::cout << "ZOO_get_pl_files_path:: <" << ZOO_CFG_INFO_FILE << "> is not a regular file ...ERROR" << std::endl;
		    error_code = ZOO_FILE_NOT_EXIST_ERR;
    	}
	}

    if(OK == error_code)
	{
        if(NULL == files_path)
        {
        	std::cout << "ZOO_get_pl_files_path:: input parameter files_path pointer is nul ...ERROR" << std::endl;
            error_code = ZOO_PARAMETER_ERR;
        }
        else
        {
            memset(files_path,0x0,sizeof(ZOO_PL_PATH_STRUCT));
        }
    }
    
	if(OK == error_code)
	{
	    boost::property_tree::ptree cfg_pt;
		boost::property_tree::read_json(ZOO_CFG_INFO_FILE,cfg_pt);
		boost::optional<boost::property_tree::ptree&> configuration_tree = cfg_pt.get_child_optional("path.configuration");
		if(configuration_tree)
		{
    		boost::optional<std::string> project = configuration_tree.get().get_optional<std::string>("project_cfg_file");   
            if(project)
            {
                std::string file = project.get();
                sprintf(files_path->configure.project_cfg_file,"%s",file.data());
            }

            boost::optional<std::string> bin_path = configuration_tree.get().get_optional<std::string>("bin");   
            if(bin_path)
            {
                std::string path = bin_path.get();
                sprintf(files_path->configure.bin,"%s",path.data());
            }

            boost::optional<std::string> log_path = configuration_tree.get().get_optional<std::string>("log");   
            if(log_path)
            {
                std::string path = log_path.get();
                sprintf(files_path->configure.log,"%s",path.data());
            }
            
            boost::optional<std::string> lib_path = configuration_tree.get().get_optional<std::string>("lib");   
            if(lib_path)
            {
                std::string path = lib_path.get();
                sprintf(files_path->configure.lib,"%s",path.data());
            }

            boost::optional<std::string> error_code_path = configuration_tree.get().get_optional<std::string>("error_code");   
            if(error_code_path)
            {
                std::string path = error_code_path.get();
                sprintf(files_path->configure.error_code,"%s",path.data());
            }

            boost::optional<std::string> cert_path = configuration_tree.get().get_optional<std::string>("cert");   
            if(cert_path)
            {
                std::string path = cert_path.get();
                sprintf(files_path->configure.cert,"%s",path.data());
            }

            boost::optional<std::string> configuration_manager_path = configuration_tree.get().get_optional<std::string>("configuration_manager");   
            if(configuration_manager_path)
            {
                std::string path = configuration_manager_path.get();
                sprintf(files_path->configure.configuration_manager,"%s",path.data());
            }
		}
		else
		{
		    std::cout << "ZOO_get_pl_files_path:: <" << ZOO_CFG_INFO_FILE << ">: path.configuration node not exist ...ERROR" << std::endl;
		    error_code = ZOO_READ_FILE_FAILED_ERR;
		}

		boost::optional<boost::property_tree::ptree&> output_tree = cfg_pt.get_child_optional("path.output");if(configuration_tree)
		{
    		boost::optional<std::string> log = output_tree.get().get_optional<std::string>("log");   
            if(log)
            {
                std::string file = log.get();
                sprintf(files_path->output.log,"%s",file.data());
            }

            boost::optional<std::string> db = output_tree.get().get_optional<std::string>("database");   
            if(db)
            {
                std::string file = db.get();
                sprintf(files_path->output.db,"%s",file.data());
            }

            boost::optional<std::string> download = output_tree.get().get_optional<std::string>("download");   
            if(download)
            {
                std::string file = download.get();
                sprintf(files_path->output.download,"%s",file.data());
            }

            boost::optional<std::string> utils = output_tree.get().get_optional<std::string>("utils");   
            if(utils)
            {
                std::string file = utils.get();
                sprintf(files_path->output.utils,"%s",file.data());
            }
        }
        else
        {
		    error_code = ZOO_READ_FILE_FAILED_ERR;
		    std::cout << "ZOO_get_pl_files_path:: <" << ZOO_CFG_INFO_FILE << ">: path.output node not exist ...ERROR" << std::endl;
        }
	}
    return error_code;
}

/**
 @brief get all files path,include user and platform
 @param ZOO_PATH_STRUCT* path
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_files_path(INOUT ZOO_PATH_STRUCT* files_path)
{
    ZOO_INT32 error_code = OK;
    
    if(OK == error_code)
    {
        error_code = ZOO_get_user_files_path(&files_path->user);
    }

    if(OK == error_code)
    {
        error_code = ZOO_get_pl_files_path(&files_path->platform);
    }

    return error_code;
}


/**
 @brief get resource id from brown dragon
 @param resource_id
 @return error code
**/
ZOO_EXPORT ZOO_INT32 ZOO_get_resource_id(INOUT ZOO_CHAR resource_id[ZOO_RESOURCE_ID_LENGHT])
{
	ZOO_INT32 error_code = OK;
    ZOO_CHAR file[64]= {0x0};
    std::string cfg_file;
    try
    {
        error_code = ZOO_get_user_cfg_file(file);
        if(OK != error_code)
        {   
            throw std::logic_error("get user config file failed");
        }
        
        if(!boost::filesystem::exists(file))
    	{
    		throw std::logic_error("file not existence");
    	}
                
        if(NULL == resource_id)
        {
        	throw std::logic_error("file not existence");
        }

        memset(resource_id,0x0,sizeof(ZOO_CHAR)*ZOO_RESOURCE_ID_LENGHT);
        boost::property_tree::ptree resource_ptree;
		boost::property_tree::read_json(file,resource_ptree);
		cfg_file = resource_ptree.get<std::string>("resource.file");
		if(boost::filesystem::exists(cfg_file))
		{
    		std::string key = resource_ptree.get<std::string>("resource.key");
    		boost::property_tree::ptree re_ptree;
    		boost::property_tree::read_json(cfg_file,re_ptree);  
    		std::string id = re_ptree.get<std::string>(key);
    		sprintf(resource_id,"%s",id.data());
		}
		else
		{ 
		    error_code = ZOO_PARAMETER_ERR;
	        std::cout << "ZOO_get_resource_id: " << cfg_file <<  " not exist" << std::endl;
		}
	}
	catch(std::exception & e)
	{
	    error_code = ZOO_PARAMETER_ERR;
	    std::cout << "ZOO_get_resource_id: " << cfg_file <<  ":" << e.what() << std::endl;
	}
	catch(...)
	{
	    error_code = ZOO_PARAMETER_ERR;
	}
    return error_code;
}

/**
 @brief get timestamp 
 @param timestamp
 @return error code
**/
ZOO_EXPORT void ZOO_get_current_timestamp(INOUT ZOO_CHAR timestamp[ZOO_TIME_BUFFER_LENGHT])
{
	std::string t = boost::posix_time::to_iso_string(boost::posix_time::microsec_clock::local_time());
    int pos = t.find('T');
    t.replace(pos,1,std::string("-"));
    t.replace(pos + 3,0,std::string(":"));
    t.replace(pos + 6,0,std::string(":"));
    memcpy(timestamp,t.data(),t.size() + 1);
    timestamp[ZOO_TIME_BUFFER_LENGHT - 1] = '\0';
    return;
}

/**
 @brief get timestamp by mircoseconds
 @return mircoseconds
**/
ZOO_EXPORT ZOO_UINT64 ZOO_get_current_timestamp_usec()
{
	struct timeval tv;
    gettimeofday(&tv,NULL);
    return (ZOO_UINT64)tv.tv_sec*1000000 + tv.tv_usec;
}

/**
 @brief get timestamp by milliseconds
 @return milliseconds
**/
ZOO_EXPORT ZOO_UINT64 ZOO_get_current_timestamp_msec()
{
	struct timeval tv;
    gettimeofday(&tv,NULL);
    return (ZOO_UINT64)tv.tv_sec*1000 + tv.tv_usec/1000;
}

/**
 @brief get current execute path
 @param resource_id
 @return error code
**/
ZOO_EXPORT const ZOO_CHAR*  ZOO_get_current_exec_path()
{
	g_current_exec_path =  boost::filesystem::current_path().string();
	return g_current_exec_path.data();
}

/**
 @brief get current parent path
 @param resource_id
 @return error code
**/
ZOO_EXPORT const ZOO_CHAR*  ZOO_get_parent_dir(INOUT const ZOO_CHAR dir[ZOO_PATH_LENGHT],
														IN ZOO_INT32 level)
{
	boost::filesystem::path current_dir(dir);
    while (level > 0)
    {
        if (!current_dir.has_parent_path())
        {
            break;
        }
        current_dir = current_dir.parent_path();
        level--;
    }
    g_parent_path = current_dir.string();
	return g_parent_path.data();
}


/**
 @brief get self process name
 @return process name
**/
ZOO_EXPORT const ZOO_CHAR * ZOO_get_process_name()
{
	return program_invocation_short_name;
}

/**
 @brief get user name
 @return user name
**/
ZOO_EXPORT const ZOO_CHAR * ZOO_get_uname()
{
	return getenv("USER");
}

/**
 @brief get thread id
 @return id
**/
ZOO_EXPORT ZOO_LONG ZOO_get_thread_id()
{
	std::stringstream ss;
	ss << std::this_thread::get_id();
	ZOO_LONG id = std::stoul(ss.str());
	return id;
}

/**
 *@brief Query tasks configurations from database.
 *@param f -the callback function
 *@param arg - function parameter
 *return error code
*/
ZOO_EXPORT ZOO_INT32 ZOO_create_thread(void *(*f) (void *),void * arg)
{
	std::thread t([=](){f(arg);});
	t.detach();
	return OK;
}

/**
 *@brief Query tasks configurations from database.
 *@param f -the callback function
 *@param arg - function parameter
 *return error code
*/
ZOO_EXPORT ZOO_INT32 ZOO_create_timer(void *(*f) (void *),
											IN void * arg,
											IN ZOO_INT32 milliseconds,
											INOUT ZOO_HANDLE * id)
{
	std::unique_lock<std::mutex> l(g_sync_lock_for_timer); 
	if(g_timer_map.size() > ZOO_THREAD_NUM_MAX)
	{
		return ZOO_PARAMETER_ERR;
	}
    static boost::mt19937 engine(boost::random_device{}());
    static boost::random::uniform_int_distribution<ZOO_HANDLE> distribution;
    *id = static_cast<ZOO_HANDLE> (distribution(engine));
    if(g_timer_map.find(*id) != g_timer_map.end())
	{	
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: Create timer [id: %d] success",*id);
		g_timer_map[*id] = ZOO_TRUE;
		std::thread t([=]()
		{
			while(g_timer_map[*id])
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
				f(arg);
			};
		});
		t.detach();
	}
	return OK;
}

/**
 *@brief Query tasks configurations from database.
 *@param id -the timer id
 *return error code
*/
ZOO_EXPORT ZOO_INT32 ZOO_stop_timer(IN ZOO_HANDLE id)
{
	std::unique_lock<std::mutex> l(g_sync_lock_for_timer); 
	auto ite = g_timer_map.find(id);
	if(ite != g_timer_map.end())
	{
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: stop timer [id: %d] success",id);
		g_timer_map[id] = ZOO_FALSE;
		g_timer_map.erase(ite);
	}
	return OK;
}


bool ZOO_check_this_process_is_TRMA()
{
    bool result = false;
    
    //TRMA is treated as user's 
    const std::string TRMA = "TRMA";

    //get this process name by linux
    const std::string this_process_name = program_invocation_short_name;
    
    if(this_process_name == TRMA)
    {
        result = true;
    }

    return result;
}

ZOO_INT32 ZOO_get_log_server(INOUT ZOO_LOG_SERVER_STRUCT* log_server)
{
    ZOO_INT32 error_code = OK;
    ZOO_CHAR file[64]= {0x0};
    try
	{
	    if(NULL == log_server)
        {
            throw std::invalid_argument("input parameter log_server pointer is null ...ERROR");
        }
        
        if(OK != ZOO_get_user_cfg_file(file))
        {   
            throw std::invalid_argument("remote_log_server.port is not type string ...ERROR");
        }

        if(!boost::filesystem::exists(file))
    	{
    		throw std::invalid_argument("user_cfg_file not exist ...ERROR");
    	}
        	
        memset(log_server,0x0,sizeof(ZOO_LOG_SERVER_STRUCT));
        log_server->enabled = ZOO_FALSE;
        boost::property_tree::ptree cfg_pt;
	    boost::property_tree::read_json(file,cfg_pt);
	    boost::property_tree::ptree server_pt = cfg_pt.get_child("servers.log");			    
	    std::string enable = server_pt.get<std::string>("enable");
	    std::string address = server_pt.get<std::string>("address"); 
	    std::string port = server_pt.get<std::string>("port");
	    sprintf(log_server->addr,"%s",address.c_str());
	    
	    if(EOF == sscanf(port.c_str(),"%hu",&log_server->port))
	    {
	        throw std::invalid_argument("remote_log_server.port is not type string ...ERROR");
	    }
	    
	    if(enable == "true" or enable == "TRUE")
	    {
	        log_server->enabled = ZOO_TRUE;
	    }
    }
    catch(std::exception & e)
    {
        std::cout << "ZOO_get_log_server:: " << e.what() << std::endl;
        error_code = ZOO_PARAMETER_ERR;
    }
    return error_code;
}


static std::once_flag log_init_once;

/**
 *@brief simple log 
*/
void ZOO_slog(IN ZOO_SEVERITY_LEVEL_ENUM level,
									IN const char * function_name,
									IN const char * format_spec, ...)
{
	const int buffer_len = 1024; // max 1k 
	char buffer[buffer_len] = {0};
    va_list args;
	va_start(args, format_spec);
	// print out remainder of message
	vsnprintf(buffer,buffer_len, format_spec, args);
	va_end(args);
	
	//initialize log system 
    std::call_once(log_init_once,
    [&]{ 
        if(ZOO_check_this_process_is_TRMA())
        {
            ZOO_USER_PATH_STRUCT user_path;
            if(OK == ZOO_get_user_files_path(&user_path))
            {
                ZOO_log_init(user_path.output.log,user_path.configure.log);
            }
        }
        else
        {
            ZOO_LOG_SERVER_STRUCT log_server;
            if(OK == ZOO_get_log_server(&log_server))
            {
                ZOO_syslog_init(&log_server);
            }
        }});
	ZOO_log_s(level,function_name,buffer);
}

/**
 *@brief Get zoo platform tasks
 *@param zoo_tasks     
 *@return error_code
 */
ZOO_EXPORT ZOO_INT32 ZOO_get_pl_tasks(ZOO_TASKS_STRUCT* pl_tasks)
{
    std::lock_guard<std::recursive_mutex> lock(g_recursive_lock);
    ZOO_INT32 error_code = OK;
    if(!boost::filesystem::exists(ZOO_CFG_INFO_FILE))
	{
		std::cout << "ZOO_get_pl_files_path:: <" << ZOO_CFG_INFO_FILE << "> not exist ...ERROR" << std::endl;
		error_code = ZOO_FILE_NOT_EXIST_ERR;
	}

    if(OK == error_code)
	{
    	if(NULL == pl_tasks)
        {
		    std::cout << "ZOO_get_pl_tasks:: input parameter files_path pointer is nul ...ERROR" << std::endl;
            error_code = ZOO_PARAMETER_ERR;
        }
        else
        {
            memset(pl_tasks,0x0,sizeof(ZOO_TASKS_STRUCT));
        }
    }
    
    ZOO_PL_PATH_STRUCT files_path;
    if(OK == error_code)
    {
        error_code = ZOO_get_pl_files_path(&files_path);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"::configure.bin:%s",files_path.configure.bin);
    }

    try
    {
    	if(OK == error_code)
    	{
    	    boost::property_tree::ptree cfg_pt;
    		boost::property_tree::read_json(ZOO_CFG_INFO_FILE,cfg_pt);
    		boost::property_tree::ptree v_pt = cfg_pt.get_child("tasks");
    		BOOST_FOREACH(boost::property_tree::ptree::value_type & v, v_pt)
            {
    		    std::string comp_id = v.second.get<std::string>("comp_id");
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"::comp_id: %s",comp_id.c_str());
    		    std::string process_name = v.second.get<std::string>("process_name");
    		    std::string trace_mode = v.second.get<std::string>("trace_mode"); 
    		    std::string sim_mode = v.second.get<std::string>("sim_mode"); 
    		    std::string moniter_mode = v.second.get<std::string>("moniter_mode"); 
    		    ZOO_INT32 stack_size = boost::lexical_cast<ZOO_INT32>(v.second.get<std::string>("stack_size")); 
    		    ZOO_INT32 priority = boost::lexical_cast<ZOO_INT32>(v.second.get<std::string>("priority"));
    		    std::string environment_variable = v.second.get<std::string>("environment_variable"); 
    		    std::string error_code_definition_file = v.second.get<std::string>("error_code_definition_file"); 
    		    std::string configuration_manager_file = v.second.get<std::string>("configuration_manager_file"); 
    		    std::string mock_file = v.second.get<std::string>("mock_file"); 	    
	            std::string fe_thread_number = v.second.get<std::string>("fe_thread_number"); 
	            std::string be_thread_number = v.second.get<std::string>("be_thread_number"); 
	            pl_tasks->task[pl_tasks->number].fe_thread_number = boost::lexical_cast<ZOO_INT32>(fe_thread_number);
	            pl_tasks->task[pl_tasks->number].be_thread_number = boost::lexical_cast<ZOO_INT32>(be_thread_number);
    		    sprintf(pl_tasks->task[pl_tasks->number].bin_path, "%s",files_path.configure.bin);
    		    sprintf(pl_tasks->task[pl_tasks->number].component,"%s",comp_id.data());
    		    sprintf(pl_tasks->task[pl_tasks->number].task_name,"%s",process_name.data());
    		    sprintf(pl_tasks->task[pl_tasks->number].env_var,"%s",environment_variable.data());
    		    sprintf(pl_tasks->task[pl_tasks->number].ec_definition_file,"%s",error_code_definition_file.data());
    		    sprintf(pl_tasks->task[pl_tasks->number].cm_file,"%s",configuration_manager_file.data());
    		    sprintf(pl_tasks->task[pl_tasks->number].mock_file,"%s",mock_file.data());
    		    pl_tasks->task[pl_tasks->number].priority   = priority;
    		    pl_tasks->task[pl_tasks->number].stack_size = stack_size;
                pl_tasks->task[pl_tasks->number].sim_mode   = ZOO_str_to_sim_mode(sim_mode.data());
    		    pl_tasks->task[pl_tasks->number].trace_mode = ZOO_str_to_trace_mode(trace_mode.data());
    		    pl_tasks->task[pl_tasks->number].monitor_mode = ZOO_str_to_watch_mode(moniter_mode.data());
    		    pl_tasks->number ++;
    		}
    	}
	}
	catch(std::exception & e)
	{
	    ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,"%s",e.what());
	}
    return error_code;
}

/**
 *@brief Get zoo platform tasks
 *@param zoo_tasks     
 *@return error_code
 */
ZOO_EXPORT ZOO_INT32 ZOO_get_user_tasks(ZOO_TASKS_STRUCT* user_tasks)
{
    std::lock_guard<std::recursive_mutex> lock(g_recursive_lock);
    ZOO_INT32 error_code = OK;
    ZOO_CHAR file[64]= {0x0};
    try
    {
        error_code = ZOO_get_user_cfg_file(file);
        if(OK != error_code)
        {   
            throw std::logic_error("get user config file failed");
        }

        if(!boost::filesystem::exists(file))
    	{
    		throw std::invalid_argument("file not exist ...ERROR");
    	}
    	
    	if(NULL == user_tasks)
        {
            throw std::invalid_argument("input user_tasks is null ...ERROR");
        }
        
        memset(user_tasks,0x0,sizeof(ZOO_TASKS_STRUCT));

        
        ZOO_USER_PATH_STRUCT files_path;
        if(OK != ZOO_get_user_files_path(&files_path))
        {
            throw std::logic_error("get user config file path failed ...ERROR");
        }
        
    	boost::property_tree::ptree cfg_pt;
    	boost::property_tree::read_json(file,cfg_pt);
    	boost::property_tree::ptree v_pt = cfg_pt.get_child("tasks");
        BOOST_FOREACH(boost::property_tree::ptree::value_type & v, v_pt)
		{
		    std::string comp_id = v.second.get<std::string>("comp_id");
		    std::string process_name = v.second.get<std::string>("process_name");
		    std::string trace_mode = v.second.get<std::string>("trace_mode"); 
		    std::string sim_mode = v.second.get<std::string>("sim_mode"); 
		    std::string moniter_mode = v.second.get<std::string>("moniter_mode"); 
		    ZOO_INT32 stack_size = boost::lexical_cast<ZOO_INT32>(v.second.get<std::string>("stack_size")); 
		    ZOO_INT32 priority = boost::lexical_cast<ZOO_INT32>(v.second.get<std::string>("priority")); 
		    std::string environment_variable = v.second.get<std::string>("environment_variable"); 
		    std::string error_code_definition_file = v.second.get<std::string>("error_code_definition_file"); 
		    std::string configuration_manager_file = v.second.get<std::string>("configuration_manager_file"); 
		    std::string mock_file = v.second.get<std::string>("mock_file");  
		    std::string fe_thread_number = v.second.get<std::string>("fe_thread_number"); 
            std::string be_thread_number = v.second.get<std::string>("be_thread_number"); 
            user_tasks->task[user_tasks->number].fe_thread_number = boost::lexical_cast<ZOO_INT32>(fe_thread_number);
            user_tasks->task[user_tasks->number].be_thread_number = boost::lexical_cast<ZOO_INT32>(be_thread_number); 
		    sprintf(user_tasks->task[user_tasks->number].bin_path, "%s",files_path.configure.bin);
		    sprintf(user_tasks->task[user_tasks->number].component,"%s",comp_id.data());
		    sprintf(user_tasks->task[user_tasks->number].task_name,"%s",process_name.data());
		    sprintf(user_tasks->task[user_tasks->number].env_var,"%s",environment_variable.data());
		    sprintf(user_tasks->task[user_tasks->number].ec_definition_file,"%s",error_code_definition_file.data());
		    sprintf(user_tasks->task[user_tasks->number].cm_file,"%s",configuration_manager_file.data());
		    sprintf(user_tasks->task[user_tasks->number].mock_file,"%s",mock_file.data());
		    user_tasks->task[user_tasks->number].priority = priority;
		    user_tasks->task[user_tasks->number].stack_size = stack_size;
            user_tasks->task[user_tasks->number].sim_mode = ZOO_str_to_sim_mode(sim_mode.data());
		    user_tasks->task[user_tasks->number].trace_mode = ZOO_str_to_trace_mode(trace_mode.data());
		    user_tasks->task[user_tasks->number].monitor_mode = ZOO_str_to_watch_mode(moniter_mode.data());
		    user_tasks->number ++;
		}
    }
    catch(std::exception & e)
    {
        error_code = ZOO_PARAMETER_ERR;
        ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,":: %s ",e.what());
    }
    return error_code;
}

/**
 *@brief Update user configure tasks
 *@param zoo_tasks     
 *@return error_code
 */
ZOO_EXPORT ZOO_INT32 ZOO_update_task_configuration(IN ZOO_TASK_STRUCT* task)
{
    std::lock_guard<std::recursive_mutex> lock(g_recursive_lock);
    ZOO_INT32 error_code = OK;
    ZOO_TASKS_STRUCT tasks;
    ZOO_CHAR user_cfg_file[64]= {0x0};
    std::string cfg_file;
    if(task == NULL)
    {
        error_code = ZOO_PARAMETER_ERR;
		std::cout << "ZOO_update_user_tasks:: user_task ponter is null ...ERROR" << std::endl;
		ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,":: %s ...ERROR","input parameter user_task pointer is null");
    }
    
    if(OK == error_code)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: process_name is %s",task->task_name);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: component is %s",task->component);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: trace_mode is %s",ZOO_trace_mode_enum_to_str(task->trace_mode));
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: sim_mode is %s",ZOO_trace_mode_enum_to_str(task->trace_mode));    
    }

    if(OK == error_code)
    {
        error_code =  ZOO_get_user_tasks(&tasks);
        if(OK == error_code)
        {
            error_code = ZOO_get_user_cfg_file(user_cfg_file);
        }
        
        if(OK == error_code)
        {
            int i = 0;
            for(; i < tasks.number; ++i)
            {
                if(strcmp(task->component,tasks.task[i].component) == 0)
                {
                    cfg_file = user_cfg_file;
                    break;
                }
            }

            if(i >= tasks.number)
            {   
                error_code = ZOO_get_pl_tasks(&tasks);
                if(OK == error_code)
                {
                    for(i = 0; i < tasks.number; ++i)
                    {
                        if(strcmp(task->component,tasks.task[i].component) == 0)
                        {
                            cfg_file = ZOO_CFG_INFO_FILE;
                            break;
                        }
                    }

                    if(i >= tasks.number)
                    {
                        error_code = ZOO_PARAMETER_ERR;
                    }
                }
            }
        }
    }

    if(OK == error_code)
    {        
		try
		{  
            boost::property_tree::ptree cfg_pt;
            boost::property_tree::read_json(cfg_file,cfg_pt);
            boost::property_tree::ptree & pt = cfg_pt.get_child("tasks");
            boost::property_tree::ptree::iterator it = pt.begin();
            while(it != pt.end())
            {
                if(it->second.get<std::string>("comp_id") == task->component)
    		    {
    		        it->second.put<std::string>("trace_mode",ZOO_trace_mode_enum_to_str(task->trace_mode));
    		        it->second.put<std::string>("sim_mode",ZOO_sim_mode_enum_to_str(task->sim_mode));
    		        it->second.put<std::string>("moniter_mode",ZOO_watch_mode_enum_to_str(task->monitor_mode));
    		        it->second.put<std::string>("stack_size",std::to_string(task->stack_size));
    		        it->second.put<std::string>("priority",std::to_string(task->priority));
    		        it->second.put<std::string>("fe_thread_number",std::to_string(task->fe_thread_number));
    		        it->second.put<std::string>("be_thread_number",std::to_string(task->be_thread_number));
    		        break;
    		    }
    		    it ++;
            }

            if(it == pt.end())
            {
                boost::property_tree::ptree new_pt;
                new_pt.put<std::string>("comp_id",task->component);
                new_pt.put<std::string>("process_name",task->task_name);
                new_pt.put<std::string>("trace_mode",ZOO_trace_mode_enum_to_str(task->trace_mode));
                new_pt.put<std::string>("sim_mode",ZOO_sim_mode_enum_to_str(task->sim_mode));
                new_pt.put<std::string>("moniter_mode",ZOO_watch_mode_enum_to_str(task->monitor_mode));
                new_pt.put<std::string>("stack_size",std::to_string(task->stack_size));
                new_pt.put<std::string>("priority",std::to_string(task->priority));
                new_pt.put<std::string>("environment_variable",task->env_var); 
    		    new_pt.put<std::string>("error_code_definition_file",task->ec_definition_file); 
    		    new_pt.put<std::string>("configuration_manager_file",task->cm_file); 
    		    new_pt.put<std::string>("mock_file",task->mock_file);  
    		    new_pt.put<std::string>("fe_thread_number",std::to_string(task->fe_thread_number));
    		    new_pt.put<std::string>("be_thread_number",std::to_string(task->be_thread_number));
                pt.push_back(std::make_pair("", new_pt));
            }
                
		    boost::property_tree::write_json(cfg_file,cfg_pt);
		}
		catch(boost::property_tree::ptree_error & e)
		{
		    error_code = ZOO_PARAMETER_ERR;
		    std::cout << "ZOO_update_task_configuration::"<< e.what() << " ...ERROR" << std::endl;
		}
    }
    return error_code;
}


/**
 *@brief Get zoo entity tasks
 *@param zoo_tasks     
 *@return error_code
 */
ZOO_EXPORT ZOO_INT32 ZOO_get_entity_tasks(ZOO_TASKS_STRUCT* tasks)
{
    ZOO_TASKS_STRUCT zoo_tasks;
    ZOO_TASKS_STRUCT user_tasks;
    ZOO_INT32 error_code = OK;
    
    if(tasks == NULL)
    {
        error_code = ZOO_PARAMETER_ERR;
    }
    else
    {
        memset(tasks,0x0,sizeof(ZOO_TASKS_STRUCT));
    }
    
    if(OK == error_code)
    {
        error_code = ZOO_get_pl_tasks(&zoo_tasks);
        if(OK == error_code)
        {
            error_code = ZOO_get_user_tasks(&user_tasks);
            if(OK == error_code)
            {
                memcpy(tasks,&zoo_tasks,sizeof(ZOO_TASKS_STRUCT));
            }
        }
    }
    
    if(OK == error_code)
    {
        tasks->number = zoo_tasks.number + user_tasks.number;
        if(tasks->number < 128)
        {
            for(int i = 0;i < user_tasks.number; ++i)
            {
                memcpy(&tasks->task[zoo_tasks.number + i],&user_tasks.task[i],sizeof(ZOO_TASK_STRUCT));
            }
        }
    }
    return error_code;
}


/**
 *@brief Get broker infor
 *@param     broker
 *@return error_code.
*/
ZOO_EXPORT ZOO_INT32 ZOO_get_brokers(INOUT ZOO_BROKER_STRUCT * broker)
{
    std::lock_guard<std::recursive_mutex> lock(g_recursive_lock);
    ZOO_INT32 error_code = OK;
    try
    {
        if(!boost::filesystem::exists(ZOO_CFG_INFO_FILE))
    	{
    		throw std::invalid_argument("file not exist ...ERROR");
    	}
    	
    	if(NULL == broker)
        {
            throw std::invalid_argument("input broker is null ...ERROR");
        }
        
        memset(broker,0x0,sizeof(ZOO_BROKER_STRUCT));
        
	    boost::property_tree::ptree cfg_pt;
		boost::property_tree::read_json(ZOO_CFG_INFO_FILE,cfg_pt);
		boost::property_tree::ptree v_pt = cfg_pt.get_child("broker");
		std::string local_frontend_send = v_pt.get<std::string>("local_frontend_send");
	    std::string local_frontend_recv = v_pt.get<std::string>("local_frontend_recv"); 
	    std::string local_backend_send = v_pt.get<std::string>("local_backend_send"); 
	    std::string local_backend_recv = v_pt.get<std::string>("local_backend_recv"); 
	    std::string remote_backend_send = v_pt.get<std::string>("remote_backend_send"); 
	    std::string remote_backend_recv = v_pt.get<std::string>("remote_backend_recv"); 
	    std::string type = v_pt.get<std::string>("mode"); 
	    broker->hwm = boost::lexical_cast<ZOO_INT32>(v_pt.get<std::string>("hwm")); 
	    broker->linger = boost::lexical_cast<ZOO_INT32>(v_pt.get<std::string>("linger"));
	    
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: broker->hwm is %d",broker->hwm); 
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: broker->linger is %d",broker->linger);
	    sprintf(broker->local_fe_send,"%s",local_frontend_send.data());
	    sprintf(broker->local_fe_recv,"%s",local_frontend_recv.data());
	    sprintf(broker->local_be_send,"%s",local_backend_send.data());
	    sprintf(broker->local_be_recv,"%s",local_backend_recv.data());
	    sprintf(broker->remote_frontend,"%s",remote_backend_send.data());
	    sprintf(broker->remote_backend,"%s",remote_backend_recv.data());
	    sprintf(broker->mode,"%s",type.data());
		
	}
	catch(std::exception & e)
	{
	    error_code = ZOO_PARAMETER_ERR;
	    std::cout << "ZOO_get_brokers::"<< e.what() << " ...ERROR" << std::endl;
	}
    return error_code;
}

/**
 *@brief Get host_info.
 *@param     host_info
 *@return error_code.
*/
ZOO_EXPORT ZOO_INT32 ZOO_get_host_info(INOUT ZOO_HOST_INFO_STRUCT * host_info)
{
    std::lock_guard<std::recursive_mutex> lock(g_recursive_lock);
    ZOO_INT32 error_code = OK;
    try
    {    
        if(!boost::filesystem::exists(ZOO_CFG_INFO_FILE))
    	{
    		throw std::invalid_argument("file not exist ...ERROR");
    	}
    	
    	if(NULL == host_info)
        {
            throw std::invalid_argument("input host_info is null ...ERROR");
        }

        memset(host_info,0x0,sizeof(ZOO_HOST_INFO_STRUCT));
        
    	boost::property_tree::ptree cfg_pt;
    	boost::property_tree::read_json(ZOO_CFG_INFO_FILE,cfg_pt);
    	boost::property_tree::ptree v_pt = cfg_pt.get_child("host");
    	std::string id = v_pt.get<std::string>("id");
        std::string name = v_pt.get<std::string>("name"); 
        std::string location = v_pt.get<std::string>("location"); 
        std::string GPS = v_pt.get<std::string>("GPS"); 
        std::string domain = v_pt.get<std::string>("domain"); 
        host_info->present = v_pt.get<std::string>("present") == "true";
        sprintf(host_info->id,"%s",id.data());
        sprintf(host_info->name,"%s",name.data());
        sprintf(host_info->location,"%s",location.data());
        sprintf(host_info->gps,"%s",GPS.data());
	}
	catch(std::exception & e)
	{
	    error_code = ZOO_PARAMETER_ERR;
	    std::cout << "ZOO_get_host_info::"<< e.what() << " ...ERROR" << std::endl;
	}
    return error_code;
}

/**
 *@brief Get http server parameters.
 *@param    server
 *@return error_code.
*/
ZOO_EXPORT ZOO_INT32 ZOO_get_server_info(ZOO_SERVER_CFG_STRUCT * server)
{
    std::lock_guard<std::recursive_mutex> lock(g_recursive_lock);
    ZOO_INT32 error_code = OK;
    ZOO_CHAR file[64]= {0x0};
    try
    {    
        if(OK == ZOO_get_user_cfg_file(file))
        {
            throw std::invalid_argument("file not exist ...ERROR");
        }
        
        if(!boost::filesystem::exists(file))
    	{
    		throw std::invalid_argument("file not exist ...ERROR");
    	}
    	
    	if(NULL == server)
        {
            throw std::invalid_argument("input server is null ...ERROR");
        }
        memset(server,0x0,sizeof(ZOO_SERVER_CFG_STRUCT));
	    boost::property_tree::ptree cfg_pt;
		boost::property_tree::read_json(file,cfg_pt);
		boost::property_tree::ptree v_pt = cfg_pt.get_child("servers.HTTP");
		std::string address = v_pt.get<std::string>("address");
        sprintf(server->ip,"%s",address.data()); 		
	    std::string port = v_pt.get<std::string>("port"); 
		sprintf(server->port,"%s",port.data());
		std::string cert_path = v_pt.get<std::string>("cert_path"); 
		sprintf(server->cert_path,"%s",cert_path.data());		
	    std::string cert = v_pt.get<std::string>("cert"); 
		sprintf(server->cert,"%s",cert.data());	
	    std::string priv_key = v_pt.get<std::string>("priv_key"); 
		sprintf(server->private_key,"%s",priv_key.data());		
	    std::string trust_chain = v_pt.get<std::string>("trust_chain"); 
		sprintf(server->trust_chain,"%s",trust_chain.data());		
	    server->tls_version = v_pt.get<ZOO_INT32>("tls_version"); 
	    server->verify_mode = v_pt.get<ZOO_INT32>("verify_mode"); 
	    server->thread_number = v_pt.get<ZOO_INT32>("thread_number"); 		    
	}
	catch(std::exception & e)
	{
	    error_code = ZOO_PARAMETER_ERR;
	    std::cout << "ZOO_get_server_info::"<< e.what() << " ...ERROR" << std::endl;
	}
    return error_code;
}

/**
 *@brief Get OTA  parameters.
 *@param     
 *@return error_code.
*/
ZOO_EXPORT  ZOO_INT32 ZOO_get_OTA_info(ZOO_OTA_CFG_STRUCT *ota)
{
    std::lock_guard<std::recursive_mutex> lock(g_recursive_lock);
    ZOO_INT32 error_code = OK;
    ZOO_CHAR file[64]= {0x0};
    error_code = ZOO_get_user_cfg_file(file);
    if(OK == error_code)
    {   
        if(!boost::filesystem::exists(file))
    	{
		    std::cout << "ZOO_get_OTA_info::file missing ...ERROR" << std::endl;
    		error_code = ZOO_FILE_NOT_EXIST_ERR;
    	}
    }
    
    if(OK == error_code)
	{
    	if(NULL == ota)
        {
		    std::cout << "ZOO_get_OTA_info::server pointer is null ...ERROR" << std::endl;
            error_code = ZOO_PARAMETER_ERR;
        }
        else
        {
            memset(ota,0x0,sizeof(ZOO_OTA_CFG_STRUCT));
        }
    }
    
	if(OK == error_code)
	{
    	try
        {    
            boost::property_tree::ptree cfg_pt;
    		boost::property_tree::read_json(file,cfg_pt);
    		boost::property_tree::ptree v_pt = cfg_pt.get_child("servers.OTA");
    		std::string url = v_pt.get<std::string>("url");
		    std::string token_signature = v_pt.get<std::string>("token_signature"); 
		    std::string user = v_pt.get<std::string>("user"); 
		    std::string password = v_pt.get<std::string>("password");
		    sprintf(ota->domain,"%s",url.data());
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: domain is %s",ota->domain);
		    sprintf(ota->token,"%s",token_signature.data());
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: token is %s",ota->token);
		    sprintf(ota->user,"%s",user.data());
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: user is %s",ota->user);
		    sprintf(ota->password,"%s",password.data());
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: password is %s",ota->password);
        }
    	catch(std::exception & e)
    	{
    	    error_code = ZOO_PARAMETER_ERR;
    	    std::cout << "ZOO_get_OTA_info::"<< e.what() << " ...ERROR" << std::endl;
    	}
	}
    return error_code;
}
