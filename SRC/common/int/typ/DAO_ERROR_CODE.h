/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No. : ZOO
 * Component ID : DAO
 * File Name : DAO_ERROR_CODE_H_
 * Description : Defines internal error code for RH system
 * History :
 * Version      Date        	User    		Comments
 * V1.0.0.0		2018-05-08     	MOZEAT    	Create file
 ***************************************************************/
#ifndef DAO_ERROR_CODE_H_
#define DAO_ERROR_CODE_H_

namespace DAO
{
#define DAO_BASE_ERROR                      (0x400)

// External error
#define DAO_CONNECT_ERROR                      (DAO_BASE_ERROR + 0x01)
#define DAO_OPEN_TRANSACTION_ERROR             (DAO_BASE_ERROR + 0x02)
#define DAO_COMMIT_TRANSACTION_ERROR           (DAO_BASE_ERROR + 0x03)
#define DAO_ROLL_BACK_TRANSACTION_ERROR        (DAO_BASE_ERROR + 0x04)
#define DAO_CLOSE_TRANSACTION_ERROR            (DAO_BASE_ERROR + 0x05)
#define DAO_CLOSE_CONNECTION_ERROR             (DAO_BASE_ERROR + 0x06)

} /* namespace DAO */

#endif /* DAO_ERROR_CODE_H_ */
