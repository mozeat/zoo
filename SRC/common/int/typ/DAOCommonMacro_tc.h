/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : DAO
 * File Name    : DAOCommonMacro_tc.h
 * Description  : {Summary Description}
 * History :
 * Version      Date				User				Comments
 * V1.0.0.0       2018-09-11		MOZEAT		Initialize
 ***************************************************************/
#ifndef DAOCOMMONMACRO_TC_H_
#define DAOCOMMONMACRO_TC_H_

/**
 * @brief Define a macro to throw WH exception
 * @param error_code        The error code
 * @param error_message     The error message
 * @param inner_exception   The inner exception
 */
#define __THROW_DAO_EXCEPTION(error_code, error_message, inner_exception) \
    throw DB_EXCEPTION_CLASS(error_code, error_message, inner_exception);
/*TR4A_trace(DAO_COMPONENT_ID, __FUNCTION_NAME__, \
		   ": function error - [%x][%s]", error_code, error_message); \
EH4A_show_exception("DB", __FILE__, __FUNCTION_NAME__,__LINE__,error_code, 0,error_message); \*/

#endif /* DAOCOMMONMACRO_TC_H_ */
