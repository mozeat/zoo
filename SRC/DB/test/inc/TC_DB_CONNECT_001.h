/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : DB
 * File Name      : DB_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-05    Generator      created
*************************************************************/
extern "C"
{
    #include <DB4A_if.h>
    #include <DB4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_DB_CONNECT_001)

/**
 *@brief 
 *@param option
 *@param session_id
**/
    BOOST_AUTO_TEST_CASE( TC_DB_CONNECT_001_001 )
    {
        DB4A_CONNECT_OPTION_STRUCT option;
        memset(&option, 0x00, sizeof(DB4A_CONNECT_OPTION_STRUCT));
        memcpy(option.db_name, "test", sizeof("test"));
        memcpy(option.name, "user_name", sizeof("user_name"));
        memcpy(option.host_name, "10.110.17.109", sizeof("10.110.17.109"));
        memcpy(option.password, "user_password", sizeof("user_password"));
        option.db_type = DB4A_MYSQL;

        BOOST_ASSERT(DB4A_connect(&TC_DB_GOLBAL_FIXTURE::m_session_id, &option) == OK);
        BOOST_TEST_MESSAGE("session id:" << TC_DB_GOLBAL_FIXTURE::m_session_id);
    }

BOOST_AUTO_TEST_SUITE_END()
