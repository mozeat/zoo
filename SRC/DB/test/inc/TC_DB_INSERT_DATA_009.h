/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : DB
 * File Name      : DB_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-05    Generator      created
*************************************************************/
extern "C"
{
    #include <DB4A_if.h>
    #include <DB4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_DB_INSERT_DATA_009)

/**
 *@brief 
 *@param session_id
 *@param table_name
 *@param new_data
 *@param fields_number
**/
    BOOST_AUTO_TEST_CASE( TC_DB_INSERT_DATA_009_001 )
    {
    const ZOO_CHAR table_name[256] = "test";
            const DB4A_FIELD_DATA_STRUCT new_data[3] = {
                {"id", "1", 0},
                {"name", "test_name", 0},
                {"age", "30", 0}
                };
            ZOO_INT32 fields_number = 3;

            BOOST_TEST(DB4A_insert_data(TC_DB_GOLBAL_FIXTURE::m_session_id,
                    table_name,
                    new_data,
                    fields_number) == OK);

            const DB4A_FIELD_DATA_STRUCT new_data2[3] = {
                {"id", "2", 0},
                {"name", "test_name1", 0},
                {"age", "33", 0}
                };
            fields_number = 3;
            BOOST_TEST(DB4A_insert_data(TC_DB_GOLBAL_FIXTURE::m_session_id,
                    table_name,
                    new_data2,
                    fields_number) == OK);

            const DB4A_FIELD_DATA_STRUCT new_data3[3] = {
                {"id", "3", 0},
                {"name", "test_name2", 0},
                {"age", "15", 0}
                };
            fields_number = 3;
            BOOST_TEST(DB4A_insert_data(TC_DB_GOLBAL_FIXTURE::m_session_id,
                    table_name,
                    new_data3,
                    fields_number) == OK);

            const DB4A_FIELD_DATA_STRUCT new_data4[3] = {
                {"id", "4", 0},
                {"name", "test_name3", 0},
                {"age", "78", 0}
                };
            fields_number = 3;
            BOOST_TEST(DB4A_insert_data(TC_DB_GOLBAL_FIXTURE::m_session_id,
                    table_name,
                    new_data4,
                    fields_number) == OK);

            const DB4A_FIELD_DATA_STRUCT new_data5[3] = {
                {"id", "5", 0},
                {"name", "test_name4", 0},
                {"age", "96", 0}
                };
            fields_number = 3;
            BOOST_TEST(DB4A_insert_data(TC_DB_GOLBAL_FIXTURE::m_session_id,
                    table_name,
                    new_data5,
                    fields_number) == OK);

    }

BOOST_AUTO_TEST_SUITE_END()
