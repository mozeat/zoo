/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : DB
 * File Name      : DB_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-05    Generator      created
*************************************************************/
extern "C"
{
    #include <DB4A_if.h>
    #include <DB4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_DB_DROP_TABLE_005)

/**
 *@brief 
 *@param session_id
 *@param table_name
**/
    BOOST_AUTO_TEST_CASE( TC_DB_DROP_TABLE_005_001 )
    {
        const ZOO_CHAR table_name[256] = "test";
        BOOST_TEST(DB4A_drop_table(TC_DB_GOLBAL_FIXTURE::m_session_id,table_name) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()
