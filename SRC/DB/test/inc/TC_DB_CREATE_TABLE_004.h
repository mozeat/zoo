/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : DB
 * File Name      : DB_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-05    Generator      created
*************************************************************/
extern "C"
{
    #include <DB4A_if.h>
    #include <DB4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_DB_CREATE_TABLE_004)

/**
 *@brief 
 *@param session_id
 *@param table_name
 *@param primary_key_name
 *@param fields
 *@param fields_number
**/
    BOOST_AUTO_TEST_CASE( TC_DB_CREATE_TABLE_004_001 )
    {
        const ZOO_CHAR primary_key_name[256] = "id";
        const DB4A_FIELD_TYPE_STRUCT fields[3] = {
            {DB4A_FIELD_TYPE_CHAR, 32, "id"},
            {DB4A_FIELD_TYPE_CHAR, 32, "name"},
            {DB4A_FIELD_TYPE_CHAR, 32, "age"}
        };
        ZOO_INT32 fields_number = 3;
        ZOO_CHAR table_name[256] = "test";

        BOOST_TEST(DB4A_create_table(TC_DB_GOLBAL_FIXTURE::m_session_id,table_name,primary_key_name,fields,fields_number) == OK);
        BOOST_TEST_MESSAGE("crate table session id:" << TC_DB_GOLBAL_FIXTURE::m_session_id);
    }

BOOST_AUTO_TEST_SUITE_END()
