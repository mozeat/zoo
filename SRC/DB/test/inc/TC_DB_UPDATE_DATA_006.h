/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : DB
 * File Name      : DB_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-05    Generator      created
*************************************************************/
extern "C"
{
    #include <DB4A_if.h>
    #include <DB4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_DB_UPDATE_DATA_006)

/**
 *@brief 
 *@param session_id
 *@param table_name
 *@param condition
 *@param new_datas
 *@param new_datas_number
**/
    BOOST_AUTO_TEST_CASE( TC_DB_UPDATE_DATA_006_001 )
    {
        ZOO_CHAR condition[256] = "id = 5";
        ZOO_CHAR table_name[256] = "test";
        const DB4A_FIELD_DATA_STRUCT new_datas[] = {
            "name", "haha", 0
            };
        ZOO_INT32 new_datas_number = 1;
        BOOST_TEST(DB4A_update_data(TC_DB_GOLBAL_FIXTURE::m_session_id,
                                table_name,condition,new_datas,new_datas_number) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()
