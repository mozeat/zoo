/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : DB
 * File Name      : DB_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-05    Generator      created
*************************************************************/
extern "C"
{
    #include <DB4A_if.h>
    #include <DB4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_DB_DELETE_DATA_BY_CONDITION_007)

/**
 *@brief 
 *@param session_id
 *@param table_name
 *@param condition
**/
    BOOST_AUTO_TEST_CASE( TC_DB_DELETE_DATA_BY_CONDITION_007_001 )
    {
        const ZOO_CHAR condition[256] = "id = 3";
        ZOO_CHAR table_name[256] = "test";
        BOOST_TEST(DB4A_delete_data_by_condition(TC_DB_GOLBAL_FIXTURE::m_session_id,table_name,condition) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()
