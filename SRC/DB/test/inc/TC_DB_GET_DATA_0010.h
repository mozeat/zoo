/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : DB
 * File Name      : DB_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-12    Generator      created
*************************************************************/
extern "C"
{
    #include <DB4A_if.h>
    #include <DB4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_DB_GET_DATA_0010)

/**
 *@brief 
 *@param session_id
 *@param record
**/
    BOOST_AUTO_TEST_CASE( TC_DB_GET_DATA_0010_001 )
    {
        ZOO_HANDLE session_id;
        DB4A_RECORD_STRUCT* record;
        BOOST_TEST(DB4A_get_data(session_id,record) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()