/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : DB
 * File Name      : DB_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-05    Generator      created
*************************************************************/
extern "C"
{
    #include <DB4A_if.h>
    #include <DB4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_DB_GET_CONNECTION_STATE_003)

/**
 *@brief 
 *@param session_id
 *@param state
**/
    BOOST_AUTO_TEST_CASE( TC_DB_GET_CONNECTION_STATE_003_001 )
    {
        ZOO_BOOL state;
        BOOST_TEST(DB4A_get_connection_state(TC_DB_GOLBAL_FIXTURE::m_session_id, &state) == OK);
        BOOST_TEST_MESSAGE("state:" << state);
    }

BOOST_AUTO_TEST_SUITE_END()
