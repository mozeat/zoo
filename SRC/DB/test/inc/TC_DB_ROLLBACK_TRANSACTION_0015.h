/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : DB
 * File Name      : DB_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-05    Generator      created
*************************************************************/
extern "C"
{
    #include <DB4A_if.h>
    #include <DB4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_DB_ROLLBACK_TRANSACTION_0015)

/**
 *@brief 
 *@param session_id
**/
    BOOST_AUTO_TEST_CASE( TC_DB_ROLLBACK_TRANSACTION_0015_001 )
    {
        const ZOO_CHAR table_name[256] = "test";
        DB4A_begin_transaction(TC_DB_GOLBAL_FIXTURE::m_session_id);
        DB4A_delete_data_by_condition(TC_DB_GOLBAL_FIXTURE::m_session_id,
                        table_name,
                        "id = 7");
        BOOST_TEST(DB4A_rollback_transaction(TC_DB_GOLBAL_FIXTURE::m_session_id) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()
