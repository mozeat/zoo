/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : DB
 * File Name      : DB_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-05    Generator      created
*************************************************************/
extern "C"
{
    #include <DB4A_if.h>
    #include <DB4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_DB_QUERY_ALL_0011)

/**
 *@brief 
 *@param session_id
 *@param table_name
**/
    BOOST_AUTO_TEST_CASE( TC_DB_QUERY_ALL_0011_001 )
    {
        const ZOO_CHAR table_name[256] = "test";
        BOOST_TEST(DB4A_query_all(TC_DB_GOLBAL_FIXTURE::m_session_id,table_name) == OK);

        DB4A_RECORD_STRUCT record;
        memset(&record, 0x00, sizeof(DB4A_RECORD_STRUCT));

        ZOO_INT32 row_size = 0;
        DB4A_get_row_size(TC_DB_GOLBAL_FIXTURE::m_session_id, &row_size);
        BOOST_TEST_MESSAGE("row:" << row_size);
        for (ZOO_INT32 i = 0; i < row_size; ++i)
        {
            DB4A_get_data(TC_DB_GOLBAL_FIXTURE::m_session_id, &record);
             BOOST_TEST_MESSAGE("col:" << record.fields_number);
            for (ZOO_INT32 i = 0; i < record.fields_number; ++i)
            {
                BOOST_TEST_MESSAGE("name:" << record.field_list[i].field_name);
                BOOST_TEST_MESSAGE("value:" << record.field_list[i].field_data);
            }
        }
    }

BOOST_AUTO_TEST_SUITE_END()
