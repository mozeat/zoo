/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : DB
 * File Name      : DB_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-05    Generator      created
*************************************************************/
#define BOOST_TEST_MODULE DB_test_module 
#define BOOST_TEST_DYN_LINK 
#include <boost/test/unit_test.hpp>
#include <boost/test/unit_test_log.hpp>
#include <boost/test/unit_test_suite.hpp>
#include <boost/test/framework.hpp>
#include <boost/test/unit_test_parameters.hpp>
#include <boost/test/utils/nullstream.hpp>
/**
 * @brief Execute <DB> UTMF ...
 * @brief <Global Fixture> header file can define the global variable.
 * @brief modify the include header files sequence to change the unit test execution sequence but not case SHUTDOWN.
 */
#include <TC_DB_GLOBAL_FIXTRUE.h>
#include <TC_DB_CONNECT_001.h>
#include <TC_DB_GET_CONNECTION_STATE_003.h>
#include <TC_DB_CREATE_TABLE_004.h>
#include <TC_DB_INSERT_DATA_009.h>
#include <TC_DB_QUERY_ALL_0011.h>
#include <TC_DB_QUERY_BY_CONDITION_0012.h>
#include <TC_DB_UPDATE_DATA_006.h>
#include <TC_DB_DELETE_DATA_BY_CONDITION_007.h>
#include <TC_DB_BEGIN_TRANSACTION_0013.h>
#include <TC_DB_COMMIT_TRANSACTION_0014.h>
#include <TC_DB_ROLLBACK_TRANSACTION_0015.h>
#include <TC_DB_DELETE_ALL_DATA_008.h>
#include <TC_DB_DROP_TABLE_005.h>
#include <TC_DB_DISCONNECT_002.h>

/**
 * @brief This case should be executed at the end of all test cases to exit current process.
 * @brief Close all this process events subscribe.
 * @brief Close message queue context thread.
 */
#include <TC_DB_SHUTDOWN.h>
