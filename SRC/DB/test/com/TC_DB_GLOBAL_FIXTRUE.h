/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : DB
 * File Name      : DB_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-05    Generator      created
*************************************************************/
extern "C"
{
    #include <DB4A_if.h>
    #include <DB4A_type.h>
    #include <MM4A_if.h>
}
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/output_test_stream.hpp>
#include <boost/test/results_reporter.hpp>
#include <boost/test/unit_test_log.hpp>
#include <string>
#include <stdlib.h>

/**
* @brief Define a test suite entry/exit,so that the setup function is called only once
* upon entering the test suite.
*/
struct TC_DB_GOLBAL_FIXTURE
{
    TC_DB_GOLBAL_FIXTURE()
    {
        BOOST_TEST_MESSAGE("TC global fixture initialize ...");
        MM4A_initialize();

    }

    ~TC_DB_GOLBAL_FIXTURE()
    {
        BOOST_TEST_MESSAGE("TC teardown ...");
        MM4A_terminate();
    }

    static ZOO_HANDLE m_session_id;
};


ZOO_HANDLE TC_DB_GOLBAL_FIXTURE::m_session_id = 0;


BOOST_TEST_GLOBAL_FIXTURE(TC_DB_GOLBAL_FIXTURE);


/**
 * @brief Define test report output formate, default --log_level=message.
*/
struct TC_DB_REPORTER
{
    TC_DB_REPORTER():reporter("../../../reporter/DB_TC_result.reporter")
    {
        boost::unit_test::unit_test_log.set_stream( reporter );
        boost::unit_test::unit_test_log.set_threshold_level(boost::unit_test::log_test_units);
    }

    ~TC_DB_REPORTER()
    {
        boost::unit_test::unit_test_log.set_stream( std::cout );
    }
   std::ofstream reporter;
};

BOOST_TEST_GLOBAL_CONFIGURATION(TC_DB_REPORTER);

