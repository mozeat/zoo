/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : DB4I.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-01    Generator      created
*************************************************************/
#include "DB4I_if.h"
/*
@brief Get request message length[bytes]
*@param function_code   function id
*@param *message_length  message length 
*@precondition:
*@postcondition:
*/
ZOO_INT32 DB4I_get_request_message_length(IN ZOO_INT32 function_code,
													INOUT ZOO_INT32 *message_length )
{    ZOO_INT32 result = OK;
    /* Check input parameter */
    if ( NULL == message_length )
    {
        result = DB4A_PARAMETER_ERR;        
    }
    else
    {
        *message_length = 0;
    }
    /*Check result */
    if ( OK == result )
    {
        switch( function_code )
        {
        case DB4A_CONNECT_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_CONNECT_CODE_REQ_STRUCT);
            break;
        case DB4A_DISCONNECT_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_DISCONNECT_CODE_REQ_STRUCT);
            break;
        case DB4A_GET_CONNECTION_STATE_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_GET_CONNECTION_STATE_CODE_REQ_STRUCT);
            break;
        case DB4A_CREATE_TABLE_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_CREATE_TABLE_CODE_REQ_STRUCT);
            break;
        case DB4A_DROP_TABLE_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_DROP_TABLE_CODE_REQ_STRUCT);
            break;
        case DB4A_UPDATE_DATA_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_UPDATE_DATA_CODE_REQ_STRUCT);
            break;
        case DB4A_DELETE_DATA_BY_CONDITION_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_DELETE_DATA_BY_CONDITION_CODE_REQ_STRUCT);
            break;
        case DB4A_DELETE_ALL_DATA_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_DELETE_ALL_DATA_CODE_REQ_STRUCT);
            break;
        case DB4A_INSERT_DATA_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_INSERT_DATA_CODE_REQ_STRUCT);
            break;
        case DB4A_GET_DATA_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_GET_DATA_CODE_REQ_STRUCT);
            break;
        case DB4A_GET_ROW_SIZE_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_GET_ROW_SIZE_CODE_REQ_STRUCT);
            break;
        case DB4A_QUERY_ALL_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_QUERY_ALL_CODE_REQ_STRUCT);
            break;
        case DB4A_QUERY_BY_CONDITION_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_QUERY_BY_CONDITION_CODE_REQ_STRUCT);
            break;
        case DB4A_BEGIN_TRANSACTION_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_BEGIN_TRANSACTION_CODE_REQ_STRUCT);
            break;
        case DB4A_COMMIT_TRANSACTION_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_COMMIT_TRANSACTION_CODE_REQ_STRUCT);
            break;
        case DB4A_ROLLBACK_TRANSACTION_CODE:
            *message_length = sizeof(DB4I_REQUEST_HEADER_STRUCT)+sizeof(DB4I_ROLLBACK_TRANSACTION_CODE_REQ_STRUCT);
            break;
        default:
               result = DB4A_PARAMETER_ERR;               
               break;
        }
    }
    return result;
}


/*
@brief Get reply message length[bytes]
*@param function_code    function id
*@param *message_length  message length 
*@precondition:
*@postcondition:
*/
ZOO_INT32 DB4I_get_reply_message_length(IN ZOO_INT32 function_code,
													INOUT ZOO_INT32 *message_length )
{    ZOO_INT32 result = OK;
    /*  Check input parameter */
    if ( NULL == message_length )
    {
        result = DB4A_PARAMETER_ERR;        
    }
    else
    {
        *message_length = 0;
    }
    /*Check result */
    if ( OK == result )
    {
        switch( function_code )
        {
        case DB4A_CONNECT_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_CONNECT_CODE_REP_STRUCT);
            break;
        case DB4A_DISCONNECT_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_DISCONNECT_CODE_REP_STRUCT);
            break;
        case DB4A_GET_CONNECTION_STATE_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_GET_CONNECTION_STATE_CODE_REP_STRUCT);
            break;
        case DB4A_CREATE_TABLE_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_CREATE_TABLE_CODE_REP_STRUCT);
            break;
        case DB4A_DROP_TABLE_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_DROP_TABLE_CODE_REP_STRUCT);
            break;
        case DB4A_UPDATE_DATA_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_UPDATE_DATA_CODE_REP_STRUCT);
            break;
        case DB4A_DELETE_DATA_BY_CONDITION_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_DELETE_DATA_BY_CONDITION_CODE_REP_STRUCT);
            break;
        case DB4A_DELETE_ALL_DATA_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_DELETE_ALL_DATA_CODE_REP_STRUCT);
            break;
        case DB4A_INSERT_DATA_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_INSERT_DATA_CODE_REP_STRUCT);
            break;
        case DB4A_GET_DATA_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_GET_DATA_CODE_REP_STRUCT);
            break;
        case DB4A_GET_ROW_SIZE_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_GET_ROW_SIZE_CODE_REP_STRUCT);
            break;
        case DB4A_QUERY_ALL_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_QUERY_ALL_CODE_REP_STRUCT);
            break;
        case DB4A_QUERY_BY_CONDITION_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_QUERY_BY_CONDITION_CODE_REP_STRUCT);
            break;
        case DB4A_BEGIN_TRANSACTION_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_BEGIN_TRANSACTION_CODE_REP_STRUCT);
            break;
        case DB4A_COMMIT_TRANSACTION_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_COMMIT_TRANSACTION_CODE_REP_STRUCT);
            break;
        case DB4A_ROLLBACK_TRANSACTION_CODE:
            *message_length = sizeof(DB4I_REPLY_HEADER_STRUCT)+sizeof(DB4I_ROLLBACK_TRANSACTION_CODE_REP_STRUCT);
            break;
        default:
               result = DB4A_PARAMETER_ERR;               
               break;
        }
    }
    return result;
}


/*
*@brief Send message to server and wait reply
*@param MQ4A_SERV_ADDR   server address
*@param *request_message 
*@param *reply_message   
*@param timeout          milliseconds
*@precondition:
*@postcondition: 
*/
ZOO_INT32 DB4I_send_request_and_reply(IN const MQ4A_SERV_ADDR server,
													IN DB4I_REQUEST_STRUCT  *request_message,
													INOUT DB4I_REPLY_STRUCT *reply_message,
													IN ZOO_INT32 timeout)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 actual_reply_length = 0;

    if(request_message == NULL)
    {
        result = DB4A_PARAMETER_ERR;
    }

    if(result == OK)
    {
        result = DB4I_get_request_message_length(request_message->request_header.function_code, &request_length);
    }

    if(result == OK)
    {
        result = DB4I_get_reply_message_length(reply_message->reply_header.function_code, &reply_length);
    }

    if(result == OK)
    {
        result = MQ4A_send_request_and_receive_reply(server,
												            request_message,
												            request_length,
												            reply_message,
												            reply_length,
												            &actual_reply_length,
												                DB4I_RETRY_INTERVAL,
												            timeout);
    }

 	return result;
}

/*
*@brief 4I_receive_reply_message
*@param MQ4A_SERV_ADDR   
*@param *request_message  
*@precondition:
*@postcondition:
*/
ZOO_INT32 DB4I_send_request_message(IN const MQ4A_SERV_ADDR server,
													IN DB4I_REQUEST_STRUCT *request_message)

{
    ZOO_INT32 result = OK;
    ZOO_INT32 request_length = 0;
    /*Get message length*/
    if ( OK == result )
    {
        result = DB4I_get_request_message_length(request_message->request_header.function_code, &request_length );

    }

    /*Send message to server*/
    if ( OK == result )
    {
        result = MQ4A_send_request( server,				 /*address*/
                                    request_message,					 /*message*/
                                    request_length,						 /*length*/
                                    DB4I_RETRY_INTERVAL );           /*retry  times*/
    }

    return result;
}

/*
*@brief reply message from server
	*@param server         
*@param function_code  
*@param *reply_message 
*@param timeout        
*@description:         4I_send_request_message
*@precondition:
*@postcondition:
*/
ZOO_INT32 DB4I_receive_reply_message(IN const MQ4A_SERV_ADDR server,
													IN ZOO_INT32 function_code,
													INOUT DB4I_REPLY_STRUCT *reply_message,
													IN ZOO_INT32 timeout)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 actual_replay_length = 0;    /*the actual reply message length*/
    ZOO_INT32 reply_length = 0; /*expect reply message length*/
    result = DB4I_get_reply_message_length( function_code, &reply_length );
    /*Get message*/
    while ( OK == result )
    {
        result = MQ4A_receive_reply( server, 
                  					reply_message,
                       				reply_length,
										&actual_replay_length,
								        timeout );
        if (OK == result && function_code == reply_message->reply_header.function_code)
        {
            break;
        }
     }

    return result;
}

/*
*@brief server send reply to clientDB4I_send_reply_message
*@param MQ4A_SERV_ADDR   
*@param *request_message  
*@precondition:
*@postcondition:
*/
ZOO_INT32 DB4I_send_reply_message(IN const MQ4A_SERV_ADDR server,
								IN ZOO_INT32 msg_id,
                              IN DB4I_REPLY_STRUCT *reply_message)

{
    ZOO_INT32 result = OK;
    ZOO_INT32 reply_length = 0;
    /*Get message length*/
    if ( OK == result )
    {
        result = DB4I_get_reply_message_length( reply_message->reply_header.function_code, &reply_length );
    }

    /**/
    if ( OK == result )
    {
        result = MQ4A_send_reply( server,				 /*address*/
                                    msg_id,				 /*id */
                                    reply_message,		 /*message*/
                                    reply_length );       /*length*/
    }

    return result;
}

/*
*@brief 
*@param event_id       
*@param *reply_message 
*@description:         
*@precondition:
*@postcondition:
*/
ZOO_INT32 DB4I_publish_event(IN const MQ4A_SERV_ADDR server,
													IN ZOO_INT32 event_id,
													INOUT DB4I_REPLY_STRUCT *reply_message)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 reply_length = 0;
    result = DB4I_get_reply_message_length(reply_message->reply_header.function_code, &reply_length);
    if (OK == result)
    {
         result = MQ4A_publish( server,
								    event_id,
 								reply_message,
								    reply_length );
    }

    return result;
}

/*
*@brief send subscribe message
*@param server            
*@param callback_function 
*@param callback_struct   
*@param event_id          
*@param *handle           
*@param *context          
 *@precondition:
*@postcondition:
*/
ZOO_INT32 DB4I_send_subscribe(IN const MQ4A_SERV_ADDR server,
													IN MQ4A_EVENT_CALLBACK_FUNCTION callback_function,
													IN MQ4A_CALLBACK_STRUCT *callback_struct,
													IN ZOO_INT32 event_id,
													INOUT ZOO_HANDLE *handle,
													INOUT void *context)
{
    ZOO_INT32 result = OK;
    if(OK == result)
    {
        result = MQ4A_subscribe( server,
										    callback_function,
											callback_struct,
											event_id,
											handle,
											context);
    }
    return result;
}

/*
*@brief Cancel subscribe 
*@param server   
*@param event_id 
*@param handle    
*@precondition:
*@postcondition:
*/
ZOO_INT32 DB4I_send_unsubscribe(IN const MQ4A_SERV_ADDR server,
													IN ZOO_INT32 event_id,
													IN ZOO_HANDLE handle)
{
    ZOO_INT32 result = OK;
    result = MQ4A_unsubscribe( server,
									 event_id,
								   	 handle );
    return result;
}

