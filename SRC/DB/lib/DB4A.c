/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : DB4A.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-01    Generator      created
*************************************************************/

#include <ZOO.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include "DB4I_type.h"
#include "DB4A_type.h"
#include "DB4I_if.h"


/**
 *@brief DB4A_connect
 *@param option
 *@param session_id
**/
ZOO_INT32 DB4A_connect(INOUT ZOO_HANDLE *session_id, IN const DB4A_CONNECT_OPTION_STRUCT* option)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_CONNECT_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.connect_req_msg.option),option,sizeof(DB4A_CONNECT_OPTION_STRUCT));
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(session_id,&reply_message->reply_body.connect_rep_msg.session_id,sizeof(ZOO_HANDLE));
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_disconnect
 *@param session_id
**/
ZOO_INT32 DB4A_disconnect(IN ZOO_HANDLE session_id)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_DISCONNECT_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.disconnect_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_get_connection_state
 *@param session_id
 *@param state
**/
ZOO_INT32 DB4A_get_connection_state(IN ZOO_HANDLE session_id, OUT ZOO_BOOL *state)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_GET_CONNECTION_STATE_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.get_connection_state_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(state,&reply_message->reply_body.get_connection_state_rep_msg.state,sizeof(ZOO_BOOL));
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_create_table
 *@param session_id
 *@param table_name
 *@param primary_key_name
 *@param fields
 *@param fields_number
**/
ZOO_INT32 DB4A_create_table(IN ZOO_HANDLE session_id,
										  IN const ZOO_CHAR* table_name,
										  IN const ZOO_CHAR* primary_key_name,
										  IN const DB4A_FIELD_TYPE_STRUCT* fields,
										  IN ZOO_INT32 fields_number)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_CREATE_TABLE_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.create_table_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
        memcpy((void *)(&request_message->request_body.create_table_req_msg.table_name),table_name,sizeof(ZOO_CHAR) * DB4I_BUFFER_LENGTH);
        memcpy((void *)(&request_message->request_body.create_table_req_msg.primary_key_name),primary_key_name,sizeof(ZOO_CHAR) * DB4I_BUFFER_LENGTH);
        memcpy((void *)(request_message->request_body.create_table_req_msg.fields),fields,sizeof(DB4A_FIELD_TYPE_STRUCT)*fields_number);
        memcpy((void *)(&request_message->request_body.create_table_req_msg.fields_number),&fields_number,sizeof(ZOO_INT32));
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_drop_table
 *@param session_id
 *@param table_name
**/
ZOO_INT32 DB4A_drop_table(IN ZOO_HANDLE session_id, IN const ZOO_CHAR* table_name)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_DROP_TABLE_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.drop_table_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
        memcpy((void *)(&request_message->request_body.drop_table_req_msg.table_name),table_name,sizeof(ZOO_CHAR) * DB4I_BUFFER_LENGTH);
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_update_data
 *@param session_id
 *@param table_name
 *@param condition
 *@param new_datas
 *@param new_datas_number
**/
ZOO_INT32 DB4A_update_data(IN ZOO_HANDLE session_id,
										IN const ZOO_CHAR* table_name,
										IN const ZOO_CHAR* condition,
										IN const DB4A_FIELD_DATA_STRUCT* new_datas,
										IN ZOO_INT32 new_datas_number)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_UPDATE_DATA_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.update_data_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
        memcpy((void *)(&request_message->request_body.update_data_req_msg.table_name),table_name,sizeof(ZOO_CHAR) * DB4I_BUFFER_LENGTH);
        memcpy((void *)(&request_message->request_body.update_data_req_msg.condition),condition,sizeof(ZOO_CHAR) * DB4I_BUFFER_LENGTH);
        memcpy((void *)(request_message->request_body.update_data_req_msg.new_datas),new_datas,sizeof(DB4A_FIELD_DATA_STRUCT)*new_datas_number);
        memcpy((void *)(&request_message->request_body.update_data_req_msg.new_datas_number),&new_datas_number,sizeof(ZOO_INT32));
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_delete_data_by_condition
 *@param session_id
 *@param table_name
 *@param condition
**/
ZOO_INT32 DB4A_delete_data_by_condition(IN ZOO_HANDLE session_id,
								 IN const ZOO_CHAR* table_name,
								 IN const ZOO_CHAR* condition)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_DELETE_DATA_BY_CONDITION_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.delete_data_by_condition_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
        memcpy((void *)(&request_message->request_body.delete_data_by_condition_req_msg.table_name),table_name,sizeof(ZOO_CHAR) * DB4I_BUFFER_LENGTH);
        memcpy((void *)(&request_message->request_body.delete_data_by_condition_req_msg.condition),condition,sizeof(ZOO_CHAR) * DB4I_BUFFER_LENGTH);
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_delete_all_data
 *@param session_id
 *@param table_name
**/
ZOO_INT32 DB4A_delete_all_data(IN ZOO_HANDLE session_id,
                                 IN const ZOO_CHAR* table_name)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_DELETE_ALL_DATA_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.delete_all_data_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
        memcpy((void *)(&request_message->request_body.delete_all_data_req_msg.table_name),table_name,sizeof(ZOO_CHAR) * DB4I_BUFFER_LENGTH);
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_insert_data
 *@param session_id
 *@param table_name
 *@param new_data
 *@param fields_number
**/
ZOO_INT32 DB4A_insert_data(IN ZOO_HANDLE session_id,
								 IN const ZOO_CHAR* table_name,
								 IN const DB4A_FIELD_DATA_STRUCT* new_data,
								 IN ZOO_INT32 fields_number)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_INSERT_DATA_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.insert_data_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
        memcpy((void *)(&request_message->request_body.insert_data_req_msg.table_name),table_name,sizeof(ZOO_CHAR) * DB4I_BUFFER_LENGTH);
        memcpy((void *)(request_message->request_body.insert_data_req_msg.new_data),new_data,sizeof(DB4A_FIELD_DATA_STRUCT)*fields_number);
        memcpy((void *)(&request_message->request_body.insert_data_req_msg.fields_number),&fields_number,sizeof(ZOO_INT32));
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_get_data
 *@param session_id
 *@param record
**/
ZOO_INT32 DB4A_get_data(IN ZOO_HANDLE session_id, INOUT DB4A_RECORD_STRUCT *record)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL;
    DB4I_REPLY_STRUCT *reply_message = NULL;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_GET_DATA_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.get_data_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        record->fields_number = reply_message->reply_body.get_data_rep_msg.record.fields_number;
        memcpy(record->field_list,reply_message->reply_body.get_data_rep_msg.record.field_list,sizeof(DB4A_FIELD_DATA_STRUCT)*record->fields_number);
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_get_row_size
 *@param session_id
 *@param row_size
**/
ZOO_INT32 DB4A_get_row_size(IN ZOO_HANDLE session_id, INOUT ZOO_INT32 *row_size)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL;
    DB4I_REPLY_STRUCT *reply_message = NULL;
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_GET_ROW_SIZE_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.get_row_size_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(row_size,&reply_message->reply_body.get_row_size_rep_msg.row_size,sizeof(ZOO_UINT32));
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_query_all
 *@param session_id
 *@param table_name
**/
ZOO_INT32 DB4A_query_all(IN ZOO_HANDLE session_id, IN const char *table_name)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_QUERY_ALL_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.query_all_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
        memcpy((void *)(&request_message->request_body.query_all_req_msg.table_name),table_name,sizeof(char) * DB4I_BUFFER_LENGTH);
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_query_by_condition
 *@param session_id
 *@param table_name
 *@param fields
 *@param fields_number
 *@param condition
**/
ZOO_INT32 DB4A_query_by_condition(IN ZOO_HANDLE session_id,
									   IN const ZOO_CHAR * table_name,
									   IN const DB4A_FIELD_DATA_STRUCT* fields,
									   IN ZOO_INT32 fields_number,
									   IN ZOO_CHAR * condition)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_QUERY_BY_CONDITION_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.query_by_condition_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
        memcpy((void *)(&request_message->request_body.query_by_condition_req_msg.table_name),table_name,sizeof(ZOO_CHAR) * DB4I_BUFFER_LENGTH);
        memcpy((void *)(request_message->request_body.query_by_condition_req_msg.fields),fields,sizeof(DB4A_FIELD_DATA_STRUCT)*fields_number);
        memcpy((void *)(&request_message->request_body.query_by_condition_req_msg.fields_number),&fields_number,sizeof(ZOO_INT32));
        memcpy((void *)(&request_message->request_body.query_by_condition_req_msg.condition),condition,sizeof(ZOO_CHAR) * DB4I_BUFFER_LENGTH);
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_begin_transaction
 *@param session_id
**/
ZOO_INT32 DB4A_begin_transaction(IN ZOO_HANDLE session_id)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_BEGIN_TRANSACTION_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.begin_transaction_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_commit_transaction
 *@param session_id
**/
ZOO_INT32 DB4A_commit_transaction(IN ZOO_HANDLE session_id)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_COMMIT_TRANSACTION_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.commit_transaction_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief DB4A_rollback_transaction
 *@param session_id
**/
ZOO_INT32 DB4A_rollback_transaction(IN ZOO_HANDLE session_id)
{
    ZOO_INT32 result = OK;
    DB4I_REQUEST_STRUCT *request_message = NULL; 
    DB4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = DB4A_ROLLBACK_TRANSACTION_CODE;
    result = DB4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (DB4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = DB4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (DB4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = DB4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.rollback_transaction_req_msg.session_id),&session_id,sizeof(ZOO_HANDLE));
    }

    if(OK == result)
    {
        result = DB4I_send_request_and_reply(DB4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}
