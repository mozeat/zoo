/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : DB
 * File Name    : DB4A_if.h
 * Description  : Database Access Object
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-05-07      MOZEAT       Create
 ***************************************************************/
#ifndef DB4A_IF_H_
#define DB4A_IF_H_

#include <ZOO.h>
#include <DB4A_type.h>
/**
 * @brief Connect to database
 * @param option:         connect database parameters
 * @param session_id:      session id
 * @return  -OK
		    -DB4A_SYSTEM_ERR
			-DB4A_TIMEOUT_ERR
			-DB4A_PARAMETER_ERR
			-DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_connect(INOUT ZOO_HANDLE *session_id, IN const DB4A_CONNECT_OPTION_STRUCT* option);


/**
 * @brief Close connection
 * @param session_id:           the unique identity
 * @return  -OK
		    -DB4A_SYSTEM_ERR
			-DB4A_TIMEOUT_ERR
			-DB4A_PARAMETER_ERR
			-DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_disconnect(IN ZOO_HANDLE session_id);


/**
 * @brief Get connection state
 * @param session_id:           the unique identity
 * @param state:        true: connected ,false: disconnected
 * @return  -OK
		    -DB4A_SYSTEM_ERR
			-DB4A_TIMEOUT_ERR
			-DB4A_PARAMETER_ERR
			-DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_get_connection_state(IN ZOO_HANDLE session_id, OUT ZOO_BOOL *state);

/**
 * @brief Create table
 * @param session_id: the unique identity
 * @param table name
 * @param primary_key_name
 * @param fields
 * @param fields_num
 * @return  -OK
		    -DB4A_SYSTEM_ERR
			-DB4A_TIMEOUT_ERR
			-DB4A_PARAMETER_ERR
			-DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_create_table(IN ZOO_HANDLE session_id,
										  IN const ZOO_CHAR* table_name,
										  IN const ZOO_CHAR* primary_key_name,
										  IN const DB4A_FIELD_TYPE_STRUCT* fields,
										  IN ZOO_INT32 fields_number);


/**
 * @brief Drop table
 * @param session_id: the unique identity
 * @param table name
 * @return  -OK
		    -DB4A_SYSTEM_ERR
			-DB4A_TIMEOUT_ERR
			-DB4A_PARAMETER_ERR
			-DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_drop_table(IN ZOO_HANDLE session_id, IN const ZOO_CHAR* table_name);

/**
 * @brief Update data to table
 * @param session_id: the unique identity
 * @return  -OK
		    -DB4A_SYSTEM_ERR
			-DB4A_TIMEOUT_ERR
			-DB4A_PARAMETER_ERR
			-DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_update_data(IN ZOO_HANDLE session_id,
										IN const ZOO_CHAR* table_name,
										IN const ZOO_CHAR* condition,
										IN const DB4A_FIELD_DATA_STRUCT* new_datas,
										IN ZOO_INT32 new_datas_number);

/**
 * @brief Delete data from table
 * @param session_id: the unique identity
 * @return  -OK
		    -DB4A_SYSTEM_ERR
			-DB4A_TIMEOUT_ERR
			-DB4A_PARAMETER_ERR
			-DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_delete_data_by_condition(IN ZOO_HANDLE session_id,
								 IN const ZOO_CHAR* table_name,
								 IN const ZOO_CHAR* condition);

/**
 *@brief Delete data from table
 * @param session_id: the unique identity
 *  * @return  -OK
            -DB4A_SYSTEM_ERR
            -DB4A_TIMEOUT_ERR
            -DB4A_PARAMETER_ERR
            -DB4A_DISCONNECT_ERR
 */
ZOO_EXPORT ZOO_INT32 DB4A_delete_all_data(IN ZOO_HANDLE session_id,
                                 IN const ZOO_CHAR* table_name);


/**
 * @brief Insert data to table, the database max columns is 20
 * @param session_id: the unique identity
 * @return  -OK
		    -DB4A_SYSTEM_ERR
			-DB4A_TIMEOUT_ERR
			-DB4A_PARAMETER_ERR
			-DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_insert_data(IN ZOO_HANDLE session_id,
								 IN const ZOO_CHAR* table_name,
								 IN const DB4A_FIELD_DATA_STRUCT* new_data,
								 IN ZOO_INT32 fields_number);

/**
 * @brief Get data from table after execute sql
 * @param session_id: the unique identity
 * @param record: a row record
 * @return  -OK
            -DB4A_SYSTEM_ERR
            -DB4A_TIMEOUT_ERR
            -DB4A_PARAMETER_ERR
            -DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_get_data(IN ZOO_HANDLE session_id, INOUT DB4A_RECORD_STRUCT *record);

/**
 * @brief Get query row count
 * @param session_id:           the unique identity
 * @param row_size:        row count
 * @return  -OK
            -DB4A_SYSTEM_ERR
            -DB4A_TIMEOUT_ERR
            -DB4A_PARAMETER_ERR
            -DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_get_row_size(IN ZOO_HANDLE session_id, INOUT ZOO_INT32 *row_size);


/**
 * @brief qurey the table_name all data 
 * @param session_id: the unique identity
 * @return  -OK
		    -DB4A_SYSTEM_ERR
			-DB4A_TIMEOUT_ERR
			-DB4A_PARAMETER_ERR
			-DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_query_all(IN ZOO_HANDLE session_id, IN const char *table_name);

/**
 * @brief query the table_name all data by condition
 * @param session_id: the unique identity
 * @return  -OK
		    -DB4A_SYSTEM_ERR
			-DB4A_TIMEOUT_ERR
			-DB4A_PARAMETER_ERR
			-DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_query_by_condition(IN ZOO_HANDLE session_id,
									   IN const ZOO_CHAR * table_name,
									   IN const DB4A_FIELD_DATA_STRUCT* fields,
									   IN ZOO_INT32 fields_number,
									   IN ZOO_CHAR * condition);

/**
 * @brief Start the transaction
 * @param session_id: the unique identity
 * @return  -OK
		    -DB4A_SYSTEM_ERR
			-DB4A_TIMEOUT_ERR
			-DB4A_PARAMETER_ERR
			-DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_begin_transaction(IN ZOO_HANDLE session_id);


/**
 * @brief Commit the transaction if there is no error in the transaction
 * @param session_id: the unique identity
 * @return  -OK
		    -DB4A_SYSTEM_ERR
			-DB4A_TIMEOUT_ERR
			-DB4A_PARAMETER_ERR
			-DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_commit_transaction(IN ZOO_HANDLE session_id);


/**
 * @brief Roll back the transaction if there is no error in the transaction
 * @param session_id: the unique identity
 * @return  -OK
		    -DB4A_SYSTEM_ERR
			-DB4A_TIMEOUT_ERR
			-DB4A_PARAMETER_ERR
			-DB4A_DISCONNECT_ERR
*/
ZOO_EXPORT ZOO_INT32 DB4A_rollback_transaction(IN ZOO_HANDLE session_id);

#endif

