/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : DB4I_type.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-01    Generator      created
*************************************************************/
#ifndef DB4I_TYPE_H
#define DB4I_TYPE_H
#include <MQ4A_type.h>
#include "DB4A_type.h"
#include "DB4A_if.h"


/**
 *@brief Macro Definitions
**/
#define DB4I_COMPONET_ID "DB"
#define DB4A_SERVER     "DB4A_SERVER"
#define DB4I_BUFFER_LENGTH    256
#define DB4I_RETRY_INTERVAL   3

/**
 *@brief Function Code Definitions
**/
#define DB4A_CONNECT_CODE 0x4442ff00
#define DB4A_DISCONNECT_CODE 0x4442ff01
#define DB4A_GET_CONNECTION_STATE_CODE 0x4442ff02
#define DB4A_CREATE_TABLE_CODE 0x4442ff03
#define DB4A_DROP_TABLE_CODE 0x4442ff04
#define DB4A_UPDATE_DATA_CODE 0x4442ff05
#define DB4A_DELETE_DATA_BY_CONDITION_CODE 0x4442ff06
#define DB4A_DELETE_ALL_DATA_CODE 0x4442ff07
#define DB4A_INSERT_DATA_CODE 0x4442ff08
#define DB4A_GET_DATA_CODE 0x4442ff09
#define DB4A_GET_ROW_SIZE_CODE 0x4442ff0a
#define DB4A_QUERY_ALL_CODE 0x4442ff0b
#define DB4A_QUERY_BY_CONDITION_CODE 0x4442ff0c
#define DB4A_BEGIN_TRANSACTION_CODE 0x4442ff0d
#define DB4A_COMMIT_TRANSACTION_CODE 0x4442ff0e
#define DB4A_ROLLBACK_TRANSACTION_CODE 0x4442ff0f

/*Request and reply header struct*/
typedef struct
{
    MQ4A_SERV_ADDR reply_addr;
    ZOO_UINT32 msg_id;
    ZOO_BOOL reply_wanted;
    ZOO_UINT32 func_id;
}DB4I_REPLY_HANDLER_STRUCT;

typedef  DB4I_REPLY_HANDLER_STRUCT * DB4I_REPLY_HANDLE;

/*Request message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL need_reply;
}DB4I_REQUEST_HEADER_STRUCT;

/*Reply message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL execute_result;
}DB4I_REPLY_HEADER_STRUCT;

/**
*@brief DB4I_CONNECT_CODE_REQ_STRUCT
**/
typedef struct 
{
     DB4A_CONNECT_OPTION_STRUCT option;
    ZOO_CHAR g_filler[8];
}DB4I_CONNECT_CODE_REQ_STRUCT;

/**
*@brief DB4I_DISCONNECT_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}DB4I_DISCONNECT_CODE_REQ_STRUCT;

/**
*@brief DB4I_GET_CONNECTION_STATE_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}DB4I_GET_CONNECTION_STATE_CODE_REQ_STRUCT;

/**
*@brief DB4I_CREATE_TABLE_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_INT32 fields_number;
    ZOO_CHAR table_name[DB4I_BUFFER_LENGTH];
    ZOO_CHAR primary_key_name[DB4I_BUFFER_LENGTH];
     DB4A_FIELD_TYPE_STRUCT fields[DB4A_MAX_COL];
    ZOO_CHAR g_filler[8];
}DB4I_CREATE_TABLE_CODE_REQ_STRUCT;

/**
*@brief DB4I_DROP_TABLE_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR table_name[DB4I_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}DB4I_DROP_TABLE_CODE_REQ_STRUCT;

/**
*@brief DB4I_UPDATE_DATA_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_INT32 new_datas_number;
    ZOO_CHAR table_name[DB4I_BUFFER_LENGTH];
    ZOO_CHAR condition[DB4I_BUFFER_LENGTH];
     DB4A_FIELD_DATA_STRUCT new_datas[DB4A_MAX_COL];
    ZOO_CHAR g_filler[8];
}DB4I_UPDATE_DATA_CODE_REQ_STRUCT;

/**
*@brief DB4I_DELETE_DATA_BY_CONDITION_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR table_name[DB4I_BUFFER_LENGTH];
    ZOO_CHAR condition[DB4I_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}DB4I_DELETE_DATA_BY_CONDITION_CODE_REQ_STRUCT;

/**
*@brief DB4I_DELETE_ALL_DATA_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR table_name[DB4I_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}DB4I_DELETE_ALL_DATA_CODE_REQ_STRUCT;

/**
*@brief DB4I_INSERT_DATA_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_INT32 fields_number;
    ZOO_CHAR table_name[DB4I_BUFFER_LENGTH];
     DB4A_FIELD_DATA_STRUCT new_data[DB4A_MAX_COL];
    ZOO_CHAR g_filler[8];
}DB4I_INSERT_DATA_CODE_REQ_STRUCT;

/**
*@brief DB4I_GET_DATA_CODE_REQ_STRUCT
**/
typedef struct
{
    ZOO_HANDLE session_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}DB4I_GET_DATA_CODE_REQ_STRUCT;

/**
*@brief DB4I_GET_ROW_SIZE_CODE_REQ_STRUCT
**/
typedef struct
{
    ZOO_HANDLE session_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}DB4I_GET_ROW_SIZE_CODE_REQ_STRUCT;

/**
*@brief DB4I_QUERY_ALL_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_CHAR c_filler[4];
    char table_name[DB4I_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}DB4I_QUERY_ALL_CODE_REQ_STRUCT;

/**
*@brief DB4I_QUERY_BY_CONDITION_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_INT32 fields_number;
    ZOO_CHAR table_name[DB4I_BUFFER_LENGTH];
    DB4A_FIELD_DATA_STRUCT fields[DB4A_MAX_COL];
    ZOO_CHAR condition[DB4I_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}DB4I_QUERY_BY_CONDITION_CODE_REQ_STRUCT;

/**
*@brief DB4I_BEGIN_TRANSACTION_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}DB4I_BEGIN_TRANSACTION_CODE_REQ_STRUCT;

/**
*@brief DB4I_COMMIT_TRANSACTION_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}DB4I_COMMIT_TRANSACTION_CODE_REQ_STRUCT;

/**
*@brief DB4I_ROLLBACK_TRANSACTION_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}DB4I_ROLLBACK_TRANSACTION_CODE_REQ_STRUCT;

/**
*@brief DB4I_CONNECT_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_HANDLE session_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}DB4I_CONNECT_CODE_REP_STRUCT;

/**
*@brief DB4I_DISCONNECT_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}DB4I_DISCONNECT_CODE_REP_STRUCT;

/**
*@brief DB4I_GET_CONNECTION_STATE_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_BOOL state;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}DB4I_GET_CONNECTION_STATE_CODE_REP_STRUCT;

/**
*@brief DB4I_CREATE_TABLE_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}DB4I_CREATE_TABLE_CODE_REP_STRUCT;

/**
*@brief DB4I_DROP_TABLE_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}DB4I_DROP_TABLE_CODE_REP_STRUCT;

/**
*@brief DB4I_UPDATE_DATA_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}DB4I_UPDATE_DATA_CODE_REP_STRUCT;

/**
*@brief DB4I_DELETE_DATA_BY_CONDITION_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}DB4I_DELETE_DATA_BY_CONDITION_CODE_REP_STRUCT;

/**
*@brief DB4I_DELETE_ALL_DATA_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}DB4I_DELETE_ALL_DATA_CODE_REP_STRUCT;

/**
*@brief DB4I_INSERT_DATA_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}DB4I_INSERT_DATA_CODE_REP_STRUCT;

/**
*@brief DB4I_GET_DATA_CODE_REP_STRUCT
**/
typedef struct
{
    DB4A_RECORD_STRUCT record;
    ZOO_CHAR g_filler[8];
}DB4I_GET_DATA_CODE_REP_STRUCT;

/**
*@brief DB4I_GET_ROW_SIZE_CODE_REP_STRUCT
**/
typedef struct
{
    ZOO_INT32 row_size;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}DB4I_GET_ROW_SIZE_CODE_REP_STRUCT;

/**
*@brief DB4I_QUERY_ALL_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}DB4I_QUERY_ALL_CODE_REP_STRUCT;

/**
*@brief DB4I_QUERY_BY_CONDITION_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}DB4I_QUERY_BY_CONDITION_CODE_REP_STRUCT;

/**
*@brief DB4I_BEGIN_TRANSACTION_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}DB4I_BEGIN_TRANSACTION_CODE_REP_STRUCT;

/**
*@brief DB4I_COMMIT_TRANSACTION_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}DB4I_COMMIT_TRANSACTION_CODE_REP_STRUCT;

/**
*@brief DB4I_ROLLBACK_TRANSACTION_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}DB4I_ROLLBACK_TRANSACTION_CODE_REP_STRUCT;


typedef struct
{
    DB4I_REQUEST_HEADER_STRUCT request_header;
    union
    {
        DB4I_CONNECT_CODE_REQ_STRUCT connect_req_msg;
        DB4I_DISCONNECT_CODE_REQ_STRUCT disconnect_req_msg;
        DB4I_GET_CONNECTION_STATE_CODE_REQ_STRUCT get_connection_state_req_msg;
        DB4I_CREATE_TABLE_CODE_REQ_STRUCT create_table_req_msg;
        DB4I_DROP_TABLE_CODE_REQ_STRUCT drop_table_req_msg;
        DB4I_UPDATE_DATA_CODE_REQ_STRUCT update_data_req_msg;
        DB4I_DELETE_DATA_BY_CONDITION_CODE_REQ_STRUCT delete_data_by_condition_req_msg;
        DB4I_DELETE_ALL_DATA_CODE_REQ_STRUCT delete_all_data_req_msg;
        DB4I_INSERT_DATA_CODE_REQ_STRUCT insert_data_req_msg;
        DB4I_GET_DATA_CODE_REQ_STRUCT get_data_req_msg;
        DB4I_GET_ROW_SIZE_CODE_REQ_STRUCT get_row_size_req_msg;
        DB4I_QUERY_ALL_CODE_REQ_STRUCT query_all_req_msg;
        DB4I_QUERY_BY_CONDITION_CODE_REQ_STRUCT query_by_condition_req_msg;
        DB4I_BEGIN_TRANSACTION_CODE_REQ_STRUCT begin_transaction_req_msg;
        DB4I_COMMIT_TRANSACTION_CODE_REQ_STRUCT commit_transaction_req_msg;
        DB4I_ROLLBACK_TRANSACTION_CODE_REQ_STRUCT rollback_transaction_req_msg;
     }request_body;
}DB4I_REQUEST_STRUCT;


typedef struct
{
    DB4I_REPLY_HEADER_STRUCT reply_header;
    union
    {
        DB4I_CONNECT_CODE_REP_STRUCT connect_rep_msg;
        DB4I_DISCONNECT_CODE_REP_STRUCT disconnect_rep_msg;
        DB4I_GET_CONNECTION_STATE_CODE_REP_STRUCT get_connection_state_rep_msg;
        DB4I_CREATE_TABLE_CODE_REP_STRUCT create_table_rep_msg;
        DB4I_DROP_TABLE_CODE_REP_STRUCT drop_table_rep_msg;
        DB4I_UPDATE_DATA_CODE_REP_STRUCT update_data_rep_msg;
        DB4I_DELETE_DATA_BY_CONDITION_CODE_REP_STRUCT delete_data_by_condition_rep_msg;
        DB4I_DELETE_ALL_DATA_CODE_REP_STRUCT delete_all_data_rep_msg;
        DB4I_INSERT_DATA_CODE_REP_STRUCT insert_data_rep_msg;
        DB4I_GET_DATA_CODE_REP_STRUCT get_data_rep_msg;
        DB4I_GET_ROW_SIZE_CODE_REP_STRUCT get_row_size_rep_msg;
        DB4I_QUERY_ALL_CODE_REP_STRUCT query_all_rep_msg;
        DB4I_QUERY_BY_CONDITION_CODE_REP_STRUCT query_by_condition_rep_msg;
        DB4I_BEGIN_TRANSACTION_CODE_REP_STRUCT begin_transaction_rep_msg;
        DB4I_COMMIT_TRANSACTION_CODE_REP_STRUCT commit_transaction_rep_msg;
        DB4I_ROLLBACK_TRANSACTION_CODE_REP_STRUCT rollback_transaction_rep_msg;
    }reply_body;
}DB4I_REPLY_STRUCT;


#endif //DB4I_type.h
