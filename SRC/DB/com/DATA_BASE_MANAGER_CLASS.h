
/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : DB
 * File Name    : databasemanagerclass.h
 * Description  : database manager class
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-10-30      chaoluo       Create
 ***************************************************************/

#ifndef DATA_BASE_MANAGER_CLASS_H
#define DATA_BASE_MANAGER_CLASS_H

extern "C"
{
	#include "DB4A_type.h"
}

#include <map>
#include <boost/smart_ptr.hpp>
#include "DATA_BASE_CLASS.h"

namespace ZOO_DB {

    class DATA_BASE_MANAGER_CLASS
    {
    public:
        ~DATA_BASE_MANAGER_CLASS();

        /**
         * @brief getInstance
         * @return  Single instance
         */
        static boost::shared_ptr<DATA_BASE_MANAGER_CLASS> instance();

        /**
         * @brief connect Database
         * @param option: database connect parameters
         * @param session_id: session id
         * @return 0 successful, -1 failed
         */
        ZOO_INT32 connect_database(ZOO_HANDLE *session_id, const DB4A_CONNECT_OPTION_STRUCT* option);

        /**
         * @brief close Database
         * @param session_id: session id
         * @return 0 successful, -1 failed
         */
        ZOO_INT32 disconnect_database(ZOO_HANDLE session_id);

        /**
         * @brief get connection state
         * @param session_id: session id
         * @param state: true=connected, flase=disconnected
         * @return 0 successful, -1 failed
         */
        ZOO_INT32 get_connect_state(ZOO_HANDLE session_id, ZOO_BOOL *state);

        /**
         * @brief create Table
         * @param session_id: session id
         * @param table_name: table name
         * @param primary_key_name: primary key name
         * @param fields: table fields
         * @param fields_number: the number of field
         * @return 0 successful, -1 failed
         */
        ZOO_INT32 create_table(ZOO_HANDLE session_id,
                        const char* table_name,
                        const char* primary_key_name,
                        const DB4A_FIELD_TYPE_STRUCT* fields,
                        int fields_number);

        /**
         * @brief drop Table
         * @param session_id: session id
         * @param name:table name
         * @return 0 successful, -1 failed
         */
        ZOO_INT32 drop_table(ZOO_HANDLE session_id, const char *name);

        /**
         * @brief insert Table
         * @param session_id: session id
         * @param table_name: table name
         * @param new_data: field values
         * @param fields_number: the number of field
         * @return
         */
        ZOO_INT32 insert_table(ZOO_HANDLE session_id,
                        const char* table_name,
                        const DB4A_FIELD_DATA_STRUCT* new_data,
                        int fields_number);

        /**
         * @brief remove Data
         * @param session_id: session id
         * @param table_name: table name
         * @param conditions: remove conditions
         * @param conditions_number: the number of condition
         * @return
         */
        ZOO_INT32 remove_data_by_condition(ZOO_HANDLE session_id,
                       const char* table_name,
                       const char* condition);

        /**
         * @brief remove table All Data
         * @param session_id:session id
         * @param table_name: table name
         * @return
         */
        ZOO_INT32 remove_all_data(ZOO_HANDLE session_id,
                          const char *table_name);

        /**
         * @brief update Data
         * @param session_id: session id
         * @param table_name: table name
         * @param conditions: update conditions
         * @param new_datas: update data value list
         * @param new_datas_number: the number of value
         * @return
         */
        ZOO_INT32 update_data(ZOO_HANDLE session_id,
                               const char* table_name,
                               const char* condition,
                               const DB4A_FIELD_DATA_STRUCT* new_datas,
                               int new_datas_number);

        /**
         * @brief get Data
         * @return Get the query result
         */
        ZOO_INT32 get_data(ZOO_HANDLE session_id, DB4A_RECORD_STRUCT *record);

        /**
         * @brief query tble-> select * from table;
         * @param session_id: session id
         * @param table_name: table name
         * @return 0 successful, -1 failed
         */
        ZOO_INT32 query_all(ZOO_HANDLE session_id, const char *table_name);

        /**
         * @brief Find data from table if condition matches
         * @param session_id: the unique identity
         * @return  -OK
                    -DB4A_SYSTEM_ERR
                    -DB4A_TIMEOUT_ERR
                    -DB4A_PARAMETER_ERR
                    -DB4A_DISCONNECT_ERR
        */
        ZOO_INT32 query_by_condition(IN ZOO_HANDLE session_id,
                               IN const ZOO_CHAR * table_name,
                               IN const DB4A_FIELD_DATA_STRUCT* fields,
                               IN ZOO_INT32 fields_number,
                               IN ZOO_CHAR * conditions);


        /**
         * @brief get_row_size
         * @param session_id: session id
         * @return record size
         */
        ZOO_INT32 get_row_size(ZOO_HANDLE session_id);

        /**
         * @brief Start the transaction
         * @param session_id: the unique identity
         * @return  0 successful, -1 failed
        */
        ZOO_INT32 begin_transaction(ZOO_HANDLE session_id);

        /**
         * @brief Commit the transaction if there is no error in the transaction
         * @param session_id: the unique identity
         * @return  0 successful, -1 failed
        */
        ZOO_INT32 commit_transaction(ZOO_HANDLE session_id);

        /**
         * @brief Roll back the transaction if there is no error in the transaction
         * @param session_id: the unique identity
         * @return  0 successful, -1 failed
        */
        ZOO_INT32 rollback_transaction(ZOO_HANDLE session_id);

    private:
        explicit DATA_BASE_MANAGER_CLASS();

        /**
         * @brief get_session_id
         * @return
         */
        ZOO_UINT32 get_session_id();

        void reset_session_id(ZOO_HANDLE session_id);

        boost::shared_ptr<DATA_BASE_CLASS> find_database(ZOO_HANDLE session_id);

    private:

        /* session id iterator */
        std::map<ZOO_UINT32, bool> m_session_ids;

        std::map<ZOO_HANDLE, boost::shared_ptr<DATA_BASE_CLASS>> m_databases;

        static boost::shared_ptr<DATA_BASE_MANAGER_CLASS> m_instance;

        ZOO_UINT32 m_id;

    };

} // end namespace


#endif // DATA_BASE_MANAGER_CLASS_H
