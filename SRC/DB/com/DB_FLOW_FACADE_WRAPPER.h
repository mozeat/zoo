#ifndef DB_FLOW_FACADE_WRAPPER_H
#define DB_FLOW_FACADE_WRAPPER_H

#include "DB4A_type.h"
#include "ZOO_type.h"

#ifdef __cplusplus
    extern "C" {
#endif


/**
 * @brief connect Database
 * @param option: connect parameter
 * @param session_id: session id
 * @return 0 successful, -1 failed
 */
ZOO_INT32 DB_connect(ZOO_HANDLE *session_id, const DB4A_CONNECT_OPTION_STRUCT *option);

/**
 * @brief close Database
 * @param session_id: session id
 * @return 0 successful, -1 failed
 */
ZOO_INT32 DB_disconnect(ZOO_HANDLE session_id);

/**
 * @brief get connection state
 * @param session_id: session id
 * @param state: true=connected, flase=disconnected
 * @return 0 successful, -1 failed
 */
ZOO_INT32 DB_get_connection_state(ZOO_HANDLE session_id, ZOO_BOOL *state);

/**
 * @brief create Table
 * @param session_id: session id
 * @param table_name: table name
 * @param primary_key_name: primary key name
 * @param fields: table fields
 * @param fields_number: the number of field
 * @return 0 successful, -1 failed
 */
ZOO_INT32 DB_create_table(ZOO_HANDLE session_id,
                const char* table_name,
                const char* primary_key_name,
                const DB4A_FIELD_TYPE_STRUCT* fields,
                int fields_number);

/**
 * @brief drop Table
 * @param session_id: session id
 * @param name:table name
 * @return 0 successful, -1 failed
 */
ZOO_INT32 DB_drop_table(ZOO_HANDLE session_id, const char *name);

/**
 * @brief insert Table
 * @param session_id: session id
 * @param table_name: table name
 * @param new_data: field values
 * @param fields_number: the number of field
 * @return
 */
ZOO_INT32 DB_insert_data(ZOO_HANDLE session_id,
                const char* table_name,
                const DB4A_FIELD_DATA_STRUCT* new_data,
                int fields_number);

/**
 * @brief get Data
 * @return
 */
ZOO_INT32 DB_get_data(ZOO_HANDLE session_id, DB4A_RECORD_STRUCT *record);

/**
 * @brief DB_get_row_size
 * @param session_id
 * @param row_size
 * @return
 */
ZOO_INT32 DB_get_row_size(ZOO_HANDLE session_id);

/**
 * @brief remove Data
 * @param session_id: session id
 * @param table_name: table name
 * @param conditions: remove conditions
 * @param conditions_number: the number of condition
 * @return
 */
ZOO_INT32 DB_delete_data_by_condition(ZOO_HANDLE session_id,
               const char* table_name,
               const char* condition);

/**
 * @brief remove table All Data
 * @param session_id:session id
 * @param table_name: table name
 * @return
 */
ZOO_INT32 DB_delete_all_data(ZOO_HANDLE session_id,
                  const char *table_name);

/**
 * @brief update Data
 * @param session_id: session id
 * @param table_name: table name
 * @param conditions: update conditions
 * @param conditions_number: the number of condition
 * @param new_datas: update data value list
 * @param new_datas_number: the number of value
 * @return
 */
ZOO_INT32 DB_update_data(ZOO_HANDLE session_id,
                               const char* table_name,
                               const char* condition,
                               const DB4A_FIELD_DATA_STRUCT* new_datas,
                               int new_datas_number);


/**
 * @brief query tble-> select * from table;
 * @param session_id: session id
 * @param table_name: table name
 * @return 0 successful, -1 failed
 */
ZOO_INT32 DB_query_all(ZOO_HANDLE session_id, const char *table_name);

/**
 * @brief Find data from table if condition matches
 * @param session_id: the unique identity
 * @return  -OK
            -DB4A_SYSTEM_ERR
            -DB4A_TIMEOUT_ERR
            -DB4A_PARAMETER_ERR
            -DB4A_DISCONNECT_ERR
*/
 ZOO_INT32 DB_query_by_condition(IN ZOO_HANDLE session_id,
                        IN const ZOO_CHAR * table_name,
                        IN const DB4A_FIELD_DATA_STRUCT* fields,
                        IN ZOO_INT32 fields_number,
                        IN ZOO_CHAR * conditions);

/**
 * @brief get Data
 * @param session_id: unique identity
 * @param record : row record
 * @return  -OK
            -DB4A_SYSTEM_ERR
            -DB4A_TIMEOUT_ERR
            -DB4A_PARAMETER_ERR
            -DB4A_DISCONNECT_ERR
*/
ZOO_INT32 DB_get_data(IN ZOO_HANDLE session_id, DB4A_RECORD_STRUCT *record);


/**
 * @brief Start the transaction
 * @param session_id: the unique identity
 * @return  0 successful, -1 failed
*/
ZOO_INT32 DB_begin_transaction(ZOO_HANDLE session_id);

/**
 * @brief Commit the transaction if there is no error in the transaction
 * @param session_id: the unique identity
 * @return  0 successful, -1 failed
*/
ZOO_INT32 DB_commit_transaction(ZOO_HANDLE session_id);

/**
 * @brief Roll back the transaction if there is no error in the transaction
 * @param session_id: the unique identity
 * @return  0 successful, -1 failed
*/
ZOO_INT32 DB_rollback_transaction(ZOO_HANDLE session_id);

#ifdef __cplusplus
    }
#endif


#endif // DB_FLOW_FACADE_WRAPPER_H

