/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : DBMA_event.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-01    Generator      created
*************************************************************/
#ifndef DBMA_EVENT_H
#define DBMA_EVENT_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include "DB4I_type.h"
#include "DB4I_if.h"
#include "DB4A_type.h"

/**
 *@brief DBMA_raise_4A_connect
 *@param option
 *@param session_id
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_connect(IN ZOO_INT32 error_code,
                                               IN ZOO_HANDLE session_id,
                                               IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_disconnect
 *@param session_id
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_disconnect(IN ZOO_INT32 error_code,
                                                  IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_get_connection_state
 *@param session_id
 *@param state
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_get_connection_state(IN ZOO_INT32 error_code,
                                                            IN ZOO_BOOL state,
                                                            IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_create_table
 *@param session_id
 *@param table_name
 *@param primary_key_name
 *@param fields
 *@param fields_number
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_create_table(IN ZOO_INT32 error_code,
                                                    IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_drop_table
 *@param session_id
 *@param table_name
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_drop_table(IN ZOO_INT32 error_code,
                                                  IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_update_data
 *@param session_id
 *@param table_name
 *@param condition
 *@param new_datas
 *@param new_datas_number
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_update_data(IN ZOO_INT32 error_code,
                                                   IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_delete_data_by_condition
 *@param session_id
 *@param table_name
 *@param condition
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_delete_data_by_condition(IN ZOO_INT32 error_code,
                                                                IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_delete_all_data
 *@param session_id
 *@param table_name
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_delete_all_data(IN ZOO_INT32 error_code,
                                                       IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_insert_data
 *@param session_id
 *@param table_name
 *@param new_data
 *@param fields_number
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_insert_data(IN ZOO_INT32 error_code,
                                                   IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_get_data
 *@param session_id
 *@param record
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_get_data(IN ZOO_INT32 error_code,
                                                IN DB4A_RECORD_STRUCT *record,
                                                IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_get_row_size
 *@param session_id
 *@param row_size
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_get_row_size(IN ZOO_INT32 error_code,
                                                    IN ZOO_INT32 row_size,
                                                    IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_query_all
 *@param session_id
 *@param table_name
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_query_all(IN ZOO_INT32 error_code,
                                                 IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_query_by_condition
 *@param session_id
 *@param table_name
 *@param fields
 *@param fields_number
 *@param condition
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_query_by_condition(IN ZOO_INT32 error_code,
                                                          IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_begin_transaction
 *@param session_id
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_begin_transaction(IN ZOO_INT32 error_code,
                                                         IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_commit_transaction
 *@param session_id
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_commit_transaction(IN ZOO_INT32 error_code,
                                                          IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_raise_4A_rollback_transaction
 *@param session_id
**/
ZOO_EXPORT ZOO_INT32 DBMA_raise_4A_rollback_transaction(IN ZOO_INT32 error_code,
                                                            IN DB4I_REPLY_HANDLE reply_handle);


#endif // DBMA_event.h
