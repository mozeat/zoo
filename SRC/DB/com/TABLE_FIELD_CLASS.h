
/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : DB
 * File Name    : tablefieldclass.h
 * Description  : database table field class
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-10-30      chaoluo       Create
 ***************************************************************/

#ifndef TABLE_FIELD_CLASS_H
#define TABLE_FIELD_CLASS_H

#include <string>

namespace ZOO_DB {

    class TABLE_FIELD_CLASS
    {
    public:
        enum FieldTypeEnum{
            Invalid = 0,
            Bool = 1,
            Int = 2,
            UInt = 3,
            LongLong = 4,
            ULongLong = 5,
            Double = 6,
            String = 7,
            Date = 8,
            Time = 9,
            DateTime = 10,
        };

        explicit TABLE_FIELD_CLASS();
        TABLE_FIELD_CLASS(std::string name, std::string value = "", FieldTypeEnum type = FieldTypeEnum::String);
        TABLE_FIELD_CLASS(const TABLE_FIELD_CLASS &other);

        TABLE_FIELD_CLASS &operator=(const TABLE_FIELD_CLASS &other);

        ~TABLE_FIELD_CLASS();

        std::string get_name() const;
        void set_name(const std::string &name);

        FieldTypeEnum get_type() const;
        void set_type(const FieldTypeEnum &type);

        std::string get_value() const;
        void set_value(const std::string &value);

        std::string get_name_len() const;
        void set_name_len(const std::string &name_len);

    private:
        std::string m_name;
        FieldTypeEnum m_type;
        std::string m_value;
        std::string m_name_len;
    };

}

#endif // TABLE_FIELD_CLASS_H
