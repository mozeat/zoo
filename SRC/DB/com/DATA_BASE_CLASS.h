﻿/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : DB
 * File Name    : DATA_BASE_CLASS.h
 * Description  : The DATA_BASE_CLASS class provides an interface for accessing a database through a connection.
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-10-30      chaoluo       Create
 ***************************************************************/

#ifndef DATA_BASE_CLASS_H
#define DATA_BASE_CLASS_H

extern "C"
{
	#include "DB4A_type.h"
}

#include "TABLE_FIELD_CLASS.h"

#include <string>
#include <map>
#include <list>
#include <boost/smart_ptr.hpp>
#include <soci/soci.h>

namespace ZOO_DB {

    typedef std::map<ZOO_INT32, std::list<boost::shared_ptr<TABLE_FIELD_CLASS>>> ZooDatabaseRecord;

    class DATA_BASE_CLASS
    {
    public:
        explicit DATA_BASE_CLASS(const std::string & backend_name, const std::string & connect_string);
        ~DATA_BASE_CLASS();

        ZOO_INT32 close();

        ZOO_INT32 get_connect_state();

        ZOO_INT32 create_table(const std::string &table_name,
                        const std::string &primary_key,
                        std::list<boost::shared_ptr<TABLE_FIELD_CLASS> > &fieldList);

        ZOO_INT32 drop_table(const std::string &table_name);

        ZOO_INT32 insert_table(const std::string &table_name,
                        std::list<boost::shared_ptr<TABLE_FIELD_CLASS>> &fieldList);

        ZOO_INT32 remove_data(const std::string &table_name, const std::string &condition);

        ZOO_INT32 remove_all_data(const std::string &table_name);

        ZOO_INT32 update(const std::string &table_name,
                   const std::string &condition,
                   std::list<boost::shared_ptr<TABLE_FIELD_CLASS>> &fieldList);

        ZOO_INT32 query(const std::string &table_name);

        ZOO_INT32 query(const std::string &table_name,
                            std::list<boost::shared_ptr<TABLE_FIELD_CLASS>> &fields,
                            const std::string &condition);

        ZOO_INT32 get_data(DB4A_RECORD_STRUCT *record);

        ZOO_INT32 get_row_size();

        /**
         * @brief Start the transaction
         * @param session_id: the unique identity
         * @return  0 successful, -1 failed
        */
        ZOO_INT32 begin_transaction();

        /**
         * @brief Commit the transaction if there is no error in the transaction
         * @param session_id: the unique identity
         * @return  0 successful, -1 failed
        */
        ZOO_INT32 commit_transaction();

        /**
         * @brief Roll back the transaction if there is no error in the transaction
         * @param session_id: the unique identity
         * @return  0 successful, -1 failed
        */
        ZOO_INT32 rollback_transaction();

    private:
        ZOO_INT32 exec_sql(const std::string &sql);

        ZOO_INT32 exec_sql(const std::vector<std::string> &sqls);

    private:
        std::string m_backend_name;

        std::string m_connect_string;

        soci::session m_session;

        soci::rowset<soci::row> m_select_rows;

        ZOO_INT32 m_index;

        ZOO_INT32 m_row_size;

        ZOO_BOOL m_connect_state;

        soci::rowset<soci::row>::iterator m_iter;

    };

}


#endif // DATA_BASE_CLASS_H
