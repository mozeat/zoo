/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : DBMA_implement.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-01    Generator      created
*************************************************************/
#ifndef DBMA_IMPLEMENT_H
#define DBMA_IMPLEMENT_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include "DBMA_event.h"
#include "DB4I_type.h"
#include "DB4A_type.h"


/**
 *@brief DBMA_implement_4A_connect
 *@param option
 *@param session_id
**/
ZOO_EXPORT void DBMA_implement_4A_connect(IN const DB4A_CONNECT_OPTION_STRUCT* option,
                                              IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_disconnect
 *@param session_id
**/
ZOO_EXPORT void DBMA_implement_4A_disconnect(IN ZOO_HANDLE session_id,
                                                 IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_get_connection_state
 *@param session_id
 *@param state
**/
ZOO_EXPORT void DBMA_implement_4A_get_connection_state(IN ZOO_HANDLE session_id,
                                                           IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_create_table
 *@param session_id
 *@param table_name
 *@param primary_key_name
 *@param fields
 *@param fields_number
**/
ZOO_EXPORT void DBMA_implement_4A_create_table(IN ZOO_HANDLE session_id,
                                                   IN const ZOO_CHAR* table_name,
                                                   IN const ZOO_CHAR* primary_key_name,
                                                   IN const DB4A_FIELD_TYPE_STRUCT* fields,
                                                   IN ZOO_INT32 fields_number,
                                                   IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_drop_table
 *@param session_id
 *@param table_name
**/
ZOO_EXPORT void DBMA_implement_4A_drop_table(IN ZOO_HANDLE session_id,
                                                 IN const ZOO_CHAR* table_name,
                                                 IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_update_data
 *@param session_id
 *@param table_name
 *@param condition
 *@param new_datas
 *@param new_datas_number
**/
ZOO_EXPORT void DBMA_implement_4A_update_data(IN ZOO_HANDLE session_id,
                                                  IN const ZOO_CHAR* table_name,
                                                  IN const ZOO_CHAR* condition,
                                                  IN const DB4A_FIELD_DATA_STRUCT* new_datas,
                                                  IN ZOO_INT32 new_datas_number,
                                                  IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_delete_data_by_condition
 *@param session_id
 *@param table_name
 *@param condition
**/
ZOO_EXPORT void DBMA_implement_4A_delete_data_by_condition(IN ZOO_HANDLE session_id,
                                                               IN const ZOO_CHAR* table_name,
                                                               IN const ZOO_CHAR* condition,
                                                               IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_delete_all_data
 *@param session_id
 *@param table_name
**/
ZOO_EXPORT void DBMA_implement_4A_delete_all_data(IN ZOO_HANDLE session_id,
                                                      IN const ZOO_CHAR* table_name,
                                                      IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_insert_data
 *@param session_id
 *@param table_name
 *@param new_data
 *@param fields_number
**/
ZOO_EXPORT void DBMA_implement_4A_insert_data(IN ZOO_HANDLE session_id,
                                                  IN const ZOO_CHAR* table_name,
                                                  IN const DB4A_FIELD_DATA_STRUCT* new_data,
                                                  IN ZOO_INT32 fields_number,
                                                  IN DB4I_REPLY_HANDLE reply_handle);

/**
 *@brief DBMA_implement_4A_get_data
 *@param session_id
 *@param record
**/
ZOO_EXPORT void DBMA_implement_4A_get_data(IN ZOO_HANDLE session_id,
                                               IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_get_row_size
 *@param session_id
 *@param row_size
**/
ZOO_EXPORT void DBMA_implement_4A_get_row_size(IN ZOO_HANDLE session_id,
                                                   IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_query_all
 *@param session_id
 *@param table_name
**/
ZOO_EXPORT void DBMA_implement_4A_query_all(IN ZOO_HANDLE session_id,
                                                IN const char* table_name,
                                                IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_query_by_condition
 *@param session_id
 *@param table_name
 *@param fields
 *@param fields_number
 *@param condition
**/
ZOO_EXPORT void DBMA_implement_4A_query_by_condition(IN ZOO_HANDLE session_id,
                                                         IN const ZOO_CHAR* table_name,
                                                         IN const DB4A_FIELD_DATA_STRUCT* fields,
                                                         IN ZOO_INT32 fields_number,
                                                         IN ZOO_CHAR* condition,
                                                         IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_begin_transaction
 *@param session_id
**/
ZOO_EXPORT void DBMA_implement_4A_begin_transaction(IN ZOO_HANDLE session_id,
                                                        IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_commit_transaction
 *@param session_id
**/
ZOO_EXPORT void DBMA_implement_4A_commit_transaction(IN ZOO_HANDLE session_id,
                                                         IN DB4I_REPLY_HANDLE reply_handle);
/**
 *@brief DBMA_implement_4A_rollback_transaction
 *@param session_id
**/
ZOO_EXPORT void DBMA_implement_4A_rollback_transaction(IN ZOO_HANDLE session_id,
                                                           IN DB4I_REPLY_HANDLE reply_handle);

#endif // DBMA_implement.h
