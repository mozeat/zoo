include ../Makefile_tpl_cov
include ../Project_config

TARGET   := DBMA
SRCEXTS  := .cpp .c 
INCDIRS  := ./inc ./com /usr/local/include
SOURCES  := 
SRCDIRS  := ./bin ./lib
CFLAGS   := 
CXXFLAGS := -std=c++14 -fstack-protector-all
CPPFLAGS := -DBOOST_ALL_DYN_LINK
LDFPATH  := -L$(THIRD_PARTY_LIBRARY_PATH)
LDFLAGS  := $(GCOV_LINK) $(LDFPATH) -lMM4A -lMQ4A -lDB4A -lZOO -lsoci_core -lsoci_mysql -ldl -lmysqlclient -lsqlite3 -lz

include ../Makefile_tpl_linux
