/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : DB
 * File Name    : DATA_BASE_CLASS.cpp
 * Description  : The DATA_BASE_CLASS class provides an interface for accessing a database through a connection.
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-10-30      chaoluo       Create
 ***************************************************************/

extern "C"
{
	#include "ZOO_if.h"
	#include "MM4A_if.h"
}


#include "DATA_BASE_CLASS.h"

#include <iostream>
#include <memory.h>
#include <stdlib.h>
#include <soci/sqlite3/soci-sqlite3.h>
#include <soci/mysql/soci-mysql.h>


using namespace std;
namespace ZOO_DB {

    DATA_BASE_CLASS::DATA_BASE_CLASS(const string &backend_name, const string &connect_string)
        : m_backend_name(backend_name),
          m_connect_string(connect_string),
          m_session(backend_name, connect_string),
          m_select_rows(m_session.prepare_table_names())
    {
        this->m_index = 0;
        this->m_row_size = 0;
        this->m_connect_state = ZOO_TRUE;
    }

    DATA_BASE_CLASS::~DATA_BASE_CLASS()
    {

    }

    ZOO_INT32 DATA_BASE_CLASS::close()
    {
        this->m_session.close();
        this->m_index = 0;
        this->m_connect_state = ZOO_FALSE;
        return  OK;
    }

    ZOO_INT32 DATA_BASE_CLASS::get_connect_state()
    {
        return this->m_connect_state;
    }

    ZOO_INT32 DATA_BASE_CLASS::create_table(const string &table_name,
                                            const string &primary_key,
                                            std::list<boost::shared_ptr<TABLE_FIELD_CLASS> > &fieldList)
    {
        std::string sql = "CREATE TABLE ";
        sql += table_name;
        sql += " (";

        std::string sql_primary_key = " PRIMARY KEY (" + primary_key + ")";
        TABLE_FIELD_CLASS *field = nullptr;
        for (auto shareField : fieldList)
        {
            field = shareField.get();
            if (nullptr == field)
            {
                continue;
            }

            sql += field->get_name();
            sql += " VARCHAR(";
            sql += field->get_name_len();
            sql += ")";
            if (0 == field->get_name().compare(primary_key))
            {
                sql += " NOT NULL";
            }
            sql += ",";
        }

        if (primary_key.empty())
        {
            sql.pop_back();
        }
        else
        {
            sql += sql_primary_key;
        }

        sql += ");";

        return this->exec_sql(sql);
    }

    ZOO_INT32 DATA_BASE_CLASS::drop_table(const string &table_name)
    {
        std::string sql = "DROP TABLE ";
        sql += table_name ;
        sql += ";";

        return this->exec_sql(sql);
    }

    ZOO_INT32 DATA_BASE_CLASS::insert_table(const string &table_name,
                                            std::list<boost::shared_ptr<TABLE_FIELD_CLASS> > &fieldList)
    {
        std::string sql = "INSERT INTO ";
        sql += table_name;
        sql += " (";

        std::string value = " VALUES (";

        TABLE_FIELD_CLASS *field = nullptr;
        for (auto shareField : fieldList)
        {
            field = shareField.get();
            if (nullptr == field)
            {
                continue;
            }
            sql += field->get_name();

            value += "\'";
            value += field->get_value();
            value += "\'";

            sql += ",";
            value += ",";
        }

        sql.pop_back();
        value.pop_back();

        sql += ")";
        value += ")";

        sql += value;
        sql += ";";

        return this->exec_sql(sql);
    }

    ZOO_INT32 DATA_BASE_CLASS::remove_data(const string &table_name, const string &condition)
    {
        std::string sql = "DELETE FROM ";
        sql += table_name;
        sql += " WHERE ";
        sql += condition;
        sql += ";";

        return this->exec_sql(sql);
    }

    ZOO_INT32 DATA_BASE_CLASS::remove_all_data(const string &table_name)
    {
        std::string sql = "DELETE FROM ";
        sql += table_name;
        sql += " WHERE 1=1;";

        return this->exec_sql(sql);
    }

    ZOO_INT32 DATA_BASE_CLASS::update(const string &table_name,
                                      const string &condition,
                                      std::list<boost::shared_ptr<TABLE_FIELD_CLASS> > &fieldList)
    {
        std::string sql = "UPDATE ";
        sql += table_name;

        TABLE_FIELD_CLASS *field = nullptr;
        for (auto shareField : fieldList)
        {
            field = shareField.get();
            if (nullptr == field)
            {
                continue;
            }

            sql += " SET ";
            sql += field->get_name();
            sql += "=";
            sql += "\'";
            sql += field->get_value();
            sql += "\'";
            sql += ",";
        }

        sql.pop_back();

        sql += " WHERE ";
        sql += condition;

        sql += ";";

        return this->exec_sql(sql);
    }

    ZOO_INT32 DATA_BASE_CLASS::query(const string &table_name)
    {
        std::string sql = "SELECT * FROM ";
        sql += table_name ;
        sql += ";";

        std::string sql_size = "SELECT COUNT(*) FROM ";
        sql_size += table_name ;
        sql_size += ";";

        std::vector<std::string> sqls;
        sqls.push_back(sql_size);
        sqls.push_back(sql);

        return this->exec_sql(sqls);
    }

    ZOO_INT32 DATA_BASE_CLASS::query(const string &table_name,
                                     std::list<boost::shared_ptr<TABLE_FIELD_CLASS> > &fields,
                                     const string &condition)
    {

        std::string sql_size = "SELECT COUNT(*) ";
        std::string sql = "SELECT ";

        TABLE_FIELD_CLASS *field = nullptr;
        for (auto shareField : fields)
        {
            field = shareField.get();
            if (nullptr == field)
            {
                continue;
            }

            sql += field->get_name();
            sql += ",";
        }

        sql.pop_back();

        sql += " FROM ";
        sql += table_name;

        sql += " WHERE ";
        sql += condition;
        sql += ";";

        sql_size += " FROM ";
        sql_size += table_name;

        sql_size += " WHERE ";
        sql_size += condition;
        sql_size += ";";

        std::vector<std::string> sqls;
        sqls.push_back(sql_size);
        sqls.push_back(sql);

        return this->exec_sql(sqls);
    }

    ZOO_INT32 DATA_BASE_CLASS::get_data(DB4A_RECORD_STRUCT *record)
    {
        if (this->m_iter != this->m_select_rows.end())
        {
            record->fields_number = this->m_iter->size();            
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "size[%d]", this->m_iter->size());
            for (ZOO_UINT32 i = 0; i < this->m_iter->size(); ++i)
            {
                std::string value = this->m_iter->get<std::string>(i);
                std::string name = this->m_iter->get_properties(i).get_name();
                memcpy(record->field_list[i].field_name, name.c_str(), name.length());
                memcpy(record->field_list[i].field_data, value.c_str(), value.length());
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "name[%s] value[%s]", name.c_str(), value.c_str());
            }

            ++this->m_iter;
            return OK;
        }

        return DB4A_SYSTEM_ERR;
    }

    ZOO_INT32 DATA_BASE_CLASS::get_row_size()
    {
        return this->m_row_size;
    }

    ZOO_INT32 DATA_BASE_CLASS::begin_transaction()
    {
        std::string sql = "BEGIN;";
        return this->exec_sql(sql);
    }

    ZOO_INT32 DATA_BASE_CLASS::commit_transaction()
    {
        std::string sql = "COMMIT;";
        return this->exec_sql(sql);
    }

    ZOO_INT32 DATA_BASE_CLASS::rollback_transaction()
    {
        std::string sql = "ROLLBACK;";
        return this->exec_sql(sql);
    }

    ZOO_INT32 DATA_BASE_CLASS::exec_sql(const string &sql)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "exec sql[%s]", sql.c_str());
        try
        {            
            this->m_session << sql;
        }
//        catch (exception const &e)
//        {
//            ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__, "exec sql[%s] error[%s]", sql.c_str(), e.what());
//            return DB4A_SYSTEM_ERR;
//        }
        catch(...)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__, "exec sql error");
            return DB4A_SYSTEM_ERR;
        }

        return  OK;
    }

    ZOO_INT32 DATA_BASE_CLASS::exec_sql(const std::vector<string> &sqls)
    {
        if (sqls.size() <= 1)
        {
            return DB4A_SYSTEM_ERR;
        }

        try
        {
            this->m_index = 0;

            soci::session _session;
            this->m_session << sqls.at(0), soci::into(this->m_row_size);
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "exec sql[%s] size[%d]", sqls.at(0).c_str(), this->m_row_size);

            this->m_select_rows = (this->m_session.prepare << sqls.at(1));
            this->m_iter = this->m_select_rows.begin();
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "exec sql[%s]", sqls.at(1).c_str());
        }
//        catch (exception const &e)
//        {
//            ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__, "exec sql[%s] error[%s]", sqls.at(0).c_str(), e.what());
//            return DB4A_SYSTEM_ERR;
//        }
        catch(...)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__, "exec sql error");
            return DB4A_SYSTEM_ERR;
        }

        return OK;
    }
}
