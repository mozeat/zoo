/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : DBMA_implement.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-01    Generator      created
*************************************************************/
#include "DBMA_implement.h"
#include "DB_FLOW_FACADE_WRAPPER.h"
#include "MM4A_if.h"

/**
 *@brief DBMA_implement_4A_connect
 *@param option
 *@param session_id
**/
void DBMA_implement_4A_connect(IN const DB4A_CONNECT_OPTION_STRUCT* option,
                                   IN DB4I_REPLY_HANDLE reply_handle)
{    
    ZOO_INT32 rtn = OK;
    ZOO_HANDLE session_id;
    /* User add some code here if had special need. */    

    rtn = DB_connect(&session_id, option);

    /* User add ... END*/
    DBMA_raise_4A_connect(rtn,session_id,reply_handle);
}

/**
 *@brief DBMA_implement_4A_disconnect
 *@param session_id
**/
void DBMA_implement_4A_disconnect(IN ZOO_HANDLE session_id,
                                      IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = DB_disconnect(session_id);

    /* User add ... END*/
	DBMA_raise_4A_disconnect(rtn,reply_handle);
}

/**
 *@brief DBMA_implement_4A_get_connection_state
 *@param session_id
 *@param state
**/
void DBMA_implement_4A_get_connection_state(IN ZOO_HANDLE session_id,
                                                IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    ZOO_BOOL state  = ZOO_FALSE;
    /* User add some code here if had special need. */
    rtn = DB_get_connection_state(session_id,&state);

    /* User add ... END*/
	DBMA_raise_4A_get_connection_state(rtn,state,reply_handle);
}

/**
 *@brief DBMA_implement_4A_create_table
 *@param session_id
 *@param table_name
 *@param primary_key_name
 *@param fields
 *@param fields_number
**/
void DBMA_implement_4A_create_table(IN ZOO_HANDLE session_id,
                                        IN const ZOO_CHAR* table_name,
                                        IN const ZOO_CHAR* primary_key_name,
                                        IN const DB4A_FIELD_TYPE_STRUCT* fields,
                                        IN ZOO_INT32 fields_number,
                                        IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = DB_create_table(session_id,table_name,primary_key_name,fields,fields_number);

    /* User add ... END*/
    DBMA_raise_4A_create_table(rtn,reply_handle);
}

/**
 *@brief DBMA_implement_4A_drop_table
 *@param session_id
 *@param table_name
**/
void DBMA_implement_4A_drop_table(IN ZOO_HANDLE session_id,
                                      IN const ZOO_CHAR* table_name,
                                      IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = DB_drop_table(session_id,table_name);

    /* User add ... END*/
	DBMA_raise_4A_drop_table(rtn,reply_handle);
}

/**
 *@brief DBMA_implement_4A_update_data
 *@param session_id
 *@param table_name
 *@param condition
 *@param new_datas
 *@param new_datas_number
**/
void DBMA_implement_4A_update_data(IN ZOO_HANDLE session_id,
                                       IN const ZOO_CHAR* table_name,
                                       IN const ZOO_CHAR* condition,
                                       IN const DB4A_FIELD_DATA_STRUCT* new_datas,
                                       IN ZOO_INT32 new_datas_number,
                                       IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = DB_update_data(session_id,table_name,condition,new_datas,new_datas_number);

    /* User add ... END*/
	DBMA_raise_4A_update_data(rtn,reply_handle);
}

/**
 *@brief DBMA_implement_4A_delete_data_by_condition
 *@param session_id
 *@param table_name
 *@param condition
**/
void DBMA_implement_4A_delete_data_by_condition(IN ZOO_HANDLE session_id,
                                                    IN const ZOO_CHAR* table_name,
                                                    IN const ZOO_CHAR* condition,
                                                    IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = DB_delete_data_by_condition(session_id,table_name,condition);

    /* User add ... END*/
	DBMA_raise_4A_delete_data_by_condition(rtn,reply_handle);
}

/**
 *@brief DBMA_implement_4A_delete_all_data
 *@param session_id
 *@param table_name
**/
void DBMA_implement_4A_delete_all_data(IN ZOO_HANDLE session_id,
                                           IN const ZOO_CHAR* table_name,
                                           IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = DB_delete_all_data(session_id,table_name);

    /* User add ... END*/
    DBMA_raise_4A_delete_all_data(rtn,reply_handle);
}

/**
 *@brief DBMA_implement_4A_insert_data
 *@param session_id
 *@param table_name
 *@param new_data
 *@param fields_number
**/
void DBMA_implement_4A_insert_data(IN ZOO_HANDLE session_id,
                                       IN const ZOO_CHAR* table_name,
                                       IN const DB4A_FIELD_DATA_STRUCT* new_data,
                                       IN ZOO_INT32 fields_number,
                                       IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = DB_insert_data(session_id,table_name,new_data,fields_number);

    /* User add ... END*/
    DBMA_raise_4A_insert_data(rtn,reply_handle);		
}

/**
 *@brief DBMA_implement_4A_get_data
 *@param session_id
 *@param record
**/
void DBMA_implement_4A_get_data(IN ZOO_HANDLE session_id,
                                    IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    DB4A_RECORD_STRUCT record ;
    memset(&record,0,sizeof(DB4A_RECORD_STRUCT));
    /* User add some code here if had special need. */
    rtn = DB_get_data(session_id, &record);

    /* User add ... END*/
    DBMA_raise_4A_get_data(rtn,&record,reply_handle);		
}

/**
 *@brief DBMA_implement_4A_get_row_size
 *@param session_id
 *@param row_size
**/
void DBMA_implement_4A_get_row_size(IN ZOO_HANDLE session_id,
                                        IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    ZOO_INT32 row_size;
    /* User add some code here if had special need. */
    row_size = DB_get_row_size(session_id);

    /* User add ... END*/
    DBMA_raise_4A_get_row_size(rtn,row_size,reply_handle);
}

/**
 *@brief DBMA_implement_4A_query_all
 *@param session_id
 *@param table_name
**/
void DBMA_implement_4A_query_all(IN ZOO_HANDLE session_id,
                                     IN const char* table_name,
                                     IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = DB_query_all(session_id,table_name);

    /* User add ... END*/
    DBMA_raise_4A_query_all(rtn,reply_handle);
}

/**
 *@brief DBMA_implement_4A_query_by_condition
 *@param session_id
 *@param table_name
 *@param fields
 *@param fields_number
 *@param condition
**/
void DBMA_implement_4A_query_by_condition(IN ZOO_HANDLE session_id,
                                              IN const ZOO_CHAR* table_name,
                                              IN const DB4A_FIELD_DATA_STRUCT* fields,
                                              IN ZOO_INT32 fields_number,
                                              IN ZOO_CHAR* condition,
                                              IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = DB_query_by_condition(session_id,table_name,fields,fields_number,condition);

    /* User add ... END*/
    DBMA_raise_4A_query_by_condition(rtn,reply_handle);
}

/**
 *@brief DBMA_implement_4A_begin_transaction
 *@param session_id
**/
void DBMA_implement_4A_begin_transaction(IN ZOO_HANDLE session_id,
                                             IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = DB_begin_transaction(session_id);

    /* User add ... END*/
    DBMA_raise_4A_begin_transaction(rtn,reply_handle);
}

/**
 *@brief DBMA_implement_4A_commit_transaction
 *@param session_id
**/
void DBMA_implement_4A_commit_transaction(IN ZOO_HANDLE session_id,
                                              IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = DB_commit_transaction(session_id);

    /* User add ... END*/
    DBMA_raise_4A_commit_transaction(rtn,reply_handle);
}

/**
 *@brief DBMA_implement_4A_rollback_transaction
 *@param session_id
**/
void DBMA_implement_4A_rollback_transaction(IN ZOO_HANDLE session_id,
                                                IN DB4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = DB_rollback_transaction(session_id);

    /* User add ... END*/
    DBMA_raise_4A_rollback_transaction(rtn,reply_handle);
}

