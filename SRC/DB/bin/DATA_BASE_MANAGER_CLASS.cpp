/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : DB
 * File Name    : DATA_BASE_MANAGER_CLASS.cpp
 * Description  : database manager class
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-10-30      chaoluo       Create
 ***************************************************************/
extern "C"
{
	#include "ZOO_if.h"
}


#include "DATA_BASE_MANAGER_CLASS.h"
#include "TABLE_FIELD_CLASS.h"

#include <boost/smart_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include <memory>
#include <string.h>

#include <iostream>
#include <random>

using namespace std;

namespace ZOO_DB {

    static char RIGHT_SLASH[] = "/";

    boost::shared_ptr<DATA_BASE_MANAGER_CLASS> DATA_BASE_MANAGER_CLASS::m_instance(new DATA_BASE_MANAGER_CLASS());

    DATA_BASE_MANAGER_CLASS::DATA_BASE_MANAGER_CLASS()
    {
        this->m_databases.clear();
        this->m_session_ids.clear();
        m_id = 0;
    }

    ZOO_UINT32 DATA_BASE_MANAGER_CLASS::get_session_id()
    {
        for (auto iter = this->m_session_ids.begin(); iter != this->m_session_ids.end(); ++iter)
        {
            if (!iter->second)
            {
                this->m_session_ids[iter->first] = ZOO_TRUE;
                return iter->first;
            }
        }

        this->m_session_ids[++this->m_id] = ZOO_TRUE;
        return this->m_id;
    }

    void DATA_BASE_MANAGER_CLASS::reset_session_id(ZOO_HANDLE session_id)
    {
        this->m_session_ids[session_id] = ZOO_FALSE;
    }

    boost::shared_ptr<DATA_BASE_CLASS> DATA_BASE_MANAGER_CLASS::find_database(ZOO_HANDLE session_id)
    {
        auto iter = this->m_databases.find(session_id);
        if (iter != this->m_databases.end())
        {
            return iter->second;
        }

        return nullptr;
    }

    DATA_BASE_MANAGER_CLASS::~DATA_BASE_MANAGER_CLASS()
    {
        m_id = 1;
        this->m_session_ids.clear();
    }

    boost::shared_ptr<DATA_BASE_MANAGER_CLASS> DATA_BASE_MANAGER_CLASS::instance()
    {
        return m_instance;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::connect_database(ZOO_HANDLE *session_id, const DB4A_CONNECT_OPTION_STRUCT *option)
    {
        if (nullptr == option)
        {
            return DB4A_ILLEGAL_ERR;
        }

        std::string connect_string;
        std::string backend_name;
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "type[%d]", option->db_type);
        switch (option->db_type)
        {
        case DB4A_SQLITE3:
        {
           	ZOO_USER_PATH_STRUCT files_path;
			ZOO_INT32 ret = ZOO_get_user_files_path(&files_path);
            std::string path = std::string(files_path.output.db) + std::string(RIGHT_SLASH);
			if (ret != OK)
			{
				path = std::string("/tmp") + std::string(RIGHT_SLASH);
			}

            backend_name = "sqlite3";
            path += option->db_name;
            path += ".db";

            connect_string += "db=";
            connect_string += path;

        }
            break;

        case DB4A_MYSQL:
        {
            backend_name = "mysql";

            connect_string += "db=";
            connect_string += option->db_name;
            connect_string += " ";
            connect_string += "user=";
            connect_string += option->name;
            connect_string += " ";
            connect_string += "host=";
            connect_string += option->host_name;
            connect_string += " ";
            connect_string += "password=\'";
            connect_string += option->password;
            connect_string += "\' ";
        }
            break;

        default:
            ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__, "not support type[%d]", option->db_type);
            return DB4A_SYSTEM_ERR;
        }

        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "backendName[%s] connectString[%s]", backend_name.c_str(), connect_string.c_str());
        ZOO_INT32 ret = OK;
        try
        {
            *session_id = this->get_session_id();

            boost::shared_ptr<DATA_BASE_CLASS> data_base = this->find_database(*session_id);
            if (nullptr == data_base)
            {
                data_base.reset(new DATA_BASE_CLASS(backend_name, connect_string));
            }

            this->m_databases[*session_id] = data_base;
        }
//        catch (exception const &e)
//        {
//            ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__, "error[%s]", e.what());
//            return DB4A_SYSTEM_ERR;
//        }
        catch (...)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__, "create table erro, session id[%d]", this->m_id);
            return DB4A_SYSTEM_ERR;
        }

        return ret;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::disconnect_database(ZOO_HANDLE session_id)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            this->reset_session_id(session_id);
            db->close();

			this->m_databases[session_id].reset();
			return OK;
        }

        return DB4A_SYSTEM_ERR;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::get_connect_state(ZOO_HANDLE session_id, ZOO_BOOL *state)
    {
        auto db = this->find_database(session_id);
        *state = ZOO_FALSE;
        if (db)
        {
            *state = db->get_connect_state();
        }

        return OK;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::create_table(ZOO_HANDLE session_id,
                                                    const char *table_name,
                                                    const char *primary_key_name,
                                                    const DB4A_FIELD_TYPE_STRUCT *fields,
                                                    int fields_number)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            std::list<boost::shared_ptr<TABLE_FIELD_CLASS>> fieldList;
            for (int i = 0; i < fields_number; ++i)
            {
                boost::shared_ptr<TABLE_FIELD_CLASS> share_field(new TABLE_FIELD_CLASS(fields[i].field_name));;
                share_field->set_name_len(boost::lexical_cast<std::string>(fields[i].length));
                fieldList.push_back(share_field);
            }
            return db->create_table(table_name, primary_key_name, fieldList);
        }

        return DB4A_SYSTEM_ERR;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::drop_table(ZOO_HANDLE session_id, const char *name)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            return db->drop_table(name);
        }

        return DB4A_SYSTEM_ERR;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::insert_table(ZOO_HANDLE session_id,
                                                    const char *table_name,
                                                    const DB4A_FIELD_DATA_STRUCT *new_data,
                                                    int fields_number)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            std::list<boost::shared_ptr<TABLE_FIELD_CLASS>> fieldList;
            for (int i = 0; i < fields_number; ++i)
            {
                boost::shared_ptr<TABLE_FIELD_CLASS> shareField(new TABLE_FIELD_CLASS);
                shareField->set_name(new_data[i].field_name);
                shareField->set_value(new_data[i].field_data);
                fieldList.push_back(shareField);
            }
            return db->insert_table(table_name, fieldList);
        }

        return DB4A_SYSTEM_ERR;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::remove_data_by_condition(ZOO_HANDLE session_id,
                                                                const char *table_name,
                                                                const char *condition)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            return db->remove_data(table_name, condition);
        }

        return DB4A_SYSTEM_ERR;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::remove_all_data(ZOO_HANDLE session_id, const char *table_name)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            return db->remove_all_data(table_name);
        }
        return DB4A_SYSTEM_ERR;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::update_data(ZOO_HANDLE session_id,
                                                   const char* table_name,
                                                   const char* condition,
                                                   const DB4A_FIELD_DATA_STRUCT* new_datas,
                                                   int new_datas_number)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            std::list<boost::shared_ptr<TABLE_FIELD_CLASS>> fieldList;
            for (int i = 0; i < new_datas_number; ++i)
            {
                boost::shared_ptr<TABLE_FIELD_CLASS> shareField(new TABLE_FIELD_CLASS);
                shareField->set_name(new_datas[i].field_name);
                shareField->set_value(new_datas[i].field_data);
                fieldList.push_back(shareField);
            }
            return db->update(table_name, condition, fieldList);
        }
        return DB4A_SYSTEM_ERR;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::get_data(ZOO_HANDLE session_id, DB4A_RECORD_STRUCT *record)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            return db->get_data(record);
        }

        return  DB4A_SYSTEM_ERR;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::query_all(ZOO_HANDLE session_id, const char *table_name)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            return db->query(table_name);
        }
        return DB4A_SYSTEM_ERR;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::query_by_condition(ZOO_HANDLE session_id,
                                                          const ZOO_CHAR * table_name,
                                                          const DB4A_FIELD_DATA_STRUCT* fields,
                                                          ZOO_INT32 fields_number,
                                                          ZOO_CHAR * conditions)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            std::list<boost::shared_ptr<TABLE_FIELD_CLASS>> fieldList;
            for (int i = 0; i < fields_number; ++i)
            {
                std::string field_name = std::string(fields[i].field_name);
                boost::shared_ptr<TABLE_FIELD_CLASS> shareField(new TABLE_FIELD_CLASS);
                shareField->set_name(field_name);
                fieldList.push_back(shareField);
            }
            return db->query(table_name, fieldList, conditions);
        }

        return DB4A_SYSTEM_ERR;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::get_row_size(ZOO_HANDLE session_id)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            return db->get_row_size();
        }

        return 0;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::begin_transaction(ZOO_HANDLE session_id)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            return db->begin_transaction();
        }

        return DB4A_SYSTEM_ERR;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::commit_transaction(ZOO_HANDLE session_id)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            return db->commit_transaction();
        }
        return DB4A_SYSTEM_ERR;
    }

    ZOO_INT32 DATA_BASE_MANAGER_CLASS::rollback_transaction(ZOO_HANDLE session_id)
    {
        auto db = this->find_database(session_id);
        if (db)
        {
            return db->rollback_transaction();
        }
        return DB4A_SYSTEM_ERR;
    }
}
