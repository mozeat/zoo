/**************************************************************
 * Copyright (C) 2018, SHANGHAI XXXX CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : DB
 * File Name    : TABLE_FIELD_CLASS.cpp
 * Description  : database table field class
 * History :
 * Version      Date            User         Comments
 * V1.0.0.0     2018-10-30      chaoluo       Create
 ***************************************************************/
#include "TABLE_FIELD_CLASS.h"

namespace ZOO_DB {
    TABLE_FIELD_CLASS::TABLE_FIELD_CLASS()
        : m_name(""),
          m_type(TABLE_FIELD_CLASS::String),
          m_value(""),
          m_name_len("")
    {

    }

    TABLE_FIELD_CLASS::TABLE_FIELD_CLASS(std::string name, std::string value, TABLE_FIELD_CLASS::FieldTypeEnum type)
        : m_name(name),
          m_type(type),
          m_value(value)
    {

    }

    TABLE_FIELD_CLASS::TABLE_FIELD_CLASS(const TABLE_FIELD_CLASS &other)
    {
        *this = other;
    }

    TABLE_FIELD_CLASS &TABLE_FIELD_CLASS::operator=(const TABLE_FIELD_CLASS &other)
    {
        if (this == &other)
        {
            return *this;
        }

        m_name = other.m_name;
        m_type = other.m_type;
        m_value = other.m_value;

        return *this;
    }

    TABLE_FIELD_CLASS::~TABLE_FIELD_CLASS()
    {

    }

    std::string TABLE_FIELD_CLASS::get_name() const
    {
        return m_name;
    }

    void TABLE_FIELD_CLASS::set_name(const std::string &name)
    {
        m_name = name;
    }

    TABLE_FIELD_CLASS::FieldTypeEnum TABLE_FIELD_CLASS::get_type() const
    {
        return m_type;
    }

    void TABLE_FIELD_CLASS::set_type(const FieldTypeEnum &type)
    {
        m_type = type;
    }

    std::string TABLE_FIELD_CLASS::get_value() const
    {
        return m_value;
    }

    void TABLE_FIELD_CLASS::set_value(const std::string &value)
    {
        m_value = value;
    }
    
    std::string TABLE_FIELD_CLASS::get_name_len() const
    {
        return m_name_len;
    }
    
    void TABLE_FIELD_CLASS::set_name_len(const std::string &name_len)
    {
        m_name_len = name_len;
    }
}
