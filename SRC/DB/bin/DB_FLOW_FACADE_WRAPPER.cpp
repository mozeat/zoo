#include "DB_FLOW_FACADE_WRAPPER.h"
#include "DATA_BASE_MANAGER_CLASS.h"
#include "ZOO_if.h"

ZOO_INT32 DB_connect(ZOO_HANDLE *session_id, const DB4A_CONNECT_OPTION_STRUCT* option)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->connect_database(session_id, option);
}

ZOO_INT32 DB_disconnect(ZOO_HANDLE session_id)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->disconnect_database(session_id);
}

ZOO_INT32 DB_get_connection_state(ZOO_HANDLE session_id, ZOO_BOOL *state)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->get_connect_state(session_id, state);
}

ZOO_INT32 DB_create_table(ZOO_HANDLE session_id,
                const char *table_name,
                const char *primary_key_name,
                const DB4A_FIELD_TYPE_STRUCT *fields,
                int fields_number)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->create_table(session_id,
                                                                 table_name,
                                                                 primary_key_name,
                                                                 fields,
                                                                 fields_number);
}

ZOO_INT32 DB_drop_table(ZOO_HANDLE session_id, const char *name)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->drop_table(session_id, name);
}

ZOO_INT32 DB_insert_data(ZOO_HANDLE session_id,
                const char *table_name,
                const DB4A_FIELD_DATA_STRUCT *new_data,
                int fields_number)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->insert_table(session_id,
                                                  table_name,
                                                  new_data,
                                                  fields_number);
}

/**
 * @brief get Data
 * @return Get the query result
 */
ZOO_INT32 DB_get_data(ZOO_HANDLE session_id, DB4A_RECORD_STRUCT *record)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->get_data(session_id, record);
}

ZOO_INT32 DB_delete_data_by_condition(ZOO_HANDLE session_id,
               const char *table_name,
               const char* condition)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->remove_data_by_condition(session_id,
                                                                table_name,
                                                                condition);
}

ZOO_INT32 DB_delete_all_data(ZOO_HANDLE session_id, const char *table_name)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->remove_all_data(session_id, table_name);
}

ZOO_INT32 DB_update_data(ZOO_HANDLE session_id,
                           const char* table_name,
                           const char* condition,
                           const DB4A_FIELD_DATA_STRUCT* new_datas,
                           int new_datas_number)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->update_data(session_id,
                                                                table_name,
                                                                condition,
                                                                new_datas,
                                                                new_datas_number);
}

ZOO_INT32 DB_query_all(ZOO_HANDLE session_id, const char *table_name)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->query_all(session_id, table_name);
}

ZOO_INT32 DB_query_by_condition(ZOO_HANDLE session_id,
                           const ZOO_CHAR * table_name,
                           const DB4A_FIELD_DATA_STRUCT* fields,
                           ZOO_INT32 fields_number,
                           ZOO_CHAR * conditions)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->query_by_condition(session_id,
                                                              table_name,
                                                              fields,
                                                              fields_number,
                                                              conditions);
}

ZOO_INT32 DB_begin_transaction(ZOO_HANDLE session_id)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->begin_transaction(session_id);
}

ZOO_INT32 DB_commit_transaction(ZOO_HANDLE session_id)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->commit_transaction(session_id);
}

ZOO_INT32 DB_rollback_transaction(ZOO_HANDLE session_id)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->rollback_transaction(session_id);
}

ZOO_INT32 DB_get_row_size(ZOO_HANDLE session_id)
{
    return ZOO_DB::DATA_BASE_MANAGER_CLASS::instance()->get_row_size(session_id);
}
