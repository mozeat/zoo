/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : DBMA_event.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-01    Generator      created
*************************************************************/
#include "DBMA_event.h"

/**
 *@brief DBMA_raise_4A_connect
 *@param option
 *@param session_id
**/
ZOO_INT32 DBMA_raise_4A_connect(IN ZOO_INT32 error_code,
                                    IN ZOO_HANDLE session_id,
                                    IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_CONNECT_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_CONNECT_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.connect_rep_msg.session_id,&session_id,sizeof(ZOO_HANDLE));
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);                
                if(OK != rtn)
                {                    
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_disconnect
 *@param session_id
**/
ZOO_INT32 DBMA_raise_4A_disconnect(IN ZOO_INT32 error_code,
                                       IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_DISCONNECT_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_DISCONNECT_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_get_connection_state
 *@param session_id
 *@param state
**/
ZOO_INT32 DBMA_raise_4A_get_connection_state(IN ZOO_INT32 error_code,
                                                 IN ZOO_BOOL state,
                                                 IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_GET_CONNECTION_STATE_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_GET_CONNECTION_STATE_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.get_connection_state_rep_msg.state,&state,sizeof(ZOO_BOOL));
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_create_table
 *@param session_id
 *@param table_name
 *@param primary_key_name
 *@param fields
 *@param fields_number
**/
ZOO_INT32 DBMA_raise_4A_create_table(IN ZOO_INT32 error_code,
                                         IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_CREATE_TABLE_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_CREATE_TABLE_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_drop_table
 *@param session_id
 *@param table_name
**/
ZOO_INT32 DBMA_raise_4A_drop_table(IN ZOO_INT32 error_code,
                                       IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_DROP_TABLE_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_DROP_TABLE_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_update_data
 *@param session_id
 *@param table_name
 *@param condition
 *@param new_datas
 *@param new_datas_number
**/
ZOO_INT32 DBMA_raise_4A_update_data(IN ZOO_INT32 error_code,
                                        IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_UPDATE_DATA_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_UPDATE_DATA_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_delete_data_by_condition
 *@param session_id
 *@param table_name
 *@param condition
**/
ZOO_INT32 DBMA_raise_4A_delete_data_by_condition(IN ZOO_INT32 error_code,
                                                     IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_DELETE_DATA_BY_CONDITION_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_DELETE_DATA_BY_CONDITION_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_delete_all_data
 *@param session_id
 *@param table_name
**/
ZOO_INT32 DBMA_raise_4A_delete_all_data(IN ZOO_INT32 error_code,
                                            IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_DELETE_ALL_DATA_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_DELETE_ALL_DATA_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_insert_data
 *@param session_id
 *@param table_name
 *@param new_data
 *@param fields_number
**/
ZOO_INT32 DBMA_raise_4A_insert_data(IN ZOO_INT32 error_code,
                                        IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_INSERT_DATA_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_INSERT_DATA_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_get_data
 *@param session_id
 *@param record
**/
ZOO_INT32 DBMA_raise_4A_get_data(IN ZOO_INT32 error_code,
                                     IN DB4A_RECORD_STRUCT *record,
                                     IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_GET_DATA_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_GET_DATA_CODE;
                reply_message->reply_header.execute_result = error_code;
                reply_message->reply_body.get_data_rep_msg.record.fields_number = record->fields_number;
                memcpy(reply_message->reply_body.get_data_rep_msg.record.field_list,record->field_list,sizeof(DB4A_FIELD_DATA_STRUCT)*record->fields_number);
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_get_row_size
 *@param session_id
 *@param row_size
**/
ZOO_INT32 DBMA_raise_4A_get_row_size(IN ZOO_INT32 error_code,
                                         IN ZOO_INT32 row_size,
                                         IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_GET_ROW_SIZE_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_GET_ROW_SIZE_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.get_row_size_rep_msg.row_size,&row_size,sizeof(ZOO_UINT32));
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_query_all
 *@param session_id
 *@param table_name
**/
ZOO_INT32 DBMA_raise_4A_query_all(IN ZOO_INT32 error_code,
                                      IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_QUERY_ALL_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_QUERY_ALL_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_query_by_condition
 *@param session_id
 *@param table_name
 *@param fields
 *@param fields_number
 *@param condition
**/
ZOO_INT32 DBMA_raise_4A_query_by_condition(IN ZOO_INT32 error_code,
                                               IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_QUERY_BY_CONDITION_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_QUERY_BY_CONDITION_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_begin_transaction
 *@param session_id
**/
ZOO_INT32 DBMA_raise_4A_begin_transaction(IN ZOO_INT32 error_code,
                                              IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_BEGIN_TRANSACTION_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_BEGIN_TRANSACTION_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_commit_transaction
 *@param session_id
**/
ZOO_INT32 DBMA_raise_4A_commit_transaction(IN ZOO_INT32 error_code,
                                               IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_COMMIT_TRANSACTION_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_COMMIT_TRANSACTION_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief DBMA_raise_4A_rollback_transaction
 *@param session_id
**/
ZOO_INT32 DBMA_raise_4A_rollback_transaction(IN ZOO_INT32 error_code,
                                                 IN DB4I_REPLY_HANDLE reply_handle)
{
    DB4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = DB4A_PARAMETER_ERR;        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = DB4I_get_reply_message_length(DB4A_ROLLBACK_TRANSACTION_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (DB4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = DB4A_ROLLBACK_TRANSACTION_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = DB4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = DB4A_SYSTEM_ERR;                    
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}


