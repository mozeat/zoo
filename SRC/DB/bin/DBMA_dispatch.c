/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : DBMA_dispatch.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-11-01    Generator      created
*************************************************************/

#include "DBMA_dispatch.h"
#include "DBMA_implement.h"

/**
 *@brief DBMA_local_4A_connect
 *@param option
 *@param session_id
**/
static ZOO_INT32 DBMA_local_4A_connect(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_connect(&request->request_body.connect_req_msg.option,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_disconnect
 *@param session_id
**/
static ZOO_INT32 DBMA_local_4A_disconnect(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_disconnect(request->request_body.disconnect_req_msg.session_id,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_get_connection_state
 *@param session_id
 *@param state
**/
static ZOO_INT32 DBMA_local_4A_get_connection_state(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_get_connection_state(request->request_body.get_connection_state_req_msg.session_id,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_create_table
 *@param session_id
 *@param table_name
 *@param primary_key_name
 *@param fields
 *@param fields_number
**/
static ZOO_INT32 DBMA_local_4A_create_table(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_create_table(request->request_body.create_table_req_msg.session_id,
                                           request->request_body.create_table_req_msg.table_name,
                                           request->request_body.create_table_req_msg.primary_key_name,
                                           request->request_body.create_table_req_msg.fields,
                                           request->request_body.create_table_req_msg.fields_number,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_drop_table
 *@param session_id
 *@param table_name
**/
static ZOO_INT32 DBMA_local_4A_drop_table(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_drop_table(request->request_body.drop_table_req_msg.session_id,
                                           request->request_body.drop_table_req_msg.table_name,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_update_data
 *@param session_id
 *@param table_name
 *@param condition
 *@param new_datas
 *@param new_datas_number
**/
static ZOO_INT32 DBMA_local_4A_update_data(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_update_data(request->request_body.update_data_req_msg.session_id,
                                           request->request_body.update_data_req_msg.table_name,
                                           request->request_body.update_data_req_msg.condition,
                                           request->request_body.update_data_req_msg.new_datas,
                                           request->request_body.update_data_req_msg.new_datas_number,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_delete_data_by_condition
 *@param session_id
 *@param table_name
 *@param condition
**/
static ZOO_INT32 DBMA_local_4A_delete_data_by_condition(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_delete_data_by_condition(request->request_body.delete_data_by_condition_req_msg.session_id,
                                           request->request_body.delete_data_by_condition_req_msg.table_name,
                                           request->request_body.delete_data_by_condition_req_msg.condition,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_delete_all_data
 *@param session_id
 *@param table_name
**/
static ZOO_INT32 DBMA_local_4A_delete_all_data(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_delete_all_data(request->request_body.delete_all_data_req_msg.session_id,
                                           request->request_body.delete_all_data_req_msg.table_name,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_insert_data
 *@param session_id
 *@param table_name
 *@param new_data
 *@param fields_number
**/
static ZOO_INT32 DBMA_local_4A_insert_data(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_insert_data(request->request_body.insert_data_req_msg.session_id,
                                           request->request_body.insert_data_req_msg.table_name,
                                           request->request_body.insert_data_req_msg.new_data,
                                           request->request_body.insert_data_req_msg.fields_number,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_get_data
 *@param session_id
 *@param record
**/
static ZOO_INT32 DBMA_local_4A_get_data(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_get_data(request->request_body.get_data_req_msg.session_id,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_get_row_size
 *@param session_id
 *@param row_size
**/
static ZOO_INT32 DBMA_local_4A_get_row_size(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_get_row_size(request->request_body.get_row_size_req_msg.session_id,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_query_all
 *@param session_id
 *@param table_name
**/
static ZOO_INT32 DBMA_local_4A_query_all(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_query_all(request->request_body.query_all_req_msg.session_id,
                                           request->request_body.query_all_req_msg.table_name,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_query_by_condition
 *@param session_id
 *@param table_name
 *@param fields
 *@param fields_number
 *@param condition
**/
static ZOO_INT32 DBMA_local_4A_query_by_condition(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_query_by_condition(request->request_body.query_by_condition_req_msg.session_id,
                                           request->request_body.query_by_condition_req_msg.table_name,
                                           request->request_body.query_by_condition_req_msg.fields,
                                           request->request_body.query_by_condition_req_msg.fields_number,
                                           request->request_body.query_by_condition_req_msg.condition,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_begin_transaction
 *@param session_id
**/
static ZOO_INT32 DBMA_local_4A_begin_transaction(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_begin_transaction(request->request_body.begin_transaction_req_msg.session_id,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_commit_transaction
 *@param session_id
**/
static ZOO_INT32 DBMA_local_4A_commit_transaction(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_commit_transaction(request->request_body.commit_transaction_req_msg.session_id,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief DBMA_local_4A_rollback_transaction
 *@param session_id
**/
static ZOO_INT32 DBMA_local_4A_rollback_transaction(const MQ4A_SERV_ADDR server,DB4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    DB4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (DB4I_REPLY_HANDLE)MM4A_malloc(sizeof(DB4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = DB4A_SYSTEM_ERR;            
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        DBMA_implement_4A_rollback_transaction(request->request_body.rollback_transaction_req_msg.session_id,
                                           reply_handle);
    }
    return rtn;
}

/**
*@brief Dispatch message from client to server internal interface
*@param context        
*@param server        address
*@param msg           request message to server
*@param len           request message length
*@param reply_msg     reply message length to caller
*@param reply_msg_len reply message length
**/
void DBMA_callback_handler(void * context,const MQ4A_SERV_ADDR server,void * msg,ZOO_UINT32 msg_id)
{
    ZOO_INT32 rtn = OK;
    ZOO_INT32 rep_length = 0;
    DB4I_REQUEST_STRUCT *request = (DB4I_REQUEST_STRUCT*)msg;
    DB4I_REPLY_STRUCT *reply = NULL;
    if(request == NULL)
    {
        rtn = DB4A_SYSTEM_ERR;        
        return;
    }

    if(OK == rtn)
    {        
        switch(request->request_header.function_code)
        {
            case DB4A_CONNECT_CODE:
                DBMA_local_4A_connect(server,request,msg_id);
                break;
            case DB4A_DISCONNECT_CODE:
                DBMA_local_4A_disconnect(server,request,msg_id);
                break;
            case DB4A_GET_CONNECTION_STATE_CODE:
                DBMA_local_4A_get_connection_state(server,request,msg_id);
                break;
            case DB4A_CREATE_TABLE_CODE:
                DBMA_local_4A_create_table(server,request,msg_id);
                break;
            case DB4A_DROP_TABLE_CODE:
                DBMA_local_4A_drop_table(server,request,msg_id);
                break;
            case DB4A_UPDATE_DATA_CODE:
                DBMA_local_4A_update_data(server,request,msg_id);
                break;
            case DB4A_DELETE_DATA_BY_CONDITION_CODE:
                DBMA_local_4A_delete_data_by_condition(server,request,msg_id);
                break;
            case DB4A_DELETE_ALL_DATA_CODE:
                DBMA_local_4A_delete_all_data(server,request,msg_id);
                break;
            case DB4A_INSERT_DATA_CODE:
                DBMA_local_4A_insert_data(server,request,msg_id);
                break;
            case DB4A_GET_DATA_CODE:
                DBMA_local_4A_get_data(server,request,msg_id);
                break;
            case DB4A_GET_ROW_SIZE_CODE:
                DBMA_local_4A_get_row_size(server,request,msg_id);
                break;
            case DB4A_QUERY_ALL_CODE:
                DBMA_local_4A_query_all(server,request,msg_id);
                break;
            case DB4A_QUERY_BY_CONDITION_CODE:
                DBMA_local_4A_query_by_condition(server,request,msg_id);
                break;
            case DB4A_BEGIN_TRANSACTION_CODE:
                DBMA_local_4A_begin_transaction(server,request,msg_id);
                break;
            case DB4A_COMMIT_TRANSACTION_CODE:
                DBMA_local_4A_commit_transaction(server,request,msg_id);
                break;
            case DB4A_ROLLBACK_TRANSACTION_CODE:
                DBMA_local_4A_rollback_transaction(server,request,msg_id);
                break;
            default:                
                rtn = DB4A_SYSTEM_ERR;
                break;
        }
    }

    if(OK != rtn && request->request_header.need_reply)
    {
        rtn = DB4I_get_reply_message_length(request->request_header.function_code, &rep_length);
        if(OK != rtn)
        {            
            rtn = DB4A_SYSTEM_ERR;
        }

        if(OK == rtn)
        {
            reply = (DB4I_REPLY_STRUCT *)MM4A_malloc(rep_length);
            if(reply == NULL)
            {                
                rtn = DB4A_SYSTEM_ERR;
            }
        }

        if(OK == rtn)
        {
            memset(reply, 0x0, rep_length);
            reply->reply_header.execute_result = rtn;
            reply->reply_header.function_code = request->request_header.function_code;
            DB4I_send_reply_message(server,msg_id,reply);
        }
    }
    return;
}

