/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : LOG_MANAGER_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "LOG_MANAGER_CLASS.h"
#include <iostream>
#include <sstream>
namespace ZOO_EH
{

    static const ZOO_BOOL ENABLE_DEBUG_MODE = ZOO_TRUE;
    LOG_MANAGER_CLASS::LOG_MANAGER_CLASS()
    {
        //ctor
    }

    LOG_MANAGER_CLASS::~LOG_MANAGER_CLASS()
    {
        //dtor
    }

    /**
     *@brief Initialize EH log path,
     * add sink to the boost log module
    **/
    void LOG_MANAGER_CLASS::initialize()
    {
    	
    }

    /**
     *@brief Write message to the EH file
     *@param file             the exception file
     *@param function_name    the exception function location
     *@param line             the exception line in the file
     *@param error_code
     *@param link_error_code  the error code
     *@param info             the exception message
    **/
    void LOG_MANAGER_CLASS::show_exception(IN const ZOO_CHAR* component_id,
                               IN const ZOO_CHAR* file,
                               IN const ZOO_CHAR* function_name,
                               IN ZOO_INT32 line,
                               IN ZOO_INT32 error_code,
                               IN ZOO_INT32 link_error_code,
                               IN const ZOO_CHAR* info)
    {
        std::ostringstream os;
        os << "[" << component_id << "]["  << file << "]" << "[line:" << line << "]:\n";
		os << "@Description:\n" ;
        os << "  - Erorr code: "    << "0x" << std::hex  << error_code      << "\n";
        os << "  - Link error code:" << "0x" << std::hex  << link_error_code << "\n";
        os << "  - Message: " << info;
		ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,function_name,std::move(os.str()).c_str());
    }

	/**
     *@brief print to file
     *@param message  the exception message
    **/
    void LOG_MANAGER_CLASS::log(const char* function_name,const char* format, ...)
    {
        char  var_str[256];
        va_list	ap;
        va_start(ap, format);
        vsnprintf(var_str,256,format, ap);
        var_str[255] = '\0';
        va_end(ap);
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,function_name,var_str);
    }

    /**
     *@brief print to screen
    **/
    void LOG_MANAGER_CLASS::debug(const char* function_name,const char* format, ...)
    {
        if(ENABLE_DEBUG_MODE)
        {
            char  var_str[256];
            va_list	ap;
            va_start(ap, format);
            vsnprintf(var_str,256,format, ap);
            var_str[255] = '\0';
            va_end(ap);
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,function_name,var_str);
        }
    }
}
