/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : ALARM_DAO_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "ALARM_DAO_CLASS.h"
#include <boost/algorithm/string.hpp>
namespace ZOO_EH
{
    ALARM_DAO_CLASS::ALARM_DAO_CLASS(std::string db_name)
        :m_opened(ZOO_FALSE),m_db_name(db_name),m_table_name("Alarms")
        ,m_sqlite_dao(*soci::factory_sqlite3())
    {
        //ctor
    }

    ALARM_DAO_CLASS::~ALARM_DAO_CLASS()
    {
        //dtor
    }

    void ALARM_DAO_CLASS::set_db_name(std::string name)
    {
        this->m_db_name = name;
    }

    /**
     *@brief open db
    **/
    void ALARM_DAO_CLASS::open()
    {
        if(this->m_opened)
            return;
        ZOO_CHAR resource_id[128];

        if(OK == ZOO_get_resource_id(resource_id))
        {
            this->m_resource_id = resource_id;
        }
        
        try
        {
            this->m_session.reset(new soci::session(this->m_sqlite_dao,
                                                    this->m_db_name));
            this->m_opened = ZOO_TRUE;
        }
        catch(soci::sqlite3_soci_error & e)
        {
            this->m_opened = ZOO_FALSE;
            __THROW_EH_EXCEPTION(EH4A_SYSTEM_ERR,e.what(),nullptr);
        }
    }



    /**
     *@brief close db
    **/
    void ALARM_DAO_CLASS::close()
    {
        this->m_opened = ZOO_FALSE;
        this->m_session.reset();
    }

    /**
     *@brief Create db table
    **/
    void ALARM_DAO_CLASS::create_table()
    {
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }

        try
        {
            std::string table_name = this->m_table_name;
            auto & sql_session = *this->m_session.get();
            std::ostringstream os;
            //create
            os << "CREATE TABLE IF NOT EXIST " << this->m_table_name << " (";
            os << "CODE INTERGER PRIMARY KEY,";
            os << "LEVEL INTERGER,";
            os << "COMPONET,VARCHAR(16) NOT NULL,";
            os << "TYPE VARCHAR(16) NOT NULL,";
            os << "MESSAGE TEXT,";
            os << "MATERIAL_ID VARCHAR(40),";
            os << "TRIGGED_TIME VARCHAR(32) NOT NULL,";
            os << "RECOVER_TIME VARCHAR(32),";
            os << "IS_TRIGGED VARCHAR(5) NOT NULL";
            os << " )";
            sql_session << os.str();
        }
        catch(soci::sqlite3_soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }

        try
        {
            std::string table_name = this->m_table_name;
            auto & sql_session = *this->m_session.get();
            std::ostringstream os;
            //create
            os << "CREATE TABLE IF NOT EXIST " << "definitions" << " (";
            os << "CODE INTERGER PRIMARY KEY,";
            os << "LEVEL INTERGER,";
            os << "COMPONET,VARCHAR(16) NOT NULL,";
            os << "TYPE VARCHAR(16) NOT NULL,";
            os << "ENGLISH TEXT,";
            os << "CHINESE TEXT,";
            os << "EXTERNAL_VISIBLE TEXT,";
            os << "EXTERNAL_CODE TEXT,";
            os << " )";
            sql_session << os.str();
        }
        catch(soci::sqlite3_soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
    }

    /**
     *@brief if not present,insert ,or update the data
    **/
    void ALARM_DAO_CLASS::replace(ZOO_INT32 error_code,
                        ZOO_INT32 level,
                        const std::string &componet_id,
                        ZOO_INT32 error_type,
                        const std::string &message,
                        const std::string &material_id,
                        const std::string &trigger_time,
                        const std::string &recover_time,
                        ZOO_BOOL is_trigged)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: error_code:%02x",error_code);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: level:%d",level);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: componet_id:%s",componet_id.c_str());
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: error_type:%d",error_type);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: material_id:%s",material_id.c_str());

        if(is_trigged)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: trigger_time:%s",trigger_time.c_str());
        }
        else
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: recover_time:%s",recover_time.c_str());
        }
        
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }
        
        try
        {
            this->update(error_code,level,componet_id,
                         error_type,message,material_id,
                         trigger_time,recover_time,is_trigged);
        }
        catch(...)
        {
            this->insert(error_code,level,componet_id,
                         error_type,message,material_id,
                         trigger_time,recover_time,is_trigged);
                         
        }
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit ...");
    }

    /**
     *@brief Update alarm db table
    **/
    void ALARM_DAO_CLASS::update(ZOO_INT32 error_code,
                                    ZOO_INT32 level,
                                    const std::string &componet_id,
                                    ZOO_INT32 error_type,
                                    const std::string &message,
                                    const std::string &material_id,
                                    const std::string &trigger_time,
                                    const std::string &recover_time,
                                    ZOO_BOOL is_trigged)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }

        auto & sql_session = *this->m_session.get();
        //soci::transaction transaction(sql_session);

        std::string comp;
        try
        {
            sql_session << "SELECT COMPONET FROM " << this->m_table_name << " WHERE CODE = :error_code",soci::use(error_code),soci::into(comp);
        }
        catch(soci::soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }

        if(!sql_session.got_data())
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"no such alarm id exist in db",nullptr);
        }
        
        try
        {
       
            std::string tigged_state = STRING_UTILITY_CLASS::to_string(is_trigged);
            std::string type = error_type == EH4A_ERROR_TYPE_SW ? "SW":"HW";

            std::ostringstream os;
            os << "UPDATE " << this->m_table_name <<" ";
            os << "SET LEVEL = :level,COMPONET = :componet_id,";
            os << "TYPE = :type,MESSAGE = :message,MATERIAL_ID = :material_id,";
            os << "TRIGGED_TIME = :trigger_time,RECOVER_TIME = :recover_time,IS_TRIGGED =:tigged_state ";
            os << "WHERE CODE = :error_code";

            sql_session << os.str(),soci::use(level),soci::use(componet_id),
                                    soci::use(type),
                                    soci::use(message),
                                    soci::use(material_id),
                                    soci::use(trigger_time),
                                    soci::use(recover_time),
                                    soci::use(tigged_state),
                                    soci::use(error_code);

            //transaction.commit();
        }
        catch(soci::sqlite3_soci_error & e)
        {
            //transaction.rollback();
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit ...");
    }

    /**
     *@brief Update alarm db table
    **/
    void ALARM_DAO_CLASS::insert(ZOO_INT32 error_code,
                                    ZOO_INT32 level,
                                    const std::string &componet_id,
                                    ZOO_INT32 error_type,
                                    const std::string &message,
                                    const std::string &material_id,
                                    const std::string &trigger_time,
                                    const std::string &recover_time,
                                    ZOO_BOOL is_trigged)
    {
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }

        auto & sql_session = *this->m_session.get();
        //soci::transaction transaction(sql_session);
            
        try
        {            
            std::string trigged_state = STRING_UTILITY_CLASS::to_string(is_trigged);
            std::string type = STRING_UTILITY_CLASS::to_string(error_type);
            
            std::ostringstream os;
            os << "INSERT INTO " << this->m_table_name ;
            os << "(CODE, LEVEL, COMPONET, TYPE, MESSAGE, MATERIAL_ID, TRIGGED_TIME, RECOVER_TIME, IS_TRIGGED, RESOURCE_ID)";
            os << " VALUES(:error_code, :level, :componet_id, :type, :message, :material_id, :trigger_time, :recover_time, :trigged_state,:resource_id)";
            sql_session << os.str(),soci::use(error_code),soci::use(level),soci::use(componet_id),
                            soci::use(type),soci::use(message),soci::use(material_id),soci::use(trigger_time),
                            soci::use(recover_time),soci::use(trigged_state),soci::use(this->m_resource_id);
            //transaction.commit();
        }
        catch(std::exception & e)
        {
            //transaction.rollback();
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
        catch(...)
        {
            
        }
    }

    /**
     *@brief Truncate table
    **/
    void ALARM_DAO_CLASS::truncate_table()
    {
        try
        {
            if(this->m_session == nullptr)
            {
                __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
            }
            std::ostringstream os;
            os << "DELETE FROM " << this->m_table_name <<" WHERE 1=1";
            auto & sql_session = *this->m_session.get();
            sql_session << os.str();
        }
        catch(soci::sqlite3_soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
    }

    /**
     *@brief Drop table
    **/
    void ALARM_DAO_CLASS::drop_table()
    {
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }

        try
        {
            std::ostringstream os;
            os << "DROP TABLE " << this->m_table_name <<" IF EXISTS";
            auto & sql_session = *this->m_session.get();
            sql_session << os.str();
        }
        catch(soci::soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
    }

    /**
     *@brief Delete table
    **/
    void ALARM_DAO_CLASS::remove(ZOO_INT32 error_code)
    {
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }

        try
        {
            std::ostringstream os;
            os << "DELETE FROM " << this->m_table_name <<" WHERE CODE = :error_code ";
            auto & sql_session = *this->m_session.get();
            sql_session << os.str(),soci::use(error_code);
        }
        catch(soci::soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
    }

    /**
     *@brief Get data from device dao
    **/
    boost::shared_ptr<ERROR_MESSAGE_CLASS> ALARM_DAO_CLASS::query(ZOO_INT32 error_code)
    {
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }
        boost::shared_ptr<ERROR_MESSAGE_CLASS> message;
        try
        {
            message.reset(new ERROR_MESSAGE_CLASS());
            std::ostringstream os;
            std::string type,componet,decription,material_id,trigged_time,recover_time,english,chinese;
            std::string is_trigged = "FALSE";
            ZOO_INT32 level = EH4A_ERROR_LEVEL_MAX;
            os << "SELECT CODE, COMPONET, TYPE, MESSAGE, MATERIAL_ID, TRIGGED_TIME, RECOVER_TIME, IS_TRIGGED FROM " ;
            os << this->m_table_name <<" WHERE CODE = :error_code";
            auto & sql_session = *this->m_session.get();
            sql_session << os.str(),soci::into(level),soci::into(componet),soci::into(type),soci::into(decription),
                            soci::into(material_id),soci::into(trigged_time),soci::into(recover_time),
                            soci::into(is_trigged),soci::use(error_code);
            message->set_alarm_id(error_code);
            message->set_alarm_type(STRING_UTILITY_CLASS::to_enum(type.c_str()));
            message->set_alarm_level(level);
            message->set_componet_id(componet);
            message->set_material_id(material_id);
            message->set_trigger_time(trigged_time);
            message->set_recover_time(recover_time);
            message->set_trigged_state(STRING_UTILITY_CLASS::bool_string_to_int(is_trigged.c_str()));
        }
        catch(soci::soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
        return message;
    }
    
    /**
     *@brief DAO has opened
    **/
    ZOO_BOOL ALARM_DAO_CLASS::is_opened()
    {
        return this->m_opened;
    }

    std::vector<EH4A_ALARM_STRUCT *> ALARM_DAO_CLASS::read_all_trigged_datas()
    {
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "> function entry ...");
        std::vector<EH4A_ALARM_STRUCT *> records;
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }
        
        try
        {           
            std::ostringstream os;
            std::string type,componet,decription,material_id,trigged_time,recover_time,english,chinese;
            os << "SELECT * FROM " << this->m_table_name ;
            auto & sql_session = *this->m_session.get();
            soci::rowset<soci::row> rs = sql_session.prepare << os.str();
            for(const soci::row & r : rs)
            {
                ZOO_BOOL is_trigged = r.get<std::string>(8) == "TRUE" ? ZOO_TRUE:ZOO_FALSE;
                if(is_trigged)
                {
                    EH4A_ALARM_STRUCT * message = new EH4A_ALARM_STRUCT();   
                    message->alarm_id = r.get<ZOO_INT32>(0);
                    if(message->alarm_id == 0)
                    {
                        delete message;
                        continue;
                    } 
                    message->level = static_cast<EH4A_ERROR_LEVEL_ENUM>(r.get<ZOO_INT32>(1)) ;
                    memcpy(message->component,r.get<std::string>(2).data(),r.get<std::string>(2).size() + 1);
                    message->type = STRING_UTILITY_CLASS::to_enum(r.get<std::string>(3).data());
                    memcpy(message->material_id,r.get<std::string>(5).data(),r.get<std::string>(5).size() + 1);
                    memcpy(message->trigger_time,r.get<std::string>(6).data(),r.get<std::string>(6).size() + 1);
                    memcpy(message->recover_time,r.get<std::string>(7).data(),r.get<std::string>(7).size() + 1);
                    message->is_triggered = is_trigged;
                    records.emplace_back(message);
                    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: trigger_time:%s",message->trigger_time);
                    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: recover_time:%s",message->recover_time);
                }
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: id:%02x",r.get<ZOO_INT32>(0));
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: is_trigged:%d",is_trigged);
            }
        }
        catch(soci::soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
        
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...");
        return records;
    }
    
    std::vector<EH4A_ALARM_STRUCT *> ALARM_DAO_CLASS::read_all_recovered_datas()
    {
        std::vector<EH4A_ALARM_STRUCT *> records;
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }
        
        try
        {           
            std::ostringstream os;
            std::string type,componet,decription,material_id,trigged_time,recover_time,english,chinese;
            os << "SELECT * FROM " << this->m_table_name ;
            auto & sql_session = *this->m_session.get();
            soci::rowset<soci::row> rs = sql_session.prepare << os.str();
            for(const soci::row & r : rs)
            {
                ZOO_BOOL is_trigged = r.get<std::string>(8) == "TRUE" ? ZOO_TRUE:ZOO_FALSE;
                ZOO_INT32 alarm_id = r.get<ZOO_INT32>(0);
                if(!is_trigged && alarm_id != 0)
                {
                    EH4A_ALARM_STRUCT * message = new EH4A_ALARM_STRUCT();   
                    message->alarm_id = alarm_id;
                    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: alarm_id:%02x",message->alarm_id);
                    message->level = static_cast<EH4A_ERROR_LEVEL_ENUM>(r.get<ZOO_INT32>(1));
                    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: level:%d",message->level);
                    memcpy(message->component,r.get<std::string>(2).data(),r.get<std::string>(2).size() + 1);
                    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: component:%s",message->component);
                    message->type = STRING_UTILITY_CLASS::to_enum(r.get<std::string>(3).data());
                    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: type:%d",message->type);
                    memcpy(message->material_id,r.get<std::string>(5).data(),r.get<std::string>(5).size() + 1);
                    memcpy(message->trigger_time,r.get<std::string>(6).data(),r.get<std::string>(6).size() + 1);
                    memcpy(message->recover_time,r.get<std::string>(7).data(),r.get<std::string>(7).size() + 1);
                    message->is_triggered = is_trigged;
                    records.emplace_back(message);
                }
            }
        }
        catch(soci::sqlite3_soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
        return records;
    }
}
