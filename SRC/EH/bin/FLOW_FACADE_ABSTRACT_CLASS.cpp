/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : FLOW_FACADE_ABSTRACT_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "FLOW_FACADE_ABSTRACT_CLASS.h"
#include <converters/CONVERTER_CLASS.h>
#include <ALARM_DAO_CLASS.h>
#include <time.h>
namespace ZOO_EH
{
    FLOW_FACADE_ABSTRACT_CLASS::FLOW_FACADE_ABSTRACT_CLASS()
    {
        //ctor
    }

    FLOW_FACADE_ABSTRACT_CLASS::~FLOW_FACADE_ABSTRACT_CLASS()
    {
        //dtor
    }

    /**
     *@brief Set device controller
     *@param device_controller
    **/
    void FLOW_FACADE_ABSTRACT_CLASS::set_device_controller(boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE> device_controller)
    {
        this->m_device_controller = device_controller;
    }

    /**
     *@brief Set alarm dao
     *@param alarm_dao
    **/
    void FLOW_FACADE_ABSTRACT_CLASS::set_alarm_dao(boost::shared_ptr<DAO_INTERFACE> alarm_dao)
    {
        this->m_alarm_dao = alarm_dao;
    }

    boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE> FLOW_FACADE_ABSTRACT_CLASS::get_device_controller()
    {
        return this->m_device_controller;
    }

    boost::shared_ptr<DAO_INTERFACE> FLOW_FACADE_ABSTRACT_CLASS::get_alarm_dao()
    {
        return this->m_alarm_dao;
    }

    void FLOW_FACADE_ABSTRACT_CLASS::set_log_manager(IN boost::shared_ptr<LOG_MANAGER_INTERFACE> log_manager)
    {
        this->m_log_manager = log_manager;
    }

    boost::shared_ptr<LOG_MANAGER_INTERFACE> FLOW_FACADE_ABSTRACT_CLASS::get_log_manager()
    {
        return this->m_log_manager;
    }

    /**
     *@brief Notify alarm id state changed,publish message to subscribe,
     *@ update alarm DAO according to alarm id
    **/
    void FLOW_FACADE_ABSTRACT_CLASS::notify_property_changed(IN boost::shared_ptr<DEVICE_INTERFACE> device,
                                                                 IN boost::shared_ptr<DAO_INTERFACE> dao,
                                                                 IN ZOO_INT32 alarm_id,
                                                                 IN ZOO_BOOL alarm_trigged,
                                                                 IN const std::string & material_id)

    {
        if(device != nullptr && dao != nullptr)
        {
            boost::shared_ptr<ERROR_MESSAGE_CLASS> message =
                this->build_alarm_message(device,dao,alarm_id,alarm_trigged,material_id);
            if(message != nullptr)
            {
                this->update_dao(dao,message);
                device->publish(message);
            }
        }
    }

    /**
     *@brief Build publish message
    **/
    boost::shared_ptr<ERROR_MESSAGE_CLASS> FLOW_FACADE_ABSTRACT_CLASS::build_alarm_message(IN boost::shared_ptr<DEVICE_INTERFACE> device,
                                                                         IN boost::shared_ptr<DAO_INTERFACE> dao,
                                                                         IN ZOO_INT32 alarm_id,
                                                                         IN ZOO_BOOL alarm_trigged,
                                                                         IN const std::string & material_id)
    {
        boost::shared_ptr<ERROR_MESSAGE_CLASS> message;
        if(device != nullptr && dao != nullptr)
        {
            message = device->get_alarm_definitions(alarm_id);
            if(message == nullptr)
            {
                message.reset(new ERROR_MESSAGE_CLASS());
                message->set_cloud_enabled(ZOO_FALSE);  
                message->set_alarm_type(EH4A_ERROR_TYPE_SW);
            }
            message->set_alarm_id(alarm_id);
            message->set_material_id(material_id);
            message->set_componet_id(device->get_marking_code());
            message->set_trigged_state(alarm_trigged);
            auto dao_store_message = this->query(dao,alarm_id);
            if(alarm_trigged)
            {
                message->set_trigger_time(this->get_current_time());
                if(dao_store_message != NULL)
                {
                    std::string recover_time = " ";
                    message->set_recover_time(recover_time);
                }
            }
            else
            {
                message->set_recover_time(this->get_current_time());
                if(dao_store_message != NULL)
                {
                    message->set_trigger_time(dao_store_message->get_trigger_time());
                }
            }
        }
        return message;
    }

    /**
     *@brief Get data from DAO
     *@param dao
     *@return null : no alarm in table,else has already in table
    **/
    boost::shared_ptr<ERROR_MESSAGE_CLASS> FLOW_FACADE_ABSTRACT_CLASS::query(IN boost::shared_ptr<DAO_INTERFACE> dao,
                                                                             IN ZOO_INT32 alarm_id)
    {
        if(!dao->is_opened())
        {
            dao->open();
        }
        return dao->query(alarm_id);
    }

    void FLOW_FACADE_ABSTRACT_CLASS::rebuild_alarms(std::vector<EH4A_ALARM_STRUCT * > & alarms)
    {
        for(std::vector<EH4A_ALARM_STRUCT * >::iterator ite = alarms.begin();
            ite != alarms.end() ; ++ite)
        {
            EH4A_ALARM_STRUCT * alarm = *ite;
            ZOO_INT32 alarm_id = alarm->alarm_id;
            auto device = this->m_device_controller->get_device(alarm_id);
            if(device)
            {
                boost::shared_ptr<ERROR_MESSAGE_CLASS> message = device->get_alarm_definitions(alarm_id);
                sprintf(alarm->description_cn,"%s",message->get_chinese_description().data());
                sprintf(alarm->description_en,"%s",message->get_english_description().data());
                alarm->cloud_enabled = message->get_oss_enabled();
                alarm->cloud_error_code = message->get_oss_error_code();
                alarm->type =static_cast<EH4A_ERROR_TYPE_ENUM>(message->get_alarm_type());
            }
        }
    }
    
    /**
     *@brief Update message to dao
    **/
    void FLOW_FACADE_ABSTRACT_CLASS::update_dao(boost::shared_ptr<DAO_INTERFACE> dao,
                                                                    boost::shared_ptr<ERROR_MESSAGE_CLASS> message)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");
        if(message != nullptr && dao != nullptr)
        {
            boost::shared_ptr<ALARM_DAO_CLASS> alarm_db = boost::dynamic_pointer_cast<ALARM_DAO_CLASS>(dao);
            if(!alarm_db->is_opened())
                alarm_db->open();
            alarm_db->replace(message->get_alarm_id(),
            					message->get_alarm_level(),
            					message->get_componet_id(),
                              	message->get_alarm_type(),
                             	message->get_english_description(),
                             	message->get_material_id(),
                              	message->get_trigger_time(),
                             	message->get_recover_time(),
                              	message->get_trigged_state());
        }
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit ...");
    }

    /**
     *@brief Get current date and time
    **/
    std::string FLOW_FACADE_ABSTRACT_CLASS::get_current_time()
    {
        time_t current_time = time(NULL);
        ZOO_CHAR time_string[80];
        ZOO_COMMON::CONVERTER_CLASS::get_instance()->convert_from_time_t_to_string(current_time,time_string);
        time_string[31] = '\0';
        return std::string(time_string);
    }
}
