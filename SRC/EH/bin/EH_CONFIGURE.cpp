/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : EH_CONFIGURE.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "EH_CONFIGURE.h"
#include <boost/thread/once.hpp>
namespace ZOO_EH
{
    /**
     * @brief Call one for instance
     */
    static boost::once_flag g_EH_instance_once_flag = BOOST_ONCE_INIT;

    /**
     * @brief Initialize NULL for static member instance
     */
    boost::shared_ptr<EH_CONFIGURE> EH_CONFIGURE::m_instance = NULL;

    EH_CONFIGURE::EH_CONFIGURE()
    {
        //ctor
    }

    EH_CONFIGURE::~EH_CONFIGURE()
    {
        //dtor
    }

    boost::shared_ptr<EH_CONFIGURE> EH_CONFIGURE::get_instance()
    {
        /**
        * @brief call only once
        */
        boost::call_once([&]{EH_CONFIGURE::m_instance.reset(new EH_CONFIGURE());},g_EH_instance_once_flag);
        return EH_CONFIGURE::m_instance;
    }

    /**
     *@brief Initialize
    **/
    void EH_CONFIGURE::initialize()
    {
        boost::call_once([&]{EH_CONFIGURE::m_instance.reset(new EH_CONFIGURE());},g_EH_instance_once_flag);
        EH_CONFIGURE::m_instance->reload();
    }

    /**
     *@brief Reload
    **/
    void EH_CONFIGURE::reload()
    {
        this->m_execute_path = ZOO_COMMON::ENVIRONMENT_UTILITY_CLASS::get_execute_path();
        if(OK != ZOO_get_user_files_path(&this->m_user_path))
        {
            
		}
    }

    /**
     *@brief Current execute path
    **/
    std::string EH_CONFIGURE::get_execute_path()
    {
        return this->m_execute_path;
    }

    /**
     *@brief Alarm db path
    **/
    std::string EH_CONFIGURE::get_alarm_db_path()
    {
        return this->m_user_path.output.db;
    }

    /**
     *@brief Device db path
    **/
    std::string EH_CONFIGURE::get_device_db_path()
    {
        return this->m_user_path.configure.error_code;
    }

    /**
     *@brief Task configuratiion db path
    **/
    std::vector<ZOO_TASK_STRUCT *> EH_CONFIGURE::get_tasks()
    {
        std::vector<ZOO_TASK_STRUCT *> tasks;
        for(int i = 0; i < this->m_cfg_tasks.number ; i++)
        {
            tasks.push_back(&this->m_cfg_tasks.task[i]);
        }    
		return tasks;
    }

	/**
     *@brief log configure path
    **/
    std::string EH_CONFIGURE::get_log_path()
	{
		return this->m_user_path.output.log;
	}
}

