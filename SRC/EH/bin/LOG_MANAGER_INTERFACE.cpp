/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : LOG_MANAGER_INTERFACE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "LOG_MANAGER_INTERFACE.h"

namespace ZOO_EH
{
    LOG_MANAGER_INTERFACE::LOG_MANAGER_INTERFACE()
    {
        //ctor
    }

    LOG_MANAGER_INTERFACE::~LOG_MANAGER_INTERFACE()
    {
        //dtor
    }
}
