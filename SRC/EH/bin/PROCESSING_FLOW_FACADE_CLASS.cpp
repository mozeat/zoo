/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : PROCESSING_FLOW_FACADE_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "PROCESSING_FLOW_FACADE_CLASS.h"
#include "ALARM_DAO_CLASS.h"
#include "DEVICE_CONTROLLER_CLASS.h"
#include "LOG_MANAGER_CLASS.h"
#include "STRING_UTILITY_CLASS.h"
#include "SESSION_MANAGER_CLASS.h"
namespace ZOO_EH
{
    PROCESSING_FLOW_FACADE_CLASS::PROCESSING_FLOW_FACADE_CLASS()
    {
        
    }

    PROCESSING_FLOW_FACADE_CLASS::~PROCESSING_FLOW_FACADE_CLASS()
    {
        //dtor
    }

    /**
     *@brief show exception on the HMI,but not set alarm tower
     *@param component_id
     *@param file
     *@param function_name
     *@param line
     *@param error_code
     *@param link_error_code
     *@param info
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::show_exception(IN const ZOO_CHAR* component_id,
                                                               IN const ZOO_CHAR* file,
                                                               IN const ZOO_CHAR* function_name,
                                                               IN ZOO_INT32 line,
                                                               IN ZOO_INT32 error_code,
                                                               IN ZOO_INT32 link_error_code,
                                                               IN const ZOO_CHAR* info)
    {
        ZOO_INT32 result = OK;

        __EH_TRY
        {
            
            this->get_log_manager()->show_exception(component_id,file,function_name,
                               					line,error_code,link_error_code,info);                					
        }
        __EH_CATCH_ALL(result)

        return result;
    }

    /*
     * @brief EH4A_set_notification
     *@param info
     **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::set_notification(IN EH4A_NOTIFY_INFO_STRUCT * info)
    {
        EHMA_raise_4A_notify_info_subscribe(info,OK,NULL);
        return OK;
    }

    /**
     *@brief Set alarm signal tower
     *@param alarm_id
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::invoke_alarm(IN const ZOO_CHAR * material_id,
    															IN ZOO_INT32 alarm_id)
    {
        ZOO_INT32 result = OK;
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"material_id:%s,alarm_id:%x",material_id,alarm_id);
        __EH_TRY
        {
            auto alarm_dao =
                boost::dynamic_pointer_cast<ALARM_DAO_CLASS>(this->get_alarm_dao());
            if(alarm_dao == nullptr)
            {
                __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"DAO is not create ...ERROR",nullptr);
            }

            auto device_controller =
                boost::dynamic_pointer_cast<DEVICE_CONTROLLER_CLASS>(this->get_device_controller());
            if(device_controller == nullptr)
            {
                __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"device_controller is not create ...ERROR",nullptr);
            }

            auto device = device_controller->get_device(alarm_id);
            if(device == nullptr)
            {
                __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"no such alarm device defined ...ERROR",nullptr);
            }

            this->notify_property_changed(device,alarm_dao,
                                          alarm_id,ZOO_TRUE,material_id);
        }
        __EH_CATCH_ALL(result)
        
        return result;
    }

    /**
     *@brief Clear the signal tower
     *@param alarm_id
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::clear_alarm(IN ZOO_INT32 alarm_id)
    {
        ZOO_INT32 result = OK;
        __EH_TRY
        {
            auto alarm_dao =
                    boost::dynamic_pointer_cast<ALARM_DAO_CLASS>(this->get_alarm_dao());
            if(alarm_dao == nullptr)
            {
                __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"DAO is not create ...ERROR",nullptr);
            }

            auto device_controller =
                    boost::dynamic_pointer_cast<DEVICE_CONTROLLER_CLASS>(this->get_device_controller());
            if(device_controller == nullptr)
            {
                __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"device_controller is not create ...ERROR",nullptr);
            }

            auto device = device_controller->get_device(alarm_id);
            if(device == nullptr)
            {
                __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"no such alarm device define ...ERROR",nullptr);
            }

            auto alarm_table = alarm_dao->query(alarm_id);
            if(alarm_table != nullptr)
            {
                auto material_id = alarm_table->get_material_id();
                this->notify_property_changed(device,alarm_dao,
                                              alarm_id,ZOO_FALSE,material_id);
            }
        }
        __EH_CATCH_ALL(result)

        return result;
    }

    /**
     *@brief Clear all alarm,reset signal tower
     *@param alarm_id
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::clear_all_alarms()
    {
        ZOO_INT32 result = OK;

        __EH_TRY
        {
            auto alarm_dao = this->get_alarm_dao();
            if(alarm_dao == nullptr)
            {
                __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"DAO is not create ...ERROR",nullptr);
            }
            alarm_dao->truncate_table();
        }
        __EH_CATCH_ALL(result)

        return result;
    }

    /**
	 * @brief Find configure alarm points
	 */
	ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::find_alarm_counts(IN EH4A_FOUND_TYPE_ENUM found_type,
		                                                        IN ZOO_INT32 * counts,
		                                                        IN ZOO_INT32 * session_id)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "> function entry ...");
        ZOO_INT32 result = OK;
        * session_id = -1;
        * counts = 0;
        std::vector<EH4A_ALARM_STRUCT * > records;
        __EH_TRY
        {
            if(found_type >= EH4A_FOUND_TYPE_MAX || found_type <= EH4A_FOUND_TYPE_MIN)
            {
                __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR, "found type out of range", NULL);
            }
        
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,
                                ":: found_type is %s",
                                STRING_UTILITY_CLASS::found_type_to_string(found_type).data());                                    	
            switch(found_type)
            {
                case EH4A_FOUND_TYPE_ALL:
                {
                    auto device_controller = this->get_device_controller();            	
                    records = device_controller->find_all_devices_alarm_datas();
                    this->rebuild_alarms(records);
                    *counts = records.size();
                    break;
                }
                case EH4A_FOUND_TYPE_TRIGGED:
                {
                    boost::shared_ptr<ALARM_DAO_CLASS> alarm_dao =
                        boost::dynamic_pointer_cast<ALARM_DAO_CLASS>(this->get_alarm_dao());
                    if(alarm_dao == nullptr)
                    {
                        __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"DAO is not create ...ERROR",nullptr);
                    }
                    records = alarm_dao->read_all_trigged_datas();
                    this->rebuild_alarms(records);
                    *counts = records.size();
                    break;
                }
                case EH4A_FOUND_TYPE_RECOVERED:
                {
                    boost::shared_ptr<ALARM_DAO_CLASS> alarm_dao =
                        boost::dynamic_pointer_cast<ALARM_DAO_CLASS>(this->get_alarm_dao());
                    if(alarm_dao == nullptr)
                    {
                        __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"DAO is not create ...ERROR",nullptr);
                    }
                    records = alarm_dao->read_all_recovered_datas();
                    this->rebuild_alarms(records);
                    *counts = records.size();
                    break;
                }
                default:
                    result = EH4A_PARAMETER_ERR;;
                    *counts = 0;
                    break;
            }

            if(!records.empty())
            {
                boost::shared_ptr<SESSION_MANAGER_CLASS> session_manager = SESSION_MANAGER_CLASS::get_instance();
                session_manager->add_session(records,session_id);
                ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,":: create session_id : %d",*session_id);
            }
        }
        catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & e) 
        { 
            ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__, ":: %s",e.what()); 
            result = e.get_error_code();
            for(std::vector<EH4A_ALARM_STRUCT * >::iterator ite = records.begin();
                ite !=  records.end();++ite)
            {
                if(*ite)
                {
                    delete *ite;
                }
            }
        }
        catch(...)
        {
            for(std::vector<EH4A_ALARM_STRUCT * >::iterator ite = records.begin();
                ite !=  records.end();++ite)
            {
                if(*ite)
                {
                    delete *ite;
                }
            }
            result = EH4A_SYSTEM_ERR;
        }
        
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: find count is %d",*counts);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...error_code[%d]",result);
        return result;
    }

	/**
	 * @brief get alarm data
	 */
	ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::get_alarm_data(IN ZOO_INT32 session_id,
                    		                                               IN ZOO_INT32 index,
                    													   INOUT EH4A_ALARM_STRUCT * data)
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "> function entry ...");
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: get data use count is %d",index);
        ZOO_INT32 result = OK;
        boost::shared_ptr<SESSION_MANAGER_CLASS> session_manager = SESSION_MANAGER_CLASS::get_instance();
        boost::shared_ptr<SESSION_CLASS> session = session_manager->get_session(session_id);
        if(NULL == session)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING, __ZOO_FUNC__, ":: invalid session id[%d]",session_id);
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...");
            return EH4A_PARAMETER_ERR;
        }

        std::vector<EH4A_ALARM_STRUCT * > records = session->get_cache();
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: total count is %lu",records.size());
        EH4A_ALARM_STRUCT * alarm = session->get_cache(index);
        if(NULL == alarm)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING, __ZOO_FUNC__, ":: invalid index[%d]",index);
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...");
            return EH4A_PARAMETER_ERR;
        }
       	memcpy(data,alarm,sizeof(EH4A_ALARM_STRUCT));

       	if(static_cast<unsigned int>(index + 1) == records.size())
       	{
       	    session->clear();
       	    session_manager->remove_session(session_id);
       	}
       	
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...");
        return result;
    }

    /**
     *@brief Get alarm informations from configure and alarm subscribe table
     *@param alarm_id
     *@param alarm_info detail description about the alarm id
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::get_alarm_information(IN ZOO_INT32 alarm_id,
                                                            OUT EH4A_ALARM_DETAILS_STRUCT *alarm_info)
    {
        ZOO_INT32 result = OK;
        __EH_TRY
        {
            auto device_controller = this->get_device_controller();
            if(device_controller == nullptr)
            {
                __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"device pointer is null ...ERROR",nullptr);
            }
            boost::shared_ptr<ERROR_MESSAGE_CLASS> configure_message;
            configure_message = device_controller->get_alarm_definitions(alarm_id);
            if(configure_message == nullptr)
            {
                __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"configure_message pointer is null ...ERROR",nullptr);
            }
            alarm_info->type = static_cast<EH4A_ERROR_TYPE_ENUM>(configure_message->get_alarm_type());
            alarm_info->level = static_cast<EH4A_ERROR_LEVEL_ENUM>(configure_message->get_alarm_level());
            std::string chinese = configure_message->get_chinese_description();
            memcpy(alarm_info->description_cn,chinese.c_str(),chinese.size());
            alarm_info->description_cn[chinese.size()] = '\0';
            
            std::string english = configure_message->get_english_description();
            memcpy(alarm_info->description_en,english.c_str(),english.size());
            alarm_info->description_en[english.size()] = '\0';
            
            std::string componet = ERROR_CODE_CONVERTER_CLASS::convert_to_componet_id(alarm_id);
            memcpy(alarm_info->component,componet.data(),componet.size());
            alarm_info->component[componet.size()] = '\0';

            alarm_info->cloud_enabled = configure_message->get_oss_enabled();
            alarm_info->cloud_error_code = configure_message->get_oss_error_code();
            
            auto alarm_dao = this->get_alarm_dao();
            if(alarm_dao == nullptr)
            {
                __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"DAO is not create ...ERROR",nullptr);
            }
            boost::shared_ptr<ERROR_MESSAGE_CLASS> alarm_message;
            alarm_message = alarm_dao->query(alarm_id);
            if(alarm_message != nullptr)
            {
                std::string trigger_time = alarm_message->get_trigger_time();
                memcpy(alarm_info->trigger_time,trigger_time.data(),trigger_time.size());
                alarm_info->trigger_time[trigger_time.size()] = '\0';
                
                std::string recover_time = alarm_message->get_recover_time();
                memcpy(alarm_info->recover_time,recover_time.data(),recover_time.size());
                alarm_info->recover_time[recover_time.size()] = '\0';
                
                std::string material_id = alarm_message->get_material_id();
                memcpy(alarm_info->material_id,material_id.data(),material_id.size());
                alarm_info->material_id[material_id.size()] = '\0';
                
                alarm_info->is_triggered = alarm_message->get_trigged_state();
            }
            else
            {
                alarm_info->trigger_time[EH4A_BUFFER_LENGTH -1] = '\0';
                alarm_info->recover_time[EH4A_BUFFER_LENGTH -1] = '\0';
                alarm_info->material_id[EH4A_BUFFER_LENGTH -1] = '\0';
                alarm_info->is_triggered = ZOO_FALSE;
            }
        }
        __EH_CATCH_ALL(result)

        return result;
    }
}
