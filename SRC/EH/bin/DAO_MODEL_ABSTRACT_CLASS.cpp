/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : DAO_MODEL_ABSTRACT_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "DAO_MODEL_ABSTRACT_CLASS.h"
#include <boost/algorithm/string.hpp>
#include "ZOO_if.h"
namespace ZOO_EH
{

    DAO_MODEL_ABSTRACT_CLASS::DAO_MODEL_ABSTRACT_CLASS()
    {
        //ctor
    }

    DAO_MODEL_ABSTRACT_CLASS::~DAO_MODEL_ABSTRACT_CLASS()
    {
        //dtor
    }

    std::string DAO_MODEL_ABSTRACT_CLASS::convert_to_hex_string_upper(ZOO_INT32 error_code)
    {
        std::ostringstream os;
        os << std::hex << error_code;
        std::string hex_string_upper = os.str();
        boost::algorithm::to_upper(hex_string_upper);
        return "0x" + hex_string_upper;
    }

    
    ZOO_UINT32 DAO_MODEL_ABSTRACT_CLASS::convert_to_hex_string_to_int(std::string error_code)
    {
    	ZOO_UINT32 ec = 0;
    	std::string tmp = std::move(error_code);
    	if(error_code.size() >= 2)
    	{
    	    // 0x54520000 0X54520000
    	    if(error_code[1] == 'x' || error_code[1] == 'X')
    	    {
    	        std::copy(tmp.begin() + 2,tmp.end(),std::back_inserter(tmp));
    	    }
    	}
    	sscanf(tmp.c_str(),"%x",&ec);
    	return ec;
    }

    EH4A_ERROR_TYPE_ENUM DAO_MODEL_ABSTRACT_CLASS::converter(std::string type)
    {
    	if(type == "HW")
    	{
    		return EH4A_ERROR_TYPE_HW;
    	}

    	if(type == "SW")
    	{
    		return EH4A_ERROR_TYPE_SW;
    	}

    	return EH4A_ERROR_TYPE_MIN;
    }
    std::string DAO_MODEL_ABSTRACT_CLASS::convert_to_component(ZOO_INT32 error_code)
    {
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "> function entry ...");
        std::ostringstream os;
        os << std::hex << error_code;
        std::string hex_string_upper = os.str();
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: hex_string is %s",hex_string_upper.data());
        std::string first;
        std::copy(hex_string_upper.begin() ,hex_string_upper.begin() + 2,std::back_inserter(first));
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: first is %s",first.data());
        std::string second;
        std::copy(hex_string_upper.begin() + 2,hex_string_upper.begin() + 4,std::back_inserter(second));
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: second is %s",second.data());
        int f = 0 ;
        sscanf(first.data(),"%x",&f);
        int s = 0;
        sscanf(second.data(),"%x",&s);
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...");
        return std::string((char *)&f,1) + std::string((char *)&s,1) ;
    }
}
