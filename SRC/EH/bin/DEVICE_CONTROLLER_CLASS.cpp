/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : DEVICE_CONTROLLER_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "DEVICE_CONTROLLER_CLASS.h"
#include "ERROR_CODE_CONVERTER_CLASS.h"
#include <iostream>
namespace ZOO_EH
{
    DEVICE_CONTROLLER_CLASS::DEVICE_CONTROLLER_CLASS()
    {
        //ctor
    }

    DEVICE_CONTROLLER_CLASS::~DEVICE_CONTROLLER_CLASS()
    {

    }

    /**
     *@brief Initialize controller and models
    **/
    void DEVICE_CONTROLLER_CLASS::initialize()
    {
        this->create_all_device_models();
    }

    /**
     *@brief Create all device models
    **/
    void DEVICE_CONTROLLER_CLASS::create_all_device_models()
    {
    	ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," >funtion entry ...");
        std::vector<ZOO_TASK_STRUCT *>  tasks = EH_CONFIGURE::get_instance()->get_tasks();
        for(std::vector<ZOO_TASK_STRUCT *>::iterator i = tasks.begin(); i != tasks.end(); ++i)
        {
        	ZOO_TASK_STRUCT * task = *i;
        	if(NULL == task)
        	{
        	    ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,":: comp pointer is NULL ...ERROR");
        	    continue;
        	}
        	
    		boost::shared_ptr<DEVICE_ABSTRCT_MODEL_CLASS> model;
			model.reset(new DEVICE_ABSTRCT_MODEL_CLASS());
			std::string db_path = EH_CONFIGURE::get_instance()->get_device_db_path();
			std::string db_name = db_path+ "/" + task->component + "_ERROR_DEFINE.db";                  
			model->set_marking_code(task->component);
			model->set_db_name(db_name);
			model->initialize();
			this->m_device_map[task->component] = model;
    	}	
    	
		ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," <funtion exit ...");
    }

    /**
     *@brief Delete all device models
    **/
    void DEVICE_CONTROLLER_CLASS::delete_all_device_models()
    {
        this->m_device_map.clear();
    }

    /**
     *@brief Publish alarm trigged or not clear
     *@param alarm_id
     *@param message associate with alarm id
    **/
    void DEVICE_CONTROLLER_CLASS::publish(IN ZOO_INT32 alarm_id,IN boost::shared_ptr<ERROR_MESSAGE_CLASS> message)
    {
        
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "> function entry ...");
        auto componet =  ERROR_CODE_CONVERTER_CLASS::convert_to_componet_id(alarm_id);
        auto found = this->m_device_map.find(componet);
        if(found != this->m_device_map.end())
        {
            auto device = found->second;
            if(device != nullptr)
                device->publish(message);
        }
		ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...");
    }

    /**
     *@brief Get alarm information from db
     *@param alarm_id
     *@param alarm_info
    **/
    boost::shared_ptr<ERROR_MESSAGE_CLASS> DEVICE_CONTROLLER_CLASS::get_alarm_definitions(IN ZOO_INT32 alarm_id)
    {
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "> function entry ...");
        boost::shared_ptr<ERROR_MESSAGE_CLASS> message;
        auto componet =  ERROR_CODE_CONVERTER_CLASS::convert_to_componet_id(alarm_id);
        auto found = this->m_device_map.find(componet);
        if(found != this->m_device_map.end())
        {
            auto device = found->second;
            if(device != nullptr)
                message = device->get_alarm_definitions(alarm_id);
        }
		ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...");
        return message;
    }

    /**
     *@brief Get device by alarm id
     *@param alarm_id
    **/
    boost::shared_ptr<DEVICE_INTERFACE> DEVICE_CONTROLLER_CLASS::get_device(IN ZOO_INT32 alarm_id)
    {
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "> function entry ...");
        boost::shared_ptr<DEVICE_INTERFACE> device;
        auto componet =  ERROR_CODE_CONVERTER_CLASS::convert_to_componet_id(alarm_id);
        auto found = this->m_device_map.find(componet);
        if(found != this->m_device_map.end())
        {
            device = this->m_device_map[componet];
        }
		ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...");
        return device;
    }

    ZOO_INT32 DEVICE_CONTROLLER_CLASS::get_all_devices_row_counts()
    {
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "> function entry ...");
    	ZOO_INT32 counts = 0;
		for(std::map<std::string,boost::shared_ptr<DEVICE_INTERFACE> >::iterator ite = this->m_device_map.begin();
			ite != this->m_device_map.end();
			ite ++)
		{
			counts += (ite->second)->get_row_counts();
		}
		ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: total count is %d",counts);
		ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...");
    	return counts;
    }

     /**
     *@brief Find all device alarm datas
    **/
    std::vector<EH4A_ALARM_STRUCT *> DEVICE_CONTROLLER_CLASS::find_all_devices_alarm_datas()
    {
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "> function entry ...");
    	std::vector<EH4A_ALARM_STRUCT *> all_datas;
    	for(std::map<std::string,boost::shared_ptr<DEVICE_INTERFACE> >::iterator ite = this->m_device_map.begin();
			ite != this->m_device_map.end();
			ite ++)
		{
			auto device_alarms = (ite->second)->get_all_cfg_alarms();
			std::copy(device_alarms.begin(), device_alarms.end(),std::back_inserter(all_datas));
		}
		ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...");
    	return all_datas;
    }
}
