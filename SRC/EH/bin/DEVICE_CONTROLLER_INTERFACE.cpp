/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : TASK_CONTROLLER_INTERFACE.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "DEVICE_CONTROLLER_INTERFACE.h"

namespace ZOO_EH
{
    DEVICE_CONTROLLER_INTERFACE::DEVICE_CONTROLLER_INTERFACE()
    {
        //ctor
    }

    DEVICE_CONTROLLER_INTERFACE::~DEVICE_CONTROLLER_INTERFACE()
    {
        //dtor
    }
}
