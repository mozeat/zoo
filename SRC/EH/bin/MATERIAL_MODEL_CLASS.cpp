/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : MATERIAL_MODEL_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "MATERIAL_MODEL_CLASS.h"

namespace ZOO_EH
{
    MATERIAL_MODEL_CLASS::MATERIAL_MODEL_CLASS()
    {
        //ctor
    }

    MATERIAL_MODEL_CLASS::~MATERIAL_MODEL_CLASS()
    {
        //dtor
    }
}
