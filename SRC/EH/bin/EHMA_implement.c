/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : EHMA_implement.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-05    Generator      created
*************************************************************/
#include <EHMA_implement.h>
#include "EH_COMMON_FLOW_FACADE_WRAPPER.h"

/**
 *@brief EHMA_implement_4A_show_exception
 *@param component_id
 *@param file
 *@param function_name
 *@param line
 *@param error_code
 *@param link_error_code
 *@param info
**/
void EHMA_implement_4A_show_exception(IN const ZOO_CHAR* component_id,
                                          IN const ZOO_CHAR* file,
                                          IN const ZOO_CHAR* function_name,
                                          IN ZOO_INT32 line,
                                          IN ZOO_INT32 error_code,
                                          IN ZOO_INT32 link_error_code,
                                          IN const ZOO_CHAR* info,
                                          IN EH4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add ... BEGIN */

	rtn = EH_show_exception(component_id, 
							file, function_name, 
							line,error_code,
							link_error_code, info);
    /* User add ... END*/
    EHMA_raise_4A_show_exception(rtn,reply_handle);
}

/**
 *@brief EHMA_implement_4A_set_notification
 *@param info
**/
void EHMA_implement_4A_set_notification(IN EH4A_NOTIFY_INFO_STRUCT* info,
                                            IN EH4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = EH_set_notification(info);

    /* User add ... END*/
    EHMA_raise_4A_set_notification(rtn,reply_handle);
}

/**
 *@brief EHMA_implement_4A_set_alarm_with_material_id
 *@param material_id
 *@param alarm_id
**/
void EHMA_implement_4A_set_alarm_with_material_id(IN const ZOO_CHAR* material_id,
                                                      IN ZOO_INT32 alarm_id,
                                                      IN EH4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add ... BEGIN */
	rtn =  EH_set_alarm_with_material_id( material_id,alarm_id);

    /* User add ... END*/
    EHMA_raise_4A_set_alarm_with_material_id(rtn,reply_handle);
}

/**
 *@brief EHMA_implement_4A_set_alarm
 *@param alarm_id
**/
void EHMA_implement_4A_set_alarm(IN ZOO_INT32 alarm_id,
                                     IN EH4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add ... BEGIN */

	
	rtn =  EH_set_alarm(alarm_id);
	
    /* User add ... END*/
    EHMA_raise_4A_set_alarm(rtn,reply_handle);
}

/**
 *@brief EHMA_implement_4A_clear_alarm
 *@param alarm_id
**/
void EHMA_implement_4A_clear_alarm(IN ZOO_INT32 alarm_id,
                                       IN EH4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add ... BEGIN */

	rtn =  EH_clear_alarm(alarm_id);
	
    /* User add ... END*/
    EHMA_raise_4A_clear_alarm(rtn,reply_handle);
}

/**
 *@brief EHMA_implement_4A_clear_all_alarms
**/
void EHMA_implement_4A_clear_all_alarms(IN EH4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add ... BEGIN */

	rtn =  EH_clear_all_alarms();
	
    /* User add ... END*/
    EHMA_raise_4A_clear_all_alarms(rtn,reply_handle);
}

/**
 *@brief EHMA_implement_4A_get_alarm_information
 *@param alarm_id
 *@param details
**/
void EHMA_implement_4A_get_alarm_info(IN ZOO_INT32 alarm_id,
                                          IN EH4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    EH4A_ALARM_DETAILS_STRUCT details ;
    memset(&details,0,sizeof(EH4A_ALARM_DETAILS_STRUCT));
    /* User add ... BEGIN */
	rtn =  EH_get_alarm_information(alarm_id, &details);

    /* User add ... END*/
    EHMA_raise_4A_get_alarm_info(rtn,&details,reply_handle);
}

/**
 *@brief EHMA_implement_4A_find_alarm_counts
 *@param counts
**/
void EHMA_implement_4A_find_alarm_counts(IN EH4A_FOUND_TYPE_ENUM found_type,
                                             IN EH4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    ZOO_INT32 counts  = 0;
    ZOO_INT32 session_id  = 0;
    /* User add some code here if had special need. */
    rtn = EH_find_alarm_counts(found_type,&counts,&session_id);

    /* User add ... END*/
    EHMA_raise_4A_find_alarm_counts(rtn,counts,session_id,reply_handle);
}

/**
 *@brief EHMA_implement_4A_get_alarm_data
 *@param index
 *@param data
**/
void EHMA_implement_4A_get_alarm_data(IN ZOO_INT32 session_id,
                                          IN ZOO_INT32 index,
                                          IN EH4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    EH4A_ALARM_STRUCT data ;
    memset(&data,0,sizeof(EH4A_ALARM_STRUCT));
    /* User add some code here if had special need. */
    rtn = EH_get_alarm_data(session_id,index,&data);

    /* User add ... END*/
    EHMA_raise_4A_get_alarm_data(rtn,&data,reply_handle);
}
