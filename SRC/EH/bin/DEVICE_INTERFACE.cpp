/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : DEVICE_INTERFACE.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "DEVICE_INTERFACE.h"

namespace ZOO_EH
{
    DEVICE_INTERFACE::DEVICE_INTERFACE()
    {
        //ctor
    }

    DEVICE_INTERFACE::~DEVICE_INTERFACE()
    {
        //dtor
    }
}
