/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : STRING_UTILITY_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "STRING_UTILITY_CLASS.h"
#include <cstring>
namespace ZOO_EH
{

    STRING_UTILITY_CLASS::STRING_UTILITY_CLASS()
    {
        //ctor
    }

    STRING_UTILITY_CLASS::~STRING_UTILITY_CLASS()
    {
        //dtor
    }

    const ZOO_CHAR * STRING_UTILITY_CLASS::to_string (ZOO_BOOL value)
    {
        const ZOO_CHAR * type_str = "";
        switch(value)
        {
            case ZOO_TRUE: type_str = "TRUE";break;
            case ZOO_FALSE: type_str = "FALSE";break;
            default:break;
        }
        return type_str;
    }

    std::string STRING_UTILITY_CLASS::str_toupper(std::string s)
    {
         std::transform(s.begin(), s.end(), s.begin(),
                       [](unsigned char c){ return std::toupper(c); } // correct
                        );
        return s;
    }

    std::string STRING_UTILITY_CLASS::str_tolower(std::string s)
    {
         std::transform(s.begin(), s.end(), s.begin(),
                       [](unsigned char c){ return std::tolower(c); } // correct
                        );
        return s;
    }

    std::string STRING_UTILITY_CLASS::found_type_to_string(EH4A_FOUND_TYPE_ENUM type)
    {
        std::string  result;
        switch(type)
        {
            case EH4A_FOUND_TYPE_ALL: result = "EH4A_FOUND_TYPE_ALL";break;
            case EH4A_FOUND_TYPE_TRIGGED: result = "EH4A_FOUND_TYPE_TRIGGED";break;
            case EH4A_FOUND_TYPE_RECOVERED: result = "EH4A_FOUND_TYPE_RECOVERED";break;
            case EH4A_FOUND_TYPE_MAX: result = "EH4A_FOUND_TYPE_MAX";break;
            case EH4A_FOUND_TYPE_MIN: result = "EH4A_FOUND_TYPE_MIN";break;
        }
        return result;
    }

    ZOO_INT32 STRING_UTILITY_CLASS::bool_string_to_int(const ZOO_CHAR * bool_string)
    {
        ZOO_BOOL result = ZOO_FALSE;
        if(str_toupper(bool_string) == "TRUE")
        {
            result = ZOO_TRUE;
        }
        return result;
    }

    const ZOO_CHAR * STRING_UTILITY_CLASS::to_string(EH4A_ERROR_TYPE_ENUM type)
    {
        const ZOO_CHAR * type_str = "";
        switch(type)
        {
        case EH4A_ERROR_TYPE_MIN:
            type_str = "EH4A_ERROR_TYPE_MIN";break;
        case EH4A_ERROR_TYPE_SW:
            type_str = "EH4A_ERROR_TYPE_MIN";break;
        case EH4A_ERROR_TYPE_HW:
            type_str = "EH4A_ERROR_TYPE_MIN";break;
        case EH4A_ERROR_TYPE_MAX:
            type_str = "EH4A_ERROR_TYPE_MIN";break;
        default:
            break;
        }
        return type_str;
    }

    EH4A_ERROR_TYPE_ENUM STRING_UTILITY_CLASS::to_enum (const ZOO_CHAR * type_string)
    {
        EH4A_ERROR_TYPE_ENUM type = EH4A_ERROR_TYPE_MIN;

        if(std::strcmp(type_string, "SW") == 0)
        {
            type = EH4A_ERROR_TYPE_SW;
        }

        if(std::strcmp(type_string, "HW") == 0)
        {
            type = EH4A_ERROR_TYPE_HW;
        }
        return type;
    }
}
