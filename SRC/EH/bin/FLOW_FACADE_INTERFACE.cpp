/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : FLOW_FACADE_INTERFACE.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "FLOW_FACADE_INTERFACE.h"

namespace ZOO_EH
{
    FLOW_FACADE_INTERFACE::FLOW_FACADE_INTERFACE()
    {
        //ctor
    }

    FLOW_FACADE_INTERFACE::~FLOW_FACADE_INTERFACE()
    {
        //dtor
    }
}

