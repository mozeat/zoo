/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : EHMA_event.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-21    Generator      created
*************************************************************/
#include <EHMA_event.h>

/**
 *@brief EHMA_raise_4A_show_exception
 *@param component_id
 *@param file
 *@param function_name
 *@param line
 *@param error_code
 *@param link_error_code
 *@param info
**/
ZOO_INT32 EHMA_raise_4A_show_exception(IN ZOO_INT32 error_code,
                                           IN EH4I_REPLY_HANDLE reply_handle)
{
    EH4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = EH4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = EH4I_get_reply_message_length(EH4A_SHOW_EXCEPTION_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (EH4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = EH4A_SHOW_EXCEPTION_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = EH4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief EHMA_raise_4A_set_notification
 *@param info
**/
ZOO_INT32 EHMA_raise_4A_set_notification(IN ZOO_INT32 error_code,
                                             IN EH4I_REPLY_HANDLE reply_handle)
{
    EH4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = EH4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = EH4I_get_reply_message_length(EH4A_SET_NOTIFICATION_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (EH4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = EH4A_SET_NOTIFICATION_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = EH4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief EHMA_raise_4A_set_alarm_with_material_id
 *@param material_id
 *@param alarm_id
**/
ZOO_INT32 EHMA_raise_4A_set_alarm_with_material_id(IN ZOO_INT32 error_code,
                                                       IN EH4I_REPLY_HANDLE reply_handle)
{
    EH4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = EH4A_PARAMETER_ERR;
        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = EH4I_get_reply_message_length(EH4A_SET_ALARM_WITH_MATERIAL_ID_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (EH4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = EH4A_SET_ALARM_WITH_MATERIAL_ID_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = EH4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief EHMA_raise_4A_set_alarm
 *@param alarm_id
**/
ZOO_INT32 EHMA_raise_4A_set_alarm(IN ZOO_INT32 error_code,
                                      IN EH4I_REPLY_HANDLE reply_handle)
{
    EH4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = EH4A_PARAMETER_ERR;
        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = EH4I_get_reply_message_length(EH4A_SET_ALARM_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (EH4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = EH4A_SET_ALARM_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = EH4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief EHMA_raise_4A_clear_alarm
 *@param alarm_id
**/
ZOO_INT32 EHMA_raise_4A_clear_alarm(IN ZOO_INT32 error_code,
                                        IN EH4I_REPLY_HANDLE reply_handle)
{
    EH4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = EH4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = EH4I_get_reply_message_length(EH4A_CLEAR_ALARM_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (EH4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = EH4A_CLEAR_ALARM_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = EH4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief EHMA_raise_4A_clear_all_alarms
**/
ZOO_INT32 EHMA_raise_4A_clear_all_alarms(IN ZOO_INT32 error_code,
                                             IN EH4I_REPLY_HANDLE reply_handle)
{
    EH4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = EH4A_PARAMETER_ERR;
        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = EH4I_get_reply_message_length(EH4A_CLEAR_ALL_ALARMS_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (EH4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = EH4A_CLEAR_ALL_ALARMS_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = EH4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief EHMA_raise_4A_get_alarm_info
 *@param alarm_id
 *@param details
**/
ZOO_INT32 EHMA_raise_4A_get_alarm_info(IN ZOO_INT32 error_code,
                                           IN EH4A_ALARM_DETAILS_STRUCT * details,
                                           IN EH4I_REPLY_HANDLE reply_handle)
{
    EH4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = EH4A_PARAMETER_ERR;
        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = EH4I_get_reply_message_length(EH4A_GET_ALARM_INFO_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (EH4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = EH4A_GET_ALARM_INFO_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.get_alarm_info_rep_msg.details,details,sizeof(EH4A_ALARM_DETAILS_STRUCT));
                rtn = EH4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief EHMA_raise_4A_find_alarm_counts
 *@param found_type
 *@param counts
 *@param session_id
**/
ZOO_INT32 EHMA_raise_4A_find_alarm_counts(IN ZOO_INT32 error_code,
                                              IN ZOO_INT32 counts,
                                              IN ZOO_INT32 session_id,
                                              IN EH4I_REPLY_HANDLE reply_handle)
{
    EH4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = EH4A_PARAMETER_ERR;
        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = EH4I_get_reply_message_length(EH4A_FIND_ALARM_COUNTS_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (EH4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = EH4A_FIND_ALARM_COUNTS_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.find_alarm_counts_rep_msg.counts,&counts,sizeof(ZOO_INT32));
                memcpy(&reply_message->reply_body.find_alarm_counts_rep_msg.session_id,&session_id,sizeof(ZOO_INT32));
                rtn = EH4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief EHMA_raise_4A_get_alarm_data
 *@param index
 *@param data
**/
ZOO_INT32 EHMA_raise_4A_get_alarm_data(IN ZOO_INT32 error_code,
                                           IN EH4A_ALARM_STRUCT * data,
                                           IN EH4I_REPLY_HANDLE reply_handle)
{
    EH4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = EH4A_PARAMETER_ERR;
        
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = EH4I_get_reply_message_length(EH4A_GET_ALARM_DATA_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (EH4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = EH4A_GET_ALARM_DATA_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.get_alarm_data_rep_msg.data,data,sizeof(EH4A_ALARM_STRUCT));
                rtn = EH4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = EH4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief EH4A_alarm_info_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
void EHMA_raise_4A_alarm_info_subscribe(IN EH4A_ALARM_STRUCT * status,IN ZOO_INT32 error_code,IN void *context)
{
    ZOO_INT32 rtn = OK;
    EH4I_REPLY_STRUCT * reply_message = NULL;
    reply_message = (EH4I_REPLY_STRUCT * ) MM4A_malloc(sizeof(EH4I_REPLY_STRUCT));
    if(NULL == reply_message)
    {
        rtn = EH4A_PARAMETER_ERR;
    }
    
    if(OK == rtn)
    {
        reply_message->reply_header.function_code = EH4A_ALARM_INFO_SUBSCRIBE_CODE;
        reply_message->reply_header.execute_result = error_code;
        memcpy(&reply_message->reply_body.alarm_info_subscribe_code_rep_msg.status,status,sizeof(EH4A_ALARM_STRUCT));
    }

    if(OK == rtn)
    {
        rtn = EH4I_publish_event(EH4A_SERVER,
                                            EH4A_ALARM_INFO_SUBSCRIBE_CODE,
                                            reply_message);
    }

    if(OK != rtn)
    {
        
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }

    return;
}
/**
 *@brief EH4A_notify_info_subscribe
 *@param notify_info
 *@param error_code
 *@param *context
**/
void EHMA_raise_4A_notify_info_subscribe(IN EH4A_NOTIFY_INFO_STRUCT * notify_info,IN ZOO_INT32 error_code,IN void *context)
{
    ZOO_INT32 rtn = OK;
    EH4I_REPLY_STRUCT * reply_message = NULL;
    reply_message = (EH4I_REPLY_STRUCT * ) MM4A_malloc(sizeof(EH4I_REPLY_STRUCT));
    if(NULL == reply_message)
    {
        rtn = EH4A_PARAMETER_ERR;
    }
    
    if(OK == rtn)
    {
        reply_message->reply_header.function_code = EH4A_NOTIFY_INFO_SUBSCRIBE_CODE;
        reply_message->reply_header.execute_result = error_code;
        memcpy(&reply_message->reply_body.notify_info_subscribe_code_rep_msg.notify_info,notify_info,sizeof(EH4A_NOTIFY_INFO_STRUCT));
    }

    if(OK == rtn)
    {
        rtn = EH4I_publish_event(EH4A_SERVER,
                                            EH4A_NOTIFY_INFO_SUBSCRIBE_CODE,
                                            reply_message);
    }

    if(OK != rtn)
    {
        
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }

    return;
}

