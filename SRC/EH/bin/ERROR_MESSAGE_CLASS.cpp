/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : ERROR_INFORMATION_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "ERROR_MESSAGE_CLASS.h"

namespace ZOO_EH
{
    ERROR_MESSAGE_CLASS::ERROR_MESSAGE_CLASS()
    {
        //ctor
        this->m_alarm_id = 0;
        this->m_alarm_level = 0;
        this->m_alarm_type = EH4A_ERROR_TYPE_MAX;
        this->m_oss_enabled = ZOO_FALSE;
        this->m_welkins_enabled = ZOO_FALSE;
        this->m_oss_error_code = 0xffffffff;
        this->m_is_trigged = ZOO_FALSE;
    }

    ERROR_MESSAGE_CLASS::~ERROR_MESSAGE_CLASS()
    {
        //dtor
    }

    void ERROR_MESSAGE_CLASS::set_trigged_state(ZOO_BOOL on)
    {
        this->m_is_trigged = on;
    }

    ZOO_BOOL ERROR_MESSAGE_CLASS::get_trigged_state()
    {
        return this->m_is_trigged;
    }

    void ERROR_MESSAGE_CLASS::set_componet_id(const std::string &  componet_id)
    {
        this->m_componet_id = componet_id;
    }

    const std::string & ERROR_MESSAGE_CLASS::get_componet_id()
    {
        return this->m_componet_id;
    }

    void ERROR_MESSAGE_CLASS::set_material_id(const std::string &  material_id)
    {
        this->m_material_id = material_id;
    }

    const std::string & ERROR_MESSAGE_CLASS::get_material_id()
    {
        return this->m_material_id;
    }

    /** @brief (one liner)
      *
      * (documentation goes here)
      */
    ZOO_INT32 ERROR_MESSAGE_CLASS::get_alarm_level()
    {
        return this->m_alarm_level;
    }

    /** @brief (one liner)
      *
      * (documentation goes here)
      */
    void ERROR_MESSAGE_CLASS::set_alarm_level(ZOO_INT32 error_level)
    {
        this->m_alarm_level = error_level;
    }

    /** @brief (one liner)
      *
      * (documentation goes here)
      */
    ZOO_INT32 ERROR_MESSAGE_CLASS::get_alarm_type()
    {
        return this->m_alarm_type;
    }

    /** @brief (one liner)
	  *
	  * (documentation goes here)
	  */
    void ERROR_MESSAGE_CLASS::set_alarm_type(ZOO_INT32 error_type)
    {
        this->m_alarm_type = error_type;
    }

    /** @brief (one liner)
     *
     * (documentation goes here)
     */
    ZOO_INT32 ERROR_MESSAGE_CLASS::get_alarm_id()
    {
        return this->m_alarm_id;
    }

    /** @brief (one liner)
      *
      * (documentation goes here)
     */
    void ERROR_MESSAGE_CLASS::set_alarm_id(ZOO_INT32 error_code)
    {
        this->m_alarm_id = error_code;
    }

    const std::string & ERROR_MESSAGE_CLASS::get_chinese_description()
    {
        return this->m_chinese_description;
    }

    void ERROR_MESSAGE_CLASS::set_chinese_description(const std::string & chinese_description)
    {
        this->m_chinese_description = chinese_description;
    }

    const std::string & ERROR_MESSAGE_CLASS::get_english_description()
    {
        return this->m_english_description;
    }

    void ERROR_MESSAGE_CLASS::set_english_description(const std::string & english_description)
    {
        this->m_english_description = english_description;
    }

    const std::string & ERROR_MESSAGE_CLASS::get_trigger_time()
    {
        return this->m_trigger_time;
    }

    void ERROR_MESSAGE_CLASS::set_trigger_time(const std::string & trigger_time)
    {
        this->m_trigger_time = trigger_time;
    }

    const std::string & ERROR_MESSAGE_CLASS::get_recover_time()
    {
        return this->m_recover_time;
    }

    void ERROR_MESSAGE_CLASS::set_recover_time(const std::string & recover_time)
    {
        this->m_recover_time = recover_time;
    }

    void ERROR_MESSAGE_CLASS::set_cloud_enabled(ZOO_BOOL enabled)
    {
        this->m_oss_enabled = enabled;
    }
    
    ZOO_BOOL ERROR_MESSAGE_CLASS::get_oss_enabled()
    {
        return this->m_oss_enabled;
    }
    
    void ERROR_MESSAGE_CLASS::set_welkins_enabled(ZOO_BOOL enabled)
    {
        this->m_welkins_enabled = enabled;
    }
    
    ZOO_BOOL ERROR_MESSAGE_CLASS::get_welkins_enabled()
    {
        return this->m_welkins_enabled;
    }

    void ERROR_MESSAGE_CLASS::set_cloud_error_code(ZOO_INT32 error_code)
    {
        this->m_oss_error_code = error_code;
    }
    
    ZOO_INT32 ERROR_MESSAGE_CLASS::get_oss_error_code()
    {
        return this->m_oss_error_code;
    }
    
    void ERROR_MESSAGE_CLASS::clear()
    {
        this->m_oss_error_code = 0xfffffff;
        this->m_alarm_id = 0;
        this->m_alarm_level = 0;
        this->m_alarm_type = EH4A_ERROR_TYPE_MAX;
        this->m_oss_enabled = ZOO_FALSE;
        this->m_welkins_enabled = ZOO_FALSE;
        this->m_oss_error_code = 0xffffffff;
    }
}
