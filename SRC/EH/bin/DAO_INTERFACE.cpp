/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : DAO_INTERFACE.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "DAO_INTERFACE.h"

namespace ZOO_EH
{
    DAO_INTERFACE::DAO_INTERFACE()
    {
        //ctor
    }

    DAO_INTERFACE::~DAO_INTERFACE()
    {
        //dtor
    }
}
