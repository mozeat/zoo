/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : ERROR_CODE_CONVERTER_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "ERROR_CODE_CONVERTER_CLASS.h"
#include <sstream>
#include <boost/lexical_cast.hpp>
namespace ZOO_EH
{
    ERROR_CODE_CONVERTER_CLASS::ERROR_CODE_CONVERTER_CLASS()
    {
        //ctor
    }

    ERROR_CODE_CONVERTER_CLASS::~ERROR_CODE_CONVERTER_CLASS()
    {
        //dtor
    }

    /**
     * @brief convert error code to componet id
     */
    std::string ERROR_CODE_CONVERTER_CLASS::convert_to_componet_id(ZOO_INT32 error_code)
    {
        std::string componet;
        std::ostringstream os;
        os << std::hex << error_code;
        if(os.str().size() >= 4)
        {
            std::string front4byte(os.str().data(),4);
            for (unsigned int i = 0; i < front4byte.size(); i += 2)
            {
                std::istringstream iss(front4byte.substr(i, 2));
                int temp = 0;
                iss >> std::hex >> temp;
                componet += static_cast<char>(temp);
            }
        }
        return componet;
    }

    /**
     * @brief convert error code to componet id
     */
    ZOO_INT32 ERROR_CODE_CONVERTER_CLASS::convert_to_error_code(std::string error_code_string)
    {
        ZOO_INT32 error_code = 0xffffffff;
        if(error_code_string.find("0x") != std::string::npos)
        {
            error_code_string.erase(error_code_string.find("0x"),2);
        }
        else if(error_code_string.find("0X") != std::string::npos)
        {
            error_code_string.erase(error_code_string.find("0X"),2);
        }
        else
        {
            //erase space
            error_code_string.erase(error_code_string.find(' '));
        }
        sscanf(error_code_string.c_str(),"%x",&error_code);
        return error_code;
    }
}

