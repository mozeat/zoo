/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : SESSION_MANAGER_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "SESSION_MANAGER_CLASS.h"
#include <vector>
namespace ZOO_EH
{
    /**
     * @brief Initialize static member instance
     **/
    boost::shared_ptr<SESSION_MANAGER_CLASS> SESSION_MANAGER_CLASS::m_instance = NULL;

    
    SESSION_MANAGER_CLASS::SESSION_MANAGER_CLASS()
    {

    }
    
    SESSION_MANAGER_CLASS::~SESSION_MANAGER_CLASS()
    {
        std::map<ZOO_INT32,boost::shared_ptr<SESSION_CLASS> >::iterator ite = this->m_sessions.begin();
        while( ite != this->m_sessions.end())
        {
            boost::shared_ptr<SESSION_CLASS> session = ite->second;
            if(session) 
                session->clear();
        }
    }

    /**
     * @brief Get instance
     **/
    boost::shared_ptr<SESSION_MANAGER_CLASS> SESSION_MANAGER_CLASS::get_instance()
    {
        if(SESSION_MANAGER_CLASS::m_instance == NULL)
        {
            SESSION_MANAGER_CLASS::m_instance.reset(new SESSION_MANAGER_CLASS());
        }
        return SESSION_MANAGER_CLASS::m_instance;
    }

    /**
     * @brief Save alarm cache
     * @param alarms   the cache content
     **/
    void  SESSION_MANAGER_CLASS::add_session(IN std::vector<EH4A_ALARM_STRUCT * > & alarms,ZOO_INT32 * session_id)      
    {
        boost::shared_ptr<SESSION_CLASS> session = boost::make_shared<SESSION_CLASS>(alarms);
        *session_id  = session->get_session_id();
        this->m_sessions[*session_id] = session;
    }

    /**
     *@brief Get alarm cache
     *@return 
    **/
    boost::shared_ptr<SESSION_CLASS>  SESSION_MANAGER_CLASS::get_session(ZOO_INT32 session_id)
    {
        boost::shared_ptr<SESSION_CLASS> session;
        if(this->m_sessions.find(session_id) != this->m_sessions.end())
        {
            session = this->m_sessions[session_id];
        }
        return session;
    }
    void SESSION_MANAGER_CLASS::remove_session(ZOO_INT32 session_id)
    {
        std::map<ZOO_INT32,boost::shared_ptr<SESSION_CLASS> >::iterator ite = this->m_sessions.find(session_id);
        if( ite != this->m_sessions.end())
        {
            this->m_sessions.erase(ite);
        }
    }
}
