/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : EH_COMMON_FLOW_FACADE_WRAPPER.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "EH_COMMON_FLOW_FACADE_WRAPPER.h"
#include "PROCESSING_FLOW_FACADE_CLASS.h"


extern ZOO_EH::PROCESSING_FLOW_FACADE_CLASS* g_processing_flow_facade;

/**
 * @brief print exception to file
 */
ZOO_INT32 EH_show_exception(IN const ZOO_CHAR * component_id,
                                            IN const ZOO_CHAR * file,
                                            IN const ZOO_CHAR * function_name,
                                            IN ZOO_INT32 line,
                                            IN ZOO_INT32 error_code,
                                            IN ZOO_INT32 link_error_code,
                                            IN const ZOO_CHAR * info)
{
    if(g_processing_flow_facade != nullptr)
    {
        return g_processing_flow_facade->show_exception(component_id,file,
                                                        function_name,line,
                                                        error_code,link_error_code,
                                                        info);
    }
    return EH4A_SYSTEM_ERR;
}

ZOO_INT32 EH_set_notification(EH4A_NOTIFY_INFO_STRUCT* info)
{
    if(g_processing_flow_facade != nullptr)
    {
        return g_processing_flow_facade->set_notification(info);
    }
    return EH4A_SYSTEM_ERR;
}

/**
 * @brief Set alarm subscribe table
 */
ZOO_INT32 EH_set_alarm(IN ZOO_INT32 alarm_id)
{
    if(g_processing_flow_facade != nullptr)
    {
        return g_processing_flow_facade->invoke_alarm("",alarm_id);
    }
    return EH4A_SYSTEM_ERR;
}

/**
* @brief Set alarm subscribe table
*/
ZOO_INT32 EH_set_alarm_with_material_id(IN const ZOO_CHAR* material_id,
														IN ZOO_INT32 alarm_id)
{
    if(g_processing_flow_facade != nullptr)
    {
        return g_processing_flow_facade->invoke_alarm(material_id,alarm_id);
    }
    return EH4A_SYSTEM_ERR;
}


/**
 * @brief Clear alarm subscribe table
 */
ZOO_INT32 EH_clear_alarm(IN ZOO_INT32 alarm_id)
{
    if(g_processing_flow_facade != nullptr)
    {
        return g_processing_flow_facade->clear_alarm(alarm_id);
    }
    return EH4A_SYSTEM_ERR;
}

/**
 * @brief Remove table data
 */
ZOO_INT32 EH_clear_all_alarms(void)
{
    if(g_processing_flow_facade != nullptr)
    {
        return g_processing_flow_facade->clear_all_alarms();
    }
    return EH4A_SYSTEM_ERR;
}

/**
 * @brief Find configure alarm points
 */
ZOO_INT32 EH_find_alarm_counts(IN EH4A_FOUND_TYPE_ENUM found_type,IN ZOO_INT32 * counts,IN ZOO_INT32 * session_id)
{
    if(g_processing_flow_facade != nullptr)
    {
        return g_processing_flow_facade->find_alarm_counts(found_type,counts,session_id);
    }
    return EH4A_SYSTEM_ERR;
}

/**
 * @brief Find configure alarm points
 */
ZOO_INT32 EH_get_alarm_data(IN ZOO_INT32 session_id,IN ZOO_INT32 index,
												INOUT EH4A_ALARM_STRUCT * data)
{
    if(g_processing_flow_facade != nullptr)
    {
        return g_processing_flow_facade->get_alarm_data(session_id,index,data);
    }
    return EH4A_SYSTEM_ERR;
}

/**
 * @brief Get alarm info by error code
 */
ZOO_INT32 EH_get_alarm_information(IN ZOO_INT32 alarm_id,
                                                INOUT EH4A_ALARM_DETAILS_STRUCT * details)
{
    if(g_processing_flow_facade != nullptr)
    {
        return g_processing_flow_facade->get_alarm_information(alarm_id,details);
    }
    return EH4A_SYSTEM_ERR;
}
