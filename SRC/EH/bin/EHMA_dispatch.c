/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : EHMA_dispatch.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-21    Generator      created
*************************************************************/

#include <EHMA_dispatch.h>
#include <EHMA_implement.h>

/**
 *@brief EHMA_local_4A_show_exception
 *@param component_id
 *@param file
 *@param function_name
 *@param line
 *@param error_code
 *@param link_error_code
 *@param info
**/
static ZOO_INT32 EHMA_local_4A_show_exception(const MQ4A_SERV_ADDR server,EH4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    EH4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = EH4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (EH4I_REPLY_HANDLE)MM4A_malloc(sizeof(EH4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = EH4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        EHMA_implement_4A_show_exception(request->request_body.show_exception_req_msg.component_id,
                                           request->request_body.show_exception_req_msg.file,
                                           request->request_body.show_exception_req_msg.function_name,
                                           request->request_body.show_exception_req_msg.line,
                                           request->request_body.show_exception_req_msg.error_code,
                                           request->request_body.show_exception_req_msg.link_error_code,
                                           request->request_body.show_exception_req_msg.info,
                                           reply_handle);
    }
    return rtn;
}
/**
 *@brief EHMA_local_4A_set_notification
 *@param info
**/
static ZOO_INT32 EHMA_local_4A_set_notification(const MQ4A_SERV_ADDR server,EH4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    EH4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = EH4A_SYSTEM_ERR;
        EH4A_show_exception(EH4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,EH4A_PARAMETER_ERR,0,"request pointer is NULL ...ERROR");
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (EH4I_REPLY_HANDLE)MM4A_malloc(sizeof(EH4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = EH4A_SYSTEM_ERR;
            EH4A_show_exception(EH4I_COMPONET_ID,__FILE__,__ZOO_FUNC__,__LINE__,EH4A_SYSTEM_ERR,0,"malloc reply_handle failed ...ERROR");
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        EHMA_implement_4A_set_notification(&request->request_body.set_notification_req_msg.info,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief EHMA_local_4A_set_alarm_with_material_id
 *@param material_id
 *@param alarm_id
**/
static ZOO_INT32 EHMA_local_4A_set_alarm_with_material_id(const MQ4A_SERV_ADDR server,EH4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    EH4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = EH4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (EH4I_REPLY_HANDLE)MM4A_malloc(sizeof(EH4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = EH4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        EHMA_implement_4A_set_alarm_with_material_id(request->request_body.set_alarm_with_material_id_req_msg.material_id,
                                           request->request_body.set_alarm_with_material_id_req_msg.alarm_id,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief EHMA_local_4A_set_alarm
 *@param alarm_id
**/
static ZOO_INT32 EHMA_local_4A_set_alarm(const MQ4A_SERV_ADDR server,EH4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    EH4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = EH4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (EH4I_REPLY_HANDLE)MM4A_malloc(sizeof(EH4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = EH4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        EHMA_implement_4A_set_alarm(request->request_body.set_alarm_req_msg.alarm_id,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief EHMA_local_4A_clear_alarm
 *@param alarm_id
**/
static ZOO_INT32 EHMA_local_4A_clear_alarm(const MQ4A_SERV_ADDR server,EH4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    EH4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = EH4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (EH4I_REPLY_HANDLE)MM4A_malloc(sizeof(EH4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = EH4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        EHMA_implement_4A_clear_alarm(request->request_body.clear_alarm_req_msg.alarm_id,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief EHMA_local_4A_clear_all_alarms
**/
static ZOO_INT32 EHMA_local_4A_clear_all_alarms(const MQ4A_SERV_ADDR server,EH4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    EH4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = EH4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (EH4I_REPLY_HANDLE)MM4A_malloc(sizeof(EH4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = EH4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        EHMA_implement_4A_clear_all_alarms(reply_handle);
    }
    return rtn;
}

/**
 *@brief EHMA_local_4A_get_alarm_info
 *@param alarm_id
 *@param details
**/
static ZOO_INT32 EHMA_local_4A_get_alarm_info(const MQ4A_SERV_ADDR server,EH4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    EH4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = EH4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (EH4I_REPLY_HANDLE)MM4A_malloc(sizeof(EH4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = EH4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        EHMA_implement_4A_get_alarm_info(request->request_body.get_alarm_info_req_msg.alarm_id,
                                           reply_handle);
    }
    return rtn;
}

/**
 *@brief EHMA_local_4A_find_alarm_counts
 *@param counts
**/
static ZOO_INT32 EHMA_local_4A_find_alarm_counts(const MQ4A_SERV_ADDR server,EH4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    EH4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = EH4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (EH4I_REPLY_HANDLE)MM4A_malloc(sizeof(EH4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = EH4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        EHMA_implement_4A_find_alarm_counts(request->request_body.find_alarm_counts_req_msg.found_type,reply_handle);
    }
    return rtn;
}

/**
 *@brief EHMA_local_4A_get_alarm_data
 *@param index
 *@param data
**/
static ZOO_INT32 EHMA_local_4A_get_alarm_data(const MQ4A_SERV_ADDR server,EH4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    EH4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = EH4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (EH4I_REPLY_HANDLE)MM4A_malloc(sizeof(EH4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = EH4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        EHMA_implement_4A_get_alarm_data(request->request_body.get_alarm_data_req_msg.session_id,
                                           request->request_body.get_alarm_data_req_msg.index,
                                           reply_handle);
    }
    return rtn;
}

/**
*@brief Dispatch message from client to server internal interface
*@param context        
*@param server        address
*@param msg           request message to server
*@param len           request message length
*@param reply_msg     reply message length to caller
*@param reply_msg_len reply message length
**/
void EHMA_callback_handler(void * context,const MQ4A_SERV_ADDR server,void * msg,ZOO_UINT32 mig_id)
{
    ZOO_INT32 rtn = OK;
    ZOO_INT32 rep_length = 0;
    EH4I_REQUEST_STRUCT *request = (EH4I_REQUEST_STRUCT*)msg;
    EH4I_REPLY_STRUCT *reply = NULL;
    if(request == NULL)
    {
        rtn = EH4A_SYSTEM_ERR;
        return;
    }

    if(OK == rtn)
    {
        switch(request->request_header.function_code)
        {
            case EH4A_SHOW_EXCEPTION_CODE:
                EHMA_local_4A_show_exception(server,request,mig_id);
                break;
            case EH4A_SET_NOTIFICATION_CODE:
                EHMA_local_4A_set_notification(server,request,mig_id);
                break;
            case EH4A_SET_ALARM_WITH_MATERIAL_ID_CODE:
                EHMA_local_4A_set_alarm_with_material_id(server,request,mig_id);
                break;
            case EH4A_SET_ALARM_CODE:
                EHMA_local_4A_set_alarm(server,request,mig_id);
                break;
            case EH4A_CLEAR_ALARM_CODE:
                EHMA_local_4A_clear_alarm(server,request,mig_id);
                break;
            case EH4A_CLEAR_ALL_ALARMS_CODE:
                EHMA_local_4A_clear_all_alarms(server,request,mig_id);
                break;
            case EH4A_GET_ALARM_INFO_CODE:
                EHMA_local_4A_get_alarm_info(server,request,mig_id);
                break;
            case EH4A_FIND_ALARM_COUNTS_CODE:
                EHMA_local_4A_find_alarm_counts(server,request,mig_id);
                break;
            case EH4A_GET_ALARM_DATA_CODE:
                EHMA_local_4A_get_alarm_data(server,request,mig_id);
                break;
            default:
                rtn = EH4A_SYSTEM_ERR;
                break;
        }
    }

    if(OK != rtn && request->request_header.need_reply)
    {
        rtn = EH4I_get_reply_message_length(request->request_header.function_code, &rep_length);
        if(OK != rtn)
        {
            rtn = EH4A_SYSTEM_ERR;
        }

        if(OK == rtn)
        {
            reply = (EH4I_REPLY_STRUCT *)MM4A_malloc(rep_length);
            if(reply == NULL)
            {
                rtn = EH4A_SYSTEM_ERR;
            }
        }

        if(OK == rtn)
        {
            memset(reply, 0x0, rep_length);
            reply->reply_header.execute_result = rtn;
            reply->reply_header.function_code = request->request_header.function_code;
            EH4I_send_reply_message(server,mig_id,reply);
        }
    }
    return;
}

