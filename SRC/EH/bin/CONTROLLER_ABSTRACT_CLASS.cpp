/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : CONTROLLER_ABSTRACT_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "CONTROLLER_ABSTRACT_CLASS.h"
namespace ZOO_EH
{
    CONTROLLER_ABSTRACT_CLASS::CONTROLLER_ABSTRACT_CLASS()
    {
        //ctor
    }

    CONTROLLER_ABSTRACT_CLASS::~CONTROLLER_ABSTRACT_CLASS()
    {
        //dtor
    }
}

