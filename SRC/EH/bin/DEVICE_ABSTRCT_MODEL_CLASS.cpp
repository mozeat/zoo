/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : TASK_ABSTRCT_MODEL_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "DEVICE_ABSTRCT_MODEL_CLASS.h"
#include <iostream>
namespace ZOO_EH
{
    DEVICE_ABSTRCT_MODEL_CLASS::DEVICE_ABSTRCT_MODEL_CLASS()
    :m_is_db_opened(ZOO_FALSE),
    m_internal_error(OK),
    m_device_enable(ZOO_FALSE),
    m_db_name("default")
    {
        //ctor
    }

    DEVICE_ABSTRCT_MODEL_CLASS::~DEVICE_ABSTRCT_MODEL_CLASS()
    {
        //dtor
    }

    /**
     *@brief Connect db
    **/
    void DEVICE_ABSTRCT_MODEL_CLASS::initialize()
    {
    	ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," >funtion entry ...");
        if(this->m_device_dao != nullptr)
        {
            this->m_device_dao->close();
            this->m_is_db_opened = ZOO_FALSE;
        }
        this->m_device_dao.reset(new DEVICE_DAO_MODEL_CLASS(""));
        this->m_device_dao->set_marking_code(this->get_marking_code());
        this->m_device_dao->set_db_name(this->m_db_name);
        this->m_device_dao->open();
        this->m_is_db_opened = ZOO_TRUE;
		ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," <funtion exit ...");
    }

    /**
     *@brief Disconnect db
    **/
    void DEVICE_ABSTRCT_MODEL_CLASS::terminate()
    {
        if(this->m_device_dao != nullptr)
        {
            if(this->m_is_db_opened)
                this->m_device_dao->close();
        }
        this->m_is_db_opened = ZOO_FALSE;
    }

    /**
     *@brief Enable or disable the device
     *@return true or false
    **/
    void DEVICE_ABSTRCT_MODEL_CLASS::set_enable(ZOO_BOOL enable)
    {
        this->m_device_enable = enable;
    }

    /**
     *@brief Get enabled
     *@return true or false
    **/
    ZOO_BOOL DEVICE_ABSTRCT_MODEL_CLASS::get_enable()
    {
        return this->m_device_enable;
    }

    /**
     *@brief Enabled the device
     *@return true or false
    **/
    void DEVICE_ABSTRCT_MODEL_CLASS::set_db_name(std::string db_name)
    {
        this->m_db_name = db_name;
    }

    /**
     *@brief Set device dao
     *@param device_dao
    **/
    void DEVICE_ABSTRCT_MODEL_CLASS::set_device_dao(IN boost::shared_ptr<DAO_INTERFACE> device_dao)
    {
        //this->m_device_dao = device_dao;
    }

    /**
     *@brief Publish alarm trigged
     *@param message
    **/
    void DEVICE_ABSTRCT_MODEL_CLASS::publish(IN boost::shared_ptr<ERROR_MESSAGE_CLASS> message)
    {
        EH4A_ALARM_STRUCT status = EH4A_ALARM_STRUCT();
        memset(&status,0,sizeof(EH4A_ALARM_STRUCT));
        status.alarm_id = message->get_alarm_id();
        status.type = static_cast<EH4A_ERROR_TYPE_ENUM>(message->get_alarm_type());
        status.is_triggered = message->get_trigged_state();
        status.level = static_cast<EH4A_ERROR_LEVEL_ENUM>(message->get_alarm_level());        
        status.cloud_enabled =  message->get_oss_enabled();
        status.cloud_error_code = message->get_oss_error_code();
        
        std::string componet_id = message->get_componet_id();
        if(componet_id.size() <= EH4A_BUFFER_LENGTH)
            memcpy(status.component,componet_id.data(),componet_id.size());

        std::string material_id = message->get_material_id();
        if(material_id.size() <= EH4A_BUFFER_LENGTH)
            memcpy(status.material_id,material_id.data(),material_id.size());

        std::string english = message->get_english_description();
        if(english.size() <= EH4A_DISPLAY_LENGTH)
            memcpy(status.description_en,english.data(),english.size());

        std::string chinese = message->get_chinese_description();
        if(chinese.size() <= EH4A_DISPLAY_LENGTH)
            memcpy(status.description_cn,chinese.data(),chinese.size());

        std::string trigger_time = message->get_trigger_time();
        if(trigger_time.size() <= EH4A_BUFFER_LENGTH)
            memcpy(status.trigger_time,trigger_time.data(),trigger_time.size());

        std::string recover_time = message->get_recover_time();
        if(recover_time.size() <= EH4A_BUFFER_LENGTH)
            memcpy(status.recover_time,recover_time.data(),recover_time.size());

        EHMA_raise_4A_alarm_info_subscribe(&status,this->m_internal_error,NULL);
    }

    /**
     *@brief Set internal error for subscribe
    **/
    void DEVICE_ABSTRCT_MODEL_CLASS::set_internal_error(ZOO_INT32 error_code)
    {
        this->m_internal_error = error_code;
    }

    /**
     *@brief Get internal error
    **/
    ZOO_INT32 DEVICE_ABSTRCT_MODEL_CLASS::get_internal_error()
    {
        return this->m_internal_error;
    }

    /**
     *@brief Get dao instance
    **/
    boost::shared_ptr<DEVICE_DAO_MODEL_CLASS> DEVICE_ABSTRCT_MODEL_CLASS::get_dao()
    {
        return this->m_device_dao;
    }

    /**
     *@brief get error data from dao
     *@param error_code
    **/
    boost::shared_ptr<ERROR_MESSAGE_CLASS> DEVICE_ABSTRCT_MODEL_CLASS::get_alarm_definitions(ZOO_INT32 error_code)
    {
        if(!this->m_device_dao->is_opened())
            this->m_device_dao->open();
        return this->m_device_dao->query(error_code);
    }

    /**
     *@brief Get all config alarms
     *@param alarm vector
    **/
    std::vector<EH4A_ALARM_STRUCT * > DEVICE_ABSTRCT_MODEL_CLASS::get_all_cfg_alarms()
    {
        if(!this->m_device_dao->is_opened())
            this->m_device_dao->open();
        return this->m_device_dao->find_all_datas();
    }

    /**
     *@brief Get all row counts
     *@param alarm vector
    **/
    ZOO_INT32 DEVICE_ABSTRCT_MODEL_CLASS::get_row_counts()
    {
        if(!this->m_device_dao->is_opened())
            this->m_device_dao->open();
        return this->m_device_dao->get_row_counts();
    }
}
