/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : DEVICE_DAO_MODEL_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "DEVICE_DAO_MODEL_CLASS.h"
#include <sstream>
#include <boost/algorithm/string.hpp>
namespace ZOO_EH
{
    DEVICE_DAO_MODEL_CLASS::DEVICE_DAO_MODEL_CLASS(std::string db_name)
    :m_opened(ZOO_FALSE)
    ,m_db_name(db_name)
    ,m_table_name("Code")
    ,m_sqlite_dao(*soci::factory_sqlite3())
    {

    }

    DEVICE_DAO_MODEL_CLASS::~DEVICE_DAO_MODEL_CLASS()
    {
        //dtor
    }

    void DEVICE_DAO_MODEL_CLASS::set_db_name(std::string name)
    {
        this->m_db_name = name;
    }

    /**
     *@brief Connect db
    **/
    void DEVICE_DAO_MODEL_CLASS::open()
    {
        ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," >funtion entry ...");
		if(this->m_opened)
            return;
        try
        {
        	ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," db_name:%s",this->m_db_name.c_str());
            this->m_session.reset(new soci::session("sqlite3",this->m_db_name));
            this->m_opened = ZOO_TRUE;
        }
        catch(const soci::sqlite3_soci_error & e)
        {
            this->m_opened = ZOO_FALSE;
			std::ostringstream os;
			os << "Can not open " << this->m_db_name << ":" << e.what();
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,os.str().c_str(),nullptr);
        }
        catch(const soci::soci_error & e)
        {
            std::ostringstream os;
			os << "Can not open " << this->m_db_name << ":" << e.what();
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,os.str().c_str(),nullptr);
        }
		ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," <funtion exit ...");
    }

    /**
     *@brief DAO has opened
    **/
    ZOO_BOOL DEVICE_DAO_MODEL_CLASS::is_opened()
    {
        return this->m_opened;
    }

    /**
     *@brief close db
    **/
    void DEVICE_DAO_MODEL_CLASS::close()
    {
        this->m_session.reset();
    }

    /**
     *@brief Create db table
    **/
    void DEVICE_DAO_MODEL_CLASS::create_table()
    {
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }

        try
        {
            auto & sql_session = *this->m_session.get();
            std::ostringstream os;
            os << "DROP TABLE " << this->m_table_name <<" IF EXISTS";
            sql_session << os.str();
            os.str("");

            //create
            os << "CREATE TABLE " << this->m_table_name << " (";
            os << "CODE TEXT NOT NULL,"
                  "TYPE TEXT NOT NULL,"
                  "LEVEL INTERGER,"
                  "ENGLISH TEXT NOT NULL,"
                  "CHINESE TEXT NOT NULL"
                  "CLOUD_ENABLED TEXT,"
                  "CLOUD_CODE TEXT,";
            os << " )";
            sql_session << os.str();
        }
        catch( soci::sqlite3_soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
        catch( soci::soci_error & e)
        {
            std::ostringstream os;
			os << "create_table " << this->m_table_name << ":" << e.what();
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,os.str().c_str(),nullptr);
        }
    }

    /**
     *@brief Drop table
    **/
    void DEVICE_DAO_MODEL_CLASS::drop_table()
    {
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }

        try
        {
            std::ostringstream os;
            os << "DROP TABLE " <<  this->m_table_name <<" IF EXISTS";
            auto & sql_session = *this->m_session.get();
            sql_session << os.str();
        }
        catch( soci::sqlite3_soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
        catch( soci::soci_error & e)
        {
            std::ostringstream os;
			os << "drop_table " << this->m_table_name << ":" << e.what();
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,os.str().c_str(),nullptr);
        }
    }

    /**
     *@brief Update alarm db table
    **/
    void DEVICE_DAO_MODEL_CLASS::insert(ZOO_INT32 error_code,
                                        std::string type,
                                        ZOO_INT32 level,
                                        std::string english,
                                        std::string chinese,
				                        ZOO_BOOL oss_enabled ,
				                        ZOO_INT32 oss_error_code)
    {
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }

        try
        {
            std::string table_name =  this->m_table_name;
            std::string cloud_enabaled = oss_enabled ==ZOO_TRUE ? "Y":"N";
         
            std::ostringstream os;
            os << "INSERT INTO " << table_name <<"(";
            os << "CODE,TYPE,LEVEL,ENGLISH,CHINESE,CLOUD_ENABLED,CLOUD_CODE) ";
            os << "VALUES(:error_code, :type, :level, :english, :chinese, :cloud_enabaled, :oss_error_code)",
                    soci::use(error_code),soci::use(type),
                    soci::use(level),soci::use(english),
                    soci::use(chinese),soci::use(cloud_enabaled),soci::use(oss_error_code);
            auto & sql_session = *this->m_session.get();
            sql_session << os.str();
        }
        catch( soci::sqlite3_soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
        catch( soci::soci_error & e)
        {
            std::ostringstream os;
			os << "insert " << this->m_table_name << ":" << e.what();
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,os.str().c_str(),nullptr);
        }
    }


    /**
     *@brief Update alarm db table
    **/
    void DEVICE_DAO_MODEL_CLASS::update(ZOO_INT32 error_code,
                                        std::string type,
                                        ZOO_INT32 level,
                                        std::string english,
                                        std::string chinese,
				                        ZOO_BOOL oss_enabled ,
				                        ZOO_INT32 oss_error_code)
    {
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }

        try
        {
            std::string table_name =  this->m_table_name;
            std::string cloud_enabaled = oss_enabled ==ZOO_TRUE ? "Y":"N";
            std::ostringstream os;
            os << "UPDATE " << table_name <<" ";
            os << "SET CODE = :error_code,TYPE = :type,LEVEL = :level,CLOUD_ENABLED = :oss,CLOUD_CODE = :oss_error_code,ENGLISH = :english,CHINESE = :chinese ";
            os << "WHERE ",soci::use(error_code),soci::use(type),soci::use(level),
            				soci::use(cloud_enabaled),soci::use(oss_error_code),
                            soci::use(english),soci::use(chinese);
            auto & sql_session = *this->m_session.get();
            sql_session << os.str();
        }
        catch( soci::sqlite3_soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
        catch( soci::soci_error & e)
        {
            std::ostringstream os;
			os << "update " << this->m_table_name << ":" << e.what();
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,os.str().c_str(),nullptr);
        }
    }

    /**
     *@brief Delete table
    **/
    void DEVICE_DAO_MODEL_CLASS::remove(ZOO_INT32 error_code)
    {
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }

        try
        {
            std::string table_name =  this->m_table_name;
            std::ostringstream os;
            os << "DELETE FROM " << table_name <<" WHERE error_code = " << error_code;
            auto & sql_session = *this->m_session.get();
            sql_session << os.str();;
        }
        catch( soci::sqlite3_soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
        catch( soci::soci_error & e)
        {
            std::ostringstream os;
			os << "remove " << this->m_table_name << ":" << e.what();
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,os.str().c_str(),nullptr);
        }
    }

    /**
     *@brief Truncate table
    **/
    void DEVICE_DAO_MODEL_CLASS::truncate_table()
    {
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }
        std::ostringstream os;
        std::string table_name =  this->m_table_name;
        os << "DELETE FROM " << table_name <<" WHERE 1=1";
        auto & sql_session = *this->m_session.get();
        sql_session << os.str();
    }

    /**
     *@brief Get data from device dao
    **/
    boost::shared_ptr<ERROR_MESSAGE_CLASS> DEVICE_DAO_MODEL_CLASS::query(ZOO_INT32 error_code)
    {
        if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }
        boost::shared_ptr<ERROR_MESSAGE_CLASS> message;
        try
        {
            std::string componet = ERROR_CODE_CONVERTER_CLASS::convert_to_componet_id(error_code);
            std::string table_name =  this->m_table_name;
            message.reset(new ERROR_MESSAGE_CLASS());
            std::ostringstream os;
            std::string type;
            std::string level;
            std::string english;
            std::string chinese;
            std::string alarm_id;
            std::string cloud_enabled;
            std::string cloud_error_code;
            alarm_id = DAO_MODEL_ABSTRACT_CLASS::convert_to_hex_string_upper(error_code);
            os << "SELECT TYPE, LEVEL,CLOUD_ENABLED,CLOUD_CODE, ENGLISH, CHINESE FROM " ;
            os << table_name <<" WHERE CODE = :alarm_id";
            auto & sql_session = *this->m_session.get();
            sql_session << os.str(),
            	soci::use(alarm_id),
            	soci::into(type),
            	soci::into(level),
            	soci::into(cloud_enabled),
            	soci::into(cloud_error_code),
            	soci::into(english),
            	soci::into(chinese);
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: type:%s",type.c_str());
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: level:%s",level.c_str());
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: cloud_enabled:%s",cloud_enabled.c_str());
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: cloud_error_code:%s",cloud_error_code.c_str());
            
            message->set_alarm_type(STRING_UTILITY_CLASS::to_enum(type.c_str()));
            message->set_alarm_level(std::atoi(level.c_str()));
            message->set_cloud_enabled(cloud_enabled == "Y" ? ZOO_TRUE:ZOO_FALSE);
            message->set_cloud_error_code(std::atoi(cloud_error_code.c_str()));
            message->set_english_description(english);
            message->set_chinese_description(chinese);
            message->set_componet_id(componet);
        }
        catch( soci::sqlite3_soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
        catch( soci::soci_error & e)
        {
            std::ostringstream os;
			os << "query " << this->m_table_name << ":" << e.what();
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,os.str().c_str(),nullptr);
        }
        return message;
    }

    /**
     *@brief find all datas
    **/
    std::vector<EH4A_ALARM_STRUCT *> DEVICE_DAO_MODEL_CLASS::find_all_datas()
    {
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "> function entry ...");
    	std::vector<EH4A_ALARM_STRUCT *> items;
		if(this->m_session == nullptr)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,"session pointer is null ...ERROR",nullptr);
        }
        
		try
        {
        	auto & sql_session = *this->m_session.get();
        	std::ostringstream os;
            os <<  "SELECT * FROM " <<  this->m_table_name;
        	soci::rowset<soci::row> rs = sql_session.prepare << os.str();
			for (const soci::row & r : rs)
			{
				EH4A_ALARM_STRUCT * data = new EH4A_ALARM_STRUCT();
				try
				{
    				std::string error_code_string = r.get<std::string>(0);
    				data->alarm_id = this->convert_to_hex_string_to_int(error_code_string);
				}
				catch(std::bad_cast & e)
				{
				    ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: alarm id string cast fail:%s",e.what());
				    data->alarm_id = r.get<int>(0);
				}
				ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: alarm id is %d",data->alarm_id);
				std::string component = this->convert_to_component(data->alarm_id);
				memcpy(data->component,component.c_str(),component.size());
				data->component[EH4A_BUFFER_LENGTH-1] = '\0';
				ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: component is %s",data->component);
				std::string type = r.get<std::string>(1);
				ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: type is %s",type.data());
				data->type  = this->converter(type);
				data->level = this->convert_error_level_from(r.get<int>(2));
				ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: level is %d",data->level);
				std::string english = r.get<std::string>(3);
				memcpy(data->description_en,english.c_str(),english.size());
				ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: discription_en is %s",data->description_en);
				std::string chinese = r.get<std::string>(4);
				memcpy(data->description_cn,chinese.c_str(),chinese.size());
				ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: discription_cn is %s",data->description_cn);
				data->cloud_enabled = r.get<std::string>(5) == "Y" ? ZOO_TRUE : ZOO_FALSE;
				if(data->cloud_enabled)
				    data->cloud_error_code  = std::stoi(r.get<std::string>(6));
				items.emplace_back(data);
 			}
		}
        catch(soci::sqlite3_soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
        catch(soci::soci_error & e)
        {
            std::ostringstream os;
			os << "find_all_datas " << this->m_table_name << ":" << e.what();
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,os.str().c_str(),nullptr);
        }
        catch(std::exception & e)
        {
            std::ostringstream os;
			os << "find datas from table <" << this->m_table_name << "> :" << e.what();
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,os.str().c_str(),nullptr);
        }
        catch(...)
        {
            ZOO_slog(IN ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__, ":: unhandle exception ...ERROR");
        }
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...");
    	return items;
    }

    /**
     *@brief count rows
    **/
    ZOO_INT32 DEVICE_DAO_MODEL_CLASS::get_row_counts()
    {
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "> function entry ...");
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: component:%s",this->get_marking_code());
    	ZOO_INT32 count = 0;
    	try 
    	{
		    auto & sql_session = *this->m_session.get();
		    sql_session << "SELECT COUNT(*) FROM " << this->m_table_name, soci::into(count);
            ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: sql:%s",sql_session.get_query().data());
		}
		catch(soci::sqlite3_soci_error & e)
        {
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,e.what(),nullptr);
        }
        catch(soci::soci_error & e)
        {
            std::ostringstream os;
			os << "get_row_counts " << this->m_table_name << ":" << e.what();
            __THROW_EH_EXCEPTION(EH4A_PARAMETER_ERR,os.str().c_str(),nullptr);
        }
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: find count is %d",count);
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...");
		return count;
    }
    EH4A_ERROR_LEVEL_ENUM DEVICE_DAO_MODEL_CLASS::convert_error_level_from(int error_level)
    {
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "> function entry ...");
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, ":: error_level:%d",error_level);
        EH4A_ERROR_LEVEL_ENUM level = EH4A_ERROR_LEVEL_MAX;
        
        switch(error_level)
        {
            case 1 : level  = EH4A_ERROR_LEVEL_1;break;
            case 2 : level  = EH4A_ERROR_LEVEL_2;break;
            case 3 : level  = EH4A_ERROR_LEVEL_3;break;
            default:break;
        }
        ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, "< function exit ...");
        return level;
    }
}
