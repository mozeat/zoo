/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : EHMA_executor_wrapper.c
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include <EHMA_executor_wrapper.h>
#include <ZOO_COMMON_MACRO_DEFINE.h>
#include "LOG_MANAGER_CLASS.h"
#include "EH_CONFIGURE.h"
#include "ALARM_DAO_CLASS.h"
#include "PROCESSING_FLOW_FACADE_CLASS.h"
#include "DEVICE_CONTROLLER_CLASS.h"
#include <signal.h>
#include <boost/stacktrace.hpp>

/**
 * @brief Define processing flow facade
 */
boost::shared_ptr<ZOO_EH::PROCESSING_FLOW_FACADE_INTERFACE> g_processing_flow_facade;

/**
 * @brief Define device controller
 */
boost::shared_ptr<ZOO_EH::DEVICE_CONTROLLER_INTERFACE> g_device_controller;

/**
 * @brief Define alarm data access model
 */
boost::shared_ptr<ZOO_EH::DAO_INTERFACE> g_alarm_dao;

/**
 * @brief Define log manager
 */
boost::shared_ptr<ZOO_EH::LOG_MANAGER_INTERFACE> g_log_manager;

void default_signal_handler(IN int signal_id)
{
    ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," > function entry ...");
    ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," receive signal[%d] ...",signal_id);
    signal(signal_id, SIG_DFL);
    ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," recover signal[%d] to default handler ...",signal_id);
    std::stringstream os;
    os << boost::stacktrace::stacktrace();
    ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," stack trace: %s",os.str().c_str());

    std::string dump_core_name = "./EHMA.dump";
    if (boost::filesystem::exists(dump_core_name.data()))
    {
        boost::filesystem::remove(dump_core_name.data());
    }
    boost::stacktrace::safe_dump_to(dump_core_name.data());
    ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," < function exit ...");
    return;
}

/**
 * @brief Register system signal handler,throw PARAMETER_EXCEPTION_CLASS if register fail,
 * the default signal handling is save stack trace to the log file and generate a dump file at execute path.
 * register a self-defined callback to SYSTEM_SIGNAL_HANDLER::resgister_siganl will change the default behavior.
**/
static void EHMA_register_system_signals()
{
    signal(SIGSEGV,default_signal_handler);

    /* Add more signals if needs,or register self-defined callback function
       to change the default behavior... */
}

/**
 *@brief Execute the start up flow.
 * This function is executed in 3 steps:
 * Step 1: Load configurations
 * Step 2: Create controllers
 * Step 3: Create facades and set controllers to created facades
**/
void EHMA_startup(void)
{
    try
    {
        /**
         * @brief Signal handler for system behavior
        */
        EHMA_register_system_signals();

        /**
         *@brief Step 1: Load configurations
        */
        ZOO_EH::EH_CONFIGURE::initialize();
        ZOO_EH::LOG_MANAGER_CLASS::initialize();
		
        /**
         *@brief Step 2: Create controllers
        */
        g_log_manager.reset(new ZOO_EH::LOG_MANAGER_CLASS());
        g_alarm_dao.reset(new ZOO_EH::ALARM_DAO_CLASS());
        g_device_controller.reset(new ZOO_EH::DEVICE_CONTROLLER_CLASS());
        g_processing_flow_facade.reset(new ZOO_EH::PROCESSING_FLOW_FACADE_CLASS());

        /**
         *@brief Step 3: Create facades and set controllers to created facades
        */
        g_processing_flow_facade->set_device_controller(g_device_controller);
        g_processing_flow_facade->set_alarm_dao(g_alarm_dao);
        g_processing_flow_facade->set_log_manager(g_log_manager);
		ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"initialize device controller ...BEGIN");
        g_device_controller->initialize();
		
		ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,"initialize device controller ...END");
        std::string alarm_subscribe_path = ZOO_EH::EH_CONFIGURE::get_instance()->get_alarm_db_path();
        g_alarm_dao->set_db_name(alarm_subscribe_path + "/" + "ZOO_ALARMS.db");
        g_alarm_dao->open();
    }
	catch(const ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & e)
    {
        ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__,e.what());
    }
    catch(...)
    {
        ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," unhandle exception ...ERROR");
    }
}

/**
 *@brief This function response to release instance or memory
**/
void EHMA_shutdown(void)
{
    /** User add */
    try
    {
        g_processing_flow_facade.reset();
    }
    catch(...)
    {
        ZOO_EH::LOG_MANAGER_CLASS::debug(__ZOO_FUNC__," unhandle exception ...ERROR");
    }
}

/**
 *@brief Subscribe events published from hardware drivers
**/
void EHMA_subscribe_driver_event(void)
{
    /** Subscribe events */
}

