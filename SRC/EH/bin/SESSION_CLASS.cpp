/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : SESSION_CLASS.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "SESSION_CLASS.h"
#include <vector>
namespace ZOO_EH
{
    SESSION_CLASS::SESSION_CLASS()
    {
        this->m_session_id = this->generate_uuid();
    }
    
    SESSION_CLASS::SESSION_CLASS(std::vector<EH4A_ALARM_STRUCT * > & alarms)
    {
        this->m_session_id = this->generate_uuid();
        this->save_cache(alarms);
    }
    
    SESSION_CLASS::~SESSION_CLASS()
    {
        this->clear();
    }

    /**
     * @brief Save alarm cache
     * @param alarms   the cache content
     **/
    void  SESSION_CLASS::save_cache(IN std::vector<EH4A_ALARM_STRUCT * > & alarms)
    {
        this->clear();
       this->m_cache = alarms;
    }

    /**
     *@brief Get alarm cache
     *@return 
    **/
    const std::vector<EH4A_ALARM_STRUCT * > &  SESSION_CLASS::get_cache()
    {
        return this->m_cache;
    }

    /**
     *@brief Get alarm cache by index
     *@param index
    **/
    EH4A_ALARM_STRUCT *  SESSION_CLASS::get_cache(IN ZOO_INT32 index)
    {
        EH4A_ALARM_STRUCT * alarm_data = NULL;
        if(static_cast<unsigned int>(index) < this->m_cache.size())
        {
            alarm_data = this->m_cache[index];
        }
        return alarm_data;
    }

    /**
     *@brief Clear and remove
    **/
    void  SESSION_CLASS::clear()
    {
        for(std::vector<EH4A_ALARM_STRUCT * >::iterator ite = this->m_cache.begin();
            ite != this->m_cache.end();
            ite ++)
        {
            EH4A_ALARM_STRUCT * alarm_data = *ite;
            if(NULL != alarm_data)
            {
                delete alarm_data;
                alarm_data = NULL;
            }
        }
        this->m_cache.clear();
    }

    /**
     *@brief Clear and remove
    **/
    ZOO_INT32  SESSION_CLASS::get_session_id()
    {
        return this->m_session_id;
    }

    ZOO_INT32 SESSION_CLASS::generate_uuid()
    {
        static ZOO_INT32 i = 1;
        return ++ i;
    }
}
