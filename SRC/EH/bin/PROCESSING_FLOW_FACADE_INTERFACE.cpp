/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : PROCESSING_FLOW_FACADE_INTERFACE.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "PROCESSING_FLOW_FACADE_INTERFACE.h"
namespace ZOO_EH
{
    PROCESSING_FLOW_FACADE_INTERFACE::PROCESSING_FLOW_FACADE_INTERFACE()
    {
        //ctor
    }

    PROCESSING_FLOW_FACADE_INTERFACE::~PROCESSING_FLOW_FACADE_INTERFACE()
    {
        //dtor
    }
}

