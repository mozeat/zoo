/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : EH4A.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-05    Generator      created
*************************************************************/

#include <ZOO.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include <EH4A_if.h>
//#include <TR4A_if.h>
#include <EH4I_type.h>
#include <EH4A_type.h>
#include <EH4I_if.h>
/**
 *@brief EH4A_show_exception
 *@param component_id
 *@param file
 *@param function_name
 *@param line
 *@param error_code
 *@param link_error_code
 *@param info
**/
ZOO_INT32 EH4A_show_exception(IN const ZOO_CHAR * component_id,
	        								        IN const ZOO_CHAR * file,
	        								        IN const ZOO_CHAR * function_name,
	        								        IN ZOO_INT32 line,
	        								        IN ZOO_INT32 error_code,
													IN ZOO_INT32 link_error_code,
	        								        IN const ZOO_CHAR * info)
{
    ZOO_INT32 result = OK;
    EH4I_REQUEST_STRUCT *request_message = NULL; 
    EH4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = EH4A_SHOW_EXCEPTION_CODE;
    result = EH4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (EH4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = EH4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (EH4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(request_message->request_body.show_exception_req_msg.component_id),component_id,strlen(component_id)  +  1);
        memcpy((void *)(request_message->request_body.show_exception_req_msg.file),file,strlen(file) + 1);
        memcpy((void *)(request_message->request_body.show_exception_req_msg.function_name),function_name,strlen(function_name) + 1);
        memcpy((void *)(&request_message->request_body.show_exception_req_msg.line),&line,sizeof(ZOO_INT32));
        memcpy((void *)(&request_message->request_body.show_exception_req_msg.error_code),&error_code,sizeof(ZOO_INT32));
        memcpy((void *)(&request_message->request_body.show_exception_req_msg.link_error_code),&link_error_code,sizeof(ZOO_INT32));
        memcpy((void *)(request_message->request_body.show_exception_req_msg.info),info,strlen(info) + 1);
    }
    if(OK == result)
    {
        result = EH4I_send_request_and_reply(EH4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief EH4A_set_notification
 *@param info
**/
ZOO_INT32 EH4A_set_notification(IN EH4A_NOTIFY_INFO_STRUCT * info)
{
    ZOO_INT32 result = OK;
    EH4I_REQUEST_STRUCT *request_message = NULL; 
    EH4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = EH4A_SET_NOTIFICATION_CODE;
    result = EH4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (EH4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = EH4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (EH4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.set_notification_req_msg.info),info,sizeof(EH4A_NOTIFY_INFO_STRUCT));
    }

    if(OK == result)
    {
        result = EH4I_send_request_and_reply(EH4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief EH4A_set_alarm_with_material_id
 *@param material_id
 *@param alarm_id
**/
 ZOO_INT32 EH4A_set_alarm_with_material_id(IN const ZOO_CHAR* material_id,
																		IN ZOO_INT32 alarm_id)
{
    ZOO_INT32 result = OK;
    EH4I_REQUEST_STRUCT *request_message = NULL; 
    EH4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = EH4A_SET_ALARM_WITH_MATERIAL_ID_CODE;
    result = EH4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (EH4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = EH4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (EH4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(request_message->request_body.set_alarm_with_material_id_req_msg.material_id),material_id,strlen(material_id) + 1);
        memcpy((void *)(&request_message->request_body.set_alarm_with_material_id_req_msg.alarm_id),&alarm_id,sizeof(ZOO_INT32));
    }

    if(OK == result)
    {
        result = EH4I_send_request_and_reply(EH4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
     MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief EH4A_set_alarm
 *@param alarm_id
**/
ZOO_INT32 EH4A_set_alarm(IN ZOO_INT32 alarm_id)
{
    ZOO_INT32 result = OK;
    EH4I_REQUEST_STRUCT *request_message = NULL; 
    EH4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = EH4A_SET_ALARM_CODE;
    result = EH4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (EH4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = EH4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (EH4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.set_alarm_req_msg.alarm_id),&alarm_id,sizeof(ZOO_INT32));
    }

    if(OK == result)
    {
        result = EH4I_send_request_and_reply(EH4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief EH4A_clear_alarm
 *@param alarm_id
**/
ZOO_INT32 EH4A_clear_alarm(IN ZOO_INT32 alarm_id)
{
    ZOO_INT32 result = OK;
    EH4I_REQUEST_STRUCT *request_message = NULL; 
    EH4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = EH4A_CLEAR_ALARM_CODE;
    result = EH4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (EH4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = EH4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (EH4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.clear_alarm_req_msg.alarm_id),&alarm_id,sizeof(ZOO_INT32));
    }

    if(OK == result)
    {
        result = EH4I_send_request_and_reply(EH4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief EH4A_clear_all_alarms
**/
ZOO_INT32 EH4A_clear_all_alarms(void)
{
    ZOO_INT32 result = OK;
    EH4I_REQUEST_STRUCT *request_message = NULL; 
    EH4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = EH4A_CLEAR_ALL_ALARMS_CODE;
    result = EH4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (EH4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = EH4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (EH4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
    }

    if(OK == result)
    {
        result = EH4I_send_request_and_reply(EH4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief EH4A_get_alarm_info
 *@param alarm_id
 *@param details
**/
ZOO_INT32 EH4A_get_alarm_info(IN ZOO_INT32 alarm_id,
													INOUT EH4A_ALARM_DETAILS_STRUCT * details)
{
    ZOO_INT32 result = OK;
    EH4I_REQUEST_STRUCT *request_message = NULL; 
    EH4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = EH4A_GET_ALARM_INFO_CODE;
    result = EH4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (EH4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = EH4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (EH4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.get_alarm_info_req_msg.alarm_id),&alarm_id,sizeof(ZOO_INT32));
    }

    if(OK == result)
    {
        result = EH4I_send_request_and_reply(EH4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(details,&reply_message->reply_body.get_alarm_info_rep_msg.details,sizeof(EH4A_ALARM_DETAILS_STRUCT));
    }

    if(request_message != NULL)
    {
    	MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief EH4A_find_alarm_counts
 *@param counts
**/
ZOO_INT32 EH4A_find_alarm_counts(IN EH4A_FOUND_TYPE_ENUM found_type,
                                                                  INOUT ZOO_INT32 * counts,
                                                                  INOUT ZOO_INT32 *session_id)
{
    ZOO_INT32 result = OK;
    EH4I_REQUEST_STRUCT *request_message = NULL; 
    EH4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = EH4A_FIND_ALARM_COUNTS_CODE;
    result = EH4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (EH4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = EH4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (EH4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.find_alarm_counts_req_msg.found_type),&found_type,sizeof(EH4A_FOUND_TYPE_ENUM));
    }

    if(OK == result)
    {
        result = EH4I_send_request_and_reply(EH4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(counts,&reply_message->reply_body.find_alarm_counts_rep_msg.counts,sizeof(ZOO_INT32));
        memcpy(session_id,&reply_message->reply_body.find_alarm_counts_rep_msg.session_id,sizeof(ZOO_INT32));
    }

    if(request_message != NULL)
    {
    	MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief EH4A_get_alarm_data
 *@param session_id
 *@param index
 *@param data
**/
ZOO_INT32 EH4A_get_alarm_data(IN ZOO_INT32 session_id,
                                                IN ZOO_INT32 index,
												INOUT EH4A_ALARM_STRUCT * data)
{
    ZOO_INT32 result = OK;
    EH4I_REQUEST_STRUCT *request_message = NULL; 
    EH4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = EH4A_GET_ALARM_DATA_CODE;
    result = EH4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (EH4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = EH4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (EH4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
        memcpy((void *)(&request_message->request_body.get_alarm_data_req_msg.session_id),&session_id,sizeof(ZOO_INT32));
        memcpy((void *)(&request_message->request_body.get_alarm_data_req_msg.index),&index,sizeof(ZOO_INT32));
    }

    if(OK == result)
    {
        result = EH4I_send_request_and_reply(EH4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(data,&reply_message->reply_body.get_alarm_data_rep_msg.data,sizeof(EH4A_ALARM_STRUCT));
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

static void EH4A_alarm_info_callback(void *context_p, MQ4A_CALLBACK_STRUCT *local_proc, void *msg)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 error_code = OK;
    EH4I_REPLY_STRUCT *reply_msg = NULL;
    EH4I_ALARM_INFO_SUBSCRIBE_CODE_CALLBACK_STRUCT * callback_struct = NULL;
    ZOO_INT32 rep_length = 0;

    if(msg == NULL)
    {
        result = EH4A_PARAMETER_ERR;
    }

    if(OK == result)
    {
        result = EH4I_get_reply_message_length(EH4A_ALARM_INFO_SUBSCRIBE_CODE, &rep_length);
        if(OK != result)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        reply_msg = (EH4I_REPLY_STRUCT * )MM4A_malloc(rep_length);
        if(OK != result)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        memcpy(reply_msg, msg, rep_length);
        if (EH4A_ALARM_INFO_SUBSCRIBE_CODE != reply_msg->reply_header.function_code)
        {
            result = EH4A_PARAMETER_ERR;
        }
        error_code = reply_msg->reply_header.execute_result;
        //memcpy((void*)&status, &reply_msg->reply_body.alarm_info_subscribe_code_rep_msg.status, sizeof(EH4A_ALARM_STRUCT));
        callback_struct = (EH4I_ALARM_INFO_SUBSCRIBE_CODE_CALLBACK_STRUCT*) local_proc;
        ((EH4A_ALARM_CALLBACK_FUNCTION)callback_struct->callback_function)(&reply_msg->reply_body.alarm_info_subscribe_code_rep_msg.status,error_code,context_p);
    }

    if(reply_msg != NULL)
    {
        MM4A_free(reply_msg);
    }
}


 ZOO_INT32 EH4A_alarm_info_subscribe(IN EH4A_ALARM_CALLBACK_FUNCTION callback_function,
												OUT ZOO_UINT32 *handle,
												INOUT void *context)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = 0;
    MQ4A_CALLBACK_STRUCT* callback = NULL;
    if (NULL == callback_function)
    {
        result = EH4A_PARAMETER_ERR;
        return result;
    }

    callback = (MQ4A_CALLBACK_STRUCT*)MM4A_malloc(sizeof(EH4I_ALARM_INFO_SUBSCRIBE_CODE_CALLBACK_STRUCT));
    if (NULL == callback)
    {
        result = EH4A_PARAMETER_ERR;
    }

    if (OK == result)
    {
        callback->callback_function = callback_function;
        event_id = EH4A_ALARM_INFO_SUBSCRIBE_CODE;
        result = EH4I_send_subscribe(EH4A_SERVER,
                                      EH4A_alarm_info_callback,
                                      callback,
                                      event_id,
                                      (ZOO_HANDLE*)handle,
                                      context);
        if (OK != result)
        {
           result = EH4A_PARAMETER_ERR;
           MM4A_free(callback);
        }
    }

    return result;
}

ZOO_INT32 EH4A_alarm_info_unsubscribe(IN ZOO_UINT32 handle)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = EH4A_ALARM_INFO_SUBSCRIBE_CODE;
    if(OK == result)
    {
        result = EH4I_send_unsubscribe(EH4A_SERVER,event_id,handle);
    }
    return result;
}

static void EH4A_notify_info_callback(void *context_p, MQ4A_CALLBACK_STRUCT *local_proc, void *msg)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 error_code = OK;
    EH4I_REPLY_STRUCT *reply_msg = NULL;
    EH4I_NOTIFY_INFO_SUBSCRIBE_CODE_CALLBACK_STRUCT * callback_struct = NULL;
    ZOO_INT32 rep_length = 0;
    //EH4A_NOTIFY_INFO_STRUCT notify_info;
    //memset(&notify_info,0,sizeof(EH4A_NOTIFY_INFO_STRUCT));
    if(msg == NULL)
    {
        result = EH4A_PARAMETER_ERR;
    }

    if(OK == result)
    {
        result = EH4I_get_reply_message_length(EH4A_NOTIFY_INFO_SUBSCRIBE_CODE, &rep_length);
        if(OK != result)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        reply_msg = (EH4I_REPLY_STRUCT * )MM4A_malloc(rep_length);
        if(NULL != reply_msg)
        {
            result = EH4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        memcpy(reply_msg, msg, rep_length);
        if (EH4A_NOTIFY_INFO_SUBSCRIBE_CODE != reply_msg->reply_header.function_code)
        {
            result = EH4A_PARAMETER_ERR;
        }
        error_code = reply_msg->reply_header.execute_result;
       // memcpy((void*)&notify_info, &reply_msg->reply_body.notify_info_subscribe_code_rep_msg.notify_info, sizeof(EH4A_NOTIFY_INFO_STRUCT));
        callback_struct = (EH4I_NOTIFY_INFO_SUBSCRIBE_CODE_CALLBACK_STRUCT*) local_proc;
        ((EH4A_NOTIFY_CALLBACK_FUNCTION)callback_struct->callback_function)(&reply_msg->reply_body.notify_info_subscribe_code_rep_msg.notify_info,error_code,context_p);
    }

    if(reply_msg != NULL)
    {
        MM4A_free(reply_msg);
    }
}


 ZOO_INT32 EH4A_notify_info_subscribe(IN EH4A_NOTIFY_CALLBACK_FUNCTION callback_function,
												OUT ZOO_UINT32 *handle,
												INOUT void *context)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = 0;
    MQ4A_CALLBACK_STRUCT* callback = NULL;
    if (NULL == callback_function)
    {
        result = EH4A_PARAMETER_ERR;
        return result;
    }

    /*function entry*/
    //TR4A_trace(EH4I_COMPONET_ID, __ZOO_FUNC__, "> function entry ... ");
    /*fill callback strcut*/
    callback = (MQ4A_CALLBACK_STRUCT*)MM4A_malloc(sizeof(EH4I_NOTIFY_INFO_SUBSCRIBE_CODE_CALLBACK_STRUCT));
    if (NULL == callback)
    {
        result = EH4A_PARAMETER_ERR;
    }

    if (OK == result)
    {
        callback->callback_function = callback_function;
        event_id = EH4A_NOTIFY_INFO_SUBSCRIBE_CODE;
        result = EH4I_send_subscribe(EH4A_SERVER,
                                      EH4A_notify_info_callback,
                                      callback,
                                      event_id,
                                      (ZOO_HANDLE*)handle,
                                      context);
        if (OK != result)
        {
           result = EH4A_PARAMETER_ERR;
           MM4A_free(callback);
        }
    }

    return result;
}

ZOO_INT32 EH4A_notify_info_unsubscribe(IN ZOO_UINT32 handle)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = EH4A_NOTIFY_INFO_SUBSCRIBE_CODE;
    if(OK == result)
    {
        result = EH4I_send_unsubscribe(EH4A_SERVER,event_id,handle);
    }
    return result;
}

