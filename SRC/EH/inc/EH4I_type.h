/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : EH4I_type.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-17    Generator      created
*************************************************************/
#ifndef EH4I_TYPE_H
#define EH4I_TYPE_H
#include <MQ4A_type.h>
#include "EH4A_type.h"
#include "EH4A_if.h"


/**
 *@brief Macro Definitions
**/
#define EH4I_COMPONET_ID "EH"
#define EH4A_SERVER     "EH4A_SERVER"
#define EH4I_BUFFER_LENGTH    256
#define EH4I_RETRY_INTERVAL   3

/**
 *@brief Function Code Definitions
**/
#define EH4A_SHOW_EXCEPTION_CODE 0x4548ff00
#define EH4A_SET_NOTIFICATION_CODE 0x4548ff01
#define EH4A_SET_ALARM_WITH_MATERIAL_ID_CODE 0x4548ff02
#define EH4A_SET_ALARM_CODE 0x4548ff03
#define EH4A_CLEAR_ALARM_CODE 0x4548ff04
#define EH4A_CLEAR_ALL_ALARMS_CODE 0x4548ff05
#define EH4A_GET_ALARM_INFO_CODE 0x4548ff06
#define EH4A_FIND_ALARM_COUNTS_CODE 0x4548ff07
#define EH4A_GET_ALARM_DATA_CODE 0x4548ff08
#define EH4A_ALARM_INFO_SUBSCRIBE_CODE 0x4548ff09
#define EH4A_NOTIFY_INFO_SUBSCRIBE_CODE 0x4548ff0b

/*Request and reply header struct*/
typedef struct
{
    MQ4A_SERV_ADDR reply_addr;
    ZOO_UINT32 msg_id;
    ZOO_BOOL reply_wanted;
    ZOO_UINT32 func_id;
}EH4I_REPLY_HANDLER_STRUCT;

typedef  EH4I_REPLY_HANDLER_STRUCT * EH4I_REPLY_HANDLE;

/*Request message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL need_reply;
}EH4I_REQUEST_HEADER_STRUCT;

/*Reply message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL execute_result;
}EH4I_REPLY_HEADER_STRUCT;

/**
*@brief EH4I_SHOW_EXCEPTION_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_INT32 line;
    ZOO_INT32 error_code;
    ZOO_INT32 link_error_code;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR component_id[EH4I_BUFFER_LENGTH];
    ZOO_CHAR file[EH4I_BUFFER_LENGTH];
    ZOO_CHAR function_name[EH4I_BUFFER_LENGTH];
    ZOO_CHAR info[EH4I_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}EH4I_SHOW_EXCEPTION_CODE_REQ_STRUCT;

/**
*@brief EH4I_SET_NOTIFICATION_CODE_REQ_STRUCT
**/
typedef struct 
{
    EH4A_NOTIFY_INFO_STRUCT info;
    ZOO_CHAR g_filler[8];
}EH4I_SET_NOTIFICATION_CODE_REQ_STRUCT;

/**
*@brief EH4I_SET_ALARM_WITH_MATERIAL_ID_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_INT32 alarm_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR material_id[EH4I_BUFFER_LENGTH];
    ZOO_CHAR g_filler[8];
}EH4I_SET_ALARM_WITH_MATERIAL_ID_CODE_REQ_STRUCT;

/**
*@brief EH4I_SET_ALARM_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_INT32 alarm_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}EH4I_SET_ALARM_CODE_REQ_STRUCT;

/**
*@brief EH4I_CLEAR_ALARM_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_INT32 alarm_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}EH4I_CLEAR_ALARM_CODE_REQ_STRUCT;

/**
*@brief EH4I_CLEAR_ALL_ALARMS_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}EH4I_CLEAR_ALL_ALARMS_CODE_REQ_STRUCT;

/**
*@brief EH4I_GET_ALARM_INFO_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_INT32 alarm_id;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}EH4I_GET_ALARM_INFO_CODE_REQ_STRUCT;

/**
*@brief EH4I_FIND_ALARM_COUNTS_CODE_REQ_STRUCT
**/
typedef struct 
{
    EH4A_FOUND_TYPE_ENUM found_type;
    ZOO_CHAR c_filler[4];
    ZOO_CHAR g_filler[8];
}EH4I_FIND_ALARM_COUNTS_CODE_REQ_STRUCT;

/**
*@brief EH4I_GET_ALARM_DATA_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_INT32 session_id;
    ZOO_INT32 index;
    ZOO_CHAR g_filler[8];
}EH4I_GET_ALARM_DATA_CODE_REQ_STRUCT;

/**
*@brief EH4I_SHOW_EXCEPTION_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}EH4I_SHOW_EXCEPTION_CODE_REP_STRUCT;

/**
*@brief EH4I_SET_NOTIFICATION_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}EH4I_SET_NOTIFICATION_CODE_REP_STRUCT;

/**
*@brief EH4I_SET_ALARM_WITH_MATERIAL_ID_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}EH4I_SET_ALARM_WITH_MATERIAL_ID_CODE_REP_STRUCT;

/**
*@brief EH4I_SET_ALARM_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}EH4I_SET_ALARM_CODE_REP_STRUCT;

/**
*@brief EH4I_CLEAR_ALARM_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}EH4I_CLEAR_ALARM_CODE_REP_STRUCT;

/**
*@brief EH4I_CLEAR_ALL_ALARMS_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}EH4I_CLEAR_ALL_ALARMS_CODE_REP_STRUCT;

/**
*@brief EH4I_GET_ALARM_INFO_CODE_REP_STRUCT
**/
typedef struct 
{
    EH4A_ALARM_DETAILS_STRUCT details;
    ZOO_CHAR g_filler[8];
}EH4I_GET_ALARM_INFO_CODE_REP_STRUCT;

/**
*@brief EH4I_FIND_ALARM_COUNTS_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_INT32 counts;
    ZOO_INT32 session_id;
    ZOO_CHAR g_filler[8];
}EH4I_FIND_ALARM_COUNTS_CODE_REP_STRUCT;

/**
*@brief EH4I_GET_ALARM_DATA_CODE_REP_STRUCT
**/
typedef struct 
{
    EH4A_ALARM_STRUCT data;
    ZOO_CHAR g_filler[8];
}EH4I_GET_ALARM_DATA_CODE_REP_STRUCT;

/**
*@brief EH4I_ALARM_INFO_SUBSCRIBE_CODE_REP_STRUCT
**/
typedef struct 
{
    EH4A_ALARM_STRUCT status;
    ZOO_CHAR g_filler[8];
}EH4I_ALARM_INFO_SUBSCRIBE_CODE_REP_STRUCT;

/**
*@brief EH4I_NOTIFY_INFO_SUBSCRIBE_CODE_REP_STRUCT
**/
typedef struct 
{
    EH4A_NOTIFY_INFO_STRUCT notify_info;
    ZOO_CHAR g_filler[8];
}EH4I_NOTIFY_INFO_SUBSCRIBE_CODE_REP_STRUCT;

typedef struct
{
    EH4I_REQUEST_HEADER_STRUCT request_header;
    union
    {
        EH4I_SHOW_EXCEPTION_CODE_REQ_STRUCT show_exception_req_msg;
        EH4I_SET_NOTIFICATION_CODE_REQ_STRUCT set_notification_req_msg;
        EH4I_SET_ALARM_WITH_MATERIAL_ID_CODE_REQ_STRUCT set_alarm_with_material_id_req_msg;
        EH4I_SET_ALARM_CODE_REQ_STRUCT set_alarm_req_msg;
        EH4I_CLEAR_ALARM_CODE_REQ_STRUCT clear_alarm_req_msg;
        EH4I_CLEAR_ALL_ALARMS_CODE_REQ_STRUCT clear_all_alarms_req_msg;
        EH4I_GET_ALARM_INFO_CODE_REQ_STRUCT get_alarm_info_req_msg;
        EH4I_FIND_ALARM_COUNTS_CODE_REQ_STRUCT find_alarm_counts_req_msg;
        EH4I_GET_ALARM_DATA_CODE_REQ_STRUCT get_alarm_data_req_msg;
     }request_body;
}EH4I_REQUEST_STRUCT;


typedef struct
{
    EH4I_REPLY_HEADER_STRUCT reply_header;
    union
    {
        EH4I_SHOW_EXCEPTION_CODE_REP_STRUCT show_exception_rep_msg;
        EH4I_SET_NOTIFICATION_CODE_REP_STRUCT set_notification_rep_msg;
        EH4I_SET_ALARM_WITH_MATERIAL_ID_CODE_REP_STRUCT set_alarm_with_material_id_rep_msg;
        EH4I_SET_ALARM_CODE_REP_STRUCT set_alarm_rep_msg;
        EH4I_CLEAR_ALARM_CODE_REP_STRUCT clear_alarm_rep_msg;
        EH4I_CLEAR_ALL_ALARMS_CODE_REP_STRUCT clear_all_alarms_rep_msg;
        EH4I_GET_ALARM_INFO_CODE_REP_STRUCT get_alarm_info_rep_msg;
        EH4I_FIND_ALARM_COUNTS_CODE_REP_STRUCT find_alarm_counts_rep_msg;
        EH4I_GET_ALARM_DATA_CODE_REP_STRUCT get_alarm_data_rep_msg;
        EH4I_ALARM_INFO_SUBSCRIBE_CODE_REP_STRUCT alarm_info_subscribe_code_rep_msg;
        EH4I_NOTIFY_INFO_SUBSCRIBE_CODE_REP_STRUCT notify_info_subscribe_code_rep_msg;
    }reply_body;
}EH4I_REPLY_STRUCT;


/**
*@brief EH4I_ALARM_INFO_SUBSCRIBE_CODE_CALLBACK_STRUCT
**/
typedef struct 
{
    EH4A_ALARM_CALLBACK_FUNCTION *callback_function;
    void * parameter;
}EH4I_ALARM_INFO_SUBSCRIBE_CODE_CALLBACK_STRUCT;

/**
*@brief EH4I_NOTIFY_INFO_SUBSCRIBE_CODE_CALLBACK_STRUCT
**/
typedef struct 
{
    EH4A_NOTIFY_CALLBACK_FUNCTION *callback_function;
    void * parameter;
}EH4I_NOTIFY_INFO_SUBSCRIBE_CODE_CALLBACK_STRUCT;

#endif //EH4I_type.h
