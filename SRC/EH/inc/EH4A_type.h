/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: EH
* File name: EH_type.h
* Description: interface
* HistEHy recEHder:
* Version   date           auth            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef EH4A_TYPE_H
#define EH4A_TYPE_H
#include <ZOO.h>

#define EH4A_BUFFER_LENGTH 64
#define EH4A_DISPLAY_LENGTH 128
/*
 *@ Error Code Definition
 */
#define EH4A_BASE_ERR          (0x45480000)
#define EH4A_SYSTEM_ERR        ((EH4A_BASE_ERR) + 0x01)
#define EH4A_TIMEOUT_ERR       ((EH4A_BASE_ERR) + 0x02)
#define EH4A_PARAMETER_ERR     ((EH4A_BASE_ERR) + 0x03)
#define EH4A_IILEGAL_CALL_ERR  ((EH4A_BASE_ERR) + 0x04)

/**
 * @brief 错误类型枚举定义
 */
typedef enum
{
	EH4A_ERROR_TYPE_MIN = -1,
	EH4A_ERROR_TYPE_SW  = 0,//软件错误类型
	EH4A_ERROR_TYPE_HW  = 1,//硬件错误类型
	EH4A_ERROR_TYPE_MAX
}EH4A_ERROR_TYPE_ENUM;

/**
 * @brief 错误等级枚举定义
 */
typedef enum
{
	EH4A_ERROR_LEVEL_MIN = -1,
	EH4A_ERROR_LEVEL_1   = 1,
	EH4A_ERROR_LEVEL_2   = 2,
	EH4A_ERROR_LEVEL_3   = 3,
	EH4A_ERROR_LEVEL_MAX
}EH4A_ERROR_LEVEL_ENUM;

/**
 * @brief 错误等级枚举定义
 */
typedef enum
{
	EH4A_FOUND_TYPE_MIN       = -1,
	EH4A_FOUND_TYPE_ALL       = 0,
	EH4A_FOUND_TYPE_TRIGGED   = 1,
	EH4A_FOUND_TYPE_RECOVERED = 2,
	EH4A_FOUND_TYPE_MAX
}EH4A_FOUND_TYPE_ENUM;
    
/**
 * @brief 错误信息结构体，用于HMI显示
 */
typedef struct
{
	EH4A_ERROR_TYPE_ENUM type;//错误码类
	ZOO_BOOL  is_triggered;//是否触发报警
	EH4A_ERROR_LEVEL_ENUM  level;//填充
	ZOO_CHAR  component[EH4A_BUFFER_LENGTH];//组件，table primary key
	ZOO_CHAR  description_cn[EH4A_DISPLAY_LENGTH];//英文描述
	ZOO_CHAR  description_en[EH4A_DISPLAY_LENGTH];//中文描述
	ZOO_CHAR  material_id[EH4A_BUFFER_LENGTH];//物料
	ZOO_CHAR  trigger_time[EH4A_BUFFER_LENGTH];//触发时间
	ZOO_CHAR  recover_time[EH4A_BUFFER_LENGTH];//恢复时间
	ZOO_BOOL  cloud_enabled;//是否传递OSS
	ZOO_INT32 cloud_error_code;//OSS指定的error code
}EH4A_ALARM_DETAILS_STRUCT;

/**
 * @brief 云错误信息结构体
 */
typedef struct
{
	ZOO_BOOL enbaled;
	ZOO_INT32 error_code;
}EH4A_CLOUD_STRUCT;

/**
 *@ brief 错误信息综合结构体数据
 */
typedef struct
{
    ZOO_INT32 alarm_id;//错误
	ZOO_BOOL  is_triggered;//是否触发报警
    EH4A_ERROR_TYPE_ENUM type;//错误码
	EH4A_ERROR_LEVEL_ENUM level;//填充
    ZOO_CHAR  component[EH4A_BUFFER_LENGTH];//组件，table primary key
	ZOO_CHAR  description_cn[EH4A_DISPLAY_LENGTH];//英文描述
	ZOO_CHAR  description_en[EH4A_DISPLAY_LENGTH];//中文描述
    ZOO_CHAR  material_id[EH4A_BUFFER_LENGTH];//物料号，无可以
    ZOO_CHAR  trigger_time[EH4A_BUFFER_LENGTH];//触发时间
	ZOO_CHAR  recover_time[EH4A_BUFFER_LENGTH];//恢复时间
	ZOO_BOOL  cloud_enabled;//是否传递云
	ZOO_INT32 cloud_error_code;//指定的error code
}EH4A_ALARM_STRUCT;

/**
 *@ brief 事件通知数据结构
 */
typedef struct
{
    ZOO_TIME_T timestamp;
    ZOO_INT32 event_id;
	ZOO_CHAR  device_model[EH4A_BUFFER_LENGTH];
    ZOO_CHAR  message[EH4A_BUFFER_LENGTH];
}EH4A_NOTIFY_INFO_STRUCT;

#endif
