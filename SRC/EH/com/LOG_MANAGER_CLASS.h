/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : LOG_MANAGER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef LOG_MANAGER_CLASS_H
#define LOG_MANAGER_CLASS_H

extern "C"
{
    #include <EH4I_type.h>
}

#include "LOG_MANAGER_INTERFACE.h"
#include <EH_CONFIGURE.h>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <fstream>
#include <functional>
#include <boost/smart_ptr.hpp>
#include <stdarg.h>
namespace ZOO_EH
{
    class LOG_MANAGER_CLASS: public virtual LOG_MANAGER_INTERFACE
    {
    public:
        LOG_MANAGER_CLASS();
        virtual ~LOG_MANAGER_CLASS();
    public:
        /**
         *@brief Initialize EH log path,
         * add sink to the boost log module
        **/
        static void initialize();

		/**
         *@brief print to file
         *@param message  the exception message
        **/
        static void log(const char* function_name,const char* format, ...);

        /**
         *@brief Write message to the EH file
         *@param file             the exception file
         *@param function_name    the exception function location
         *@param line             the exception line in the file
         *@param error_code
         *@param link_error_code  the error code
         *@param info             the exception message
        **/
        void show_exception(IN const ZOO_CHAR* component_id,
				                               IN const ZOO_CHAR* file,
				                               IN const ZOO_CHAR* function_name,
				                               IN ZOO_INT32 line,
				                               IN ZOO_INT32 error_code,
				                               IN ZOO_INT32 link_error_code,
				                               IN const ZOO_CHAR* info);

       /**
         *@brief print to terminate
         *@param message  the exception message
        **/
       static void debug(const char* function_name,const char* format, ...);
    };
}
#endif // LOG_MANAGER_CLASS_H
