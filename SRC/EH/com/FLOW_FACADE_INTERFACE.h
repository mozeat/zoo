/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : FLOW_FACADE_INTERFACE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef FLOW_FACADE_INTERFACE_H
#define FLOW_FACADE_INTERFACE_H
extern "C"
{
    #include <EHMA_event.h>
    #include <EH4A_type.h>
}
#include <boost/smart_ptr.hpp>
#include <DEVICE_CONTROLLER_INTERFACE.h>
#include <DAO_INTERFACE.h>
#include <LOG_MANAGER_INTERFACE.h>
namespace ZOO_EH
{
    class FLOW_FACADE_INTERFACE
    {
    public:
        FLOW_FACADE_INTERFACE();
        virtual ~FLOW_FACADE_INTERFACE();
    public:
        /**
         *@brief Set device controller
         *@param device_controller
        **/
        virtual void set_device_controller(boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE> device_controller) = 0;

        /**
         *@brief Set alarm dao
         *@param alarm_dao
        **/
        virtual void set_alarm_dao(boost::shared_ptr<DAO_INTERFACE> alarm_dao) = 0;

        /**
         *@brief Set log manager
         *@param log_manager
        **/
        virtual void set_log_manager(boost::shared_ptr<LOG_MANAGER_INTERFACE> log_manager) = 0;
    };
}


#endif // FLOW_FACADE_INTERFACE_H
