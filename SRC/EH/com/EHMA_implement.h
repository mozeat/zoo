/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : EHMA_implement.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-14    Generator      created
*************************************************************/
#ifndef EHMA_IMPLEMENT_H
#define EHMA_IMPLEMENT_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include <EH4A_if.h>
#include <TR4A_if.h>
#include <EHMA_event.h>
#include <EH4I_type.h>
#include <EH4A_type.h>


/**
 *@brief EHMA_implement_4A_show_exception
 *@param component_id
 *@param file
 *@param function_name
 *@param line
 *@param error_code
 *@param link_error_code
 *@param info
**/
ZOO_EXPORT void EHMA_implement_4A_show_exception(IN const ZOO_CHAR* component_id,
                                                     IN const ZOO_CHAR* file,
                                                     IN const ZOO_CHAR* function_name,
                                                     IN ZOO_INT32 line,
                                                     IN ZOO_INT32 error_code,
                                                     IN ZOO_INT32 link_error_code,
                                                     IN const ZOO_CHAR* info,
                                                     IN EH4I_REPLY_HANDLE reply_handle);
/**
 *@brief EHMA_implement_4A_set_notification
 *@param info
**/
ZOO_EXPORT void EHMA_implement_4A_set_notification(IN EH4A_NOTIFY_INFO_STRUCT* info,
                                                       IN EH4I_REPLY_HANDLE reply_handle);
/**
 *@brief EHMA_implement_4A_set_alarm_with_material_id
 *@param material_id
 *@param alarm_id
**/
ZOO_EXPORT void EHMA_implement_4A_set_alarm_with_material_id(IN const ZOO_CHAR* material_id,
                                                                 IN ZOO_INT32 alarm_id,
                                                                 IN EH4I_REPLY_HANDLE reply_handle);
/**
 *@brief EHMA_implement_4A_set_alarm
 *@param alarm_id
**/
ZOO_EXPORT void EHMA_implement_4A_set_alarm(IN ZOO_INT32 alarm_id,
                                                IN EH4I_REPLY_HANDLE reply_handle);
/**
 *@brief EHMA_implement_4A_clear_alarm
 *@param alarm_id
**/
ZOO_EXPORT void EHMA_implement_4A_clear_alarm(IN ZOO_INT32 alarm_id,
                                                  IN EH4I_REPLY_HANDLE reply_handle);
/**
 *@brief EHMA_implement_4A_clear_all_alarms
**/
ZOO_EXPORT void EHMA_implement_4A_clear_all_alarms(IN EH4I_REPLY_HANDLE reply_handle);
/**
 *@brief EHMA_implement_4A_get_alarm_info
 *@param alarm_id
 *@param details
**/
ZOO_EXPORT void EHMA_implement_4A_get_alarm_info(IN ZOO_INT32 alarm_id,
                                                     IN EH4I_REPLY_HANDLE reply_handle);
/**
 *@brief EHMA_implement_4A_find_alarm_counts
 *@param found_type
 *@param counts
 *@param session_id
**/
ZOO_EXPORT void EHMA_implement_4A_find_alarm_counts(IN EH4A_FOUND_TYPE_ENUM found_type,
                                                        IN EH4I_REPLY_HANDLE reply_handle);
/**
 *@brief EHMA_implement_4A_get_alarm_data
 *@param session_id
 *@param index
 *@param data
**/
ZOO_EXPORT void EHMA_implement_4A_get_alarm_data(IN ZOO_INT32 session_id,
                                                     IN ZOO_INT32 index,
                                                     IN EH4I_REPLY_HANDLE reply_handle);

#endif // EHMA_implement.h
