#ifndef EH_COMMON_FLOW_FACADE_WRAPPER_H
#define EH_COMMON_FLOW_FACADE_WRAPPER_H

#ifdef __cplusplus
extern "C"
{
#endif

    #include <EH4A_type.h>

    /**
     * @brief print exception to file
     */
    ZOO_INT32 EH_show_exception(IN const ZOO_CHAR * component_id,
        								        IN const ZOO_CHAR * file,
        								        IN const ZOO_CHAR * function_name,
        								        IN ZOO_INT32 line,
        								        IN ZOO_INT32 error_code,
												IN ZOO_INT32 link_error_code,
        								        IN const ZOO_CHAR * info);

	ZOO_INT32 EH_set_notification(EH4A_NOTIFY_INFO_STRUCT* info);
	/**
	* @brief Set alarm subscribe table
	*/
	ZOO_INT32 EH_set_alarm_with_material_id(IN const ZOO_CHAR* material_id,IN ZOO_INT32 alarm_id);

	/**
	 * @brief Set alarm subscribe table
	 */
	ZOO_INT32 EH_set_alarm(IN ZOO_INT32 alarm_id);

	/**
	 * @brief Clear alarm subscribe table
	 */
	ZOO_INT32 EH_clear_alarm(IN ZOO_INT32 alarm_id);

	/**
	 * @brief Clear all alarm subscribe table
	 */
	ZOO_INT32 EH_clear_all_alarms(void);

	/**
	 * @brief Find configure alarm points
	 */
	ZOO_INT32 EH_find_alarm_counts(IN EH4A_FOUND_TYPE_ENUM found_type,IN ZOO_INT32 * counts,IN ZOO_INT32 * session_id);

	/**
	 * @brief Find configure alarm points
	 */
	ZOO_INT32 EH_get_alarm_data(IN ZOO_INT32 session_id,IN ZOO_INT32 index,
												INOUT EH4A_ALARM_STRUCT * data);
	
	/**
	 * @brief get alarm info by error code
	 */
	ZOO_INT32 EH_get_alarm_information(IN ZOO_INT32 alarm_id,
													INOUT EH4A_ALARM_DETAILS_STRUCT *details);
#ifdef __cplusplus
}
#endif


#endif // EH_COMMON_FLOW_FACADE_WRAPPER_H
