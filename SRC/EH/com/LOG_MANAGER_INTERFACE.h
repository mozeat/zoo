/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : LOG_MANAGER_INTERFACE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef LOG_MANAGER_INTERFACE_H
#define LOG_MANAGER_INTERFACE_H
extern "C"
{
    #include <ZOO.h>
    #include <ZOO_if.h>
}
#include <string>
namespace ZOO_EH
{
    class LOG_MANAGER_INTERFACE
    {
    public:
        LOG_MANAGER_INTERFACE();
        virtual ~LOG_MANAGER_INTERFACE();
    public:
        /**
         *@brief Write message to the EH file
         *@param file             the exception file
         *@param function_name    the exception function location
         *@param line             the exception line in the file
         *@param error_code
         *@param link_error_code  the error code
         *@param info             the exception message
        **/
        virtual void show_exception(IN const ZOO_CHAR* component_id,
                               IN const ZOO_CHAR* file,
                               IN const ZOO_CHAR* function_name,
                               IN ZOO_INT32 line,
                               IN ZOO_INT32 error_code,
                               IN ZOO_INT32 link_error_code,
                               IN const ZOO_CHAR* info) = 0;
    };
}
#endif // LOG_MANAGER_INTERFACE_H
