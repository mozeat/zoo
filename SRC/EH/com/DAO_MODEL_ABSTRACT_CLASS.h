/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : DAO_MODEL_ABSTRACT_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef DAO_MODEL_ABSTRACT_CLASS_H
#define DAO_MODEL_ABSTRACT_CLASS_H
extern "C"
{
    #include <ZOO.h>
    #include <stdlib.h>
    #include <EH4A_type.h>
}
#include <string>
#include <sstream>
namespace ZOO_EH
{
    class DAO_MODEL_ABSTRACT_CLASS
    {
    public:
        DAO_MODEL_ABSTRACT_CLASS();
        virtual ~DAO_MODEL_ABSTRACT_CLASS();
    public:
        std::string convert_to_hex_string_upper(ZOO_INT32 error_code);
        
        ZOO_UINT32 convert_to_hex_string_to_int(std::string error_code);

        EH4A_ERROR_TYPE_ENUM converter(std::string type);
        std::string convert_to_component(ZOO_INT32 error_code);
    protected:
    };
}
#endif // DAO_MODEL_ABSTRACT_CLASS_H
