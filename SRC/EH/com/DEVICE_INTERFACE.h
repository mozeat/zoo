/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : DEVICE_INTERFACE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef DEVICE_INTERFACE_H
#define DEVICE_INTERFACE_H
#include "models/MARKING_MODEL_INTERFACE.h"
#include "DAO_INTERFACE.h"
namespace ZOO_EH
{
    class DEVICE_INTERFACE : public virtual ZOO_COMMON::MARKING_MODEL_INTERFACE
    {
    public:
        DEVICE_INTERFACE();
        virtual ~DEVICE_INTERFACE();
    public:
        /**
         *@brief Connect db
        **/
        virtual void initialize() = 0;

        /**
         *@brief Disconnect db
        **/
        virtual void terminate() = 0;

        /**
         *@brief Enabled the device
         *@return true or false
        **/
        virtual void set_enable(ZOO_BOOL enable) = 0;

        /**
         *@brief Enabled the device
         *@return true or false
        **/
        virtual void set_db_name(std::string db_name) = 0;

        /**
         *@brief Set device dao
         *@param device_dao
        **/
        virtual void set_device_dao(IN boost::shared_ptr<DAO_INTERFACE> device_dao) = 0;

        /**
         *@brief Publish alarm trigged
         *@param message
        **/
        virtual void publish(IN boost::shared_ptr<ERROR_MESSAGE_CLASS> message) = 0;

        /**
         *@brief Get alarm details
         *@param error_code
        **/
        virtual boost::shared_ptr<ERROR_MESSAGE_CLASS> get_alarm_definitions(IN ZOO_INT32 alarm_id) = 0;

		/**
         *@brief Get all config alarms
         *@param alarm vector
        **/
        virtual std::vector<EH4A_ALARM_STRUCT * > get_all_cfg_alarms() = 0;

        /**
         *@brief Get all row counts
         *@param alarm vector
        **/
        virtual ZOO_INT32 get_row_counts() = 0;
    };
}
#endif // DEVICE_INTERFACE_H
