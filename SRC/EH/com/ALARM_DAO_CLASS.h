/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : ALARM_DAO_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef ALARM_DAO_CLASS_H
#define ALARM_DAO_CLASS_H
extern "C"
{
    #include <EH4I_type.h>
}
#include <soci/soci.h>
#include <soci/sqlite3/soci-sqlite3.h>
#include <DAO_INTERFACE.h>
#include <ZOO_COMMON_MACRO_DEFINE.h>
#include <STRING_UTILITY_CLASS.h>
#include <models/MARKING_MODEL_INTERFACE.h>
#include <ERROR_CODE_CONVERTER_CLASS.h>
#include <EH_COMMON_MACRO_DEFINE.h>
#include <LOG_MANAGER_CLASS.h>
#include <DAO_MODEL_ABSTRACT_CLASS.h>
namespace ZOO_EH
{
    class ALARM_DAO_CLASS : public virtual DAO_INTERFACE
                            ,public virtual ZOO_COMMON::MARKING_MODEL_INTERFACE
                            ,public DAO_MODEL_ABSTRACT_CLASS
    {
    public:
        ALARM_DAO_CLASS(std::string db_name = "Alarm");
        virtual ~ALARM_DAO_CLASS();
    public:
        void set_db_name(std::string name);
        /**
         *@brief Connect db
        **/
        void open();

        /**
         *@brief close db
        **/
        void close();

        /**
         *@brief Create db table
        **/
        void create_table();


        /**
         *@brief if not present,insert ,or update the data
        **/
        void replace(ZOO_INT32 error_code,
                        ZOO_INT32 level,
                        const std::string & componet_id,
                        ZOO_INT32 error_type,
                        const std::string &message,
                        const std::string &material_id,
                        const std::string &trigger_time,
                        const std::string &recover_time,
                        ZOO_BOOL is_trigged);
        /**
         *@brief Update alarm db table,use transaction
        **/
        void update(ZOO_INT32 error_code,
                        ZOO_INT32 level,
                        const std::string &componet_id,
                        ZOO_INT32 error_type,
                        const std::string &message,
                        const std::string &material_id,
                        const std::string &trigger_time,
                        const std::string &recover_time,
                        ZOO_BOOL is_trigged);

        /**
         *@brief Update alarm db table,use transaction
        **/
        void insert(ZOO_INT32 error_code,
                        ZOO_INT32 level,
                        const std::string &componet_id,
                        ZOO_INT32 error_type,
                        const std::string &message,
                        const std::string &material_id,
                        const std::string &trigger_time,
                        const std::string &recover_time,
                        ZOO_BOOL is_trigged);

        /**
         *@brief Truncate table
        **/
        void truncate_table();

        /**
         *@brief Drop table
        **/
        void drop_table();

        /**
         *@brief Delete table
        **/
        void remove(ZOO_INT32 error_code);

        /**
         *@brief Get data from device dao
        **/
        boost::shared_ptr<ERROR_MESSAGE_CLASS> query(ZOO_INT32 error_code);

        /**
         *@brief DAO has opened
        **/
        ZOO_BOOL is_opened();

        std::vector<EH4A_ALARM_STRUCT *> read_all_trigged_datas();
        std::vector<EH4A_ALARM_STRUCT *> read_all_recovered_datas();
    private:

        ZOO_BOOL m_opened;

        /**
         *@brief Database file name
        **/
        std::string m_db_name;

        /**
         *@brief Table name
        **/
        std::string m_table_name;

        std::string m_resource_id;
        /**
         *@brief Session
        **/
        boost::shared_ptr<soci::session> m_session;

        /**
         *@brief Sqlite3 instance
        **/
        soci::backend_factory const & m_sqlite_dao;
    };
}


#endif // ALARM_DAO_CLASS_H
