/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : STRING_UTILITY_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef STRING_UTILITY_CLASS_H
#define STRING_UTILITY_CLASS_H

extern "C"
{
    #include <EH4A_type.h>
}
#include <cctype>
#include <string>
#include  <algorithm>
namespace ZOO_EH
{
    class STRING_UTILITY_CLASS
    {
    public:
        STRING_UTILITY_CLASS();
        virtual ~STRING_UTILITY_CLASS();
    public:
        static ZOO_INT32 bool_string_to_int(const ZOO_CHAR * bool_string);
        static const ZOO_CHAR * to_string (ZOO_BOOL value);
        static const ZOO_CHAR * to_string (EH4A_ERROR_TYPE_ENUM type);
        static EH4A_ERROR_TYPE_ENUM to_enum (const ZOO_CHAR * type_string);
        static std::string str_toupper(std::string s);
        static std::string str_tolower(std::string s);
        static std::string found_type_to_string(EH4A_FOUND_TYPE_ENUM type);
    private:
    };
}
#endif // STRING_UTILITY_CLASS_H
