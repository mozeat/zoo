/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : SESSION_MANAGER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef SESSION_MANAGER_CLASS_H
#define SESSION_MANAGER_CLASS_H
#include "SESSION_CLASS.h"
#include <map>
namespace ZOO_EH
{
    class SESSION_MANAGER_CLASS 
    {
    public:
        SESSION_MANAGER_CLASS();
        virtual ~SESSION_MANAGER_CLASS();
    public:

        /**
         * @brief Get instance
         **/
        static boost::shared_ptr<SESSION_MANAGER_CLASS> get_instance();
        
        /**
         * @brief Save alarm cache
         * @param alarms   the cache content
         **/
        void add_session(IN std::vector<EH4A_ALARM_STRUCT * > & alarms,ZOO_INT32 * session_id);        

        /**
         * @brief Get session 
         **/
        boost::shared_ptr<SESSION_CLASS> get_session(ZOO_INT32 session_id);

        void remove_session(ZOO_INT32 session_id);
    private:

        /**
         * @brief The instance  
         **/
        static boost::shared_ptr<SESSION_MANAGER_CLASS> m_instance;
        
        /**
         *@brief Define sessions attribute
        **/
        std::map<ZOO_INT32,boost::shared_ptr<SESSION_CLASS> > m_sessions;
    };
}
#endif // SESSION_MANAGER_CLASS_H
