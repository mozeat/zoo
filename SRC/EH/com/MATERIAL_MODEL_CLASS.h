/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : MATERIAL_MODEL_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef MATERIAL_MODEL_CLASS_H
#define MATERIAL_MODEL_CLASS_H

#include <MATERIAL_INTERFACE.h>

namespace ZOO_EH
{
    class MATERIAL_MODEL_CLASS : public virtual MATERIAL_INTERFACE
    {
    public:
        MATERIAL_MODEL_CLASS();
        virtual ~MATERIAL_MODEL_CLASS();

    public:

    private:
    };
}
#endif // MATERIAL_MODEL_CLASS_H
