/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : EHMA_event.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-17    Generator      created
*************************************************************/
#ifndef EHMA_EVENT_H
#define EHMA_EVENT_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include <EH4A_if.h>
#include <TR4A_if.h>
#include <EH4I_type.h>
#include <EH4I_if.h>
#include <EH4A_type.h>

/**
 *@brief EHMA_raise_4A_show_exception
 *@param component_id
 *@param file
 *@param function_name
 *@param line
 *@param error_code
 *@param link_error_code
 *@param info
**/
ZOO_EXPORT ZOO_INT32 EHMA_raise_4A_show_exception(IN ZOO_INT32 error_code,
                                                      IN EH4I_REPLY_HANDLE reply_handle);

/**
 *@brief EHMA_raise_4A_set_notification
 *@param info
**/
ZOO_EXPORT ZOO_INT32 EHMA_raise_4A_set_notification(IN ZOO_INT32 error_code,
                                                        IN EH4I_REPLY_HANDLE reply_handle);

/**
 *@brief EHMA_raise_4A_set_alarm_with_material_id
 *@param material_id
 *@param alarm_id
**/
ZOO_EXPORT ZOO_INT32 EHMA_raise_4A_set_alarm_with_material_id(IN ZOO_INT32 error_code,
                                                                  IN EH4I_REPLY_HANDLE reply_handle);

/**
 *@brief EHMA_raise_4A_set_alarm
 *@param alarm_id
**/
ZOO_EXPORT ZOO_INT32 EHMA_raise_4A_set_alarm(IN ZOO_INT32 error_code,
                                                 IN EH4I_REPLY_HANDLE reply_handle);

/**
 *@brief EHMA_raise_4A_clear_alarm
 *@param alarm_id
**/
ZOO_EXPORT ZOO_INT32 EHMA_raise_4A_clear_alarm(IN ZOO_INT32 error_code,
                                                   IN EH4I_REPLY_HANDLE reply_handle);

/**
 *@brief EHMA_raise_4A_clear_all_alarms
**/
ZOO_EXPORT ZOO_INT32 EHMA_raise_4A_clear_all_alarms(IN ZOO_INT32 error_code,
                                                        IN EH4I_REPLY_HANDLE reply_handle);

/**
 *@brief EHMA_raise_4A_get_alarm_info
 *@param alarm_id
 *@param details
**/
ZOO_EXPORT ZOO_INT32 EHMA_raise_4A_get_alarm_info(IN ZOO_INT32 error_code,
                                                      IN EH4A_ALARM_DETAILS_STRUCT *details,
                                                      IN EH4I_REPLY_HANDLE reply_handle);

/**
 *@brief EHMA_raise_4A_find_alarm_counts
 *@param found_type
 *@param counts
 *@param session_id
**/
ZOO_EXPORT ZOO_INT32 EHMA_raise_4A_find_alarm_counts(IN ZOO_INT32 error_code,
                                                         IN ZOO_INT32 counts,
                                                         IN ZOO_INT32 session_id,
                                                         IN EH4I_REPLY_HANDLE reply_handle);

/**
 *@brief EHMA_raise_4A_get_alarm_data
 *@param session_id
 *@param index
 *@param data
**/
ZOO_EXPORT ZOO_INT32 EHMA_raise_4A_get_alarm_data(IN ZOO_INT32 error_code,
                                                      IN EH4A_ALARM_STRUCT * data,
                                                      IN EH4I_REPLY_HANDLE reply_handle);

/**
 *@brief EH4A_alarm_info_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
ZOO_EXPORT void EHMA_raise_4A_alarm_info_subscribe(IN EH4A_ALARM_STRUCT * status,IN ZOO_INT32 error_code,IN void *context);

/**
 *@brief EH4A_notify_info_subscribe
 *@param notify_info
 *@param error_code
 *@param *context
**/
ZOO_EXPORT void EHMA_raise_4A_notify_info_subscribe(IN EH4A_NOTIFY_INFO_STRUCT * notify_info,IN ZOO_INT32 error_code,IN void *context);

#endif // EHMA_event.h
