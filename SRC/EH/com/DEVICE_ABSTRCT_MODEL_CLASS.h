/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : TASK_ABSTRCT_MODEL_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef DEVICE_ABSTRCT_MODEL_CLASS_H
#define DEVICE_ABSTRCT_MODEL_CLASS_H

extern "C"
{
    #include <EHMA_event.h>
}

#include <algorithm>
#include <vector>
#include <boost/smart_ptr.hpp>
#include "ERROR_MESSAGE_CLASS.h"
#include "DEVICE_DAO_MODEL_CLASS.h"
#include <models/MARKING_MODEL_INTERFACE.h>
#include "DEVICE_INTERFACE.h"
namespace ZOO_EH
{
    class DEVICE_ABSTRCT_MODEL_CLASS : public virtual DEVICE_INTERFACE
    {
    public:
        DEVICE_ABSTRCT_MODEL_CLASS();
        virtual ~DEVICE_ABSTRCT_MODEL_CLASS();
    public:
        /**
         *@brief Connect db
        **/
        void initialize();

        /**
         *@brief Disconnect db
        **/
        void terminate();

        /**
         *@brief Enabled the device
         *@return true or false
        **/
        void set_enable(ZOO_BOOL enable);

        /**
         *@brief Get enabled
         *@return true or false
        **/
        ZOO_BOOL get_enable();

        /**
         *@brief Enabled the device
         *@return true or false
        **/
        void set_db_name(std::string db_name);

        /**
         *@brief Set device dao
         *@param device_dao
        **/
        void set_device_dao(IN boost::shared_ptr<DAO_INTERFACE> device_dao);

        /**
         *@brief Publish alarm trigged
         *@param message
        **/
        void publish(IN boost::shared_ptr<ERROR_MESSAGE_CLASS> message);

        /**
         *@brief Set internal error for subscribe
        **/
        void set_internal_error(ZOO_INT32 error_code);

        /**
         *@brief Get internal error
        **/
        ZOO_INT32 get_internal_error();

        /**
         *@brief Get dao instance
        **/
        boost::shared_ptr<DEVICE_DAO_MODEL_CLASS> get_dao();

        /**
         *@brief get error data from dao
         *@param error_code
        **/
        boost::shared_ptr<ERROR_MESSAGE_CLASS> get_alarm_definitions(ZOO_INT32 error_code);

        /**
         *@brief Get all config alarms
         *@param alarm vector
        **/
        std::vector<EH4A_ALARM_STRUCT * > get_all_cfg_alarms();

        /**
         *@brief Get all row counts
         *@param alarm vector
        **/
        ZOO_INT32 get_row_counts();
    private:

        /**
         *@brief db open state
        **/
        ZOO_BOOL m_is_db_opened;

        /**
         *@brief Internal error code
        **/
        ZOO_INT32 m_internal_error;

        /**
         *@brief Device has been active
        **/
        ZOO_BOOL m_device_enable;

        /**
         *@brief Database name
        **/
        std::string m_db_name;

        /**
         *@brief Device dao
        **/
        boost::shared_ptr<DEVICE_DAO_MODEL_CLASS> m_device_dao;
    };
}
#endif // TASK_ABSTRCT_MODEL_CLASS_H
