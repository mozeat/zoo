/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : DEVICE_CONTROLLER_INTERFACE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef DEVICE_CONTROLLER_INTERFACE_H
#define DEVICE_CONTROLLER_INTERFACE_H

extern "C"
{
    #include <ZOO.h>
    #include <EH4A_type.h>
}
#include "ZOO_if.h"
#include "DEVICE_ABSTRCT_MODEL_CLASS.h"
#include <models/MARKING_MODEL_INTERFACE.h>
#include "DEVICE_INTERFACE.h"
namespace ZOO_EH
{
    class DEVICE_CONTROLLER_INTERFACE : public virtual ZOO_COMMON::MARKING_MODEL_INTERFACE
    {
    public:
        DEVICE_CONTROLLER_INTERFACE();
        virtual ~DEVICE_CONTROLLER_INTERFACE();
    public:
        /**
         *@brief Initialize controller and models
        **/
        virtual void initialize() = 0;

        /**
         *@brief Create all device models
        **/
        virtual void create_all_device_models() = 0;

        /**
         *@brief Delete all device models
        **/
        virtual void delete_all_device_models() = 0;

        /**
         *@brief Publish alarm trigged
         *@param message
        **/
        virtual void publish(IN ZOO_INT32 alarm_id,IN boost::shared_ptr<ERROR_MESSAGE_CLASS> message) = 0;

        /**
         *@brief Get alarm information from db
         *@param alarm_id
         *@param alarm_info
        **/
        virtual boost::shared_ptr<ERROR_MESSAGE_CLASS> get_alarm_definitions(IN ZOO_INT32 alarm_id) = 0;

        /**
         *@brief Get device by alarm id
         *@param alarm_id
        **/
        virtual boost::shared_ptr<DEVICE_INTERFACE> get_device(IN ZOO_INT32 alarm_id) = 0;

		/**
         *@brief Get all device row counts
         *@param alarm_id
        **/
        virtual ZOO_INT32 get_all_devices_row_counts() = 0;

        /**
         *@brief Find all device alarm datas
        **/
        virtual std::vector<EH4A_ALARM_STRUCT *> find_all_devices_alarm_datas() = 0;
    };
}

#endif // TASK_CONTROLLER_INTERFACE_H
