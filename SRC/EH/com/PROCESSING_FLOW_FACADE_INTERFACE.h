/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : PROCESSING_FLOW_FACADE_INTERFACE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef PROCESSING_FLOW_FACADE_INTERFACE_H
#define PROCESSING_FLOW_FACADE_INTERFACE_H
#include "FLOW_FACADE_INTERFACE.h"
namespace ZOO_EH
{
    class PROCESSING_FLOW_FACADE_INTERFACE : public virtual FLOW_FACADE_INTERFACE
    {
    public:
        PROCESSING_FLOW_FACADE_INTERFACE();
        virtual ~PROCESSING_FLOW_FACADE_INTERFACE();

    public:
        /**
         *@brief show exception on the HMI,but not set alarm tower
         *@param component_id
         *@param file
         *@param function_name
         *@param line
         *@param error_code
         *@param link_error_code
         *@param info
        **/
        virtual ZOO_INT32 show_exception(IN const ZOO_CHAR* component_id,
                                           IN const ZOO_CHAR* file,
                                           IN const ZOO_CHAR* function_name,
                                           IN ZOO_INT32 line,
                                           IN ZOO_INT32 error_code,
                                           IN ZOO_INT32 link_error_code,
                                           IN const ZOO_CHAR* info) = 0;
        /*
         * @brief EH4A_set_notification
        **/
        virtual ZOO_INT32 set_notification(IN EH4A_NOTIFY_INFO_STRUCT * info) = 0;

        /*
         * @brief EH4A_set_alarm_with_material_id
        **/
        virtual ZOO_INT32 invoke_alarm(IN const ZOO_CHAR * material_id,IN ZOO_INT32 alarm_id) = 0;

        /**
         *@brief Clear the signal tower
         *@param alarm_id
        **/
        virtual ZOO_INT32 clear_alarm(IN ZOO_INT32 alarm_id) = 0;

        /**
         *@brief Clear all alarm,reset signal tower
         *@param alarm_id
        **/
        virtual ZOO_INT32 clear_all_alarms() = 0;

        /**
		 * @brief Find configure alarm points
         * @param found_type
		 */
		virtual ZOO_INT32 find_alarm_counts(IN EH4A_FOUND_TYPE_ENUM found_type,
		                                                        IN ZOO_INT32 * counts,
		                                                        IN ZOO_INT32 * session_id) = 0;

		/**
		 * @brief get alarm data
		 */
		virtual ZOO_INT32 get_alarm_data(IN ZOO_INT32 session_id,
		                                               IN ZOO_INT32 index,
													   INOUT EH4A_ALARM_STRUCT * data) = 0;

        /**
         *@brief Get alarm informations
         *@param alarm_id
        **/
        virtual ZOO_INT32 get_alarm_information(IN ZOO_INT32 alarm_id,OUT EH4A_ALARM_DETAILS_STRUCT *alarm_info) = 0;

    private:
    };
}


#endif // PROCESSING_FLOW_FACADE_INTERFACE_H
