/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : TASK_CONTROLLER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef DEVICE_CONTROLLER_CLASS_H
#define DEVICE_CONTROLLER_CLASS_H
#include "DEVICE_CONTROLLER_INTERFACE.h"
#include "DEVICE_ABSTRCT_MODEL_CLASS.h"
#include "EH_CONFIGURE.h"
#include <sstream>
#include <map>
#include <boost/smart_ptr.hpp>
namespace ZOO_EH
{
    class DEVICE_CONTROLLER_CLASS : public virtual DEVICE_CONTROLLER_INTERFACE
    {
    public:
        DEVICE_CONTROLLER_CLASS();
        virtual ~DEVICE_CONTROLLER_CLASS();
    public:
        /**
         *@brief Initialize controller and models
        **/
        void initialize();

        /**
         *@brief Create all device models
        **/
        void create_all_device_models();

        /**
         *@brief Delete all device models
        **/
        void delete_all_device_models();

        /**
         *@brief Publish alarm trigged
         *@param message
        **/
        void publish(IN ZOO_INT32 alarm_id,IN boost::shared_ptr<ERROR_MESSAGE_CLASS> message);

        /**
         *@brief Get alarm information from db
         *@param alarm_id
         *@return alarm definitions and message
        **/
        boost::shared_ptr<ERROR_MESSAGE_CLASS> get_alarm_definitions(IN ZOO_INT32 alarm_id);

        /**
         *@brief Get device by alarm id
         *@param alarm_id
        **/
        boost::shared_ptr<DEVICE_INTERFACE> get_device(IN ZOO_INT32 alarm_id);

        ZOO_INT32 get_all_devices_row_counts();

        /**
         *@brief Find all device alarm datas
        **/
        std::vector<EH4A_ALARM_STRUCT *> find_all_devices_alarm_datas();

    private:
        std::map<std::string,boost::shared_ptr<DEVICE_INTERFACE> > m_device_map;
    };
}
#endif // DEVICE_CONTROLLER_CLASS_H
