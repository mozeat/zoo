/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : CONTROLLER_ABSTRACT_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef CONTROLLER_ABSTRACT_CLASS_H
#define CONTROLLER_ABSTRACT_CLASS_H
#include "DEVICE_ABSTRCT_MODEL_CLASS.h"
#include <boost/smart_ptr.hpp>
namespace ZOO_EH
{
    class CONTROLLER_ABSTRACT_CLASS
    {
    public:
        CONTROLLER_ABSTRACT_CLASS();
        virtual ~CONTROLLER_ABSTRACT_CLASS();
    public:
        //virtual void create_all_models();
        //virtual boost::shared_ptr<DEVICE_ABSTRCT_MODEL_CLASS> get_model(std::string && marking_code);
    protected:

        std::map<std::string,boost::shared_ptr<DEVICE_ABSTRCT_MODEL_CLASS> > m_models_map;
    };
}


#endif // TASK_CONTROLLER_ABSTRACT_CLASS_H
