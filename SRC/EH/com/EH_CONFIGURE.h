/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : EH_CONFIGURE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef EH_CONFIGURE_H
#define EH_CONFIGURE_H

extern "C"
{
    #include "ZOO_if.h"
}

#include <utils/ENVIRONMENT_UTILITY_CLASS.h>
#include <boost/smart_ptr.hpp>
namespace ZOO_EH
{
    class EH_CONFIGURE
    {
    public:
        EH_CONFIGURE();
        virtual ~EH_CONFIGURE();
        static boost::shared_ptr<EH_CONFIGURE> get_instance();
    public:
        /**
         *@brief Initialize
        **/
        static void initialize();

        /**
         *@brief reload
        **/
        void reload();

        /**
         *@brief alarm db path
        **/
        std::string get_execute_path();

        /**
         *@brief alarm db path
        **/
        std::string get_alarm_db_path();

        /**
         *@brief Get device error define database path
        **/
        std::string get_device_db_path();

		/**
	     *@brief Task configuratiion db path
	    **/
        std::vector<ZOO_TASK_STRUCT *> get_tasks();

		/**
         *@brief log configure path
        **/
        std::string get_log_path();
    private:
        std::string m_execute_path;
        
        /**
         * @brief The config tasks structure
         */
		ZOO_TASKS_STRUCT  m_cfg_tasks;

        /**
         * @brief The output files path
         */
        ZOO_USER_PATH_STRUCT m_user_path;
        static boost::shared_ptr<EH_CONFIGURE> m_instance;
    };
}

#endif // EH_CONFIGURE_H
