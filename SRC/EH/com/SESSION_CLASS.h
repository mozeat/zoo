/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : SESSION_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef SESSION_CLASS_H
#define SESSION_CLASS_H
extern "C"
{
    #include <EH4A_type.h>
    #include <ZOO.h>
}
#include <vector>
#include <boost/smart_ptr.hpp>

namespace ZOO_EH
{
    class SESSION_CLASS 
    {
    public:
        SESSION_CLASS();
        SESSION_CLASS(std::vector<EH4A_ALARM_STRUCT * > & alarms);
        virtual ~SESSION_CLASS();
    public:
        /**
         *@brief Save alarm cache
         *@param alarms   the cache content
        **/
        void save_cache(IN std::vector<EH4A_ALARM_STRUCT * > & alarms);
        
        /**
         *@brief Get alarm cache
         *@return 
        **/
        const std::vector<EH4A_ALARM_STRUCT * > & get_cache();

        /**
         *@brief Get alarm cache by index
         *@param index
        **/
        EH4A_ALARM_STRUCT * get_cache(IN ZOO_INT32 index);

        /**
         *@brief Clear and remove
        **/
        void clear() ;

        /**
         *@brief Clear and remove
        **/
        ZOO_INT32 get_session_id();
    private :
        static ZOO_INT32 generate_uuid();
    private:
        /**
         *@brief Session id
        **/
        ZOO_INT32 m_session_id;

        /**
         *@brief Cache content
        **/
        std::vector<EH4A_ALARM_STRUCT * > m_cache;
    };
}

#endif // SESSION_CLASS_H
