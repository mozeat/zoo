/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : ERROR_CODE_CONVERTER_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef ERROR_CODE_CONVERTER_CLASS_H
#define ERROR_CODE_CONVERTER_CLASS_H
extern "C"
{
    #include <EH4A_type.h>
}

#include <stdio.h>
#include <stdlib.h>
#include <string>
namespace ZOO_EH
{
    class ERROR_CODE_CONVERTER_CLASS
    {
    public:
        ERROR_CODE_CONVERTER_CLASS();
        virtual ~ERROR_CODE_CONVERTER_CLASS();

    public:
        /**
         * @brief convert error code to componet id
         */
        static std::string convert_to_componet_id(ZOO_INT32 error_code);

        /**
         * @brief convert error code to componet id
         */
        static ZOO_INT32 convert_to_error_code(std::string error_code_string);
    };
}
#endif // ERROR_CODE_CONVERTER_CLASS_H
