#ifndef EH_COMMON_MACRO_DEFINE_H_H
#define EH_COMMON_MACRO_DEFINE_H_H

#include "ZOO_if.h"
#include <PARAMETER_EXCEPTION_CLASS.h>

#define __EH_TRY try

/**
 * @brief Define a macro to throw EH exception
 * @param error_code        The error code
 * @param error_message     The error message
 * @param inner_exception   The inner exception,& std::exception
 */
#define __THROW_EH_EXCEPTION(error_code,error_message,inner_exception) \
        ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING, __ZOO_FUNC__, ":: %s",error_message); \
        throw ZOO_COMMON::PARAMETER_EXCEPTION_CLASS(error_code, error_message, inner_exception);

#define __EH_CATCH_ALL(result) catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & e) \
        { \
            ZOO_slog(IN ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__, ":: %s",e.what()); \
            result = e.get_error_code();\
        }\
        catch(...){}

#endif // EH_COMON_MACRO_DEFINE_H_H
