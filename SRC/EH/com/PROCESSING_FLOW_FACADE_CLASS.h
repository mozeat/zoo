/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : PROCESSING_FLOW_FACADE_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef PROCESSING_FLOW_FACADE_CLASS_H
#define PROCESSING_FLOW_FACADE_CLASS_H
#include "FLOW_FACADE_ABSTRACT_CLASS.h"
#include "PROCESSING_FLOW_FACADE_INTERFACE.h"
#include <vector>
namespace ZOO_EH
{
    class PROCESSING_FLOW_FACADE_CLASS: public virtual PROCESSING_FLOW_FACADE_INTERFACE
                                        ,public virtual FLOW_FACADE_ABSTRACT_CLASS
    {
    public:
        PROCESSING_FLOW_FACADE_CLASS();
        virtual ~PROCESSING_FLOW_FACADE_CLASS();

    public:
        /**
         *@brief show exception on the HMI,but not set alarm tower
         *@param component_id
         *@param file
         *@param function_name
         *@param line
         *@param error_code
         *@param link_error_code
         *@param info
        **/
        ZOO_INT32 show_exception(IN const ZOO_CHAR* component_id,
                                           IN const ZOO_CHAR* file,
                                           IN const ZOO_CHAR* function_name,
                                           IN ZOO_INT32 line,
                                           IN ZOO_INT32 error_code,
                                           IN ZOO_INT32 link_error_code,
                                           IN const ZOO_CHAR* info);


        /*
         * @brief EH4A_set_notification
         *@param info
         **/
        ZOO_INT32 set_notification(IN EH4A_NOTIFY_INFO_STRUCT * info);

        
       /**
         *@brief Set alarm signal tower
         *@param alarm_id
        **/
        ZOO_INT32 invoke_alarm(IN const ZOO_CHAR * material_id,IN ZOO_INT32 alarm_id);

        /**
         *@brief Clear the signal tower
         *@param alarm_id
        **/
        ZOO_INT32 clear_alarm(IN ZOO_INT32 alarm_id);

        /**
         *@brief Clear all alarm,reset signal tower
         *@param alarm_id
        **/
        ZOO_INT32 clear_all_alarms();

		/**
		 * @brief Find configure alarm points
		 */
		ZOO_INT32 find_alarm_counts(IN EH4A_FOUND_TYPE_ENUM found_type,
		                                                IN ZOO_INT32 * counts,
		                                                IN ZOO_INT32 * session_id);

		/**
		 * @brief get alarm data
		 */
		ZOO_INT32 get_alarm_data(IN ZOO_INT32 session_id,
		                                               IN ZOO_INT32 index,
													   INOUT EH4A_ALARM_STRUCT * data);
													
        /**
         *@brief Get alarm informations
         *@param alarm_id
        **/
        ZOO_INT32 get_alarm_information(IN ZOO_INT32 alarm_id,
                                   OUT EH4A_ALARM_DETAILS_STRUCT *alarm_info);
    };
}
#endif // COMMON_FLOW_FACADE_CALSS_H
