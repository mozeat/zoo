/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : EHMA_dispatch.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-05    Generator      created
*************************************************************/

#ifndef EHMA_DISPATCH_H
#define EHMA_DISPATCH_H
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include <EH4I_type.h>
#include <EH4I_if.h>
#include <EH4A_type.h>
#include <EHMA_implement.h>

/**
*@brief Dispatch message from client to server internal interface
*@param context        
*@param server        address
*@param msg           request message to server
*@param len           request message length
*@param reply_msg     reply message length to caller
*@param msg_id        reply message msg_id
**/
ZOO_EXPORT void EHMA_callback_handler(void * context,const MQ4A_SERV_ADDR server,void * msg,ZOO_UINT32 msg_id);

#endif // EHMA_dispatch.h
