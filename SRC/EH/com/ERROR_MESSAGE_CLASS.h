/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : ERROR_MESSAGE_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef ERROR_MESSAGE_CLASS_H
#define ERROR_MESSAGE_CLASS_H
extern "C"
{
    #include <EH4A_type.h>
}
#include <string>
namespace ZOO_EH
{
    class ERROR_MESSAGE_CLASS
    {
    public:
        /**
         * @brief Default Constructor
         */
        ERROR_MESSAGE_CLASS();

        /**
         * @brief Destructor
         */
        virtual ~ERROR_MESSAGE_CLASS();
    public:
    	/**
         * @brief Set alarm state
         * @param on - the tigger state is on or off
         * @return void
         */
        void set_trigged_state(ZOO_BOOL on);

    	/**
         * @brief Get alarm state
         */
        ZOO_BOOL get_trigged_state();
        void set_componet_id(const std::string & componet_id);
        const std::string & get_componet_id();
        void set_material_id(const std::string &   material_id);
        const std::string & get_material_id();
        void set_alarm_id(ZOO_INT32 error_code);
        ZOO_INT32 get_alarm_id();
        void set_alarm_type(ZOO_INT32 error_type);
        ZOO_INT32 get_alarm_type();
        void set_alarm_level(ZOO_INT32 error_level);
        ZOO_INT32 get_alarm_level();
        const std::string & get_chinese_description();
        void set_chinese_description(const std::string & chinese_description);
        const std::string & get_english_description();
        void set_english_description(const std::string & english_description);
        const std::string & get_trigger_time();
        void set_trigger_time(const std::string & trigger_time);
        const std::string & get_recover_time();
        void set_recover_time(const std::string & recover_time);

		void set_cloud_enabled(ZOO_BOOL enabled);
        ZOO_BOOL get_oss_enabled();
        void set_welkins_enabled(ZOO_BOOL enabled);
        ZOO_BOOL get_welkins_enabled();
        void set_cloud_error_code(ZOO_INT32 error_code);
        ZOO_INT32 get_oss_error_code();
        
        void clear();
    private:
   		/**
         * @brief trigged state
         */
        ZOO_BOOL m_is_trigged;

   		/**
         * @brief alarm id
         */
        ZOO_INT32 m_alarm_id;

        /**
         * @brief alarm type
         */
        ZOO_INT32 m_alarm_type;

        /**
         * @brief alarm level
         */
        ZOO_INT32 m_alarm_level;

        /**
         * @brief the oss cloud error enabled
         */
        ZOO_BOOL m_oss_enabled;

		/**
         * @brief the welkins cloud error enabled
         */
        ZOO_BOOL m_welkins_enabled;

        /**
         * @brief the special error code for oss
         */
        ZOO_INT32 m_oss_error_code;

        /**
         * @brief the device name
         */
        std::string m_componet_id;

        std::string m_chinese_description;
        std::string m_english_description;
        std::string m_material_id;
        std::string m_trigger_time;
        std::string m_recover_time;
    };
}
#endif // ERROR_INFORMATION_CLASS_H
