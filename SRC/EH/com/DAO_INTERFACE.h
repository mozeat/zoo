/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : DAO_INTERFACE.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef DAO_INTERFACE_H
#define DAO_INTERFACE_H
#include <string>
#include <vector>
#include <boost/smart_ptr.hpp>
#include <soci/soci.h>
#include <soci/sqlite3/soci-sqlite3.h>
#include "ERROR_MESSAGE_CLASS.h"
namespace ZOO_EH
{
    class DAO_INTERFACE
    {
    public:
        DAO_INTERFACE();
        virtual ~DAO_INTERFACE();
    public:
        virtual void set_db_name(std::string name) = 0;

        /**
         *@brief Connect db
        **/
        virtual void open() = 0;

        /**
         *@brief close db
        **/
        virtual void close() = 0;

        /**
         *@brief Create db table
        **/
        virtual void create_table() = 0;

        /**
         *@brief Drop table
        **/
        virtual void drop_table() = 0;

        /**
         *@brief Delete table
        **/
        virtual void remove(ZOO_INT32 error_code) = 0;

        /**
         *@brief Truncate table
        **/
        virtual void truncate_table() = 0;

        /**
         *@brief DAO has opened
        **/
        virtual ZOO_BOOL is_opened() = 0;

        /**
         *@brief Get data from device dao
        **/
        virtual  boost::shared_ptr<ERROR_MESSAGE_CLASS> query(ZOO_INT32 error_code) = 0;
    };
}


#endif // DAO_INTERFACE_H
