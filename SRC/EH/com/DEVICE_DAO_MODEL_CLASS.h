/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : DEVICE_DAO_MODEL_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef DEVICE_DAO_MODEL_CLASS_H
#define DEVICE_DAO_MODEL_CLASS_H
#include <DAO_INTERFACE.h>
#include <ZOO_COMMON_MACRO_DEFINE.h>
#include "STRING_UTILITY_CLASS.h"
#include <models/MARKING_MODEL_INTERFACE.h>
#include "EH_COMMON_MACRO_DEFINE.h"
#include "ERROR_CODE_CONVERTER_CLASS.h"
#include <LOG_MANAGER_CLASS.h>
#include <DAO_MODEL_ABSTRACT_CLASS.h>
namespace ZOO_EH
{
    class DEVICE_DAO_MODEL_CLASS : public virtual DAO_INTERFACE
                                 ,public virtual ZOO_COMMON::MARKING_MODEL_INTERFACE
                                 ,public DAO_MODEL_ABSTRACT_CLASS
    {
    public:
        DEVICE_DAO_MODEL_CLASS(std::string db_name);
        virtual ~DEVICE_DAO_MODEL_CLASS();
    public:
        void set_db_name(std::string name);
        /**
         *@brief Connect db
        **/
        void open();

        /**
         *@brief close db
        **/
        void close();

        /**
         *@brief Create db table
        **/
        void create_table();

        /**
         *@brief Drop table
        **/
        void drop_table();

        /**
         *@brief Truncate table
        **/
        void truncate_table();

        /**
         *@brief Update alarm db table
        **/
        void insert(ZOO_INT32 error_code,
                        std::string type,
                        ZOO_INT32 level,
                        std::string english,
                        std::string chinese,
                        ZOO_BOOL oss_enabled = ZOO_FALSE,
                        ZOO_INT32 oss_error_code = 0);


        /**
         *@brief Update alarm db table
        **/
        void update(ZOO_INT32 error_code,
                        std::string type,
                        ZOO_INT32 level,
                        std::string english,
                        std::string chinese,
                        ZOO_BOOL oss_enabled = ZOO_FALSE,
                        ZOO_INT32 oss_error_code = 0);

        /**
         *@brief Delete error from table
        **/
        void remove(ZOO_INT32 error_code);

        /**
         *@brief Get data from device dao
        **/
        boost::shared_ptr<ERROR_MESSAGE_CLASS> query(ZOO_INT32 error_code);

        /**
         *@brief DAO has opened
        **/
        ZOO_BOOL is_opened();

		/**
         *@brief find all datas
        **/
        std::vector<EH4A_ALARM_STRUCT *> find_all_datas();
		/**
         *@brief count rows
        **/
        ZOO_INT32 get_row_counts();
    private:
        EH4A_ERROR_LEVEL_ENUM convert_error_level_from(int error_level);
    private:

        ZOO_BOOL m_opened;

        std::string m_db_name;

        std::string m_table_name;
        /**
         *@brief Session
        **/
        boost::shared_ptr<soci::session> m_session;

        /**
         *@brief Sqlite3 instance
        **/
        soci::backend_factory const & m_sqlite_dao;
    };
}


#endif // DEVICE_DAO_MODEL_CLASS_H
