/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : FLOW_FACADE_ABSTRACT_CLASS.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef FLOW_FACADE_ABSTRACT_CLASS_H
#define FLOW_FACADE_ABSTRACT_CLASS_H
#include "DEVICE_CONTROLLER_INTERFACE.h"
#include "DAO_INTERFACE.h"
#include "LOG_MANAGER_CLASS.h"
#include "FLOW_FACADE_INTERFACE.h"
namespace ZOO_EH
{
    class FLOW_FACADE_ABSTRACT_CLASS : public virtual FLOW_FACADE_INTERFACE
    {
    public:
        FLOW_FACADE_ABSTRACT_CLASS();
        virtual ~FLOW_FACADE_ABSTRACT_CLASS();
    public:
        /**
         *@brief Set device controller
         *@param device_controller
        **/
        void set_device_controller(boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE> device_controller);

        /**
         *@brief Set alarm dao
         *@param alarm_dao
        **/
        void set_alarm_dao(boost::shared_ptr<DAO_INTERFACE> alarm_dao);

        /**
         *@brief Set device controller
        **/
        boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE> get_device_controller();

        /**
         *@brief Get alarm dao
        **/
        boost::shared_ptr<DAO_INTERFACE> get_alarm_dao();

        /**
         *@brief Set log manager
        **/
        void set_log_manager(boost::shared_ptr<LOG_MANAGER_INTERFACE> log_manager);

        /**
         *@brief Get log manager
        **/
        boost::shared_ptr<LOG_MANAGER_INTERFACE> get_log_manager();


        /**
         *@brief Notify alarm id state changed,publish message to subscribe,
         *@ update alarm DAO according to alarm id
        **/
        void notify_property_changed(IN boost::shared_ptr<DEVICE_INTERFACE> device,
                                     IN boost::shared_ptr<DAO_INTERFACE> dao,
                                     IN ZOO_INT32 alarm_id,
                                     IN ZOO_BOOL alarm_trigged,
                                     IN const std::string &  material_id);
    private:

        /**
         *@brief Build publish message
        **/
        boost::shared_ptr<ERROR_MESSAGE_CLASS> build_alarm_message(IN boost::shared_ptr<DEVICE_INTERFACE> device,
                                                                         IN boost::shared_ptr<DAO_INTERFACE> dao,
                                                                         IN ZOO_INT32 alarm_id,
                                                                         IN ZOO_BOOL alarm_trigged,
                                                                        IN const std::string & material_id);
        /**
         *@brief Update message to dao
        **/
        void update_dao(IN boost::shared_ptr<DAO_INTERFACE> dao,
                                            IN boost::shared_ptr<ERROR_MESSAGE_CLASS> message);

        /**
         *@brief Get data from DAO
         *@param dao
         *@return null : no alarm in table,else has already in table
        **/
        boost::shared_ptr<ERROR_MESSAGE_CLASS> query(IN boost::shared_ptr<DAO_INTERFACE> dao,IN ZOO_INT32 alarm_id);

    protected:
        void rebuild_alarms(std::vector<EH4A_ALARM_STRUCT * > & alarms);
                
        /**
         *@brief Get current date and time
        **/
        std::string get_current_time();


    private:
        /**
         *@brief device controller
        **/
        boost::shared_ptr<DEVICE_CONTROLLER_INTERFACE> m_device_controller;

        /**
         *@brief Alarm dao
        **/
        boost::shared_ptr<DAO_INTERFACE> m_alarm_dao;

        /**
         *@brief Log manager
        **/
        boost::shared_ptr<LOG_MANAGER_INTERFACE> m_log_manager;
    };
}
#endif // FLOW_FACADE_ABSTRACT_CLASS_H
