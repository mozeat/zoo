/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : EH_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-21    Generator      created
*************************************************************/
extern "C"
{
    #include <EH4A_if.h>
    #include <EH4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_EH_GET_ALARM_INFO_006)

/**
 *@brief 
 *@param alarm_id
 *@param details
**/
    BOOST_AUTO_TEST_CASE( TC_EH_GET_ALARM_INFO_006_001 )
    {
        ZOO_INT32 alarm_id = TC_EH_GOLBAL_FIXTURE::alarm_id;
        EH4A_ALARM_DETAILS_STRUCT details;
        memset(&details,0,sizeof(EH4A_ALARM_DETAILS_STRUCT));
        BOOST_TEST(EH4A_get_alarm_info(alarm_id,&details) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()