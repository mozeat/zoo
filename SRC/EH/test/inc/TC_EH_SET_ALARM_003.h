/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : EH_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-21    Generator      created
*************************************************************/
extern "C"
{
    #include <EH4A_if.h>
    #include <EH4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_EH_SET_ALARM_003)

/**
 *@brief 
 *@param alarm_id
**/
    BOOST_AUTO_TEST_CASE( TC_EH_SET_ALARM_003_001 )
    {
        ZOO_INT32 alarm_id = TC_EH_GOLBAL_FIXTURE::alarm_id;
        BOOST_TEST(EH4A_set_alarm(alarm_id) == OK);
        BOOST_TEST(EH4A_set_alarm(0x54520002) == OK);
        BOOST_TEST(EH4A_set_alarm(0x54520003) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()