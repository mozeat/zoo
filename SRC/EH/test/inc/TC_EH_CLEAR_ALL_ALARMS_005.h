/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : EH_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-21    Generator      created
*************************************************************/
extern "C"
{
    #include <EH4A_if.h>
    #include <EH4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_EH_CLEAR_ALL_ALARMS_005)

/**
 *@brief 
**/
    BOOST_AUTO_TEST_CASE( TC_EH_CLEAR_ALL_ALARMS_005_001 )
    {
        BOOST_TEST(EH4A_clear_all_alarms() == OK);
    }

BOOST_AUTO_TEST_SUITE_END()