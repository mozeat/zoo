/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : EH_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-21    Generator      created
*************************************************************/
extern "C"
{
    #include <EH4A_if.h>
    #include <EH4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_EH_FIND_ALARM_COUNTS_007)

/**
 *@brief 
 *@param counts
**/
    BOOST_AUTO_TEST_CASE( TC_EH_FIND_ALARM_COUNTS_007_001 )
    {
        ZOO_INT32 session_id = 0;
        BOOST_TEST_MESSAGE("counts is " << TC_EH_GOLBAL_FIXTURE::counts);
        BOOST_TEST(EH4A_find_alarm_counts(EH4A_FOUND_TYPE_TRIGGED,&TC_EH_GOLBAL_FIXTURE::counts,&session_id) == OK);
        BOOST_TEST_MESSAGE("counts is " << TC_EH_GOLBAL_FIXTURE::counts);
        for(ZOO_INT32 index = 0;index < TC_EH_GOLBAL_FIXTURE::counts;index ++)
    	{
        	EH4A_ALARM_STRUCT data = EH4A_ALARM_STRUCT();
        	BOOST_TEST(EH4A_get_alarm_data(session_id,index,&data) == OK);
        	BOOST_TEST_MESSAGE("alarm_id:" << data.alarm_id);
        }
    }

    BOOST_AUTO_TEST_CASE( TC_EH_FIND_ALARM_COUNTS_007_002 )
    {
        ZOO_INT32 session_id = 0;
        BOOST_TEST_MESSAGE("counts is " << TC_EH_GOLBAL_FIXTURE::counts);
        BOOST_TEST(EH4A_find_alarm_counts(EH4A_FOUND_TYPE_ALL,&TC_EH_GOLBAL_FIXTURE::counts,&session_id) == OK);
        BOOST_TEST_MESSAGE("counts is " << TC_EH_GOLBAL_FIXTURE::counts);
        for(ZOO_INT32 index = 0;index < TC_EH_GOLBAL_FIXTURE::counts;index ++)
    	{
        	EH4A_ALARM_STRUCT data = EH4A_ALARM_STRUCT();
        	BOOST_TEST(EH4A_get_alarm_data(session_id,index,&data) == OK);
        	BOOST_TEST_MESSAGE("alarm_id:" << data.alarm_id);
        }
    }

BOOST_AUTO_TEST_SUITE_END()