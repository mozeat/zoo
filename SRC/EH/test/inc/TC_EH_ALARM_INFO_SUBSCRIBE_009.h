/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : EH_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-21    Generator      created
*************************************************************/
extern "C"
{
    #include <EH4A_if.h>
    #include <EH4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_EH_ALARM_INFO_SUBSCRIBE_009)

	void alarm_callback(IN EH4A_ALARM_STRUCT * status,
								IN ZOO_INT32 error_code,
								IN void *context)
	{
		BOOST_TEST_MESSAGE( "alarm_id: " << status->alarm_id );
		BOOST_TEST_MESSAGE( "is_trigged: " << status->is_triggered );
	}
	
	/**
	 *@brief 
	 *@param callback_function
	 *@param handle
	 *@param context
	**/
    BOOST_AUTO_TEST_CASE( TC_EH_ALARM_INFO_SUBSCRIBE_007_001 )
    {
        void * context = NULL;
        BOOST_TEST(EH4A_alarm_info_subscribe(alarm_callback,
        										&TC_EH_GOLBAL_FIXTURE::handle,
        										context) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()