/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : EH_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-21    Generator      created
*************************************************************/
extern "C"
{
    #include <EH4A_if.h>
    #include <EH4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_EH_SHOW_EXCEPTION_001)

/**
 *@brief 
 *@param component_id
 *@param file
 *@param function_name
 *@param line
 *@param error_code
 *@param link_error_code
 *@param info
**/
    BOOST_AUTO_TEST_CASE( TC_EH_SHOW_EXCEPTION_001_001 )
    {
        const ZOO_CHAR* component_id = TC_EH_GOLBAL_FIXTURE::componet_id.data();
        const ZOO_CHAR* file = "test_file.cpp";
        const ZOO_CHAR* function_name = "test_function";
        ZOO_INT32 line = 100;
        ZOO_INT32 link_error_code = 200;
        const ZOO_CHAR* info = "test exception message";
        BOOST_TEST(EH4A_show_exception(component_id,file,function_name,line,
                                       TC_EH_GOLBAL_FIXTURE::alarm_id,link_error_code,info) == OK);
    }

BOOST_AUTO_TEST_SUITE_END()