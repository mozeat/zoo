/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : EH_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-21    Generator      created
*************************************************************/
#define BOOST_TEST_MODULE EH_test_module 
#define BOOST_TEST_DYN_LINK 
#include <boost/test/unit_test.hpp>
#include <boost/test/unit_test_log.hpp>
#include <boost/test/unit_test_suite.hpp>
#include <boost/test/framework.hpp>
#include <boost/test/unit_test_parameters.hpp>
#include <boost/test/utils/nullstream.hpp>
/**
 * @brief Execute <EH> UTMF ...
 * @brief <Global Fixture> header file can define the global variable.
 * @brief modify the include header files sequence to change the unit test execution sequence but not case SHUTDOWN.
 */
#include <TC_EH_GLOBAL_FIXTRUE.h>
#include <TC_EH_ALARM_INFO_SUBSCRIBE_009.h>
#include <TC_EH_SHOW_EXCEPTION_001.h>
#include <TC_EH_SET_ALARM_003.h>
#include <TC_EH_SET_ALARM_WITH_MATERIAL_ID_002.h>
#include <TC_EH_GET_ALARM_INFO_006.h>
#include <TC_EH_FIND_ALARM_COUNTS_007.h>
#include <TC_EH_GET_ALARM_DATA_008.h>
#include <TC_EH_CLEAR_ALARM_004.h>
#include <TC_EH_CLEAR_ALL_ALARMS_005.h>
#include <TC_EH_ALARM_INFO_UNSUBSCRIBE_0010.h>

/**
 * @brief This case should be executed at the end of all test cases to exit current process.
 * @brief Close all this process events subscribe.
 * @brief Close message queue context thread.
 */
//#include <TC_EH_SHUTDOWN.h>
