/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : EH_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-05-21    Generator      created
*************************************************************/
extern "C"
{
    #include <EH4A_if.h>
    #include <EH4A_type.h>
    #include <MM4A_if.h>
}
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/output_test_stream.hpp>
#include <boost/test/results_reporter.hpp>
#include <boost/test/unit_test_log.hpp>
#include <string>
/**
* @brief Define a test suite entry/exit,so that the setup function is called only once
* upon entering the test suite.
*/
struct TC_EH_GOLBAL_FIXTURE
{
    TC_EH_GOLBAL_FIXTURE()
    {
        BOOST_TEST_MESSAGE("TC global fixture initialize ...");
        MM4A_initialize();
    }

    ~TC_EH_GOLBAL_FIXTURE()
    {
        BOOST_TEST_MESSAGE("TC teardown ...");
        MM4A_terminate();
    }
	static ZOO_INT32 counts ;
    static ZOO_INT32 alarm_id ;
    static std::string componet_id;
    static std::string material_id;
    static ZOO_UINT32 handle;
};
ZOO_INT32 TC_EH_GOLBAL_FIXTURE::counts = 0;
ZOO_INT32 TC_EH_GOLBAL_FIXTURE::alarm_id = 0x54520001;
ZOO_UINT32 TC_EH_GOLBAL_FIXTURE::handle = 0;
std::string TC_EH_GOLBAL_FIXTURE::componet_id = "TR";
std::string TC_EH_GOLBAL_FIXTURE::material_id = "P0000084AB184170061300001100004";

BOOST_TEST_GLOBAL_FIXTURE(TC_EH_GOLBAL_FIXTURE);


/**
 * @brief Define test report output formate, default --log_level=message.
*/
struct TC_EH_REPORTER
{
    TC_EH_REPORTER():reporter("../../../reporter/EH_TC_result.reporter")
    {
        boost::unit_test::unit_test_log.set_stream( reporter );
        boost::unit_test::unit_test_log.set_threshold_level(boost::unit_test::log_test_units);
    }

    ~TC_EH_REPORTER()
    {
        boost::unit_test::unit_test_log.set_stream( std::cout );
    }
   std::ofstream reporter;
};

BOOST_TEST_GLOBAL_CONFIGURATION(TC_EH_REPORTER);

