TARGET   := libMM4A.so
SRCEXTS  := .c
INCDIRS  := ./inc ./com
SOURCES  := ./lib/MM4A.c ./lib/ncx_slab.c
SRCDIRS  := ./lib
CFLAGS   :=
CXXFLAGS := -std=c++11 
CPPFLAGS := -fPIC 
LDFLAGS  := $(GCOVER_LINK)  -lnsl -shared

include ../Project_config
include ../Makefile_tpl_linux
