/*******************************************************************************
* Copyright (C) 2017, shanghai ZOO ltd
* All rights reserved.
* Product: ZOO
* Module: ZOO
* Component id: MM
* File name: MM_type.h
* Description: interface fMM socket
* HistMMy recMMder:
* Version   date           authMM            context
* 1.0       2018-04-21     weiwang.sun       created
******************************************************************************/
#ifndef MM4A_TYPE_H
#define MM4A_TYPE_H
#include <ZOO.h>
#include <MM4A_type.h>

/*
* Component Id Definition
*/
#ifndef COMPONENT_ID_MM
#define COMPONENT_ID_MM "MM"
#endif

#define MM_BLOCK_SIZE_MAX 20*1024*1024 //20M
#define DEFAULT_MEMORY_SIZE 20*1024*1024 //20
/*
* ErrMM Code Definition
*/
#define MM4A_BASE_ERR     (0x4D4D0000)
#define MM4A_SYSTEM_ERR  ((MM4A_BASE_ERR) + 0x01)
#define MM4A_TIMEOUT_ERR  ((MM4A_BASE_ERR) + 0x02)
#define MM4A_PARAMETER_ERR  ((MM4A_BASE_ERR) + 0x03)
#define MM4A_IILEGAL_CALL_ERR  ((MM4A_BASE_ERR) + 0x04)



#endif

