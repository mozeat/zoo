#ifndef _NCX_LOG_H_
#define _NCX_LOG_H_
#include "ZOO_if.h"
#define LOG_LEVEL 8
#define LV_TRACE 1
#define LV_DEBUG 2
#define LV_INFO  4
#define LV_ERROR 8
#define LV_ALERT 16

#define log(level, format, ...) \
	do { \
		if ( level >= ZOO_SEVERITY_LEVEL_WARNING ) {\
			ZOO_slog(level, __ZOO_FUNC__, format, ##__VA_ARGS__); \
		} \
	} while(0)

#define trace(format, ...) log(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__ ,format, ##__VA_ARGS__)
#define debug(format, ...) log(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__ ,format, ##__VA_ARGS__)
#define info(format, ...)  log(ZOO_SEVERITY_LEVEL_NOTIFICATION ,__ZOO_FUNC__, format, ##__VA_ARGS__)
#define error(format, ...) log(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__ ,format,##__VA_ARGS__)
#define alert(format, ...) log(ZOO_SEVERITY_LEVEL_WARNING,__ZOO_FUNC__ ,format, ##__VA_ARGS__)

#endif
