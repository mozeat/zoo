#ifndef _NCX_LOCK_H_
#define _NCX_LOCK_H_
#include <pthread.h>
#define ncx_shmtx_lock(x)   { pthread_spin_lock(&((x)->lock)); }
#define ncx_shmtx_unlock(x) { pthread_spin_unlock(&((x)->lock)); }

typedef struct 
{
	ncx_uint_t spin;
	ncx_mutex_t lock;
} ncx_shmtx_t;

#endif
