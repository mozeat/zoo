/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO 
 * Component ID : MM
 * File Name    : MM4A_if.h
 * Description  :
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#include <MM4A_if.h>
#include <MM4A_type.h>
#include <ncx_slab.h>

/*
 * @brief define global ncx_slab_pool_t pointer
 */
static ncx_slab_pool_t* g_memory_pool = NULL;

/*
 * @brief define global memory pointer
 */
static unsigned char* g_memory = NULL;

/*****************************************************
**@brief  MM4A_initialize
**@description     : 申请一块20M内存区
**@preconditions   :
**@input  parameter: 
**@output parameter:
**@return          : ZOO_INT32
****************************************************/
ZOO_INT32 MM4A_initialize()
{
    if(g_memory != NULL)
    {
        return OK;
    }
    g_memory = (u_char *) malloc(MM_BLOCK_SIZE_MAX);
    g_memory_pool = (ncx_slab_pool_t *)(g_memory);
    g_memory_pool->addr = g_memory;                                                                                                      
    g_memory_pool->min_shift = 3;                                                                                                     
    g_memory_pool->end = g_memory + MM_BLOCK_SIZE_MAX; 
    ncx_slab_init(g_memory_pool);
    return OK;
}

/*****************************************************
**@brief  MM4A_terminate
**@description     : 释放申请的内存区
**@preconditions   : 已调用MM4A_initialize
**@input  parameter: 
**@output parameter:
**@return          : ZOO_INT32
****************************************************/
void MM4A_terminate()
{
    if(NULL != g_memory)
	{
		free(g_memory);
		g_memory = NULL;
	}
    g_memory_pool = NULL;
}

/*****************************************************
**@brief  MM4A_malloc
**@description     : 在20M空间内存，申请指定字节大小的内存小块
**@preconditions   : 已调用MM4A_initialize
**@input  parameter: size_t bytes
**@output parameter:
**@return          : void * - 
    
****************************************************/
void* MM4A_malloc(size_t bytes)
{
    if(g_memory_pool == NULL)
        return NULL;
    void * shptr = ncx_slab_alloc_locked(g_memory_pool,bytes);
    if(shptr)
    {
        memset(shptr,0x0,bytes);
    }
    return shptr;
}

/*****************************************************
**@brief  MM4A_free
**@description     : 释放指定字节大小的内存小块
**@preconditions   : 已调用MM4A_malloc
**@input  parameter: 
**@output parameter:
**@return          : ZOO_INT32
****************************************************/
void MM4A_free(void* pointer)
{
    if(g_memory_pool != NULL && pointer != NULL)
        ncx_slab_free_locked(g_memory_pool,pointer);
}

/*****************************************************
**@brief  MM4A_malloc_lock
**@description     : 申请指定字节大小的内存块，用于多线程
**@preconditions   :已调用MM4A_initialize
**@input  parameter: 
**@output parameter:
**@return          : ZOO_INT32
****************************************************/
void *MM4A_malloc_lock(size_t bytes)
{
    if(g_memory_pool == NULL)
        return NULL;
    return ncx_slab_alloc_locked(g_memory_pool,bytes);
}

/*****************************************************
**@brief  MM4A_malloc_unlock
**@description     : 释放指定字节大小的内存块，用于多线程
**@preconditions   :
**@input  parameter: 
**@output parameter:
**@return          : ZOO_INT32
****************************************************/
void MM4A_malloc_unlock(void* pointer)
{
    if(g_memory_pool == NULL)
        return;
    ncx_slab_free_locked(g_memory_pool,pointer);
}
