/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : OTA_PROTOCAL_PARSER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef OTA_PROTOCAL_PARSER_CLASS_H
#define OTA_PROTOCAL_PARSER_CLASS_H
extern "C"
{
	#include "ZOO_if.h"
}
#include "UM_protocol_type.h"
#include "SW_MODULE_CLASS.h"
#include "SW_MODULE_PARSER_CLASS.h"
#include <map>
#include <string>
#include <chrono>                                                                                                                                                                                                                                                         
#include <iomanip>                                                                                                                                                                                                                                                             
#include <iostream>                                                                                                                                                                                                                                                            
#include <sstream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/optional.hpp>
#include <boost/foreach.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
namespace UM
{
	class OTA_PROTOCAL_PARSER_CLASS 
	{
	public:
        /*
         * @brief Constructor
        **/ 
		OTA_PROTOCAL_PARSER_CLASS();

        /*
         * @brief Destructor
        **/ 
        virtual ~OTA_PROTOCAL_PARSER_CLASS();
	public: 
		/*
         * @brief Get config data url from response
         * @param response
        **/ 
		std::string get_url(std::string domain,
								std::string resource_id,
								std::string user);

        /*
         * @brief Get cancel action url from response
         * @param response
        **/ 
		std::string get_cancel_action_url(std::string domain,
                            								    std::string resource_id,
                            								    std::string user,
                            								    std::string action_id);
                
		/*
         * @brief Get config data url from response
         * @param response
        **/ 
		std::string get_cancel_feedback_url(std::string domain,
								    std::string resource_id,
								    std::string user,
								    std::string action_id);
        
		ZOO_UINT32 get_polling_sleep_time(std::string & reply);
		
		/*
         * @brief Get request headers
         * @param accept
        **/ 
		std::vector<std::string> get_headers(std::string token);
        void replace(std::string& json, const std::string& placeholder, const std::string& value);
        
		/*
         * @brief Build response message for upgrade api
         * @param action_id
         * @param finished
         * @param total_packages
         * @param current_count
         * @param execution
         * @param details
        **/ 
		std::string build_feedback_message(std::string action_id,
													std::string finished,
													ZOO_INT32 total_packages,
													ZOO_INT32 cur_packages,
													std::string execution,
													std::string details);

		/*
         * @brief build feedback for put config data api
         * @param mode
         * @param VIN
         * @param hw_version
         * @param finished
         * @param execution
         * @param details
        **/ 
		std::string build_feedback_message(std::string action_id,
													std::string mode,
													std::string VIN,
													std::string hw_version,
													std::string finished,
													std::string execution,
													std::string details);

		/*
		 * @brief build feedback for cancel action api
		 * @param action_id
		 * @param finished
		 * @param execution
		 * @param details
		**/
		std::string build_feedback_message(std::string action_id,
													std::string finished,
													std::string execution,
													std::string details);

       
		/*
         * @brief Get config data url from response
         * @param response
        **/ 
		std::string get_config_data_url(std::string domain,
                            								    std::string resource_id,
                            								    std::string user);

                                                                
		/*
         * @brief Get deployment url from response
         * @param response
        **/ 
		std::string get_deployment_base_url(std::string & response);
        

		std::string get_deployment_feedback_url(std::string domain,
                            								    std::string resource_id,
                            								    std::string user,
                            								    std::string action_id);
            
		/*
		 * @brief Get id from deployment
		 * @param response
		**/ 
		std::string get_id_from_deployment(std::string & message);

        

        std::string parse_stop_id(std::string & message);
        
        /*
		 * @brief Parse update type
		 * @return type
		**/ 
        DEPLOYMENT_TYPE_ENUM parse_update_processing_type(std::string & message);
        
		/*
		 * @brief parse SW Module from deployment_reply_message
		 * @param modules
		**/ 
		std::vector<boost::shared_ptr<SW_MODULE_CLASS> > parse_software_modules(std::string && deployment_reply_message);

        
	};
}
#endif
