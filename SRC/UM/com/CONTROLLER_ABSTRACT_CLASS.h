/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : CONTROLLER_ABSTRACT_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef CONTROLLER_ABSTRACT_CLASS_H
#define CONTROLLER_ABSTRACT_CLASS_H

extern "C" 
{
    #include <ZOO.h>
    #include <UM4A_type.h>
}
#include "UM_COMMON_MACRO_DEFINE.h"
#include "CONTROLLER_INTERFACE.h"
#include <boost/shared_ptr.hpp>

namespace UM
{
    class CONTROLLER_ABSTRACT_CLASS : public virtual CONTROLLER_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       CONTROLLER_ABSTRACT_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~CONTROLLER_ABSTRACT_CLASS();
    public:
       /*
        * @brief Get the event_publisher attribute value
       **/ 
       boost::shared_ptr<EVENT_PUBLISHER_CLASS> get_event_publisher();

       /*
        * @brief Set the event_publisher attribute value
        * @param event_publisher    The new event_publisher attribute value
       **/ 
       void set_event_publisher(IN boost::shared_ptr<EVENT_PUBLISHER_CLASS> event_publisher);

       /*
        * @brief brief Add observer will be notified when property changed
        * @param observer  Property changed observer
       **/ 
       void add_observer(IN PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* observer);

       /*
        * @brief Add observer will be notified when property changed
        * @param observer  Property changed observer
       **/ 
       void remove_observer(IN PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* observer);

       /*
        * @brief Clean
       **/ 
       virtual void clean();

       /*
        * @brief Notify property has been changed
        * @property_name     The property has been changed
       **/ 
       void notify_of_property_changed(IN const ZOO_UINT32 property_name);

       /*
        * @brief Get status.
       **/ 
       void get_status(INOUT UM4A_STATUS_STRUCT * status);

       /*
        * @brief Set status.
       **/ 
       void set_status(IN UM4A_STATUS_STRUCT * status);

    protected:
       /*
        * @brief The event_publisher attribute
       **/ 
       boost::shared_ptr<EVENT_PUBLISHER_CLASS> m_event_publisher;

    private:
       /*
        * @brief The status of models.
       **/ 
       UM4A_STATUS_STRUCT * m_status;

       /*
        * @brief The list of observer instances will be notified when property has been changed
       **/ 
       std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>*> m_observers;

    };
}// namespace UM

#endif
