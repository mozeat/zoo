/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : STATE_MANAGER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef STATE_MANAGER_CLASS_H
#define STATE_MANAGER_CLASS_H
#include <boost/shared_ptr.hpp>
#include "UM_COMMON_MACRO_DEFINE.h"
#include "PERSISTENCE_CLASS.h"
namespace UM
{
    class STATE_MANAGER_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       STATE_MANAGER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~STATE_MANAGER_CLASS();
    public:
       /*
        * @brief Set driver state
       **/ 
       void set_driver_state(IN ZOO_DRIVER_STATE_ENUM state);

       /*
        * @brief Get driver state
       **/ 
       ZOO_DRIVER_STATE_ENUM  get_driver_state();

       /*
        * @brief Update driver state
       **/ 
       void update_driver_state(IN ZOO_DRIVER_STATE_ENUM state);

       /*
        * @brief Set ota state
       **/ 
       void set_ota_state(IN ZOO_OTA_STATE_ENUM ota_state);

       /*
        * @brief Get ota state
       **/ 
       ZOO_OTA_STATE_ENUM  get_ota_state();

	  /*
       * @brief Set deployment_url
       * @param 
       **/ 
       void  save(IN ZOO_OTA_STATE_ENUM ota_state,std::string version,std::string release_note);

       std::string get_last_version();
    private:
       
       /*
        * @brief The driver state.
       **/ 
       ZOO_DRIVER_STATE_ENUM  m_driver_state;

       boost::shared_ptr<PERSISTENCE_CLASS> m_dao;
    };
}// namespace UM
#endif
