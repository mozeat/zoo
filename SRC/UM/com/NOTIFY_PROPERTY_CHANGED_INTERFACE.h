/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : NOTIFY_PROPERTY_CHANGED_INTERFACE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef NOTIFY_PROPERTY_CHANGED_INTERFACE_H
#define NOTIFY_PROPERTY_CHANGED_INTERFACE_H

extern "C" 
{
    #include <ZOO.h>
}
#include <vector>
#include <boost/any.hpp>
#include "PROPERTY_CHANGED_OBSERVER_INTERFACE.h"
namespace UM
{
    template <typename T>
    class NOTIFY_PROPERTY_CHANGED_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       NOTIFY_PROPERTY_CHANGED_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~NOTIFY_PROPERTY_CHANGED_INTERFACE(){}
    public:
        /*
         * @brief Add observer will be notified when property changed
         * @param observer  Property changed observer
        **/
        virtual void add_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<T>* observer) = 0;

        /*
         * @brief Remove observer from subscribe list
        **/
        virtual void remove_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<T>* observer) = 0;

        /*
         * @brief Remove observer from subscribe listd
        **/
        virtual void clean() = 0;

        /*
         * @brief Notify property has been changed
         * @param property_name     The property has been changed
        **/
        virtual void notify_of_property_changed(const ZOO_UINT32 property_name) = 0;

    };
} //namespace UM
#endif
