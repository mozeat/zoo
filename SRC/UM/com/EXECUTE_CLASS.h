/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : EXECUTE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef EXECUTE_CLASS_H
#define EXECUTE_CLASS_H

extern "C" 
{
    #include <ZOO.h>
    #include <UM4A_type.h>
}
#include <vector>
#include <string>
#include <initializer_list>
#include <utility>
namespace UM
{
    class EXECUTE_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       EXECUTE_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~EXECUTE_CLASS();
    public:
    	/*
         * @brief Set exe name 
         * @param name        the name
        **/ 
		void set_exe_name(std::string name);

    	/*
         * @brief Get exe name 
         * @return name        
        **/ 
		std::string get_exe_name();

    	/*
         * @brief Set exe params 
         * @param param        the exe parm is string
        **/ 
        template<typename ...T>
		void set_params(T && ...param)
		{
			std::initializer_list<int>{(this->set_param(std::forward<T>(param)),0)...};
		}

		void add_param(std::string && param);

    	/*
         * @brief Get exe name 
         * @param params        
        **/ 
		std::vector<std::string> get_params();


		void set_timeout_value(ZOO_UINT32 minutes);
		
		ZOO_UINT32 get_timeout_value();
	private:
		void set_param(std::string param);
		
	private:		
		/*
         * @brief The exe name attribute        
        **/ 
		std::string m_exe_name;

		/*
         * @brief The timeout attribute        
        **/ 
		ZOO_UINT32 m_timeout_minutes;
		
		/*
         * @brief The params attribute        
        **/ 
		std::vector<std::string> m_params;
    };
}// namespace UM
#endif
