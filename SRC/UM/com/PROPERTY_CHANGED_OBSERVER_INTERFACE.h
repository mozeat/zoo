/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : PROPERTY_CHANGED_OBSERVER_INTERFACE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef PROPERTY_CHANGED_OBSERVER_INTERFACE_H
#define PROPERTY_CHANGED_OBSERVER_INTERFACE_H

extern "C" 
{
    #include <ZOO.h>
}
#include <vector>
#include <boost/any.hpp>
#include <boost/shared_ptr.hpp>
namespace UM
{
    template <typename T>
    class PROPERTY_CHANGED_OBSERVER_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       PROPERTY_CHANGED_OBSERVER_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~PROPERTY_CHANGED_OBSERVER_INTERFACE(){}
    public:
        /*
         * @brief This method is executed when property changed value
         * @param model             The model type
         * @param property_name     The property has been changed value
        **/
        virtual void on_property_changed(T* model,const ZOO_UINT32 property_name) = 0;

    };
} //namespace UM
#endif
