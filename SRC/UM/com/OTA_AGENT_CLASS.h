/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : SW_MODULE_DOWNLOADER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef OTA_AGENT_CLASS_H
#define OTA_AGENT_CLASS_H
#include "CONTROLLER_INTERFACE.h"
#include "MARKING_MODEL_INTERFACE.hpp"
#include "CONTROLLER_ABSTRACT_CLASS.h"
#include "CURL_WRAPPER_CLASS.h"
#include "OTA_PROTOCOL_PARSER_CLASS.h"
#include "ENUM_REGISTER_CLASS.h"
#include "UM_CONFIGURE.h"
#include "PROPERTY_CHANGE_KEY_DEFINE.h"
#include "utils/AUTO_RESET_EVENT_CLASS.h"
#include <vector>
#include <algorithm> 
namespace UM
{
	class OTA_AGENT_CLASS :public virtual CONTROLLER_ABSTRACT_CLASS,
									public virtual NOTIFY_PROPERTY_CHANGED_INTERFACE<CONTROLLER_INTERFACE>
	{
	public:
        /*
         * @brief Constructor
        **/ 
        OTA_AGENT_CLASS();

        /*
        * @brief Destructor
        **/ 
        virtual ~OTA_AGENT_CLASS();

        /*
        * @brief Destructor
        **/ 
        static boost::shared_ptr<OTA_AGENT_CLASS> get_instance();
  public:  

        /*
         * @brief Load 
        **/ 
        void load_configuration();

        /*
         * @brief Run communitate routines to OTA server
        **/ 
        void run();

        /*
         * @brief Stop routines
        **/ 
        void stop();

        /*
        * @brief begin cancel or other session .
        **/
        void open_session();

        /*
        * @brief end cancel or other session .
        **/
        void close_session();

        /*
        * @brief handle session .
        **/
        void handle_session();

        /*
         * @brief Publish uprgrade result to OTA server.
         * @param result  
         * @param message  
        **/ 
        void publish(EXECUTION_RESULT_ENUM result,std::string message = "");

       /*
         * @brief Publish uprgrade result to OTA server.
         * @param total_packages  
         * @param cur_packages   
         * @param message  
        **/ 
        void publish(int total_packages,
                               int cur_packages,
                                        std::string message = "");

        /*
         * @brief Add observer will be notified when property changed.
         * @param observer  Property changed observer
        **/ 
        void add_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* observer);

        /*
         * @brief Remove observer will be notified when property changed.
         * @param observer  Property changed observer
        **/ 
        void remove_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* observer);

        /*
        * @brief Clean.
        **/ 
        void clean();

        /*
        * @brief Notify property has been changed.
        * @param property_name     The property has been changed
        **/ 
        void notify_of_property_changed(const ZOO_UINT32 property_name);

        /*
         * @brief This method is executed when property changed value
         * @param model             The model type
         * @param property_name     The property has been changed value
        **/
        void on_property_changed(MARKING_MODEL_INTERFACE* model,const ZOO_UINT32 property_name);


        /*
        * @brief Get fg file.
        **/ 
        std::string get_ota_cfg_file();

        /*
        * @brief Set_domain .
         * @param domain             The domain
        **/ 
        void set_host_info(std::string host_name,
        				 std::string domain,
        					std::string resource_id,
        					std::string token);
        /*
        * @brief get_software_modules .
         * @return modules
        **/ 							
        std::vector<boost::shared_ptr<SW_MODULE_CLASS> > get_software_modules();

        /*
         * @brief Create SW_modules .
         * @return modules
        **/ 
        std::vector<boost::shared_ptr<SW_MODULE_CLASS> > create_software_modules();

        /*
         * @brief get deployment_url .
         * @return std::string
        **/
        std::string get_deployment_url();

        /*
         * @brief Get process type : force or attempt
         * @return the process type
        **/
        DEPLOYMENT_TYPE_ENUM get_process_type();
    private:	
        /*
        * @brief Listen .
         * @param domain             The domain
        **/
        void listen(std::string domain);

        /*
        * @brief parse_deployment_modules .
         * @param deployment_json            
        **/
        void parse_deployment_modules(std::string deployment_json);

        /*
        * @brief download_config_file .
        **/
        void download_config_file();

        /*
        * @brief download_packages .
        **/
        void download_packages();

        /*
        * @brief download_packages .
        * @param vin_code		   
        * @param hw_version		   
        **/
        void register_device_info(std::string url,
        				std::string vin_code,
        				std::string hw_version);

        /*
        * @brief remove file 
        * @param file		   
        * @return bool   
        **/
        ZOO_BOOL remove_file(std::string file);

        /*
        * @brief remove_all_files
        * @param path	  
        **/
        void remove_all_files(std::string path);

        /*
        * @brief update state machine  
        **/
        void update_state(int state);

        /*
        * @brief handle_cancel_action .
        * @param do_cancel 
        **/
        void handle_cancel_action(ZOO_BOOL do_cancel);

        /*
        * @brief handle_cancel_action .
        * @return ZOO_BOOL 
        **/
        ZOO_BOOL is_canceled_action_trigged();

        /*
        * @brief to lower alpha
        * @param lower alpha 
        **/
        std::string to_lower(std::string 	 upper);

        /*
        * @brief Get cfg modules from all deploy modules
        * @param config_file        config.json
        * @param entity_modules 
        **/
        std::vector<boost::shared_ptr<SW_MODULE_CLASS> > get_cfg_modules_from(
        				std::string config_file,
        				const std::vector<boost::shared_ptr<SW_MODULE_CLASS> > & entity_modules);
    private:	
        /*
        * @brief The instance.
        **/ 
        static boost::shared_ptr<OTA_AGENT_CLASS> m_instance;

        /*
        * @brief The  cfg file.
        **/ 
        std::string m_cfg_json_file;

        /*
        * @brief The  host name.
        **/ 
        std::string m_host_name;

        /*
        * @brief The download id attribute.
        **/ 
        std::string m_action_id;

        /*
        * @brief The resource id attribute.
        **/ 
        std::string m_resource_id;

        /*
        * @brief The token attribute.
        **/ 
        std::string m_token;

        /*
        * @brief The domain attribute.
        **/ 
        std::string m_domain;

        /*
        * @brief The deployment_url attribute.
        **/ 
        std::string m_deployment_url;

        /*
        * @brief The config json file url attribute.
        **/ 
        std::string m_config_json_url;

        /*
        * @brief The cfg data attribute.
        **/ 
        std::string m_cfg_data_url;

        /*
         * @brief The hardware version attribute.
        **/
        std::string m_hardware_version;
        
        /*
        * @brief The stop flag attribute.
        **/ 
        ZOO_BOOL m_stopped;

        /*
        * @brief The session state with ota server .
        **/ 
        ZOO_BOOL m_do_session;

        /*
        * @brief The state.
        **/ 
        int m_state;

        /*
        * @brief Upgrade type : force or Attempt.
        **/ 
        DEPLOYMENT_TYPE_ENUM m_process_type;
        
        /*
        * @brief The req.
        **/ 
        int m_req_interval;
        
        /*
        * @brief The entity sw modules.
        **/ 
        std::vector<boost::shared_ptr<SW_MODULE_CLASS> > m_entity_sw_modules;

        /*
        * @brief The enabled sw modules.
        **/ 
        std::vector<boost::shared_ptr<SW_MODULE_CLASS> > m_cfg_sw_modules;

        /*
        * @brief The event publisher.
        **/ 
        boost::shared_ptr<EVENT_PUBLISHER_CLASS> m_event_publisher;

        /*
        * @brief The application layer protocol attribute.
        **/
        boost::shared_ptr<OTA_PROTOCAL_PARSER_CLASS> m_protocol;

        /*
        * @brief The event handle.
        **/
        boost::shared_ptr<ZOO_COMMON::AUTO_RESET_EVENT_CLASS> m_wait_handle;

        /*
        * @brief The list of observer instances will be notified when property has been changed
        **/ 
        std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>*> m_observers;
    };
}
#endif
