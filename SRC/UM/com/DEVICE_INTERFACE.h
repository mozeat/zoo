/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : DEVICE_INTERFACE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef DEVICE_INTERFACE_H
#define DEVICE_INTERFACE_H

extern "C" 
{
    #include <ZOO.h>
    #include <UM4A_type.h>
}
#include "UM_COMMON_MACRO_DEFINE.h"
#include "MARKING_MODEL_INTERFACE.hpp"
#include "SW_MODULE_CLASS.h"
#include "PROPERTY_CHANGE_KEY_DEFINE.h"
#include "UM_CONFIGURE.h"

namespace UM
{
	
    class DEVICE_INTERFACE: public virtual MARKING_MODEL_INTERFACE
                             		,public virtual NOTIFY_PROPERTY_CHANGED_INTERFACE<MARKING_MODEL_INTERFACE>
    {
    public:
       /*
        * @brief Constructor
       **/ 
       DEVICE_INTERFACE();

       /*
        * @brief Destructor
       **/ 
       virtual ~DEVICE_INTERFACE();
    public:

       /*
        * @brief upgrade the model.
       **/ 
       virtual void install(boost::shared_ptr<SW_MODULE_CLASS> module);

       /*
        * @brief Cancel the sw model.
       **/ 
       virtual void cancel();
       
       /*
        * @brief Add observer will be notified when property changed.
        * @param observer  Property changed observer
       **/ 
       virtual void add_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer);

       /*
        * @brief Remove observer will be notified when property changed.
        * @param observer  Property changed observer
       **/ 
       virtual void remove_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer);

       /*
        * @brief Clean.
       **/ 
       void clean();
       
       /*
        * @brief Notify property has been changed.
        * @param property_name     The property has been changed
       **/ 
       virtual void notify_of_property_changed(const ZOO_UINT32 property_name);

	  /*
        * @brief update install state.
       **/ 
	   void update_state(int state);

		/*
        * @brief update install state.
       **/ 
	   virtual void reset_model();

    protected:
    
       int m_state;
       
       /*
        * @brief The list of observer instances will be notified when property has been changed.
       **/ 
       std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>*> m_observers;
    };
}// namespace UM

#endif
