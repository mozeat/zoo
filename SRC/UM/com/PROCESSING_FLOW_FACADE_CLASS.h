/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : PROCESSING_FLOW_FACADE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef PROCESSING_FLOW_FACADE_CLASS_H
#define PROCESSING_FLOW_FACADE_CLASS_H

#include "PROCESSING_FLOW_FACADE_INTERFACE.h"
#include "FLOW_FACADE_ABSTRACT_CLASS.h"
#include "utils/AUTO_RESET_EVENT_CLASS.h"
#include <atomic>
#include <mutex>

namespace UM
{
    class PROCESSING_FLOW_FACADE_CLASS : public virtual PROCESSING_FLOW_FACADE_INTERFACE,
                                         			public virtual FLOW_FACADE_ABSTRACT_CLASS 
    {
    public:
       /*
        * @brief Constructor
       **/ 
       PROCESSING_FLOW_FACADE_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~PROCESSING_FLOW_FACADE_CLASS();
    public:
		/*
		* @brief Perform upgrade
		**/
		ZOO_INT32 perform_upgrade();

        /*
         * @brief Undo the upgrade
        **/
        ZOO_INT32 undo_the_upgrade();

        /*
         * @brief Get status
         * @param status
        **/
        ZOO_INT32 get_status(OUT UM4A_STATUS_STRUCT * status);

		/*
         * @brief recover if abort abnormally.
        **/
        void recover_if_abort_abnormally();

		/*
		* @brief This method is executed when property changed value
		* @param model             The model type
		* @param property_name     The property has been changed value
		**/ 
		OVERRIDE
		void on_property_changed(CONTROLLER_INTERFACE * model,
										const ZOO_UINT32 property_name);

		/*
		 * @brief This method is executed when property changed value
		 * @param model The source object contains property changed
		 * @param property_name The property has been changed value
		**/ 
		OVERRIDE
		void on_property_changed(IN MARKING_MODEL_INTERFACE* model,
											IN const ZOO_UINT32 property_name);								
	private:

		/*
		* @brief Handle upgrade ...
		**/
		void handle_upgrade();
		
		/*
    	* @brief Check current state is ...
    	 * @param state  driver state
    	**/
		void check_state_is(IN ZOO_DRIVER_STATE_ENUM state);

		/*
    	* @brief Check current state is ...
    	 * @param state  ota state
    	**/
		void check_ota_state_is(IN ZOO_OTA_STATE_ENUM state);

		/*
		* @brief Stop tasks ...
		**/
		void stop_all_running_tasks();

		/*
		* @brief startup tasks ...
		**/
		void startup_all_tasks();
		
		/*
		* @brief Show hmi upgrade informations .
		* @param sw_module
		* @param device
		**/
		void prepare_install_package(boost::shared_ptr<INSTALL_SEQUENCE_CLASS> sequence);
		
		/*
		* @brief handle_install_package
		* @param sw_module
		* @param device
		**/
		void handle_install_package(boost::shared_ptr<INSTALL_SEQUENCE_CLASS> sequence);

		/*
		* @brief post_install_package
		* @param sw_module
		* @param device
		**/
		void post_install_package(boost::shared_ptr<INSTALL_SEQUENCE_CLASS> sequence);	

		/*
		* @brief notify_upgrade_request
		**/
		void notify_upgrade_request(boost::shared_ptr<INSTALL_SEQUENCE_CLASS> sequence);

		/*
		 * @brief notify inprogress changed
		**/
		void notify_inprogress_changed(boost::shared_ptr<SW_MODULE_CLASS> module);

		/*
		 * @brief notify inprogress changed
		**/
		void notify_state_changed(ZOO_INT32 error_code = OK);

        /*
		 * @brief reset flow
		**/
		void reset_flow();
        
        /*
         * @brief Make SW module associate with device controller
		 * @param sequence         the SW modules
		 * @param controller       the installer tool
        **/
        void associate_device_with_software_model(boost::shared_ptr<INSTALL_SEQUENCE_CLASS> sequence,
                                                       boost::shared_ptr<INSTALLER_CONTROLLER_INTERFACE> controller);
	private:
		std::mutex m_sync_lock;
		
		boost::shared_ptr<ZOO_COMMON::AUTO_RESET_EVENT_CLASS> m_wait_handle;
    };
}//namespace UM

#endif
