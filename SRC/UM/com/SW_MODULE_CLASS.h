/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : SW_MODULE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef SW_MODULE_CLASS_H
#define SW_MODULE_CLASS_H
extern "C"
{
	#include "UM4A_type.h"
}
#include "EXECUTE_CLASS.h"
#include <string>
#include <boost/shared_ptr.hpp>


namespace UM
{
    class DEVICE_INTERFACE;
    class SW_MODULE_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       SW_MODULE_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~SW_MODULE_CLASS();
    public:
    
        /*
         * @brief Set the swm order.
         * @param order.
        **/ 
        void set_order(ZOO_INT32 order);
        ZOO_INT32 get_order();

        /*
         * @brief Set package_name.
         * @param package_name.
        **/ 
        void set_package_name(std::string package_name);
        std::string get_package_name();

        /*
         * @brief Set release note.
         * @param release_note.
        **/ 
        void set_release_note(std::string & release_note);
        const std::string & get_release_note();

        /*
         * @brief Set type.
         * @param type.
        **/ 
        void set_type(std::string type);
        std::string get_type();    

        /*
         * @brief Set version.
         * @param version.
        **/ 
        void set_version(std::string            version); 
        std::string & get_version();  

        /*
         * @brief Set inprogress value.
         * @param inprogress_value.
        **/ 
        void set_inprogress_value(ZOO_INT32                     inprogress_value); 
        ZOO_INT32 & get_inprogress_value(); 

        /*
         * @brief Set package md5.
        **/ 
        void set_package_md5(std::string                md5);
        std::string get_package_md5();

        /*
         * @brief Set package sha1.
         * @param url.
        **/ 
        void set_package_sha1(std::string sha1);
        std::string get_package_sha1();

        /*
         * @brief Set download url.
         * @param url.
        **/ 
        void set_download_url(std::string url);
        std::string get_download_url();
        
        void set_estimate_consume_minutes(ZOO_INT32 minutes);
        ZOO_INT32 get_estimate_consume_minutes();
       
        boost::shared_ptr<DEVICE_INTERFACE> get_device();

        /*
         * @brief Set install device.
         * @param device.
        **/ 
        void set_device(boost::shared_ptr<DEVICE_INTERFACE> device);
            
        /*
         * @brief clean 
        **/ 
        void clean();

        /*
         * @brief add an executor for execute sequence.
         * @param executor
        **/ 
        void add_executor(boost::shared_ptr<EXECUTE_CLASS> executor);

        /*
         * @brief Set executors for execute sequence.
         * @param executor
        **/ 
        void set_executors(std::vector<boost::shared_ptr<EXECUTE_CLASS> > & executors);
        
        /*
         * @brief Get all executors.
         * @return executors
        **/ 
        std::vector<boost::shared_ptr<EXECUTE_CLASS> > get_executors();
       
    private:
    	ZOO_INT32  m_order;
    	std::string m_package_name;
    	std::string m_release_note;
    	std::string m_type;
    	std::string m_version;
    	std::string m_package_md5;
    	std::string m_package_sha1;
    	std::string m_download_url;
    	ZOO_INT32  m_inprogress_value;
        ZOO_INT32 m_estimate_consume_minutes;
    	boost::shared_ptr<DEVICE_INTERFACE> m_device;
    	/*
        * @brief the property of executors. 
        **/
    	std::vector<boost::shared_ptr<EXECUTE_CLASS> > m_executors;
    };
}// namespace UM
#endif
