/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : TASK_MANAGER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef TASK_MANAGER_CLASS_H
#define TASK_MANAGER_CLASS_H

extern "C" 
{
    #include <ZOO_if.h>
    #include <UM4A_type.h>
    #include <SM4A_if.h>
}

namespace UM
{
    class TASK_MANAGER_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       TASK_MANAGER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~TASK_MANAGER_CLASS();
    public:
	   /*
        * @brief Destructor
       **/ 
      static ZOO_INT32 start_all_tasks();

       /*
        * @brief Destructor
       **/ 
      static ZOO_INT32 stop_all_tasks();
    };
} // namespace UM
#endif
