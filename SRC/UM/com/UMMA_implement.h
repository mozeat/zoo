/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : UMMA_implement.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-16    Generator      created
*************************************************************/
#ifndef UMMA_IMPLEMENT_H
#define UMMA_IMPLEMENT_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include <UMMA_event.h>
#include "UM4I_type.h"
#include "UM4A_type.h"


/**
 *@brief UMMA_implement_4A_perform_upgrade
**/
ZOO_EXPORT void UMMA_implement_4A_perform_upgrade(IN UM4I_REPLY_HANDLE reply_handle);
/**
 *@brief UMMA_implement_4A_undo_the_upgrade
**/
ZOO_EXPORT void UMMA_implement_4A_undo_the_upgrade(IN UM4I_REPLY_HANDLE reply_handle);
/**
 *@brief UMMA_implement_4A_get_status
 *@param status
**/
ZOO_EXPORT void UMMA_implement_4A_get_status(IN UM4I_REPLY_HANDLE reply_handle);

#endif // UMMA_implement.h
