/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : CURL_WRAPPER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef CURL_WRAPPER_CLASS_H
#define CURL_WRAPPER_CLASS_H
extern "C"
{
	#include "ZOO_if.h"
	#include "UM4A_type.h"
}

#include <stdio.h> 
#include "UM_COMMON_MACRO_DEFINE.h"
#include <map>
#include <string>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <chrono>
#include <boost/process.hpp>
#include <curl/curl.h>
#include <boost/iostreams/device/mapped_file.hpp>
namespace UM
{
	class CURL_WRAPPER_CLASS 
	{
	public:
        /*
         * @brief Constructor
        **/ 
		CURL_WRAPPER_CLASS();

        /*
         * @brief Destructor
        **/ 
        virtual ~CURL_WRAPPER_CLASS();
	public:  

		/*
         * @brief Http get url from hawkbit
         * @param url             request
         * @param headers         the http header
         * @param timeout         seconds
        **/ 
		std::string get(IN std::string & url,
									IN std::vector<std::string> & headers,
									IN ZOO_INT32 timeout = 60);			
							
		/*
         * @brief Stop routines
        **/ 
		std::string post(IN std::string & url,
										IN std::vector<std::string> & headers,
										IN std::string & body,
										IN ZOO_INT32 timeout = 60);

		/*
         * @brief http Put method
        **/ 
		void put(IN std::string url,
										IN std::vector<std::string> & headers,
										IN std::string & body,
										IN ZOO_INT32 timeout = 60);
		/*
         * @brief Download file from ota server.
         * @param url  
         * @param headers  
         * @param output_file   
         * @param timeout  
        **/ 
		void donwload(IN std::string url,
							IN std::vector<std::string> & headers,
								 IN std::string output_file,
								 IN ZOO_INT32 timeout = 60*10);

		/*
         * @brief Download file from ota server.
         * @param url  
         * @param output_file  
         * @param output_file   
         * @param md5    
         * @param timeout  
        **/ 
		void donwload(IN std::string url,
							IN std::vector<std::string> & headers,
								 IN std::string output_file,
								 IN std::string md5,
								 IN ZOO_INT32 timeout = 60*10);
	private:
		CURL * m_curl;
	};
}
#endif
