/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : PROCESSING_FLOW_FACADE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-16    Generator      created
*************************************************************/
#ifndef PROCESSING_FLOW_FACADE_INTERFACE_H
#define PROCESSING_FLOW_FACADE_INTERFACE_H

extern "C" 
{
    #include <ZOO.h>
    #include <UM4A_type.h>
}
#include "UM_COMMON_MACRO_DEFINE.h"
#include "MARKING_MODEL_INTERFACE.hpp"
#include <boost/shared_ptr.hpp>
#include "FLOW_FACADE_INTERFACE.h"

namespace UM
{
    class PROCESSING_FLOW_FACADE_INTERFACE : public virtual FLOW_FACADE_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       PROCESSING_FLOW_FACADE_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~PROCESSING_FLOW_FACADE_INTERFACE(){}
    public:
        /*
         * @brief UM4A_perform_upgrade
        **/
        virtual ZOO_INT32 perform_upgrade() = 0;

        /*
         * @brief UM4A_undo_the_upgrade
        **/
        virtual ZOO_INT32 undo_the_upgrade() = 0;

        /*
         * @brief UM4A_get_status
        **/
        virtual ZOO_INT32 get_status(OUT UM4A_STATUS_STRUCT * status) = 0;

		/*
         * @brief recover if abort abnormally.
        **/
        virtual void recover_if_abort_abnormally() = 0;

    };
}// namespace UM
#endif
