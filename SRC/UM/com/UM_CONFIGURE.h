/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : UM_CONFIGURE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef UM_CONFIGURE_H
#define UM_CONFIGURE_H
extern "C"
{
	#include "UM4A_type.h"
}
#include "ZOO_if.hpp"
#include "UM_COMMON_MACRO_DEFINE.h"
#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <utils/ENVIRONMENT_UTILITY_CLASS.h>

namespace UM
{
    class UM_CONFIGURE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       UM_CONFIGURE();

       /*
        * @brief Destructor
       **/ 
       virtual ~UM_CONFIGURE();
    public:
        /*
         * @brief Get instance
        **/
        static boost::shared_ptr<UM_CONFIGURE> get_instance();

        /*
         * @brief Initialize
        **/
        void initialize();

        /*
         * @brief Reload configurations
        **/
        void reload();

        /*
         * @brief Get execute path.
        **/
        std::string get_execute_path();

		/*
         * @brief Get download path.
        **/
        std::string get_download_path();

        std::string get_db_path();
    private:
        /*
         * @brief The instance
        **/
        static boost::shared_ptr<UM_CONFIGURE> m_instance;

        /*
         * @brief Execute base path
        **/
        std::string m_execute_path;
       
        /*
         * @brief uer file configure path
        **/
        ZOO_USER_PATH_STRUCT m_files_path;

        std::string m_mac_addr;
    };
} //namespace UM
#endif
