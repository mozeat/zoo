/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : EXECUTE_HELPER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef EXECUTE_HELPER_CLASS_H
#define EXECUTE_HELPER_CLASS_H
#include "EXECUTE_CLASS.h"
#include "UM_CONFIGURE.h"
#include <boost/process.hpp>
#include <boost/process/args.hpp>
#include <chrono>
namespace UM
{
    class EXECUTE_HELPER_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       EXECUTE_HELPER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~EXECUTE_HELPER_CLASS();
    public:

       /*
        * @brief invoke method.
       **/ 
       static void invoke(boost::shared_ptr<EXECUTE_CLASS> invoke_model);
    };
}// namespace UM
#endif
