/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : UM_COMMON_MACRO_DEFINE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef UM_COMMON_MACRO_DEFINE_H
#define UM_COMMON_MACRO_DEFINE_H
#include "ZOO_if.h"
#include "exceptions/PARAMETER_EXCEPTION_CLASS.h"

/*
 * @brief Wrapper try
 **/
#define __UM_TRY try

/*
* @brief Define a macro to throw UM exception
* @param error_code        The error code
* @param error_message     The error message
* @param inner_exception   The inner exception,& std::exception
**/
#define __THROW_UM_EXCEPTION(error_code,error_message,inner_exception) \
ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__," :: 0x%x - %s",error_code,error_message); \
throw ZOO_COMMON::PARAMETER_EXCEPTION_CLASS(error_code, error_message, inner_exception);

/*
* @brief Define a macro catch model exception
* @param continue        continue throw exception
**/
#define __UM_CATCH(continue) catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & e) \
{\
    if(continue) throw ZOO_COMMON::PARAMETER_EXCEPTION_CLASS(e.get_error_code(), e.get_error_message(),NULL);\
}

/*
* @brief Define a macro catch model exception
* @param continue        continue throw exception
**/
#define __UM_CATCH_ catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & e) 

/*
* @brief Define a macro to catch all exception
* @param result
**/
#define __UM_CATCH_ALL(result) catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & e) \
{\
	ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__," :: 0x%x - %s",e.get_error_code(),e.get_error_message()); \
    result = e.get_error_code();\
}\
catch(...){}


#endif
