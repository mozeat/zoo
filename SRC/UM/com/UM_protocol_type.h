/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : CRUL_WRAPPER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef UM_PROTOCAOL_TYPE_H
#define UM_PROTOCAOL_TYPE_H

/*
 * @brief Define the device exectue type enum
**/ 
typedef enum
{
	CLOSED = 0,
	PROCEEDING,
	CANCELED,
	SCHEDULED,
	REJECTED,
	RESUMED
}EXECUTION_TYPE_ENUM;

/*
 * @brief Define the device exectue result enum
**/ 
typedef enum
{
	SUCCESS = 0,
	FAILURE,
	NONE
}EXECUTION_RESULT_ENUM;

/*
 * @brief Define the device opertaion mode
**/
typedef enum
{
	MERGE = 0,
	REPLACE,
	REMOVE
}OPERATION_MODE_ENUM;

/*
 * @brief Define the device deployment type
**/
typedef enum
{
	SKIP = 0,
	ATTEMPT ,
	FORCED
}DEPLOYMENT_TYPE_ENUM;

#endif
