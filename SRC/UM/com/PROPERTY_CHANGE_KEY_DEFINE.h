/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : PROPERTY_CHANGE_KEY_DEFINE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef PROPERTY_CHANGE_KEY_DEFINE_H
#define PROPERTY_CHANGE_KEY_DEFINE_H
extern "C"
{
#include <ZOO.h>
}
/******************************************************************
* Define property key 
*****************************************************************/
#define OTA_SWM_IDLE            (0) //配置文件下载升级请求
#define OTA_SWM_CFG_REQ         (5) //配置文件下载升级请求
#define OTA_SWM_PKG_REQ         (6) //升级包下载请求
#define OTA_SWM_CFG_RDY         (7) //配置文件准备完成
#define OTA_SWM_PKG_RDY         (8) //升级包准备完成
#define OTA_SWM_CANCEL_REQ      (9) //
#define OTA_SWM_CANCELD         (10) //
#define OTA_SWM_WAITING         (11) //
#define OTA_SWM_INSTALLING      (12) //
#define OTA_SWM_SUCCESS	        (13) //

//Define deive install state machine
#define DEVICE_INSTALL_IDLE     (20)
#define DEVICE_INSTALL_ONGOING  (21)
#define DEVICE_INSTALL_FAIL     (22)
#define DEVICE_INSTALL_SUCCESS  (23)


#endif
