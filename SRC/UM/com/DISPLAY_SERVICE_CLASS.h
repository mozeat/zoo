/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : DISPLAY_SERVICE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef DISPLAY_SERVICE_CLASS_H
#define DISPLAY_SERVICE_CLASS_H
#include "EVENT_PUBLISHER_CLASS.h"
namespace UM
{
    class DISPLAY_SERVICE_CLASS 
    {
    public:
       /*
        * @brief Constructor
       **/ 
       DISPLAY_SERVICE_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~DISPLAY_SERVICE_CLASS();
    public:
		/*
		* @brief startup hmi
		**/
		ZOO_INT32 startup();

		/*
		* @brief shutdown hmi
		**/
		ZOO_INT32 shutdown();

		/*
		* @brief running state
		**/
		ZOO_BOOL is_running();
		
		/*
		* @brief Update status
		* @param status
		**/
		ZOO_INT32 update(OUT UM4A_STATUS_STRUCT * status,IN ZOO_INT32 error_code);

		/*
		* @brief Update inprogress status
		* @param status
		**/
		ZOO_INT32 update(OUT UM4A_INPROGRESS_STRUCT * inprogress,IN ZOO_INT32 error_code);		
	private:
		boost::shared_ptr<EVENT_PUBLISHER_CLASS> m_event_publisher;
		ZOO_BOOL m_is_running;
    };
}//namespace UM
#endif
