/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : UDS_MODEL_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef UDS_MODEL_CLASS_H
#define UDS_MODEL_CLASS_H
#include "DEVICE_INTERFACE.h"
namespace UM
{
    class UDS_MODEL_CLASS: virtual public DEVICE_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       UDS_MODEL_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~UDS_MODEL_CLASS();
    };
}// namespace UM

#endif

