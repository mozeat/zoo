/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : OTA_PROTOCAL_PARSER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef ENUM_REGISTER_CLASS_H_
#define ENUM_REGISTER_CLASS_H_


/*##########################################################################*
 Header File
 *###########################################################################*/
#include "converters/ENUM_BASE_CONVERTER_CLASS.h"
#include "UM_protocol_type.h"
#include "PROPERTY_CHANGE_KEY_DEFINE.h"

REGISTER_ENUM_BEGIN(EXECUTION_TYPE_ENUM)
{
        REGISTER_ENUM(CLOSED);
        REGISTER_ENUM(PROCEEDING);
        REGISTER_ENUM(CANCELED);
        REGISTER_ENUM(SCHEDULED);
        REGISTER_ENUM(REJECTED);
        REGISTER_ENUM(RESUMED);
}
REGISTER_ENUM_END;

REGISTER_ENUM_BEGIN(EXECUTION_RESULT_ENUM)
{
        REGISTER_ENUM(SUCCESS);
        REGISTER_ENUM(FAILURE);
        REGISTER_ENUM(NONE);
}
REGISTER_ENUM_END;

REGISTER_ENUM_BEGIN(OPERATION_MODE_ENUM)
{
        REGISTER_ENUM(MERGE);
        REGISTER_ENUM(REPLACE);
        REGISTER_ENUM(REMOVE);
}
REGISTER_ENUM_END;

REGISTER_ENUM_BEGIN(DEPLOYMENT_TYPE_ENUM)
{
        REGISTER_ENUM(SKIP);
        REGISTER_ENUM(ATTEMPT);
        REGISTER_ENUM(FORCED);
}
REGISTER_ENUM_END;

typedef int PROPERTY_KEY;

REGISTER_ENUM_BEGIN(PROPERTY_KEY)
{
        REGISTER_ENUM(OTA_SWM_IDLE);
        REGISTER_ENUM(OTA_SWM_CFG_REQ);
        REGISTER_ENUM(OTA_SWM_PKG_REQ);
        REGISTER_ENUM(OTA_SWM_CFG_RDY);
        REGISTER_ENUM(OTA_SWM_PKG_RDY);
        REGISTER_ENUM(OTA_SWM_CANCEL_REQ);
        REGISTER_ENUM(OTA_SWM_CANCELD);
        REGISTER_ENUM(OTA_SWM_WAITING);
        REGISTER_ENUM(OTA_SWM_INSTALLING);
        REGISTER_ENUM(OTA_SWM_SUCCESS);
}
REGISTER_ENUM_END;

#endif