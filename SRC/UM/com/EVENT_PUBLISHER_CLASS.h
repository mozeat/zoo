/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : EVENT_PUBLISHER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef EVENT_PUBLISHER_CLASS_H
#define EVENT_PUBLISHER_CLASS_H

extern "C" 
{
    #include <ZOO_if.h>
    #include <UM4A_type.h>
}
#include "UM_COMMON_MACRO_DEFINE.h"
#include <boost/shared_ptr.hpp>

namespace UM
{
    /*
     *@brief Define publish event to subsriber class.
    **/
    class EVENT_PUBLISHER_CLASS 
    {
    public:
       /*
        * @brief Constructor
       **/ 
       EVENT_PUBLISHER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~EVENT_PUBLISHER_CLASS();
    public:
       /*
        * @brief Publish upgrade_notify changed event to subscriber.
        * @param status
        * @param error_code
        * @param *context
       **/ 
       void publish_UMMA_raise_4A_status_subscribe(UM4A_STATUS_STRUCT status,ZOO_INT32 error_code,void *context);

       /*
        * @brief Publish upgrade_inprogress_status changed event to subscriber.
        * @param status
        * @param error_code
        * @param *context
       **/ 
       void publish_UMMA_raise_4A_inprogress_subscribe(UM4A_INPROGRESS_STRUCT status,ZOO_INT32 error_code,void *context);

    };
} // namespace UM

#endif
