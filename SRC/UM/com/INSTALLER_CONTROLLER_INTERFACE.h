/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : INSTALLER_CONTROLLER_INTERFACE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef INSTALLER_CONTROLLER_INTERFACE_H
#define INSTALLER_CONTROLLER_INTERFACE_H

#include "UM_COMMON_MACRO_DEFINE.h"
#include "SW_MODULE_CLASS.h"
#include "DEVICE_INTERFACE.h"
#include "CONTROLLER_INTERFACE.h"
#include "PROPERTY_CHANGE_KEY_DEFINE.h"
#include <map>
namespace UM
{
    class INSTALLER_CONTROLLER_INTERFACE: virtual public CONTROLLER_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       INSTALLER_CONTROLLER_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~INSTALLER_CONTROLLER_INTERFACE(){}
    public:
       /*
        * @brief Initialize the model
        * @return throw exception 
       **/ 
       virtual void initialize() = 0;

       /*
        * @brief install all models by configurations.
        * @param sw_module 
       **/ 
       virtual void install(IN boost::shared_ptr<SW_MODULE_CLASS> sw_module)   = 0;

       /*
        * @brief install all models by configurations.
        * @param type 
       **/ 
       virtual boost::shared_ptr<DEVICE_INTERFACE> get_model(std::string type) = 0;
       
    };
} // namespace UM

#endif
