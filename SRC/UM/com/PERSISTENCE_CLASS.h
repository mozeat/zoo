/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : PERSISTENCE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef PERSISTENCE_CLASS_H
#define PERSISTENCE_CLASS_H

extern "C" 
{
    #include <ZOO_if.h>
    #include <UM4A_type.h>
}
#include "UM_COMMON_MACRO_DEFINE.h"
#include <soci/soci.h>
#include <soci/sqlite3/soci-sqlite3.h>
#include <boost/shared_ptr.hpp>
namespace UM
{
    class PERSISTENCE_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       PERSISTENCE_CLASS(std::string db_file);

       /*
        * @brief Destructor
       **/ 
       virtual ~PERSISTENCE_CLASS();
    public:
       /*
        * @brief Set ota state
       **/ 
       void update_ota_state(IN ZOO_OTA_STATE_ENUM ota_state);

       /*
        * @brief Insert ota state
       **/ 
       void insert(IN ZOO_OTA_STATE_ENUM ota_state,std::string version = "unknown" ,std::string message = "unknown");
       
       /*
        * @brief Get ota state
       **/ 
       ZOO_OTA_STATE_ENUM  get_ota_state();

       std::string get_last_version();
    public:
       ZOO_INT32 get_counts();
        
    private:
        soci::session m_db;
        std::string m_version;
        std::string m_message;
    };
}// namespace UM
#endif
