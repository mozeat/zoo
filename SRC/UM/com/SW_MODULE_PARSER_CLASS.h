/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : SW_MODULE_PARSER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef SW_MODULE_PARSER_CLASS_H
#define SW_MODULE_PARSER_CLASS_H

extern "C"
{
	#include "UM4A_type.h"
}
#include "UM_CONFIGURE.h"
#include "SW_MODULE_CLASS.h"
#include "EXECUTE_CLASS.h"
#include <vector>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/optional.hpp>

namespace UM
{
    class SW_MODULE_PARSER_CLASS
    {
    public:
       /*
        * @brief Constructor
       **/ 
       SW_MODULE_PARSER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~SW_MODULE_PARSER_CLASS();
    public:
    
       /*
        * @brief Parse software modules
        * @param file   config.json file path
       **/ 
       static std::vector<boost::shared_ptr<SW_MODULE_CLASS> > parse_modules(std::string file);

       /*
        * @brief Parse major software version
        * @param file   config.json file path
       **/ 
       static std::string parse_major_software_version(std::string file);
    };
} //namespace UM
#endif
