/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : INSTALL_SEQUENCE_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef INSTALL_SEQUENCE_CLASS_H
#define INSTALL_SEQUENCE_CLASS_H
#include <vector>
#include "SW_MODULE_CLASS.h"
#include <boost/shared_ptr.hpp>
namespace UM
{
	class INSTALL_SEQUENCE_CLASS 
	{
	public:
        /*
         * @brief Constructor
        **/ 
		INSTALL_SEQUENCE_CLASS();

        /*
         * @brief Destructor
        **/ 
        virtual ~INSTALL_SEQUENCE_CLASS();
	public:  

		/*
         * @brief  Parse SW modules from ota config file.
        **/
		void parse_modules(std::string file);
	
		/*
         * @brief Get first SW module.
         **/ 
		boost::shared_ptr<SW_MODULE_CLASS> get_first_module();

		/*
         * @brief Set next SW module.
         **/ 
		void set_next_module(boost::shared_ptr<SW_MODULE_CLASS> module);

		/*
         * @brief Get next SW module.
        **/ 
		boost::shared_ptr<SW_MODULE_CLASS> get_next_module();

		/*
         * @brief Set current SW module.
         **/
		void set_current_module(boost::shared_ptr<SW_MODULE_CLASS> module);
		
		/*
         * @brief Get current SW module.
         **/ 
		boost::shared_ptr<SW_MODULE_CLASS> get_current_module();
		
		/*
		 * @brief Get last SW module.
		 **/ 
		boost::shared_ptr<SW_MODULE_CLASS> get_last_module();

		/*
		 * @brief Get next SW module.
		 **/ 
		void move_to_next();

		/*
		 * @brief Get next SW module.
		 **/ 
		int get_module_number();
		
		/*
		 * @brief Clear SW module.
		**/ 
		void clear();

        /*
		 * @brief Set SW modules.
	     * @param modules.
		 **/ 
		void set_modules(std::vector<boost::shared_ptr<SW_MODULE_CLASS>> modules);

        /*
		 * @brief Get SW modules.
		 **/ 
		std::vector<boost::shared_ptr<SW_MODULE_CLASS>> get_mudules();

        /*
		 * @brief Get SW modules.
		 **/ 
        const std::string & get_major_version();
	private:
        /*
         * @brief The major SW module.
        **/ 
        std::string m_major_software_version;
        
		/*
         * @brief The current SW module.
        **/ 
		boost::shared_ptr<SW_MODULE_CLASS> m_next_module;

		/*
         * @brief The current SW module.
        **/ 
		boost::shared_ptr<SW_MODULE_CLASS> m_current_module;
		
		/*
        * @brief The list of SW module
        **/ 
        std::vector<boost::shared_ptr<SW_MODULE_CLASS>> m_modules;
	};
}
#endif
