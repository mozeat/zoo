/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : FLOW_FACADE_INTERFACE.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef FLOW_FACADE_INTERFACE_H
#define FLOW_FACADE_INTERFACE_H

extern "C" 
{
    #include <ZOO_if.h>
    #include <UM4A_type.h>
    #include <SM4A_if.h>
}
#include "UM_COMMON_MACRO_DEFINE.h"
#include "MARKING_MODEL_INTERFACE.hpp"
#include "STATE_MANAGER_CLASS.h"
#include "INSTALLER_CONTROLLER_INTERFACE.h"
#include "DISPLAY_SERVICE_CLASS.h"

namespace UM
{
    class FLOW_FACADE_INTERFACE: public virtual         PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>,
    									  public virtual PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>
    {
    public:
       /*
        * @brief Constructor
       **/ 
       FLOW_FACADE_INTERFACE(){}

       /*
        * @brief Destructor
       **/ 
       virtual ~FLOW_FACADE_INTERFACE(){}
    public:

       virtual void set_event_publisher(IN boost::shared_ptr<EVENT_PUBLISHER_CLASS> event_publisher) = 0;
       
       /*
        * @brief Set device controller instance
       **/ 
       virtual void set_device_controller(IN boost::shared_ptr<INSTALLER_CONTROLLER_INTERFACE> device_controller) = 0;

       /*
        * @brief Set state manager instance
       **/ 
       virtual void set_state_manager(IN boost::shared_ptr<STATE_MANAGER_CLASS> state_manager) = 0;

		/*
        * @brief install_sequence attribute value
        * @param install_sequence  The new install_sequence attribute value 
       **/
	   virtual void set_install_sequence(boost::shared_ptr<INSTALL_SEQUENCE_CLASS> install_sequence) = 0;

	   /*
        * @brief Set display service instance
       **/ 
	   virtual void set_display_service(boost::shared_ptr<UM::DISPLAY_SERVICE_CLASS> display_service) = 0;
    };
} // namespace UM

#endif
