/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : UMMA_event.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-16    Generator      created
*************************************************************/
#ifndef UMMA_EVENT_H
#define UMMA_EVENT_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ZOO.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include "UM4I_type.h"
#include "UM4I_if.h"
#include "UM4A_type.h"

/**
 *@brief UMMA_raise_4A_perform_upgrade
**/
ZOO_EXPORT ZOO_INT32 UMMA_raise_4A_perform_upgrade(IN ZOO_INT32 error_code,
                                                       IN UM4I_REPLY_HANDLE reply_handle);

/**
 *@brief UMMA_raise_4A_notify_recieved_ack
**/
ZOO_EXPORT ZOO_INT32 UMMA_raise_4A_undo_the_upgrade(IN ZOO_INT32 error_code,
                                                           IN UM4I_REPLY_HANDLE reply_handle);

/**
 *@brief UMMA_raise_4A_get_status
 *@param status
**/
ZOO_EXPORT ZOO_INT32 UMMA_raise_4A_get_status(IN ZOO_INT32 error_code,
                                                  IN UM4A_STATUS_STRUCT status,
                                                  IN UM4I_REPLY_HANDLE reply_handle);

/**
 *@brief UM4A_status_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
ZOO_EXPORT void UMMA_raise_4A_status_subscribe(IN UM4A_STATUS_STRUCT status,IN ZOO_INT32 error_code,IN void *context);

/**
 *@brief UM4A_inprogress_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
ZOO_EXPORT void UMMA_raise_4A_inprogress_subscribe(IN UM4A_INPROGRESS_STRUCT status,IN ZOO_INT32 error_code,IN void *context);


#endif // UMMA_event.h
