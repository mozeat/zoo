/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : UM_FLOW_FACADE_WRAPPER.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef UM_FLOW_FACADE_WRAPPER_H
#define UM_FLOW_FACADE_WRAPPER_H

#ifdef __cplusplus
extern "C" 
{
#endif
    #include "UM4A_type.h"
    /*
     * @brief UM4A_perform_upgrade
    **/
     ZOO_INT32 UM_perform_upgrade();

    /*
     * @brief UM4A_undo_the_upgrade
    **/
     ZOO_INT32 UM_undo_the_upgrade();

    /*
     * @brief UM4A_get_status
    **/
     ZOO_INT32 UM_get_status(OUT UM4A_STATUS_STRUCT * status);

#ifdef __cplusplus
}
#endif
#endif
