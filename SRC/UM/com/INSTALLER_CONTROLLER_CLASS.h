/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : INSTALLER_CONTROLLER_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef INSTALLER_CONTROLLER_CLASS_H
#define INSTALLER_CONTROLLER_CLASS_H
#include "DEVICE_INTERFACE.h"
#include "CONTROLLER_ABSTRACT_CLASS.h"
#include "INSTALLER_CONTROLLER_INTERFACE.h"

namespace UM
{
	typedef enum
	{
		INSTALLER_DPKG = 0,
		INSTALLER_UDS = 1
	}INSTALLER_TYPE_ENUM;
	
    class INSTALLER_CONTROLLER_CLASS: public virtual CONTROLLER_ABSTRACT_CLASS,
    										public virtual INSTALLER_CONTROLLER_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       INSTALLER_CONTROLLER_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~INSTALLER_CONTROLLER_CLASS();
    public:
       /*
        * @brief Initialize the model
        * @return throw exception 
       **/ 
       void initialize();

       /*
        * @brief Create all models by configurations.
       **/ 
       void create_all_models();

       /*
        * @brief install all models by configurations.
       **/ 
       void install(IN boost::shared_ptr<SW_MODULE_CLASS> sw_module);

       /*
        * @brief Clean
       **/ 
       OVERRIDE
       void clean();
       /*
        * @brief Notify property has been changed
        * @property_name     The property has been changed
       **/ 
       OVERRIDE
       void on_property_changed(MARKING_MODEL_INTERFACE* model,const ZOO_UINT32 property_name);

		boost::shared_ptr<DEVICE_INTERFACE> get_model(std::string type);
    private:
       /*
        * @brief Create one model by the given type. and insert into m_sw_module_map container
       **/ 
       void create_model_by_type(IN ZOO_INT32 type);

    private:
       /*
        * @brief Define device entities.
       **/ 
       std::map<ZOO_INT32,boost::shared_ptr<DEVICE_INTERFACE> > m_device_map;
    };
} //namespace UM
#endif
