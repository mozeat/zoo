/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : FLOW_FACADE_ABSTRACT_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef FLOW_FACADE_ABSTRACT_CLASS_H
#define FLOW_FACADE_ABSTRACT_CLASS_H

extern "C" 
{
    #include <ZOO.h>
    #include <UM4A_type.h>
}
#include "UM_COMMON_MACRO_DEFINE.h"
#include "FLOW_FACADE_INTERFACE.h"
#include <boost/shared_ptr.hpp>

namespace UM
{
    class FLOW_FACADE_ABSTRACT_CLASS : public virtual FLOW_FACADE_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       FLOW_FACADE_ABSTRACT_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~FLOW_FACADE_ABSTRACT_CLASS();
    public:

       void set_event_publisher(IN boost::shared_ptr<EVENT_PUBLISHER_CLASS> event_publisher);

       /*
        * @brief Set device controller instance
       **/ 
       void set_device_controller(IN boost::shared_ptr<INSTALLER_CONTROLLER_INTERFACE> device_controller);

       /*
        * @brief Get device controller instance
       **/ 
       boost::shared_ptr<INSTALLER_CONTROLLER_INTERFACE>  get_device_controller();

       /*
        * @brief Set state manager instance
       **/ 
       void set_state_manager(IN boost::shared_ptr<STATE_MANAGER_CLASS> state_manager);

       /*
        * @brief Get state manager instance
       **/ 
       boost::shared_ptr<STATE_MANAGER_CLASS>  get_state_manager();

		/*
        * @brief install_sequence attribute value
        * @param install_sequence  The new install_sequence attribute value 
       **/
	   void set_install_sequence(boost::shared_ptr<INSTALL_SEQUENCE_CLASS> install_sequence);

	   /*
        * @brief Set display service instance
       **/ 
	   void set_display_service(boost::shared_ptr<UM::DISPLAY_SERVICE_CLASS> display_service);
	   
       /*
        * @brief This method is executed when property changed value
        * @param model The source object contains property changed
        * @param property_name The property has been changed value
       **/ 
       virtual void on_property_changed(IN CONTROLLER_INTERFACE* model,IN const ZOO_UINT32 property_name);

		
		/*
		 * @brief This method is executed when property changed value
		 * @param model The source object contains property changed
		 * @param property_name The property has been changed value
		**/ 
		virtual void on_property_changed(IN MARKING_MODEL_INTERFACE* model,IN const ZOO_UINT32 property_name);
    protected:

    	boost::shared_ptr<EVENT_PUBLISHER_CLASS> m_event_publisher;
    	
		/*
		* @brief The device controller instance
		**/ 
		boost::shared_ptr<INSTALLER_CONTROLLER_INTERFACE>  m_device_controller;

		/*
		* @brief The state manager instance
		**/ 
		boost::shared_ptr<STATE_MANAGER_CLASS>  m_state_manager;

		/*
		* @brief The install_sequence instance
		**/ 
		boost::shared_ptr<INSTALL_SEQUENCE_CLASS>  m_module_sequence;

		/*
		* @brief The display instance
		**/ 
		boost::shared_ptr<UM::DISPLAY_SERVICE_CLASS> m_display_service;
    };
} //namespace UM

#endif
