/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : DPKG_MODEL_CLASS.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#ifndef DPKG_MODEL_CLASS_H
#define DPKG_MODEL_CLASS_H
#include "DEVICE_INTERFACE.h"
namespace UM
{
    class DPKG_MODEL_CLASS: virtual public DEVICE_INTERFACE
    {
    public:
       /*
        * @brief Constructor
       **/ 
       DPKG_MODEL_CLASS();

       /*
        * @brief Destructor
       **/ 
       virtual ~DPKG_MODEL_CLASS();
    };
}// namespace UM

#endif
