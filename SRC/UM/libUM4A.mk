include ../Makefile_tpl_cov
TARGET   := libUM4A.so
SRCEXTS  := .c .cpp 
INCDIRS  := ./inc ./com
SOURCES  := 
SRCDIRS  := ./lib 
CFLAGS   := -fPIC
CXXFLAGS := -std=c++14
CPPFLAGS := $(GCOV_FLAGS) -fPIC
LDFLAGS  := $(GCOV_LINK)  -lnsl -shared

include ../Project_config
include ../Makefile_tpl_linux