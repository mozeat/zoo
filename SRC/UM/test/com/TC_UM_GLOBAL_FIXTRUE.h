/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : UM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
extern "C"
{
    #include <UM4A_if.h>
    #include <UM4A_type.h>
    #include <MM4A_if.h>
}
#include <boost/test/included/unit_test.hpp>
#include <boost/test/tools/output_test_stream.hpp>
#include <boost/test/results_reporter.hpp>
#include <boost/test/unit_test_log.hpp>
#include <string>
/**
* @brief Define a test suite entry/exit,so that the setup function is called only once
* upon entering the test suite.
*/
struct TC_UM_GOLBAL_FIXTURE
{
    TC_UM_GOLBAL_FIXTURE()
    {
        BOOST_TEST_MESSAGE("TC global fixture initialize ...");
        MM4A_initialize();
    }

    ~TC_UM_GOLBAL_FIXTURE()
    {
        BOOST_TEST_MESSAGE("TC teardown ...");
        MM4A_terminate();
    }
};

BOOST_TEST_GLOBAL_FIXTURE(TC_UM_GOLBAL_FIXTURE);


ZOO_UINT32 g_notify_fd;
ZOO_UINT32 g_inprogress_fd;
/**
 * @brief Define test report output formate, default --log_level=message.
*/
struct TC_UM_REPORTER
{
    TC_UM_REPORTER():reporter("../../../reporter/UM_TC_result.reporter")
    {
        boost::unit_test::unit_test_log.set_stream( reporter );
        boost::unit_test::unit_test_log.set_threshold_level(boost::unit_test::log_test_units);
    }

    ~TC_UM_REPORTER()
    {
        boost::unit_test::unit_test_log.set_stream( std::cout );;
    }
   std::ofstream reporter;
};

BOOST_TEST_GLOBAL_CONFIGURATION(TC_UM_REPORTER);

