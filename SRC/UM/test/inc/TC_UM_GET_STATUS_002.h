/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : UM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
extern "C"
{
    #include <UM4A_if.h>
    #include <UM4A_type.h>
    #include <MM4A_if.h>
}
#include <chrono>
#include <thread>
BOOST_AUTO_TEST_SUITE(TC_UM_GET_STATUS_002)


	void upgrage_notify_callback(UM4A_STATUS_STRUCT status,
												IN ZOO_INT32 error_code,
													IN void *context)
	{
		if(status.state == ZOO_OTA_STATE_READY)
		{
			BOOST_TEST(UM4A_undo_the_upgrade() == OK);
			BOOST_TEST(UM4A_perform_upgrade() == OK);
		}
		BOOST_TEST_MESSAGE(__ZOO_FUNC__ << " status.state = "<< status.state);
		BOOST_TEST_MESSAGE(__ZOO_FUNC__ << " error_code = "<< error_code);
	}
	
	void upgrage_inprogress_callback(UM4A_INPROGRESS_STRUCT status,
												IN ZOO_INT32 error_code,
													IN void *context)
	{
		BOOST_TEST_MESSAGE(__ZOO_FUNC__ << " status.package_id = "<< status.package_id);
		BOOST_TEST_MESSAGE(__ZOO_FUNC__ << " status.inprogress = "<< status.inprogress);
		BOOST_TEST_MESSAGE(__ZOO_FUNC__ << " error_code = "<< error_code);
	}
	
    BOOST_AUTO_TEST_CASE( TC_UM_GET_STATUS_002_001 )
    {
        BOOST_TEST(UM4A_status_subscribe(upgrage_notify_callback,&g_notify_fd, NULL) == OK);
        BOOST_TEST(UM4A_inprogress_subscribe(upgrage_inprogress_callback,&g_inprogress_fd, NULL) == OK);
    }

    BOOST_AUTO_TEST_CASE( TC_UM_GET_STATUS_002_002 )
    {
        UM4A_STATUS_STRUCT status = UM4A_STATUS_STRUCT();
        BOOST_TEST(UM4A_get_status(&status) == OK);
        while(status.state != ZOO_OTA_STATE_INSTALLING)
        {
        	BOOST_TEST(UM4A_get_status(&status) == OK);
			BOOST_TEST_MESSAGE(__ZOO_FUNC__ << " status.state = "<< status.state);
        	std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }

BOOST_AUTO_TEST_SUITE_END()