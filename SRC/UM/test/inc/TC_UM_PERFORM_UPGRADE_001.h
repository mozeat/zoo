/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : UM_unit_test.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
extern "C"
{
    #include <UM4A_if.h>
    #include <UM4A_type.h>
    #include <MM4A_if.h>
}
BOOST_AUTO_TEST_SUITE(TC_UM_PERFORM_UPGRADE_001)

    /**
     *@brief 
    **/
    BOOST_AUTO_TEST_CASE( TC_UM_PERFORM_UPGRADE_001_001 )
    {
        BOOST_TEST(UM4A_perform_upgrade() == OK);
    }

BOOST_AUTO_TEST_SUITE_END()