/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : PROCESSING_FLOW_FACADE_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "PROCESSING_FLOW_FACADE_CLASS.h"
#include "utils/THREAD_POOL.h"
#include "PROPERTY_CHANGE_KEY_DEFINE.h"
#include "OTA_AGENT_CLASS.h"
#include "ZOO_enum_to_str.h"
#include "TASK_MANAGER_CLASS.h"
namespace UM
{
    #define __OPEN_SESSION__ OTA_AGENT_CLASS::get_instance()->open_session();
    
    #define __CLOSE_SESSION__ OTA_AGENT_CLASS::get_instance()->close_session();

    #define __PUBLISH_RESULT_TO_OTA_SERVER(result,message) OTA_AGENT_CLASS::get_instance()->publish(result,message)

    #define OTA_AGENT OTA_AGENT_CLASS::get_instance()
    
    /*
     * @brief Constructor
    **/ 
    PROCESSING_FLOW_FACADE_CLASS::PROCESSING_FLOW_FACADE_CLASS()
    {
		this->m_module_sequence.reset(new INSTALL_SEQUENCE_CLASS());
		this->m_display_service.reset(new DISPLAY_SERVICE_CLASS());
		this->m_wait_handle.reset(new ZOO_COMMON::AUTO_RESET_EVENT_CLASS(ZOO_FALSE));
    }

    /*
     * @brief Constructor
    **/ 
    PROCESSING_FLOW_FACADE_CLASS::~PROCESSING_FLOW_FACADE_CLASS()
    {

    }

    /*
     * @brief Perform upgrade
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::perform_upgrade()
    {
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry ...");
		ZOO_INT32 error_code = OK;
		__UM_TRY
		{
			/*
			 *@brief step 1: check dirver state is ilde.
			*/
			this->check_state_is(ZOO_DRIVER_STATE_IDLE);

			/*
			 *@brief step 2: check ota state is ready.
			*/
			this->check_ota_state_is(ZOO_OTA_STATE_READY);
			
			/*
			 *@brief step 3: handle upgrade async.
			*/
			ZOO_COMMON::THREAD_POOL::get_instance()->queue_working([&]()
			{
				this->handle_upgrade();
			});
		}
       __UM_CATCH_ALL(error_code)
		
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function exit ...");
		return error_code;
    }

    /*
     * @brief Undo the upgrade
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::undo_the_upgrade()
    {
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry ...");
        ZOO_INT32 error_code = OK;
       
		__UM_TRY
		{
			/*
			 *@brief step 1: check dirver state is ilde.
			*/
			this->check_state_is(ZOO_DRIVER_STATE_IDLE);

			/*
			 *@brief step 2: check ota state is ready.
			*/
			this->check_ota_state_is(ZOO_OTA_STATE_READY);
			
			/*
			 *@brief step 3: handle cancel upgrade.
			*/ 
			__OPEN_SESSION__
            {
                ZOO_CHAR message[128];
                sprintf(message,"%s","User undo the upgrade");
                __PUBLISH_RESULT_TO_OTA_SERVER(FAILURE,message);
            }
			__CLOSE_SESSION__
		}
       __UM_CATCH_ALL(error_code)
        
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function exit ...");
        return error_code;
    }

    /*
     * @brief Get status
     * @param status the property attribute
    **/
    ZOO_INT32 PROCESSING_FLOW_FACADE_CLASS::get_status(OUT UM4A_STATUS_STRUCT * status)
    {
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry ...");
        ZOO_INT32 error_code = OK;
       __UM_TRY
       {
           status->state = this->m_state_manager->get_ota_state();
           status->packages = this->m_module_sequence->get_module_number();
           sprintf(status->major_version,"%s",this->m_module_sequence->get_major_version().data());
           status->major_version[UM4A_VERISON_LENGTH - 1] = '\0';
           
           std::string release_note;
           std::vector<boost::shared_ptr<SW_MODULE_CLASS>> entity_sw_modules =
                                                this->m_module_sequence->get_mudules();
           std::for_each(entity_sw_modules.begin(),entity_sw_modules.end(),[&](auto & module)
           {
               release_note.append(module->get_release_note());
           }); 
           
           if(release_note.size() < UM4A_BUFFER_LENGTH)
           {
               sprintf(status->release_note,"%s",release_note.data());
           }
           else
           {
               memcpy(status->release_note,release_note.data(),UM4A_BUFFER_LENGTH);
           }
           
           release_note[UM4A_BUFFER_LENGTH -1] = '\0';
           ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,
           										":: state: %s,packages: %d",
           										ZOO_ota_state_enum_to_str(status->state),
           										status->packages);
       }
       __UM_CATCH_ALL(error_code)
       ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function exit ...");
       return error_code;
    }

	/*
     * @brief recover if abort abnormally.
    **/
    void PROCESSING_FLOW_FACADE_CLASS::recover_if_abort_abnormally()
    {
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry ...");
	    __UM_TRY
		{
			auto state = this->m_state_manager->get_ota_state();
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,
									":: last ota state is %s",
									ZOO_ota_state_enum_to_str(state));
			switch(state)
			{
				case ZOO_OTA_STATE_READY:
				case ZOO_OTA_STATE_INSTALLING:
				{
			        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,":: %s","resume install ...now");
					this->m_module_sequence->set_modules(OTA_AGENT_CLASS::get_instance()->create_software_modules());
					this->handle_upgrade();
					break;
				}
				case ZOO_OTA_STATE_FINISHED:
				case ZOO_OTA_STATE_IDLE:
				case ZOO_OTA_STATE_DOWNLOADING:
				default:
				{
					this->stop_all_running_tasks();
					this->startup_all_tasks();
					break;
				}
			}
		}
		__UM_CATCH(ZOO_FALSE)
		this->m_state_manager->set_ota_state(ZOO_OTA_STATE_IDLE);
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function exit...");
    }

    /*
     * @brief This method is executed when property changed value
     * @param model             The model type
     * @param property_name     The property has been changed value
    **/ 
    void PROCESSING_FLOW_FACADE_CLASS::on_property_changed(CONTROLLER_INTERFACE * model,const ZOO_UINT32 property_name)
    {
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,":: property_id: %d",property_name);
    	switch(property_name)
    	{	
    		case OTA_SWM_CFG_RDY:
    		{
    			this->m_state_manager->save(ZOO_OTA_STATE_DOWNLOADING,"unknow","unknow");
    			this->m_module_sequence->clear();
                this->m_device_controller->clean();
                this->m_device_controller->initialize();
				break;
    		}
			case OTA_SWM_PKG_RDY:
			{							
    			this->m_state_manager->save(ZOO_OTA_STATE_READY,this->m_module_sequence->get_major_version(),
    			                                                    this->m_module_sequence->get_major_version());
				this->m_module_sequence->set_modules(
							OTA_AGENT_CLASS::get_instance()->get_software_modules());
				
                /*
                 *@brief associate device with SW module.
                **/
                this->associate_device_with_software_model(this->m_module_sequence,this->m_device_controller);
                
				/** 
			     *@brief inform app new packages arrived.
			    */
			    this->notify_upgrade_request(this->m_module_sequence);

                if(OTA_AGENT_CLASS::get_instance()->get_process_type() == FORCED)
                {
                    /**
    				 *@brief begin session with OTA server.handle cancel operation
    				*/
    				ZOO_COMMON::THREAD_POOL::get_instance()->queue_working(
    				[this]()
    				{
    					this->handle_upgrade();
    				});
                }
                else
                {
    			    /**
    				 *@brief begin session with OTA server.handle cancel operation
    				*/
    				ZOO_COMMON::THREAD_POOL::get_instance()->queue_working(
    				[]()
    				{
    					OTA_AGENT_CLASS::get_instance()->handle_session();
    				});
				}
				
				break;
			}
			
			case OTA_SWM_CANCELD:
			{
				std::unique_lock<std::mutex> l(this->m_sync_lock);
				this->m_state_manager->set_ota_state(ZOO_OTA_STATE_IDLE);
				this->m_state_manager->set_driver_state(ZOO_DRIVER_STATE_IDLE);
    			this->notify_upgrade_request(this->m_module_sequence);
				break;
			}
			
    		case DEVICE_INSTALL_ONGOING:
    		{
    			std::ostringstream os;
    			auto current_module = this->m_module_sequence->get_current_module();
    			if(current_module)
    			{
    				os << "[" << (current_module->get_order()) << "]: ";
    				os << "<" << (current_module->get_package_name()) << "> is installing.";
    				ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION, __ZOO_FUNC__,
												":: ONGOING: %s",os.str().c_str());
    			}
    			break;
    		}

    		case DEVICE_INSTALL_SUCCESS:
    		{
    			std::ostringstream os;
    			auto current_module = this->m_module_sequence->get_current_module();
    			if(current_module)
    			{
    				os << "[" << (current_module->get_order()) << "]: ";
    				os << "<" << (current_module->get_package_name()) << "> upgrade success.";
    				ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION, __ZOO_FUNC__,
												":: SUCCESS: %s",os.str().c_str());
    			}
    			break;
    		}
    		case DEVICE_INSTALL_FAIL:
    		{
				this->m_state_manager->set_ota_state(ZOO_OTA_STATE_IDLE);
				this->m_state_manager->set_driver_state(ZOO_DRIVER_STATE_IDLE);
    			this->notify_state_changed(UM4A_SYSTEM_ERR);
    			std::ostringstream os;
    			auto current_module = this->m_module_sequence->get_current_module();
    			if(current_module)
    			{
    				os << "order[" << (current_module->get_order()) << "]: ";
    				os << "" << (current_module->get_package_name()) << " upgrade failure.";
    				ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION, __ZOO_FUNC__,
												":: SUCCESS: %s",os.str().c_str());
    			}
    			break;
    		}
    		default:
			{
    			std::unique_lock<std::mutex> l(this->m_sync_lock);
				this->m_state_manager->set_ota_state(ZOO_OTA_STATE_IDLE);
				this->m_state_manager->set_driver_state(ZOO_DRIVER_STATE_IDLE);
    			this->notify_upgrade_request(this->m_module_sequence);
    			ZOO_slog(IN ZOO_SEVERITY_LEVEL_WARNING, __ZOO_FUNC__,
    										":: unhandle property_name:%d changed",
    										property_name);
    			break;
    		}
        }
        //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function exit...");
    }

	/*
	 * @brief This method is executed when property changed value
	 * @param model The source object contains property changed
	 * @param property_name The property has been changed value
	**/ 
	OVERRIDE
	void PROCESSING_FLOW_FACADE_CLASS::on_property_changed(IN MARKING_MODEL_INTERFACE* model,
																	IN const ZOO_UINT32 property_name)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry ...");
		switch(property_name)
		{
	    	default:break;
    	}
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function exit...");
	}
	
	/*
	* @brief Handle upgrade ...
	**/
	void PROCESSING_FLOW_FACADE_CLASS::handle_upgrade()
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry ...");
		__OPEN_SESSION__
		{
			__UM_TRY
			{
				/*
				 *@brief step : Prepare install package.
				*/
				this->prepare_install_package(this->m_module_sequence);

				/*
				 *@brief step : Handle install package.
				*/
				this->handle_install_package(this->m_module_sequence);

				/*
				 *@brief step : Post install package.
				*/
				this->post_install_package(this->m_module_sequence);

				/*
				 *@brief step : Post install package.
				*/
				__PUBLISH_RESULT_TO_OTA_SERVER(SUCCESS,"SUCCESS");

			}
			__UM_CATCH_
			{
				ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__,
													":: error_code:0x%02x,message:%s",
													e.get_error_code(),
													e.get_error_message());
				__PUBLISH_RESULT_TO_OTA_SERVER(FAILURE,e.get_error_message());
			}
			catch(std::exception & e)
			{
				__PUBLISH_RESULT_TO_OTA_SERVER(FAILURE,e.what());
			}
		}
		__CLOSE_SESSION__
		
        /*
         * @brief step : reset flow.
        */
        this->reset_flow();
        
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function exit...");
	}
	
    /*
	 * @brief Check current state is ...
	 * @param state  driver state
	 **/
	void PROCESSING_FLOW_FACADE_CLASS::check_state_is(IN ZOO_DRIVER_STATE_ENUM state)
	{
		auto current_state = this->m_state_manager->get_driver_state();
		if(state != current_state)
		{
			ZOO_CHAR message[128] = {0};
			sprintf(message,":: current state is %s,input state is %s",
							ZOO_driver_state_enum_to_str(current_state),
							ZOO_driver_state_enum_to_str(state));
			__THROW_UM_EXCEPTION(UM4A_ILLEGAL_CALL_ERR, message, NULL);
		}
	}

	/*
	* @brief Check current state is ...
	 * @param state  ota state
	**/
	void PROCESSING_FLOW_FACADE_CLASS::check_ota_state_is(IN ZOO_OTA_STATE_ENUM state)
	{
		auto current_state = this->m_state_manager->get_ota_state();
		if(state != current_state)
		{
			ZOO_CHAR message[128] = {0};
			sprintf(message,":: current state is %s,input state is %s",
							ZOO_ota_state_enum_to_str(current_state),
							ZOO_ota_state_enum_to_str(state));
			__THROW_UM_EXCEPTION(UM4A_ILLEGAL_CALL_ERR, message, NULL);
		}
	}

	/*
	* @brief Stop tasks ...
	**/
	void PROCESSING_FLOW_FACADE_CLASS::stop_all_running_tasks()
	{
		auto error_code = TASK_MANAGER_CLASS::stop_all_tasks();
		if(OK != error_code)
		{
			ZOO_CHAR message[128] = {0};
			sprintf(message,":: can not stop tasks [%x]  ...",error_code);
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, message, NULL);
		}
	}
	
	/*
	 * @brief startup tasks ...
	**/
	void PROCESSING_FLOW_FACADE_CLASS::startup_all_tasks()
	{
		auto error_code = TASK_MANAGER_CLASS::start_all_tasks();
		if(OK != error_code)
		{
			ZOO_CHAR message[128] = {0};
			sprintf(message,":: can not start tasks [%x]  ...",error_code);
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, message, NULL);
		}
	}

	/*
	 * @brief Show hmi upgrade informations .
	**/
	void PROCESSING_FLOW_FACADE_CLASS::prepare_install_package(boost::shared_ptr<INSTALL_SEQUENCE_CLASS> sequence)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry...");
		if(sequence == NULL)
		{
			ZOO_CHAR message[128] = {0};
			sprintf(message,"%s",":: sequence pointer is null  ...");
			__THROW_UM_EXCEPTION(UM4A_PARAMETER_ERR, message, NULL);
		}
		std::string last_version = this->m_state_manager->get_last_version();
		std::string new_version  = this->m_module_sequence->get_major_version();
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
													":: install version from %s to %s,sequence size is %lu",
													last_version.c_str(),
													new_version.c_str(),
													sequence->get_mudules().size());

		/*
		 *@brief step : check state is ilde.
		*/
		this->m_state_manager->set_ota_state(ZOO_OTA_STATE_INSTALLING);
		this->m_state_manager->set_driver_state(ZOO_DRIVER_STATE_BUSY);

		/*
		 *@brief step : check package is ready,
		                this requirement is used to upgrade offline,
		                while packages are post from mobilephone
		*/
		if(this->m_module_sequence->get_mudules().empty())
		{
		    this->m_module_sequence->set_modules(OTA_AGENT_CLASS::get_instance()->create_software_modules());
		}

		/*
		 *@brief step : check packages ready.
		*/
		if(this->m_module_sequence->get_mudules().empty())
		{
		    __THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, ":: no packages found", NULL);
		}
		
		/*
		 *@brief step : stop all tasks.
		*/
		this->stop_all_running_tasks();

		/*
		 *@brief Startup UMUI.
		**/
		ZOO_COMMON::THREAD_POOL::get_instance()->queue_working(
		[&]()
		{	
			this->m_display_service->startup();
		});
		
		/*
		 *@brief wait UMUI startup.
		**/
		std::this_thread::sleep_for(std::chrono::seconds(2));	
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function exit...");
	}
	
    /*
	 * @brief handle_install_package
	 * @param sw_module
	 **/
	void PROCESSING_FLOW_FACADE_CLASS::handle_install_package(boost::shared_ptr<INSTALL_SEQUENCE_CLASS> sequence)
	{
		if(sequence == nullptr)
		{
			ZOO_CHAR message[128] = {0};
			sprintf(message,"%s",":: sequence pointer is null  ...");
			__THROW_UM_EXCEPTION(UM4A_PARAMETER_ERR, message, nullptr);
		}

		auto modules = sequence->get_mudules(); 
		std::for_each(modules.begin(),modules.end(),[&](auto & module)
		{
			sequence->set_current_module(module);
			auto device = module->get_device();
			if(device == nullptr)
			{
				ZOO_CHAR message[128] = {0};
				sprintf(message,"%s",":: device pointer is null  ...");
				__THROW_UM_EXCEPTION(UM4A_PARAMETER_ERR, message, nullptr);
			}
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,
			        "::%s install %s",device->get_marking_code().data(),
			        module->get_package_name().data());
		    OTA_AGENT->publish(modules.size(),module->get_order(),module->get_package_name() + " is ongoing ...");
			device->install(module);
			this->notify_inprogress_changed(module);
		});
	}

	/*
	* @brief post_install_package
	* @param sw_module
	* @param device
	**/
	void PROCESSING_FLOW_FACADE_CLASS::post_install_package(boost::shared_ptr<INSTALL_SEQUENCE_CLASS> sequence)
	{
		if(sequence == NULL)
		{
			ZOO_CHAR message[128] = {0};
			sprintf(message,"%s",":: sequence pointer is null  ...");
			__THROW_UM_EXCEPTION(UM4A_PARAMETER_ERR, message, NULL);
		}

		/*
		 * @brief step : upgrade inprogress value
		**/
		auto modules = sequence->get_mudules(); 
		std::for_each(modules.begin(),modules.end(),[&](auto & module)
		{
			/*
			 * @brief Send moduel packages info to UMUI.
			**/
			this->notify_inprogress_changed(module);
		});

		/*
		 *@brief step : Startup all tasks.
		*/
		this->startup_all_tasks();

	}

	/*
	* @brief notify_upgrade_request
	**/
	void PROCESSING_FLOW_FACADE_CLASS::notify_upgrade_request(boost::shared_ptr<INSTALL_SEQUENCE_CLASS> sequence)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry ...");
		if(sequence == NULL)
		{
			ZOO_CHAR message[128] = {0};
			sprintf(message,"%s",":: sequence pointer is null  ...");
			__THROW_UM_EXCEPTION(UM4A_PARAMETER_ERR, message, NULL);
		}
		
		UM4A_STATUS_STRUCT status = UM4A_STATUS_STRUCT();
		status.state = this->m_state_manager->get_ota_state();
		status.packages = sequence->get_module_number();
		sprintf(status.major_version,"%s",sequence->get_major_version().data());
        status.major_version[UM4A_VERISON_LENGTH - 1] = '\0';

        std::string release_note;
        std::vector<boost::shared_ptr<SW_MODULE_CLASS>> entity_sw_modules =
                                            sequence->get_mudules();
        std::for_each(entity_sw_modules.begin(),entity_sw_modules.end(),[&](auto & module)
        {
            release_note.append(module->get_release_note());
            status.estimate_total_time += module->get_estimate_consume_minutes();
        }); 

        if(release_note.size() < UM4A_BUFFER_LENGTH)
        {
            sprintf(status.release_note,"%s",release_note.data());
        }
        else
        {
            memcpy(status.release_note,release_note.data(),UM4A_BUFFER_LENGTH);
        }  
		this->m_event_publisher->publish_UMMA_raise_4A_status_subscribe(status,OK,NULL);

		auto modules = sequence->get_mudules();
		std::for_each(modules.begin(),modules.end(),[&](auto & module)
		{
			if(module == NULL)
			{
				ZOO_CHAR message[128] = {0};
				sprintf(message,"%s",":: module pointer is null  ...");
				__THROW_UM_EXCEPTION(UM4A_PARAMETER_ERR, message, NULL);
			}
			this->notify_inprogress_changed(module);
		});
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function exit ...");
	}

	/*
	 * @brief notify inprogress changed
	**/
	void PROCESSING_FLOW_FACADE_CLASS::notify_inprogress_changed(boost::shared_ptr<SW_MODULE_CLASS> module)
	{
		UM4A_INPROGRESS_STRUCT inprogress = UM4A_INPROGRESS_STRUCT();
		inprogress.package_id = module->get_order();
		inprogress.inprogress = module->get_inprogress_value();
		
		if(module->get_version().size() < UM4A_BUFFER_LENGTH)
		{
			sprintf(inprogress.version,"%s",module->get_version().c_str());
		}
		else
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,
								__ZOO_FUNC__,":: version string size out of range...");
		}

		if(module->get_release_note().size() < UM4A_BUFFER_LENGTH)
		{
			sprintf(inprogress.description,"%s",module->get_release_note().c_str());
		}
		else
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,
								__ZOO_FUNC__,":: release note string size out of range ");
		}
		this->m_event_publisher->publish_UMMA_raise_4A_inprogress_subscribe(inprogress,OK,NULL);
	}
	
	/*
	 * @brief notify inprogress changed
	**/
	void PROCESSING_FLOW_FACADE_CLASS::notify_state_changed(ZOO_INT32 error_code)
	{
	    /*
		 * @brief Send total packages info to UMUI.
		**/
		UM4A_STATUS_STRUCT status = UM4A_STATUS_STRUCT();
		status.state = this->m_state_manager->get_ota_state();
		status.packages = this->m_module_sequence->get_module_number();
		this->m_event_publisher->publish_UMMA_raise_4A_status_subscribe(status,error_code,NULL);
	}
	
	/*
	 * @brief reset flow
	**/
	void PROCESSING_FLOW_FACADE_CLASS::reset_flow()
	{
		/*
		 *@brief step : Reset state to idle.
		*/
		this->m_state_manager->set_ota_state(ZOO_OTA_STATE_FINISHED);
		this->notify_state_changed();
    	this->m_state_manager->set_driver_state(ZOO_DRIVER_STATE_IDLE);
    	this->m_state_manager->set_ota_state(IN ZOO_OTA_STATE_IDLE);
		this->notify_state_changed();
    	this->m_device_controller->clean();
    	this->m_module_sequence->clear();
	}
	
    /*
     * @brief Associate  SW module with device controller
     * @param sequence         the SW modules
     * @param controller       the installer tool
    **/
    void PROCESSING_FLOW_FACADE_CLASS::associate_device_with_software_model(IN boost::shared_ptr<INSTALL_SEQUENCE_CLASS> sequence,
                                                                              IN boost::shared_ptr<INSTALLER_CONTROLLER_INTERFACE> controller)
	{
	    auto modules = sequence->get_mudules(); 
		std::for_each(modules.begin(),modules.end(),[&](auto & SW_module)
		{
		    auto type = SW_module->get_type();
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,":: device type:%s",type.c_str());
			auto device = controller->get_model(type);
			SW_module->set_device(device);
		});
	}
} //namespace UM
