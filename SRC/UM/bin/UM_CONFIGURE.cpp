/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : UM_CONFIGURE.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "UM_CONFIGURE.h"
//#include <sys/socket.h>
//#include <sys/ioctl.h>
//#include <linux/if.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>


namespace UM
{
    /*
     * @brief The instance
    **/
    boost::shared_ptr<UM_CONFIGURE> UM_CONFIGURE::m_instance = NULL;

    /*
     * @brief Constructor
    **/ 
    UM_CONFIGURE::UM_CONFIGURE()
    {

    }

    /*
     * @brief Destructor
    **/ 
    UM_CONFIGURE::~UM_CONFIGURE()
    {

    }

    /*
     * @brief Get instance
    **/
    boost::shared_ptr<UM_CONFIGURE> UM_CONFIGURE::get_instance()
    {
        if(UM_CONFIGURE::m_instance == NULL)
        {
            UM_CONFIGURE::m_instance.reset(new UM_CONFIGURE());
        }
        return UM_CONFIGURE::m_instance;
    }

    /*
     * @brief Initialize
    **/
    void UM_CONFIGURE::initialize()
    {
        UM_CONFIGURE::m_instance->reload();
    }
    
    /*
     * @brief Reload configurations
    **/
    void UM_CONFIGURE::reload()
    {
        uid_t ruid,euid,suid;
        if(0 == getresuid(&ruid,&euid,&suid))
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
                                            "real:%d teffective:%d tset-user:%d n",
                                            ruid,euid,suid);
        }

        // Get download path
        if(OK != ZOO_get_user_files_path(&this->m_files_path))
        {
             ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,":: get file path failed");
        }

        // Check download path exists
        if(boost::filesystem::exists(this->m_files_path.output.download))
        {
            boost::system::error_code ec;
            boost::filesystem::create_directories(this->m_files_path.output.download,ec);
            if(ec)
            {
                ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,
                        ":: create path %s failed",this->m_files_path.output.download);
                struct passwd *pw = getpwuid(getuid());
                std::string dowload_path = std::string(pw->pw_dir) + "/" + "Downloads";  
                sprintf(this->m_files_path.output.download,"%s",dowload_path.c_str());
                ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,__ZOO_FUNC__,
                        ":: set download path to %s",this->m_files_path.output.download);
                if(!boost::filesystem::exists(dowload_path))
                {
                    boost::filesystem::create_directories(dowload_path,ec);
                }         
            }
        }

        if(boost::filesystem::exists(this->m_files_path.output.db))
        {
            boost::system::error_code ec;
            boost::filesystem::create_directories(this->m_files_path.output.db,ec);
            if(ec)
            {
                ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,
                        ":: create path %s failed",this->m_files_path.output.db);
                struct passwd *pw = getpwuid(getuid());
                std::string db_path = std::string(pw->pw_dir) + "/" + "data/DB";  
                sprintf(this->m_files_path.output.db,"%s",db_path.c_str());
                ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,__ZOO_FUNC__,
                        ":: set download path to %s",this->m_files_path.output.db);
                if(!boost::filesystem::exists(db_path))
                {
                    boost::filesystem::create_directories(db_path,ec);
                }         
            }
        }
    }

    /*
     * @brief Initialize
    **/
    std::string UM_CONFIGURE::get_execute_path()
    {
        return ZOO_COMMON::ENVIRONMENT_UTILITY_CLASS::get_execute_path();;
    }

	/*
     * @brief Get download path.
    **/
    std::string UM_CONFIGURE::get_download_path()
    {
        return this->m_files_path.output.download;
    }

    std::string UM_CONFIGURE::get_db_path()
    {
        return this->m_files_path.output.db;
    }
} //namespace UM
