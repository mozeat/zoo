/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product		  : ZOO
 * Component id   : UM
 * File Name	  : OTA_AGENT_CLASS.cpp
 * Description	  : {Summary Description}
 * History		  : 
 * Version		  date			author		   context 
 * V1.0.0		  2019-07-12	Generator	   created
*************************************************************/
#include "OTA_AGENT_CLASS.h"
namespace UM
{

	constexpr ZOO_UINT32 MINUTES_30 = 1000*60*3;
	
	/*
     * @brief 
    **/ 
	boost::shared_ptr<OTA_AGENT_CLASS> OTA_AGENT_CLASS::m_instance;
	
	/*
     * @brief Constructor
    **/ 
	OTA_AGENT_CLASS::OTA_AGENT_CLASS()
	:m_cfg_json_file("config.json")
	,m_stopped(ZOO_FALSE)
	,m_do_session(ZOO_FALSE)
	,m_state(OTA_SWM_IDLE)
	,m_process_type(ATTEMPT)
	,m_req_interval(60*3)
    {
		this->m_wait_handle.reset(new ZOO_COMMON::AUTO_RESET_EVENT_CLASS(ZOO_FALSE));
		this->m_protocol.reset(new OTA_PROTOCAL_PARSER_CLASS());
    }

    /*
     * @brief Destructor
    **/ 
    OTA_AGENT_CLASS::~OTA_AGENT_CLASS()
    {
    	
    }
    
    /*
     * @brief Destructor
    **/ 
	boost::shared_ptr<OTA_AGENT_CLASS> OTA_AGENT_CLASS::get_instance()
    {
        if(OTA_AGENT_CLASS::m_instance == NULL)
        {
        	OTA_AGENT_CLASS::m_instance.reset(new OTA_AGENT_CLASS());
        }
        return OTA_AGENT_CLASS::m_instance;
    }
    
	/*
	 * @brief Load 
	**/ 
	void OTA_AGENT_CLASS::load_configuration()
	{
	    
        ZOO_OTA_CFG_STRUCT info;
        if(OK != ZOO_get_OTA_info(&info))
		{
			ZOO_CHAR message[128] = {0};
			sprintf(message,"%s","get ota config fail");
			__THROW_UM_EXCEPTION(UM4A_ILLEGAL_CALL_ERR, message, NULL);
		}
		
		ZOO_CHAR resource_id [ ZOO_RESOURCE_ID_LENGHT ] = {0};
		if(OK != ZOO_get_resource_id(resource_id))
		{
			ZOO_CHAR message[128] = {0};
			sprintf(message,"%s","get resource id fail");
		    sprintf(resource_id,"UNKNOWN_%ld",time(NULL));
		}
		
	    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,":: resource_id:%s", resource_id);

        ZOO_USER_VERSION_STRUCT user_version;
		if(OK != ZOO_get_user_version_info(&user_version))
		{
		    
			ZOO_CHAR message[128] = {0};
			sprintf(message,"%s","get user info fail");
			//__THROW_UM_EXCEPTION(UM4A_ILLEGAL_CALL_ERR, message, NULL);
			this->m_hardware_version = "Default";
		}
        this->m_hardware_version = user_version.project;
	    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,":: project:%s", user_version.project);

		this->set_host_info(info.user,info.domain,resource_id,info.token);
	}

	/*
     * @brief Run communitate routines to ota server
    **/ 
	void OTA_AGENT_CLASS::run()
    {
   		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","> function entry ...");
    	ZOO_INT32 error_code = OK;
    	auto address = this->m_protocol->get_url(this->m_domain,
    												this->m_resource_id,
    												this->m_host_name);									
        while(!this->m_stopped)
        {	
        	__UM_TRY
        	{
        		this->clean();
				this->listen(address);
				this->download_config_file();
				this->notify_of_property_changed(OTA_SWM_CFG_RDY);
				this->download_packages();
				this->notify_of_property_changed(OTA_SWM_PKG_RDY);
				this->m_wait_handle->wait_one();
        	}
        	__UM_CATCH_ALL(error_code)
	        std::this_thread::sleep_for(std::chrono::seconds(this->m_req_interval));
        }
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","< function exit ...error_code[%d]",error_code);
    }
    
    /*
     * @brief Stop routines
    **/ 
	void OTA_AGENT_CLASS::stop()
	{
		this->m_stopped = ZOO_TRUE;
		this->m_wait_handle->reset();
	}
	
	/*
     * @brief process cancal action 
    **/
	void OTA_AGENT_CLASS::open_session()
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","> function entry ...");
		this->m_do_session = ZOO_TRUE;							
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","< function exit ...");
	}

	/*
     * @brief process cancal action 
    **/
	void OTA_AGENT_CLASS::close_session()
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","> function entry ...");
		this->m_do_session = ZOO_FALSE;
		this->m_wait_handle->set();
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","< function exit ...");
	}

	/*
	 * @brief handle session .
	**/
	void OTA_AGENT_CLASS::handle_session()
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","> function entry ...");
		while(1)
		{
			__UM_TRY
        	{	
        		auto canceled = this->is_canceled_action_trigged();
        		if(this->m_do_session)
        		{	
        			if(canceled)
        				this->handle_cancel_action(!canceled);
        			break;
        		}
        		
    			if(canceled)
    			{
    				this->handle_cancel_action(canceled);
    				this->notify_of_property_changed(OTA_SWM_CANCELD);
    				break;
    			}
        	}
        	__UM_CATCH(ZOO_FALSE)
			std::this_thread::sleep_for(std::chrono::seconds(10));
		}								
		this->m_wait_handle->set();
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","< function exit ...");
	}
	
	/*
	 * @brief Publish uprgrade result to OTA server.
	 * @param result the upgrade result
	 * @param message the debug message
	**/ 
	void OTA_AGENT_CLASS::publish(EXECUTION_RESULT_ENUM result,std::string message)
    {
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","> function entry ...");
		auto res = this->to_lower(ENUM_CONVERTER(EXECUTION_RESULT_ENUM)().to_string(result));
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
											":: result:%s,message:%s",
											res.c_str(),
											message.c_str());
		auto finished = 
			this->to_lower(ENUM_CONVERTER(EXECUTION_TYPE_ENUM)().to_string(result == NONE?PROCEEDING:CLOSED));
    	auto url = this->m_protocol->get_deployment_feedback_url(this->m_domain,
    												this->m_resource_id,
    												this->m_host_name,this->m_action_id);
    	auto headers = this->m_protocol->get_headers(this->m_token);
    	auto body = this->m_protocol->build_feedback_message(this->m_action_id,
				    											res,finished,message);
		auto curl = boost::make_shared<CURL_WRAPPER_CLASS>()->post(url,headers,body);
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","< function exit ...");
    }

    /*
     * @brief Publish uprgrade result to OTA server.
     * @param total_packages  
     * @param cur_packages   
     * @param message  
    **/ 
    void OTA_AGENT_CLASS::publish(int total_packages,
                           int cur_packages,
                                    std::string message)
    {
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
											":: total_packages:%d,cur_packages:%d",
											total_packages,
											cur_packages);
											
    	auto url = this->m_protocol->get_deployment_feedback_url(this->m_domain,
    												this->m_resource_id,
    												this->m_host_name,
    												this->m_action_id);
    												
    	auto headers = this->m_protocol->get_headers(this->m_token);
    	auto body = this->m_protocol->build_feedback_message(this->m_action_id,
																			"none",
																			total_packages,
																			cur_packages,
																			"proceeding",
																			message);
		auto curl = boost::make_shared<CURL_WRAPPER_CLASS>()->post(url,headers,body);
    }
    
	/*
      * @brief brief Add observer will be notified when property changed
      * @param observer  Property changed observer
    **/ 
    void OTA_AGENT_CLASS::add_observer(IN PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* observer)
    {
        this->m_observers.push_back(observer);
    }

    /*
      * @brief Add observer will be notified when property changed
      * @param observer  Property changed observer
    **/ 
    void OTA_AGENT_CLASS::remove_observer(IN  PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* observer)
    {
        boost::ignore_unused(observer);
    }

    /*
     * @brief Clean.
    **/ 
    void OTA_AGENT_CLASS::clean()
    {
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "> function entry ...");	
        this->m_action_id = "";
        this->m_deployment_url = "";
        this->m_config_json_url = "";
        //this->m_cfg_data_url = "";
        this->m_state = OTA_SWM_IDLE;
        this->m_process_type = ATTEMPT;
        this->m_do_session = ZOO_FALSE;
        __UM_TRY
        {
	        auto modules = SW_MODULE_PARSER_CLASS::parse_modules(this->m_cfg_json_file);
	        for (auto m : modules)
	        {
	        	auto path = UM_CONFIGURE::get_instance()->get_download_path();
	        	auto file = path + "/" + m->get_package_name();
	        	this->remove_file(file);
	        }
        }
        __UM_CATCH(ZOO_FALSE)
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "< function exit ...");	
    }

    /*
     * @brief This method is executed when property changed value
     * @param model The source object contains property changed
     * @param property_name The property has been changed value
    **/ 
    void OTA_AGENT_CLASS::notify_of_property_changed(IN const ZOO_UINT32 property_name)
    {
        std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>*>::iterator observer_itr =
            this->m_observers.begin();
        while (observer_itr != this->m_observers.end())
        {
            PROPERTY_CHANGED_OBSERVER_INTERFACE<CONTROLLER_INTERFACE>* observer = *observer_itr;;
            observer->on_property_changed(this, property_name);
            observer_itr ++;
        }
    }
    
	/*
	 * @brief This method is executed when property changed value
	 * @param model 			The model type
	 * @param property_name 	The property has been changed value
	**/
	void OTA_AGENT_CLASS::on_property_changed(MARKING_MODEL_INTERFACE* model,
															const ZOO_UINT32 property_name)
    {
        boost::ignore_unused(model);
        boost::ignore_unused(property_name);
    }

    /*
    * @brief Get fg file.
    **/ 
    std::string OTA_AGENT_CLASS::get_ota_cfg_file()
    {
    	return this->m_cfg_json_file;
    }

    /*
	 * @brief Set_domain .
     * @param domain             The domain
	**/ 
	void OTA_AGENT_CLASS::set_host_info(std::string host_name,
											 	std::string domain,
												std::string resource_id,
												std::string token)
    {
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: host_name:%s",host_name.c_str());
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: domain:%s",domain.c_str());
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: resource_id:%s",resource_id.c_str());
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: token:%s",token.c_str());
    	this->m_domain = domain;
    	this->m_token = token;
    	this->m_resource_id = resource_id;
    	this->m_host_name = host_name;
    }

	/*
	 * @brief get_SW_modules .
	 * @return modules
	**/ 							
	std::vector<boost::shared_ptr<SW_MODULE_CLASS> > OTA_AGENT_CLASS::get_software_modules()
	{
		return this->m_cfg_sw_modules;
	}

	/*
	 * @brief Create SW_modules .
     * @return modules
	**/ 
	std::vector<boost::shared_ptr<SW_MODULE_CLASS> > OTA_AGENT_CLASS::create_software_modules()
	{
		return SW_MODULE_PARSER_CLASS::parse_modules(this->m_cfg_json_file);
	}
	
	/*
	 * @brief get deployment_url .
     * @return std::string
	**/
	std::string OTA_AGENT_CLASS::get_deployment_url()
	{
		return this->m_deployment_url;
	}

	/*
     * @brief Get process type : force or attempt
     * @return the process type
    **/
    DEPLOYMENT_TYPE_ENUM OTA_AGENT_CLASS::get_process_type()
	{
		return this->m_process_type;
	}

    /*
	 * @brief Listen .
     * @param domain             The domain
	**/
	void OTA_AGENT_CLASS::listen(std::string domain)
	{
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");	
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, 
        						":: url: %s",domain.c_str());	
		auto headers = this->m_protocol->get_headers(this->m_token);	
		auto curl = boost::make_shared<CURL_WRAPPER_CLASS>(); 
		std::string reply = curl->get(domain,headers);
		if(reply.empty())
		{
	        __THROW_UM_EXCEPTION(UM4A_TIMEOUT_ERR, ":: No upgrade package request.", NULL);
        }

		/**
         *@brief Reply the last cancel action if any.
        */
		/*__UM_TRY
		{
			this->m_cancel_action_url = this->m_protocol->get_cancel_action_url(reply);
			this->handle_cancel_action(ZOO_TRUE);
		}
		__UM_CATCH(ZOO_FALSE)*/

		auto sleep = this->m_protocol->get_polling_sleep_time(reply);
		this->m_req_interval = sleep;
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, 
												":: sleep : %d",sleep);
		/**
		 *@brief Put device info to server
		*/
    	__UM_TRY
    	{
			this->m_cfg_data_url = this->m_protocol->get_config_data_url(domain,this->m_resource_id,
	    												                    this->m_host_name);
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, 
									":: config data url: %s",this->m_cfg_data_url.c_str());
	       	this->register_device_info(this->m_cfg_data_url,this->m_resource_id,this->m_hardware_version);
       	}
       	__UM_CATCH(ZOO_FALSE)

	    /**
     	 *@brief Parse deployment message;
    	*/
		__UM_TRY
    	{
       		this->m_deployment_url = this->m_protocol->get_deployment_base_url(reply);
        	ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION,__ZOO_FUNC__, 
        						":: deployment url: %s",this->m_deployment_url.c_str());
	        this->parse_deployment_modules(
	        	std::move(curl->get(this->m_deployment_url,headers)));
       	}
       	__UM_CATCH(ZOO_TRUE)
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit ...");	
	}

	/*
	 * @brief parse_deployment_modules .
     * @param deployment_json            
	**/
	void OTA_AGENT_CLASS::parse_deployment_modules(std::string deployment_json)
	{
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","> function entry ...");
		
        this->m_process_type = this->m_protocol->parse_update_processing_type(deployment_json);        
		this->m_action_id = this->m_protocol->get_id_from_deployment(deployment_json);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION,__ZOO_FUNC__, 
        						":: action_id: %s",
        						this->m_action_id.c_str());        						
		this->m_entity_sw_modules = this->m_protocol->parse_software_modules(std::move(deployment_json));		
		std::for_each(this->m_entity_sw_modules.begin(),this->m_entity_sw_modules.end(),[&](auto & m)
		{
			if(m->get_package_name() == this->m_cfg_json_file)
			{
				this->m_config_json_url = m->get_download_url();
        		ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION,__ZOO_FUNC__, 
        						":: config.json_url: %s",
        						this->m_config_json_url.c_str());
			}
		});
		
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, 
					        						":: modules size: %lu",
					        						this->m_entity_sw_modules.size());
        //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","< function exit ...");						
	}
	
	/*
	 * @brief download_config_json .
     * @param 
	**/
	void OTA_AGENT_CLASS::download_config_file()
	{
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","> function entry ...");
		auto headers = this->m_protocol->get_headers(this->m_token);
		auto path = UM_CONFIGURE::get_instance()->get_download_path();
		if(!boost::filesystem::exists(path))
		{
			boost::filesystem::create_directory(path);
		}
		
		auto file = path + "/" + this->m_cfg_json_file;
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: file: %s",file.c_str());
		if(boost::filesystem::exists(file))
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: remove the old file: %s ...BEGIN",file.c_str());
			this->remove_file(file);
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: remove the old file: %s ...END",file.c_str());
		}
		
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
							":: %s http url: %s",
								this->m_cfg_json_file.c_str(),
								this->m_config_json_url.c_str());

		//find config json file module  
		auto ite = std::find_if(this->m_entity_sw_modules.begin(),this->m_entity_sw_modules.end(),
		[&](auto & m)
		{
			return (m->get_package_name() == this->m_cfg_json_file);
		});

		if(ite == this->m_entity_sw_modules.end())
		{
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "config.json module is missing ...", NULL);
		}
		
		auto config_json_module = *ite;
		auto md5 = config_json_module->get_package_md5();
		auto curl = boost::make_shared<CURL_WRAPPER_CLASS>(); 
		curl->donwload(this->m_config_json_url,headers,file,md5);
		this->m_cfg_sw_modules = this->get_cfg_modules_from(this->m_cfg_json_file, 
															this->m_entity_sw_modules);
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","< function exit ...");
	}
	

	/*
	 * @brief download_packages .
	**/
	void OTA_AGENT_CLASS::download_packages()
	{
		auto headers = this->m_protocol->get_headers(this->m_token);
		auto path = UM_CONFIGURE::get_instance()->get_download_path();
		if(!boost::filesystem::exists(path))
		{
			boost::filesystem::create_directory(path);
		}
		auto curl = boost::make_shared<CURL_WRAPPER_CLASS>(); 
		std::for_each(this->m_cfg_sw_modules.begin(),this->m_cfg_sw_modules.end(),
		[&](auto & module)
		{	
			auto url =  module->get_download_url();
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: url: %s",url.c_str());
			auto file = path + "/" + module->get_package_name();
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: file: %s",file.c_str());
			if(boost::filesystem::exists(file))
			{
				ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: remove the old file: %s ...BEGIN",file.c_str());
				this->remove_file(file);
				ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: remove the old file: %s ...END",file.c_str());
			}
    		curl->donwload(url,headers,file,module->get_package_md5());
		});
	}
	
	/*
	 * @brief download_packages .
	 * @param vin_code		   
	 * @param hw_version		   
	**/
	void OTA_AGENT_CLASS::register_device_info(std::string url,
													std::string vin_code,
														std::string hw_version)
	{
		auto headers = this->m_protocol->get_headers(this->m_token);
		auto mode = "merge",finished = "success",execution = "closed",details = "config data";
        auto cfg_data = this->m_protocol->build_feedback_message(this->m_action_id,
																			mode,
																			vin_code ,
																			hw_version,
																			finished,
																			execution,
																			details);
		ZOO_slog(ZOO_SEVERITY_LEVEL_NOTIFICATION,__ZOO_FUNC__, 
        						":: cfg data: %s",cfg_data.c_str());		
        auto curl = boost::make_shared<CURL_WRAPPER_CLASS>(); 						
        curl->put(url,headers,cfg_data);
	}

	/*
	 * @brief remove file 
     * @param file         
     * @return bool   
	**/
	ZOO_BOOL OTA_AGENT_CLASS::remove_file(std::string file)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: filename: %s",file.c_str());
		boost::system::error_code ec;
		if(boost::filesystem::exists(file))
		{
			boost::filesystem::remove(file,ec);
		}
		else
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: %s","is not exist");
		}
		
		if(ec)
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, " : %s","failed");
			return ZOO_FALSE;
		}
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: %s","success");
		return ZOO_TRUE;
	}
	
	/*
	 * @brief remove_all_files
	 * @param path	  
	**/
	void OTA_AGENT_CLASS::remove_all_files(std::string path)
	{
        boost::system::error_code ec;
        boost::filesystem::remove_all(path,ec);
        if(ec)
        {
        	ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,
        						__ZOO_FUNC__,
        						":: remove path[%s] fail[%s]",
        						path.c_str(),ec.message().c_str());
        }
	}

	/*
	 * @brief update state machine  
	**/
	void OTA_AGENT_CLASS::update_state(int state)
	{
		ENUM_CONVERTER(PROPERTY_KEY) converter;
		if(this->m_state != state)
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, 
								":: OTA agent state changes from %s to %s",
								converter.to_string(this->m_state),
								converter.to_string(state));
			this->m_state = state;
		}
		else
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, 
								":: OTA agent state is the same as %s",
								converter.to_string(state));
		}
	}

	/*
	* @brief handle_cancel_action .
	* @param rejected 
	**/
	void  OTA_AGENT_CLASS::handle_cancel_action(ZOO_BOOL do_cancel)
	{
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","> function entry...");
		auto headers = this->m_protocol->get_headers(this->m_token);
		auto path = UM_CONFIGURE::get_instance()->get_download_path();
		std::string finished = this->to_lower(ENUM_CONVERTER(EXECUTION_RESULT_ENUM)().to_string(SUCCESS));
		std::string execution = this->to_lower(ENUM_CONVERTER(EXECUTION_TYPE_ENUM)().to_string(REJECTED));
		if(do_cancel)
		{
			execution = this->to_lower(ENUM_CONVERTER(EXECUTION_TYPE_ENUM)().to_string(CLOSED));
		}
		auto body = this->m_protocol->build_feedback_message(this->m_action_id,
										finished,
										execution,
										do_cancel ? "canceled":"installing");
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, 
												":: action id:%s",
												this->m_action_id.c_str());	
		auto url = this->m_protocol->get_cancel_feedback_url(this->m_domain,this->m_resource_id,
	    												this->m_host_name,this->m_action_id);
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, 
												":: cancel_action_url:%s",
												url.c_str());	
		auto curl = boost::make_shared<CURL_WRAPPER_CLASS>();
		curl->post(url,headers,body);		
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","< function exit...");								
	}

	/*
	 * @brief handle_cancel_action .
	 * @param rejected 
	**/
	ZOO_BOOL OTA_AGENT_CLASS::is_canceled_action_trigged()
	{
	    //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","> function entry...");
		try
		{
			auto url = this->m_protocol->get_cancel_action_url(this->m_domain,this->m_resource_id,
	    												this->m_host_name,this->m_action_id);	
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, 
												":: cancel url is %s",url.c_str());
			auto headers = this->m_protocol->get_headers(this->m_token);
			auto curl = boost::make_shared<CURL_WRAPPER_CLASS>();
			std::string reply = curl->get(url,headers);
			std::string stop_id =  this->m_protocol->parse_stop_id(reply);
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, 
												":: current action id is %s,req stop id is %s",
												this->m_action_id.c_str(),
												stop_id.c_str());
			if(stop_id == this->m_action_id)	
			{
			    return ZOO_TRUE;
			}
		    //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","< function exit...");										
	        return ZOO_FALSE;
        }
        catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & e)
        {
        	ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,"%s",e.what());
		    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: cancel action is not trigged");
		    //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","< function exit...");	
			return ZOO_FALSE;
        }
        catch(std::exception & e)
        {
        	ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,"%s",e.what());
		    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, ":: cancel action is not trigged");
		    //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__, "%s","< function exit...");	
			return ZOO_FALSE;
        }
	}

	/*
 	 * @brief to lower alpha
 	 * @param lower alpha 
	**/
	std::string OTA_AGENT_CLASS::to_lower(std::string    upper)
	{
		 std::transform(upper.begin(), upper.end(), upper.begin(), 
                   [](unsigned char c){ return std::tolower(c); });
        return upper;
	}
	
	/*
 	 * @brief Get cfg modules from all deploy modules
 	 * @param config_file        config.json
 	 * @param entity_modules 
	**/
	std::vector<boost::shared_ptr<SW_MODULE_CLASS> > OTA_AGENT_CLASS::get_cfg_modules_from(
								std::string config_file,
								const std::vector<boost::shared_ptr<SW_MODULE_CLASS> > & entity_modules)
	{
		std::vector<boost::shared_ptr<SW_MODULE_CLASS> > results;
		auto config_modules = SW_MODULE_PARSER_CLASS::parse_modules(config_file);
		for (auto & m : entity_modules)
		{
			for(auto & c : config_modules)
			{
				if(c->get_package_name() == m->get_package_name())
				{
				    std::vector<boost::shared_ptr<EXECUTE_CLASS> > executors =
				        c->get_executors();
				    m->set_executors(executors);
					results.emplace_back(m);
				}
			}
		}
		return results;
	}
}
