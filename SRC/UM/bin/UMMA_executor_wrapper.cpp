/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : UMMA_executor_wrapper.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include <UMMA_executor_wrapper.h>
#include <ZOO_COMMON_MACRO_DEFINE.h>

#include "utils/THREAD_POOL.h"
#include "PROCESSING_FLOW_FACADE_CLASS.h"
#include "STATE_MANAGER_CLASS.h"
#include "INSTALLER_CONTROLLER_CLASS.h"
#include "UM_CONFIGURE.h"
#include "DISPLAY_SERVICE_CLASS.h"
#include "OTA_AGENT_CLASS.h"
/*
 * @brief Define the device controller.
**/
boost::shared_ptr<UM::INSTALLER_CONTROLLER_INTERFACE> g_device_controller;

/*
 * @brief Define processing flow instance.
**/
boost::shared_ptr<UM::PROCESSING_FLOW_FACADE_INTERFACE> g_processing_flow;

/*
 * @brief Define the state manager.
**/
boost::shared_ptr<UM::STATE_MANAGER_CLASS> g_state_manager;

/*
 * @brief Define the display manager.
**/
boost::shared_ptr<UM::DISPLAY_SERVICE_CLASS> g_display_service;

/*
 * @brief Define the event publish.
**/
boost::shared_ptr<UM::EVENT_PUBLISHER_CLASS> g_event_publish;

/**  
 * @brief Register system signal handler,throw PARAMETER_EXCEPTION_CLASS if register fail,
 * the default signal handling is save stack trace to the log file and generate a dump file at execute path.
 * register a self-defined callback to SYSTEM_SIGNAL_HANDLER::resgister_siganl will change the default behavior.
**/
static void UMMA_register_system_signals()
{
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > function entry ...");
    __ZOO_SYSTEM_SIGNAL_REGISTER(UM4I_COMPONET_ID,SIGSEGV); 
    __ZOO_SYSTEM_SIGNAL_REGISTER(UM4I_COMPONET_ID,SIGABRT);

    /* Add more signals if needs,or register self-defined callback function
       to change the default behavior... */
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < function exit ...");
}

/**
 *@brief Execute the start up flow.
 * This function is executed in 3 steps: 
 * Step 1: Load configurations 
 * Step 2: Create controllers
 * Step 3: Create facades and set controllers to created facades
**/ 
void UMMA_startup(void)
{
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > function entry ...");
    ZOO_INT32 error_code = OK;
	__UM_TRY
	{
	    /**
	     * @brief Signal handler for system behavior 
	    */
	    UMMA_register_system_signals();

	    /** 
	     *@brief Step 1: Load configurations 
	    */
	    UM::UM_CONFIGURE::get_instance()->initialize();
	    
	    /** 
	     *@brief Startup thread pool. 
	    */
	    ZOO_COMMON::THREAD_POOL::get_instance()->startup();

	    /** 
	     *@brief Step 2: Create controllers 
	    */
	    g_state_manager.reset(new UM::STATE_MANAGER_CLASS());
	    g_device_controller.reset(new UM::INSTALLER_CONTROLLER_CLASS());
	    g_display_service.reset(new UM::DISPLAY_SERVICE_CLASS());
	    g_event_publish.reset(new UM::EVENT_PUBLISHER_CLASS());

	    /** 
	     *@brief Step 3: Create facades and set controllers to created facades 
	    */
	    g_processing_flow.reset(new UM::PROCESSING_FLOW_FACADE_CLASS());
	    g_processing_flow->set_device_controller(g_device_controller);
	    g_processing_flow->set_state_manager(g_state_manager);
	    g_processing_flow->set_display_service(g_display_service);
	    g_processing_flow->set_event_publisher(g_event_publish);
	    g_device_controller->add_observer(g_processing_flow.get());

	    
		UM::OTA_AGENT_CLASS::get_instance()->add_observer(g_processing_flow.get());
		UM::OTA_AGENT_CLASS::get_instance()->load_configuration();
		/** 
		 *@brief Step 4: Handle exceptions
		*/
		g_processing_flow->recover_if_abort_abnormally();
	}
	__UM_CATCH_ALL(error_code)

	/** 
     *@brief Step 5: Async agent working.
    */
    ZOO_COMMON::THREAD_POOL::get_instance()->queue_working(
	[]()
	{
		UM::OTA_AGENT_CLASS::get_instance()->run();
	});
	
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < function exit [%d]...",error_code);
}

/**
 *@brief This function response to release instance or memory 
**/ 
void UMMA_shutdown(void)
{
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > function entry ...");
    /** User add */
    g_processing_flow.reset();
    g_device_controller.reset();
    g_state_manager.reset();
    ZOO_COMMON::THREAD_POOL::get_instance()->shutdown();
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < function exit ...");
}

/**
 *@brief Subscribe events published from hardware drivers 
**/
void UMMA_subscribe_driver_event(void)
{
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > function entry ...");
    /** Subscribe events */

    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < function exit ...");
}

