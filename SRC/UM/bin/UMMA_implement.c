/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : UMMA_implement.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-16    Generator      created
*************************************************************/
#include <UMMA_implement.h>
#include "UM_FLOW_FACADE_WRAPPER.h"

/**
 *@brief UMMA_implement_4A_perform_upgrade
**/
void UMMA_implement_4A_perform_upgrade(IN UM4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = UM_perform_upgrade();
    /* User add ... END*/

    UMMA_raise_4A_perform_upgrade(rtn,reply_handle);
}

/**
 *@brief UMMA_implement_4A_undo_the_upgrade
**/
void UMMA_implement_4A_undo_the_upgrade(IN UM4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    /* User add some code here if had special need. */
    rtn = UM_undo_the_upgrade();
    /* User add ... END*/

    UMMA_raise_4A_undo_the_upgrade(rtn,reply_handle);
}

/**
 *@brief UMMA_implement_4A_get_status
 *@param status
**/
void UMMA_implement_4A_get_status(IN UM4I_REPLY_HANDLE reply_handle)
{
    ZOO_INT32 rtn = OK;
    UM4A_STATUS_STRUCT status ;
    memset(&status,0,sizeof(UM4A_STATUS_STRUCT));
    /* User add some code here if had special need. */
    rtn = UM_get_status(&status);
    /* User add ... END*/

    UMMA_raise_4A_get_status(rtn,status,reply_handle);
}
