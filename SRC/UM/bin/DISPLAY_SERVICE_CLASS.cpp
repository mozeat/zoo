/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : DISPLAY_SERVICE_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "DISPLAY_SERVICE_CLASS.h"
#include <boost/process.hpp>
#include <unistd.h>
namespace UM
{
	/*
	* @brief Constructor
	**/ 
	DISPLAY_SERVICE_CLASS::DISPLAY_SERVICE_CLASS():m_is_running(ZOO_FALSE)
	{
		this->m_event_publisher.reset(new EVENT_PUBLISHER_CLASS());
	}

	/*
	* @brief Destructor
	**/ 
	DISPLAY_SERVICE_CLASS::~DISPLAY_SERVICE_CLASS()
	{
		
	}

	/*
	* @brief startup hmi
	**/
	ZOO_INT32 DISPLAY_SERVICE_CLASS::startup()
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function entry ...");
		std::error_code ec;		
		std::string exe = "UMUI";
    	boost::process::child c(boost::process::search_path(exe),"&",ec);
    	this->m_is_running = ZOO_TRUE;
    	c.wait();	
    	this->m_is_running = ZOO_FALSE;
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function exit ...");
    	return OK;
	}

	/*
	* @brief shutdown hmi
	**/
	ZOO_INT32 DISPLAY_SERVICE_CLASS::shutdown()
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry ...");
		std::string cmd = "pidof ";
        pid_t pid = -1;
        cmd += "UMUI";
        FILE *fp = popen(cmd.c_str(), "r");
        if(fp)
        {
            if(EOF == fscanf(fp, "%d", &pid))
            {
            	ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR, __ZOO_FUNC__,":: get pid failed");
            	pclose(fp);
            	return UM4A_SYSTEM_ERR;
            }
            pclose(fp);
			kill(pid, SIGTERM);						
			std::this_thread::sleep_for(std::chrono::seconds(1));		
			kill(pid,SIGKILL);
        }
        
    	this->m_is_running = ZOO_FALSE;
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function exit ...");
    	return OK;	
	}

	/*
	* @brief running state
	**/
	ZOO_BOOL DISPLAY_SERVICE_CLASS::is_running()
	{
		return this->m_is_running;
	}

	/*
	* @brief Update status
	* @param status
	**/
	ZOO_INT32 DISPLAY_SERVICE_CLASS::update(OUT UM4A_STATUS_STRUCT * status,IN ZOO_INT32 error_code)
	{
		this->m_event_publisher->publish_UMMA_raise_4A_status_subscribe(*status,error_code,NULL);
		return OK;
	}

	/*
	* @brief Update inprogress status
	* @param status
	**/
	ZOO_INT32 DISPLAY_SERVICE_CLASS::update(OUT UM4A_INPROGRESS_STRUCT * inprogress,IN ZOO_INT32 error_code)
	{
		this->m_event_publisher->publish_UMMA_raise_4A_inprogress_subscribe(*inprogress,error_code,NULL);
		return OK;
	}								
}//namespace UM
