/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : PERSISTENCE_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "PERSISTENCE_CLASS.h"
#include "ZOO_enum_to_str.h"
namespace UM
{
    /*
      @brief Constructor
     **/ 
    PERSISTENCE_CLASS::PERSISTENCE_CLASS(std::string db_file):
    m_db("sqlite3",db_file)
    {       
        try 
        {
            this->m_db << "CREATE TABLE IF NOT EXISTS OTA ("
                "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                "STATE VARCHAR(50) NOT NULL,"
                "TIME INTEGER NOT NULL,"
                "VERSION VARCHAR(50),"
                "MESSAGE TEXT"
                ")";
            if(this->get_counts() == 0)
            {
                this->insert(ZOO_OTA_STATE_IDLE,"unknown","initialized");
            }
        }
        catch(soci::sqlite3_soci_error & e)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,__FUNCTION__,":: %s",e.what());
        }
        catch(...)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,__FUNCTION__,":: %s","unknown error");
        }
    }

    /*
     * @brief Destructor
     **/ 
    PERSISTENCE_CLASS::~PERSISTENCE_CLASS()
    {
        
    }
    
    /*
     * @brief Set ota state
     **/ 
    void PERSISTENCE_CLASS::update_ota_state(IN ZOO_OTA_STATE_ENUM ota_state)
    {
        std::string state =  ZOO_ota_state_enum_to_str(ota_state);
        ZOO_TIME_T now = ZOO_get_current_timestamp_msec();
        try 
        {
            this->m_db << "INSERT INTO OTA(STATE,TIME,VERSION,MESSAGE) VALUES(:state,:now,:version,:message);", 
            soci::use(state),soci::use(now),soci::use(this->m_version),soci::use(this->m_message);
        } 
        catch (const std::exception & e)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,__FUNCTION__,":: %s",e.what());
        }
    }
    /*
     * @brief Set ota state
    **/ 
    void PERSISTENCE_CLASS::insert(IN ZOO_OTA_STATE_ENUM ota_state,std::string version ,std::string message)
    {
        std::string state =  ZOO_ota_state_enum_to_str(ota_state);
        ZOO_TIME_T now = ZOO_get_current_timestamp_msec();
        try 
        {
            this->m_db << "INSERT INTO OTA(STATE,TIME,VERSION,MESSAGE) VALUES(:state,:now,:version,:message);", 
            soci::use(state),soci::use(now),soci::use(version),soci::use(message);
            this->m_message = message;
            this->m_version =version;
        } 
        catch (const std::exception & e)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,__FUNCTION__,":: %s",e.what());
        }
    }

    /*
     * @brief Get ota state
     **/ 
    ZOO_OTA_STATE_ENUM  PERSISTENCE_CLASS::get_ota_state()
    {
        ZOO_OTA_STATE_ENUM state = ZOO_OTA_STATE_MAX;
        std::string st;
        try 
        {
            this->m_db << "SELECT STATE FROM OTA ORDER BY rowid DESC LIMIT 1;", soci::into(st);
            state = ZOO_str_to_OTA_state(st.c_str());
        } 
        catch (const std::exception & e)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,__FUNCTION__,"::  %s",e.what());
        }
        return state;
    }

    std::string PERSISTENCE_CLASS::get_last_version()
    {
        try 
        {
            this->m_db << "SELECT VERSION FROM OTA ORDER BY rowid DESC LIMIT 1;", soci::into(this->m_version);
        } 
        catch (const std::exception & e)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,__FUNCTION__,"::  %s",e.what());
        }
        return this->m_version;
    }

    ZOO_INT32 PERSISTENCE_CLASS::get_counts()
    {
        ZOO_LONG counts = 0;
        try 
        {
            this->m_db << "SELECT COUNT(*) FROM OTA;", soci::into(counts);
        } 
        catch (const std::exception & e)
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING,__FUNCTION__,"::  %s",e.what());
        }
        return counts;
    }
}// namespace UM
