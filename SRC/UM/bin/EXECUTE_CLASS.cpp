/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : EXECUTE_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "EXECUTE_CLASS.h"
namespace UM
{
	/*
	* @brief Constructor
	**/ 
	EXECUTE_CLASS::EXECUTE_CLASS():m_timeout_minutes(5)
	{
		
	}

	/*
	* @brief Destructor
	**/ 
	EXECUTE_CLASS::~EXECUTE_CLASS()
	{
		
	}

	/*
	 * @brief Set exe name 
	 * @param name        the name
	**/ 
	void EXECUTE_CLASS::set_exe_name(std::string name)
	{
		this->m_exe_name = name;
	}

	/*
	 * @brief Get exe name 
	 * @return name        
	**/ 
	std::string EXECUTE_CLASS::get_exe_name()
	{
		return this->m_exe_name;
	}

	void EXECUTE_CLASS::add_param(std::string && param)
	{
		this->m_params.emplace_back(std::move(param));
	}
	
	/*
	 * @brief Get exe name 
	 * @param params        
	**/ 
	std::vector<std::string> EXECUTE_CLASS::get_params()
	{
		return this->m_params;
	}
	
	void EXECUTE_CLASS::set_param(std::string param)
	{
		this->m_params.emplace_back(std::move(param));
	}

	void EXECUTE_CLASS::set_timeout_value(ZOO_UINT32 minutes)
	{
		this->m_timeout_minutes = minutes;
	}
	
	ZOO_UINT32 EXECUTE_CLASS::get_timeout_value()
	{
		return this->m_timeout_minutes;
	}
}// namespace UM
