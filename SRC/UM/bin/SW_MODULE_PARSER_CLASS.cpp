/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : SW_MODULE_PARSER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "SW_MODULE_PARSER_CLASS.h"

namespace UM
{

	/*
	* @brief Constructor
	**/ 
	SW_MODULE_PARSER_CLASS::SW_MODULE_PARSER_CLASS()
	{
	}

	/*
	* @brief Destructor
	**/ 
	SW_MODULE_PARSER_CLASS::~SW_MODULE_PARSER_CLASS()
	{
	}


	/*
	* @brief Parse software modules
	* @param file   config.json file path
	**/ 
	std::vector<boost::shared_ptr<SW_MODULE_CLASS> > SW_MODULE_PARSER_CLASS::parse_modules(std::string file)
	{
		__ZOO_LOG(" > function entry...");
		std::vector<boost::shared_ptr<SW_MODULE_CLASS> > modules;
		auto download_path =  UM_CONFIGURE::get_instance()->get_download_path();
		auto cfg_file = download_path + "/" + file;
		__ZOO_LOG(" :: " << file << "[" << cfg_file << "]");
		if(!boost::filesystem::exists(cfg_file))
		{
			__THROW_UM_EXCEPTION(UM4A_PARAMETER_ERR,"config file is not exist",NULL);
		}

		try
		{
			boost::property_tree::ptree ota_tree;
			boost::property_tree::json_parser::read_json(cfg_file,ota_tree);
			BOOST_FOREACH(const boost::property_tree::ptree::value_type & p,ota_tree.get_child("SoftwareModule"))
			{
				boost::property_tree::ptree module_tree = p.second;
				ZOO_INT32 order = module_tree.get<int>("Order"); 
				__ZOO_LOG(":: order:" << order);
				std::string package_name = module_tree.get<std::string>("PackageName"); 
				__ZOO_LOG(":: package_name:" << package_name);

				std::string release_note;
				BOOST_FOREACH(const boost::property_tree::ptree::value_type & p,
								module_tree.get_child("ReleaseNote"))
				{	
				     release_note.append(p.second.get_value<std::string>()).append("\n");
				    __ZOO_LOG(":: release_note:" << release_note); 
				}

				std::string version = module_tree.get<std::string>("Version"); 
    			__ZOO_LOG(":: version:" << version);
				std::string type = module_tree.get<std::string>("Type"); 
				__ZOO_LOG(":: type:" << type);

				boost::shared_ptr<SW_MODULE_CLASS> module(new SW_MODULE_CLASS());
				module->set_order(order);
				module->set_package_name(package_name);
				module->set_version(version);
				module->set_type(type);
				module->set_release_note(release_note);
				auto timeout = module_tree.get<unsigned int>("Timeout");
				module->set_estimate_consume_minutes(timeout);
				BOOST_FOREACH(const boost::property_tree::ptree::value_type & p,
								module_tree.get_child("Command"))
				{
					boost::shared_ptr<EXECUTE_CLASS> executor(new EXECUTE_CLASS());
					std::vector<std::string> cmd_with_args; 
					auto cmd = p.second.get_value<std::string>();
					__ZOO_LOG(":: executor : " << cmd);
					boost::split(cmd_with_args,cmd,boost::is_any_of("\t "),boost::token_compress_on);
					if(!cmd_with_args.empty())
					{
						executor->set_exe_name(cmd_with_args.front());
						__ZOO_LOG(":: cmd : " << cmd_with_args.front());
					}

					if(cmd_with_args.size() >= 2)
					{
						std::for_each(cmd_with_args.begin() + 1,cmd_with_args.end(),
						[&](auto & arg)
						{
							__ZOO_LOG(":: param : " << arg);
							executor->add_param(std::forward<std::string>(arg));
						}); 	
					}
					executor->set_timeout_value(timeout);
					module->add_executor(executor);
				}
				modules.emplace_back(module);
			}
		}
		catch(boost::property_tree::ptree_error & e)
		{
			__THROW_UM_EXCEPTION(UM4A_PARAMETER_ERR,e.what(),NULL);
		}
		__ZOO_LOG(" < function exit...");
		return modules;
	}

    /*
     * @brief Parse major software version
     * @param file   config.json file path
     **/ 
	std::string SW_MODULE_PARSER_CLASS::parse_major_software_version(std::string file)
	{
	    __ZOO_LOG(" > function entry...");
		std::vector<boost::shared_ptr<SW_MODULE_CLASS> > modules;
		auto download_path =  UM_CONFIGURE::get_instance()->get_download_path();
		auto cfg_file = download_path + "/" + file;
		__ZOO_LOG(" :: " << file << "[" << cfg_file << "]");
		if(!boost::filesystem::exists(cfg_file))
		{
			__THROW_UM_EXCEPTION(UM4A_PARAMETER_ERR,":: config file is not exist",NULL);
		}
		
		boost::property_tree::ptree ota_tree;
		boost::property_tree::json_parser::read_json(cfg_file,ota_tree);
		boost::optional<std::string> major_software_version = ota_tree.get_optional<std::string>("Version");
		if(major_software_version)
		{
	        __ZOO_LOG(":: major_software_version: " << major_software_version.get());
	    }
		__ZOO_LOG(" < function exit...");
		return major_software_version.get();
	}
} //namespace UM
