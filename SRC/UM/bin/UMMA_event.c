/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : UMMA_event.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-16    Generator      created
*************************************************************/
#include <UMMA_event.h>

/**
 *@brief UMMA_raise_4A_perform_upgrade
**/
ZOO_INT32 UMMA_raise_4A_perform_upgrade(IN ZOO_INT32 error_code,
                                            IN UM4I_REPLY_HANDLE reply_handle)
{
    UM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = UM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = UM4I_get_reply_message_length(UM4A_PERFORM_UPGRADE_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (UM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = UM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = UM4A_PERFORM_UPGRADE_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = UM4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = UM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief UMMA_raise_4A_notify_recieved_ack
**/
ZOO_INT32 UMMA_raise_4A_undo_the_upgrade(IN ZOO_INT32 error_code,
                                                IN UM4I_REPLY_HANDLE reply_handle)
{
    UM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = UM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = UM4I_get_reply_message_length(UM4A_NOTIFY_RECIEVED_ACK_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (UM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = UM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = UM4A_NOTIFY_RECIEVED_ACK_CODE;
                reply_message->reply_header.execute_result = error_code;
                rtn = UM4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = UM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief UMMA_raise_4A_get_status
 *@param status
**/
ZOO_INT32 UMMA_raise_4A_get_status(IN ZOO_INT32 error_code,
                                       IN UM4A_STATUS_STRUCT status,
                                       IN UM4I_REPLY_HANDLE reply_handle)
{
    UM4I_REPLY_STRUCT* reply_message = NULL;
    ZOO_INT32 rtn = OK;
    ZOO_INT32 reply_length = 0;
    if(reply_handle == NULL)
    {
        rtn = UM4A_PARAMETER_ERR;
    }

    if(OK == rtn)
    {
        if(reply_handle->reply_wanted == ZOO_TRUE)
        {
            rtn = UM4I_get_reply_message_length(UM4A_GET_STATUS_CODE, &reply_length);
            if(OK == rtn)
            {
                reply_message = (UM4I_REPLY_STRUCT*)MM4A_malloc(reply_length);
                if(reply_message == NULL)
                {
                    rtn = UM4A_SYSTEM_ERR;
                }
            }

            if(OK == rtn)
            {
                reply_message->reply_header.function_code  = UM4A_GET_STATUS_CODE;
                reply_message->reply_header.execute_result = error_code;
                memcpy(&reply_message->reply_body.get_status_rep_msg.status,&status,sizeof(UM4A_STATUS_STRUCT));
                rtn = UM4I_send_reply_message(reply_handle->reply_addr,reply_handle->msg_id,reply_message);
                if(OK != rtn)
                {
                    rtn = UM4A_SYSTEM_ERR;
                }
            }
        }
    }

    if(reply_handle != NULL)
    {
        MM4A_free(reply_handle);
        reply_handle = NULL;
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
        reply_message = NULL;
    }
    return rtn;
}

/**
 *@brief UM4A_status_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
void UMMA_raise_4A_status_subscribe(IN UM4A_STATUS_STRUCT status,IN ZOO_INT32 error_code,IN void *context)
{
    ZOO_INT32 rtn = OK;
    UM4I_REPLY_STRUCT * reply_message = NULL;
    reply_message = (UM4I_REPLY_STRUCT * ) MM4A_malloc(sizeof(UM4I_REPLY_STRUCT));
    if(NULL == reply_message)
    {
        rtn = UM4A_PARAMETER_ERR;
    }
    
    if(OK == rtn)
    {
        reply_message->reply_header.function_code = UM4A_STATUS_SUBSCRIBE_CODE;
        reply_message->reply_header.execute_result = error_code;
        memcpy(&reply_message->reply_body.status_subscribe_code_rep_msg.status,&status,sizeof(UM4A_STATUS_STRUCT));
    }

    if(OK == rtn)
    {
        rtn = UM4I_publish_event(UM4A_SERVER,
                                            UM4A_STATUS_SUBSCRIBE_CODE,
                                            reply_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }

    return;
}
/**
 *@brief UM4A_inprogress_subscribe
 *@param status
 *@param error_code
 *@param *context
**/
void UMMA_raise_4A_inprogress_subscribe(IN UM4A_INPROGRESS_STRUCT status,IN ZOO_INT32 error_code,IN void *context)
{
    ZOO_INT32 rtn = OK;
    UM4I_REPLY_STRUCT * reply_message = NULL;
    reply_message = (UM4I_REPLY_STRUCT * ) MM4A_malloc(sizeof(UM4I_REPLY_STRUCT));
    if(NULL == reply_message)
    {
        rtn = UM4A_PARAMETER_ERR;
    }
    
    if(OK == rtn)
    {
        reply_message->reply_header.function_code = UM4A_INPROGRESS_SUBSCRIBE_CODE;
        reply_message->reply_header.execute_result = error_code;
        memcpy(&reply_message->reply_body.inprogress_subscribe_code_rep_msg.status,&status,sizeof(UM4A_INPROGRESS_STRUCT));
    }

    if(OK == rtn)
    {
        rtn = UM4I_publish_event(UM4A_SERVER,
                                            UM4A_INPROGRESS_SUBSCRIBE_CODE,
                                            reply_message);
    }


    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }

    return;
}

