/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : INSTALL_SEQUENCE_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "INSTALL_SEQUENCE_CLASS.h"
#include "SW_MODULE_PARSER_CLASS.h"
namespace UM
{
	/*
	 * @brief Constructor
	**/ 
	INSTALL_SEQUENCE_CLASS::INSTALL_SEQUENCE_CLASS()
	{
	
	}

	/*
	 * @brief Destructor
	**/ 
	INSTALL_SEQUENCE_CLASS::~INSTALL_SEQUENCE_CLASS()
	{
		
	}
	
	void INSTALL_SEQUENCE_CLASS::parse_modules(std::string file)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> ...");
		this->m_modules = SW_MODULE_PARSER_CLASS::parse_modules(file);
		this->m_current_module = this->m_modules.front();
		this->m_next_module =  this->m_current_module;
		this->m_major_software_version = SW_MODULE_PARSER_CLASS::parse_major_software_version(file);
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< ...");
	}

	/*
	 * @brief Get first SW module.
	**/ 
	boost::shared_ptr<SW_MODULE_CLASS> INSTALL_SEQUENCE_CLASS::get_first_module()
	{
		return this->m_modules.front();
	}

	/*
     * @brief Set next SW module.
    **/ 
	void INSTALL_SEQUENCE_CLASS::set_next_module(boost::shared_ptr<SW_MODULE_CLASS> module)
	{
		this->m_next_module = module;
	}

	/*
	 * @brief Get next SW module.
	**/ 
	boost::shared_ptr<SW_MODULE_CLASS> INSTALL_SEQUENCE_CLASS::get_next_module()
	{
		return this->m_next_module;
	}

	/*
     * @brief Set current SW module.
    **/
	void INSTALL_SEQUENCE_CLASS::set_current_module(boost::shared_ptr<SW_MODULE_CLASS> module)
	{
		this->m_current_module = module;
	}
	
	/*
	 * @brief Get current SW module.
	**/ 
	boost::shared_ptr<SW_MODULE_CLASS> INSTALL_SEQUENCE_CLASS::get_current_module()
	{
		return this->m_current_module;
	}

	/*
	 * @brief Get last SW module.
	**/ 
	boost::shared_ptr<SW_MODULE_CLASS> INSTALL_SEQUENCE_CLASS::get_last_module()
	{
		return this->m_modules.back();
	}

	/*
	 * @brief Get last SW module.
	**/ 
	void INSTALL_SEQUENCE_CLASS::move_to_next()
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> ...");
		/*if(!this->m_modules.empty())
		{
			this->m_modules.pop_front();
			this->m_current_module =  this->m_next_module;
			this->m_next_module = this->m_modules.front();
		}
		else
		{
			this->m_next_module.reset();
			this->m_current_module.reset();
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: this is the end SW module.");
		}*/
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< ...");
	}

	/*
	 * @brief Get next SW module.
	**/ 
	int INSTALL_SEQUENCE_CLASS::get_module_number()
	{
		return this->m_modules.size();
	}

	/*
	 * @brief Clear SW module.
	**/ 
	void INSTALL_SEQUENCE_CLASS::clear()
	{
		this->m_modules.clear();
		this->m_current_module.reset();
	}

    /*
	 * @brief Set SW modules.
	 * @param modules.
	 **/
	void INSTALL_SEQUENCE_CLASS::set_modules(std::vector<boost::shared_ptr<SW_MODULE_CLASS>> modules)
	{
		this->m_modules = modules; 
	}
	
	/*
	 * @brief Get SW modules.
	 **/     
	std::vector<boost::shared_ptr<SW_MODULE_CLASS>> INSTALL_SEQUENCE_CLASS::get_mudules()
	{
		return this->m_modules;
	}

	/*
	 * @brief Get SW modules.
	 **/ 
    const std::string & INSTALL_SEQUENCE_CLASS::get_major_version()
    {
        return this->m_major_software_version;
    }
}
