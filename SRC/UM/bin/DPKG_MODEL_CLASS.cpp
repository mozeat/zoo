/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : DPKG_MODEL_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "DPKG_MODEL_CLASS.h"
namespace UM
{
	/*
	* @brief Constructor
	**/ 
	DPKG_MODEL_CLASS::DPKG_MODEL_CLASS()
	{
	}

	/*
	* @brief Destructor
	**/ 
	DPKG_MODEL_CLASS::~DPKG_MODEL_CLASS()
	{

	}

}// namespace UM
