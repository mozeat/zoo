/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : EVENT_PUBLISHER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-15    Generator      created
*************************************************************/
#include "EVENT_PUBLISHER_CLASS.h"
#include "UMMA_event.h"

namespace UM
{
    /*
     * @brief Constructor
    **/ 
    EVENT_PUBLISHER_CLASS::EVENT_PUBLISHER_CLASS()
    {

    }

    /*
     * @brief Constructor
    **/ 
    EVENT_PUBLISHER_CLASS::~EVENT_PUBLISHER_CLASS()
    {

    }

    /*
    * @brief Publish status changed event to subscriber.
    * @param status
    * @param error_code
    * @param *context
    **/ 
    void EVENT_PUBLISHER_CLASS::publish_UMMA_raise_4A_status_subscribe(UM4A_STATUS_STRUCT status,ZOO_INT32 error_code,void *context)
    {
        UMMA_raise_4A_status_subscribe(status,error_code,context);
    }

    /*
    * @brief Publish inprogress changed event to subscriber.
    * @param status
    * @param error_code
    * @param *context
    **/ 
    void EVENT_PUBLISHER_CLASS::publish_UMMA_raise_4A_inprogress_subscribe(UM4A_INPROGRESS_STRUCT status,ZOO_INT32 error_code,void *context)
    {
        UMMA_raise_4A_inprogress_subscribe(status,error_code,context);
    }

} //namespace UM

