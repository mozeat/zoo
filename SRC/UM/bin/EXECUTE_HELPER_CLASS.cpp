/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : EXECUTE_HELPER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "EXECUTE_HELPER_CLASS.h"
namespace UM
{
	/*
	* @brief Constructor
	**/ 
	EXECUTE_HELPER_CLASS::EXECUTE_HELPER_CLASS()
	{
	
	}

	/*
	* @brief Destructor
	**/ 
	EXECUTE_HELPER_CLASS::~EXECUTE_HELPER_CLASS()
	{
	
	}

	/*
	* @brief invoke method.
	**/ 
	void EXECUTE_HELPER_CLASS::invoke(boost::shared_ptr<EXECUTE_CLASS> invoke_model)
	{
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry...");
		if(nullptr == invoke_model)
		{
			__THROW_UM_EXCEPTION(UM4A_PARAMETER_ERR, "invoke_model pointer is null", NULL);
		} 

		auto exe = invoke_model->get_exe_name() + " ";
		auto args = invoke_model->get_params();
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,":: exe:%s ...",exe.data());
		for(auto & arg : args)
		{  
		    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,":: arg:%s ...",arg.data());
		    exe += arg + " ";
		};
		
		std::error_code ec;
		//boost::process::ipstream ips;boost::process::search_path(
		boost::process::child c(exe,ec);
		//std::string line;
	    /*while (c.running() && std::getline(ips, line) && !line.empty())  
	    {
	    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__," :: %s",line.c_str());
	    }*/

	    auto minutes = invoke_model->get_timeout_value();
	    if(!c.wait_for(std::chrono::minutes(minutes)))
	    {
	        c.terminate();
		    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,":: %s ...error",exe.data());
	    	char message[128] = {0};
	    	sprintf(message,":: cmd execute timeout(%u minutes)",minutes);
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR,message, NULL);
	    }
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,":: %s  ...success",exe.data());

	    if(ec)
	    {
	        char message[128] = {0};
	    	sprintf(message,":: %s",ec.message().data());
			ZOO_slog(ZOO_SEVERITY_LEVEL_WARNING, __ZOO_FUNC__,":: %s",message);
	    }
        
	    if(c.exit_code() != OK)
	    {
	    	char message[128] = {0};
	    	sprintf(message,":: install failed[error_code = %d]",c.exit_code());
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR,message,NULL);
	    }
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function exit...");
	}
}// namespace UM
