/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : CURL_WRAPPER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "CURL_WRAPPER_CLASS.h"
#include <map>
#include <openssl/md5.h>
namespace UM
{

	/*
	 * @brief Constructor
	**/ 
	static size_t ota_reply_callback(void *ptr, size_t size, size_t nmemb, std::string* data) 
	{
    	data->append((char*) ptr, size * nmemb);
    	return size * nmemb;
	}
	
	/*
	 * @brief Constructor
	**/ 
	CURL_WRAPPER_CLASS::CURL_WRAPPER_CLASS()
	{
		this->m_curl = curl_easy_init();
		if(this->m_curl == NULL)
		{
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "curl_easy_init fail", NULL);
		}
		
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_TCP_KEEPALIVE, 1L))
	    {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_TCP_KEEPALIVE fail", NULL);
	    }
	    
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_HEADER, 0L))
	    {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_HEADER fail", NULL);
	    }
	    
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_NOSIGNAL, 1L))
	    {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_NOSIGNAL fail", NULL);
	    }
	    
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_VERBOSE, 0L))
	    {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_VERBOSE fail", NULL);
	    }
	    
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_TCP_KEEPALIVE, 1L))
	    {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_TCP_KEEPALIVE fail", NULL);
	    }
	    
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_NOPROGRESS, 1L))
	    {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_NOPROGRESS fail", NULL);
	    }
	    
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_USERAGENT, "curl/7.42.0"))
	    {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_USERAGENT fail", NULL);
	    }
	    
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_MAXREDIRS, 50L))
	    {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_MAXREDIRS fail", NULL);
	    }
	    
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_CONNECTTIMEOUT, 10L))
	    {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_CONNECTTIMEOUT fail", NULL);
	    }
	}

	/*
	 * @brief Destructor
	**/ 
	CURL_WRAPPER_CLASS::~CURL_WRAPPER_CLASS()
	{
		if(this->m_curl)
		{
			curl_easy_cleanup(this->m_curl);
		}
	}

	/*
	 * @brief Http get url from hawkbit
	 * @param url             request
	 * @param headers         the http header
	 * @param timeout         seconds
	**/ 
	std::string CURL_WRAPPER_CLASS::get(IN std::string & url,
											IN std::vector<std::string> & headers,
											IN ZOO_INT32 timeout)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > url: %s",url.c_str());
		std::string response;
		
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_URL, url.c_str()))
	    {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_URL fail", NULL);
	    }
	    
        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_TIMEOUT, timeout))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_TIMEOUT fail", NULL);
        }
        
        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_WRITEFUNCTION, ota_reply_callback))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_WRITEFUNCTION fail", NULL);
        }
        
        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_WRITEDATA, &response))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_WRITEDATA fail", NULL);
        }

        curl_slist *list = NULL;
        for(auto & v : headers)
        {
		    list = curl_slist_append(list,v.c_str());
	    }
	    
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_HTTPHEADER, list))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_HTTPHEADER fail", NULL);
        }

        auto  error_code = curl_easy_perform(this->m_curl);
        curl_slist_free_all(list);
        if(CURLE_OK != error_code)
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, curl_easy_strerror(error_code), NULL);
        }

        long response_code = OK;
        double elapsed = 0.;
        if(CURLE_OK != curl_easy_getinfo(this->m_curl, CURLINFO_RESPONSE_CODE, &response_code))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLINFO_RESPONSE_CODE fail", NULL);       	
        }
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: error_code: %d",error_code);
        
        if(CURLE_OK != curl_easy_getinfo(this->m_curl, CURLINFO_TOTAL_TIME, &elapsed))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLINFO_TOTAL_TIME fail", NULL);
        }

        if(response_code != 200)
        {
        	//__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "reply error", NULL);
        }
        
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: response: %s",response.c_str());
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: execute time: %g",elapsed);
        //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < exit ..");
	    return response;
	}			
	
		
	/*
	 * @brief Stop routines
	**/ 
	std::string CURL_WRAPPER_CLASS::post(IN std::string & url,
											IN std::vector<std::string> & headers,
											IN std::string & body,
											IN ZOO_INT32 timeout)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > url: %s",url.c_str());
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > body: %s",body.c_str());
		std::string response;
		
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_URL, url.c_str()))
	    {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_CONNECTTIMEOUT fail", NULL);
	    }
	    
        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_TIMEOUT, timeout))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_TIMEOUT fail", NULL);
        }
        
        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_WRITEFUNCTION, ota_reply_callback))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_WRITEFUNCTION fail", NULL);
        }
        
        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_WRITEDATA, &response))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_WRITEDATA fail", NULL);
        }

        curl_slist *list = NULL;
        for(auto & v : headers)
        {
		    list = curl_slist_append(list,v.c_str());
	    }
	    
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_HTTPHEADER, list))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_HTTPHEADER fail", NULL);
        }

        if(CURLE_OK !=  curl_easy_setopt(this->m_curl, CURLOPT_POST, 1L))
        {
        	__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_HTTPHEADER fail", NULL);
        }

        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_POSTFIELDS, body.c_str()))
        {
        	__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_HTTPHEADER fail", NULL);
        }
        
        long response_code = 200;
        double elapsed = 0.;

        auto  error_code = curl_easy_perform(this->m_curl);
        curl_slist_free_all(list);

        if(CURLE_OK != curl_easy_getinfo(this->m_curl, CURLINFO_RESPONSE_CODE, &response_code))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLINFO_RESPONSE_CODE fail", NULL);       	
        }

        
        if(CURLE_OK != curl_easy_getinfo(this->m_curl, CURLINFO_TOTAL_TIME, &elapsed))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLINFO_TOTAL_TIME fail", NULL);
        }

        //if(CURLE_OK !=  curl_easy_setopt (this->m_curl, CURLOPT_POST, 0L))
        //{
        //	__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_HTTPHEADER fail", NULL);
        //}
        
        if(CURLE_OK != error_code)
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, curl_easy_strerror(error_code), NULL);
        }

        if(response_code != 200)
        {
            ZOO_CHAR message[128];
            sprintf(message,"http response code:%ld,not eq to 200 ...ERROR",response_code);
        	__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, message, NULL);
        }
        
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: response: %s",response.c_str());
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: execute time: %g",elapsed);
        //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < ");
	    return response;
	}

	/*
     * @brief http Put method
    **/ 
	void CURL_WRAPPER_CLASS::put(IN std::string  url,
									IN std::vector<std::string> & headers,
									IN std::string & body,
									IN ZOO_INT32 timeout)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > url: %s",url.c_str());
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > body: %s",body.c_str());

	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_URL, url.c_str()))
	    {
	    	__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_CONNECTTIMEOUT fail", NULL);
	    }
	    
		if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_TIMEOUT, timeout))
		{
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_TIMEOUT fail", NULL);
		}
		
        curl_slist *list = NULL;
        for(auto & v : headers)
        {
		    list = curl_slist_append(list,v.c_str());
	    }
	    
	    list = curl_slist_append(list, "Expect:");
	    
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_HTTPHEADER, list))
	    {
	    	__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_HTTPHEADER fail", NULL);
	    }
	    
		if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_POSTFIELDS, body.c_str()))
		{
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_POSTFIELDS fail", NULL);
		}
		
        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_CUSTOMREQUEST, "PUT"))
        {
        	__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_CUSTOMREQUEST fail", NULL);
        }
        
        auto error_code = curl_easy_perform(this->m_curl);
        curl_slist_free_all(list);
        long response_code = OK;
        double elapsed = 0.;
        if(CURLE_OK != curl_easy_getinfo(this->m_curl, CURLINFO_RESPONSE_CODE, &response_code))
        {
        	__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLINFO_RESPONSE_CODE fail", NULL);
        }
        
        if(CURLE_OK != curl_easy_getinfo(this->m_curl, CURLINFO_TOTAL_TIME, &elapsed))
        {
        	__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLINFO_TOTAL_TIME fail", NULL);
        }
        
        if(CURLE_OK != error_code)
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, curl_easy_strerror(error_code), NULL);
        }

        if(response_code != 200)
        {
        	//__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "http request fail", NULL);
        }
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: execute time: %g",elapsed);
        //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < ");
	}

	/*
	 * @brief Download file from ota server.
	 * @param url  
	 * @param output_file  
	**/ 
	void CURL_WRAPPER_CLASS::donwload(IN std::string url,
											IN std::vector<std::string> & headers,
											IN std::string output_file,
											IN ZOO_INT32 timeout)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > url: %s",url.c_str());
		std::string response;
		FILE *fp = fopen(output_file.c_str(),"wb");
		if(fp == NULL)
		{
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "fopen file fail", NULL);
		}

		if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_FOLLOWLOCATION, 1))
	    {
	    	__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_FOLLOWLOCATION fail", NULL);
	    }
		
		if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_URL, url.c_str()))
	    {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "setopt CURLOPT_URL fail", NULL);
	    }
	    
        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_TIMEOUT, timeout))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_TIMEOUT fail", NULL);
        }
        
        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_WRITEFUNCTION, NULL))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_WRITEFUNCTION fail", NULL);
        }
        
        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_WRITEDATA, fp))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_WRITEDATA fail", NULL);
        }

        curl_slist *list = NULL;
        for(auto & v : headers)
        {
		    list = curl_slist_append(list,v.c_str());
	    }
	    
	    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_HTTPHEADER, list))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_HTTPHEADER fail", NULL);
        }

		auto  error_code = curl_easy_perform(this->m_curl);
		curl_slist_free_all(list);
        if(CURLE_OK != error_code)
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, curl_easy_strerror(error_code), NULL);
        }
        
        if(fp)
        {
        	fclose(fp);
        }
		
        long response_code = OK;
        double elapsed = 0.;
        if(CURLE_OK != curl_easy_getinfo(this->m_curl, CURLINFO_RESPONSE_CODE, &response_code))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLINFO_RESPONSE_CODE fail", NULL);       	
        }
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: error_code: %d",error_code);
        
        if(CURLE_OK != curl_easy_getinfo(this->m_curl, CURLINFO_TOTAL_TIME, &elapsed))
        {
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLINFO_TOTAL_TIME fail", NULL);
        }

        if(response_code != 200)
        {
        	__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "reply error", NULL);
        }
        
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: response: %s",response.c_str());
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: execute time: %g",elapsed);
        //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < exit ..");
	}

	/*
     * @brief Download file from ota server.
     * @param url  
     * @param output_file  
     * @param output_file   
     * @param md5    
     * @param timeout  
    **/ 
	void CURL_WRAPPER_CLASS::donwload(IN std::string url,
						IN std::vector<std::string> & headers,
							 IN std::string output_file,
							 IN std::string md5,
							 IN ZOO_INT32 timeout)
	{
        //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," > %s","...");
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: url: %s",url.c_str());
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: output_file: %s",output_file.c_str());
        std::string response;
		FILE *fp = fopen(output_file.c_str(),"wb");
		__UM_TRY
		{
			if(fp == NULL)
			{
				__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "fopen file fail", NULL);
			}

			if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_FOLLOWLOCATION, 1))
		    {
		    	__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_FOLLOWLOCATION fail", NULL);
		    }
	    
			if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_URL, url.c_str()))
		    {
				__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_URL fail", NULL);
		    }
		    
	        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_TIMEOUT, timeout))
	        {
				__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_TIMEOUT fail", NULL);
	        }
	        
	        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_WRITEFUNCTION, NULL))
	        {
				__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_WRITEFUNCTION fail", NULL);
	        }
	        
	        if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_WRITEDATA, fp))
	        {
				__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_WRITEDATA fail", NULL);
	        }

	        curl_slist *list = NULL;
	        for(auto & v : headers)
	        {
			    list = curl_slist_append(list,v.c_str());
		    }
		    
		    if(CURLE_OK != curl_easy_setopt(this->m_curl, CURLOPT_HTTPHEADER, list))
	        {
				__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLOPT_HTTPHEADER fail", NULL);
	        }

			auto  error_code = curl_easy_perform(this->m_curl);
			curl_slist_free_all(list);
	        if(CURLE_OK != error_code)
	        {
				__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, curl_easy_strerror(error_code), NULL);
	        }
	        
	        long response_code = OK;
	        double elapsed = 0.;
	        if(CURLE_OK != curl_easy_getinfo(this->m_curl, CURLINFO_RESPONSE_CODE, &response_code))
	        {
				__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLINFO_RESPONSE_CODE fail", NULL);       	
	        }
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: error_code: %d",error_code);
	        
	        if(CURLE_OK != curl_easy_getinfo(this->m_curl, CURLINFO_TOTAL_TIME, &elapsed))
	        {
				__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "CURLINFO_TOTAL_TIME fail", NULL);
	        }

	        if(response_code != 200)
	        {
	        	__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR, "reply error", NULL);
	        }
	        
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: response: %s",response.c_str());
	        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," :: execute time: %g",elapsed);

			if(fp)
	        {
	        	fclose(fp);
	        }
        }
        catch(ZOO_COMMON::PARAMETER_EXCEPTION_CLASS & e)
        {
        	if(fp)
	        {
	        	fclose(fp);
	        }
	        __THROW_UM_EXCEPTION(e.get_error_code(), e.get_error_message(), NULL);
        }
		
		auto md5_from_file = [](const std::string& path)
		{
		    unsigned char result[MD5_DIGEST_LENGTH];
		    boost::iostreams::mapped_file_source src(path);
		    MD5((unsigned char*)src.data(), src.size(), result);
		    std::ostringstream os;
		    os<< std::hex << std::setfill('0');
		    for(auto c: result) os<<std::setw(2)<<(int)c;
		    return os.str();
		};
		
		std::string download_file_md5 = md5_from_file(output_file);
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
        						" :: download file md5: %s,expected md5: %s",
        						download_file_md5.c_str(),md5.c_str());
		if(md5 != download_file_md5)
		{
			__THROW_UM_EXCEPTION(UM4A_SYSTEM_ERR,"file md5 failed.", NULL);
		}

        //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__," < %s","...");
	}
}
