/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : UDS_MODEL_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "UDS_MODEL_CLASS.h"
namespace UM
{
	/*
	* @brief Constructor
	**/ 
	UDS_MODEL_CLASS::UDS_MODEL_CLASS()
	{
	}

	/*
	* @brief Destructor
	**/ 
	UDS_MODEL_CLASS::~UDS_MODEL_CLASS()
	{

	}

}// namespace UM

