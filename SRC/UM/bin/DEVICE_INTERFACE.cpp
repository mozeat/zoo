/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : DEVICE_INTERFACE.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "DEVICE_INTERFACE.h"
#include "EXECUTE_HELPER_CLASS.h"
namespace UM
{
    /*
     * @brief Constructor
    **/ 
    DEVICE_INTERFACE::DEVICE_INTERFACE():m_state(DEVICE_INSTALL_IDLE)
    {
    }

    /*
     * @brief Destructor
    **/ 
    DEVICE_INTERFACE::~DEVICE_INTERFACE()
    {
    }
    
	/*
	* @brief upgrade the model.
    **/ 
    void DEVICE_INTERFACE::install(boost::shared_ptr<SW_MODULE_CLASS> module)
    {
    	//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry...");
    	if(module == NULL)
    	{
    		__THROW_UM_EXCEPTION(UM4A_PARAMETER_ERR, "module pointer is NULL.", NULL);
    	}
		//this->notify_of_property_changed(DEVICE_INSTALL_ONGOING);
		boost::shared_ptr<EXECUTE_HELPER_CLASS> execute_helper(new EXECUTE_HELPER_CLASS());
		auto executors = module->get_executors();
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,":: executors numbers %d",executors.size());
		std::for_each(executors.begin(),executors.end(),[&](auto & executor)
		{
			__UM_TRY
			{
				execute_helper->invoke(executor);
				auto inprogress_value = module->get_inprogress_value() + 70 / executors.size();
				if(inprogress_value < 100)
				{
					module->set_inprogress_value(inprogress_value);
				}
			}
			__UM_CATCH_
			{
				//this->notify_of_property_changed(DEVICE_INSTALL_FAIL);
				__THROW_UM_EXCEPTION(e.get_error_code(), e.get_error_message(),NULL);
			}
		});
		module->set_inprogress_value(100);
		//this->notify_of_property_changed(DEVICE_INSTALL_SUCCESS);
       //ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< fucntion exit ...");
    }

    /*
	 * @brief Cancel the sw model.
    **/ 
    void DEVICE_INTERFACE::cancel()
    {
    	this->update_state(DEVICE_INSTALL_IDLE);
    }

    /*
     * @brief Add observer will be notified when property changed.
     * @param observer  Property changed observer
    **/ 
    void DEVICE_INTERFACE::add_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer)
    {
        this->remove_observer(observer);
        this->m_observers.push_back(observer);
    }

    /*
     * @brief Remove observer will be notified when property changed.
     * @param observer  Property changed observer
    **/ 
    void DEVICE_INTERFACE::remove_observer(PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer)
    {
        std::vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>*>::iterator observer_itr =
            this->m_observers.begin();
        while (observer_itr != this->m_observers.end())
        {
            PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* find_observer = *observer_itr;
            if (observer == find_observer)
            {
                this->m_observers.erase(observer_itr);
                break;
            }
        }
    }

    /*
     * @brief Clean.
    **/
    void DEVICE_INTERFACE::clean()
    {
        this->m_observers.clear();
		this->update_state(DEVICE_INSTALL_IDLE);
    }

    /*
     * @brief Notify property has been changed.
     * @param property_name     The property has been changed
    **/ 
    void DEVICE_INTERFACE::notify_of_property_changed(const ZOO_UINT32 property_name)
    {
        for (vector<PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>*>::iterator observer_itr =
            this->m_observers.begin(); observer_itr != this->m_observers.end(); observer_itr++)
        {
            PROPERTY_CHANGED_OBSERVER_INTERFACE<MARKING_MODEL_INTERFACE>* observer = *observer_itr;
            if (observer != NULL)
            {
                observer->on_property_changed(this,property_name);
            }
        }
    }

    void DEVICE_INTERFACE::update_state(int state)
    {
        if(state != this->m_state)
        {
        	this->m_state = state;
        }
        this->notify_of_property_changed(state);
    }
    
	/*
	* @brief update install state.
    **/ 
    void DEVICE_INTERFACE::reset_model()
    {
        this->m_observers.clear();
		this->update_state(DEVICE_INSTALL_IDLE);
    }

} // namespace UM

