/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : SW_MODULE_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "SW_MODULE_CLASS.h"
namespace UM
{
	SW_MODULE_CLASS::SW_MODULE_CLASS()
	:m_order(0)
	,m_inprogress_value(0)
	{
	}

	SW_MODULE_CLASS::~SW_MODULE_CLASS()
	{
	}

	/*
	* @brief Set the swm order.
	* @param order.
	**/ 
	void SW_MODULE_CLASS::set_order(ZOO_INT32 order)
	{
		this->m_order = order;
	}
	
	ZOO_INT32 SW_MODULE_CLASS::get_order()
	{
		return this->m_order;
	}

	/*
	* @brief Set package_name.
	* @param package_name.
	**/ 
	void SW_MODULE_CLASS::set_package_name(std::string package_name)
	{
		this->m_package_name = package_name;
	}
	
	std::string SW_MODULE_CLASS::get_package_name()
	{
		return this->m_package_name;
	}

	/*
	* @brief Set release note.
	* @param release_note.
	**/ 
	void SW_MODULE_CLASS::set_release_note(std::string & release_note)
	{
		this->m_release_note = release_note;
	}
	
	const std::string & SW_MODULE_CLASS::get_release_note()
	{
		return this->m_release_note;
	}

	/*
	* @brief Set type.
	* @param type.
	**/ 
	void SW_MODULE_CLASS::set_type(std::string type)
	{
		this->m_type = type;
	}    
	
	std::string SW_MODULE_CLASS::get_type()
	{
		return this->m_type;
	}  

	/*
	* @brief Set version.
	* @param version.
	**/ 
	void SW_MODULE_CLASS::set_version(std::string     version)
	{
		this->m_version = version;
	}
	
	std::string & SW_MODULE_CLASS::get_version()
	{
		return this->m_version;
	}  
	
	/*
	 * @brief Set inprogress value.
	 * @param inprogress_value.
	**/ 
	void SW_MODULE_CLASS::set_inprogress_value(ZOO_INT32		  inprogress_value)
	{
		this->m_inprogress_value = inprogress_value;
	}
	
	ZOO_INT32 & SW_MODULE_CLASS::get_inprogress_value()
	{
		return this->m_inprogress_value;
	}  

	/*
	* @brief Set package md5.
	**/ 
	std::string SW_MODULE_CLASS::get_package_md5()
	{
		return this->m_package_md5;
	}
	
	/*
    * @brief Get package md5.
    **/ 
    void SW_MODULE_CLASS::set_package_md5(std::string md5)
	{
		this->m_package_md5 = md5;
	}

	/*
	* @brief Set package sha1.
	**/ 
	void SW_MODULE_CLASS::set_package_sha1(std::string       sha1)
	{
		this->m_package_sha1 = sha1;
	}
	
	std::string SW_MODULE_CLASS::get_package_sha1()
	{
		return this->m_package_sha1;
	}
   
	/*
	* @brief Set package md5.
	**/ 
	std::string SW_MODULE_CLASS::get_download_url()
	{
		return this->m_download_url;
	}

	void SW_MODULE_CLASS::set_estimate_consume_minutes(ZOO_INT32 minutes)
	{
		this->m_estimate_consume_minutes = minutes;
	}
	
    ZOO_INT32 SW_MODULE_CLASS::get_estimate_consume_minutes()
	{
		return this->m_estimate_consume_minutes;
	}
        
	/*
    * @brief Get package md5.
    **/ 
    void SW_MODULE_CLASS::set_download_url(std::string url)
	{
		this->m_download_url = url;
	}
    boost::shared_ptr<DEVICE_INTERFACE> SW_MODULE_CLASS::get_device()
	{
		return this->m_device;
	}
    
    /*
     * @brief Set install device.
     * @param device.
    **/ 
    void SW_MODULE_CLASS::set_device(boost::shared_ptr<DEVICE_INTERFACE> device)
	{
		this->m_device = device;
	}
        

	/*
	* @brief clean 
	**/ 
	void SW_MODULE_CLASS::clean()
	{
		this->m_executors.clear();
		this->m_download_url = "";
		this->m_package_sha1 = "";
		this->m_package_md5 = "";
		this->m_type = "";
		this->m_version = "";
		this->m_release_note = "";
		this->m_package_name = "";
		this->m_order = 0;
		this->m_inprogress_value = 0;
		this->m_device.reset();
	}

	/*
	* @brief add an executor for execute sequence.
	* @param executor
	**/ 
	void SW_MODULE_CLASS::add_executor(boost::shared_ptr<EXECUTE_CLASS> executor)
	{
		this->m_executors.emplace_back(executor);
	}

	/*
     * @brief Set executors for execute sequence.
     * @param executor
    **/ 
    void SW_MODULE_CLASS::set_executors(std::vector<boost::shared_ptr<EXECUTE_CLASS> > & executors)
    {
        this->m_executors.clear();
        this->m_executors = executors;
    }

	/*
	* @brief Get all executors.
	* @return executors
	**/ 
	std::vector<boost::shared_ptr<EXECUTE_CLASS> > SW_MODULE_CLASS::get_executors()
	{
		return this->m_executors;
	}
}
