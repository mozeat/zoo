/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : UM_FLOW_FACADE_WRAPPER.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "UM_FLOW_FACADE_WRAPPER.h"
#include "PROCESSING_FLOW_FACADE_CLASS.h"


/*
 * @brief extern g_processing_flow defined from executor_wrapper.cpp
**/
extern UM::PROCESSING_FLOW_FACADE_CLASS * g_processing_flow;

/*
 * @brief UM4A_perform_upgrade
**/
ZOO_INT32 UM_perform_upgrade()
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->perform_upgrade();
    }
    return UM4A_SYSTEM_ERR;
}

/*
 * @brief UM4A_undo_the_upgrade
**/
ZOO_INT32 UM_undo_the_upgrade()
{
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->undo_the_upgrade();
    }
    return UM4A_SYSTEM_ERR;
}

/*
 * @brief UM4A_get_status
**/
ZOO_INT32 UM_get_status(OUT UM4A_STATUS_STRUCT * status)
{
	//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"pointer is %d",g_processing_flow);
    if(g_processing_flow != nullptr)
    {
        return g_processing_flow->get_status(status);
    }
    return UM4A_SYSTEM_ERR;
}

