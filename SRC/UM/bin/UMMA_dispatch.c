/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : UMMA_dispatch.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-16    Generator      created
*************************************************************/

#include <UMMA_dispatch.h>
#include <UMMA_implement.h>

/**
 *@brief UMMA_local_4A_perform_upgrade
**/
static ZOO_INT32 UMMA_local_4A_perform_upgrade(const MQ4A_SERV_ADDR server,UM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    UM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = UM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (UM4I_REPLY_HANDLE)MM4A_malloc(sizeof(UM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = UM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        UMMA_implement_4A_perform_upgrade(reply_handle);
    }
    return rtn;
}

/**
 *@brief UMMA_local_4A_undo_the_upgrade
**/
static ZOO_INT32 UMMA_local_4A_undo_the_upgrade(const MQ4A_SERV_ADDR server,UM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    UM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = UM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (UM4I_REPLY_HANDLE)MM4A_malloc(sizeof(UM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = UM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        UMMA_implement_4A_undo_the_upgrade(reply_handle);
    }
    return rtn;
}

/**
 *@brief UMMA_local_4A_get_status
 *@param status
**/
static ZOO_INT32 UMMA_local_4A_get_status(const MQ4A_SERV_ADDR server,UM4I_REQUEST_STRUCT * request,ZOO_UINT32 msg_id)
{ 
    UM4I_REPLY_HANDLE reply_handle = NULL;
    ZOO_INT32 rtn = OK;
    if(request == NULL)
    {
        rtn = UM4A_SYSTEM_ERR;
        return rtn;
    }

    if(rtn == OK)
    {
        reply_handle = (UM4I_REPLY_HANDLE)MM4A_malloc(sizeof(UM4I_REPLY_HANDLER_STRUCT));
        if(reply_handle == NULL)
        {
            rtn = UM4A_SYSTEM_ERR;
            return rtn;
        }
    }

    if(OK == rtn)
    {
        memcpy(reply_handle->reply_addr,server,MQ4A_SERVER_LENGHT * sizeof(char));
        reply_handle->msg_id = msg_id;
        reply_handle->reply_wanted = request->request_header.need_reply;
        reply_handle->func_id = request->request_header.function_code;
        UMMA_implement_4A_get_status(reply_handle);
    }
    return rtn;
}

/**
*@brief Dispatch message from client to server internal interface
*@param context        
*@param server        address
*@param msg           request message to server
*@param len           request message length
*@param reply_msg     reply message length to caller
*@param reply_msg_len reply message length
**/
void UMMA_callback_handler(void * context,const MQ4A_SERV_ADDR server,void * msg,ZOO_UINT32 msg_id)
{
    ZOO_INT32 rtn = OK;
    ZOO_INT32 rep_length = 0;
    UM4I_REQUEST_STRUCT *request = (UM4I_REQUEST_STRUCT*)msg;
    UM4I_REPLY_STRUCT *reply = NULL;
    if(request == NULL)
    {
        rtn = UM4A_SYSTEM_ERR;
        return;
    }

    if(OK == rtn)
    {
        switch(request->request_header.function_code)
        {
            case UM4A_PERFORM_UPGRADE_CODE:
                UMMA_local_4A_perform_upgrade(server,request,msg_id);
                break;
            case UM4A_NOTIFY_RECIEVED_ACK_CODE:
                UMMA_local_4A_undo_the_upgrade(server,request,msg_id);
                break;
            case UM4A_GET_STATUS_CODE:
                UMMA_local_4A_get_status(server,request,msg_id);
                break;
            default:
                rtn = UM4A_SYSTEM_ERR;
                break;
        }
    }

    if(OK != rtn && request->request_header.need_reply)
    {
        rtn = UM4I_get_reply_message_length(request->request_header.function_code, &rep_length);
        if(OK != rtn)
        {
            rtn = UM4A_SYSTEM_ERR;
        }

        if(OK == rtn)
        {
            reply = (UM4I_REPLY_STRUCT *)MM4A_malloc(rep_length);
            if(reply == NULL)
            {
                rtn = UM4A_SYSTEM_ERR;
            }
        }

        if(OK == rtn)
        {
            memset(reply, 0x0, rep_length);
            reply->reply_header.execute_result = rtn;
            reply->reply_header.function_code = request->request_header.function_code;
            UM4I_send_reply_message(server,msg_id,reply);
        }
    }
    return;
}

