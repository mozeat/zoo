/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : STATE_MANAGER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "STATE_MANAGER_CLASS.h"
#include "ZOO_enum_to_str.h"
#include "UM_CONFIGURE.h"
namespace UM
{
    /*
     * @brief Constructor
    **/ 
    STATE_MANAGER_CLASS::STATE_MANAGER_CLASS()
    {
		this->m_driver_state = ZOO_DRIVER_STATE_IDLE;
		std::string db_file = UM_CONFIGURE::get_instance()->get_db_path()+ "/" + "ZOO_FLATFORM.db"; 
		this->m_dao.reset(new PERSISTENCE_CLASS(db_file));
    }

    /*
     * @brief Constructor
    **/ 
    STATE_MANAGER_CLASS::~STATE_MANAGER_CLASS()
    {
        
    }

    /*
     * @brief Set driver state
    **/ 
    void STATE_MANAGER_CLASS::set_driver_state(IN ZOO_DRIVER_STATE_ENUM state)
    {
        this->m_driver_state = state;
    }

    /*
     * @brief Get driver state
    **/ 
    ZOO_DRIVER_STATE_ENUM STATE_MANAGER_CLASS::get_driver_state()
    {
        return this->m_driver_state;
    }

    /*
     * @brief Update driver state
    **/ 
    void STATE_MANAGER_CLASS::update_driver_state(IN ZOO_DRIVER_STATE_ENUM state)
    {
        if(this->m_driver_state != state)
        {
            this->m_driver_state = state;
        }
    }

    /*
     * @brief Set ota state
    **/ 
    void STATE_MANAGER_CLASS::set_ota_state(IN ZOO_OTA_STATE_ENUM ota_state)
    {
        ZOO_OTA_STATE_ENUM cur_state = this->m_dao->get_ota_state();
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
													":: changes ota state from %s to %s",
													ZOO_ota_state_enum_to_str(cur_state),
													ZOO_ota_state_enum_to_str(ota_state));
	    this->m_dao->insert(ota_state);
    }

    /*
     * @brief Get ota state
    **/ 
    ZOO_OTA_STATE_ENUM STATE_MANAGER_CLASS::get_ota_state()
    {
        return this->m_dao->get_ota_state();;
    }

    /*
	 * @brief Set deployment_url
	 * @param 
	**/ 
	void  STATE_MANAGER_CLASS::save(IN ZOO_OTA_STATE_ENUM ota_state,std::string version,std::string release_note)
	{
		this->m_dao->insert(ota_state,version,release_note);
	}

	std::string STATE_MANAGER_CLASS::get_last_version()
	{
	    return this->m_dao->get_last_version();
	}
} //namespace UM
