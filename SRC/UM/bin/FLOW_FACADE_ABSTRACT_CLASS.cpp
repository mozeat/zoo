/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : FLOW_FACADE_ABSTRACT_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "FLOW_FACADE_ABSTRACT_CLASS.h"
namespace UM
{
    /*
     * @brief Constructor
    **/ 
    FLOW_FACADE_ABSTRACT_CLASS::FLOW_FACADE_ABSTRACT_CLASS()
    {

    }

    /*
     * @brief Constructor
    **/ 
    FLOW_FACADE_ABSTRACT_CLASS::~FLOW_FACADE_ABSTRACT_CLASS()
    {

    }
    
	void FLOW_FACADE_ABSTRACT_CLASS::set_event_publisher(IN boost::shared_ptr<EVENT_PUBLISHER_CLASS> event_publisher)
    {
		this->m_event_publisher = event_publisher;
    }

    /*
     * @brief Set device controller instance
    **/ 
    void FLOW_FACADE_ABSTRACT_CLASS::set_device_controller(IN boost::shared_ptr<INSTALLER_CONTROLLER_INTERFACE> device_controller)
    {
        this->m_device_controller = device_controller;
    }

    /*
     * @brief Get device controller instance
    **/ 
    boost::shared_ptr<INSTALLER_CONTROLLER_INTERFACE>  FLOW_FACADE_ABSTRACT_CLASS::get_device_controller()
    {
        return this->m_device_controller;
    }

    /*
     * @brief Set state manager instance
    **/ 
    void FLOW_FACADE_ABSTRACT_CLASS::set_state_manager(IN boost::shared_ptr<STATE_MANAGER_CLASS> state_manager)
    {
        this->m_state_manager = state_manager;
    }

    /*
     * @brief Get state manager instance
    **/ 
    boost::shared_ptr<STATE_MANAGER_CLASS>  FLOW_FACADE_ABSTRACT_CLASS::get_state_manager()
    {
        return this->m_state_manager;
    }
    
	/*
	 * @brief install_sequence attribute value
	 * @param install_sequence	The new install_sequence attribute value 
	**/
	void FLOW_FACADE_ABSTRACT_CLASS::set_install_sequence(boost::shared_ptr<INSTALL_SEQUENCE_CLASS> install_sequence)
	{
        this->m_module_sequence = install_sequence;
    }
    
	/*
	 * @brief Set display service instance
	**/ 
	void FLOW_FACADE_ABSTRACT_CLASS::set_display_service(boost::shared_ptr<UM::DISPLAY_SERVICE_CLASS> display_service)
	{
        this->m_display_service = display_service;
    }

    /*
     * @brief This method is executed when property changed value
     * @param model The source object contains property changed
     * @param property_name The property has been changed value
    **/ 
    void FLOW_FACADE_ABSTRACT_CLASS::on_property_changed(IN CONTROLLER_INTERFACE* model,IN const ZOO_UINT32 property_name)
    {
        // The implementation for this method will be written in concrete classes
    }

	/*
	 * @brief This method is executed when property changed value
	 * @param model The source object contains property changed
	 * @param property_name The property has been changed value
	**/ 
	void FLOW_FACADE_ABSTRACT_CLASS::on_property_changed(IN MARKING_MODEL_INTERFACE* model,IN const ZOO_UINT32 property_name)
	{
		
	}
} //namespace UM

