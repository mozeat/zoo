/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : DEVICE_CONTROLLER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "INSTALLER_CONTROLLER_CLASS.h"
#include "DPKG_MODEL_CLASS.h"
#include "UDS_MODEL_CLASS.h"
namespace UM
{
    /*
     * @brief Constructor
    **/ 
    INSTALLER_CONTROLLER_CLASS::INSTALLER_CONTROLLER_CLASS()
    {

    }

    /*
     * @brief Destructor
    **/ 
    INSTALLER_CONTROLLER_CLASS::~INSTALLER_CONTROLLER_CLASS()
    {

    }

    /*
     * @brief Initialize the model
     * @return throw exception 
    **/ 
    void INSTALLER_CONTROLLER_CLASS::initialize()
    {
    	this->m_device_map.clear();
        this->create_all_models();
    }

    /*
     * @brief Create all models
    **/ 
    void INSTALLER_CONTROLLER_CLASS::create_all_models()
    {
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> ...");
        this->create_model_by_type(INSTALLER_DPKG);
        this->create_model_by_type(INSTALLER_UDS);
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< ...");
    }
    
    /*
    * @brief Install all models by configurations.
    **/ 
    void INSTALLER_CONTROLLER_CLASS::install(IN boost::shared_ptr<SW_MODULE_CLASS> sw_module)
    {
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> ...");
        if(sw_module)
        {
        	auto type = sw_module->get_type();
        	auto found = this->m_device_map.begin();
        	while(found != this->m_device_map.end())
        	{
        		auto device = found->second;
        		if(device->get_marking_code() == type)
        		{
        			device->install(sw_module);
        		}
        		++ found;
        	}
        }
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< ...");
    }
    
	/*
	 * @brief Clear all models by configurations.
	**/ 
	void INSTALLER_CONTROLLER_CLASS::clean()
    {
        this->m_device_map.clear();
    }

    /*
     * @brief Notify property has been changed
     * @property_name     The property has been changed
    **/ 
    void INSTALLER_CONTROLLER_CLASS::on_property_changed(MARKING_MODEL_INTERFACE* model,
    															            const ZOO_UINT32 property_name)
    {
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry...");
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__," :: property_name:%d",property_name);
    	boost::shared_ptr<DEVICE_INTERFACE> device = 
    						model->get_pointer<DEVICE_INTERFACE>();
    	switch(property_name)
    	{
    		case DEVICE_INSTALL_IDLE:
    			break;
    		case DEVICE_INSTALL_ONGOING:
        		this->notify_of_property_changed(DEVICE_INSTALL_ONGOING);
    			break;
    		case DEVICE_INSTALL_FAIL:
        		this->notify_of_property_changed(DEVICE_INSTALL_FAIL);
        		device->reset_model();
    			break;
    		case DEVICE_INSTALL_SUCCESS:
        		this->notify_of_property_changed(DEVICE_INSTALL_SUCCESS);
        		device->reset_model();
    			break;
        	default:
        		device->reset_model();
        		break;
        }
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function exit...");
    }

	boost::shared_ptr<DEVICE_INTERFACE> INSTALLER_CONTROLLER_CLASS::get_model(std::string type)
	{
		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry...");
		boost::shared_ptr<DEVICE_INTERFACE> model = this->m_device_map.at(INSTALLER_DPKG);
		auto found = this->m_device_map.begin();
    	while(found != this->m_device_map.end())
    	{
    		auto device = found->second;
    		ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,
    							":: current device type is %s,expected %s",
    							device->get_marking_code().c_str(),type.c_str());
    		if(type == device->get_marking_code())
    		{
    			model = device;
    			break;
    		}
    		++ found;
    	}
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function exit...");
    	return model;
	}
	
    /*
     * @brief Create one model by the given type.
     **/ 
    void INSTALLER_CONTROLLER_CLASS::create_model_by_type(IN ZOO_INT32 type)
    {
    	ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"> function entry...");
    	boost::shared_ptr<DEVICE_INTERFACE> device;
        switch(type)
        {
        	case INSTALLER_DPKG:
    		{
				ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,":: create device model %s","dpkg ...SUCCESS");
    			device.reset(new DPKG_MODEL_CLASS());
    			device->set_marking_code("dpkg");
    			device->add_observer(this);
    			this->m_device_map[INSTALLER_DPKG] = device;
    			break;
    		}
    		case INSTALLER_UDS:
    		{
    			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,":: create device model %s","uds ...SUCCESS");
    			device.reset(new UDS_MODEL_CLASS());
    			device->set_marking_code("uds");
    			device->add_observer(this);
    			this->m_device_map[INSTALLER_UDS] = device;
    			break;
    		}
    		default:
    			break;
        }
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__,"< function exit...");
    }
} //namespace UM
