/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : TASK_MANAGER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "TASK_MANAGER_CLASS.h" 
#include <thread>
#include <chrono>
namespace UM
{
	/*
	* @brief Constructor
	**/ 
	TASK_MANAGER_CLASS::TASK_MANAGER_CLASS()
	{
	}

	/*
	* @brief Destructor
	**/ 
	TASK_MANAGER_CLASS::~TASK_MANAGER_CLASS()
	{
	}

	/*
	* @brief Destructor
	**/ 
	ZOO_INT32 TASK_MANAGER_CLASS::start_all_tasks()
	{
		ZOO_INT32 retry_times = 3;
		while (SM4A_start_all_tasks() != OK && retry_times)
		{
			SM4A_stop_all_tasks();
			std::this_thread::sleep_for(std::chrono::seconds(2));;
			retry_times --;
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,
							__ZOO_FUNC__,":: stop task retry ...%d",(3 - retry_times));
		}
		if(retry_times <= 0)
		{
			ZOO_slog(ZOO_SEVERITY_LEVEL_CRITICAL,
							__ZOO_FUNC__,":: stop task retry timeout");
		}
		return retry_times <= 0 ? UM4A_SYSTEM_ERR : OK;
	}

	/*
	* @brief Destructor
	**/ 
	ZOO_INT32 TASK_MANAGER_CLASS::stop_all_tasks()
	{
	    if(OK != SM4A_initialize())
	    {
	        ZOO_slog(ZOO_SEVERITY_LEVEL_CRITICAL,
							__ZOO_FUNC__,":: SM4A_initialize failure");
	        return UM4A_PARAMETER_ERR;
	    }	   
		return OK;
	}
} // namespace UM
