/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : UM
 * File Name      : OTA_PROTOCAL_PARSER_CLASS.cpp
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-12    Generator      created
*************************************************************/
#include "OTA_PROTOCOL_PARSER_CLASS.h"
namespace UM
{
	/*
	 * @brief Constructor
	**/ 
	OTA_PROTOCAL_PARSER_CLASS::OTA_PROTOCAL_PARSER_CLASS()
	{
	
	}

	/*
	 * @brief Destructor
	**/ 
	OTA_PROTOCAL_PARSER_CLASS::~OTA_PROTOCAL_PARSER_CLASS()
	{
	
	}

	/*
     * @brief Get config data url from response
     * @param response
    **/ 
	std::string OTA_PROTOCAL_PARSER_CLASS::get_url(std::string domain,
														std::string resource_id,
														std::string user)
	{
    	std::ostringstream os;
    	os << domain << "/";
	    os << user << "/";
	    os << "controller/v1/" << resource_id;
	    return os.str();
	}

	/*
     * @brief Get polling sleep time
     * @param sleep_time
    **/ 
	ZOO_UINT32 OTA_PROTOCAL_PARSER_CLASS::get_polling_sleep_time(std::string & reply)
	{
		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");
		ZOO_INT32 error_code = OK;
		__UM_TRY
		{	
    		std::stringstream ss(reply);
			boost::property_tree::ptree root;
			boost::property_tree::read_json(ss,root);
			auto cfg_tree = root.get_child("config");
			auto polling_tree = cfg_tree.get_child("polling");
			std::string date_and_time = polling_tree.get<std::string>("sleep");
    		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
			//										":: time string is %s",date_and_time.c_str());
			// 2019-06-25 is useless
			auto time =
					boost::posix_time::time_from_string("2019-06-25 " + date_and_time).time_of_day();
			ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
										":: convert time string %s to seconds %lu",
										date_and_time.c_str(),
										time.minutes() * 60);
    		//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
			//										"< function exit ...");
			return time.minutes() * 60;
    	}
    	__UM_CATCH_ALL(error_code)
    	//ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,
		//								"< function exit [error_code = %x]...",error_code);
	    return 60*2;
	}

	/*
     * @brief Get request headers
     * @param accept
    **/ 
	std::vector<std::string> OTA_PROTOCAL_PARSER_CLASS::get_headers(std::string token)
	{
		std::vector<std::string> headers;
    	headers.push_back("Accept: application/hal+json");
    	headers.push_back("Content-Type: application/json;charset=UTF-8");
    	headers.push_back("Authorization: GatewayToken " + token);
    	return headers;
	}

	/*
     * @brief Build response message for upgrade api
     * @param action_id
     * @param finished
     * @param total_packages
     * @param current_count
     * @param execution
     * @param details
    **/ 
	std::string OTA_PROTOCAL_PARSER_CLASS::build_feedback_message(std::string action_id,
																			std::string finished,
																			ZOO_INT32 total_packages,
																			ZOO_INT32 cur_packages,
																			std::string execution,
																			std::string details)
	{
		boost::property_tree::ptree root;
		root.put<std::string>("id", action_id);
		root.put<std::string>("time", std::to_string(ZOO_get_current_timestamp_msec()));
		
		boost::property_tree::ptree details_array;
		boost::property_tree::ptree details_1;
		details_1.put<std::string>("", details);
		details_array.push_back(std::make_pair("",details_1));
		
	    boost::property_tree::ptree node_status_result_progress;
	    node_status_result_progress.put<std::string>("of", ":of");
	    node_status_result_progress.put<std::string>("cnt", ":cnt");
		
	    boost::property_tree::ptree node_status_result;
	    node_status_result.put<std::string>("finished","none");
	    node_status_result.add_child("progress",node_status_result_progress);
	    
	    boost::property_tree::ptree node_status;
		node_status.put<std::string>("execution",execution);
	    node_status.add_child("result",node_status_result);
		node_status.add_child("details",details_array);
		
	    root.add_child("status",node_status);
	    std::ostringstream ss;
	    boost::property_tree::write_json(ss, root, false);
	    ss << std::endl;
	    std::string json = ss.str();
	    this->replace(json, ":of", std::to_string(total_packages));
        this->replace(json, ":cnt", std::to_string(cur_packages));
	    return json;
	}

	/*
     * @brief build feedback for put config data
     * @param action_id
     * @param ZOO_INT32
     * @param total_packages
     * @param current_count
     * @param execution
     * @param details
    **/ 
	std::string OTA_PROTOCAL_PARSER_CLASS::build_feedback_message(std::string action_id,
																			std::string mode,
																			std::string VIN,
																			std::string hw_version,
																			std::string finished,
																			std::string execution,
																			std::string details)
	{
		boost::property_tree::ptree root;
		root.put<string>("mode", mode);
		
		boost::property_tree::ptree node_data;
		node_data.put<string>("VIN", VIN);
		node_data.put<string>("hwRevision", hw_version);
	    root.add_child("data",node_data);
	    
		root.put<string>("id", action_id);
		root.put<string>("time", std::to_string(ZOO_get_current_timestamp_msec()));
	    
		boost::property_tree::ptree details_array;
		boost::property_tree::ptree details_1;
		details_1.put<string>("", details);
		details_array.push_back(std::make_pair("",details_1));

	    boost::property_tree::ptree node_status;
	    boost::property_tree::ptree node_status_result;
	    node_status_result.put<string>("finished", finished);
	    node_status.add_child("result",node_status_result);
	    node_status.put<string>("execution",execution);
	    node_status.add_child("details",details_array);
	    root.add_child("status",node_status);

	    std::ostringstream ss;
	    boost::property_tree::write_json(ss, root, false);
	    ss << std::endl;
	    return ss.str();
	}

    void OTA_PROTOCAL_PARSER_CLASS::replace(std::string& json, const std::string& placeholder, const std::string& value) 
    {
        boost::replace_all<std::string>(json, "\"" + placeholder + "\"", value);
    }

	/*
     * @brief build feedback for cancel action api
     * @param action_id
     * @param finished
     * @param execution
     * @param details
    **/ 
	std::string OTA_PROTOCAL_PARSER_CLASS::build_feedback_message(std::string action_id,
												std::string finished,
												std::string execution,
												std::string details)
	{
		boost::property_tree::ptree root;
		root.put<string>("id", action_id);
		root.put<string>("time", std::to_string(ZOO_get_current_timestamp_msec()));
		
	    boost::property_tree::ptree node_status;
	    boost::property_tree::ptree node_status_result;
	    boost::property_tree::ptree progress;
	    node_status_result.put<string>("finished", finished);
	    progress.put<string>("of",":of");
	    progress.put<string>("cnt",":cnt");	    
        node_status_result.add_child("progress",progress);	    
	    node_status.add_child("result",node_status_result);

		node_status.put<string>("execution",execution);
        
	    boost::property_tree::ptree details_array;
		boost::property_tree::ptree details_1;
		details_1.put<string>("", details);
		details_array.push_back(std::make_pair("",details_1));
		node_status.add_child("details",details_array);
		
	    root.add_child("status",node_status);

	    std::ostringstream ss;
	    boost::property_tree::write_json(ss, root, false);
	    ss << std::endl;
	    std::string json = ss.str();
	    this->replace(json, ":of", std::to_string(1));
        this->replace(json, ":cnt", std::to_string(1));
	    return json;
	}
	
	/*
	 * @brief Get config data url from request
	 * @param response
	**/ 
	std::string OTA_PROTOCAL_PARSER_CLASS::get_config_data_url(std::string domain,
                            								    std::string resource_id,
                            								    std::string user)
	{
		std::ostringstream os;
    	os << domain << "/";
	    os << user << "/";
	    os << "controller/v1/" << resource_id << "/";
	    os << "configData";
	    return os.str();
	}

	/*
	 * @brief Get deployment url from request
	 * @param response
	**/ 
	std::string OTA_PROTOCAL_PARSER_CLASS::get_deployment_base_url(std::string & response)
	{
		__UM_TRY
		{
			std::stringstream ss(response);
			boost::property_tree::ptree root;
			boost::property_tree::read_json(ss,root);
			auto child = root.get_child_optional("_links.deploymentBase");
			if(!child)
			{
			  	__THROW_UM_EXCEPTION(UM4A_PARAMETER_ERR, ":: no new packages ...", NULL);
			}
			
			return child.get().get<std::string>("href");
		}
		__UM_CATCH(true)
		
	}

	std::string OTA_PROTOCAL_PARSER_CLASS::get_deployment_feedback_url(std::string domain,
                            								    std::string resource_id,
                            								    std::string user,
                            								    std::string action_id)
    {
        std::ostringstream os;
    	os << domain << "/";
	    os << user << "/";
	    os << "controller/v1/" << resource_id << "/";
	    os << "deploymentBase" << "/";
	    os << action_id << "/";;
	    os << "feedback";
	    return os.str();
    }

	/*
     * @brief Get cancel action url from response
     * @param response
    **/ 
	std::string OTA_PROTOCAL_PARSER_CLASS::get_cancel_action_url(std::string domain,
                            								    std::string resource_id,
                            								    std::string user,
                            								    std::string action_id)
	{
		std::ostringstream os;
    	os << domain << "/";
	    os << user << "/";
	    os << "controller/v1/" << resource_id << "/";
	    os << "cancelAction" << "/";
	    os << action_id ;
	    return os.str();
	}
	
	/*
     * @brief Get config data url from response
     * @param response
    **/ 
	std::string OTA_PROTOCAL_PARSER_CLASS::get_cancel_feedback_url(std::string domain,
							    std::string resource_id,
							    std::string user,
							    std::string action_id)
    {
        std::ostringstream os;
    	os << domain << "/";
	    os << user << "/";
	    os << "controller/v1/" << resource_id << "/";
	    os << "cancelAction" << "/";
	    os << action_id << "/";
	    os << "feedback";
	    return os.str();
    }
    
	/*
	 * @brief Get id from deployment
	 * @param response
	**/ 
	std::string OTA_PROTOCAL_PARSER_CLASS::get_id_from_deployment(std::string & message)
	{
		std::stringstream ss(message);
		boost::property_tree::ptree root;
		boost::property_tree::read_json(ss,root);
		auto optional = root.get_optional<std::string>("id");
		if(optional)
		{
			return optional.get();
		}
		return "";
	}

	std::string OTA_PROTOCAL_PARSER_CLASS::parse_stop_id(std::string & message)
	{
	    std::stringstream ss(message);
		boost::property_tree::ptree root;
		boost::property_tree::read_json(ss,root);
		boost::property_tree::ptree cancelAction = root.get_child("cancelAction");
		std::string stop_id = cancelAction.get<std::string>("stopId");
	    return stop_id;
	}

	/*
	 * @brief Get deployment type
	 * @return type
	**/ 
    DEPLOYMENT_TYPE_ENUM OTA_PROTOCAL_PARSER_CLASS::parse_update_processing_type(std::string & message)
    {
        DEPLOYMENT_TYPE_ENUM type = SKIP;
        std::stringstream ss(message);
		boost::property_tree::ptree root;
		boost::property_tree::read_json(ss,root);
		boost::property_tree::ptree deployment = root.get_child("deployment");
		auto optional = deployment.get_optional<std::string>("update");
		if(optional)
		{
			std::string tmp = optional.get();
			if(tmp == "attempt")
			{
			    type = ATTEMPT;
			}
			
			if(tmp == "forced")
			{
			    type = FORCED;
			}
		}
		return type;
    }

	/*
	 * @brief parse SW Module from deployment_reply_message
	 * @param modules
	**/ 
	std::vector<boost::shared_ptr<SW_MODULE_CLASS> > OTA_PROTOCAL_PARSER_CLASS::parse_software_modules(std::string && deployment_reply_message)
	{
		std::vector<boost::shared_ptr<SW_MODULE_CLASS> > modules;
		try
		{
			std::stringstream ss(deployment_reply_message);
			boost::property_tree::ptree root;
			boost::property_tree::read_json(ss,root);
			BOOST_FOREACH(boost::property_tree::ptree::value_type & v,
								root.get_child("deployment.chunks"))
			{
				BOOST_FOREACH(boost::property_tree::ptree::value_type & artifact,
													v.second.get_child("artifacts"))
				{	
					auto module = boost::make_shared<SW_MODULE_CLASS>();
					std::string file_name = artifact.second.get<std::string>("filename");
					ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"filename: %s",file_name.c_str());
					module->set_package_name(file_name);
					
					auto hashes_tree = artifact.second.get_child("hashes");
					std::string md5 = hashes_tree.get<std::string>("md5");
					ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: md5: %s",md5.c_str());
					module->set_package_md5(md5);
					
					std::string sha1 = hashes_tree.get<std::string>("sha1");
					ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: sha1: %s",sha1.c_str());
					module->set_package_sha1(sha1);
					
					auto links_tree = artifact.second.get_child("_links.download-http");
					std::string download_http = links_tree.get<std::string>("href");
					ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: download-http: %s",download_http.c_str());
					module->set_download_url(download_http);
					modules.emplace_back(module);
				}
			}
		}
		catch(boost::property_tree::ptree_error & e)
		{
			__THROW_UM_EXCEPTION(UM4A_PARAMETER_ERR,e.what(),NULL);
		}
		return modules;
	}
}
