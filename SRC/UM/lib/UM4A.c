/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : UM4A.c
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-16    Generator      created
*************************************************************/

#include <ZOO.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <MQ4A_if.h>
#include <MQ4A_type.h>
#include <MM4A_if.h>
#include "UM4I_type.h"
#include "UM4A_type.h"
#include "UM4I_if.h"
/**
 *@brief UM4A_perform_upgrade
**/
ZOO_INT32 UM4A_perform_upgrade()
{
    ZOO_INT32 result = OK;
    UM4I_REQUEST_STRUCT *request_message = NULL; 
    UM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = UM4A_PERFORM_UPGRADE_CODE;
    result = UM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (UM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = UM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = UM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (UM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = UM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
    }

    if(OK == result)
    {
        result = UM4I_send_request_and_reply(UM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief UM4A_undo_the_upgrade
**/
ZOO_INT32 UM4A_undo_the_upgrade()
{
    ZOO_INT32 result = OK;
    UM4I_REQUEST_STRUCT *request_message = NULL; 
    UM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = UM4A_NOTIFY_RECIEVED_ACK_CODE;
    result = UM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (UM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = UM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = UM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (UM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
    }

    if(OK == result)
    {
        if(reply_message == NULL)
        {
            result = UM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
    }

    if(OK == result)
    {
        result = UM4I_send_request_and_reply(UM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

/**
 *@brief UM4A_get_status
 *@param status
**/
ZOO_INT32 UM4A_get_status(OUT UM4A_STATUS_STRUCT * status)
{
    ZOO_INT32 result = OK;
    UM4I_REQUEST_STRUCT *request_message = NULL; 
    UM4I_REPLY_STRUCT *reply_message = NULL;	
    ZOO_INT32 reply_length = 0;
    ZOO_INT32 ZOO_timeout = 60000;
    ZOO_INT32 request_length = 0;
    ZOO_INT32 func_code = UM4A_GET_STATUS_CODE;
    result = UM4I_get_request_message_length(func_code, &request_length);
    if(OK == result)
    {
        request_message = (UM4I_REQUEST_STRUCT *)MM4A_malloc(request_length);
    }

    if(OK == result)
    {
        if(request_message == NULL)
        {
            result = UM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        result = UM4I_get_reply_message_length(func_code, &reply_length);
    }

    if(OK == result)
    {
        reply_message = (UM4I_REPLY_STRUCT *)MM4A_malloc(reply_length);
        if(reply_message == NULL)
        {
            result = UM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        request_message->request_header.function_code = func_code;
        reply_message->reply_header.function_code = func_code;
        request_message->request_header.need_reply = ZOO_TRUE;
    }

    if(OK == result)
    {
        result = UM4I_send_request_and_reply(UM4A_SERVER,request_message,reply_message,ZOO_timeout);
    }

    if(OK == result)
    {
        result = reply_message->reply_header.execute_result;
        memcpy(status,&reply_message->reply_body.get_status_rep_msg.status,sizeof(UM4A_STATUS_STRUCT));
    }

    if(request_message != NULL)
    {
        MM4A_free(request_message);
    }

    if(reply_message != NULL)
    {
        MM4A_free(reply_message);
    }
    return result;
}

static void UM4A_status_callback(void *context_p, MQ4A_CALLBACK_STRUCT *local_proc, void *msg)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 error_code = OK;
    UM4I_REPLY_STRUCT *reply_msg = NULL;
    UM4I_STATUS_SUBSCRIBE_CODE_CALLBACK_STRUCT * callback_struct = NULL;
    ZOO_INT32 rep_length = 0;
    UM4A_STATUS_STRUCT status;
    memset(&status,0,sizeof(UM4A_STATUS_STRUCT));
    if(msg == NULL)
    {
        result = UM4A_PARAMETER_ERR;
    }

    if(OK == result)
    {
        result = UM4I_get_reply_message_length(UM4A_STATUS_SUBSCRIBE_CODE, &rep_length);
        if(OK != result)
        {
            result = UM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        reply_msg = (UM4I_REPLY_STRUCT * )MM4A_malloc(rep_length);
        if(reply_msg == NULL)
        {
            result = UM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        memcpy(reply_msg, msg, rep_length);
        if (UM4A_STATUS_SUBSCRIBE_CODE != reply_msg->reply_header.function_code)
        {
            result = UM4A_PARAMETER_ERR;
        }
        error_code = reply_msg->reply_header.execute_result;
        memcpy((void*)&status, &reply_msg->reply_body.status_subscribe_code_rep_msg.status, sizeof(UM4A_STATUS_STRUCT));
        callback_struct = (UM4I_STATUS_SUBSCRIBE_CODE_CALLBACK_STRUCT*) local_proc;
        ((UM4A_STATUS_FUNCTION)callback_struct->callback_function)(status,error_code,context_p);
    }

    if(reply_msg != NULL)
    {
        MM4A_free(reply_msg);
    }
}


 ZOO_INT32 UM4A_status_subscribe(IN UM4A_STATUS_FUNCTION callback_function,
																OUT ZOO_UINT32 *handle,
																INOUT void *context)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = 0;
    MQ4A_CALLBACK_STRUCT* callback = NULL;
    if (NULL == callback_function)
    {
        result = UM4A_PARAMETER_ERR;
        return result;
    }

    /*fill callback strcut*/
    callback = (MQ4A_CALLBACK_STRUCT*)MM4A_malloc(sizeof(UM4I_STATUS_SUBSCRIBE_CODE_CALLBACK_STRUCT));
    if (NULL == callback)
    {
        result = UM4A_PARAMETER_ERR;
    }

    if (OK == result)
    {
        callback->callback_function = callback_function;
        event_id = UM4A_STATUS_SUBSCRIBE_CODE;
        result = UM4I_send_subscribe(UM4A_SERVER,
                                      UM4A_status_callback,
                                      callback,
                                      event_id,
                                      (ZOO_HANDLE*)handle,
                                      context);
        if (OK != result)
        {
           result = UM4A_PARAMETER_ERR;
           MM4A_free(callback);
        }
    }

    return result;
}

ZOO_INT32 UM4A_status_unsubscribe(IN ZOO_UINT32 handle)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = UM4A_STATUS_SUBSCRIBE_CODE;
    if(OK == result)
    {
        result = UM4I_send_unsubscribe(UM4A_SERVER,event_id,handle);
    }
    return result;
}

static void UM4A_inprogress_callback(void *context_p, MQ4A_CALLBACK_STRUCT *local_proc, void *msg)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 error_code = OK;
    UM4I_REPLY_STRUCT *reply_msg = NULL;
    UM4I_INPROGRESS_SUBSCRIBE_CODE_CALLBACK_STRUCT * callback_struct = NULL;
    ZOO_INT32 rep_length = 0;
    UM4A_INPROGRESS_STRUCT status;
    memset(&status,0,sizeof(UM4A_INPROGRESS_STRUCT));
    if(msg == NULL)
    {
        result = UM4A_PARAMETER_ERR;
    }

    if(OK == result)
    {
        result = UM4I_get_reply_message_length(UM4A_INPROGRESS_SUBSCRIBE_CODE, &rep_length);
        if(OK != result)
        {
            result = UM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        reply_msg = (UM4I_REPLY_STRUCT * )MM4A_malloc(rep_length);
        if(reply_msg == NULL)
        {
            result = UM4A_PARAMETER_ERR;
        }
    }

    if(OK == result)
    {
        memcpy(reply_msg, msg, rep_length);
        if (UM4A_INPROGRESS_SUBSCRIBE_CODE != reply_msg->reply_header.function_code)
        {
            result = UM4A_PARAMETER_ERR;
        }
        error_code = reply_msg->reply_header.execute_result;
        memcpy((void*)&status, &reply_msg->reply_body.inprogress_subscribe_code_rep_msg.status, sizeof(UM4A_INPROGRESS_STRUCT));
        callback_struct = (UM4I_INPROGRESS_SUBSCRIBE_CODE_CALLBACK_STRUCT*) local_proc;
        ((UM4A_INPROGRESS_CALLBACK_FUNCTION)callback_struct->callback_function)(status,error_code,context_p);
    }

    if(reply_msg != NULL)
    {
        MM4A_free(reply_msg);
    }
}


 ZOO_INT32 UM4A_inprogress_subscribe(IN UM4A_INPROGRESS_CALLBACK_FUNCTION callback_function,
																OUT ZOO_UINT32 *handle,
																INOUT void *context)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = 0;
    MQ4A_CALLBACK_STRUCT* callback = NULL;
    if (NULL == callback_function)
    {
        result = UM4A_PARAMETER_ERR;
        return result;
    }

    /*fill callback strcut*/
    callback = (MQ4A_CALLBACK_STRUCT*)MM4A_malloc(sizeof(UM4I_INPROGRESS_SUBSCRIBE_CODE_CALLBACK_STRUCT));
    if (NULL == callback)
    {
        result = UM4A_PARAMETER_ERR;
    }

    if (OK == result)
    {
        callback->callback_function = callback_function;
        event_id = UM4A_INPROGRESS_SUBSCRIBE_CODE;
        result = UM4I_send_subscribe(UM4A_SERVER,
                                      UM4A_inprogress_callback,
                                      callback,
                                      event_id,
                                      (ZOO_HANDLE*)handle,
                                      context);
        if (OK != result)
        {
           result = UM4A_PARAMETER_ERR;
           MM4A_free(callback);
        }
    }

    return result;
}

ZOO_INT32 UM4A_inprogress_unsubscribe(IN ZOO_UINT32 handle)
{
    ZOO_INT32 result = OK;
    ZOO_INT32 event_id = UM4A_INPROGRESS_SUBSCRIBE_CODE;
    if(OK == result)
    {
        result = UM4I_send_unsubscribe(UM4A_SERVER,event_id,handle);
    }
    return result;
}

