/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : XX
 * File Name      : UM4I_type.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-07-16    Generator      created
*************************************************************/
#ifndef UM4I_TYPE_H
#define UM4I_TYPE_H
#include <MQ4A_type.h>
#include "UM4A_type.h"
#include "UM4A_if.h"


/**
 *@brief Macro Definitions
**/
#define UM4I_COMPONET_ID "UM"
#define UM4A_SERVER     "UM4A_SERVER"
#define UM4I_BUFFER_LENGTH    256
#define UM4I_RETRY_INTERVAL   3

/**
 *@brief Function Code Definitions
**/
#define UM4A_PERFORM_UPGRADE_CODE 0x554dff01
#define UM4A_NOTIFY_RECIEVED_ACK_CODE 0x554dff02
#define UM4A_GET_STATUS_CODE 0x554dff03
#define UM4A_STATUS_SUBSCRIBE_CODE 0x554dff04
#define UM4A_INPROGRESS_SUBSCRIBE_CODE 0x554dff06

/*Request and reply header struct*/
typedef struct
{
    MQ4A_SERV_ADDR reply_addr;
    ZOO_UINT32 msg_id;
    ZOO_BOOL reply_wanted;
    ZOO_UINT32 func_id;
}UM4I_REPLY_HANDLER_STRUCT;

typedef  UM4I_REPLY_HANDLER_STRUCT * UM4I_REPLY_HANDLE;

/*Request message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL need_reply;
}UM4I_REQUEST_HEADER_STRUCT;

/*Reply message header struct*/
typedef struct
{
    ZOO_UINT32 function_code;
    ZOO_BOOL execute_result;
}UM4I_REPLY_HEADER_STRUCT;

/**
*@brief UM4I_PERFORM_UPGRADE_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}UM4I_PERFORM_UPGRADE_CODE_REQ_STRUCT;

/**
*@brief UM4I_NOTIFY_RECIEVED_ACK_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}UM4I_NOTIFY_RECIEVED_ACK_CODE_REQ_STRUCT;

/**
*@brief UM4I_GET_STATUS_CODE_REQ_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}UM4I_GET_STATUS_CODE_REQ_STRUCT;

/**
*@brief UM4I_PERFORM_UPGRADE_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}UM4I_PERFORM_UPGRADE_CODE_REP_STRUCT;

/**
*@brief UM4I_NOTIFY_RECIEVED_ACK_CODE_REP_STRUCT
**/
typedef struct 
{
    ZOO_CHAR g_filler[8];
}UM4I_NOTIFY_RECIEVED_ACK_CODE_REP_STRUCT;

/**
*@brief UM4I_GET_STATUS_CODE_REP_STRUCT
**/
typedef struct 
{
    UM4A_STATUS_STRUCT status;
    ZOO_CHAR g_filler[8];
}UM4I_GET_STATUS_CODE_REP_STRUCT;

/**
*@brief UM4I_STATUS_SUBSCRIBE_CODE_REP_STRUCT
**/
typedef struct 
{
    UM4A_STATUS_STRUCT status;
    ZOO_CHAR g_filler[8];
}UM4I_STATUS_SUBSCRIBE_CODE_REP_STRUCT;

/**
*@brief UM4I_INPROGRESS_SUBSCRIBE_CODE_REP_STRUCT
**/
typedef struct 
{
    UM4A_INPROGRESS_STRUCT status;
    ZOO_CHAR g_filler[8];
}UM4I_INPROGRESS_SUBSCRIBE_CODE_REP_STRUCT;

typedef struct
{
    UM4I_REQUEST_HEADER_STRUCT request_header;
    union
    {
        UM4I_PERFORM_UPGRADE_CODE_REQ_STRUCT perform_upgrade_req_msg;
        UM4I_NOTIFY_RECIEVED_ACK_CODE_REQ_STRUCT notify_recieved_ack_req_msg;
        UM4I_GET_STATUS_CODE_REQ_STRUCT get_status_req_msg;
     }request_body;
}UM4I_REQUEST_STRUCT;


typedef struct
{
    UM4I_REPLY_HEADER_STRUCT reply_header;
    union
    {
        UM4I_PERFORM_UPGRADE_CODE_REP_STRUCT perform_upgrade_rep_msg;
        UM4I_NOTIFY_RECIEVED_ACK_CODE_REP_STRUCT notify_recieved_ack_rep_msg;
        UM4I_GET_STATUS_CODE_REP_STRUCT get_status_rep_msg;
        UM4I_STATUS_SUBSCRIBE_CODE_REP_STRUCT status_subscribe_code_rep_msg;
        UM4I_INPROGRESS_SUBSCRIBE_CODE_REP_STRUCT inprogress_subscribe_code_rep_msg;
    }reply_body;
}UM4I_REPLY_STRUCT;


/**
*@brief UM4I_STATUS_SUBSCRIBE_CODE_CALLBACK_STRUCT
**/
typedef struct 
{
    UM4A_STATUS_FUNCTION *callback_function;
    void * parameter;
}UM4I_STATUS_SUBSCRIBE_CODE_CALLBACK_STRUCT;

/**
*@brief UM4I_INPROGRESS_SUBSCRIBE_CODE_CALLBACK_STRUCT
**/
typedef struct 
{
    UM4A_INPROGRESS_CALLBACK_FUNCTION *callback_function;
    void * parameter;
}UM4I_INPROGRESS_SUBSCRIBE_CODE_CALLBACK_STRUCT;

#endif //UM4I_type.h
