/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO 
 * Component ID : UM
 * File Name    : UM4A_if.h
 * Description  : Upgrade Management
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef UM4A_IF_H
#define UM4A_IF_H
#include "UM4A_type.h"

/**************************************************************************
INTERFACE <ZOO_INT32 UM4A_initialize>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正常
                UM4A_SYSTEM_ERR -- 系统错误
                UM4A_TIMEOUT_ERR -- 通信超时
                UM4A_PARAMETER_ERR -- 参数错误
<Description>:  开始升级程序
<PRECONDITION>:  已经收到升级通知
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 UM4A_perform_upgrade();

/**************************************************************************
INTERFACE <ZOO_INT32 UM4A_undo_the_upgrade>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正常
                UM4A_SYSTEM_ERR -- 系统错误
                UM4A_TIMEOUT_ERR -- 通信超时
                UM4A_PARAMETER_ERR -- 参数错误
<Description>:  撤销本次升级，告知远程本次升级取消。
<PRECONDITION>:  已经收到升级通知
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 UM4A_undo_the_upgrade();

/**************************************************************************
INTERFACE <ZOO_INT32 UM4A_get_status>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
    OUT:        UM4A_STATUS_STRUCT status
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正常
                UM4A_SYSTEM_ERR -- 系统错误
                UM4A_TIMEOUT_ERR -- 通信超时
                UM4A_PARAMETER_ERR -- 参数错误
<Description>:  获取UM组件状态
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 UM4A_get_status(OUT UM4A_STATUS_STRUCT * status);

/**************************************************************************
INTERFACE <ZOO_INT32 UM4A_status_info_subscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
    OUT:        UM4A_STATUS_STRUCT status
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正常
                UM4A_SYSTEM_ERR -- 系统错误
                UM4A_TIMEOUT_ERR -- 通信超时
                UM4A_PARAMETER_ERR -- 参数错误
<Description>:  升级包已下载完成订阅
}
**************************************************************************/
typedef void(*UM4A_STATUS_FUNCTION)(IN UM4A_STATUS_STRUCT status,IN ZOO_INT32 error_code,
													IN void *context);

ZOO_EXPORT ZOO_INT32 UM4A_status_subscribe(IN UM4A_STATUS_FUNCTION callback_function,
																OUT ZOO_UINT32 *handle,
																INOUT void *context);

/**************************************************************************
INTERFACE <ZOO_INT32 UM4A_ready_unsubscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
    OUT:        UM4A_STATUS_STRUCT status
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正常
                UM4A_SYSTEM_ERR -- 系统错误
                UM4A_TIMEOUT_ERR -- 通信超时
                UM4A_PARAMETER_ERR -- 参数错误
<Description>:  解除订阅安装包准备状态
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 UM4A_upgrade_status_unsubscribe(IN ZOO_UINT32 handle);

/**************************************************************************
INTERFACE <ZOO_INT32 UM4A_upgrade_status_subscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
    OUT:        UM4A_STATUS_STRUCT status
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正常
                UM4A_SYSTEM_ERR -- 系统错误
                UM4A_TIMEOUT_ERR -- 通信超时
                UM4A_PARAMETER_ERR -- 参数错误
<Description>:  安装包已经下载完成。
}
**************************************************************************/
typedef void(*UM4A_INPROGRESS_CALLBACK_FUNCTION)(IN UM4A_INPROGRESS_STRUCT status,IN ZOO_INT32 error_code,
													IN void *context);

ZOO_EXPORT ZOO_INT32 UM4A_inprogress_subscribe(IN UM4A_INPROGRESS_CALLBACK_FUNCTION callback_function,
																OUT ZOO_UINT32 *handle,
																INOUT void *context);

/**************************************************************************
INTERFACE <ZOO_INT32 UM4A_ready_unsubscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         none
    OUT:        UM4A_STATUS_STRUCT status
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 正常
                UM4A_SYSTEM_ERR -- 系统错误
                UM4A_TIMEOUT_ERR -- 通信超时
                UM4A_PARAMETER_ERR -- 参数错误
<Description>:  解除订阅
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 UM4A_inprogress_unsubscribe(IN ZOO_UINT32 handle);
#endif