/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : 
 * File Name      : ZOO_distribution_center.h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
#ifndef ZOO_DISTRIBUTION_CENTER_H
#define ZOO_DISTRIBUTION_CENTER_H
#include <ZOO.h>
#include <ZOO_abstract_type.h>

/**************************************************************************
INTERFACE <ZOO4A_factory_subscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_SOUTH_IMPORT_CB  south_import_cb
				void * context
    OUT:        none
    INOUT:      ZOO_HANDLE * fd
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功, 其他 -- 失败
<Description>:  接收包裹
}
**************************************************************************/
typedef void(*ZOO4A_PACKAGE_CB)(IN ZOO_PACKAGE_STRUCT * data,
                                   IN ZOO_INT32 error_code,
								   INOUT void * context);
ZOO_EXPORT ZOO_INT32 ZOO4A_package_subscribe(IN ZOO4A_PACKAGE_CB package_cb,
															INOUT ZOO_HANDLE * fd,
															IN void * context);
ZOO_EXPORT ZOO_INT32 ZOO4A_package_unsubscribe(IN ZOO_HANDLE fd);

/**************************************************************************
INTERFACE <ZOO4A_south_portal_notification_subscribe>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_PACKAGE_STRUCT * package
				none
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功,
				其他 -- 失败
<Description>:  向配送中心投递包裹
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 ZOO4A_deliver_package(IN ZOO_PACKAGE_STRUCT * package);
