/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : 
 * File Name      : .h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
#ifndef ZOO_PORTAL_H
#define ZOO_PORTAL_H
#include <ZOO.h>
#include <ZOO_abstract_type.h>

/**************************************************************************
INTERFACE <ZOO_INT32 ZOO4A_south_portal_import>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_ARTIFACT_STRUCT * data
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功, 其他 -- 失败
<Description>:  北向数据接入API
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 ZOO4A_north_portal_import(IN ZOO_NORTH_DATA_STRUCT * north_data);


/**************************************************************************
INTERFACE <ZOO_INT32 ZOO4A_south_portal_import>
{
<InterfaceType>:FUNCTION<Blocking/NonBlocking/cbf>
<Parameters>
    IN:         ZOO_ARTIFACT_STRUCT * data
    OUT:        none
    INOUT:      none
<Timeout>:      60
<Server>:       Default
<Returns>:      OK -- 成功, 其他 -- 失败
<Description>:  南向数据接入API
}
**************************************************************************/
ZOO_EXPORT ZOO_INT32 ZOO4A_south_portal_import(IN ZOO_SOUTH_DATA_STRUCT * south_data);


#endif