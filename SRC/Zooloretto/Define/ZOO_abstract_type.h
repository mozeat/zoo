/***********************************************************
 * Copyright (C) 2018, Shanghai ZOO VEHICLE CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : 
 * File Name      : .h
 * Description    : {Summary Description}
 * History        : 
 * Version        date          author         context 
 * V1.0.0         2019-10-09    Generator      created
*************************************************************/
#ifndef ZOO_ABSTRACT_TYPE_H
#define ZOO_ABSTRACT_TYPE_H
#include <ZOO.h>

/*
 * @brief define macro
 */
#ifndef ZOO_DATA_STR_LEN
#define ZOO_DATA_STR_LEN 128
#endif

/*
 * @brief Define data type enum
 */
typedef enum
{
	ZOO_DATA_TYPE_MIN = -1,
	ZOO_DATA_TYPE_INFO,
	ZOO_DATA_TYPE_CMD,
	ZOO_DATA_TYPE_FEEDBACK,
	ZOO_DATA_TYPE_ALARM,
	ZOO_DATA_TYPE_NOTIFICATION,
	ZOO_DATA_TYPE_RESERVED,
	ZOO_DATA_TYPE_MAX
}ZOO_DATA_TYPE_ENUM;

/*
 * @brief Define data structure
 */
typedef struct
{
	ZOO_CHAR id[ZOO_DATA_STR_LEN];//data id
	ZOO_CHAR value[ZOO_DATA_STR_LEN];
}ZOO_SOUTH_DATA_STRUCT;

/*
 * @brief Define data structure
 */
typedef struct
{
	ZOO_CHAR url[ZOO_DATA_STR_LEN];//"api/v1/command/"
	ZOO_CHAR param[ZOO_DATA_STR_LEN];
}ZOO_NORTH_DATA_STRUCT;

/*
 * @brief Define data structure
 */
typedef struct
{
	ZOO_CHAR id[ZOO_DATA_STR_LEN];//data id
	ZOO_CHAR value[ZOO_DATA_STR_LEN];
	ZOO_CHAR value_type[ZOO_DATA_STR_LEN];
	ZOO_CHAR range_min[ZOO_DATA_STR_LEN];
	ZOO_CHAR range_max[ZOO_DATA_STR_LEN];
	ZOO_CHAR unit[ZOO_DATA_STR_LEN];
	ZOO_CHAR description[ZOO_DATA_STR_LEN];
	ZOO_CHAR device_id[ZOO_DATA_STR_LEN];
	ZOO_CHAR manufacturer[ZOO_DATA_STR_LEN];
}ZOO_GOODS_STRUCT;

/*
 * @brief Define data structure
 */
typedef struct
{
	ZOO_CHAR id[ZOO_DATA_STR_LEN];//"id"
	ZOO_CHAR processing_site[ZOO_DATA_STR_LEN];//"factory_usr_001,factory_usr_002,cloud,databse"
	ZOO_CHAR recipients[ZOO_DATA_STR_LEN];//"cloud,databse"
	ZOO_GOODS_STRUCT goods;//"goods"
}ZOO_PACKAGE_STRUCT;

#endif