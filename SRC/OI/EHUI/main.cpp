/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : main.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "EHUI.h"
#include <QApplication>
extern "C"
{
    #include "MM4A_if.h"
    #include "EH4A_if.h"
}

/*
 *@brief define SMUI instance.
*/
EHUI* g_alarm_hmi;

/*
 *@brief define SMUI instance.
*/
ZOO_UINT32 g_upgrade_notify_fd = 0;

ZOO_EXPORT void upgrade_alarm_call_function(IN EH4A_ALARM_STRUCT *status,IN ZOO_INT32 error_code,IN void *context)
{
    context=context;
    //printf("triggered time %s\n",status.trigger_time);
    if(g_alarm_hmi)
         g_alarm_hmi->update(status,error_code);

}

ZOO_EXPORT int subscribe_EH4A_alarm_info()
{
    if(OK != EH4A_alarm_info_subscribe(upgrade_alarm_call_function,	&g_upgrade_notify_fd,NULL))
    {
        return 1;
    }

    return OK;
}

int main(int argc, char *argv[])
{
    MM4A_initialize();
    QApplication a(argc, argv);
    g_alarm_hmi = new EHUI();
    subscribe_EH4A_alarm_info();
    g_alarm_hmi->show();
    int error_code = a.exec();
    MM4A_terminate();
    EH4A_alarm_info_unsubscribe(g_upgrade_notify_fd);
    return error_code;
}
