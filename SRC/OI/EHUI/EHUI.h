/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : EHUI.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef EHUI_H
#define EHUI_H

extern "C"
{
    #include "ZOO_if.h"
    #include "ZOO_tc.h"
    #include "ZOO_enum_to_str.h"
    #include "EH4A_if.h"
    #include "EH4A_type.h"
}
#include "ZOO_if.hpp"
#include "ui_EHUI.h"
#include "ZOOPerformFunctionThread.h"
#include <QMutex>

class EHUI : public QMainWindow, private Ui::EHUI
{
    Q_OBJECT

public:
    /**
     * @brief Default constructor.
     * @param parent
     */
    explicit EHUI(QWidget *parent = nullptr);

    /**
     * @brief destructor.
     */
    ~EHUI();
public:
    void update(EH4A_ALARM_STRUCT * alarm,ZOO_INT32 error_code);

    void run(int funcCode);
    static void execute(void * param);

private slots:

    /**
     * @brief  The callback function is called if the "add" button clicked.
     */
    void on_IDC_btn_refresh_clicked();

    /**
     * @brief  The callback function is called if the "delete" button clicked.
     */
    void on_IDC_btn_clear_clicked();

    /**
     * @brief The callback function is called if the table widget clicked.
     * @param row-the row selectd
     * @param column-the column selected
     */
    void on_IDC_btn_clear_all_clicked();
    
    void enable_all_buttons();


private:
    void peformFuncSlot(int funcCode);
    void execute_refresh();
    void disable_all_buttons();
    void handle_alarm(EH4A_ALARM_STRUCT * alarm,ZOO_INT32 error_code);
    
    /**
     * @brief The function is called to initialize.
     */
    void initialize();
    void initialize_title();
    void insert_alarm(EH4A_ALARM_STRUCT * alarm);
    void update_alarm(EH4A_ALARM_STRUCT * alarm);
    bool check_alarm_id_present(ZOO_INT32 alarm_id);
    int get_alarm_in_row(ZOO_INT32 alarm_id);
    void show_message(QString info,QColor color = Qt::black);
private:
    ZOO::PerformFunctionThread * m_funcThread; 
    typedef enum
    {
        FUNCTION_CODE_MIN = -1,
        FC_REFRESH = 0
    }FUNCTION_CODE_ENUM;  
    FUNCTION_CODE_ENUM m_funcCode;    
private:
    QMutex m_sync_lock;
};
#endif // EHUI_H
