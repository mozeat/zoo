/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : EHUI.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "EHUI.h"
#include <QMessageBox>
#include <stdio.h>
#include <QStandardItemModel>
#include <QDebug>
EHUI::EHUI(QWidget *parent) :  QMainWindow(parent)
{
    setupUi(this);

    qRegisterMetaType<QList<QPersistentModelIndex> > ("QList<QPersistentModelIndex>");
    qRegisterMetaType<QAbstractItemModel::LayoutChangeHint> ("QAbstractItemModel::LayoutChangeHint");
    this->m_funcThread =  new ZOO::PerformFunctionThread();
    QObject::connect(this->m_funcThread, SIGNAL(doFuncFinishedSignal()), this, SLOT(enable_all_buttons()));
    this->initialize_title();
    this->initialize();
    qRegisterMetaType<ZOO_INT32>("ZOO_INT32");
    this->m_funcCode = FUNCTION_CODE_MIN;
}
EHUI::~EHUI()
{
    QObject::disconnect(this->m_funcThread, SIGNAL(doFuncFinishedSignal()),
                        this, SLOT(enable_all_buttons()));                  
}
void EHUI::update(EH4A_ALARM_STRUCT * alarm,ZOO_INT32 error_code)
{
    this->handle_alarm(alarm, error_code);
}

void EHUI::run(int funcCode)
{
    switch(funcCode)
    {
        case FC_REFRESH:
            this->execute_refresh();
            break;
        default:
            break;
    }
}
void EHUI::execute(void * param)
{
    ZOO::THREAD_RUN_FUNC_CONTEXT_STRUCT * param_p = (ZOO::THREAD_RUN_FUNC_CONTEXT_STRUCT *)param;
    EHUI * ehui = qobject_cast<EHUI *>(param_p->performObj);
	if(NULL != ehui)
	{
		ehui->run(param_p->funcCode);
	}
}
void EHUI::peformFuncSlot(int funcCode)
{
    bool result = true;
	ZOO::THREAD_RUN_FUNC_CONTEXT_STRUCT * param = new ZOO::THREAD_RUN_FUNC_CONTEXT_STRUCT;
	param->performObj = this;
	param->widgetIndex = 0;
	param->funcCode = funcCode;

	if(NULL != m_funcThread)
	{
		result = m_funcThread->performFunctionStart(EHUI::execute, param);
		if(!result)
		{
			delete param;
		}
	}
	else
	{
	    delete param;
	}
}

void EHUI::on_IDC_btn_refresh_clicked()
{
    __ZOO_LOG("> function entry ...");
    m_funcCode = FC_REFRESH;
    this->disable_all_buttons();
    this->peformFuncSlot(m_funcCode);
    __ZOO_LOG("< function exit...")
}
void EHUI::on_IDC_btn_clear_clicked()
{
   __ZOO_LOG("> function entry ...");

   __ZOO_LOG("< function exit...")
}

void EHUI::on_IDC_btn_clear_all_clicked()
{
    __ZOO_LOG("> function entry ...");
    this->show_message("Clear all alarms ...",Qt::black);

    this->show_message("Clear all alarms ...success",Qt::black);
    __ZOO_LOG("< function exit ...");
}

void EHUI::execute_refresh()
{
    __ZOO_LOG("> function entry ...");
    this->show_message(QString("Refresh ..."),Qt::black);
    QStandardItemModel * model = static_cast<QStandardItemModel*>(IDC_tbv_alarms->model());
    int rows = model->rowCount();
    for(int i = rows - 1; i >= 0; i--)
    {
        QStandardItem* item = model->item(i,0);
        QString text = item->text();
        ZOO_INT32 alarm_id = text.toInt();
        EH4A_ALARM_DETAILS_STRUCT details;
        memset(&details,0x0,sizeof(EH4A_ALARM_DETAILS_STRUCT));
        if(OK == EH4A_get_alarm_info(alarm_id,&details))
        {
            if(!details.is_triggered)
            {
                model->removeRow(i);
            }
        }
    }

    this->show_message(QString("Refresh ...success"),Qt::black);
    __ZOO_LOG("< function exit ...");
}

void EHUI::handle_alarm(EH4A_ALARM_STRUCT * alarm,ZOO_INT32 error_code)
{
    this->show_message(QString("Handle alarm id %1 ...").arg(alarm->alarm_id,0,16),Qt::black);
    __ZOO_LOG(":: alarm is triggered " << alarm->is_triggered);
    __ZOO_LOG(":: trigger_time  " << alarm->trigger_time);
    __ZOO_LOG(":: recover_time  " << alarm->recover_time);
    if(OK == error_code)
    {   
        if(this->check_alarm_id_present(alarm->alarm_id))
        {
            this->update_alarm(alarm);
        }
        else
        {
            if(alarm->is_triggered)
            {
                this->insert_alarm(alarm);
            }
        }
    }
    this->show_message(QString("Handle alarm id %1 ... success").arg(alarm->alarm_id,0,16),Qt::black);
}

void EHUI::disable_all_buttons()
{
    IDC_btn_refresh->setEnabled(false);
}

void EHUI::enable_all_buttons()
{
    IDC_btn_refresh->setEnabled(true);
}

/**
 * @brief The function is called to initialize tasks configuration table.
 */
 void EHUI::initialize()
 {
    __ZOO_LOG("> function entry ...");

    QStandardItemModel * model = static_cast<QStandardItemModel*>(IDC_tbv_alarms->model());
    model->clear();
    QStringList labels;
    labels<< "ID" << "LEVEL"<< "TRIGGERED TIME" <<"RECOVER TIME" << "TYPE" << "COMP" << "ENGLISH" << "CHINESE" << "MATERIAL_ID";
    model->setHorizontalHeaderLabels(labels);
    
    ZOO_INT32 counts = 0;
    ZOO_INT32 session_id = 0;
    ZOO_INT32 error_code = EH4A_find_alarm_counts(EH4A_FOUND_TYPE_TRIGGED,&counts,&session_id);
    if(OK != error_code)
    {
        __ZOO_LOG_WARNING("find alarm counts fail");
        this->show_message("Initialize alarm table ...failed",Qt::red);
        return;
    }
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,
                        __ZOO_FUNC__,
                        "found counts is %i,session_id is %i",
                        counts,session_id);
    if(session_id <= 0)
    {
        //QMessageBox::information(NULL, "find alarm", "out of range");
        return;
    }
    
    for(int i = 0;i < counts; ++ i)
    {
        EH4A_ALARM_STRUCT  alarm;
        memset(&alarm,0,sizeof(EH4A_ALARM_STRUCT));
        error_code = EH4A_get_alarm_data(session_id,i,&alarm);
        if(OK == error_code)
        {
            this->insert_alarm(&alarm);
        }
        else
        {
            QMessageBox::information(NULL, "find alarm", "out of range");
            break;
        }
    }
    __ZOO_LOG("< function exit ...");
 }

 void EHUI::initialize_title()
{
    __ZOO_LOG("> function entry ...");
    QStandardItemModel* model = new QStandardItemModel();
    QStringList labels;
    labels<< "ID" << "LEVEL"<< "TRIGGERED TIME" <<"RECOVER TIME" << "TYPE" << "COMP" << "ENGLISH" << "CHINESE" << "MATERIAL_ID";
    model->setHorizontalHeaderLabels(labels);
    IDC_tbv_alarms->setSelectionMode(QAbstractItemView::SingleSelection);
    IDC_tbv_alarms->setEditTriggers(QAbstractItemView::NoEditTriggers);
    IDC_tbv_alarms->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    IDC_tbv_alarms->setModel(model);
    IDC_tbv_alarms->show();
    __ZOO_LOG("< function exit ...");
}

void EHUI::insert_alarm(EH4A_ALARM_STRUCT * alarm)
{
    QMutexLocker g(&m_sync_lock);
    __ZOO_LOG("> function entry ...");
    __ZOO_LOG(":: alarm_id is " << alarm->alarm_id);
    __ZOO_LOG(":: triggered time " << alarm->trigger_time);
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: triggered time %s",alarm->trigger_time);
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: recover time %s",alarm->recover_time);
    this->show_message(QString("Insert alarm id %1 ...").arg(alarm->alarm_id,0,16),Qt::black);
    QStandardItemModel * model = static_cast<QStandardItemModel*>(IDC_tbv_alarms->model());
    int row = model->rowCount();
    __ZOO_LOG(":: insert to row " << row << " is triggered :" << alarm->is_triggered);
    QStandardItem* id = new QStandardItem(QString("%1").arg(alarm->alarm_id));

    model->setItem(row,0,id);
    
    QStandardItem* level = new QStandardItem(QString("%1").arg(alarm->level));
    model->setItem(row,1,level);

    QStandardItem* tt = new QStandardItem(QString("%1").arg(alarm->trigger_time));
    model->setItem(row,2,tt);

    QStandardItem* rt = new QStandardItem(QString("%1").arg(alarm->recover_time));
    model->setItem(row,3,rt);
    
    QStandardItem* type = new QStandardItem(QString("%1").arg(alarm->type == 0 ? "SW" : "HW"));
    model->setItem(row,4,type);
    
    QStandardItem* comp = new QStandardItem(QString("%1").arg(QString(alarm->component)));
    model->setItem(row,5,comp);

    QStandardItem* english = new QStandardItem(QString("%1").arg(QString(alarm->description_en)));
    model->setItem(row,6,english);

    QStandardItem* chinese = new QStandardItem(QString("%1").arg(QString(alarm->description_cn)));
    model->setItem(row,7,chinese);

    QStandardItem* material_id = new QStandardItem(QString("%1").arg(QString(alarm->material_id)));
    model->setItem(row,8,material_id);
    this->show_message(QString("Insert alarm id %1 ...success").arg(alarm->alarm_id,0,16),Qt::black);
    
    __ZOO_LOG("< function exit ...");
}

void EHUI::update_alarm(EH4A_ALARM_STRUCT * alarm)
{
    __ZOO_LOG("> function entry ...");
    
    __ZOO_LOG(":: triggered time " << alarm->trigger_time);
    __ZOO_LOG(":: recover time " << alarm->recover_time);
    QStandardItemModel * model = static_cast<QStandardItemModel*>(IDC_tbv_alarms->model());
    int row = this->get_alarm_in_row(alarm->alarm_id);
    this->show_message(QString("Update alarm id %1 ...").arg(alarm->alarm_id,0,16),Qt::black);
    __ZOO_LOG(":: alarm id "<< alarm->alarm_id << " in row:" << row << " is triggered:" << alarm->is_triggered);
    if(row >= 0)
    {
        QStandardItem *	item = model->item(row,2);
        item->setText(QString("%1").arg(QString(alarm->trigger_time)));

        item = model->item(row,3);
        item->setText(QString("%1").arg(QString(alarm->recover_time)));

        item = model->item(row,8);
        item->setText(QString("%1").arg(QString(alarm->material_id)));        
    }

    if(!alarm->is_triggered && row >= 0)
    {
        QBrush brush(QColor(Qt::green));
        for(int column = 0; column < 9; column ++)
            model->item(row,column)->setBackground(brush);
    }
    this->show_message(QString("Update alarm id %1 ...success").arg(alarm->alarm_id,0,16),Qt::black);
    __ZOO_LOG("< function exit ...");
}

bool EHUI::check_alarm_id_present(ZOO_INT32 alarm_id)
{
    __ZOO_LOG("> function entry ...");
    bool result = false;
    QStandardItemModel * model = static_cast<QStandardItemModel*>(IDC_tbv_alarms->model());
    int rows = model->rowCount();
    
    for(int i = 0; i < rows; ++i)
    {
        QStandardItem* item = model->item(i);
        
        QString text = item->text();
        __ZOO_LOG(":: row " << i << "," << ",input alarm id is " << alarm_id);
        __ZOO_LOG(":: text " << (item->text().toStdString()));
        if(alarm_id == text.toInt())
        {
            result = true;
            break;
        }
    }
    __ZOO_LOG(":: result is " << result);
    __ZOO_LOG("< function exit ...");
    return result;
}

int EHUI::get_alarm_in_row(ZOO_INT32 alarm_id)
{
    int row = -1;
    QStandardItemModel * model = static_cast<QStandardItemModel*>(IDC_tbv_alarms->model());
    int rows = model->rowCount();
    
    for(int i = 0; i < rows; ++i)
    {
        QStandardItem* item = model->item(i,0);
        QString text = item->text();
        if(alarm_id == text.toInt())
        {
            row = i;
            break;
        }
    }
    
    return row;
}

void EHUI::show_message(QString info,QColor color)
{
    IDC_lbl_message->setText(info);
    QPalette pa;
    pa.setColor(QPalette::WindowText,color);
    IDC_lbl_message->setPalette(pa);
}

