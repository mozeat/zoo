#-------------------------------------------------
#
# Project created by QtCreator 2019-07-30T11:42:25
#
#-------------------------------------------------

QT       += core gui network

QT_cfg = ../../Makefile_tpl_qt
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = TRUI
TEMPLATE = app

# 判断公共配置文件是否存在
!exists($$QT_cfg) {
error($$QT_cfg not exists!)
}

# 包含公共配置文件描述
#include($$PRO_cfg)
include($$QT_cfg)

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        TRUI.cpp

HEADERS += \
        TRUI.h 

FORMS += \
        TRUI.ui 

