/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : EH
 * File Name      : TRUI.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "TRUI.h"
#include <QMessageBox>
#include <stdio.h>
#include <QFile>
#include <QStandardItemModel>
#include <boost/filesystem.hpp>
#include <QTextStream>
#include <QRegExp>
#include <QRegularExpression>
#include <QDebug>
TRUI::TRUI(QWidget *parent) :  QMainWindow(parent)
{
    setupUi(this);
    this->initialize_title();
    this->initialize();
    connect(IDC_cmb_component,
            SIGNAL(currentIndexChanged(const QString &)),
            this,
            SLOT(on_currentIndexChanged(const QString &)));
    
    connect(IDC_lne_search_ctx,
            SIGNAL(textChanged(const QString )),
            this,
            SLOT(on_textChanged(const QString &)));  
    connect(IDC_btn_serach,
            SIGNAL(clicked()),
            this,
            SLOT(on_IDC_btn_serach_clicked()));  
}
TRUI::~TRUI()
{
    
}

void TRUI::on_IDC_btn_refresh_clicked()
{
    __ZOO_LOG("> function entry ...");
    this->show_message(QString("Refresh ..."),Qt::black);
    this->initialize();
    this->show_message(QString("Refresh ...success"),Qt::black);
    __ZOO_LOG("< function exit ...");
}

void TRUI::on_currentIndexChanged(const QString & text)
{
    this->show_message(text);
    this->display_trace_info(this->get_trace_file(text.toStdString()));
}

void TRUI::on_IDC_btn_serach_clicked()
{
    QString comp_id = IDC_cmb_component->currentText();
    QString text = IDC_lne_search_ctx->text();
    QStringList ctx_list =  text.split('|',QString::SkipEmptyParts);
    this->show_message(QString("Serach %1 ...").arg(text),Qt::black);
    this->reset_trace_table();
    QString file(this->get_trace_file(comp_id.toStdString()).c_str());
    QFile f(file);
    if (!f.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        this->show_message(QString("Open %1 ...failed").arg(file),Qt::black);
        return;
    }
    
    int row = 0;
    QTextStream in(&f);
    while (!in.atEnd())
    {
        QString line = in.readLine(); 
        bool contain = false;
        for(QStringList::iterator ite = ctx_list.begin();ite !=  ctx_list.end();ite++)
        {
            if(line.contains(*ite,Qt::CaseInsensitive))
            {
                contain = true;
            }
        }
        
        if(contain)
        {
            QStringList ctx = this->parse_trace_info(line);  
            QString timestamp;
            QString thread_id;
            QString info = line;
            QString level;
            if(ctx.size() > 3)
            {
                timestamp = ctx.at(0);
                thread_id = ctx.at(1);
                level = ctx.at(2);
                info  = ctx.at(3);
            }
            this->display_line(row,timestamp,thread_id,level,info);
            row ++;
        }
        f.close();
    }
    this->show_message(QString("Serach %1 ...success").arg(file),Qt::black);
}

void TRUI::on_textChanged(const QString & text)
{
    QString comp_id = IDC_cmb_component->currentText();
    
    if(text.isEmpty())
    {
        this->display_trace_info(this->m_trace_file_map.begin()->second);
    }
    else
    {
        this->reset_trace_table();
        QString file(this->get_trace_file(comp_id.toStdString()).c_str());
        QFile f(file);
        if (!f.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            this->show_message(QString("Open %1 ...failed").arg(file),Qt::black);
            return;
        }
        
        int row = 0;
        QTextStream in(&f);
        while (!in.atEnd())
        {
            QString line = in.readLine();             
            if(line.contains(text,Qt::CaseInsensitive))
            {    
                QStringList ctx = this->parse_trace_info(line);  
                QString timestamp;
                QString thread_id;
                QString info = line;
                QString level;
                if(ctx.size() > 3)
                {
                    timestamp = ctx.at(0);
                    thread_id = ctx.at(1);
                    level = ctx.at(2);
                    info  = ctx.at(3);
                }
                this->display_line(row,timestamp,thread_id,level,info);
                row ++;
            }
        }
        f.close();
    }
}

QStringList TRUI::parse_trace_info(QString line)
{
    QStringList result;
    QRegularExpression re("\\[(?<timestamp>.+)\\]\\s+\\[(?<threadId>.+)\\]\\s+<(?<level>\\w+)>\\s+(?<info>.+)");
    QRegularExpressionMatch match = re.match(line);
    if(match.hasMatch())
    {       
         result << match.captured("timestamp") << match.captured("threadId");
         result << match.captured("level") << match.captured("info");
    }
    else
    {
        result << line;
    }
    return result;
}

void TRUI::reset_trace_table()
{
    QStandardItemModel * model = static_cast<QStandardItemModel*>(IDC_tbv_trace->model());
    model->clear();
    QStringList labels;
    labels<< "Time" << "Thread Id" << "Level" <<"Infromations";
    model->setHorizontalHeaderLabels(labels);
}

/**
 * @brief The function is called to initialize tasks configuration table.
 */
 void TRUI::initialize()
 {
    IDC_cmb_component->clear();
    std::string sys_base = getenv("HOME");
    this->m_trace_file_map = this->get_components(sys_base + "/log");
    std::map<std::string,std::string>::iterator ite = this->m_trace_file_map.begin();
    while(ite != this->m_trace_file_map.end())
    {
        IDC_cmb_component->addItem(ite->first.data());
        ite ++;
    }
    
    if(!this->m_trace_file_map.empty())
    {
        this->display_trace_info(this->m_trace_file_map.begin()->second);
    }
 }

void TRUI::initialize_title()
{
    __ZOO_LOG("> function entry ...");
    QStandardItemModel* model = new QStandardItemModel();
    QStringList labels;
    labels<< "Time" << "Thread Id" << "Level" << "Infromations";
    model->setHorizontalHeaderLabels(labels);
    IDC_tbv_trace->setSelectionMode(QAbstractItemView::SingleSelection);
    IDC_tbv_trace->setEditTriggers(QAbstractItemView::NoEditTriggers);
    IDC_tbv_trace->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    IDC_tbv_trace->setModel(model);
    IDC_tbv_trace->show();
    __ZOO_LOG("< function exit ...");
}

std::map<std::string,std::string> TRUI::get_components(std::string dir)
{
    __ZOO_LOG("> function entry ...");
    std::map<std::string,std::string> components;
    boost::filesystem::path cur_dir(dir);
    if (!boost::filesystem::exists(cur_dir))
    {
        return components;
    }

    boost::filesystem::recursive_directory_iterator it(cur_dir);
    boost::filesystem::recursive_directory_iterator endit;
    while (it != endit)
    {
        if (boost::filesystem::is_directory(*it))
        {
            std::string comp = it->path().filename().string();
            if(comp == "TR")
            {
                components[comp] = it->path().string() + "/" +  "default.log";
            }
            else
            {
                components[comp] = it->path().string() + "/" + comp + ".log";
            }
        }
        ++it;
    }
    __ZOO_LOG("< function exit ...");
    return components;
}
std::string TRUI::get_trace_file(std::string component_id)
{
    __ZOO_LOG("> function entry ...");
    std::string file;
    std::map<std::string,std::string>::iterator ite = this->m_trace_file_map.begin();
    while(ite != this->m_trace_file_map.end())
    {
        if(ite->first == component_id)
        {
            file = ite->second;
            break;
        }
        ite ++;
    }
    __ZOO_LOG("< function exit ...");
    return file;
}

void TRUI::display_trace_info(std::string file)
{
    this->show_message(QString("Open %1 ...").arg(file.data()),Qt::black);
    QFile f(file.data());
    if (!f.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        this->reset_trace_table();
        this->show_message(QString("Open %1 ...failed").arg(file.data()),Qt::black);
        return;
    }
    int row = 0;
    QTextStream in(&f);
    while (!in.atEnd())
    {
        QString line = in.readLine(); 
        QStringList ctx = this->parse_trace_info(line);  
        QString timestamp;
        QString thread_id;
        QString info = line;
        QString level;
        if(ctx.size() > 3)
        {
            timestamp = ctx.at(0);
            thread_id = ctx.at(1);
            level = ctx.at(2);
            info  = ctx.at(3);
        }
        this->display_line(row,timestamp,thread_id,level,info);
        row ++;
    }
    f.close();
    this->show_message(QString("Open %1 ...success").arg(file.data()),Qt::black);
}

void TRUI::display_line(int row,QString & timestamp,QString & thread_id,QString & level,QString & info)
{
    QStandardItemModel * model = static_cast<QStandardItemModel*>(IDC_tbv_trace->model());
    QStandardItem* timestamp_item = new QStandardItem(timestamp);
    QStandardItem* thread_item = new QStandardItem(thread_id);
    QStandardItem* level_item = new QStandardItem(level);
    QStandardItem* informations_item = new QStandardItem(info);
    model->setItem(row,0,timestamp_item);
    model->setItem(row,1,thread_item);
    model->setItem(row,2,level_item);
    model->setItem(row,3,informations_item);
}

void TRUI::show_message(QString info,QColor color)
{
    IDC_lbl_message->setText(info);
    QPalette pa;
    pa.setColor(QPalette::WindowText,color);
    IDC_lbl_message->setPalette(pa);
}

