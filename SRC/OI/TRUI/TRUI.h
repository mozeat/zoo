/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : TRUI.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef TRUI_H
#define TRUI_H

extern "C"
{
    #include "ZOO_if.h"
    #include "ZOO_tc.h"
    #include "ZOO_enum_to_str.h"
    #include "TR4A_if.h"
    #include "TR4A_type.h"
}
#include "ZOO_if.hpp"
#include "ui_TRUI.h"
#include <QMutex>

class TRUI : public QMainWindow, private Ui::TRUI
{
    Q_OBJECT

public:
    /**
     * @brief Default constructor.
     * @param parent
     */
    explicit TRUI(QWidget *parent = nullptr);

    /**
     * @brief destructor.
     */
    ~TRUI();

private slots:

    /**
     * @brief  The callback function is called if the "refresh" button clicked.
     */
    void on_IDC_btn_refresh_clicked();

    void on_currentIndexChanged(const QString & text);

    void on_IDC_btn_serach_clicked();
    void on_textChanged(const QString & text);
    QStringList parse_trace_info(QString line);

    void reset_trace_table();
private:

    /**
     * @brief The function is called to initialize.
     */
    void initialize();
    void initialize_title();
    std::map<std::string,std::string> get_components(std::string dir);
    std::string get_trace_file(std::string component_id);
    void display_trace_info(std::string file);
    void display_line(int row,QString & timestamp,QString & thread_id,QString & level,QString & info);
    void show_message(QString info,QColor color = Qt::black);
private:
    std::map<std::string,std::string> m_trace_file_map;
};
#endif // TRUI_H
