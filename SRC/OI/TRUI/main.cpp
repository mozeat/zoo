/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : TR
 * File Name      : main.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "TRUI.h"
#include <QApplication>
extern "C"
{
    #include "MM4A_if.h"
    #include "TR4A_if.h"
}

int main(int argc, char *argv[])
{
    MM4A_initialize();
    QApplication a(argc, argv);
    TRUI trui;
    trui.show();
    int error_code = a.exec();
    MM4A_terminate();
    return error_code;
}
