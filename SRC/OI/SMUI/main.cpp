#include "SMUI.h"
#include <QApplication>
#include "qdebug.h"
extern "C"
{
    #include "MM4A_if.h"
}

int main(int argc, char *argv[])
{
    MM4A_initialize();
    QApplication a(argc, argv);
    SMUI smui;
    smui.show();
    int error_code = a.exec();
    MM4A_terminate();
    return error_code;
}
