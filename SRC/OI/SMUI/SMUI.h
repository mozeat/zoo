/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : SMUI.h
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#ifndef SMUI_H
#define SMUI_H

extern "C"
{
    #include "ZOO_if.h"
    #include "ZOO_tc.h"
    #include "ZOO_enum_to_str.h"
    #include "EH4A_if.h"
    #include "EH4A_type.h"
}
#include "ZOO_if.hpp"
#include "ui_SMUI.h"
#include <QComboBox>
#include "keypad.h"

class SMUI : public QMainWindow, private Ui::SMUI
{
    Q_OBJECT

public:
    /**
     * @brief Default constructor.
     * @param parent
     */
    explicit SMUI(QWidget *parent = nullptr);

    /**
     * @brief destructor.
     */
    ~SMUI();

private slots:

    /**
     * @brief  The callback function is called if the "add" button clicked.
     */
    void on_IDC_btn_add_clicked();

    /**
     * @brief  The callback function is called if the "delete" button clicked.
     */
    void on_IDC_btn_delete_clicked();

    /**
     * @brief The callback function is called if the table widget clicked.
     * @param row-the row selectd
     * @param column-the column selected
     */
    void on_IDC_tlw_task2_cellClicked(int row, int column);

    /**
     * @brief The function is called to popup keypad window.
     * @param tableItem
     */
    void keypadPop(QTableWidgetItem* tableItem);

    /**
     * @brief The function is called to close keypad.
     */
    void closeKeypad();

    /**
     * @brief The callback function is called if the "save" button clicked.
     */
    void on_IDC_btn_save_clicked();

    /**
     * @brief The callback function is called if the "modify" button clicked.
     */
    void on_IDC_btn_modify_clicked();

    /**
     * @brief The callback function is called if the "update" button clicked.
     */
    void on_IDC_btn_update_clicked();

    /**
     * @brief The function is called to traversal the component that is existed.
     */
    void traversal_cfg_component();


private:

    /**
     * @brief The function is called to initialize tasks configuration table.
     */
    void initialize_cfg_table();

    /**
     * @brief The function is called to update tasks configuration.
     */
    void updata_cfg_table();

    /**
     * @brief The function is called to creat combox in table.
     * @param cbx-combobox number
     */
    void creat_cbx(ZOO_INT16 cbx);  

    /**
     * @brief The function is called to set window enable/disable.
     * @param status-
     *              1:enable;
     *              0:disable
     */
    void set_edit_status(ZOO_INT16 status);

private:
    /**
     * @brief The trace mode combox attribute.
     */
    std::vector<QComboBox*> m_entity_trace_mode;

    /**
     * @brief The simulation mode combox attribute.
     */
    std::vector<QComboBox*> m_all_sim_mode;

    /**
     * @brief The monitior mode combox attribute.
     */
    std::vector<QComboBox*> m_all_monitior_mode;

    /**
     * @brief The tasks struct attribute.
     */
    ZOO_TASKS_STRUCT m_entity_tasks;

    /**
     * @brief The current row attribute.
     */
    ZOO_INT16 m_current_row;

    /**
     * @brief The current column attribute.
     */
    ZOO_INT16 m_current_column;

    /**
     * @brief The initialize row number attribute.
     */

    ZOO_INT16 m_initialization_row;

    /**
     * @brief instance  keypad class.
     */
    Keypad* m_keypad;

    /**
     * @brief The task config struct attribute.
     */
    ZOO_TASK_STRUCT*  m_task_config=new ZOO_TASK_STRUCT();

    /**
    * @brief alarm struct
    */
   EH4A_ALARM_STRUCT m_alarm_struct;

   /**
    * @brief alarm error code
    */
   ZOO_INT32 m_alarm_error_code;

   /**
    * @brief m_edit_status-
    *        1:edit enable;
    *        0:edit disable;
    */
   ZOO_INT32 m_edit_status;

  };

#endif // SMUI_H
