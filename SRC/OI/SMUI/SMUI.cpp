/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : SM
 * File Name      : SMUI.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/
#include "SMUI.h"
#include "qdebug.h"
#include <QMessageBox>
#include <stdio.h>
#include <QFile>
#include <QTextStream>

SMUI::SMUI(QWidget *parent) :  QMainWindow(parent)
{
    setupUi(this);
    this->m_keypad = nullptr;
    this->initialize_cfg_table();
    this->m_current_row = 0;
    this->m_current_column = 0;
    this->m_initialization_row = 0;
    memset(&m_alarm_struct,0x0,sizeof(EH4A_ALARM_STRUCT));
    this->m_alarm_error_code = 0;
    this->m_edit_status = 0;
}

SMUI::~SMUI()
{
    if(this->m_keypad)
    {
        disconnect(m_keypad, SIGNAL(close_numpad()), this, SLOT(closeKeypad()));
        delete m_keypad;
        this->m_keypad = nullptr;
    }
}
void SMUI::creat_cbx(ZOO_INT16 cbx)
{   __ZOO_LOG("> function entry...");
    QComboBox *IDC_cbx_trace_mode = new QComboBox();

    IDC_cbx_trace_mode->setStyleSheet("QComboBox QAbstractItemView{background:rgb(255,255,255)}");
    IDC_cbx_trace_mode->setStyleSheet("QComboBox{color:rgb(0,0,0)}");
    QStringList trace_mode_list;
    trace_mode_list<<"ZOO_TRACE_MODE_MIN"
                    <<"ZOO_TRACE_DISABLE"
                   <<"ZOO_TRACE_ENABLE";
    IDC_cbx_trace_mode->addItems(trace_mode_list);
    this->m_entity_trace_mode.push_back(IDC_cbx_trace_mode);
    this->IDC_tlw_task2->setCellWidget(cbx,2,this->m_entity_trace_mode[cbx]);

    QComboBox *IDC_cbx_sim_mode = new QComboBox();
    IDC_cbx_sim_mode->setStyleSheet("QComboBox{background:rgb(255,255,255)}");
    IDC_cbx_sim_mode->setStyleSheet("QComboBox{color:rgb(0,0,0)}");
    QStringList  sim_mode_list;
    sim_mode_list<<"ZOO_SIM_MODE_MIN"
                  <<"ZOO_SIM_DISABLE"
                 <<"ZOO_SIM_MODE_1"
                <<"ZOO_SIM_MODE_2"
               <<"ZOO_SIM_MODE_3"
              <<"ZOO_SIM_MODE_4";
    IDC_cbx_sim_mode->addItems(sim_mode_list);
    this->m_all_sim_mode.push_back(IDC_cbx_sim_mode);
    this->IDC_tlw_task2->setCellWidget(cbx,3,this->m_all_sim_mode[cbx]);

    QComboBox *IDC_cbx_monitior_mode = new QComboBox();
    IDC_cbx_monitior_mode->setStyleSheet("QComboBox{background:rgb(255,255,255)}");
    IDC_cbx_monitior_mode->setStyleSheet("QComboBox{color:rgb(0,0,0)}");
    QStringList monitior_mode_list;
    monitior_mode_list<<"ZOO_WATCH_MODE_MIN"
                       <<"ZOO_WATCH_MODE_DISABLE"
                      <<"ZOO_WATCH_MODE_TIMES_1"
                     <<"ZOO_WATCH_MODE_TIMES_2"
                    <<"ZOO_WATCH_MODE_TIMES_3"
                   <<"ZOO_WATCH_MODE_TIMES_4"
                  <<"ZOO_WATCH_MODE_TIMES_5"
                 <<"ZOO_WATCH_MODE_TIMES_6" ;
    IDC_cbx_monitior_mode->addItems(monitior_mode_list);
    this->m_all_monitior_mode.push_back(IDC_cbx_monitior_mode);
    this->IDC_tlw_task2->setCellWidget(cbx,4,this->m_all_monitior_mode[cbx]);
    __ZOO_LOG("< function exit...");
}
void SMUI::initialize_cfg_table()
{   
    __ZOO_LOG(">function entry...");
    if(OK != ZOO_get_entity_tasks(&this->m_entity_tasks))
    {
        __ZOO_LOG(":: get configure tasks fail");
        return;
    }
    __ZOO_LOG(":: task numbers :" << this->m_entity_tasks.number);

    this->m_entity_trace_mode.clear();
    this->m_all_sim_mode.clear();
    this->m_all_monitior_mode.clear();

    if(this->m_entity_tasks.number <= 0)
    {
        __ZOO_LOG_ERROR("task size is 0 ");

        return;
    }

    this->IDC_tlw_task2->setRowCount(this->m_entity_tasks.number);
    this->IDC_tlw_task2->setColumnCount(8);

    for (auto var = 0; var < this->m_entity_tasks.number; ++var)
    {
        SMUI::creat_cbx(var);
        ZOO_TASK_STRUCT & task = this->m_entity_tasks.task[var];

        this->IDC_tlw_task2->setItem(var,0,new QTableWidgetItem(task.component));
        __ZOO_LOG(" ::component :"<<task.component);

        this->IDC_tlw_task2->setItem(var,1,new QTableWidgetItem(task.task_name));
        __ZOO_LOG(" ::task name :"<<task.task_name);

        this->m_entity_trace_mode[var]->setCurrentIndex(task.trace_mode + 1);
        __ZOO_LOG(" ::trace mode :"<<task.trace_mode);

        this->m_all_sim_mode[var]->setCurrentIndex(task.sim_mode + 1);
        __ZOO_LOG(" ::sim mode :"<<task.sim_mode);

        this->m_all_monitior_mode[var]->setCurrentIndex(task.monitor_mode + 1);
        __ZOO_LOG(" ::monitior mode :"<<task.monitor_mode);

        this->IDC_tlw_task2->setItem(var,5,new QTableWidgetItem(QString::number(task.priority)));
        __ZOO_LOG(" ::priority :"<<task.priority);

        this->IDC_tlw_task2->setItem(var,6,new QTableWidgetItem(QString::number(task.stack_size)));
        __ZOO_LOG(" ::stack size :"<<task.stack_size);

        this->IDC_tlw_task2->setItem(var,7,new QTableWidgetItem(tr("Y")));
    }

    this->IDC_tlw_task2->resizeColumnsToContents();

    for (int var = 0; var < this->m_entity_tasks.number; ++var)
    {
        auto item=this->IDC_tlw_task2->item(var,0);
        item->setBackground(QBrush(QColor(240,240,240)));
    }
     this->m_initialization_row = this->IDC_tlw_task2->rowCount();
     this->set_edit_status(0);

    __ZOO_LOG("> function exit...")
}
void SMUI::updata_cfg_table()
{
    __ZOO_LOG("> function entry...");
    if(OK != ZOO_get_entity_tasks(&this->m_entity_tasks))
    {
        __ZOO_LOG(":: get configure tasks fail");
        return;
    }

    for (auto var = 0; var < this->m_entity_tasks.number; ++var)
    {
        ZOO_TASK_STRUCT & task = this->m_entity_tasks.task[var];

        this->IDC_tlw_task2->item(var,0)->setText(task.component);
        __ZOO_LOG("::component="<<task.component);

        this->IDC_tlw_task2->item(var,1)->setText(task.task_name);
        __ZOO_LOG("::task_name="<<task.task_name);

        this->m_entity_trace_mode[var]->setCurrentIndex(task.trace_mode+1);
        __ZOO_LOG("::trace_mode"<<task.trace_mode);

        this->m_all_sim_mode[var]->setCurrentIndex(task.sim_mode+1);
        __ZOO_LOG("::sim_mode"<<task.sim_mode);

        this->m_all_monitior_mode[var]->setCurrentIndex(task.monitor_mode+1);
        __ZOO_LOG("::monitor_mode"<<task.monitor_mode);

        this->IDC_tlw_task2->item(var,5)->setText(QString::number(task.priority));
        __ZOO_LOG("::priority"<<task.priority);

        this->IDC_tlw_task2->item(var,6)->setText(QString::number(task.stack_size));
        __ZOO_LOG("::stack_size"<<task.stack_size);

        this->IDC_tlw_task2->item(var,7)->setText("Y");
    }

    for (int var = 0; var < this->m_entity_tasks.number; ++var)
    {
        auto item = this->IDC_tlw_task2->item(var,0);
        item->setBackground(QBrush(QColor(240,240,240)));
    }
     this->m_initialization_row = this->m_entity_tasks.number;

    __ZOO_LOG("< function exit ...");

}
void SMUI::keypadPop(QTableWidgetItem* tableItem)
{
    __ZOO_LOG("> function entry...");
    if(nullptr == this->m_keypad)
    {
        this->m_keypad = new Keypad(this, tableItem, QString("%1-%2").arg(this->m_current_row + 1).arg(this->m_current_column + 1));
    }
    else
    {
        this->m_keypad->setTableItemStr(tableItem, QString("%1-%2").arg(this->m_current_row + 1).arg(this->m_current_column + 1));
    }

    connect(this->m_keypad, SIGNAL(close_keypad()), this, SLOT(closeKeypad()));
    connect(this->m_keypad, SIGNAL(enter_keypad()), this, SLOT(traversal_cfg_component()));
    this->m_keypad->show();
    __ZOO_LOG("< function exit...");
}
void SMUI::closeKeypad()
{
    __ZOO_LOG("> function entry...");
    qDebug("slot close keypad");
    if(this->m_keypad)
    {
        disconnect(this->m_keypad, SIGNAL(close_keypad()), this, SLOT(closeKeypad()));
        disconnect(this->m_keypad, SIGNAL(close_keypad()), this, SLOT(closeKeypad()));
        this->m_keypad->hide();
        setFocus();
    }

     SMUI::set_edit_status(1);
     __ZOO_LOG("< function exit...");
}
void SMUI::on_IDC_btn_add_clicked()
{
    __ZOO_LOG("> function entry...");
    this->IDC_tlw_task2->setRowCount(this->IDC_tlw_task2->rowCount() + 1);
    this->IDC_tlw_task2->setColumnCount(8);

    for (int var = 0; var < 8; ++var) 
    {

        this->IDC_tlw_task2->setItem(this->IDC_tlw_task2->rowCount() - 1,var,new QTableWidgetItem(""));
    }

    SMUI::creat_cbx(this->IDC_tlw_task2->rowCount() - 1);
    __ZOO_LOG("< function exit...");
}
void SMUI::on_IDC_btn_delete_clicked()
{
    __ZOO_LOG("> function entry...");
    /*int current_row = this->IDC_tlw_task2->currentRow();
    if(current_row >= 0)
    {
        std::string component = this->IDC_tlw_task2->currentItem()->text().toStdString();
        auto error_code = ZOO_remove_cfg_task(component.data());

        if(error_code == OK)
        {
            this->m_entity_trace_mode.erase(m_entity_trace_mode.begin()+this->IDC_tlw_task2->currentRow());
            this->m_all_sim_mode.erase(m_all_sim_mode.begin()+this->IDC_tlw_task2->currentRow());
            this->m_all_monitior_mode.erase(m_all_monitior_mode.begin()+this->IDC_tlw_task2->currentRow());
            this->IDC_tlw_task2->removeRow(this->IDC_tlw_task2->currentRow());
            this->m_initialization_row=this->IDC_tlw_task2->rowCount()-1;
            __ZOO_LOG(":: initialization row :"<<this->IDC_tlw_task2->rowCount()-1);
            this->IDC_tlw_task2->setCurrentItem(nullptr);
        }
        else
        {
            QMessageBox::information(this,NULL,"fail to delete the component.");
        }
    }*/
    __ZOO_LOG("< function exit...");
}
void SMUI::on_IDC_tlw_task2_cellClicked( int row,  int column)
{
    __ZOO_LOG("> function entry...");
    this->m_current_row = row;
    __ZOO_LOG("::current row :" << this->m_current_row);

    this->m_current_column = column;
    __ZOO_LOG(":: current column :" << this->m_current_column);

    if(this->m_edit_status == 1)
    {
        if ((this->m_current_row > this->m_initialization_row - 1) || (this->m_current_column > 0))
        {
            keypadPop(this->IDC_tlw_task2->item(this->m_current_row, this->m_current_column));
            SMUI::set_edit_status(0);
            __ZOO_LOG(":: keypadpop:"<<"popup the keypad window");

        }
    }
   __ZOO_LOG("< function exit...");
}
void SMUI::on_IDC_btn_save_clicked()
{
    __ZOO_LOG("> function entry...");

    ZOO_INT32 result=0;
    auto current_row_count=this->IDC_tlw_task2->rowCount();
    for (int var = 0; var <current_row_count ; ++var)
    {
        auto comp = IDC_tlw_task2->item(var,0)->text();
        __ZOO_LOG("::comp="<<comp.toStdString());

        auto name = IDC_tlw_task2->item(var,1)->text();
        __ZOO_LOG("::name="<<name.toStdString());

        auto trace_mode=this->m_entity_trace_mode[var]->currentText().toStdString();
        __ZOO_LOG("::trace_mode="<<trace_mode);

        auto sim_mode=this->m_all_sim_mode[var]->currentText().toStdString();
        __ZOO_LOG("::sim_mode="<<sim_mode);

        auto monitior_mode=this->m_all_monitior_mode[var]->currentText().toStdString();
        __ZOO_LOG("::monitior_mode="<<monitior_mode);

        auto priority = IDC_tlw_task2->item(var,5)->text();
        __ZOO_LOG("::priority="<<priority.toStdString());

        auto stack_size = IDC_tlw_task2->item(var,6)->text();
        __ZOO_LOG("::stack_size="<<stack_size.toStdString());

        auto error_code = IDC_tlw_task2->item(var,7)->text().toUpper();
        __ZOO_LOG("::error_code="<<error_code.toStdString());

        memcpy(this->m_task_config->component,comp.toStdString().c_str(), comp.size()+1);
        memcpy(this->m_task_config->task_name,name.toStdString().c_str(),name.size()+1);
        this->m_task_config->trace_mode=ZOO_str_to_trace_mode(trace_mode.c_str());
        this->m_task_config->sim_mode=ZOO_str_to_sim_mode(sim_mode.c_str());
        this->m_task_config->monitor_mode=ZOO_str_to_watch_mode(monitior_mode.c_str());
        this->m_task_config->priority=priority.toInt();
        this->m_task_config->stack_size=stack_size.toInt();
        

        if(ZOO_update_task_configuration(this->m_task_config) != OK)
        {
            QString error_message;
            error_message="fail to save component named: ";
            error_message+=error_message+m_task_config->component;
            QMessageBox::information(this,NULL,error_code);
            result=1;
            __ZOO_LOG_ERROR(":: fail to save component named :"<<m_task_config->component);
        }
    }

    if(result == 0)
    {
        QMessageBox::information(this,NULL,"successful");
    }

    __ZOO_LOG("< function exit...");
}
void SMUI::on_IDC_btn_modify_clicked()
{
    __ZOO_LOG("> function entry...");
      this->set_edit_status(1);
    __ZOO_LOG("< function exit...");
}
void SMUI::on_IDC_btn_update_clicked()
{
    __ZOO_LOG("> function entry...");
    this->updata_cfg_table();
    this->set_edit_status(0);
    __ZOO_LOG("< function exit...");

}
void SMUI::set_edit_status(ZOO_INT16 status)
{
    __ZOO_LOG("> function entry...");

    for (std::size_t trace_mode_cbx_num = 0; trace_mode_cbx_num < this->m_entity_trace_mode.size(); ++trace_mode_cbx_num)
    {
        this->m_entity_trace_mode[trace_mode_cbx_num]->setEnabled(status);
    }

    for (std::size_t sim_mode_cbx_num = 0; sim_mode_cbx_num < this->m_all_sim_mode.size(); ++sim_mode_cbx_num)
    {
        this->m_all_sim_mode[sim_mode_cbx_num]->setEnabled(status);
    }

    for (std::size_t monitior_mode_cbx_num = 0; monitior_mode_cbx_num <  this->m_all_monitior_mode.size(); ++ monitior_mode_cbx_num)
    {
        m_all_monitior_mode[monitior_mode_cbx_num]->setEnabled(status);
    }

    this->IDC_btn_add->setEnabled(status);
    this->IDC_btn_save->setEnabled(status);
    this->IDC_btn_delete->setEnabled(status);   
    this->IDC_btn_update->setEnabled(status);
    this->m_edit_status = status;
    __ZOO_LOG("< function exit...");
}
void SMUI::traversal_cfg_component()
{
    __ZOO_LOG("< function entry...");
    auto table_item  = this->IDC_tlw_task2->currentItem();
    auto current_row = this->IDC_tlw_task2->currentRow();
    __ZOO_LOG(":: current row :"<<current_row);

    auto component=this->IDC_tlw_task2->item(table_item->row(),table_item->column())->text();
    __ZOO_LOG(":: component :"<<component.toStdString());
    for (int var = 0; var < current_row; ++var)
    {
        if((this->IDC_tlw_task2->item(var,0)->text() == component)&&(table_item->row()!=var)&&(table_item->column()==0))
        {
            qDebug()<<"there is alredy task named :"<< this->IDC_tlw_task2->item(var,0)->text();
            __ZOO_LOG_ERROR(":: there is alredy task named :"<< this->IDC_tlw_task2->item(var,0)->text().toStdString());
            QMessageBox::information(this,NULL,"This task already existsk","ok");
            this->IDC_tlw_task2->item(table_item->row(),table_item->column())->setText("");
         }
    }
     __ZOO_LOG("< function exit...");
}

