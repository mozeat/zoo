#include "UMUI.h"
#include <QApplication>
#include "MM4A_if.h"
#include "UM4A_if.h"
#include <boost/shared_ptr.hpp>


/*
 *@brief define UMUI instance.
*/
boost::shared_ptr<UMUI> g_ota_hmi;

/*
 *@brief define UMUI instance.
*/
ZOO_HANDLE g_upgrade_notify_fd = 0;

/*
 *@brief define UMUI instance.
*/
ZOO_HANDLE g_inprogress_status_notify_fd  = 0;

/*
 *@brief Register upgrade_notify callback.
 *@param status 
 *@brief context.
*/
ZOO_EXPORT void upgrade_upgrade_notify_callback(IN UM4A_STATUS_STRUCT status,IN ZOO_INT32 error_code,
															IN void *context)
{
	if(g_ota_hmi)
		g_ota_hmi->update_upgrade_status(status,error_code,context);
}


/*
 *@brief Register inprogress callback.
 *@param status 
 *@brief context.
*/
ZOO_EXPORT void upgrade_inprogress_status_callback(IN UM4A_INPROGRESS_STRUCT status,IN ZOO_INT32 error_code,
													IN void *context)
{
	if(g_ota_hmi)
		g_ota_hmi->update_inprogress_staus(status, error_code, context);
}

ZOO_EXPORT int subscribe_all_UM_info()
{
	if(OK != UM4A_status_subscribe(upgrade_upgrade_notify_callback,
													&g_upgrade_notify_fd,NULL))
	{
		return 1;
	}

	if(OK != UM4A_inprogress_subscribe(upgrade_inprogress_status_callback,
															&g_inprogress_status_notify_fd,NULL))
	{
		return 1;
	}

	return OK;
}

/*
 *@brief The main enterance.
*/
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFont font;
    font.setFamily(("Blue Sky Standard"));
    a.setFont(font);

    /*
	 *@brief Memory pool initialize for ipc.
	*/
	MM4A_initialize();
    g_ota_hmi.reset(new UMUI());

    /*
	 *@brief subscribe upgrade message.
	*/
    subscribe_all_UM_info();

    /*
	 *@brief Show .
    */ 
	g_ota_hmi->show();

	/*
	 *@brief Release memory pool.
	*/
	MM4A_terminate();
    return a.exec();
}
