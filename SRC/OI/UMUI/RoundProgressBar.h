#ifndef ROUNDPROGRESSBAR_H
#define ROUNDPROGRESSBAR_H
#include <QObject>
#include <QWidget>
#include <QPainter>

class RoundProgressBar : public QWidget {
    Q_OBJECT
    qreal p; // progress 0.0 to 1.0
  public:
    RoundProgressBar(QWidget * p = 0) : QWidget(p), p(0) {
      setMinimumSize(210, 210);
    }
public slots:
    void setValue(qreal pp) {
      if (p == pp) return;
      p = pp;
      update();
    }
protected:
  void paintEvent(QPaintEvent *) {
    qreal pd = p * 360;
    qreal rd = 360 - pd;
    QPainter p(this);
    p.fillRect(rect(), Qt::transparent);
    p.translate(4, 4);
    p.setRenderHint(QPainter::Antialiasing);
    QPainterPath path, path2;
    path.moveTo(100, 0);
    path.arcTo(QRectF(0, 0, 200, 200), 90, -pd);
    QPen pen, pen2;
    pen.setCapStyle(Qt::FlatCap);
    pen.setColor(QColor("#00bebe"));
    pen.setWidth(5);
    p.strokePath(path, pen);

    path2.moveTo(100, 0);
    pen2.setCapStyle(Qt::FlatCap);
    pen2.setColor(QColor("#1f5167"));
    pen2.setWidth(4);
    path2.arcTo(QRectF(0, 0, 200, 200), 90, rd);
    p.strokePath(path2, pen2);
  }
};
#endif // ROUNDPROGRESSBAR_H
