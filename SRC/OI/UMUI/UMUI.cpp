#include "UMUI.h"
#include "ui_UMUI.h"

const QString PROMPT =  QString("OTA 正在升级系统软件,请稍候");

/**
 * @brief Destructor
 */
UMUI::UMUI(QWidget *parent) :
    QWidget(parent),ui(new Ui::UMUI()),m_error_code(OK)
{
    ui->setupUi(this);
    
    initialize();
}

/**
 * @brief Constructor
 */
UMUI::~UMUI()
{
    SAFTY_DELETE_POINTER(this->m_timer);
    SAFTY_DELETE_POINTER(this->ui);
}

/*
 *@brief Initialize facade.
*/
void UMUI::initialize()
{
    QHBoxLayout* header_layout = new QHBoxLayout();
    header_layout->setMargin(0);
    header_layout->setSpacing(0);
    header_layout->addStretch();
    header_layout->addWidget(ui->clock);

    QHBoxLayout* rndBar_layout = new QHBoxLayout();
    rndBar_layout->setMargin(0);
    rndBar_layout->setSpacing(0);
    rndBar_layout->addStretch(1);
    rndBar_layout->addWidget(ui->IDC_Rnb_package_index);
    rndBar_layout->addStretch(1);

    QVBoxLayout* background_layout = new QVBoxLayout(ui->background);
    background_layout->setMargin(0);
    background_layout->setSpacing(0);
    background_layout->addLayout(header_layout);
    background_layout->addStretch(29);
    background_layout->addLayout(rndBar_layout);
    background_layout->addStretch(10);
    background_layout->addWidget(ui->IDC_Lbl_package_name);
    background_layout->addStretch(24);

    ui->IDC_Lbl_package_name->setText(PROMPT);
    this->m_timer = new QTimer(this);
    
    this->m_timer->setInterval(500);
    this->QObject::connect(m_timer, SIGNAL(timeout()), this, SLOT(timeout()));
    this->m_timer->start();
    this->m_total_packages = 5;
    this->set_inprogress_value(185,500);
    
}

void UMUI::set_inprogress_value(int current, int total)
{
    QString str = QString("%1/%2");

    auto s = str.arg(current/100 + 1).arg(this->m_total_packages);

    ui->IDC_Lbl_indicator->setText(s);

    if(total != 0)
    {
        ui->IDC_Rnb_package_index->setValue(current*1.0/total);
    }
    else
    {
        ui->IDC_Lbl_indicator->setText("");
    }
}

void UMUI::timeout()
{
    static int count =0;
    count ++;

    ui->clock->setText(QTime::currentTime().toString("hh:mm:ss  "));

    QString dot;
    for(int i = 0; i < count; i++)
    {
        dot += ".";
    }

    if(m_error_code == OK)
    {
        ui->IDC_Lbl_package_name->setText(PROMPT + dot);
        count = count %3;
    }
    else
    {
        ui->IDC_Lbl_package_name->setText("安装失败，请联系运维");
        this->m_timer->stop();
    }
}

/*
 *@brief Update .
 *@param status 
 *@brief context.
*/
void UMUI::update_upgrade_status(IN UM4A_STATUS_STRUCT status,
																	IN ZOO_INT32 error_code,
																			IN void *context)
{
	Q_UNUSED(context)
	ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, " packages::%d",status.packages);
	ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, " state::%d",status.state);
	if(error_code == OK)
	{
		this->m_total_packages = status.packages;
		if(status.state == ZOO_OTA_STATE_FINISHED 
				|| status.state == ZOO_OTA_STATE_IDLE)
		{
			qApp->exit();
		}
	}
	this->m_error_code = error_code;
}

/*
 *@brief Update .
 *@param status 
 *@brief context.
*/
void UMUI::update_inprogress_staus(IN UM4A_INPROGRESS_STRUCT status,
																			IN ZOO_INT32 error_code,
																			IN void *context)
{
	Q_UNUSED(context)
	ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, " package_id::%d",status.package_id);
	ZOO_slog(IN ZOO_SEVERITY_LEVEL_NORMAL, __ZOO_FUNC__, " progress::%d",status.inprogress);
	if(error_code == OK)
	{
        this->set_inprogress_value((status.inprogress + 100 * status.package_id - 1), (100 * this->m_total_packages));
	}
	this->m_error_code = error_code;
}
