#-------------------------------------------------
#
# Project created by QtCreator 2019-07-13T14:45:51
#
#-------------------------------------------------

TARGET = UMUI

CONFIG += c++11

QT_cfg = ../../Makefile_tpl_qt

# 判断公共配置文件是否存在
!exists($$QT_cfg) {
error($$QT_cfg not exists!)
}

# 包含公共配置文件描述
#include($$PRO_cfg)
include($$QT_cfg)

LIBS += 

INCLUDEPATH += 
               
SOURCES += *.cpp 

HEADERS += *.h

FORMS += *.ui

RESOURCES += \
    source.qrc

