#ifndef UMUI_H
#define UMUI_H

extern "C"
{
	#include "UM4A_if.h"
	#include "ZOO_if.h"
}
#include <functional>
#include <QWidget>
#include <QTime>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTimer>
#include "RoundProgressBar.h"
namespace Ui {
class UMUI;
}

class UMUI : public QWidget
{
    Q_OBJECT

public:

	/**
     * @brief Destructor
     */
    explicit UMUI(QWidget *parent = nullptr);

    /**
     * @brief Constructor
     */
    ~UMUI();

    /*
	 *@brief Initialize facade.
	*/
    void initialize();

    /*
	 *@brief Update .
	 *@param status 
	 *@brief context.
	*/
    void update_upgrade_status(UM4A_STATUS_STRUCT status,
    										IN ZOO_INT32 error_code,
    											IN void *context);
    /*
	 *@brief Update .
 	 *@param status 
 	 *@brief context.
	*/											
    void update_inprogress_staus(UM4A_INPROGRESS_STRUCT status,
    												IN ZOO_INT32 error_code,
														IN void *context);

private:
    void set_inprogress_value(int current, int total);

private slots:
    void timeout();

private:
    Ui::UMUI *ui;
    QTimer *m_timer;
    int m_total_packages;
    ZOO_INT32 m_error_code;
};

#endif // UMUI_H
