/***********************************************************
 * Copyright (C) 2018, boost CO., LTD
 * All rights reserved.
 * Product        : ZOO
 * Component id   : OI
 * File Name      : PerformFunctionThread.cpp
 * Description    : {Summary Description}
 * History        :
 * Version        date          author         context
 * V1.0.0         2018-10-12    Generator      created
*************************************************************/

#include <ZOO.h>
#include <MM4A_type.h>
#include <MM4A_if.h>
#include "ZOOPerformFunctionThread.h"

namespace ZOO
{
	PerformFunctionThread::PerformFunctionThread(QObject * parent)
		: QThread(parent),
		b_ThreadTerminated(false)
	{
	}

	PerformFunctionThread::~PerformFunctionThread()
	{
		while(isRunning())
		{
			b_ThreadTerminated = true;

			if(!performFunctionStart(NULL, NULL))
			{
				terminate();
			}

			this->wait(5);
		}
	}

	bool PerformFunctionThread::performFunctionStart(THREAD_RUN_FUNC_HANDLE runFunc, void * context_p)
	{
		bool result = true;
		result = m_ThreadMutex.tryLock();

		if(result)
		{
			m_ThreadRunFunc_p = runFunc;

			m_ThreadRunFuncContext_p = context_p;

			if(!isRunning())
			{
				start();
			}

			m_NoFuncRunWaitCondition.wakeAll();

			m_ThreadMutex.unlock();
		}
		else
		{
			printf("m_ThreadMutex.tryLock failed!!\n");
		}

		return result;
	}

	void PerformFunctionThread::run()
	{
		while(1)
		{
			m_ThreadMutex.lock();

			if(NULL != m_ThreadRunFunc_p)
			{
				emit doFuncStartedSignal();

				m_ThreadRunFunc_p(m_ThreadRunFuncContext_p);

				emit doFuncFinishedSignal();
			}
			else
			{
				printf("m_ThreadRunFunc_p is NULL!!!\n");
			}

			if(b_ThreadTerminated)
			{
				break;
			}

			m_NoFuncRunWaitCondition.wait(&m_ThreadMutex);

			m_ThreadMutex.unlock();
			
		}
	}
}



