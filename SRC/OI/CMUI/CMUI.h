/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CMUI
 * File Name    : CMUI.h
 * Description  : ����׷��
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef CMUI_H
#define CMUI_H

extern "C"
{
    #include "CM4A_if.h"
    #include "CM4A_type.h"
    #include "ZOO_if.h"
}
#include "StructureModelClass.h"
#include "ui_CMUI.h"
#include <QMainWindow>
#include <QModelIndex>
#include <QMap>
#include <QStringList>
#include <boost/filesystem.hpp>

class CMUI : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:
    CMUI(QWidget *parent = 0);
    ~CMUI();
public slots:
    void on_save_clicked();
    void on_reload_clicked();
    void on_componentIndex_currentIndexChanged(const QString & compId);
private:
    QStringList parseComponentsFrom(QString cm_dir);
    QMap<QString, CM4A_GROUP_STRUCT*> getGroups(QStringList components);
    void updateModel(const QString & compId);
    void load();
private:
    QMap<QString, CM4A_GROUP_STRUCT*> m_config_structure_map;
    StructureModelClass *m_model;
    QString m_cm_path;
    QString m_current_comp_id;
};

#endif // MAINWINDOW_H
