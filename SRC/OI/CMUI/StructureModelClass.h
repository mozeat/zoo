/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CMUI
 * File Name    : StructureModelClass.h
 * Description  : ����׷��
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/

#ifndef StructureModelClass_H
#define StructureModelClass_H
extern "C"
{
    #include "CM4A_if.h"
    #include "CM4A_type.h"
    #include "ZOO_if.h"
}

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

class MemberItemClass;

class StructureModelClass : public QAbstractItemModel
{
    Q_OBJECT

public:
    StructureModelClass(const QStringList &headers, QMap<QString, CM4A_GROUP_STRUCT*> & config_datas,
              QObject *parent = 0);
    ~StructureModelClass();
	QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;
    bool setHeaderData(int section, Qt::Orientation orientation,
                       const QVariant &value, int role = Qt::EditRole) override;
    bool insertColumns(int position, int columns,
                       const QModelIndex &parent = QModelIndex()) override;
    bool removeColumns(int position, int columns,
                       const QModelIndex &parent = QModelIndex()) override;
    bool insertRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex()) override;
    bool removeRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex()) override;
    CM4A_GROUP_STRUCT* getGroup();

private slots:
    void on_item_dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
private:
    void setupModelData(QMap<QString, CM4A_GROUP_STRUCT*> config_datas, MemberItemClass *parent);
    MemberItemClass * add_child(MemberItemClass *parent,CM4A_ROW_DATA_STRUCT* row_data);
    
    void set_child_data(MemberItemClass *parent,CM4A_ROW_DATA_STRUCT* row_data);
    
    MemberItemClass *getItem(const QModelIndex &index) const;

    MemberItemClass *rootItem;
    CM4A_GROUP_STRUCT* m_group;
};
#endif // StructureModelClass_H
