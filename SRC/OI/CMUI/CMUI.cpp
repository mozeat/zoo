/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CMUI
 * File Name    : CMUI.cpp
 * Description  : ����׷��
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#include "CMUI.h"
#include <QFile>

CMUI::CMUI(QWidget *parent)
    : QMainWindow(parent)
{
    setupUi(this);
    this->m_model = NULL;
    QString config_dir = ZOO_get_parent_dir(ZOO_get_current_exec_path(),3);
    this->m_cm_path = config_dir + "/config/CM";     
    this->load();
    connect(this->save, SIGNAL(clicked()), this, SLOT(on_save_clicked()));
    connect(this->reload, SIGNAL(clicked()), this, SLOT(on_reload_clicked()));
    connect(this->componentIndex, SIGNAL(currentIndexChanged(const QString &)), 
                                this, SLOT(on_componentIndex_currentIndexChanged(const QString &)));   
}

CMUI::~CMUI()
{
    for(QMap<QString,CM4A_GROUP_STRUCT*>::iterator ite = this->m_config_structure_map.begin();
                ite != this->m_config_structure_map.end();ite ++)
    {
        CM4A_GROUP_STRUCT * group = ite.value();
        if(group)
            SAFTY_DELETE_POINTER(group);
    }

    SAFTY_DELETE_POINTER(this->m_model);
}

void CMUI::on_save_clicked()
{
    if(this->m_model != NULL)
    {
        QString current_comp = this->componentIndex->currentText();
        std::string comp = current_comp.toStdString();
        CM4A_GROUP_STRUCT* group = this->m_model->getGroup();
        if(OK != CM4A_write_config(comp.data(),group))
        {
            ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,":: write %s configure failed",comp.data());
        }
    }
}

void CMUI::on_reload_clicked()
{
    this->load();
}

void CMUI::on_componentIndex_currentIndexChanged(const QString & compId)
{
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: current comp is %s",compId.toStdString().data());

    this->updateModel(compId);
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit ...");
}

QStringList CMUI::parseComponentsFrom(QString cm_dir)
{
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: cm_dir is %s",cm_dir.toStdString().data());
    QStringList components;
    boost::filesystem::path cur_dir(cm_dir.toStdString());
    if (!boost::filesystem::exists(cur_dir))
    {
        return components;
    }

    boost::filesystem::recursive_directory_iterator it(cur_dir);
    boost::filesystem::recursive_directory_iterator endit;
    while (it != endit)
    {
        std::string extension = it->path().extension().string();
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"extension:%s",extension.data());
        if (boost::filesystem::is_regular_file(*it) && extension == ".ini")
        {
            QString filename = it->path().filename().c_str();
            QString component = filename.left(2);
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"componet:%s",component.toStdString().data());
            components << component;
        }
        ++it;
    }
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit ...");
    return components;
}

QMap<QString, CM4A_GROUP_STRUCT*> CMUI::getGroups(QStringList components)
{
    QMap<QString, CM4A_GROUP_STRUCT*>::iterator ite = this->m_config_structure_map.begin();
    while(ite != this->m_config_structure_map.end())
    {
        SAFTY_DELETE_POINTER(ite.value());
        ite ++;
    }
    this->m_config_structure_map.clear();
    
    for(QString & comp : components)
    {
        CM4A_GROUP_STRUCT * group = new CM4A_GROUP_STRUCT();
        std::string comp_string = comp.toStdString();
        const char *component_id = comp_string.data();        
        if(OK == CM4A_read_config(component_id,group))
        {
            this->m_config_structure_map[comp] = group;
        }
        else
        {
            delete group;
            ZOO_slog(ZOO_SEVERITY_LEVEL_ERROR,__ZOO_FUNC__,":: read %s configure failed",component_id);
        }
    }
    return this->m_config_structure_map;
}

void CMUI::updateModel(const QString & compId)
{
    QStringList headers;
    headers << tr("Component");
    QMap<QString, CM4A_GROUP_STRUCT*>::iterator ite = this->m_config_structure_map.begin();
    while(ite != this->m_config_structure_map.end())
    {
        if(ite.key() == compId)
        {     
            QMap<QString, CM4A_GROUP_STRUCT*> first_one;
            first_one.insert(compId,ite.value());
            QItemSelectionModel * model = view->selectionModel();
            this->m_model = new StructureModelClass(headers,first_one);
            view->setModel(this->m_model);
            view->expandAll();
            SAFTY_DELETE_POINTER(model);
            for (int column = 0; column < this->m_model->columnCount(); ++column)
                view->resizeColumnToContents(column);
            break;
        }
        ++ ite;
    }
}

void CMUI::load()
{
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"> function entry ...");
    QString current_comp = this->componentIndex->currentText();
    if(this->m_model)
    {
        if(this->m_model->hasChildren()) 
        {
             this->m_model->removeRows(0, this->m_model->rowCount());
        }
    }
    this->componentIndex->clear();
    QStringList comps = this->parseComponentsFrom(this->m_cm_path);
    this->componentIndex->addItems(comps);
    this->m_config_structure_map = this->getGroups(comps);
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: current comp is %s",current_comp.toStdString().data());
    this->updateModel(current_comp);
    ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"< function exit ...");
}

