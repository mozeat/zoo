/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CMUI
 * File Name    : StructureModelClass.cpp
 * Description  : ����׷��
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#include <QtWidgets>
#include "MemberItemClass.h"
#include "StructureModelClass.h"

StructureModelClass::StructureModelClass(const QStringList &headers,QMap<QString, CM4A_GROUP_STRUCT*> & config_datas, QObject *parent)
    : QAbstractItemModel(parent)
{
    QVector<QVariant> rootData;
    foreach (QString header, headers)
        rootData << header;

    rootItem = new MemberItemClass(rootData);
    setupModelData(config_datas, rootItem);
    connect(this,SIGNAL(dataChanged(const QModelIndex &, const QModelIndex &)),
            this,SLOT(on_item_dataChanged(const QModelIndex &, const QModelIndex &)));
}
StructureModelClass::~StructureModelClass()
{
    delete rootItem;
}
int StructureModelClass::columnCount(const QModelIndex & /* parent */) const
{
    return rootItem->columnCount();
}

QVariant StructureModelClass::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    MemberItemClass *item = getItem(index);

    return item->data(index.column());
}

Qt::ItemFlags StructureModelClass::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

MemberItemClass *StructureModelClass::getItem(const QModelIndex &index) const
{
    if (index.isValid()) {
        MemberItemClass *item = static_cast<MemberItemClass*>(index.internalPointer());
        if (item)
            return item;
    }
    return rootItem;
}


QVariant StructureModelClass::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex StructureModelClass::index(int row, int column, const QModelIndex &parent) const
{
    if (parent.isValid() && parent.column() != 0)
        return QModelIndex();

    MemberItemClass *parentItem = getItem(parent);

    MemberItemClass *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

bool StructureModelClass::insertColumns(int position, int columns, const QModelIndex &parent)
{
    bool success;

    beginInsertColumns(parent, position, position + columns - 1);
    success = rootItem->insertColumns(position, columns);
    endInsertColumns();

    return success;
}

bool StructureModelClass::insertRows(int position, int rows, const QModelIndex &parent)
{
    MemberItemClass *parentItem = getItem(parent);
    bool success;

    beginInsertRows(parent, position, position + rows - 1);
    success = parentItem->insertChildren(position, rows, rootItem->columnCount());
    endInsertRows();

    return success;
}

QModelIndex StructureModelClass::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    MemberItemClass *childItem = getItem(index);
    MemberItemClass *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->childNumber(), 0, parentItem);
}


bool StructureModelClass::removeColumns(int position, int columns, const QModelIndex &parent)
{
    bool success;

    beginRemoveColumns(parent, position, position + columns - 1);
    success = rootItem->removeColumns(position, columns);
    endRemoveColumns();

    if (rootItem->columnCount() == 0)
        removeRows(0, rowCount());

    return success;
}

bool StructureModelClass::removeRows(int position, int rows, const QModelIndex &parent)
{
    MemberItemClass *parentItem = getItem(parent);
    bool success = true;

    beginRemoveRows(parent, position, position + rows - 1);
    success = parentItem->removeChildren(position, rows);
    endRemoveRows();

    return success;
}

CM4A_GROUP_STRUCT* StructureModelClass::getGroup()
{
    return this->m_group;
}

void StructureModelClass::on_item_dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    MemberItemClass * item = this->getItem(topLeft);
    int rowNum = item->getRowNum();
    Q_UNUSED(bottomRight)
    if(rowNum <= this->m_group->rows)
    {
        CM4A_ROW_DATA_STRUCT & row_data = this->m_group->members[rowNum];
        std::string current_value = item->data(2).toString().toStdString();
        std::string default_value = item->data(3).toString().toStdString();
        std::string unit = item->data(4).toString().toStdString();
        std::string description = item->data(5).toString().toStdString();
        sprintf(row_data.value.content,"%s",current_value.data());
        sprintf(row_data.default_value.content,"%s",default_value.data());
        sprintf(row_data.unit.content,"%s",unit.data());
        sprintf(row_data.description.content,"%s",description.data());  
        ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,":: current_value[%s],default_value[%s],unit[%s],description[%s]",
              current_value.data(),
                default_value.data(),
                unit.data(),
                description.data());
    }
                
}

int StructureModelClass::rowCount(const QModelIndex &parent) const
{
    MemberItemClass *parentItem = getItem(parent);

    return parentItem->childCount();
}


bool StructureModelClass::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole)
        return false;

    MemberItemClass *item = getItem(index);
    bool result = item->setData(index.column(), value);

    if (result)
        emit dataChanged(index, index);

    return result;
}

bool StructureModelClass::setHeaderData(int section, Qt::Orientation orientation,
                              const QVariant &value, int role)
{
    if (role != Qt::EditRole || orientation != Qt::Horizontal)
        return false;

    bool result = rootItem->setData(section, value);

    if (result)
        emit headerDataChanged(orientation, section, section);

    return result;
}

void StructureModelClass::setupModelData(QMap<QString, CM4A_GROUP_STRUCT*> config_datas, MemberItemClass *parent)
{
    QList<MemberItemClass*> parents;
    parents << parent;
    for(QMap<QString, CM4A_GROUP_STRUCT*>::iterator ite = config_datas.begin();
           ite != config_datas.end();ite ++)
    {
        //set the title
        this->m_group = ite.value();
        QString comp = ite.key();
        QString file_struct_name = ite.key() + "CM_FILE_STRUCT";
        MemberItemClass *parent = parents.last();
        parent->insertChildren(parent->childCount(), 1, rootItem->columnCount());
        parent = parent->child(parent->childCount() - 1);
        parent->setData(0,comp);

        MemberItemClass *root_child = parent;
        MemberItemClass *pre_child = parent;
        MemberItemClass *cur_child = NULL;

        for(int count = 0; count < ite.value()->rows; count ++)
        {
            CM4A_GROUP_STRUCT* group = ite.value();
            CM4A_ROW_DATA_STRUCT row_data = group->members[count];                     
            ZOO_slog(ZOO_SEVERITY_LEVEL_NORMAL,__ZOO_FUNC__,"level:%d,name:%s,value:%s",
                                                row_data.tier,row_data.name,row_data.value.content);       
            int current_tier  =  row_data.tier; 
            int previous_tier =  0;
            //set component id 
            if(count == 0)
            {
                previous_tier = group->members[0].tier;
                cur_child = this->add_child(root_child,&row_data); 
                cur_child->setData(2,"current_value");
                cur_child->setData(3,"default_value");
                cur_child->setData(4,"unit");
                cur_child->setData(5,"description");
                pre_child = cur_child;
            }
            //set member by column 
            else
            {
                //handle current group root
                previous_tier = group->members[count - 1].tier;                
                if(current_tier > previous_tier)
                {
                    cur_child = this->add_child(pre_child,&row_data); 
                    pre_child = cur_child;
                }
                
                //handle current group member  
                if(current_tier == previous_tier)
                {
                    cur_child = this->add_child(pre_child->parent(),&row_data); 
                    pre_child = cur_child;
                }
                
                //handle previous group member
                if(current_tier < previous_tier)
                {
                    MemberItemClass *tmp = pre_child;
                    int tier = previous_tier - current_tier;
                    for (int l = 0;l < tier ; ++ l)
                    {
                        tmp = tmp->parent();
                    }
                    cur_child = this->add_child(tmp->parent(),&row_data); 
                    pre_child = cur_child;
                }
            }       
            cur_child->setComponentId(comp);    
            cur_child->setRowNum(count);
        } 
    }
}

MemberItemClass * StructureModelClass::add_child(MemberItemClass *parent,CM4A_ROW_DATA_STRUCT* row_data)
{
    parent->insertChildren(parent->childCount(), 1, rootItem->columnCount());
    MemberItemClass * child = parent->child(parent->childCount() - 1);
    if(child)
    {
        child->setData(0,row_data->name);
        child->setData(1,row_data->data_type);
        child->setData(2,row_data->value.content);
        child->setData(3,row_data->default_value.content);
        child->setData(4,row_data->unit.content);
        child->setData(5,row_data->description.content);
        child->setData(6,"");
        child->setData(7,"");
    }
    return child;
}
void StructureModelClass::set_child_data(MemberItemClass *parent,CM4A_ROW_DATA_STRUCT* row_data)
{
    parent->child(parent->childCount() - 1)->setData(0,row_data->name);
    parent->child(parent->childCount() - 1)->setData(1,row_data->data_type);
    parent->child(parent->childCount() - 1)->setData(2,row_data->data_type);
    parent->child(parent->childCount() - 1)->setData(3,row_data->data_type);
    parent->child(parent->childCount() - 1)->setData(4,row_data->data_type);
    parent->child(parent->childCount() - 1)->setData(5,row_data->data_type);
    parent->child(parent->childCount() - 1)->setData(6,row_data->data_type);
    parent->child(parent->childCount() - 1)->setData(7,row_data->data_type);
}

