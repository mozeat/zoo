/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CMUI
 * File Name    : MemberItemClass.cpp
 * Description  : ����׷��
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#include "MemberItemClass.h"
#include <QStringList>

MemberItemClass::MemberItemClass(const QVector<QVariant> &data, MemberItemClass *parent)
{
    parentItem = parent;
    itemData = data;
    m_rowNum = 0;
}

MemberItemClass::~MemberItemClass()
{
    qDeleteAll(childItems);
}
MemberItemClass *MemberItemClass::child(int number)
{
    return childItems.value(number);
}
int MemberItemClass::childCount() const
{
    return childItems.count();
}
int MemberItemClass::childNumber() const
{
    if (parentItem)
        return parentItem->childItems.indexOf(const_cast<MemberItemClass*>(this));

    return 0;
}
int MemberItemClass::columnCount() const
{
    return 7;
}
QVariant MemberItemClass::data(int column) const
{
    return itemData.value(column);
}
bool MemberItemClass::insertChildren(int position, int count, int columns)
{
    if (position < 0 ) //|| position > childItems.size()
        return false;

    for (int row = 0; row < count; ++row) 
    {
        QVector<QVariant> data(columns);
        MemberItemClass *item = new MemberItemClass(data, this);
        childItems.insert(position, item);
    }

    return true;
}
bool MemberItemClass::insertColumns(int position, int columns)
{
    if (position < 0 || position > itemData.size())
        return false;

    for (int column = 0; column < columns; ++column)
        itemData.insert(position, QVariant());

    foreach (MemberItemClass *child, childItems)
        child->insertColumns(position, columns);

    return true;
}
MemberItemClass *MemberItemClass::parent()
{
    return parentItem;
}
bool MemberItemClass::removeChildren(int position, int count)
{
    if (position < 0 || position + count > childItems.size())
        return false;

    for (int row = 0; row < count; ++row)
        delete childItems.takeAt(position);

    return true;
}

bool MemberItemClass::removeColumns(int position, int columns)
{
    if (position < 0 || position + columns > itemData.size())
        return false;

    for (int column = 0; column < columns; ++column)
        itemData.remove(position);

    foreach (MemberItemClass *child, childItems)
        child->removeColumns(position, columns);

    return true;
}

bool MemberItemClass::setData(int column, const QVariant &value)
{
    if (column < 0 || column >= itemData.size())
        return false;

    itemData[column] = value;
    return true;
}
void MemberItemClass::setComponentId(QString compId)
{
    this->componentId = compId;
}
QString MemberItemClass::getComponentId()
{
    return this->componentId;
}

void MemberItemClass::setRowNum(int rowNumber)
{
    this->m_rowNum = rowNumber;
}

int MemberItemClass::getRowNum()
{
    return this->m_rowNum;
}

