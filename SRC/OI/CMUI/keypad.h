/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CMUI
 * File Name    : Keypad.cpp
 * Description  : ����׷��
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef KEYPAD_H
#define KEYPAD_H

#include <QWidget>
#include <QButtonGroup>
#include <QTableWidgetItem>

namespace Ui {
class Keypad;
}

class Keypad : public QWidget
{
    Q_OBJECT

public:
    explicit Keypad(QWidget *parent = nullptr, QTableWidgetItem* table_item = nullptr, QString title = "", bool is_upper_case = false);
    ~Keypad();

    enum keyPadButtonGroup
    {
        KPBG_NUM0,
        KPBG_NUM1,
        KPBG_NUM2,
        KPBG_NUM3,
        KPBG_NUM4,
        KPBG_NUM5,
        KPBG_NUM6,
        KPBG_NUM7,
        KPBG_NUM8,
        KPBG_NUM9,
        KPBG_LETTERA,
        KPBG_LETTERB,
        KPBG_LETTERC,
        KPBG_LETTERD,
        KPBG_LETTERE,
        KPBG_LETTERF,
        KPBG_LETTERG,
        KPBG_LETTERH,
        KPBG_LETTERI,
        KPBG_LETTERJ,
        KPBG_LETTERK,
        KPBG_LETTERL,
        KPBG_LETTERM,
        KPBG_LETTERN,
        KPBG_LETTERO,
        KPBG_LETTERP,
        KPBG_LETTERQ,
        KPBG_LETTERR,
        KPBG_LETTERS,
        KPBG_LETTERT,
        KPBG_LETTERU,
        KPBG_LETTERV,
        KPBG_LETTERW,
        KPBG_LETTERX,
        KPBG_LETTERY,
        KPBG_LETTERZ,
        KPBG_AT,
        KPBG_COLON,
        KPBG_POINT,
        KPBG_SLASH,
        KPBG_BLANK,
        KPBG_UPPER_LOWER,
        KPBG_AC,
        KPBG_DELETE,
        KPBG_ENTER,
        KPBG_BACK,
        KPBG_MAX
    };

    void setTableItemStr(QTableWidgetItem* table_item = nullptr, QString title = "", bool is_upper_case = false);

signals:
    void close_keypad();
    void enter_keypad();

private slots:
    void onButtonGroupPressed(int index);

private:
    QTableWidgetItem* m_table_item;
    bool m_upper_case;
    QButtonGroup * m_pButtonGroup;
    Ui::Keypad *ui;
};

#endif // KEYPAD_H
