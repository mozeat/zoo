#include "keypad.h"
#include "ui_keypad.h"

Keypad::Keypad(QWidget *parent, QTableWidgetItem* table_item, QString title, bool is_upper_case) :
    QWidget(parent),
    m_table_item(table_item),
    m_upper_case(is_upper_case),
    ui(new Ui::Keypad)
{
    ui->setupUi(this);

    setWindowFlags(Qt::FramelessWindowHint);
    setGeometry((parent->width()-width())/2, (parent->height()-height())/2, width(), height());

    m_pButtonGroup = new QButtonGroup(this);
    m_pButtonGroup->addButton(ui->button_0, KPBG_NUM0);
    m_pButtonGroup->addButton(ui->button_1, KPBG_NUM1);
    m_pButtonGroup->addButton(ui->button_2, KPBG_NUM2);
    m_pButtonGroup->addButton(ui->button_3, KPBG_NUM3);
    m_pButtonGroup->addButton(ui->button_4, KPBG_NUM4);
    m_pButtonGroup->addButton(ui->button_5, KPBG_NUM5);
    m_pButtonGroup->addButton(ui->button_6, KPBG_NUM6);
    m_pButtonGroup->addButton(ui->button_7, KPBG_NUM7);
    m_pButtonGroup->addButton(ui->button_8, KPBG_NUM8);
    m_pButtonGroup->addButton(ui->button_9, KPBG_NUM9);
    m_pButtonGroup->addButton(ui->button_a, KPBG_LETTERA);
    m_pButtonGroup->addButton(ui->button_b, KPBG_LETTERB);
    m_pButtonGroup->addButton(ui->button_c, KPBG_LETTERC);
    m_pButtonGroup->addButton(ui->button_d, KPBG_LETTERD);
    m_pButtonGroup->addButton(ui->button_e, KPBG_LETTERE);
    m_pButtonGroup->addButton(ui->button_f, KPBG_LETTERF);
    m_pButtonGroup->addButton(ui->button_g, KPBG_LETTERG);
    m_pButtonGroup->addButton(ui->button_h, KPBG_LETTERH);
    m_pButtonGroup->addButton(ui->button_i, KPBG_LETTERI);
    m_pButtonGroup->addButton(ui->button_j, KPBG_LETTERJ);
    m_pButtonGroup->addButton(ui->button_k, KPBG_LETTERK);
    m_pButtonGroup->addButton(ui->button_l, KPBG_LETTERL);
    m_pButtonGroup->addButton(ui->button_m, KPBG_LETTERM);
    m_pButtonGroup->addButton(ui->button_n, KPBG_LETTERN);
    m_pButtonGroup->addButton(ui->button_o, KPBG_LETTERO);
    m_pButtonGroup->addButton(ui->button_p, KPBG_LETTERP);
    m_pButtonGroup->addButton(ui->button_q, KPBG_LETTERQ);
    m_pButtonGroup->addButton(ui->button_r, KPBG_LETTERR);
    m_pButtonGroup->addButton(ui->button_s, KPBG_LETTERS);
    m_pButtonGroup->addButton(ui->button_t, KPBG_LETTERT);
    m_pButtonGroup->addButton(ui->button_u, KPBG_LETTERU);
    m_pButtonGroup->addButton(ui->button_v, KPBG_LETTERV);
    m_pButtonGroup->addButton(ui->button_w, KPBG_LETTERW);
    m_pButtonGroup->addButton(ui->button_x, KPBG_LETTERX);
    m_pButtonGroup->addButton(ui->button_y, KPBG_LETTERY);
    m_pButtonGroup->addButton(ui->button_z, KPBG_LETTERZ);
    m_pButtonGroup->addButton(ui->button_at, KPBG_AT);
    m_pButtonGroup->addButton(ui->button_colon, KPBG_COLON);
    m_pButtonGroup->addButton(ui->button_point, KPBG_POINT);
    m_pButtonGroup->addButton(ui->button_slash, KPBG_SLASH);
    m_pButtonGroup->addButton(ui->button_blank, KPBG_BLANK);
    m_pButtonGroup->addButton(ui->button_upper_lower, KPBG_UPPER_LOWER);
    m_pButtonGroup->addButton(ui->button_ac, KPBG_AC);
    m_pButtonGroup->addButton(ui->button_del, KPBG_DELETE);
    m_pButtonGroup->addButton(ui->button_enter, KPBG_ENTER);
    m_pButtonGroup->addButton(ui->back, KPBG_BACK);
    connect(m_pButtonGroup, SIGNAL(buttonPressed(int)), this, SLOT(onButtonGroupPressed(int)));

    setTableItemStr(table_item, title, is_upper_case);
}

Keypad::~Keypad()
{
    disconnect(m_pButtonGroup, SIGNAL(buttonPressed(int)), this, SLOT(onButtonGroupPressed(int)));

    delete m_pButtonGroup;

    delete ui;
}

void Keypad::onButtonGroupPressed(int index)
{
    switch (index) {
    case KPBG_NUM0:
    case KPBG_NUM1:
    case KPBG_NUM2:
    case KPBG_NUM3:
    case KPBG_NUM4:
    case KPBG_NUM5:
    case KPBG_NUM6:
    case KPBG_NUM7:
    case KPBG_NUM8:
    case KPBG_NUM9:
        ui->input->setText(ui->input->text().append(QString('0' + index - KPBG_NUM0)));
        break;
    case KPBG_LETTERA:
    case KPBG_LETTERB:
    case KPBG_LETTERC:
    case KPBG_LETTERD:
    case KPBG_LETTERE:
    case KPBG_LETTERF:
    case KPBG_LETTERG:
    case KPBG_LETTERH:
    case KPBG_LETTERI:
    case KPBG_LETTERJ:
    case KPBG_LETTERK:
    case KPBG_LETTERL:
    case KPBG_LETTERM:
    case KPBG_LETTERN:
    case KPBG_LETTERO:
    case KPBG_LETTERP:
    case KPBG_LETTERQ:
    case KPBG_LETTERR:
    case KPBG_LETTERS:
    case KPBG_LETTERT:
    case KPBG_LETTERU:
    case KPBG_LETTERV:
    case KPBG_LETTERW:
    case KPBG_LETTERX:
    case KPBG_LETTERY:
    case KPBG_LETTERZ:
    {
        if(m_upper_case)
        {
            ui->input->setText(ui->input->text().append(QString('A' + index - KPBG_LETTERA)));
        }
        else
        {
            ui->input->setText(ui->input->text().append(QString('a' + index - KPBG_LETTERA)));
        }
        break;
    }
    case KPBG_AT:
        ui->input->setText(ui->input->text().append(QString("@")));
        break;
    case KPBG_COLON:
        ui->input->setText(ui->input->text().append(QString(":")));
        break;
    case KPBG_POINT:
        ui->input->setText(ui->input->text().append(QString(".")));
        break;
    case KPBG_SLASH:
        ui->input->setText(ui->input->text().append(QString("/")));
        break;
    case KPBG_BLANK:
        ui->input->setText(ui->input->text().append(QString(" ")));
        break;
    case KPBG_UPPER_LOWER:
    {
        m_upper_case = !m_upper_case;
        if(m_upper_case)
        {
            for(int i = KPBG_LETTERA; i <= KPBG_LETTERZ; i++)
            {
                m_pButtonGroup->button(i)->setText(QString('A' + i - KPBG_LETTERA));
            }
        }
        else
        {
            for(int i = KPBG_LETTERA; i <= KPBG_LETTERZ; i++)
            {
                m_pButtonGroup->button(i)->setText(QString('a' + i - KPBG_LETTERA));
            }
        }
        break;
    }
    case KPBG_AC:
        ui->input->clear();
        break;
    case KPBG_DELETE:
    {
        ui->input->backspace();
        break;
    }
    case KPBG_ENTER:
    {
        if(m_table_item)
        {
           m_table_item->setText(ui->input->text());
        }
        emit enter_keypad();
        emit close_keypad();
        break;
    }
    case KPBG_BACK:
        emit close_keypad();
        break;
    default:
        break;
    }
}

void Keypad::setTableItemStr(QTableWidgetItem* table_item, QString title, bool is_upper_case)
{
    m_table_item = table_item;
    if(m_table_item)
    {
        ui->input->setText(m_table_item->text());
    }

    ui->title->setText(title);

    m_upper_case = is_upper_case;
    if(m_upper_case)
    {
        for(int index = KPBG_LETTERA; index <= KPBG_LETTERZ; index++)
        {
            m_pButtonGroup->button(index)->setText(QString('A' + index - KPBG_LETTERA));
        }
    }
    else
    {
        for(int index = KPBG_LETTERA; index <= KPBG_LETTERZ; index++)
        {
            m_pButtonGroup->button(index)->setText(QString('a' + index - KPBG_LETTERA));
        }
    }
}
