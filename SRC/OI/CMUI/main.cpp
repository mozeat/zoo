/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CMUI
 * File Name    : main.cpp
 * Description  : ����׷��
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#include <QApplication>
#include "CMUI.h"
extern "C"
{
    #include "MM4A_if.h"
}

int main(int argc, char *argv[])
{
    MM4A_initialize();
    QApplication app(argc, argv);
    CMUI cmui;
    cmui.show();
    int error_code = app.exec();
    MM4A_terminate();
    return error_code;
}
