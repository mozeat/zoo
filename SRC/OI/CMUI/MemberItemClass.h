/**************************************************************
 * Copyright (C) 2017, SHANGHAI NEXTEV CO., LTD
 * All rights reserved.
 * Product No.  : ZOO
 * Component ID : CMUI
 * File Name    : MemberItemClass.h
 * Description  : ����׷��
 * History :
 * Version      Date            User        Comments
 * V1.0         2018-03-04      weiwang.sun Create file
 ***************************************************************/
#ifndef MEMBER_ITEM_CLASS_H
#define MEMBER_ITEM_CLASS_H
extern "C"
{
    #include "CM4A_if.h"
    #include "CM4A_type.h"
    #include "ZOO_if.h"
}

#include <QList>
#include <QVariant>
#include <QVector>

//! [0]
class MemberItemClass
{
public:
    explicit MemberItemClass(const QVector<QVariant> &data, MemberItemClass *parent = 0);
    ~MemberItemClass();
public:
    MemberItemClass *child(int number);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    bool insertChildren(int position, int count, int columns);
    bool insertColumns(int position, int columns);
    MemberItemClass *parent();
    bool removeChildren(int position, int count);
    bool removeColumns(int position, int columns);
    int childNumber() const;
    bool setData(int column, const QVariant &value);
    void setComponentId(QString compId);
    QString getComponentId();
    void setRowNum(int rowNumber);
    int getRowNum();
private:
    QList<MemberItemClass*> childItems;
    QVector<QVariant> itemData;
    MemberItemClass *parentItem;
    QString componentId;
    int m_rowNum;
};
#endif // MemberItemClass_H
