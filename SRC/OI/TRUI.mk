SRC_DIR := TRUI
MACHINE = $(ARCH)

ifeq ($(MACHINE),x86)
QMAKE := qmake
%:
	cd $(SRC_DIR) && $(QMAKE) -o Makefile CONFIG+=$(MACHINE) && $(MAKE) $@	
else
QMAKE := /opt/arm/Qt-5.11.2/bin/qmake
%:
	cd $(SRC_DIR) && $(QMAKE) -o Makefile CONFIG+=$(MACHINE) && $(MAKE) $@	
endif     
