#!/usr/bin/env python3
# -*- coding: utf-8 -*-

' a interprocess communication code generator which use header file XX4A_if.h XX4A_type.h  '
from symbol import parameters

__author__ = 'Weiwang Sun'

import sys, re, getopt, os
import time
import codecs
import shutil

def convert_array_size_from_type(type = "ZOO_CHAR",macro_list = []):
    if '][' in type:
        pattern = r'[a-zA-Z0-9_]+\[(.*)\]\s*\[(.*)\]'
        result = re.findall(pattern,type)
        f = result[0][0]
        s = result[0][1]
        s_v = 1
        f_v = 1
        #print(f)
        #print(s)
        if f.isdigit() == False:
            for e in macro_list:
                if f in e.get_name():
                    f_v = int(e.get_value())
        else:
            f_v = int(f)

        #print(f_v)
        if s.isdigit() == False:
            for e in macro_list:
                if s in e.get_name():
                    s_v = int(e.get_value())
        else:
            s_v = int(s)
        #print(s_v)
        return f_v * s_v * (convert_size_from_type(type))

    else:
        pattern = r'[a-zA-Z0-9_]+\[(.*)\]'
        result = re.findall(pattern, type)
        s = result[0]
        #print(s)
        s_v = int(s)
        if s.isdigit() == False:
            for e in macro_list:
                if s in e.get_name():
                    s_v = int(e.get_value())
        return s_v * (convert_size_from_type(type))

def convert_size_from_type(type = ""):
    if 'ZOO_CHAR' in type:
        return 1

    elif 'ZOO_FLOAT' in type:
        return 4

    elif 'ZOO_DOUBLE' in type:
        return 8

    elif 'ZOO_INT32' in type:
        return 4

    elif 'ZOO_UINT32' in type:
        return 4

    elif 'ZOO_INT16' in type:
        return 4

    elif 'ZOO_UINT16' in type:
        return 4

    elif 'ZOO_INT8' in type:
        return 1

    elif 'ZOO_UINT8' in type:
        return 1

    elif 'ZOO_INT64' in type:
        return 8

    elif 'ZOO_UINT64' in type:
        return 8

    elif 'ZOO_LDOUBLE' in type:
        return 12

    elif 'ZOO_ULONG32' in type:
        return 4

    elif 'ENUM' in type:
        return 4

    elif 'enum' in type:
        return 4

    elif 'ZOO_BOOL' in type:
        return 4

    elif 'bool' in type:
        return 1

    elif 'int' in type:
        return 4

    elif 'float' in type:
        return 4

    elif 'double' in type:
        return 8
    else :
        return 4
    return 1

def calculate_struct_size(struct = []):
    return 4;

def is_struct_type(type = 'STRUCT'):
    if 'STRUCT' in type:
        return 1
    elif 'struct' in type:
        return 1
    else:
        return 0

def is_enum_type(type = 'enum'):
    if 'enum' in type:
        return 1
    elif 'ENUM' in type:
        return 1
    else:
        return 0

def is_array_type(type = '[]'):
    if '[' in type:
        return 1
    else:
        return 0

def is_double_dimensional_array(v):
    if '][' in v:
        return 1
    else:
        return 0

def is_single_dimensional_array(v):
    if '[' in v:
        return 1
    else:
        return 0

def get_array_length(name):
    if '[' in name:
        pat = re.compile(r"[a-zA-Z_0-9]*?\[\s?(.*?)\s?\]")
        result=re.findall(pat,name)
        print(result)
        return int(result[0])
    return 0

class ENVIRONMENT_CLASS(object):
    def __init__(self):
        self._current_path = os.getcwd()

    def get_current_path(self):
        return self._current_path


class COMMENT_CLASS(object):
    def __init__(self, comment=None):
        self._str = comment

    def get_string(self):
        return '/* ' + self._str + ' */'

    def get_comment(self, comment):
        return '/**' + '\n' + ' *@brief ' + comment + '\n' + '**/' + '\n'

    def get_comment_param(self, comment, parameters=[]):
        buffer = '/**' + '\n' + ' *@brief ' + comment + '\n'
        for s in parameters:
            p = ' *@param ' + s + '\n'
            buffer = buffer + s
        buffer = buffer + '**/' + '\n'
        return buffer


class FILE_HEADER_COMMENT_CLASS(object):
    def __init__(self, product='', compoent_id='', filename='',file_size = '', description='Auto generate configure file by CM tool',
                 author='Generator', context='created'):
        self._product = product
        self._compoent_id = compoent_id
        self._filename = filename
        self._description = description
        self._version = 'V1.0.0'
        self._date = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        self._author = author
        self._context = context
        self._file_size = file_size

    def add_property(self, product, compoent_id, filename, description, author, context):
        self._product = product
        self._compoent_id = compoent_id
        self._filename = filename
        self._description = description
        self._author = author
        self._context = context

    def get_list(self):
        tmplist = []
        tmplist.append('/***********************************************************')
        tmplist.append(' * Copyright (C) 2018, Shanghai NIO VEHICLE CO., LTD')
        tmplist.append(' * All rights reserved.')
        tmplist.append(' * Product        : ' + self._product)
        tmplist.append(' * Component id   : ' + self._compoent_id)
        tmplist.append(' * File Name      : ' + self._filename)
        tmplist.append(' * Description    : ' + self._description)
        tmplist.append(' * File_size      : ' + self._file_size)
        tmplist.append(' * History        : ')
        tmplist.append(' * Version        date          author         context ')
        tmplist.append(
            ' * ' + self._version + '         ' + self._date + '    ' + self._author + '      ' + self._context)
        tmplist.append('*************************************************************/')
        return tmplist

class MEMBER_CLASS:
    def __init__(self, type='', name='',min='', max='',default = '',description = '',unit = '',current = ""):
        self._type = type
        self._name = name
        self._description = description
        self._min = min
        self._max = max
        self._default = default
        self._current = current
        self._unit = unit
    def get_min_value(self):
        return self._min
    def get_max_value(self):
        return self._max
    def get_default_value(self):
        return  self._default
    def get_current_value(self):
        return  self._current
    def get_type(self):
        return self._type
    def get_name(self):
        return self._name
    def get_description(self):
        return  self._description
    def get_unit(self):
        return self._unit


class MACRO_CLASS(object):
    def __init__(self, name='', value = '',type = ''):
        self._name = name
        self._value = value
        self._type = type
    def get_name(self):
        return  self._name
    def get_value(self):
        return self._value
    def get_type(self):
        return  self._type

class MACRO_PARSER(object):
    def __init__(self, file='', componet = ''):
        self._file = file
        self._macro_list = []
        self._componet = componet
        self._macro_regex_pattern = r'#define\s+([a-zA-Z0-9_]+)\s+([a-zA-Z0-9_]+)'
    def get_macro_list(self):
        with open(self._file, 'r+', encoding='utf-8') as f:
            text = f.read()
            result = re.findall(self._macro_regex_pattern, text)
            print('********************** find macro **********************')
            print(result)
            for m in result:
                name = m[0]
                val = m[1]
                print(name)
                print(val)
                self._macro_list.append(MACRO_CLASS(name,val))
        return self._macro_list

class ENUM_CLASS(object):
    def __init__(self, name = '', member=[]):
        self._name = name
        self._member = member
    def get_name(self):
        return self._name
    def get_members(self):
        return self._member


class ENUM_PARSER(object):
    def __init__(self, file  = "",componet = ''):
        self._file = file
        self._enum_list = []
        self._componet = componet
        self._typedef_enum_regex_pattern = r"typedef\s+enum\s*{([^{}]*)}\s*([a-zA-Z0-9_]+)\s*;"
        self._eumerator_regex_pattern = r'([a-zA-Z][a-zA-Z0-9_]+)'
    def get_enum_list(self):
        #parse header file
        enum_patten = re.compile(self._typedef_enum_regex_pattern, re.UNICODE | re.VERBOSE | re.MULTILINE | re.DOTALL)
        enumerator_list = []
        with open(self._file, 'r+', encoding='utf-8') as f:
            text = f.read()
            result = re.findall(enum_patten,text)
            enumerator_list = result
            print(enumerator_list)
            print(enumerator_list[0])
        #parse member name
        for e in enumerator_list:
            a = e[0].replace("\n","").replace("\t","")
            print(a)
            a_s = a.split(",")
            print(a_s)
            enum_find_result = re.findall(self._eumerator_regex_pattern,a)
            print(enum_find_result)
            member = []
            for m in enum_find_result:
                print("enum m:" + m)
                member.append(MEMBER_CLASS('enum',m))
            self._enum_list.append(ENUM_CLASS(e[1],member))
            print("enum type :" + e[1])
        return self._enum_list


class STRUCT_CLASS(object):
    def __init__(self, name='',member=[]):
        self._name = name
        self._member = member
    def get_name(self):
        return self._name
    def get_members(self):
        return self._member

#class parse struct from header file
class STRUCT_PARSER(object):
    def __init__(self, file  = "",componet = '',macro_list = []):
        self._file = file
        self._struct_list = []
        self._macro_list = macro_list
        self._componet = componet
        self._typedef_struct_regex_pattern = r"typedef\s+struct\s*{([^{}]*)}\s*([a-zA-Z0-9_]+)\s*;"
        self._member_regex_pattern = r'([a-zA-Z0-9_]+)\s+([a-zA-Z0-9_\[\]]+)\s*;(\s*\/\/(.*))?'
        self._default_regex_pattern = r'\/\/\s*default\[([^\]]+)'
        self._current_regex_pattern = r'current\[([^\]]+)'
        self._unit_regex_pattern = r'unit\[([^\]]+)'
        self._range_regex_pattern = r'range\[([^\]]+)'
        self._description_regex_pattern = r'@([^\n]+)'
        self._array_regex_pattern = r'([a-zA-Z0-9-]+)(\[.*)'
        self._array_length_regex_pattern = r'\[([^\]]+)'
    def get_struct_list(self):
        # parse header file
        struct_patten = re.compile(self._typedef_struct_regex_pattern, re.UNICODE | re.VERBOSE | re.MULTILINE | re.DOTALL)
        structure_list = []
        with open(self._file, 'r+', encoding='utf-8') as f:
            text = f.read()
            result = re.findall(struct_patten, text)
            structure_list = result
        #print(structure_list)
        # parse member name
        for e in structure_list:
            find_result = re.findall(self._member_regex_pattern, e[0])
            #print(e)
            #print('**************************parse structure member*********************************')
            member = []
            for m in find_result:
                #print(m)
                typ = m[0]
                name = m[1]
                print("typ: " + typ)
                print("name: " + name)
                #check member variable is arrary , reformate typr and varaiable name
                #for example : "int pixel[2][2]' to 'pixel int[2][2]'
                if '[' in name:
                    name = re.findall(self._array_regex_pattern,name)
                    array = name[0][1]
                    for macro in self._macro_list:
                        src = array
                        target = macro.get_name()
                        new = macro.get_value()
                        if target in src:
                            array = array.replace(target,new)
                    typ = typ + array
                    name = name[0][0]
                    print("name: " + name)

                default = re.findall(self._default_regex_pattern,m[2])
                if len(default) > 0:
                    default = default[0]
                else :
                    default = ''

                current = re.findall(self._current_regex_pattern, m[2])
                if len(current) > 0:
                    current = current[0]
                else:
                    current = ''

                unit = re.findall(self._unit_regex_pattern,m[2])
                if len(unit) > 0:
                    unit = unit[0]
                else :
                    unit = ''

                range = re.findall(self._range_regex_pattern,m[2])
                if len(range) > 0:
                    range_tmp = range[0].split(',')
                    min = range_tmp[0]
                    max = range_tmp[1]
                else:
                    min = ''
                    max = ''

                description = re.findall(self._description_regex_pattern,m[2])
                if len(description) > 0:
                    description = description[0]
                else :
                    description = ''
                member.append(MEMBER_CLASS(typ,name,min,max,default,description,unit,current))
            self._struct_list.append(STRUCT_CLASS(e[1],member))

        return self._struct_list

class ENUM_FORMATE(object):
    def __init__(self, enum = ENUM_CLASS(),variable_name = '',value = '',default_value = '',level = '',description = ''):
        self._enum = enum
        self._name = variable_name
        self._level = level
        self._value = value
        self._default_value = default_value
        self._description = description
    def formate(self):
        space_num = ''
        if int(self._level) > 1:
            for s in range(int(self._level) -1):
                space_num = space_num + '    '
        fm = space_num + '#' + self._level + '<' + self._name + '> ' + '@Group' + ' %' + self._enum.get_name() + ' ('+  self._value +') !'+ self._default_value + ' ~{[-,-]}' + ' & $' + self._description + '' + '\n'
        for e in self._enum.get_members():
            fm = fm + space_num + '    ' + '#' + str(int(self._level) + 1) + '<' + e.get_name() +'> ' + '@member ' + ' %ENUM' + ' () '  + '!' + ' ' + '~{[-,-]}' + ' ' + e.get_default_value() + ' & ' + '$' + '\n'
        return fm

class COMMON_FORMATE(object):
    def __init__(self, member = MEMBER_CLASS(),variable_name = '',level = ''):
        self._member = member
        self._name = variable_name
        self._level = level
    def formate(self):
        space_num = ''
        if int(self._level) > 1:
            for s in range(int(self._level) - 1):
                space_num = space_num + '    '
        s = self._member
        fm = space_num + '#' + str(int(self._level)) + '<' + s.get_name() +'> ' + '@Member ' + ' %' + s.get_type() + ' '  + '(' + s.get_current_value() + ') !' + s.get_default_value() +  ' ~{[' + s.get_min_value() + ',' + s.get_max_value() +  ']}' + ' '+ '&' + s.get_unit() + ' ' + '$' + s.get_description() + '\n'
        return fm

def get_struct_from_list(name = '',s_list = []):
    result = STRUCT_CLASS()
    for s in s_list:
        if name in s.get_name():
            result = s
    return result

def get_enum_from_list(name = '',e_list = []):
    result = ENUM_CLASS()
    for e in e_list:
        #print('---------------find enum from ---------------')
        #print('search enum: ' + name)
        #print('current enum: ' + e.get_name())
        if name in e.get_name():
            result = e
    #print(result.get_name())
    return result

class CALCULATE_STRUCT_SIZE(object):
    def __init__(self, stu = STRUCT_CLASS(),marco_list = [],stu_list=[STRUCT_CLASS()]):
        self._stucture = stu
        self._marco_list = marco_list
        self._stucture_list = stu_list
    def get_size(self):
        size = 0
        for s in self._stucture.get_members():
            typ = s.get_type()
            if is_struct_type(typ) == 1:
                sub_struct = get_struct_from_list(typ, self._stucture_list)
                size = size + CALCULATE_STRUCT_SIZE(sub_struct,self._marco_list,self._stucture_list).get_size()
            elif is_array_type(typ) == 1:
                size = size + convert_array_size_from_type(typ)
            else:
                size = size + convert_size_from_type(typ)
            print(typ + ' size : ' + str(size))
        return size

class CALCULATE_FILE_SIZE(object):
    def __init__(self, main_stu=STRUCT_CLASS(), variable_name='',stu_list=[STRUCT_CLASS()], marco_list=[]):
        self._main_stucture = main_stu
        self._name = variable_name
        self._stucture_list = stu_list
        self._marco_list = marco_list

    def get_size(self):
        size  = CALCULATE_STRUCT_SIZE(self._main_stucture,self._marco_list,self._stucture_list).get_size()
        return size

class STRUCTURE_FORMATE(object):
    def __init__(self, stu = STRUCT_CLASS(),variable_name = '',level = '',stu_list = [STRUCT_CLASS()],enum_list = []):
        self._stucture = stu
        self._name = variable_name
        self._level = level
        self._stucture_list = stu_list
        self._enum_list = enum_list
    def formate(self):
        space_num = ''
        if int(self._level) > 1:
            for s in range(int(self._level) - 1):
                space_num = space_num + '    '
        fm = space_num + '#' + self._level + '<' + self._name + '> ' + '@Group' + ' %' + self._stucture.get_name() + ' $ ' + '' + '\n'
        sub_level = str(int(self._level) + 1)
        #print('structure sub level:' + sub_level)
        for s in self._stucture.get_members():
            typ = s.get_type()
            name = s.get_name()
            if is_array_type(name) == 1:
                for i in get_array_length(name):
                    print("-------------i: " + int(i))
                    if is_struct_type(typ) == 1:
                        #print('find sub structure: ' + typ)
                        sub_struct = get_struct_from_list(typ,self._stucture_list)
                        fm = fm + STRUCTURE_FORMATE(sub_struct,s.get_name(),sub_level,self._stucture_list,self._enum_list).formate()
                    elif is_enum_type(typ) == 1:
                        #print('find sub enum: ' + typ)
                        sub_enum = get_enum_from_list(typ,self._enum_list)
                        fm = fm + ENUM_FORMATE(sub_enum,s.get_name(),s.get_current_value(),s.get_default_value(),sub_level,s.get_description()).formate()
                    else :
                        #print('find common: ' + typ)
                        fm = fm + COMMON_FORMATE(s,s.get_name(),sub_level).formate()
            else:
                if is_struct_type(typ) == 1:
                    # print('find sub structure: ' + typ)
                    sub_struct = get_struct_from_list(typ, self._stucture_list)
                    fm = fm + STRUCTURE_FORMATE(sub_struct, s.get_name(), sub_level, self._stucture_list,
                                                self._enum_list).formate()
                elif is_enum_type(typ) == 1:
                    # print('find sub enum: ' + typ)
                    sub_enum = get_enum_from_list(typ, self._enum_list)
                    fm = fm + ENUM_FORMATE(sub_enum, s.get_name(),s.get_current_value(), s.get_default_value(), sub_level,
                                           s.get_description()).formate()
                else:
                    # print('find common: ' + typ)
                    fm = fm + COMMON_FORMATE(s, s.get_name(), sub_level).formate()
        return fm

class XXMC_FILE_STRUCT(object):
    def __init__(self,componet = '', enum_list = [],struct_list = [],macro_list = [],file = ''):
        self._componet = componet
        self._enum_list = enum_list
        self._struct_list = struct_list
        self._macro_list = macro_list
        self._configure_file = file.replace('.h','.ini')
    def generate_configure_file(self):
        main_structure = self.get_main_structure()
        size = CALCULATE_FILE_SIZE(main_structure,self._macro_list,self._struct_list).get_size()
        print("size  : " + str(size))
        with open(self._configure_file, 'w+', encoding='utf-8') as f:
            for c in FILE_HEADER_COMMENT_CLASS('ZOO', self._componet,self._configure_file,str(size)).get_list():
                f.write(c)
                f.write('\n')
            f.write(STRUCTURE_FORMATE(main_structure,main_structure.get_name(),'1',self._struct_list,self._enum_list).formate())
        return 0

    def get_main_structure(self):
        s = STRUCT_CLASS()
        for s in self._struct_list:
            if "CM_FILE_STRUCT" in s.get_name():
                print('find main structure:' + s.get_name())
                return s
        return s

class Componet(object):
    def __init__(self, type_h = ''):
        self._type_file = type_h
    def get_type_file(self):
        return self._type_file
    def get_id(self):
        return parse_compoent_id(self._type_file)

def parse_compoent_id(file=''):
    print("intput fileName = " + file)
    patten = re.compile(r'(\w+)CM_\w*\.h')
    id = patten.findall(file)
    #print(id)
    print("compoent_id: " + id[0])
    return id[0]


# **
# @ search interface header file
# @ return 1 found else fail
# **
def check_is_type_header_file(file_name=''):
    print("intput fileName = " + file_name)
    result = file_name.find("CM_data.h")
    #print(result)
    if result > -1:
        # print("find XX4A_type.h file: true")
        return 1  # find = true
    else:
        # print("find XX4A_type.h file: failed")
        return 0  # find = true

def search_header_file(dir=''):
    path = os.listdir(dir)
    print("current dir :" + dir)
    file_list = []
    for p in path:
        if os.path.isfile(p):
            if check_is_type_header_file(p) >= 1:
                file_list.append(Componet(p))
    return file_list

def CopyFile(srcfile, dstfile):
    print(srcfile)
    if not os.path.isfile(srcfile):
        print(srcfile)
    else:
        fpath, fname = os.path.split(dstfile)  # 分离文件名和路径
        if not os.path.exists(fpath):
            os.makedirs(fpath)  # 创建路径
        shutil.copyfile(srcfile, dstfile)  # 复制文件

def FileGenenrator(XXCM_data = '',compoent = ''):
    compoent_id = compoent
    enum_list = ENUM_PARSER(XXCM_data, compoent).get_enum_list()
    macro_list = MACRO_PARSER(XXCM_data,compoent).get_macro_list()
    struct_list = STRUCT_PARSER(XXCM_data,compoent,macro_list).get_struct_list()
    XXMC_FILE_STRUCT(compoent, enum_list, struct_list, macro_list, XXCM_data).generate_configure_file()
    return


def ComponentGenerator():
    root_dir = os.getcwd()
    componet_file_list = search_header_file(root_dir)
    for comp in componet_file_list:
        print("data file:" + comp.get_type_file())
        FileGenenrator(comp.get_type_file(),comp.get_id())
    return


if __name__ == '__main__':
    ComponentGenerator()
