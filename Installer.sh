#***********************************************************
#* Copyright (C) 2018, Shanghai NIO VEHICLE CO., LTD
#* All rights reserved.
#* Product        : ZOO
#* Component id   : 
#* File Name      : 
#* Description    : {Summary Description}
#* History        : 
#* Version        date          author         context 
#* V1.0.0         2019-01-11    weiwang.sun     created
#*************************************************************
#!/bin/bash

#############################################################################
#  Global PATH define
#############################################################################
ZOO_DIR="$( cd "$( dirname "$0" )" && pwd )"
IT_DIR=$ZOO_DIR/IT
SRC_DIR=$ZOO_DIR/SRC
IT_INC_DIR=$IT_DIR/inc
PANDA_INC_DIR=$SRC_DIR/common/ext/inc
PANDA_TYP_DIR=$SRC_DIR/common/ext/typ
#############################################################################
#  Step 1: Parse version from Version.label
#############################################################################
VERSION_CTRL_FILE=$ZOO_DIR/Version.label
version=$(cat $VERSION_CTRL_FILE)
echo $version 

#############################################################################
#  Step 2: Remove install inc PATH
#############################################################################
if [ -d "$ZOO_DIR" ]; then
	rm -f $ZOO_DIR/*.tar.gz	
fi

if [ -d "$IT_INC_DIR" ]; then
	rm -rf $IT_INC_DIR/*
else
	mkdir -p $IT_INC_DIR		
fi

#############################################################################
#  Step 3: This section is use to copy componet api header files to the inc path
#  inc path includes all components api.
#  CC_LIST include all components 
#############################################################################
CC_INTERFACE_HEADER=4A_if.h
CC_TYPE_HEADER=4A_type.h
CC_LIST="CM DB MM SM TR EH CAN RC MQ UM"

for c in $CC_LIST;
do 
CC=$c
CC_DIR=$SRC_DIR/$CC/inc
CC_IF=$CC$CC_INTERFACE_HEADER
CC_TYPE=$CC$CC_TYPE_HEADER
if [ ! -d "$CC_DIR" ]; then
	echo "$CC_DIR ... not exist ERROR."
	exit 1
else
	cp -f $CC_DIR/$CC_IF $IT_INC_DIR
	cp -f $CC_DIR/$CC_TYPE $IT_INC_DIR
fi
done


#############################################################################
#  Step 4: Copy zinc to inc dir
#############################################################################
CC=zinc
CC_DIR=$SRC_DIR/$CC

cp -rf $PANDA_INC_DIR/* $CC_DIR/panda
cp -rf $PANDA_TYP_DIR/* $CC_DIR/panda

if [ ! -d "$CC_DIR" ]; then
	echo "$CC_DIR ... not exist ERROR."
	exit 1
else
	cp -rf $CC_DIR/* $IT_INC_DIR
fi

#############################################################################
#  Step 5: Make tar.gz package according the version.label
#############################################################################
FORMATE=.tar.gz
PACKAGE=$version$FORMATE
tar -czvf $PACKAGE IT/

#############################################################################
#  Step 6: Deploy package to test environment
#  "ssh-copy-id -i ~/.ssh/id_rsa.pub zoo@10.110.19.90" set no password login 
#  DelpoyToTest.sh has two parameters
#############################################################################
#chmod 777 Delpoy.exp
#ssh-copy-id -i ~/.ssh/id_rsa.pub zoo@10.110.19.90
#/usr/bin/expect Delpoy.exp  

#############################################################################
#  Step 7: Execute unit test
#############################################################################


#############################################################################
#  Step 8: Push package to Pre-publisher gitlab.
#############################################################################
chmod 777 Pre-publisher.sh
ARGV1=$ZOO_DIR
ARGV2=$PACKAGE
ARGV3=$version
./Pre-publisher.sh $ARGV1 $ARGV2 $ARGV3

#############################################################################
#  Step 9: Delete ZOO path install package(*.tar.gz)
#############################################################################
rm -f $ZOO_DIR/$PACKAGE

