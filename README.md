#  1.zoo
This is an adapative software platform wrote by c/c++,arms to make develop appliation software more fast and easily.  
![datei](doc/ZOO.png) 
## 1.1 What is the zoo goals? 
 * Open it. usually c/c++ platform is private for any companies,it is the reasons why c/c++ not so popular in application software area,but java does.  
there are some open source platform such as ACE,but it's still not so easy for developers.  
## 1.2 What is the application feilds ?  
 * The first project was applied in IOT feild with environment is arm & linux.but it can extend to any fields and should be very easy to make changes,such as server backend development.  
 * it can be an extended distribution system.  
## 1.3 What is the platform features
 *  MQ(Message Queue):a high level ipc framework based on c-style interface.you can define any c-style api for your projects and don't write any code for ipc,the framework helps u to fill them.   
 *  TR(Trace):a high performace log collector.  
 *  DB(Database):a high wrapped database module with c-style,abandon sql language.  
 *  UM(Upgrade Manager):an OTA framework for iot systems.  
 *  SM(System Manager):a system manager for configure process and monitor them.  
 *  CM(Configure manager):a very high abstract module for parsing configure file.  
 *  EH(Exception Handler):a exception handler for subscriber and publish alarms.  
 *  RC(Remote Control):an MQTT module for remote control.  
 *  HTTP:an http and web socket module for subscribe and response.  
## 1.4 OS environment support
 Strongly recommend linux OS. other OS needs to make a little changes,usually should changed the environment paramters,such as get process name by linux use the global variable *program_invocation_short_name*.
 unsupported OS type: tiny OS,such as ucos,RThread and so on.
## 1.5 Third party libraries support
 *  boost   
 *  zeromq  
 *  soci & sqlite & mysql  
 *  network  
 *  openssl  
 *  mosiqitto  
## 1.6 Build  
 * step1: git clone the source code.
 * step2: cd SRC && make   
   _[**attendtion**]: if is x86 step2: cd SRC && make ARCH=x86_   
### 1.6.1 Platform Makefiles   
 * Makefile : the project makefile entrance. 
 * Project_config : configure the third party libraries include path and .so libs,gcc toolchains.  
 * Project.mk : configure software modules,add or note according your application.  
 * Makefile_tpl_linux : gcc / g++ compiler arguments set.  
 * Makefile_tpl_cc : for all software moudles make and clean set.  
 * Makefile_tpl_qt : for Qt application.  
 * Makefile_tpl_cov : static coverity test.  
## 1.7 Configuration  
There are two configuration files,one is zoo.json, the other is project_for_zoo.json.  
  * *zoo.json* is for configure platform,such as *path* *broker* *process*.  
  * *project_for_zoo.json* is for platform load user parameters such as processes and path   
  
# 2. How to create an application demo  
Before talk about how to create apps.we have to clear one thing that is usually application softwares isolate from platform's,  
they deployed at different path, which means platform is hidden for applications,it locates on the top layer of applications layer.  
General speaking zoo should be embedded with OS,such as zoo deploy at path : /opt/ZOO ,while app deploys at: /home/ironman/,   
this is useful while upgrade app or zoo platform through OTA,they can be treated as two parts.  

Probably should follow as bellow steps:    
 * step1: define application software modules abbreviation.
 * step2: create c-style api header file with the name **XX4A_if.h** and the data type definition file **XX4A_type.h**.
 * step3: create project source directories copy from platform directory structure including makefiles.    
 * step4: generate ipc code use python script **IPC.py**, or **FRAMEWORK.py**. **FRAMEWORK.py** not only generate ipc code but application framework code called FCMS(Flow Controller Model Service).     
 * step6: change makefiles Project.mk and Project_config.  
 * step7: make ...  
# 2.1 Define application software module abbreviation  
recommend two letters,such as **XX**,then **XX** will be the unique keywords identy the module.
# 2.2 create c-style api header file  
There are some rules for define c interface and data types,as python script need to identy the keywords.  
 * Rule 1:**XX4A_** or **XX4T** is the keywords. **4A** : for application,**4T**: for test.      
 * Rule 2:every api should begin with **XX4A_** or **XX4T_**(4A : for application,4T for test).   
 * Rule 3:every typedef structure  should begin with **XX4A_** or **XX4T_**.   
 * Rule 4:**XX4A_@@@@** will be recognized as a sync interface. for example *XX4A_initialize()*,the response timeout set 60s as defualt.  
 * Rule 5:**XX4A_@@@@_sig** will be recognized as a signal interface. for example *XX4A_on_button_sig* (one way).  
 * Rule 6:**XX4A_@@@@_req** and **XX4A_@@@@_wait** will be recognized as a async interface,shoul use as a pair. for example *XX4A_initialize_req()*,*XX4A_initialize_wait()*.  
 * Rule 7:**XX4A_@@@@_subscribe** will be recognized as a subscribe interface. this interface should be register a callback function.
 * Rule 8:**void** arguments not supported.
 * Rule 8:two-dimensional array arguments not supported.

# 2.3 Generate IPC code  
Put the two header files at the same directory level with script **IPC.py**,execute the script.multi-modules header files can be generated the same time. the script will scan current directory with the keywords **XX4A_**.  
# 2.4 Generate FCMS code   
Put the two header files at the same directory level with script **FRAMEWORK.py**,execute the script.multi-modules header files can be generated the same time. the script will scan current directory with the keywords **XX4A_**. 

# license 
Apache Licence 2.0
